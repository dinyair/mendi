import React, { useState,  useEffect } from "react"
import { useSelector, useDispatch } from 'react-redux';
import { Card, CardHeader, CardBody, CardTitle, Button, Spinner , Row, Col} from "reactstrap"
import { useDropzone } from "react-dropzone"
import { addProject, deleteProject } from '@containers/planning/initial-state';
import { FormattedMessage } from "react-intl"
import { ScriptsActionTypes } from '@containers/scripts/enums';

  export const ProgrammaticallyDropzone = (props) => {
  const dispatch = useDispatch();
  const { navigateToBreakDownScenes } = props
  const state = useSelector(state => state)
  const { user, events, scripts } = state;
  const activeEvent = events.filter((event) => event.preview)[0];
  const [loading, setLoading] = useState(false)
  const [projectID, setProjectID] = useState(false)
  const [scriptsObject, setScriptsObject] = useState([])
  const [files, setFiles] = useState([])
  const { getRootProps, getInputProps, open  } = useDropzone({
    // accept: "image/*",
    accept: ".doc, .docx, .pdf",
    multiple: true,
    noClick: true,
    noKeyboard: true,
    onDrop: async acceptedFiles => {
        setLoading(true)
        let newProject = await addProject({company_id: user.company_id,project_id: activeEvent.id , attachments: acceptedFiles})
        if( newProject && newProject.script && newProject.script[0] && newProject.script[0].attachments ) {

          newProject.script.forEach((a,i) => {
              acceptedFiles[i].preview =  a.attachments[0].file_url
          });
        setFiles(acceptedFiles)
        setScriptsObject(newProject.script)
        setLoading(false)
       }
    }
  })


  const setScripts = (s) => {
    const unique = [...new Map([...scripts, ...s].map(item => [item.chapter_number, item])).values()]
    dispatch({
      type: ScriptsActionTypes.SET_SCRIPTS,
      payload: unique
    });
    setFiles([])
    setScriptsObject([])
    navigateToBreakDownScenes()
  }



  
  const thumbs = files.map(file => (
    <Col>
    <div className="dz-thumb" key={file.name}>
      <div className="dz-thumb-inner">
      <iframe
        src={"https://docs.google.com/viewer?url=" + file.preview + "&embedded=true"}
        style={{ width: '300px',
                 height: '350px'}}
        className="doc-preview"
      ></iframe>

      {/* <img src={file.preview} className="dz-img" alt={file.name} /> */}
      </div>
      </div>
    </Col>
  ))

  
  useEffect(
    () => () => {
      // Make sure to revoke the data uris to avoid memory leaks
      // files.forEach(file => URL.revokeObjectURL(file.preview))
    },
    [files]
  )
  return (
    <section>
      <div {...getRootProps({ className: "dropzone" })}>
        <input {...getInputProps()} />
        
        <p className="mx-1">
         Lorem Ipsum הוא פשוט טקסט גולמי של תעשיית ההדפסה וההקלדה.
        </p>
      </div>
      {!files.length ?
       loading ?
       <Button color="primary" className="mb-1">
        <Spinner color="white" size="sm" type="grow" />
        <span className="ml-50"> <FormattedMessage id='loading'/>...</span>
      </Button>
       :
      <Button.Ripple color="primary" outline className="my-1" onClick={open}>
          <FormattedMessage id='upload_script'/>
      </Button.Ripple>
      :
      <Button.Ripple color="primary" outline className="my-1"
       onClick={()=>{
        deleteProject(projectID)
        setScriptsObject([])
        setFiles([])
        }}>
         <FormattedMessage id='rechoose'/>
      </Button.Ripple>}
      
      <Row  className="justify-content-start">
        {thumbs}
      </Row>

    {thumbs && thumbs.length ? (
      <Button.Ripple color="primary" outline className="my-1"
       onClick={()=> setScripts(scriptsObject)}>
         <FormattedMessage id='ok'/>
      </Button.Ripple>
     ) : null}

    </section>
  )
}

class DropzoneProgrammatically extends React.Component {

  constructor(props){
    super(props)
  }
  render() {
    return (
      <Card>
        <CardHeader>
          <CardTitle> <FormattedMessage id={'uploading_script'} /></CardTitle>
        </CardHeader>
        <CardBody>
          <ProgrammaticallyDropzone navigateToBreakDownScenes={this.props.navigateToBreakDownScenes}/>
        </CardBody>
      </Card>
    )
  }
}

export default DropzoneProgrammatically