import * as planogramApi from '@src/planogram/shared/api/planogram.provider';
import { setStore } from '@src/planogram/shared/store/planogram/store/store.actions';
import { PlanogramAisle } from "@src/planogram/shared/store/planogram/planogram.types";
import * as systemApi from '@src/planogram/shared/api/settings.provider';
import { setBranches, setLayouts, setStores } from '@src/planogram/shared/store/system/data/data.actions';
import { getNewPlano } from '@containers/user/initial-state';
import { NEWPLANO_ACTION } from "@src/planogram/shared/store/newPlano/newPlano.types";
import { rowData } from '@src/planogram/DataReports/index'

export const getEmptyAisle = async (store: any, user: any, aisleId: number) => {
    let newAisle = await planogramApi.fetchPlanogramAisle(store ? store.store_id : '', aisleId, user ? user.db : '');
    if (!newAisle || newAisle === undefined) {
        alert("Problem retrieving the aisle from the server");
        return;
    }
    if (store) {
        for (let i = 0; i < store.aisles.length; i++) {
            if (store.aisles[i] != undefined && newAisle != undefined && store.aisles[i].aisle_id === newAisle.aisle_id) {
                store.aisles[i] = newAisle;
                await setStore(store);
                break;
            }
        }
        return store
    } else return null
    // this.setState({ isLoading: false });
}


export const PreviousSavedStore = (aisleSaveArray: any, aisleSaveIndex: number) => {
    let newIndex = aisleSaveIndex - 1;
    if (newIndex > 8) newIndex = 8;
    if (newIndex < 0) newIndex = 0;
    let newAisle = aisleSaveArray[newIndex];
    if (newAisle) {
        return {
            aisleSaveArray: aisleSaveArray[newIndex],
            newIndex
        }
    } else return null
}

export const NextSavedStore = (aisleSaveArray: any, aisleSaveIndex: number) => {
    let newIndex = aisleSaveIndex + 1;
    if (newIndex > 9) newIndex = 9;
    if (newIndex < 0) newIndex = 0;
    let newAisle = aisleSaveArray[newIndex];
    if (newAisle) {
        return {
            aisleSaveArray: aisleSaveArray[newIndex],
            newIndex
        }
    } else return null
}

export const SaveAisle = async (store: any, aisle: PlanogramAisle | null) => {
    if (!store || !aisle || !aisle.aisle_id) { alert("Unable to save current aisle"); return null; }
    let _aisle = await planogramApi.saveStoreAisleSpecial(store.store_id, aisle)
    if (_aisle) return _aisle
    else return null

}


export const FlashBtn = (classname: string, btn: any) => {
    btn.classList.add(classname);
    setTimeout(() => { btn.classList.remove(classname) }, 3500)
}

export function toMap(list: any[], property = "BarCode") {
    const map: any = {};
    if (list && list.length) {
        for (let i = 0; i < list.length; i++)
            map[list[i][property]] = list[i];
    }
    return map;
}
export interface itemsToFetchInterface {
    state?: any
    type: string,
    forceUpdate: boolean
    params?: any
    changeRows?: any
    setDataRows?: any
    dontUpdateState?:boolean
}
export const fetchAndUpdatePlanogram = async (itemsToFetch: itemsToFetchInterface[], dispatch: any,) => {
    let data: any = {};
    if (itemsToFetch && dispatch) {
        for (let i = 0; i < itemsToFetch.length; i++) {
            let item = itemsToFetch[i];
            if (item.state && !item.state.length || item.forceUpdate) {
                switch (item.type) {
                    case 'stores':
                        data.store = await systemApi.fetchStores();
                        dispatch(setStores(data.store));
                        break;
                    case 'layouts':
                        data.layouts = await systemApi.fetchLayouts();
                        dispatch(setLayouts(data.layouts));
                        break;
                    case 'branches':
                        data.branches = await systemApi.fetchBranches();
                        dispatch(setBranches(data.branches));
                        break;
                    case 'newplano': {
                        if (item.params) {
                            // Data that can be sent 
                            // updatedb// aisle_id // date_flag // fromDate // toDate // branches // layoutId // groups // subgroups // copy
                            let dataToSend: any = {}
                            for (let [key, value] of Object.entries(item.params)) {
                                dataToSend[key] = value
                            }
                            data.newPlano = await getNewPlano(dataToSend)
                            if (item.changeRows) {
                                // if (item.setDataRows) {
                                    // data.newPlano.data = data.newPlano.newplano ? data.newPlano.newplano : data.newPlano.data
                                    let rowsToSend = data.newPlano.data && data.newPlano.data.length ? data.newPlano.data : data.newPlano.newplano ? data.newPlano.newplano:[]
                                    data.newPlano.data = rowData(rowsToSend, item.changeRows)
                                // }
                            }
                            if (data.newPlano && !item.dontUpdateState) {
                                dispatch({
                                    type: NEWPLANO_ACTION.SET_NEWPLANO,
                                    payload: data.newPlano
                                });
                            }
                        }
                    }
                }
            }
        }
    }
    return data;
};
