import { getRequest, postRequest } from "@src/http";

export const getCatalogUpdated = async () => {
    const resp = await getRequest<any>(`getcatalog`, {});
    return resp
}
export const getSubGroup = async () => {
    const resp = await getRequest<any>(`getSubGroup`, {});
    return resp
}
export const getDivision = async () => {
    const resp = await getRequest<any>(`getClasses`, {});
    return resp
}
export const getSeries = async () => {
    const resp = await getRequest<any>(`getDegems`)
    return resp
}
export const getBranch = async () => {
    const resp = await getRequest<any>(`getBranch`, {});
    return resp
}

export const getAllBranches = async () => {
    const resp = await getRequest<any>(`getBranch1`, {});
    return resp
}
export const getSecondCatalog = async () => {
    const resp = await getRequest<any>(`getCatalogA?arc1=22`)
    return resp
}
export const getFullCatalogNoArchive = async () => {
    const resp = await getRequest<any>(`getCatalogA?arc1=0`)
    return resp
}
export const getGroup = async () => {
    const resp = await getRequest<any>(`getGroup`)
    return resp
}
export const getSuppliers = async () => {
    const resp = await getRequest<any>('AppSapakim')
    return resp
}
export const getSubSuppliers = async () => {
    const resp = await getRequest<any>('AppGetSubSapak')
    return resp
}
export const getSegments = async () => {
    const resp = await getRequest<any>('getSegment')
    return resp
}

export const getLayouts = async () => {
    const resp = await getRequest<any>('getLayout')
    return resp
}

export const getModels = async () => {
    const resp = await getRequest<any>('getModel')
    return resp
}


export const getLogo = async () => {
    const resp = await getRequest<any>(`getLogo`, {});
    return resp
}


// export const reportsInitialState: any = {};
