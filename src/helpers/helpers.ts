
export const filteredOptions = (options: any[], field: string, filter: string | null) => {
  if (filter && filter.length) return options.filter((o: any) => o[field].includes(filter.toLowerCase().trim()))
  else return options
}
export const aggFunction = (AllRows: any[], rows: any, type: string) => {
  let rowsToCalc = AllRows.filter((g => g.isSelected))

  let totalRows = rowsToCalc.reduce((acc: number, row: any) => acc + Number(row[String(type)]), 0)
  // console.log(type, totalRows, rows)
  let result = (rows / totalRows) * 100
  if (!result) result = 0
  return result
}

export function toMap(list: any[], property = "BarCode") {
  const map: any = {};
  if (list && list.length) {
      for (let i = 0; i < list.length; i++)
          map[list[i][property]] = list[i];
  }
  return map;
}


export const groupBy = (arr: any[], fields: any[], fatherIndex?: number, PropfathersArray?: any, formatMessage?: any) => {

  let field = fields[0]  // one field at a time
  if (!field) return arr
  // let filteredArr = arr.filter((g=>g['isSelected']))
  // console.log({filteredArr})
  let retArr = Object.values(
    arr.reduce((obj, current) => {
      if (!obj[current[field.field]]) obj[current[field.field]] = { field: field.field, value: current[field.field], fatherIndex, rows: [] }
      obj[current[field.field]].rows.push(current)
      return obj
    }, {}))

  if (fields.length) {
    retArr.forEach((obj: any, index: number) => {
      let fathersArray = PropfathersArray && PropfathersArray.length ? PropfathersArray : []
      if (fatherIndex || fatherIndex === 0) {
        fathersArray = [...fathersArray, fatherIndex]
      }
      let rowsForCalc = obj.rows.filter(((g: any) => g.isSelected))
      obj.avgTotal = rowsForCalc.reduce((acc: number, row: any) => acc + Number(row['Avg']), 0)
      obj.totalRowsPrice = rowsForCalc.reduce((acc: number, row: any) => acc + row['TotalPrice'], 0)
      obj.totalRowsAmount = rowsForCalc.reduce((acc: number, row: any) => acc + row['TotalAmount'], 0)
      obj.totalMarketAmount = rowsForCalc.reduce((acc: number, row: any) => acc + row['MarketPrice'], 0)
      obj.group = true
      obj.collapsed = true

      obj.id = field.id ? obj.rows[0][field.id] : 0
      obj.count = obj.rows.length
      obj.index = index
      obj.fathersArray = fathersArray

      let TotalAmount_buy = rowsForCalc.reduce((acc: number, row: any) => acc + Number(row['TotalAmount_buy']), 0)
      let TotalPrice_buy = rowsForCalc.reduce((acc: number, row: any) => acc + Number(row['TotalPrice_buy']), 0)

      obj.percentTotalProfit =  TotalPrice_buy && TotalAmount_buy && obj.totalRowsPrice && obj.totalRowsAmount ? (1 - ((TotalPrice_buy / TotalAmount_buy) / ((obj.totalRowsPrice / obj.totalRowsAmount) / 1.17))) * 100 : 0
      obj.percentPrice = aggFunction(arr, obj.totalRowsPrice, 'TotalPrice')
      obj.percentAmount = aggFunction(arr, obj.totalRowsAmount, 'TotalAmount')
      obj.percentAvg = aggFunction(arr, obj.avgTotal, 'Avg')
      obj.percentMarketPrice = aggFunction(arr, obj.totalMarketAmount, 'MarketPrice')
      obj.editedPercentAvg = null//aggFunction(arr, obj.avgTotal, 'Avg')
      obj.rows = groupBy(formatMessage ? rowData(obj.rows, formatMessage) : obj.rows, fields.slice(1), index, fathersArray, formatMessage)

      //Object.entries(mg).slice(1).reduce((obj:any, [k, v]) => ({ ...obj, [k]: v }), {})
    })
  }
  return retArr
}



//   function groupBy(arr:any[], mg:any[]) {
// 		let fields:any[] = Object.keys(mg).length ?  Object.keys(mg) : []
// 		let grouped = {};
// 		if( !fields.length ) return arr
// 		arr.forEach(function (a) {
// 				fields.reduce(function (o, g, i) {                            // take existing object,
// 				o[a[g]] = o[a[g]] || (i + 1 === fields.length ? [] : {}); // or generate new obj, or
// 				return o[a[g]];                                        // at last, then an array
// 			}, grouped).push(a);

// 		});
// 		return grouped
// }

export const rowData = (rows: any[], formatMessage: any) => {
  let newRows = rows ? rows.map((p: any, index: number) => {
    let mergedRows = rows.filter((g: any) => g.isSelected)//mergedGroups[mergedGroups.length-1] ? rowData.filter((row:any)=> row[mergedGroups[mergedGroups.length-1]] === p[mergedGroups[mergedGroups.length-1]] ) : rowData
    let totalRowsPrice = mergedRows.reduce((acc: number, row: any) => acc + row['TotalPrice'], 0)
    let Percent_TotalPrice: number = p['TotalPrice'] > 0 && totalRowsPrice > 0 ? ((p['TotalPrice'] / totalRowsPrice) * 100) : 0
    let totalRowsAmount = mergedRows.reduce((acc: number, row: any) => acc + row['TotalAmount'], 0)
    let Percent_TotalAmount: number = p['TotalAmount'] && totalRowsAmount ? ((p['TotalAmount'] / totalRowsAmount) * 100) : 0
    let Avg = p['TotalPrice'] && p['TotalAmount'] ? ((p['TotalPrice'] + p['TotalAmount']) / 2) : 0
    let Percent_Avg = Percent_TotalPrice && Percent_TotalAmount ? ((Percent_TotalPrice + Percent_TotalAmount) / 2) : 0
    let Percent_Profit: number = p.TotalPrice_buy && p.TotalAmount_buy && p.TotalPrice && p.TotalAmount ? (1 - ((p.TotalPrice_buy / p.TotalAmount_buy) / ((p.TotalPrice / p.TotalAmount) / 1.17))) * 100 : 0
    let totalRowsMarket = mergedRows.reduce((acc: number, row: any) => acc + row['TotalMarket'], 0)
    let Percent_MarketAmount: number = p['TotalMarket'] && totalRowsMarket ? ((p['TotalMarket'] / totalRowsMarket) * 100) : 0
    let Series = p.Series && p.Series != 'null' ? p.Series : formatMessage({ id: 'other' })
    let Segment = p.Segment && p.Segment != 'null' ? p.Segment : formatMessage({ id: 'other' })
    let Brand = p.Brand && p.Brand != 'null' ? p.Brand : formatMessage({ id: 'other' })
    let Status = p.Blocked ? formatMessage({ id: 'Blocked' }) : p.NewBarCode ? formatMessage({ id: 'NewBarCode' }) : p.LowSale ? formatMessage({ id: 'LowSale' }) : formatMessage({ id: 'Regular' })

    return {
      ...p,
      Status,
      Series,
      isSelected: p.isSelected ? true : false,
      Segment,
      Brand,
      TotalPrice: p.TotalPrice ? Number(p.TotalPrice.toFixed(0)) : 0,
      Percent_TotalPrice: p.isSelected ? Number(Percent_TotalPrice.toFixed(Percent_TotalPrice > 0 ? 2 : 0)) : 0,
      Percent_TotalAmount: p.isSelected ? Number(Percent_TotalAmount.toFixed(Percent_TotalAmount > 0 ? 2 : 0)) : 0,
      Avg: Avg,
      Percent_Avg: p.isSelected ? Number(Percent_Avg.toFixed(Percent_Avg > 0 ? 2 : 0)) : 0,
      MarketPrice: p.MarketPrice ? Number(p.MarketPrice.toFixed(2)) : 0,
      Percent_Profit: p.isSelected ? Number(Percent_Profit.toFixed(Percent_Profit > 0 ? 2 : 0)) : 0,
      Percent_MarketAmount: p.isSelected ? Number(Percent_MarketAmount.toFixed(Percent_MarketAmount > 0 ? 2 : 0)) : 0
    }
  }) : []
  return newRows
}



export const filterBy = (data: any[], filter: { [x: string]: any }, sort: { asc: any; col: string | number }) => {

  return data.filter((row: any) => {
    //Сolumn comparison
    return Object.keys(filter).every(title => {
      //corresponding column
      const currentFilter = filter[title]
      if (currentFilter === null || currentFilter.value === "" || currentFilter.type === "") return true
      console.log(typeof row[title])
      if (typeof row[title] === "number") {

        // filter type
        switch (currentFilter.type) {
          case "=":
            return row[title] == currentFilter.value
          case "!=":
            return row[title] != currentFilter.value
          case "<":
            return row[title] < currentFilter.value
          case ">":
            return row[title] > currentFilter.value
          case "<=":
            return row[title] <= currentFilter.value
          case ">=":
            return row[title] >= currentFilter.value
          case "><":
            return row[title] >= currentFilter.value && row[title] <= currentFilter.value2
          default:
            break;
        }
      }
      if (typeof row[title] === "string") {
        let reg
        // filter type
        switch (currentFilter.type) {
          case "=":
            return row[title].includes(currentFilter.value)
          case "!=":
            return !row[title].includes(currentFilter.value)
          case "<":
            reg = new RegExp(`^${currentFilter.value}`, "gi")
            return reg.test(row[title]);
          case ">":
            reg = new RegExp(`${currentFilter.value}$`, "gi")
            return reg.test(row[title]);
          case "<=":
            return row[title] == currentFilter.value
          case ">=":
            return row[title] != currentFilter.value
          default:
            break;
        }
      }
    }
    )
    // sort by sorting params
  }).sort((a: any, b: any) => {
    if (sort.asc) {
      return a[sort.col] - b[sort.col]
    } else {
      return b[sort.col] - a[sort.col]
    }
    // map array to rows
  })
}

export const Sorting = (data: any[], sort: any) => {
  if (['boolean'].includes(sort.type)) {
    let sortedRows: any[] = data.sort(function (a, b) { return b[sort.field] - a[sort.field] })
    return sortedRows
  }
  else if (['string', 'large_string'].includes(sort.type)) {
    return data.sort((a: any, b: any) => {
      if (sort.asc) {
        // if (sort.field && a[sort.field]) return  a[sort.field].localeCompare(b[sort.field] ? b[sort.field] : '')
        if (sort.field) return a[sort.field] ? a[sort.field].localeCompare(b[sort.field] ? b[sort.field] : '') : ''.localeCompare(b[sort.field] ? b[sort.field] : '')
        else return a.localeCompare(b)
      } else {
        // if (sort.field) return  b[sort.field].localeCompare(a[sort.field] ? a[sort.field] : '')
        if (sort.field) return b[sort.field] ? b[sort.field].localeCompare(b[sort.field] ? b[sort.field] : '') : ''.localeCompare(a[sort.field] ? a[sort.field] : '')
        else return b.localeCompare(a)
      }
    })
  }
  else if (['number', 'percentage'].includes(sort.type)) {
    return data.sort((a: any, b: any) => {
      if (sort.asc) {
        return a[sort.field] - b[sort.field]
      } else {
        return b[sort.field] - a[sort.field]
      }
    })
  }
  else if (['custom_header'].includes(sort.type)) {
    return data.sort((a: any, b: any) => {
      if (sort.asc) {
        if (sort.field && a[sort.field]) return a[sort.field].localeCompare(b[sort.field] ? b[sort.field] : '')
        else return a.localeCompare(b)
      } else {
        if (sort.field) return b[sort.field].localeCompare(a[sort.field] ? a[sort.field] : '')
        else return b.localeCompare(a)
      }
    })
  }
  else return data

}



import { getCatalogUpdated, getSubGroup, getDivision, getSeries, getSecondCatalog, getGroup, getSuppliers, getSubSuppliers, getBranch, getSegments, getModels, getLayouts, getFullCatalogNoArchive, getAllBranches, getLogo } from './fetchData'
import { setDivisions } from '@src/redux/actions/divisions/divisionsActions'
import { setSeries } from '@src/redux/actions/series/seriesAction'
import { setSubGroups, setGroups } from '@src/redux/actions/groups/groupsAction'
import { setNewSuppliers, setNewSubSuppliers } from '@src/redux/actions/suppliers/supplierAction'
import { setCatalogUpdated, setCatalogFullData, setCatalogFullDataNoArchive } from '@src/redux/actions/catalog/catalogActions'
import { setBranches, setUserBranches } from '@src/redux/actions/userBranches/userBranchesAction'
import { setSegments } from '@src/redux/actions/segments/segmentsActions'
import { setLayouts, setModels, setNewLayouts } from '@src/planogram/shared/store/system/data/data.actions'
import { setLogo } from '@src/redux/actions/logo/logoAction'
import { Flag } from 'react-feather'

export const fetchAndUpdateStore = async (dispatch: any, fetchItem: any, updateAnyway?: boolean) => {
  let isDispatched: any = {}
  for (let i = 0; i < fetchItem.length; i++) {
    if (fetchItem[i].state && !fetchItem[i].state.length || fetchItem[i].state && fetchItem[i].updateAnyway) {
      switch (fetchItem[i].type) {
        case 'catalog': let catalogResponse = await getCatalogUpdated()
          dispatch(setCatalogUpdated(catalogResponse))
          isDispatched = { ...isDispatched, catalog: catalogResponse }
          break;
        case 'catalogFullData':
          let catalogR = await getSecondCatalog()
          dispatch(setCatalogFullData(catalogR))
          isDispatched = { ...isDispatched, catalogFullData: catalogR }
          break;
        case 'catalogFullDataNoArchive':
          let catalogNoArchive = await getFullCatalogNoArchive()
          dispatch(setCatalogFullDataNoArchive(catalogNoArchive))
          isDispatched = { ...isDispatched, catalogFullDataNoArchive: catalogNoArchive }
          break;
        case 'groups': let groupsResponse = await getGroup()
          dispatch(setGroups(groupsResponse))
          isDispatched = { ...isDispatched, groups: groupsResponse }
          break;
        case 'subSuppliers': let subSuppliers = await getSubSuppliers()
          dispatch(setNewSubSuppliers(subSuppliers))
          isDispatched = { ...isDispatched, subSuppliers }
          break;
        case 'suppliers': let suppliersResponse = await getSuppliers()
          dispatch(setNewSuppliers(suppliersResponse))
          isDispatched = { ...isDispatched, suppliers: suppliersResponse }
          break;
        case 'divisions': let divisionResponse = await getDivision()
          dispatch(setDivisions(divisionResponse))
          isDispatched = { ...isDispatched, divisions: divisionResponse }
          break;
        case 'series':
          let seriesResponse = await getSeries()
          dispatch(setSeries(seriesResponse))
          isDispatched = { ...isDispatched, series: seriesResponse }
          break;
        case 'models':
          let modelsResponse = await getModels()
          dispatch(setModels(modelsResponse))
          isDispatched = { ...isDispatched, models: modelsResponse }
          break;
        case 'layouts':
          let layoutsResponse = await getLayouts()
          dispatch(setNewLayouts(layoutsResponse))
          isDispatched = { ...isDispatched, layouts: layoutsResponse }
          break;
        case 'segments':
          let segments = await getSegments()
          dispatch(setSegments(segments))
          isDispatched = { ...isDispatched, segments }
          break;
        case 'subGroups': let subGroupResponse = await getSubGroup()
          dispatch(setSubGroups(subGroupResponse))
          isDispatched = { ...isDispatched, subGroups: subGroupResponse }
          break
        case 'userBranches': let allBranches = await getBranch()
          let saleBranches = allBranches.map((branch: any) => {
            if (branch.BranchType === 0) return branch
            else return null
          }).filter((branch: any) => branch)
          dispatch(setUserBranches(saleBranches))
          isDispatched = { ...isDispatched, userBranches: saleBranches }
          break
        case 'branches': let branches = await getAllBranches()
          dispatch(setBranches(branches))
          isDispatched = { ...isDispatched, branches }
          break
        case 'logo': let logo = await getLogo()
          dispatch(setLogo(logo))
          isDispatched = { ...isDispatched, logo }
          break
        default: return;
      }
    }
  }
  return isDispatched
}

export function isEmpty(obj: any) {
  for (var key in obj) {
    if (obj.hasOwnProperty(key))
      return false;
  }
  return true;
}
