export const ValidatePassword = (value: string) => {
  // var re = /^[a-zA-Z0-9 ]+$/;
  // 8 char
  // one special char
  // one uppercase
  // one lower case
  // one number
  var re = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()-+_=])[A-Za-z\d@$!%*?&]{8,}$/
  return value.length === 0 ? false : re.test(value)
}

export const ValidatePasswordWithErrors = (value: string) => {
  let error:any = null;
  value.length < 8 ? error = 'min_8_chars' :
  !/(?=.*[A-Z])/.test(value) ? error = 'missing_capital_letter' : 
  !/(?=.*[a-z])/.test(value) ? error = 'missing_small_letter' : 
  !/(?=.*\d)/.test(value) ? error = 'missing_digit' : 
  !/(?=.*[!@#$%^&*()-+_=])/.test(value) ? error = "missing_special_char" : null;

  return error
}

export const isNumberOnly = (value: string) => { return Number(value.trim()) }
export const isStringOnly = (value: string) => { return !/\d/.test(value) }