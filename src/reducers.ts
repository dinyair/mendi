import { History } from 'history';
import { connectRouter } from 'connected-react-router';
import { Reducer, combineReducers } from 'redux';
import { default as usereducer } from '@containers/user/reducer';
import {
	storesReducer, branchesReducer, userBranchesReducer, catalogReducer, catalogUpdatedReducer,
	orderReducer, groupsReducer, subgroupsReducer, catalogFullDataReducer, catalogFullDataNoArchiveReducer, divisionsReducer, seriesReducer, segmentsReducer, layoutsReducer, modelsReducer, logoReducer
} from '@containers/user/CompanyReducer';
import calenderReducer from "./redux/reducers/calendar/"
import emailReducer from "./redux/reducers/email/"
import chatReducer from "./redux/reducers/chat/"
import todoReducer from "./redux/reducers/todo/"
import customizer from "./redux/reducers/customizer/"
import navbar from "./redux/reducers/navbar/"
import dataList from "./redux/reducers/data-list/"
import { stockOrders } from '@src/containers/stock/orders/services/reducer';
import { stockOrdersOrder } from '@src/containers/order/services/reducer';
import {  subSuppliersReducer, suppliersReducer } from '@src/containers/settings/suppliers/reducer';
const rootReducer = (history: History<any>) => combineReducers({

	router: connectRouter(history),
	user: usereducer,
	_branches: branchesReducer,
	_userBranches: userBranchesReducer,
	soters: storesReducer,
	_catalog: catalogReducer,
	_catalogFullData: catalogFullDataReducer,
	_catalogFullDataNoArchive: catalogFullDataNoArchiveReducer,
	_catalogUpdated: catalogUpdatedReducer,
	_orderToPlace: orderReducer,
	_groups: groupsReducer,
	_subGroups: subgroupsReducer,
	_divisions: divisionsReducer,
	_segments: segmentsReducer,
	_models: modelsReducer,
	_series: seriesReducer,
	_layouts: layoutsReducer,
	_suppliers: suppliersReducer,
	_subSuppliers: subSuppliersReducer,
	_logo:logoReducer,
	stockOrders,
	stockOrdersOrder,
	calendar: calenderReducer,
	emailApp: emailReducer,
	todoApp: todoReducer,
	chatApp: chatReducer,
	customizer: customizer,
	navbar: navbar,
	dataList: dataList,
});

export default rootReducer
