interface Config {
  readonly ipServer: string
  readonly iconsPath: string
  readonly imagesPath: string
  readonly productsImages: string
  readonly captchaKey : string
}


const ipServer = process.env.NODE_ENV === 'development' ? "http://localhost:5017" : "https://client.algoretail.co.il"

export const config: Config = {
  "ipServer": ipServer,
  "iconsPath": '../../../assets/icons/',
  "imagesPath": '../../../assets/images/',
  "productsImages": process.env.NODE_ENV === 'development' ? 'https://client.algoretail.co.il/media/images/products/' : 'https://grid.algoretail.co.il/media/images/products/',
  "captchaKey" : '6LfdA-kbAAAAAPz4Gwk8t8KdHR0hxgqu-qJP_kJI'
}

