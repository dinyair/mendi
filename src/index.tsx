import * as React from "react"
import { Store } from 'redux';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import { install, applyUpdate } from 'offline-plugin/runtime';
import { App } from '@src/App';

import { RootStore, configureStore, history } from './store';
// import { vuexyStore } from "./redux/storeConfig/store"
// import * as serviceWorker from "./serviceWorker"

import './@fake-db'
import "./index.scss"
import "prismjs/themes/prism-tomorrow.css"
import "@vuexy/rippleButton/RippleButton"
import "react-perfect-scrollbar/dist/css/styles.css"

// import "@ag-grid-enterprise/all-modules/dist/styles/ag-grid.css";
// import "@ag-grid-enterprise/all-modules/dist/styles/ag-theme-alpine.css";
// import "@ag-grid-enterprise/all-modules/dist/styles/ag-theme-material.css";

import {LicenseManager} from "@ag-grid-enterprise/core";
LicenseManager.setLicenseKey("For_Trialing_ag-Grid_Only-Not_For_Real_Development_Or_Production_Projects-Valid_Until-1_May_2021_[v2]_MTYxOTgyMzYwMDAwMA==a717ffda041154ad580159495341d1fd");

export const store: Store<RootStore> = configureStore();

const node: HTMLElement | null = document.getElementById('app') || document.createElement('div');
const renderRoot = (app: JSX.Element): void => render(app, node);
const router = (Application: any): JSX.Element => (
		<Provider  store={store} >
			<ConnectedRouter history={history} >
							<Application />
			</ConnectedRouter>
	</Provider>

);

renderRoot(router(App));

if (module.hot) {
	module.hot.accept();
	renderRoot(router(require('./app').App));
}

if (process.env.NODE_ENV === 'develompent') {
	install({
		onUpdateReady: () => {},
		onUpdated: () => {}
	});
}


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.unregister()
// serviceWorker.register();
