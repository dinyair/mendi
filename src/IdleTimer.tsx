import * as React from 'react';
import { useIdleTimer } from 'react-idle-timer'
import { isAuth, logOut } from './containers/user/initial-state';
import { useHistory } from 'react-router';
import { Routes } from './utilities';
interface Props {
}

export const IdleTimer: React.FunctionComponent = (props: Props) => {
    const timeout = 1000 * 60 * 60 * 2; // (ms * seconds * minutes * hours - Exactly 2 hours)
    const [remaining, setRemaining] = React.useState(timeout);
    const [elapsed, setElapsed] = React.useState(0);
    const [lastActive, setLastActive] = React.useState(+new Date());
    const [isIdle, setIsIdle] = React.useState(true);
    const history = useHistory();
    
    const handleOnActive = () => setIsIdle(false);
    const handleOnIdle = () => setIsIdle(true);
	let URL = window.location.pathname;

    const {
        reset,
        pause,
        resume,
        getRemainingTime,
        getLastActiveTime,
        getElapsedTime
    } = useIdleTimer({
        timeout,
        onActive: handleOnActive,
        onIdle: handleOnIdle
    })

    React.useEffect(() => {
        setRemaining(getRemainingTime())
        setLastActive(getLastActiveTime())
        setElapsed(getElapsedTime())

        setInterval(() => {
            setRemaining(getRemainingTime())
            setLastActive(getLastActiveTime())
            setElapsed(getElapsedTime())
        }, 1000)
    }, [])

    React.useEffect(() => {
		if (!isAuth() && URL !== Routes.LOGIN && URL !== Routes.ADMIN_LOGIN) history.push(Routes.LOGIN)
        if (!remaining) logOut(() => history.push(Routes.LOGIN))
    }, [remaining])

    // React.useEffect(() => {
    //     console.log({ remaining, elapsed, lastActive, isIdle })
    // }, [remaining])

    return (<></>)
};

export default IdleTimer;

