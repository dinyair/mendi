import React from "react"
import { NavLink } from "react-router-dom"
import { config } from "@src/config";
import './SidebarHeader.scss';

const mainLogo = `../../../${config.iconsPath}navbar/main-logo.png`;


const SidebarHeader = (props) => {
	const {  sidebarVisibility } = props
	const closeSideSideBar = () => {
		sidebarVisibility()
		setTimeout(() => {
			sidebarVisibility()
		}, 1);
	}
	return (
		<div className="navbar-header" onClick={closeSideSideBar}>
			<NavLink to="/" className="navbar-brand">
				<div className="main-logo">
					<img src={mainLogo} alt="Algoretail" />
				</div>
			</NavLink>
		</div>
	)
}

export default SidebarHeader
