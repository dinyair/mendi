import { Routes } from "@utilities";

export const isActiveLink = (activePath, navigationItem) => {
	if (activePath !== Routes.HOME) {
		if (activePath === navigationItem.navLink) {
			return true;
		} else if (navigationItem.children) {
			return navigationItem.children.some(({ navLink }) => {
				const regExp = new RegExp(navigationItem.id);
				return navLink === activePath || regExp.test(activePath);
			});
		}

		const regExp = new RegExp(navigationItem.id);
		return regExp.test(activePath);
	} else if (activePath === Routes.HOME && activePath === navigationItem.navLink) {
		return true;

	}

	return false;
};
