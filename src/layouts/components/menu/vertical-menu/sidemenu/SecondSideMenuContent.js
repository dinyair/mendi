import React from 'react'
import { Link } from "react-router-dom";
import classnames from "classnames";
import { Icon } from '@components';
import { navigationConfig } from '../../../../../configs/sub-categories/navigationConfig';
import './SecondSideMenuContent.scss';
import { FormattedMessage } from 'react-intl';
import { isActiveLink } from './utils/isActiveLink';
import { useSelector } from 'react-redux';

const iconStyle = { height: '3.5rem', width: '3rem' };

export const SecondSideMenuContent = ({ activeId, toggleMenu, activePath }) => {
	const user = useSelector((state) => state.user)

	const navTree = navigationConfig();

	const subMenu = navTree.find(({ id }) => id === activeId);

	if (!subMenu || !subMenu.children) {
		return null;
	}

	function navigate(href, newTab) {
		var a = document.createElement('a');
		a.href = href;
		if (newTab) {
			a.setAttribute('target', '_blank');
		}
		a.click();
	}

	return (
		<ul className="sub-menu">
			{subMenu.children.map(item => {
				const { id, title, iconSrc, navLink } = item;
				const userPermissions = user.permissions 
				
				return (
					<li onClick={(e) =>{e.preventDefault(); toggleMenu(true)}} className={classnames('sub-menu__item', { 
						active: isActiveLink(activePath, item) ,
						'opacity-03': userPermissions && userPermissions[item.navLink] && userPermissions[item.navLink].read_flag === 0
						})} key={id}>
						{iconSrc && (<Icon src={iconSrc} style={iconStyle} />)}
						{
							
						(!item.navLink || (userPermissions && userPermissions[item.navLink] && userPermissions[item.navLink].read_flag > 0 )) ? 
						item.newTab ?
							<div className='sub-menu__link' onClick={() => navigate(item.link, true)}><FormattedMessage id={title} /></div>
							:
							<Link id={item.navLink} className='sub-menu__link' to={navLink}><FormattedMessage id={title} /></Link>
							:
							<div className='sub-menu__link'><FormattedMessage id={title} /></div>
						}

					</li>
				)
			})}
		</ul>
	)
}

export default SecondSideMenuContent;
