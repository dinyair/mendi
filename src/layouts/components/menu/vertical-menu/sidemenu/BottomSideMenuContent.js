import React  from 'react'
import { Search as SearchIcon, User, MessageCircle, LogOut} from 'react-feather';
import './BottomSideMenuContent.scss';
import { logOut } from '@containers/user/initial-state';
import { Routes } from '@src/utilities';
import { useHistory } from "react-router-dom";
import { Network, International } from '@src/layouts/components/navbar/NavbarUser'
import { useSelector } from 'react-redux';
import { formatMessage } from 'devextreme/localization';
// import { Search } from './Search'


export const BottomSideMenuContent = () => {
	const history = useHistory();
	const user = useSelector((state) => state.user)

	const bottomItems = [
		{
		id: 'search',
		title: 'Search',
		Icon: SearchIcon,
	}, {
		id: 'profile',
		title: user ? user.name : formatMessage({id: 'logout'}),
		Icon: LogOut,
		onClick: (e,history) => { e.preventDefault(); logOut(()=>history.push(Routes.LOGIN))}
		}
		,{
		id: 'contact-us',
		title: 'Contact Us',
		Icon: MessageCircle,
	}
];

	return <>
	
	<International/>
	<Network/>
	{bottomItems.map(({id, title, Icon, onClick}, index, array) => (
		<li className={`bottom-menu__item overflow-hidden ${!onClick ? 'opacity-03' : ''} ${index === array.length - 1 ? 'pb-0': ''}`} key={id}  onClick={onClick ? (e)=> {onClick(e,history)} : null}>
			<Icon size={18} color='#dadada'/>
			<span>{title}</span>
		</li>
	))}


		{/* <Search  handleAppOverlay={props.handleAppOverlay}/> */}
	</>
}

export default BottomSideMenuContent;
