import packageJson from '../package.json';
global.appVersion = packageJson.version;

export const packageVersion = packageJson.version;
import Cookies from 'universal-cookie';
import { logOut } from '@containers/user/initial-state';
import { Routes } from './utilities';

const cookies = new Cookies();


export const refreshCacheAndReload = () => {
  console.log('Clearing cache and hard reloading...')
  
  if (caches) {
    // Service worker cache should be cleared with caches.delete()
    caches.keys().then(function(names) {
      for (let name of names) caches.delete(name);
    });
  }


  // delete browser cache and hard reload
  logOut(()=>
  window.location.reload(true)
  )

    // history.push(Routes.LOGIN)

}



// Function to check if the local version is smaller than the server version.. 
// export const semverGreaterThan = (versionA, versionB) => {
//   const versionsA = versionA.split(/\./g);

//   const versionsB = versionB.split(/\./g);
//   while (versionsA.length || versionsB.length) {
//     const a = Number(versionsA.shift());

//     const b = Number(versionsB.shift());
//     // eslint-disable-next-line no-continue
//     if (a === b) continue;
//     // eslint-disable-next-line no-restricted-globals
//     return a > b || isNaN(b);
//   }
//   return false;
// };