import React from "react"
import { IntlProvider } from "react-intl"

import messages_heb from "assets/data/locales/heb.json"
import messages_en from "assets/data/locales/en.json"
import messages_ru from "assets/data/locales/ru.json"
import messages_de from "assets/data/locales/de.json"
import messages_fr from "assets/data/locales/fr.json"
import messages_pt from "assets/data/locales/pt.json"

const menu_messages = {
  heb: messages_heb,
  eng: messages_en,
  ru: messages_ru,
  de: messages_de,
  fr: messages_fr,
  pt: messages_pt
}

const Context = React.createContext({state: []})

class IntlProviderWrapper extends React.Component {
  constructor(props) {
    const { user } = props
    super(props)
    this.state = {
      locale: user && user.lang ? user.lang : 'eng',
      messages: user && user.lang ? menu_messages[user.lang] : menu_messages["eng"]
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const { user } = this.props
    if( user !== prevProps.user ) this.setState({locale: user && user.lang ? user.lang : 'eng', messages: user && user.lang ? menu_messages[user.lang] : menu_messages["eng"]})
  }
  render() {
    const { children, } = this.props
    const { locale, messages } = this.state
    return (
      <Context.Provider
        value={{
          state: this.state,
          switchLanguage: language => {
            this.setState({
              locale: language,
              messages: menu_messages[language]
            })
          }
        }}
      >
        <IntlProvider
          key={locale}
          locale={locale}
          messages={messages}
          defaultLocale="heb"
        >
          {children}
        </IntlProvider>
      </Context.Provider>
    )
  }
}

export { IntlProviderWrapper, Context as IntlContext }
