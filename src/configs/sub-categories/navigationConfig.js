import {
	Routes
} from '@utilities';
import {
	config
} from '../../config'

export const TYPES = {
	ITEM: 'ITEM',
	COLLAPSE: 'COLLAPSE',
};


export const navigationConfig = () => {
	return [{
		id: 'dashboard',
		title: 'Dashboard',
		type: 'external-link',
		iconSrc: `../../${config.iconsPath}navbar/side-bar-schora.svg`,
		navLink: Routes.EX_DASHBOARD,
		link:'https://grid.algoretail.co.il/dash',
		newTab: true,

	}, {
		id: 'stock',
		title: 'Stock',
		type: TYPES.COLLAPSE,
		iconSrc: `../../${config.iconsPath}navbar/side-bar-mlai.svg`,
		navLink: Routes.STOCK,
		children: [{
				id: 'orders',
				title: 'Orders',
				type: TYPES.ITEM,
				navLink: Routes.STOCK_ORDERS,
			}, {
				id: 'receive-stock',
				title: 'ReceiveStock',
				navLink: Routes.RECEIVE_STOCK,
				type: TYPES.ITEM,

			},
			{
				id: 'transfers',
				title: 'transferGoods',
				type: TYPES.ITEM,
				navLink: Routes.STOCK_TRANSFERS,
			},
			{
				id: 'receive-transfers',
				title: 'Recieve Transfers',
				type: 'external-link',
				navLink: Routes.EX_STOCK_RECEIVE_TRANSFERS,
				link: 'https://grid.algoretail.co.il/tablet/transfer-receive',
				newTab: true,
			},
			{
				id: 'returns',
				title: 'ReturnGoods',
				type: TYPES.ITEM,
				navLink: Routes.STOCK_RETURNS,
			}, {
				id: 'destruction',
				title: 'destructionGoods',
				type: TYPES.ITEM,
				navLink: Routes.STOCK_DESTRUCTION,
			}, {
				id: 'disassembles',
				title: 'Disassembles',
				type: TYPES.ITEM,
				navLink: Routes.STOCK_DISSAMBLES,
			}
		]
	}, {
		id: 'inventory',
		title: 'Inventory',
		type: TYPES.COLLAPSE,
		iconSrc: `../../${config.iconsPath}navbar/side-bar-planning.svg`,
		navLink: Routes.INVERTORY,
		children: [{
				id: 'InvertoryBalances',
				title: 'invertoryBalances',
				type: TYPES.ITEM,
				navLink: Routes.INVERTORY_BALANCES,
			},
			{
				id: 'invertoryCount',
				title: 'inventoryCount',
				// type: TYPES.ITEM,
				type: 'external-link',
				link: 'https://grid.algoretail.co.il/tablet/manage/stock1',
				newTab: true,
				// navLink: Routes.INVERTORY_COUNT,
			},
			// {
			// 	id: 'invertoryCount',
			// 	title: 'inventoryCount',
			// 	type: TYPES.ITEM,
			// 	navLink: Routes.INVERTORY_COUNT,
			// },
			{
				id: 'import-invertory-excel',
				title: 'Import Invertory Count From Excel',
				type: 'external-link',
				navLink: Routes.EX_INVERTORY_IMPORT_EXCEL,
				link: 'https://grid.algoretail.co.il/stockXls',
				newTab: true,
			},
			{
				id: 'potential',
				title: 'Potential',
				type: TYPES.ITEM,
				navLink: Routes.INVERTORY_POTENTIAL,
			},
			{
				id: 'status',
				title: 'Status',
				type: TYPES.ITEM,
				navLink: Routes.INVERTORY_STATUS,
			},
		]
	}, {
		id: 'branch-planning',
		title: 'Branch Planning',
		type: TYPES.COLLAPSE,
		iconSrc: `../../${config.iconsPath}navbar/side-bar-reports.svg`,
		navLink: Routes.BRANCH_PLANNING,
		children: [
			{
				id: 'planogram',
				title: 'Planogram',
				type: TYPES.ITEM,
				navLink: Routes.PLANOGRAM,
			},
			{
				id: 'supply-days',
				title: 'Supply Days',
				type: TYPES.ITEM,
				navLink: Routes.BRANCH_PLANNING_SUPPLYDAYS,
			},
			{
				id: 'low-sale-block',
				title: 'Low Sale Block',
				type: 'external-link',
				navLink: Routes.EX_BRANCH_PLANNING_POOR_SELLERS_BLOCK,
				link: 'https://grid.algoretail.co.il/lowsale',
				newTab: true,
			},
			{
				id: 'low-sale-exceptions',
				title: 'Low Sale Exceptions',
				type: 'external-link',
				navLink: Routes.EX_BRANCH_PLANNING_POOR_SELLERS_EXCEPTIONS,
				link: 'https://grid.algoretail.co.il/out_lowsale',
				newTab: true,
			},
			{
				id: 'layout-planning',
				title: 'Layout Planning',
				type: TYPES.ITEM,
				navLink: Routes.BRANCH_PLANNING_LAYOUT_PLANNING,
			},
			{
				id: 'branch-variety',
				title: 'Branch Variety',
				type: TYPES.ITEM,
				navLink: Routes.BRANCH_PLANNING_BRANCH_VARIETY,
			}, 
			{
				id: 'variety-recommendation',
				title: 'Variety Recommendation',
				type: 'external-link',
				navLink: Routes.EX_BRANCH_PLANNING_VARIETY_RECCOMENDATION,
				link: 'https://grid.algoretail.co.il/repspt3',
				newTab: true,
			},
			{
				id: 'variety-recommendation-table',
				title: 'Variety Recommendation Table',
				type: 'external-link',
				navLink: Routes.EX_BRANCH_PLANNING_VARIETY_RECCOMENDATION_TABLE,
				link: 'https://grid.algoretail.co.il/migvan',
				newTab: true,
			},
			{
				id: 'division-to-branches',
				title: 'Division To Branches',
				type: 'external-link',
				navLink: Routes.EX_BRANCH_PLANNING_DIVISONS_TO_BRANCHES,
				link: 'https://grid.algoretail.co.il/repspt',
				newTab: true,
			},
		]
	}, {
		id: 'reports',
		title: 'Reports',
		type: TYPES.COLLAPSE,
		iconSrc: `../../${config.iconsPath}navbar/side-bar-catalog.svg`,
		navLink: Routes.REPORTS,
		children: [{
				id: 'items-trends',
				title: 'item tracking report',
				type: TYPES.ITEM,
				navLink: Routes.REPORTS_ITEM_TRENDS,
			}, {
				id: 'orderAnalyze',
				title: 'order-analyze',
				type: TYPES.ITEM,
				navLink: Routes.REPORTS_ORDER_ANALYZA,
			},
			{
				id: 'sales',
				title: 'Sales',
				type: TYPES.ITEM,
				navLink: Routes.REPORTS_SALES,
			}, {
				id: 'hourly-sells',
				title: 'Hourly Sells',
				type: 'external-link',
				navLink: Routes.EX_REPORTS_HOURLY_SALES,
				link: 'https://grid.algoretail.co.il/repdep1',
				newTab: true,
			},
			{
				id: 'waste',
				title: 'Waste',
				type: 'external-link',
				navLink: Routes.REPORTS_WASTE,
				link: 'https://grid.algoretail.co.il/repdep',
				newTab: true,
			},

			{
				id: 'sale-opportunity',
				title: 'Sale Opportunity',
				type: 'external-link',
				navLink: Routes.EX_REPORTS_SELL_OPPORTUNITY,
				link: 'https://grid.algoretail.co.il/osale',
				newTab: true,
			},
			{
				id: 'no-sale-days',
				title: 'No Sale Days',
				type: 'external-link',
				navLink: Routes.EX_REPORTS_NO_SELL_DAYS,
				link: 'https://grid.algoretail.co.il/nosale',
				newTab: true,
			},
		]


	}, {
		id: 'catalogue',
		title: 'Catalogue',
		type: TYPES.COLLAPSE,
		iconSrc: `../../${config.iconsPath}navbar/side-bar-yadion.svg`,
		navLink: Routes.CATALOG,
		children: [{
				id: 'items',
				title: 'Items',
				type: TYPES.ITEM,
				navLink: Routes.CATALOG_ITEMS,
			}, {
				id: 'categories',
				title: 'Categories',
				type: TYPES.ITEM,
				navLink: Routes.CATALOG_CATEGORIES,
			}, {
				id: 'items-settings',
				title: 'Items Settings',
				type: TYPES.ITEM,
				navLink: Routes.CATALOG_ITEMS_SETTINGS,
			},
			{
				id: 'subtitutes',
				title: 'Subtitutes',
				type: 'external-link',
				navLink: Routes.EX_CATALOG_SUBTITUTES,
				link: 'https://grid.algoretail.co.il/subbar',
				newTab: true,
			},
			{
				id: 'suppliers-variety',
				title: 'Supplier’s Variety',
				type: 'external-link',
				navLink: Routes.CATALOG_SUPPLIERS_VARIETY,
				link: 'https://grid.algoretail.co.il/migvansapak',
				newTab: true,
			}
		]
	}, {
		id: 'settings',
		title: 'Settings',
		type: TYPES.COLLAPSE,
		iconSrc: `../../${config.iconsPath}navbar/side-bar-settings.svg`,
		navLink: Routes.SETTINGS,
		children: [{
			id: 'suppliers',
			title: 'Suppliers',
			// type: 'external-link',
			navLink: Routes.SETTINGS_SUPPLIERS,
			// link: 'https://grid.algoretail.co.il/sapakim',
			// newTab: true,
		}, {
			id: 'branches',
			title: 'Branches',
			type: 'external-link',
			navLink: Routes.SETTINGS_BRANCHES,
			link: 'https://grid.algoretail.co.il/branches',
			newTab: true,
		}, {
			id: 'users',
			title: 'Users',
			type: TYPES.ITEM,
			navLink: Routes.SETTINGS_USERS,
		}, {
			id: 'seasons',
			title: 'Seasons',
			type: TYPES.ITEM,
			navLink: Routes.SETTINGS_SEASONS,
		}, {
			id: 'client-settings',
			title: 'Client Settings',
			type: TYPES.ITEM,
			navLink: Routes.SETTINGS_CLIENT_SETTINGS,
		}]

	}]
};
