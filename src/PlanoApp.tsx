import * as React from "react";
import { PlanoRoutes } from './PlanoRoutes'
import { Provider } from 'react-redux';
import { store as storePlano} from '@src/planogram/shared/store';

export const PLANOGRAM_BASE_URL = "/planogram"

interface Props {
}

export const PlanoApp: React.FC<Props> = (props: Props) => {
    return <Provider store={storePlano} >
                <PlanoRoutes/>
             </Provider>
};

export default PlanoApp;

