import * as React from "react";
import { Switch } from 'react-router-dom';
import PlangoramEditor from '@src/planogram/PlanogramEditor';
import Selection from '@src/planogram/Selection';
import { PlanogramDocument } from '@src/planogram/PlanogramDocument';
import { ProtectedRoute } from '@src/planogram/shared/components/AppRoute';
import { PlanogramDimension } from '@src/planogram/PlanogramDimension';
import { PlanogramStockCount } from '@src/planogram/PlanogramStockCount';
import { useSelector, useDispatch } from 'react-redux';
import { AppState } from '@src/planogram/shared/store/app.reducer';
import { isEmpty } from '@src/helpers/helpers'
import { useIntl } from "react-intl"
import SweetAlert from 'react-bootstrap-sweetalert';
import { NEWPLANO_ACTION } from "@src/planogram/shared/store/newPlano/newPlano.types";
// import { ReactReduxContext } from 'react-redux'
import { PLANOGRAM_BASE_URL } from './PlanoApp'
import { AppRoute } from '@src/App'
interface Props {
}

export const PlanoRoutes: React.FC<Props> = (props: Props) => {
    const [sweetAlert, setSweetAlert] = React.useState<boolean>(false);
    const planoAlert = useSelector((state: AppState) => state.alert)

    // const { store } = React.useContext(ReactReduxContext)
    const dispatch = useDispatch();
    const { formatMessage } = useIntl();
    React.useEffect(() => {
        if (!isEmpty(planoAlert)) {
            setSweetAlert(true)
            setTimeout(() => {
                setSweetAlert(false)
                dispatch({ type: NEWPLANO_ACTION.SET_ALERT, payload: {} });
            }, planoAlert.timeMs);
        }
    }, [planoAlert])

    React.useEffect( ()=>{
        return ( ()=>{
            dispatch({
                type: NEWPLANO_ACTION.SET_NEWPLANO,
                payload: null
            });
        });
     }, []);

    return  <>
            <Switch>
            <AppRoute
                fullLayout
                path={[PLANOGRAM_BASE_URL + "/:type/editor/:branch_id/:store_id", PLANOGRAM_BASE_URL + "/:type/view/:branch_id/:store_id", PLANOGRAM_BASE_URL + "/:type/view/:branch_id/:store_id/:aisle_index", PLANOGRAM_BASE_URL + "/:type/view/:branch_id/:store_id/:aisle_index/:second_aisle_index"]}// PLANOGRAM_BASE_URL + "/:type/:PageType/:branch_id/:store_id/ 
                component={PlangoramEditor} />
            <ProtectedRoute
                path={[PLANOGRAM_BASE_URL, PLANOGRAM_BASE_URL + "/:type/:branch_id/:store_id/:aisle_id"]}
                component={Selection} />
            <ProtectedRoute
                level={100}
                path={PLANOGRAM_BASE_URL + "/document/:branch_id/:store_id"}
                component={PlanogramDocument} />
            <ProtectedRoute
                level={100}
                path={PLANOGRAM_BASE_URL + "/dimension"}
                component={PlanogramDimension} />
            <ProtectedRoute
                level={100}
                path={PLANOGRAM_BASE_URL + "/stockcount/:branch_id/:store_id"}
                component={PlanogramStockCount} />
        </Switch>

            {planoAlert &&
                <SweetAlert
                success={planoAlert.type == 'success' ? true : false}
                error={planoAlert.type == 'error' ? true : false}
                title={formatMessage({ id: planoAlert.text ? planoAlert.text : 'no text' })}
                show={sweetAlert}
                closeOnClickOutside
                confirmBtnCssClass='display-hidden'
                onConfirm={() => setSweetAlert(false)}
            ></SweetAlert>}
             </>
};

export default PlanoRoutes;

