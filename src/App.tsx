import * as React from 'react';
import { hot } from 'react-hot-loader/root';
import { useSelector, useDispatch } from 'react-redux';
import * as AuthActions from "./redux/actions/auth/authActions"
import { checkAuth } from '@containers/login/initial-state'
import { RootStore } from '@src/store';
import { checkResetPasswordDiff, isAuth } from '@containers/user/initial-state';
import { useHistory } from "react-router-dom";
import { Route, } from 'react-router-dom';
import { Switch, useLocation } from 'react-router';
import { refreshCacheAndReload, packageVersion } from "./cacheBuster.js"
import 'app.scss';
import "flatpickr/dist/themes/light.css";
import "assets/scss/plugins/forms/flatpickr/flatpickr.scss"
import { store } from "@src/planogram/shared/store";
import { setAuth } from "@src/planogram/shared/store/auth/auth.actions";
import { Routes } from './utilities';
import * as Loadables from './loadables';
import { IntlProviderWrapper } from "./utility/context/Internationalization"
import Cookies from 'universal-cookie';
import { Layout } from "./utility/context/Layout"
import { install } from 'offline-plugin/runtime';
import { ContextLayout } from "@src/utility/context/Layout"
import IdleTimer from './IdleTimer';

const cookies = new Cookies();

export const AppRoute = ({ component: Component, render, fullLayout, ...rest }: any) => {
	const user = useSelector((state: RootStore) => state.user)
	const location = useLocation();



	React.useEffect(() => {
		if (process.env.NODE_ENV === 'production') { // check version statement
			// console.log({ location })
			fetch('/meta.json', { cache: "no-store" })
				.then((response) => response.json())
				.then((meta) => {
					const latestVersion = meta.version;
					const currentVersion = packageVersion;
					const shouldForceRefresh = latestVersion !== currentVersion;
					console.log({ latestVersion, packageVersion })

					if (shouldForceRefresh) {
						console.log(`We have a new version - ${latestVersion}. Should force refresh`);

						install({
							onUpdateReady: () => { },
							onUpdated: () => { }
						});
						refreshCacheAndReload();
					} else {
						console.log(`You already have the latest version - ${latestVersion}. No cache refresh needed.`);
					}
				});
		} else {
			// console.log(`You are on development. No cache refresh needed.`);
		}
	}, [location]);

	return (
		<Route
			{...rest}
			render={render ? render : props => {
				return (
					<ContextLayout.Consumer>
						{context => {

							if (context && context.state) {
								let LayoutTag =
									fullLayout === true
										? context.fullLayout
										: context.state.activeLayout === "horizontal"
											? context.horizontalLayout
											: context.VerticalLayout
								return (
									<LayoutTag {...props} permission={user}>

										<Component {...props} />
									</LayoutTag>
								)
							}
						}}

					</ContextLayout.Consumer>
				)
			}}
		/>

	)
}

export const App = hot(() => {
	const dispatch = useDispatch();
	const user = useSelector((state: RootStore) => state.user)
	const location = useLocation();
	const history = useHistory();
	const userPermissions = user ? user.permissions : null
	let URL = window.location.pathname
	const [isLoading, setIsLoading] = React.useState(true)


	React.useEffect(() => {
		if (!isLoading && isAuth() && !URL.includes(Routes.PLANOGRAM) && URL !== Routes.HOME && URL !== Routes.LOGIN && URL !== Routes.RESET_PASSWORD && (!userPermissions || !userPermissions[URL] || userPermissions && userPermissions[URL] && userPermissions[URL].read_flag === 0)) {
			history.push('/');
		}

		if (isAuth() && URL != Routes.RESET_PASSWORD && checkResetPasswordDiff(user) >= 90) {
			history.push('/login/reset');
		}

		if (isAuth() === undefined && URL !== Routes.LOGIN && URL !== Routes.ADMIN_LOGIN) {
			window.location.replace('/login');
		}
	}, [location])




	
	React.useEffect(() => {
		checkAuth().then((res: any) => {
			if (res.access_token && res.user) {
				const { user, access_token, err } = res
				if (user && access_token) {
					const obj = { ...user }
					const diff = checkResetPasswordDiff(user)
					delete obj.permissions
					const expires = new Date((Date.now() + ((86400 * 1000))))
					cookies.set('token', access_token, { path: '/', expires, secure: true });
					cookies.set('user', JSON.stringify(obj), { path: '/', expires, secure: true });
					dispatch(AuthActions.setUser(user))
					store.dispatch(setAuth(user, access_token));
					setIsLoading(false)
					const userPermissions = user ? user.permissions : null

					if (diff >= 90) {
						history.push('/login/reset');
					}
					if (isAuth() && URL !== Routes.HOME && URL !== Routes.RESET_PASSWORD && (!userPermissions || !userPermissions[URL] || userPermissions && userPermissions[URL] && userPermissions[URL].read_flag === 0 || URL === Routes.LOGIN || URL === Routes.ADMIN_LOGIN)) {
						if (!URL.includes(Routes.PLANOGRAM) || !URL.includes(Routes.MEDIA) ) history.push('/');
					}
				}
			}
			else {
				// console.log(URL)
				if (URL !== Routes.ADMIN_LOGIN && URL !== Routes.LOGIN) {
					window.location.replace('/login');
				}
				dispatch(AuthActions.setUser({}))
			}
		})
	}, []);

	return (
		<Layout user={user}>
			<IntlProviderWrapper user={user}>
				{location.pathname !== Routes.LOGIN && <IdleTimer />}
				<Switch>
					<AppRoute path={[Routes.LOGIN, Routes.ADMIN_LOGIN, Routes.RESET_PASSWORD]} component={Loadables.Login} fullLayout />
					{user && user.lang && (<>
						<AppRoute path={Routes.HOME} exact={true} component={Loadables.Home} />
						<AppRoute path={Routes.PLANOGRAM} component={Loadables.Planogram} />
						<AppRoute path={Routes.REPORTS_ORDER_ANALYZA} component={Loadables.OrderAnalyze} />
						<AppRoute path={Routes.REPORTS_ITEM_TRENDS} component={Loadables.ItemTrends} />
						<AppRoute path={Routes.BRANCH_PLANNING_SUPPLYDAYS} component={Loadables.SupplyDays} />
						<AppRoute path={Routes.CATALOG_ITEMS} component={Loadables.CatalogItems} />
						<AppRoute path={Routes.CATALOG_CATEGORIES} component={Loadables.CatalogCategories} />
						<AppRoute path={Routes.STOCK_ORDERS} exact={true} component={Loadables.StockOrders} />
						<AppRoute path={Routes.STOCK_SIMULATOR} exact={true} component={Loadables.StockSimulator} />
						<AppRoute path={Routes.EDIT_ORDER} exact={true} component={Loadables.EditOrder} />
						<AppRoute path={Routes.SETTINGS_USERS} exact={true} component={Loadables.SettingsUsers} />
						<AppRoute path={Routes.INVERTORY_BALANCES} exact={true} component={Loadables.InvertoryBalance} />
						<AppRoute path={Routes.INVERTORY_COUNT} exact={true} component={Loadables.InvertoryCount} />
						<AppRoute path={Routes.SETTINGS_SUPPLIERS} exact={true} component={Loadables.SettingsSuppliers} />
						<AppRoute path={Routes.RECEIVE_STOCK} exact={true} component={Loadables.RecieveStock} />
						<AppRoute path={Routes.STOCK_TRANSFERS} exact={true} component={Loadables.StockTransfers} />
						<AppRoute path={Routes.STOCK_RETURNS} exact={true} component={Loadables.StockReturns} />
						<AppRoute path={Routes.STOCK_DESTRUCTION} exact={true} component={Loadables.StockDestruction} />
						<AppRoute path={Routes.SETTINGS_CLIENT_SETTINGS} exact={true} component={Loadables.ClientSettings} />
					</>)}
				</Switch>
			</IntlProviderWrapper>
		</Layout>
	);

});
