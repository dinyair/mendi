import { CompanyActionTypes } from '../../../containers/user/enums'

export const setCatalogUpdated = (catalogUpdated: any[]) => {
  return (dispatch: any) => {
    dispatch({
      type: CompanyActionTypes.SET_CATALOGUP,
      payload: catalogUpdated
    });
  }
}
export const setCatalogFullData = (catalog: any[]) => {
  return (dispatch: any) => {
    dispatch({
      type: CompanyActionTypes.SET_CATALOG_FULL_DATA,
      payload: catalog
    });
  }
}
export const setCatalogFullDataNoArchive = (catalog: any[]) => {
  return (dispatch: any) => {
    dispatch({
      type: CompanyActionTypes.SET_CATALOG_FULL_DATA_NO_ARCHIVE,
      payload: catalog
    });
  }
}
