import { CompanyActionTypes } from '../../../containers/user/enums'

export interface Divisions {
	Id:   number;
	Name: string;
	Mhan: number;
}

export const setDivisions = (divisions: Divisions[]) => {
  return (dispatch: any) => {
    dispatch({
      type: CompanyActionTypes.SET__DIVISIONS,
      payload: divisions
    });
  }
}
