import { CompanyActionTypes } from '../../../containers/user/enums'
export interface Groups {
	Id:   number;
	Name: string;
}

export const setGroups = (groups: Groups[]) => {
  return (dispatch: any) => {
    dispatch({
      type: CompanyActionTypes.SET_GROUPS,
      payload: groups
    });
  }
}
export const setSubGroups = (subGroups: any[]) => {
  return (dispatch: any) => {
    dispatch({
      type: CompanyActionTypes.SET_SUBGROUPS,
      payload: subGroups
    });
  }
}