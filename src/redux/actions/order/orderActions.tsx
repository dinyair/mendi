import { CompanyActionTypes } from '../../../containers/user/enums'

export const setOrder:any = (itemToOrder:any[]) => {
  return (dispatch: any) => {
    dispatch({
      type: CompanyActionTypes.SET_ORDER,
      payload: itemToOrder
    });
  }
}