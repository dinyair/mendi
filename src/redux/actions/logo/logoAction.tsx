import { CompanyActionTypes } from '@containers/user/enums';

export const setLogo = (logo: any) => {
  return (dispatch: any) => {
    dispatch({
        type: CompanyActionTypes.SET_LOGO,
        payload: logo
    });
  }
}





