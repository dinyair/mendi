import { CompanyActionTypes } from '../../../containers/user/enums'

export const setSeries = (series: any[]) => {
  return (dispatch: any) => {
    dispatch({
      type: CompanyActionTypes.SET__SERIES,
      payload: series
    });
  }
}
