import { CompanyActionTypes } from '../../../containers/user/enums'

export const setSegments = (segments: any[]) => {
  return (dispatch: any) => {
    dispatch({
      type: CompanyActionTypes.SET_SEGMENTS,
      payload: segments
    });
  }
}
