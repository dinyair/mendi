import { SuppliersActionTypes } from "@src/containers/settings/suppliers/enums";

export const setNewSuppliers = (suppliers: any[]) => {
  return (dispatch: any) => {
    dispatch({
      type: SuppliersActionTypes.SET_SUPPLIERS,
      payload: suppliers
    });
  }
}
export const setNewSubSuppliers = (subSuppliers: any[]) => {
  return (dispatch: any) => {
    dispatch({
      type: SuppliersActionTypes.SET_SUBSUPPLIERS,
      payload: subSuppliers
    });
  }
}