import { CompanyActionTypes } from '@containers/user/enums';

export const setUserBranches = (branches: any) => {
  return (dispatch: any) => {
    dispatch({
        type: CompanyActionTypes.SET__USER__BRANCHES,
        payload: branches
    });
  }
}


export const setBranches = (branches: any) => {
  return (dispatch: any) => {
    dispatch({
        type: CompanyActionTypes.SET_BRANCHES,
        payload: branches
    });
  }
}





