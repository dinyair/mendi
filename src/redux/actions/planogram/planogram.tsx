import { CompanyActionTypes } from '@containers/user/enums';

export const setStores = (stores: any) => {
  return (dispatch: any) => {
    dispatch({
        type: CompanyActionTypes.SET_STORES,
        payload: stores
    });
  }
}



