

import { UserActionTypes } from '@containers/user/enums';

export const  setUser = (user:any) => {
    return (dispatch: any) => {
      dispatch({
        type: UserActionTypes.SET_USER,
        payload: user
      });
    }
  }
  
  
export const  setUserPermissions = (permissions:any) => {
    return (dispatch: any) => {
      dispatch({
        type: UserActionTypes.SET_USER_PERMISSIONS,
        payload: permissions
      });
    }
  }
  
  