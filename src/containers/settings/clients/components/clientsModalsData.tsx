import * as React from 'react';
import { useIntl } from 'react-intl';
import { ModalInterface, ModalTypes } from '@src/components/GenericModals/interfaces';
import { GenericModals } from '@src/components/GenericModals';
import { ClientRow } from '../interfaces';
import InputText from '@src/components/input-text';
import InputNumber from '@src/components/input-number';

interface Props {
	modalType: ModalTypes;
	toggleModal: (type: ModalTypes) => void;
	modalHandlers: any;
	selectedRow: ClientRow;
}

export const ClientsModalsData: React.FC<Props> = props => {
	const { toggleModal, modalHandlers, selectedRow, modalType, } = props;
	const { formatMessage } = useIntl();

	const cleanRow: ClientRow = {
		Name: null, db: null,
		ContactName: null, Phone: null,
		Email: null, Logo: null, AlgoLogo: null,
		DailyFrom: null, DailyTo: null,
		WeeklyFrom: null, WeeklyTo: null,
		RegularFrom: null, RegularTo: null,
		ShortFrom: null, ShortTo: null
	}
	const [newRow, setNewRow] = React.useState<ClientRow>(selectedRow ? selectedRow : cleanRow)

	const fields = [[{
		id: 'Name',
		label: formatMessage({ id: 'Name' }),
		value: newRow.Name,
		type: 'string'
	},
	{
		id: 'db',
		label: formatMessage({ id: 'db' }),
		value: newRow.db,
		type: 'string'
	}],
	[{
		id: 'ContactName',
		label: formatMessage({ id: 'ContactName' }),
		value: newRow.ContactName,
		type: 'string'
	},
	{
		id: 'Email',
		label: formatMessage({ id: 'Email' }),
		value: newRow.Email,
		type: 'string'
	}],
	[{
		id: 'Phone',
		label: formatMessage({ id: 'Phone' }),
		value: newRow.Phone,
		type: 'number'
	}],
	[{
		id: 'Logo',
		label: formatMessage({ id: 'Logo' }),
		value: newRow.Logo,
		type: 'string'
	},{
		id: 'AlgoLogo',
		label: formatMessage({ id: 'AlgoLogo' }),
		value: newRow.AlgoLogo,
		type: 'string'
	}],
	[{
		id: 'DailyFrom',
		label: formatMessage({ id: 'DailyFrom' }),
		value: newRow.DailyFrom,
		type: 'number'
	}, {
		id: 'DailyTo',
		label: formatMessage({ id: 'DailyTo' }),
		value: newRow.DailyTo,
		type: 'number'
	}],
	[{
		id: 'WeeklyFrom',
		label: formatMessage({ id: 'WeeklyFrom' }),
		value: newRow.WeeklyFrom,
		type: 'number'
	}, {
		id: 'WeeklyTo',
		label: formatMessage({ id: 'WeeklyTo' }),
		value: newRow.WeeklyTo,
		type: 'number'
	}],
	[{
		id: 'RegularFrom',
		label: formatMessage({ id: 'RegularFrom' }),
		value: newRow.RegularFrom,
		type: 'number'
	}, {
		id: 'RegularTo',
		label: formatMessage({ id: 'RegularTo' }),
		value: newRow.RegularTo,
		type: 'number'
	}],
	[{
		id: 'ShortFrom',
		label: formatMessage({ id: 'ShortFrom' }),
		value: newRow.ShortFrom,
		type: 'number'
	}, {
		id: 'ShortTo',
		label: formatMessage({ id: 'ShortTo' }),
		value: newRow.ShortTo,
		type: 'number'
	},]]


	const addEditModal: ModalInterface = {
		classNames: 'min-width-40-rem',
		isOpen: modalType === ModalTypes.add || modalType === ModalTypes.edit,
		toggle: () => toggleModal(ModalTypes.none),
		header: "",
		headerClassNames: 'pb-1 pt-2',
		bodyClassNames: 'pt-0 pb-0',
		body: (
			<div>
				<p className="font-medium-5 text-bold-700">{modalType === ModalTypes.add ? formatMessage({ id: 'addClient' }) : formatMessage({ id: 'editClient' })}</p>
				{fields.map((fieldsRow) => {
					return <div className='d-flex flex-row justify-content-between pl-1'>
						{fieldsRow.map((item) => {
							console.log(item)
							return (
								<div className="width-18-rem">
									<label className="font-medium-1 pl-1 pr-1" htmlFor={item.id}>{item.label}</label>
									{item.type === 'string' ?
										<InputText
											id={item.id}
											onChange={(e) => {
												if (e.target.value.trim().length === 0 || e.target.value == '') setNewRow({ ...newRow, [item.id]: null })
												else setNewRow({ ...newRow, [item.id]: e.target.value })
											}}
											value={item.value ? item.value : ""}
											placeholder={item.label}
											inputClassNames="pl-1-5 pr-1-5"
											noMarginBottom
											classNames={'mb-1'}
										/>
										:
										<InputNumber
											id={item.id}
											onChange={(e) => {
												if (e.target.value.trim().length === 0 || e.target.value == '') setNewRow({ ...newRow, [item.id]: null })
												else setNewRow({ ...newRow, [item.id]: e.target.value })
											}}
											value={item.value ? item.value : ""} placeholder={item.label}
											hideFocus={true}
											inputHeight={'2.3rem'}
											classNames="mr-1 mb-1"
										/>}
								</div>
							)
						})}
					</div>
				})}
			</div>
		),
		buttons: [
			{
				color: 'primary',
				onClick: () =>
					modalType === ModalTypes.add ? modalHandlers.add(newRow) : modalHandlers.edit(newRow),
				label: modalType === ModalTypes.add ? formatMessage({ id: 'add' }) : formatMessage({ id: 'Update' }),
				disabled: (!newRow.db || !newRow.Name)
			}
		]
	}


	return <GenericModals key={`modal`} modal={addEditModal} />;
};

export default ClientsModalsData;

// Name:           string;
// db:             string;
// ContactName:    null | string; איש קשר
// Email:          null | string; אימייל
// Phone:          null | number; טלפון
// Logo:           null | string; לוגו
// AlgoLogo:       null | string; לוגו משולב

// DailyFrom:      number | null; יומי מתחת
// DailyTo:        number | null; יומי עד
// WeeklyFrom:     number | null; שבועי מתחת
// WeeklyTo:       number | null; שבועי עד
// RegularFrom:    number | null; רגיל מתחת
// RegularTo:      number | null; רגיל מעל
// ShortFrom:      number | null; תוקף קצר מתחת
// ShortTo:        number | null;  תוקף קצר מעל


