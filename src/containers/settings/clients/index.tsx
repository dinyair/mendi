import * as React from 'react';
import { useIntl } from "react-intl"
import { useSelector, useDispatch } from 'react-redux';
import { AgGrid, Icon } from '@src/components';
import { ModalTypes } from '@src/components/GenericModals/interfaces';
import { Button, Col, Modal, Spinner } from "reactstrap"
import { getClients, insertNewClient } from './initial-state';
import { ClientRow } from './interfaces';
import ClientsModalsData from './components/clientsModalsData';
import { config } from '@src/config';
import ExportExcel from '@src/components/ExportExcel/ExportExcel';


interface Props {
}



export const Clients: React.FC<Props> = (props: Props) => {
	const { formatMessage } = useIntl();

	const [dataIsReady, setDataIsReady] = React.useState<Boolean>(false)
	const [rows, setRows] = React.useState<ClientRow[] | null>()
	const [selectedRow, setSelectedRow] = React.useState<any | null>(null)
	const [modalType, setModalType] = React.useState<ModalTypes>(ModalTypes.none)

	const [fields, setFields] = React.useState<any>([
		{ text: "Name", type: 'string', width: window.innerWidth * 0.1, minWidth: 120 },
		{ text: "db", type: 'string', width: window.innerWidth * 0.1, minWidth: 100 },
		{ text: "ContactName", type: 'string', width: window.innerWidth * 0.1, minWidth: 120 },
		{ text: "Phone", type: 'string', width: window.innerWidth * 0.1, minWidth: 120 },
		{ text: "Email", type: 'string', width: window.innerWidth * 0.1, minWidth: 120 },
		{
			text: "actions", type: 'string', width: window.innerWidth * 0.1, minWidth: 50, noFilter: true,
			cellRenderer: 'IconRender', renderIcons: [
				{ actions: true },
				{
					type: 'edit', title: 'Edit', onClick: (data: any) => {
						setSelectedRow(data)
						toggleModal(ModalTypes.edit)
					}, icon: 'edit.svg'
				},
			],
		},
	])


	React.useEffect(() => {
		fetchData()
	}, [])


	const fetchData = async () => {
		const clients: ClientRow[] = await getClients()
		setRows(clients)
		console.log({clients})
		setDataIsReady(true)
	}


	const toggleModal = (newType: ModalTypes) => {
		if (newType === ModalTypes.none) {
			setSelectedRow(null)
			// setValidationFields([...originalFields])
		}
		setModalType(newType)
	}

	document.title = formatMessage({ id: 'Client Settings' })

    const handleAdd = async (row: any) => {
        let shouldClose = true;
        console.log({row})
        if (shouldClose) toggleModal(ModalTypes.none)
        const res: any = await insertNewClient(row)
        console.log({res})
    }

	const handleEdit = async (row: any) => {
		let shouldClose = true;
		console.log({row})
		if (shouldClose) toggleModal(ModalTypes.none)
	}


	const modalHandlers = {
		add: (fields: any) => handleAdd(fields),
		edit: (fields: any) => handleEdit(fields),
	};

	return (
		<>
			<div className='d-flex-column'>
				<div className='mb-1 font-medium-5 text-bold-700 text-black d-flex'>
					{formatMessage({ id: 'Client Settings' })}
				</div>

				<div className='d-flex flex-row justify-content-between width-55-rem mb-1'>
					<div>
						<Button disabled={dataIsReady ? false : true} onClick={() => { toggleModal(ModalTypes.add) }} className='round width-11-rem height-2-5-rem d-flex align-items-center justify-content-center' color='primary'>+ {formatMessage({ id: 'addClient' })}</Button>
					</div>
					{rows ? 
					<div className="d-flex justify-content-center align-items-center ml-05 cursor-pointer">
					<ExportExcel fields={fields} data={rows} direction={'ltr'} translateHeader={true} filename={formatMessage({ id: 'Client Settings' })}
						icon={<Icon src={config.iconsPath + "table/excel-download.svg"}/>} />
						 </div> : null}
				</div>

			</div>

			<div className='mb-1 d-flex width-55-rem'>
				<AgGrid
					rowBufferAmount={15}
					defaultSortFieldNum={0}
					id={'code'}
					gridHeight={'35rem'}
					translateHeader
					fields={fields}
					groups={[]}
					totalRows={[]}
					rows={rows}
					cellGreenFields={['name']}
					successDark={'name'}
					pagination
					resizable
					displayLoadingScreen={dataIsReady ? false : true}
				/>
			</div>

			{[ModalTypes.edit, ModalTypes.add].includes(modalType) ?
				<ClientsModalsData
					modalHandlers={modalHandlers}
					modalType={modalType}
					selectedRow={selectedRow}
					toggleModal={(type: ModalTypes) => { setSelectedRow(null); toggleModal(type) }}
				/> : null}
		</>
	);
};

export default Clients;
