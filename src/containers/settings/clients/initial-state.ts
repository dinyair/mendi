import { getRequest, postRequest } from '@src/http';
import { ClientRow } from './interfaces';

export const getClients = async () => {
    const resp = await getRequest<any>(`getClients`)
    return resp
}

export const insertNewClient = async (clientData: ClientRow) => {
    const resp = await postRequest<any>(`insertNewClient`, { clientData })
    console.log({resp})
    return resp
}
