export interface ClientRow {
	Name: null | string;
	db:  null |string;
	ContactName: null | string;// איש קשר
	Phone: null | string;// טלפון
	Email: null | string; //אימייל
	Logo: null | string; //לוגו
	AlgoLogo: null | string;// לוגו משולב
	DailyFrom: number | null;// יומי מתחת
	DailyTo: number | null;// יומי עד
	WeeklyFrom: number | null;// שבועי מתחת
	WeeklyTo: number | null;// שבועי עד
	RegularFrom: number | null;// רגיל מתחת
	RegularTo: number | null;// רגיל מעל
	ShortFrom: number | null;// תוקף קצר מתחת
	ShortTo: number | null;//  תוקף קצר מעל
	BranchId_peruk?: null | string;
	VAT?: number | null;
	Daily?: number | null;
	Weekly?: number | null;
	Regular?: number | null;
	Short?: number | null;
}