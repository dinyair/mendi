import { deleteRequest, getRequest, postRequest } from "@src/http";

export const getUsers = () => new Promise((resolve,reject) =>{
	getRequest('manage/users/getUsers')
	.then((res:any) => { 
			resolve(res)
	}).catch(err=> reject(err))
})

export const getRoles = () => new Promise((resolve,reject) =>{
	getRequest('/getRoles')
	.then((res:any) => { 
			resolve(res)
	}).catch(err=> reject(err))
})

export const addRole = (role:string, copyFrom: string) => new Promise((resolve,reject) =>{
	postRequest('/addRole', {role, copyFrom})
	.then((res:any) => { 
			resolve(res)
	}).catch(err=> reject(err))
})


export const deleteRole = (role:string,) => new Promise((resolve,reject) =>{
	console.log(role)
	deleteRequest('/deleteRole/' +role)
	.then((res:any) => { 
			resolve(res)
	}).catch(err=> reject(err))
})


export const getRolePermissions = (role:string) => new Promise((resolve,reject) =>{
	getRequest('/getRolePermissions/' + role)
	.then((res:any) => { 
			resolve(res)
	}).catch(err=> reject(err))
})

export const updateRolePermissions = (role: string, permissions:any) => new Promise((resolve,reject) =>{
	postRequest('/updateRolePermissions/' + role, permissions )
	.then((res:any) => { 
			resolve(res)
	}).catch(err=> reject(err))
})

export const updateUser = (user:any, send_sms:boolean) => new Promise((resolve,reject) =>{
	postRequest('/updateUser/'+ user.id , {user, send_sms})
	.then((res:any) => { 
			resolve(res)
	}).catch(err=> reject(err))
})


export const addUser = (user:any, send_sms: boolean) => new Promise((resolve,reject) =>{
	postRequest('/addUser', {user, send_sms})
	.then((res:any) => { 
			resolve(res)
	}).catch(err=> reject(err))
})


export const deleteUser = (id:number) => new Promise((resolve,reject) =>{
	deleteRequest('/deleteUser/'+ id)
	.then((res:any) => { 
			resolve(res)
	}).catch(err=> reject(err))
})

export const disableUser = (id:number) => new Promise((resolve,reject) =>{
	getRequest('/disableUser/'+ id)
	.then((res:any) => { 
			resolve(res)
	}).catch(err=> reject(err))
})

export const changeUserPassword = (id:number, password: string) => new Promise((resolve,reject) =>{
	postRequest('/updateUserPassword', {id, password})
	.then((res:any) => { 
			resolve(res)
	}).catch(err=> reject(err))
})


// rec1: {id: 247, username: "0523770133", password: "0523770133", level: 40, db: "nkeshet", name: "אירה אגייב",…}
// db: "nkeshet"
// id: 247
// level: 40
// name: "אירה אגייב"
// pass1: null
// password: "0523770133"
// snif: 14
// snif_arr: [14]
// tafkid: "ס. מנהל סניף"
// tel: "0523770133"
// username: "0523770133"

// export const reportsInitialState: any = {};
