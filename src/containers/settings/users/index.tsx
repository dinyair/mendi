import * as React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { RootStore } from '@src/store';
import { TabContent, TabPane } from 'reactstrap';
import { useIntl } from "react-intl";
import { Tabs, Tab, } from "@src/components/tabs";
import { Users } from './Users/Users'
import { Permissions } from './permissions/Permissions'
import { getRoles } from './initial-state'




export const SettingsUsers: React.FC = () => {
	const { formatMessage } = useIntl();
	


	const TABS = {
		USERS: formatMessage({ id: 'users' }),
		PERMISSIONS: formatMessage({ id: 'permissions' })
	};

	const [roles, setRoles] = React.useState<any[]>();
	React.useEffect(()=>{
		if( !roles || !roles.length ) {
			getRoles().then((roles:any)=> { setRoles(roles) } )
		}
	},[])
	const [activeTab, setActiveTab] = React.useState(TABS.USERS);
	document.title = activeTab
	return (<>
			<div className='mb-3'>
				<Tabs value={TABS.USERS} onChangTab={setActiveTab}>
					<Tab caption={TABS.USERS} name={TABS.USERS} />
					<Tab caption={TABS.PERMISSIONS} name={TABS.PERMISSIONS} />
				</Tabs>
			</div>
			<TabContent activeTab={activeTab}>
				<TabPane tabId={TABS.USERS}>
					<Users roles={roles} />
				</TabPane>
				<TabPane tabId={TABS.PERMISSIONS}>
					<Permissions roles={roles} setRoles={setRoles} />
				</TabPane>
			</TabContent>

	</>);
};

export default SettingsUsers;
