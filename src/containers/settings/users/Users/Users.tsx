import * as React from 'react';
import { useIntl, FormattedMessage } from "react-intl"
import { AgGrid } from '@components';
import { Modal, ModalBody, ModalFooter, Spinner } from 'reactstrap'
import { ModalHeader } from '@src/components/modal';

import { getUsers, updateUser, addUser, disableUser } from '../initial-state'
import { DataDrawer } from './mockData';
import { GenericDrawer } from '@src/components/generic-drawer';


import { FilterDrawer } from '@src/components/filter-drawer';

import { fetchAndUpdateStore } from '@src/helpers/helpers';
import { RootStore } from '@src/store';
import { useSelector, useDispatch } from 'react-redux';
import AsyncSelect from "react-select/async";
import { Button } from 'reactstrap'
import { resetWarningCache } from 'prop-types';

interface Props {
	readonly roles: any
}

export const Users: React.FC<Props> = (props: Props) => {
	const { formatMessage } = useIntl();
	const dispatch = useDispatch()
	const user = useSelector((state: RootStore) => state.user)
	const userPermissions = user ? user.permissions : null
	let URL = window.location.pathname
	const userEditFlag = userPermissions && userPermissions[URL] && userPermissions[URL].edit_flag === 1

	const { roles } = props
	const userBranches = useSelector((state: RootStore) => state._userBranches)

	const [loadSpinner, setLoadSpinner] = React.useState<boolean>(false)
	const [rows, setRows] = React.useState<any>()
	const [showDrawer, setShowDrawer] = React.useState<any>(false)
	const [selectedUser, setSelectedUser] = React.useState<any>(null)

	const [isDeleteModalOpen, setDeleteModalOpen] = React.useState(false)
	const [userToDelete, setUserToDelete] = React.useState<any>()
	const [sendSMS, setSendSMS] = React.useState<any>(false)

	const [isDataReady, setIsDataReady] = React.useState<boolean>(false)

	const [dataChange, setDataChange] = React.useState<boolean>(false)

	const [fields, setFields] = React.useState<any>([
		{ text: "username", noFilter: true, type: 'string', width: window.innerWidth * 0.04, minWidth: 20, },
		{ text: "name", noFilter: true, type: 'string', width: window.innerWidth * 0.04, minWidth: 20, },
		{ text: "email", noFilter: true, type: 'string', width: window.innerWidth * 0.04, minWidth: 20, },
		{ text: "phone", noFilter: true, type: 'string', width: window.innerWidth * 0.04, minWidth: 20, },
		{ text: "tafkid", noFilter: true, type: 'string', width: window.innerWidth * 0.04, minWidth: 20, },
		{ text: "role", noFilter: true, type: 'string', width: window.innerWidth * 0.04, minWidth: 20, },
		{ text: "created_at", noFilter: true, type: 'DateTime', width: window.innerWidth * 0.04, minWidth: 20, },
		{ text: "lastLogin", noFilter: true, type: 'DateTime', width: window.innerWidth * 0.04, minWidth: 20, },
		{
			text: "actions", noFilter: true, type: 'string', width: window.innerWidth * 0.04, minWidth: 18,
			cellRenderer: 'IconRender', renderIcons: [
				{ actions: true, onlyViewForDeleted: true },
				{ type: 'edit', title: 'Edit', disabled: 'showEditIcon', onClick: (data: any) => handleActionClick('edit', data), icon: 'edit.svg' },
				userEditFlag ? { type: 'delete', title: 'Delete', disabled: 'showDeleteIcon', onClick: (data: any) => handleActionClick('delete', data), icon: 'trash.svg' } : null,
			],
		},
	])

	React.useEffect(() => {
		setIsDataReady(false)
		getUsers().then((users: any) => {
			setRows(users.map((userOBJ: any) => {
				const term = userOBJ.isActive && userOBJ.id !== user.id && (user.role === 'admin' ? true : (user.role === 'support' && userOBJ.role === 'manager') ? true : !['support', 'manager'].includes(userOBJ.role) ? true : false);
				return {
					...userOBJ, role: userOBJ.role ? userOBJ.role : formatMessage({ id: 'other' }),
					showDeleteIcon: term, showEditIcon: term
				}
			}));
		});
		(async () => {
			await fetchAndUpdateStore(dispatch, [
				{ state: userBranches, type: 'userBranches' }
			])
		})();
		setIsDataReady(true)

	}, [])



	const handleActionClick = async (type: string, data: any) => {
		switch (type) {
			case 'delete':
				setUserToDelete(data.id)
				setDeleteModalOpen(true)
				break;
			case 'edit':
				setShowDrawer(true)
				setSelectedUser(data)
				break;
		}
	}


	const saveSideBarChanges = async (newData: any) => {
		let newUser: any = {
			id: selectedUser ? selectedUser.id : null,
			db: selectedUser ? selectedUser.db : user ? user.db : null,
			username: newData.layouts[0].formGroup[0].fields[0].value,
			name: newData.layouts[0].formGroup[0].fields[1].value,
			email: newData.layouts[0].formGroup[0].fields[2].value,
			tel: newData.layouts[0].formGroup[0].fields[0].value,//newData.layouts[0].formGroup[0].fields[3].value,
			// phone:newData.layouts[0].formGroup[0].fields[0].value,
			role: newData.layouts[1].formGroup[0].fields[0].value.label,
			tafkid: newData.layouts[1].formGroup[0].fields[2].value,
		}


		let branches: any = newData.layouts[1].formGroup[0].fields[1].value.length ? newData.layouts[1].formGroup[0].fields[1].value : newData.layouts[1].formGroup[0].fields[1].defaultValue ? newData.layouts[1].formGroup[0].fields[1].defaultValue : []
		if (branches) newUser.snif_arr = branches.map((o: any) => o.value).toString()

		let password: any = newData.layouts[2].formGroup[0].fields[0].value === newData.layouts[2].formGroup[0].fields[1].value ? newData.layouts[2].formGroup[0].fields[0].value : null
		if (password) newUser.password = password




		if (selectedUser) { // edit
			let newRow: any = await updateUser(newUser, sendSMS)
			setRows([])

			if (newRow) setRows(rows.map((user: any) => {
				if (user.id === selectedUser.id) return { ...newRow, role: newRow.role ? newRow.role : formatMessage({ id: 'other' }), showDeleteIcon: newRow.isActive, showEditIcon: true }
				else return user
			}))
		} else { // add user
			newUser.lang = user.lang
			newUser.createdBy = user.id
			let newRow: any = await addUser(newUser, sendSMS)
			if (newRow) {
				newRow.showEditIcon = newRow.isActive
				setRows([])
				if (newRow) setRows([...rows, { ...newRow, phone: newRow.tel, role: newRow.role ? newRow.role : formatMessage({ id: 'other' }), showDeleteIcon: newRow.isActive, showEditIcon: true }])
			}
		}
		setIsDataReady(true)
	}


	const handleDelete = async () => {
		setDeleteModalOpen(false)
		await disableUser(userToDelete)
		setRows([])
		setRows(rows.map((row: any) => {
			if (row.id === userToDelete.id) return { ...row, showDeleteIcon: 0, showEditIcon: 0, isActive: 0 }
			else return row
		}
		))
		setIsDataReady(true)
	}

	const closeDeleteModal = () => {
		setDeleteModalOpen(false)
		setUserToDelete(null)
	}

	const deleteModals = [{
		headerClasses: 'margin-auto',
		isOpen: isDeleteModalOpen, toggle: closeDeleteModal, header: (<span className="font-weight-bold">{formatMessage({ id: 'warning' })}</span>),
		body: (<><p>{formatMessage({ id: 'delete user warning' })}<br /> {formatMessage({ id: 'irreversible warning' })}  </p> <p>{formatMessage({ id: 'areYouSure' })}</p></>),
		buttons: [
			{ color: 'primary', onClick: closeDeleteModal, label: 'noBack' },
			{ color: 'primary', onClick: handleDelete, label: 'yesDelete', outline: true }
		]
	}]


	const returnModal = (modal: any) => {
		return (<Modal isOpen={modal.isOpen} toggle={modal.toggle} className='modal-dialog-centered modal-md text-center width-10-per min-width-25-rem'>
			<ModalHeader onClose={modal.toggle} classNames={modal.headerClasses}>
				<h5 className="modal-title">{modal.header}</h5>
			</ModalHeader>
			<ModalBody>
				{modal.body}
			</ModalBody>
			<ModalFooter>
				<div className="m-auto">
					{modal.buttons.map((ele: any, index: number) => <Button outline={ele.outline ? true : false} className='round ml-05 mr-05' key={index} color={ele.color} onClick={ele.onClick} disabled={ele.disabled}>{formatMessage({ id: ele.label })}</Button>)}
				</div>
			</ModalFooter>
		</Modal>
		)
	}
	// console.log(rows ? Array.from(new Set(rows.map((row: any) => row.role ? row.role : formatMessage({ id: 'other' })))) : [])
	return (<>
		{
			loadSpinner
				?
				<Spinner />
				:
				<>
					<div className="mr-5 mt-3" style={{ width: '85%' }}>

						<AgGrid
							onRowDoubleClick={(data: any) => {
								setSelectedUser(data)
								setTimeout(() => {
									setShowDrawer(true);
								}, 1);
							}
							}
							resizable
							pagination
							descSort
							defaultSortFieldNum={0}
							deletedFlag={'isDeleted'}
							id={'username'}
							gridHeight={'66vh'}
							translateHeader
							fields={fields}
							groups={[]}
							totalRows={[]}
							rows={rows}
							checkboxFirstColumn={false}
							freeSearch={{ rows: rows, fieldsToSearch: ['name', 'phone'], title: formatMessage({ id: 'users__freeSearchMessage' }) }}
							checkboxFilterBox={{
								rows: rows, title: formatMessage({ id: 'choose user type' }),
								options: rows ? Array.from(new Set(rows.map((row: any) => row.role ? row.role : formatMessage({ id: 'other' })))) : [],
								flag: 'role'
							}}
							buttonOnClick={userEditFlag ? { title: 'user', function: () => { setSelectedUser(null); setShowDrawer(true); }, disabled: userBranches && userBranches.length && roles && roles.length && rows && rows.length ? false : true } : undefined}
							displayLoadingScreen={!isDataReady}
						/>



					</div>
					{showDrawer && (
						<GenericDrawer {...{
							drawerType: 'ACCORDION',
							modalBeforeClose: true,
							accordion: true,
							hideZeroFilters: true,
							caption: selectedUser ? `${formatMessage({ id: 'user' })}: ${selectedUser.name}` : formatMessage({ id: 'create new user' }),
							shown: showDrawer,
							edit: selectedUser ? true : false,
							required: !selectedUser ? ['username', 'name', 'user type', 'password', 'confirm password'] : ['username', 'name'],
							toggleModal: () => { setShowDrawer(!showDrawer) },
							data: DataDrawer(selectedUser, { roles, userBranches, setSendSMS }, userEditFlag, user),
							didDateChange: setDataChange,
							saveDisabled: !userEditFlag,
							saveChanges: dataChange ? saveSideBarChanges : () => { },
							alignButtonsLeft: true,
						}} />)}
				</>
		}

		{isDeleteModalOpen && returnModal(deleteModals[0])}
	</>)
}
export default Users