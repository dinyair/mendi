import { InputTypes } from "@src/components/form-group";
import { useIntl } from "react-intl"
import { FilterDrawerData } from "@src/components/filter-drawer";
import { ValidatePassword, isStringOnly, isNumberOnly } from '@src/helpers/password-validation'

export const DataDrawer = (data: any, h: any, userEditFlag: boolean, user:any) => {
	const { formatMessage } = useIntl();

	let filterDataDrawers: FilterDrawerData = {
		layouts: [{
			title: formatMessage({ id: 'personal information' }),
			layoutId: '1',
			formGroup: [{
				formId: '1',
				fields: [{
					disabled: !userEditFlag ||data ? true : false,
					label: formatMessage({ id: 'phone' }),
					value: data && data.username ? data.username : '',
					name: 'username',
					type: InputTypes.TEXT,
					alllowsCharacters: [isNumberOnly],
					columns: 2
				}, {
					disabled: !userEditFlag,
					label: formatMessage({ id: 'name' }),
					value: data && data.name ? data.name : '',
					name: 'name',
					type: InputTypes.TEXT,
					alllowsCharacters: [isStringOnly],
					columns: 2
				}, {
					disabled: !userEditFlag,
					label: formatMessage({ id: 'email' }),
					value: data && data.email ? data.email : '',
					name: 'email',
					type: InputTypes.TEXT,
					columns: 2
				},
					// {
					// 	label: formatMessage({ id: 'phone' }),
					// 	value: data ? data.phone ? data.phone : data.tel ? data.tel : '' : '',
					// 	name: 'phone',
					// 	type: InputTypes.TEXT,
					// 	columns: 2
					// }
				]
			}]
		},
		{
			title: formatMessage({ id: 'role settings' }),
			layoutId: '2',
			formGroup: [{
				formId: '1',
				fields: [{
					disabled: !userEditFlag,
					label: formatMessage({ id: 'user type' }),
					value: data && data.role ? data.role : '',
					name: 'user type',
					type: InputTypes.SELECT,
					options: h.roles && h.roles.length ?  (user.role === 'admin' ? h.roles : h.roles.filter((role:any) => role.role != 'support')).map((role: any) => { return { label: role.role, value: role.Id } }) : [],
					columns: 2
				}, {
					disabled: !userEditFlag,
					isMulti: true,
					label: formatMessage({ id: 'branches' }),
					value: [],
					defaultValue: data && data.branches && data.branches.length ? data.branches : '',
					name: 'branches',
					type: InputTypes.SELECT,
					options: h.userBranches && h.userBranches.length ? h.userBranches.map((branch: any) => { return { label: branch.Name, value: branch.BranchId } }) : [],
					columns: 2
				},
				{
					disabled: !userEditFlag,
					label: formatMessage({ id: 'role' }),
					value: data && data.tafkid ? data.tafkid : '',
					name: 'role',
					type: InputTypes.TEXT,
					alllowsCharacters: [isStringOnly],
					columns: 2
				}]
			}]
		}, {
			title: formatMessage({ id: 'password and system preferences' }),
			layoutId: '3',
			description: `<div>
							<p>				
								<b>${formatMessage({id: 'at_least_eight_digits'})}</b>
								<span>${formatMessage({id: 'in_addition_it_must_contain_one'})}</span>
								<span>${formatMessage({id: 'of_all_the_following_character_types'})}</span>:
							</p>

							<ul>
								<li>
									${formatMessage({id: 'capital_letters'})}
									<b>(${formatMessage({id: 'capital_letters_examples'})}...)</b>
								</li>
								<li>
									${formatMessage({id: 'small_letters'})}
									<b>(${formatMessage({id: 'small_letters_examples'})}...)</b>
								</li>
								<li>
									${formatMessage({id: 'digits'})}
									<b>(${formatMessage({id: 'digits_examples'})}...)</b>
								</li>
								<li>
									${formatMessage({id: 'special_symbols'})}
									<b>(${formatMessage({id: 'special_symbols_examples'})}...)</b>
								</li>
							</ul>
						  </div>`,
			formGroup: [{
				formId: '1',
				fields: [{
					disabled: !userEditFlag,
					label: formatMessage({ id: 'new password' }),
					value: '',
					name: 'password',
					type: InputTypes.PASSWORD,
					columns: 4,
					validations: [ValidatePassword],
				}, {
					disabled: !userEditFlag,
					label: formatMessage({ id: 'confirm password' }),
					value: '',
					name: 'confirm password',
					type: InputTypes.PASSWORD,
					columns: 4,
					actionButton: { buttonText: 'send-password-sms',
					onClick: (_callback: any) => {
						if (h.setSendSMS) h.setSendSMS(true)
						if (_callback) { _callback() } } },
				}]
			}]


		}]
	}
	return filterDataDrawers
}






