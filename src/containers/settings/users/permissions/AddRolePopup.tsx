
import * as React from 'react';
import { Icon } from '@components';
import { config } from '@src/config'
import { useIntl, FormattedMessage } from "react-intl"
import { components } from "react-select";
import Select from "react-select"
import makeAnimated from "react-select/animated";
import * as moment from 'moment';
import classnames from 'classnames';
import {
  Button,
  Modal,
  ModalHeader,
  Spinner,
  ModalBody,
  ModalFooter,
} from "reactstrap"



interface Props {
  readonly isOpen: boolean;
  readonly toogle: (b: boolean) => void;
  readonly action: (role: string, copyFrom: string) => void;
  readonly setRoles: (roles: any[]) => void;
  readonly roles: any
}





export const AddRolePopup: React.FunctionComponent<Readonly<Props>> = (props: Readonly<Props>) => {
  const { formatMessage } = useIntl();
  const { isOpen, toogle, roles, setRoles, action } = props
	const customizer = { direction: document.getElementsByTagName("html")[0].dir }
  const [role, setRole] = React.useState<any>()
  const [copyFrom, setCopyFrom] = React.useState<any>()
  const [loading, setLoading] = React.useState<boolean>(false);
  const [inputValue, setInputValue] = React.useState<any[]>(Array.from({ length: 10 }));


  const DropdownIndicator = (props: any) => {
    return (
      <components.DropdownIndicator {...props}>
        {/* <Icon src={'../'+config.iconsPath + "table/date.svg"} /> */}
      </components.DropdownIndicator>
    );
  };



  const MultiValue = (props: any) => (
    <components.MultiValue {...props}>
      <span>{props.data.label}</span>
    </components.MultiValue>
  );
  const animatedComponents = makeAnimated();


  const selectAllOption = {
    value: "<SELECT_ALL>",
    label: formatMessage({ id: "select_all" })
  };


  return (
    <Modal
      isOpen={isOpen}
      toggle={() => toogle(false)}
      backdrop={'static'}
      className="modal-dialog-centered modal-sm">
      {/*
                    <ModalHeader toggle={()=>toogle(false)}>
                    </ModalHeader> 
                  */}
      <ModalBody>
        <div className="my-1 d-flex">
          <div className="cursor-pointer" onClick={() => { setRole(null); setCopyFrom(null); toogle(false) }}>
            <Icon src={'../' + config.iconsPath + "table/arrow-right.svg"}
              				className={classnames("mr-05", {
                        "rotate-180": customizer.direction == 'ltr',
                      })}
               style={{ height: '0.5rem', width: '0.2rem' }} />
          </div>
          <div className="font-medium-3 text-bold-600">
            <FormattedMessage id="create new role" />
          </div>
        </div>
        <div className="d-flex flex-column  align-items-center justify-content-center flex-wrap justify-content-center">
          {[{
            text: 'role',
            placeholder: formatMessage({ id: 'role' }),
            value: role,
            setValue: setRole
          }].map((option: any, oi: number) => (
            <div key={oi} className="position-relative mb-05  AddRolePopup" >
              <input className="select__control pl-05 width-20-rem" type="text" value={option.value} onChange={(e: any) => option.setValue(e.target.value)}
                placeholder={option.placeholder} style={{
                  borderRadius: '2vh!important', maxHeight: '1.5rem', alignItems: 'baseline',
                  height: '1.5rem', minHeight: 38
                }} />
            </div>
          ))}
          {[{
            isMulti: false,
            allowSelectAll: false,
            text: 'copy from',
            value: copyFrom,
            setValue: setCopyFrom,
            options: roles && roles[0] ? roles.map((option: any, index: number) => {
              return { index, value: option.Id, label: option.role }
            }) : []
          },
          ].map((s: any, si: number) => {
            return (
              <div key={si} className="position-relative mb-05  AddRolePopup" >
                {/* <div><FormattedMessage id={s.text}/>:</div> */}
                <Select
                  isMulti={s.isMulti}
                  // inputValue={inputValue[si] ? inputValue[si].value : ''}
                  onInputChange={(query, { action }) => {
                    // Prevents resetting our input after option has been selected
                    if (action !== "set-value") setInputValue(inputValue.map((iv: any, i: number) => {
                      if (i === si) return {
                        ...iv,
                        value: query
                      }
                    }));
                  }
                  }
                  allowSelectAll={s.allowSelectAll}
                  closeMenuOnSelect={s.isMulti ? false : true}
                  hideSelectedOptions={false}
                  blurInputOnSelect={false}
                  components={s.isMulti ? { Option, MultiValue, animatedComponents } : { MultiValue, DropdownIndicator, animatedComponents }}
                  name={s.text}
                  placeholder={formatMessage({ id: s.text })}
                  styles={{
                    indicatorsContainer: (prevStyle, state) => false ? ({
                      ...prevStyle,
                      display: 'none'
                    }) : {},
                    valueContainer: (base) => ({
                      ...base,
                      maxHeight: '1.5rem',
                      height: '1.5rem',
                      position: 'initial'
                    }),

                    control: styles => ({
                      ...styles, width: '19rem!important', borderRadius: '2vh!important', maxHeight: '1.5rem', alignItems: 'baseline',
                      height: '1.5rem'
                    })
                  }}
                  className="ml-auto mr-2"
                  classNamePrefix='select'
                  onChange={(e: any, actionMeta: any) => {
                    if (e) {
                      const { action, option, removedValue } = actionMeta;
                      if (s.isMulti) {
                        const { value, index } = e
                        if (action === "select-option" && option.value === selectAllOption.value) {
                          s.setValue([selectAllOption, ...s.options])
                        } else if (
                          (action === "deselect-option" &&
                            option.value === selectAllOption.value) ||
                          (action === "remove-value" &&
                            removedValue.value === selectAllOption.value)
                        ) {
                          s.setValue([])
                        } else if (
                          actionMeta.action === "deselect-option" &&
                          e.length === s.options.length
                        ) {
                          s.setValue(s.options.filter(({ value }) => value !== option.value));
                        } else {
                          s.setValue(e)
                        }
                      } else {
                        if (e === s.value) s.setValue(null);
                        else {
                          s.setValue(e)
                        }
                      }
                    } else s.setValue(s.isMulti ? [] : null)
                  }}
                  value={s.value}
                  options={s.allowSelectAll ? [selectAllOption, ...s.options] : s.options}
                />
              </div>
            )
          })
          }


        </div>
      </ModalBody>
      <ModalFooter className="justify-content-end no-border">
        {loading && (<Spinner />)}
        <Button
          disabled={loading || !role || !role.length}
          color="primary"
          className="round text-bold-400 d-flex justify-content-center align-items-center width-7-rem cursor-pointer btn-primary"
          onClick={async () => {
            setLoading(true)
            let newRole: any = await action(role, copyFrom && copyFrom.label ? copyFrom.label : null)
            setLoading(false);
            if (newRole) setRole(null); setCopyFrom(null); setRoles([...roles, ...newRole]); toogle(false)
          }}
        >
          <FormattedMessage id="add" />
        </Button>
      </ModalFooter>
    </Modal>
  )
}