import * as React from 'react';
import { useIntl, FormattedMessage } from "react-intl"
import { Spinner } from 'reactstrap'
import { ChevronLeft } from 'react-feather';
import classnames from 'classnames'
import { GroupItem } from '@src/planogram/DataReports/setting-supplier-areas/GroupItem'
import { navigationConfig } from '@src/configs/sub-categories/navigationConfig';
import { addRole, deleteRole, getRolePermissions, updateRolePermissions } from '../initial-state'
import AsyncSelect from "react-select/async";
import { Button } from 'reactstrap'
import { AddRolePopup } from './AddRolePopup'
import { RootStore } from '@src/store';
import { useSelector } from 'react-redux';
import { Icon } from '@src/components';
import { config } from '@src/config';
import { ModalTypes } from '@src/components/GenericModals/interfaces';
import AddRoleModalData from './addRoleModalData';
import DeleteRoleModalData from './deleteRoleModalData';


const navTree = navigationConfig().filter((nt: any) => nt.id !== 'dashboDashboardard');

interface Props {
	readonly roles: any
	readonly setRoles: (roles: any[]) => void
}


export const Permissions: React.FC<Props> = (props: Props) => {
	const { formatMessage } = useIntl();
	const user = useSelector((state: RootStore) => state.user)
	const userPermissions = user ? user.permissions : null
	let URL = window.location.pathname
	const userEditFlag = userPermissions && userPermissions[URL] && userPermissions[URL].edit_flag === 1
	const [isAbleToEdit, setIsAbleToEdit ] = React.useState<boolean>(true);
	const { roles, setRoles } = props
	const [loadSpinner, setLoadSpinner] = React.useState<boolean>(false)
	const [role, setRole] = React.useState<any>(null);
	const [rolePermissions, setRolePermissions] = React.useState<any>(null);
	// const [isAddRolePopupOpen, setIsAddRolePopupOpen] = React.useState<any>();
	let isSuperiorRole: any = ['admin', 'support', 'manager'].includes(user.role)



	const [modalType, setModalType] = React.useState<ModalTypes>(ModalTypes.none)



	React.useEffect(() => {
		if (role) {
			getRolePermissions(role.label).then((rolePermissions: any) => {
				console.log(role.created_by_admin)
				console.log(user.role)
				console.log('condition : ',role.created_by_admin === 1 && user.role !== 'admin')
				if(role.created_by_admin === 1 && user.role !== 'admin') setIsAbleToEdit(false)
				else setIsAbleToEdit(true)

				let newRolePermissions: any = {};
				rolePermissions.forEach((rp: any) => { newRolePermissions[rp.action] = rp })
				setRolePermissions(newRolePermissions)
			}
			)
		}
	}, [role])





	const handleDeleteRole = async () => {
		toggleModal(ModalTypes.none)

		await deleteRole(role.role).then((res) => {
			setRolePermissions(null)
			const rolesCopy = [...roles].filter((ele: any) => {
				return ele.role !== role.role
			})

			setRole(null)
			setRoles([])
			setTimeout(() => {
				setRoles(rolesCopy)
			}, 1);
		})
	}

	const Rows = (childRows: any[]) => {

		return (
			<div
				className={classnames("align-items-center d-flex-column", {
					'max-height-40vh overflow-y-scroll overflow-x-hidden': childRows && childRows[0] && childRows[0].children
				})}>
				{
					childRows && childRows.length ? (
						childRows.map((row: any, index: number) => {
							return (<>
								<GroupItem
									checkbox
									disabled={!userEditFlag || !isAbleToEdit}
									data={rolePermissions}
									updateData={(data: any) => { setRolePermissions(data) }}
									onCheckboxChanged={updateRolePermissions}
									dontAllowClick={false}
									hideArrow={!row.children || !row.children[0]}
									className='m-0'
									childNum={childRows && childRows[1] && childRows[1].children ? 0 : 1}
									row={row.children}
									rows={childRows}
									mainRow={row}
									zebraBg={index % 2 === 0 ? true : false}
									buttonClasses={'width-100-per'}
									buttonDivClass={'width-100-per justify-content-end'}
								>
									{/* {JSON.stringify(row)} */}
									{row.children && row.children[0]
										?
										Rows(row.children)
										:
										null
									}
								</GroupItem>
							</>)
						}
						)) : null

				}
			</div>

		)
	}

	const addRoleModalHandlers = {
		'action': async (role: string, copyFrom: string) => addRole(role, copyFrom),
		'setRoles': (data: any) => { console.log(data); setRoles(data) }
	};


	const toggleModal = (newType: ModalTypes) => {
		setModalType(newType)
	}


	return (<>
		{loadSpinner ? <Spinner /> : null}
		<>
			<div className="mt-2 ml-05 width-50-per min-width-40-rem d-flex align-items-center">
				<AsyncSelect
					id="role"
					onChange={(option: any) => { if (option !== role) setRole(option); setRolePermissions(null) }}
					components={{ IndicatorsContainer: (props: any) => <div {...props} /> }}
					className='width-12-rem height-2-5-rem'
					value={role}
					placeholder={formatMessage({ id: 'choose user type' })}
					defaultOptions={roles?.filter((role:any) => role.role !== 'admin').map((role: any) => {
						return {
							...role, value: role.Id, label: role.role
						}
					})}
					styles={{
						control: provided => ({
							...provided,
							paddingRight: 5,

						})
					}}
					theme={theme => ({
						...theme,
						borderRadius: 20,
						spacing: {
							baseUnit: 3.5,
							controlHeight: 30,
							menuGutter: 8,
						},
						colors: {
							...theme.colors,
							primary: '#31baab'
						}
					})}
				/>

				{userEditFlag && (user.role === 'manager' && role && role.netw !== 'Admin' || user.role === 'admin' && role && role.label !== 'admin') ?
					<div onClick={() => toggleModal(ModalTypes.delete)}>
						<Icon src={`${config.iconsPath}/planogram/trash.svg`}
							className="ml-05 cursor-pointer" style={{ height: '0.5rem', width: '0.2rem' }} />
					</div> : null}

				{isSuperiorRole ?
					<Button
						onClick={() => toggleModal(ModalTypes.add)}
						color="primary"
						className="width-10-rem ml-auto height-2-5-rem round text-bold-400 d-flex justify-content-center align-items-center cursor-pointer btn-primary"
					>
						+ &nbsp;<FormattedMessage id="user type" />
					</Button> : null}



			</div>




			{role && rolePermissions && (
				<div className="mt-2 ml-05 width-50-per border-light-gray min-width-40-rem max-height-70-vh overflow-x-hidden">
					<div className='groupItem border-bottom-light-gray m-0'>
						<button className='groupItem__button d-flex align-items-center width-100-per'>
							<span className='groupItem__button_icon ml-05 text-bold-700'>מסך</span>
							<div className='d-flex justify-content-evenly min-width-37-rem width-100-per'>
								<div className='align-self-center width-15-rem' />
								<div className='d-flex justify-content-between width-100-per'>
									<span className='align-self-center width-15-rem elipsis align-left'></span>
									<div className='d-flex justify-content-evenly position-relative width-35vw width-100-per justify-content-end'>
										{[formatMessage({ id: 'read' }), formatMessage({ id: 'edit' }),].map((item: any, index: number) => {
											return (
												<div className="d-flex" key={index}>
													<span className='border-light-table height-70-per m-auto'></span>
													<div
														className={classnames("mr-2 height-2-2-rem width-5-rem d-flex justify-content-center  position-relative font-small-3 cursor-pointer py-05 border-2 text-black text-bold-700 border-radius-2rem", {
														})}>
														{item}
													</div>
												</div>
											)
										})}
									</div>
								</div>

							</div>
						</button>
					</div>

					{Rows(navTree)}

				</div>
			)}

			{modalType === ModalTypes.add ?
				<AddRoleModalData
					modalHandlers={addRoleModalHandlers}
					modalType={modalType}
					roles={roles}
					toggleModal={(type: ModalTypes) => toggleModal(type)} /> 
			: 
			modalType === ModalTypes.delete ?
					<DeleteRoleModalData modalHandlers={handleDeleteRole} role={role.role} modalType={modalType} toggleModal={(type: ModalTypes) => toggleModal(type)} /> 
			: null}
		</>
	</>)
}
export default Permissions
