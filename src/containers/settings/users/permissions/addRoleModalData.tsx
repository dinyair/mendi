import * as React from 'react';

import { GenericModals } from '@src/components/GenericModals/index';
import { ModalInterface, ModalsInterface, ModalTypes } from '@src/components/GenericModals/interfaces';

import { Icon } from '@components';
import { config } from '@src/config'
import { useIntl, FormattedMessage } from "react-intl"
import { components } from "react-select";
import Select from "react-select"
import makeAnimated from "react-select/animated";
import * as moment from 'moment';
import { Spinner } from 'reactstrap';


interface Props {
    modalType: ModalTypes;
    toggleModal: (type: ModalTypes) => void;
    modalHandlers: any;
    readonly roles: any;
}

export const AddRoleModalData: React.FC<Props> = props => {

    const {
        toggleModal,
        modalHandlers,
        modalType,
        roles
    } = props;

    const { formatMessage } = useIntl();

    const [role, setRole] = React.useState<any>()
    const [copyFrom, setCopyFrom] = React.useState<any>()
    const [loading, setLoading] = React.useState<boolean>(false);
    const [inputValue, setInputValue] = React.useState<any[]>(Array.from({ length: 10 }));


    const DropdownIndicator = (props: any) => {
        return (
            <components.DropdownIndicator {...props}>
                {/* <Icon src={'../'+config.iconsPath + "table/date.svg"} /> */}
            </components.DropdownIndicator>
        );
    };

    const MultiValue = (props: any) => (
        <components.MultiValue {...props}>
            <span>{props.data.label}</span>
        </components.MultiValue>
    );
    const animatedComponents = makeAnimated();


    const selectAllOption = {
        value: "<SELECT_ALL>",
        label: formatMessage({ id: "select_all" })
    };


    const addModal: ModalInterface = {
            classNames: 'modal-dialog-centered modal-sm',
            isOpen: modalType === ModalTypes.add,
            toggle: () => toggleModal(ModalTypes.none),
            header: (<div className="my-1 d-flex">
                {/* <div className="cursor-pointer" onClick={() => { setRole(null); setCopyFrom(null); toggleModal(ModalTypes.none) }}>
                    <Icon src={'../' + config.iconsPath + "table/arrow-right.svg"}
                        className="mr-05" style={{ height: '0.5rem', width: '0.2rem' }} />
                </div> */}
                <div className="font-medium-4 text-bold-600 ml-1-5">
                    <FormattedMessage id="create user" />
                </div>
            </div>),
            body: (
                <>
                    <div className="d-flex align-items-center justify-content-center">
                        <div>
                        {[{
                            text: 'role',
                            placeholder: formatMessage({ id: 'user type' }),
                            value: role,
                            setValue: setRole
                        }].map((option: any, oi: number) => (
                            <div key={oi} className="position-relative ml-1 mr-05 mb-05  AddRolePopup" >
                                <input className="select__control pl-05 width-12-rem" type="text" value={option.value} onChange={(e: any) => option.setValue(e.target.value)}
                                    placeholder={option.placeholder} style={{
                                        borderRadius: '2vh!important', maxHeight: '1.5rem', alignItems: 'baseline',
                                        height: '1.5rem', minHeight: 38
                                    }} />
                            </div>
                        ))}
                        </div>
                        
                        <div>
                        {[{
                            isClearable: true,
                            isMulti: false,
                            allowSelectAll: false,
                            text: 'copy permissions from',
                            value: copyFrom,
                            setValue: setCopyFrom,
                            options: roles && roles[0] ? roles.map((option: any, index: number) => {
                                return { index, value: option.Id, label: option.role }
                            }) : []
                        },
                        ].map((s: any, si: number) => {
                            return (
                                <div key={si} className="position-relative mb-05   AddRolePopup" >
                                    {/* <div><FormattedMessage id={s.text}/>:</div> */}
                                    <Select 
                                        isMulti={s.isMulti}
                                        isClearable={s.isClearable}
                                        // inputValue={inputValue[si] ? inputValue[si].value : ''}
                                        onInputChange={(query, { action }) => {
                                            // Prevents resetting our input after option has been selected
                                            if (action !== "set-value") setInputValue(inputValue.map((iv: any, i: number) => {
                                                if (i === si) return {
                                                    ...iv,
                                                    value: query
                                                }
                                            }));
                                        }
                                        }
                                        allowSelectAll={s.allowSelectAll}
                                        closeMenuOnSelect={s.isMulti ? false : true}
                                        hideSelectedOptions={false}
                                        blurInputOnSelect={false}
                                        components={s.isMulti ? { Option, MultiValue, animatedComponents } : { MultiValue, DropdownIndicator, animatedComponents }}
                                        name={s.text}
                                        placeholder={formatMessage({ id: s.text })}
                                        styles={{
                                            indicatorsContainer: (prevStyle, state) => false ? ({
                                                ...prevStyle,
                                                display: 'none'
                                            }) : {},
                                            valueContainer: (base) => ({
                                                ...base,
                                                maxHeight: '1.5rem',
                                                height: '1.5rem',
                                                width:'5rem',
                                                position: 'initial'
                                            }),

                                            control: styles => ({
                                                ...styles, width: '12rem!important', borderRadius: '2vh!important', maxHeight: '1.5rem', alignItems: 'baseline',
                                                height: '1.5rem'
                                            })
                                        }}
                                        className="ml-auto mr-2"
                                        classNamePrefix='select'
                                        onChange={(e: any, actionMeta: any) => {
                                            if (e) {
                                                const { action, option, removedValue } = actionMeta;
                                                if (s.isMulti) {
                                                    const { value, index } = e
                                                    if (action === "select-option" && option.value === selectAllOption.value) {
                                                        s.setValue([selectAllOption, ...s.options])
                                                    } else if (
                                                        (action === "deselect-option" &&
                                                            option.value === selectAllOption.value) ||
                                                        (action === "remove-value" &&
                                                            removedValue.value === selectAllOption.value)
                                                    ) {
                                                        s.setValue([])
                                                    } else if (
                                                        actionMeta.action === "deselect-option" &&
                                                        e.length === s.options.length
                                                    ) {
                                                        s.setValue(s.options.filter((ele:any) => ele.value !== option.value));
                                                    } else {
                                                        s.setValue(e)
                                                    }
                                                } else {
                                                    if (e === s.value) s.setValue(null);
                                                    else {
                                                        s.setValue(e)
                                                    }
                                                }
                                            } else s.setValue(s.isMulti ? [] : null)
                                        }}
                                        value={s.value}
                                        options={s.allowSelectAll ? [selectAllOption, ...s.options] : s.options}
                                    />
                                </div>
                            )
                        })
                        }
                        </div>


                    </div>
                </>
            ),
            buttons: [{
                color: 'primary', onClick: async () => {
                    setLoading(true)
                    let newRole: any = await modalHandlers.action(role, copyFrom && copyFrom.label ? copyFrom.label : null)
                    setLoading(false);
                    if (newRole) setRole(null); setCopyFrom(null); modalHandlers.setRoles([...roles, ...newRole]);
                    toggleModal(ModalTypes.none)
                },
                label: formatMessage({ id: 'add' }),
                disabled: loading || !role || !role.length
            }],
            footerChilds: [loading ? (<Spinner />) : null],
        }


    return (
        <GenericModals key={modalType} modal={addModal} />
    );
};



export default AddRoleModalData;
