import * as React from 'react';

import { GenericModals } from '@src/components/GenericModals/index';
import { ModalInterface, ModalsInterface, ModalTypes } from '@src/components/GenericModals/interfaces';

import { Icon } from '@components';
import { config } from '@src/config'
import { useIntl, FormattedMessage } from "react-intl"
import { components } from "react-select";
import Select from "react-select"
import makeAnimated from "react-select/animated";
import * as moment from 'moment';
import { Spinner } from 'reactstrap';


interface Props {
    modalType: ModalTypes,
    toggleModal: (type: ModalTypes) => void;
    modalHandlers: any;
    readonly role: any
}

export const DeleteRoleModalData: React.FC<Props> = props => {

    const {
        toggleModal,
        modalHandlers,
        modalType,
        role
    } = props;

    const { formatMessage } = useIntl();

    React.useEffect(()=> {
        console.log(role)
    })



    const deleteModal: ModalInterface = {
        classNames: 'modal-dialog-centered modal-sm',
        isOpen: modalType === ModalTypes.delete, toggle: () => toggleModal(ModalTypes.none), header: formatMessage({id:'WarningBeforeDelete'}), body: (<> 
        {formatMessage({id:'areYouSureYouWantToDeleteRole'})} {role}?
        </> ),
        buttons: [{ color: 'primary', onClick: () => toggleModal(ModalTypes.none), label: formatMessage({ id: 'No' }) }, { color: 'primary', outline: true, onClick: modalHandlers, label: formatMessage({ id: 'yesDelete' }) }
        ]
    }



return (
    <GenericModals key={modalType} modal={deleteModal} />
);
};



export default DeleteRoleModalData;
