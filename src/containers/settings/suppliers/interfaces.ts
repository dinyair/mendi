import { SuppliersActionTypes } from './enums';

export interface SuppliersAction {
	type: SuppliersActionTypes;
	payload: any;
}

export interface Supplier {
	readonly pos: number;
	readonly id: number;
	readonly company_id: string;
	readonly supplier_name: string,
	readonly color: string;
	readonly type: string;
	readonly contact_name: string;
}

export interface SubSupplier {
	Id: number;
	SubsapakId: number;
	SubsapakName: string;
	SapakId: number;
	ClassesId: number;
	groupId: number;
}

export interface Supplier {
	Id: number;
	Name: string;
	Comax: number | null;
	weeks: number;
	allow_order: number;
	flag_con: number;
}


export interface SuppliersRow {
	code: number;
	name: string;
	weeks: number;
	allow_order: number;
	flag_con: number;
	subSuppliers: SubSupplierRow[];
	code_name: string;
}

export interface SubSupplierRow {
	supplierId: number;
	code:       number;
	name:       string;
	detail:     SubSupplierDetail[];
	code_name: string;
}

export interface SubSupplierDetail {
	Id:           number;
	SubsapakId:   number;
	SubsapakName: string;
	SapakId:      number;
	ClassesId:    number;
	groupId:      number;
}

