import * as React from 'react';
import { useIntl } from 'react-intl';
import { ModalInterface, ModalTypes } from '@src/components/GenericModals/interfaces';
import { GenericModals } from '@src/components/GenericModals';
import { FormGroup, InputTypes } from '@src/components/form-group';
import { DrawerTab } from '@src/components/edit-drawer';
import classnames from 'classnames';
import { Icon } from '@src/components';
import { config } from '@src/config';
import { SubSupplierDetail, SubSupplierRow, SuppliersRow } from '../interfaces';
import { CheckBoxGroupsOptions, CheckBoxStatus, CheckBoxSubOption } from '@src/components/CheckBoxGroupsAccordion/interfaces';
import { CheckBoxesGroupsAccordion } from '@src/components/CheckBoxGroupsAccordion';

interface Props {
	modalType: ModalTypes;
	toggleModal: (type: ModalTypes) => void;
	modalHandlers: any;
	selectedRow?: SubSupplierRow;
	supplierData: SuppliersRow;
	divisionsWithGroups: CheckBoxGroupsOptions[];
	validationFields: any[];
}

export const SubSuppliersModalsData: React.FC<Props> = props => {
	const { toggleModal, modalHandlers, selectedRow, modalType, divisionsWithGroups, validationFields } = props;
	const { formatMessage } = useIntl();
	const [currentLevel, setCurrentLevel] = React.useState<number>(0);
	const [fieldsToReturn, setfieldsToReturn] = React.useState<any>();
	const [selectedCheckBoxOptions, setSelectedCheckBoxOptions] = React.useState<CheckBoxGroupsOptions[]>()
	const [subSupplierModalFields, setSubSupplierModalFields] = React.useState<any>()
	const customizer = { direction: document.getElementsByTagName("html")[0].dir }

	React.useEffect(() => {
		setSubSupplierModalFields(validationFields)
		setfieldsToReturn({ fields: validationFields })
	}, [validationFields])

	React.useEffect(() => {
		const selectedOptions = selectedRow && selectedRow.detail && selectedRow.detail.length > 0 ?
			selectedRow.detail.map((ele: SubSupplierDetail) => {
				return ele.groupId
			}) : []

		let newOptions: CheckBoxGroupsOptions[] = [...divisionsWithGroups];
		divisionsWithGroups.forEach((option: CheckBoxGroupsOptions, optionIndex: number) => {
			option.SubOptions.forEach((subOption: CheckBoxSubOption, subOptionIndex: number) => {
				newOptions[optionIndex].SubOptions[subOptionIndex].Status = selectedOptions.includes(subOption.Id) ? CheckBoxStatus.checked : CheckBoxStatus.unchecked;
			})
		})

		setSelectedCheckBoxOptions(newOptions.filter((ele) => ele))
	}, [])

	const handleNextLevel = () => {
		setCurrentLevel(currentLevel + 1);
	}

	const handleBackLevel = () => {
		setCurrentLevel(currentLevel - 1);
	}



	let drawerTabs = (): Array<DrawerTab> => [
		{
			title: '',
			tabId: '1',
			formGroup: [
				{
					formId: '1',
					fields: [...subSupplierModalFields],
				}
			]
		}
	];


	const genericForm = () =>
		drawerTabs().map(({ tabId, formGroup }) =>
			formGroup.map(({ title, formId, fields, additionalFields, extraFields }) => (
				<FormGroup
					key={formId}
					{...{
						title,
						fields: fieldsToReturn ? fieldsToReturn.fields : subSupplierModalFields,
						additionalFields,
						extraFields,
						id: tabId,
						formId,
						formGroupListClassName: "m-0",
						onChangeData: (tabId, formId, fields) => {
							setfieldsToReturn(fields);
						}

					}}
				/>
			))
		);

	const addEditModals: ModalInterface[] = [
		{
			classNames: 'width-35-per min-width-30-rem max-width-30-rem',
			isOpen: modalType === ModalTypes.add || modalType === ModalTypes.edit,
			toggle: () => toggleModal(ModalTypes.none),
			header: "",
			headerClassNames: 'pb-1 pt-2',
			body: (
				<div>
					<p className="font-medium-5 text-bold-700 ">{modalType === ModalTypes.add ? formatMessage({ id: 'addSubSupplier' }) : formatMessage({ id: 'data editing' })}</p>
					<div className="mt-2">{subSupplierModalFields ? genericForm() : null}</div>

					<div className={`d-flex align-items-center justify-content-center ${divisionsWithGroups && divisionsWithGroups.length > 0 ? 'text-turquoise' : 'text-gray opacity-07'} text-bold-600`}>
						<span className={divisionsWithGroups && divisionsWithGroups.length > 0 ? "cursor-pointer" : ""} onClick={
							divisionsWithGroups && divisionsWithGroups.length > 0 ? () => handleNextLevel() : () => { }
						}>
							+ {formatMessage({ id: 'assignDivisionsAndGroups' })}
						</span>
					</div>
				</div>
			),
			buttons: [
				{
					color: 'primary',
					onClick: () => {
						modalType === ModalTypes.add ? modalHandlers.add(fieldsToReturn, divisionsWithGroups) : modalHandlers.edit(fieldsToReturn, divisionsWithGroups)
					},
					label: modalType === ModalTypes.add ? formatMessage({ id: 'add' }) : formatMessage({ id: 'Update' }),
				}
			]
		},
		{
			classNames: 'width-35-per min-width-30-rem max-width-30-rem',
			isOpen: modalType === ModalTypes.add || modalType === ModalTypes.edit,
			toggle: () => toggleModal(ModalTypes.none),
			header: "",
			hideHeader: true,
			body: (
				<div className="mt-1 mb-1">
					<CheckBoxesGroupsAccordion
						options={selectedCheckBoxOptions ? selectedCheckBoxOptions : divisionsWithGroups}
						returnSelectedData={(data: CheckBoxGroupsOptions[]) => setSelectedCheckBoxOptions(data)}
						allowSelectMulti
						searchBarPlaceHolder={formatMessage({ id: 'searchDivisionsAndGroups' })}
						titleLabel={formatMessage({ id: 'assignDivisionsAndGroups' })}
						backButton={<span className={classnames("cursor-pointer width-2-rem mr-1", {
							"rotate-180": customizer.direction == 'ltr',
						})} onClick={() => handleBackLevel()}>
							<Icon src={'../' + config.iconsPath + "table/arrow-right.svg"} />
						</span>}
					/>
				</div>
			),
			footerClassNames: 'd-none',
		}
	]

	const deleteModal: ModalInterface = {
		classNames: 'modal-dialog-centered modal-sm',
		isOpen: modalType === ModalTypes.delete, toggle: () => toggleModal(ModalTypes.none), header: formatMessage({ id: 'WarningBeforeDelete' }), body: (<>
			{formatMessage({ id: 'areYouSureYouWantToDeleteSubSupplier' })}
		</>),
		buttons: [{ color: 'primary', onClick: () => toggleModal(ModalTypes.none), label: formatMessage({ id: 'No' }) }, { color: 'primary', outline: true, onClick: () => modalHandlers.delete(), label: formatMessage({ id: 'yesDelete' }) }
		]
	}


	return <GenericModals key={`modal`} modal={modalType === ModalTypes.delete ? deleteModal : addEditModals[currentLevel]} />;
};

export default SubSuppliersModalsData;
