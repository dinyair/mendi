import * as React from 'react';
import { useIntl } from 'react-intl';
import { ModalInterface, ModalTypes } from '@src/components/GenericModals/interfaces';
import { GenericModals } from '@src/components/GenericModals';
import { DrawerTab } from '@src/components/edit-drawer/interfaces';
import { FormGroup, InputTypes } from '@src/components/form-group';

interface Props {
	modalType: ModalTypes;
	toggleModal: (type: ModalTypes) => void;
	modalHandlers: any;
	selectedRow: any;
	validationFields: any;
}


export const SuppliersModalsData: React.FC<Props> = props => {
	const { toggleModal, modalHandlers, selectedRow, modalType, validationFields } = props;
	const { formatMessage } = useIntl();
	const customizer = { direction: document.getElementsByTagName("html")[0].dir }

	const [supplierModalFields, setSpplierModalFields] = React.useState<any>()

	React.useEffect(() => {
		setSpplierModalFields(editSubGroupFields)
		setFieldsToReturn({ fields: editSubGroupFields })
	}, [validationFields])

	let editSubGroupFields = [
		...validationFields,
		{
			label: formatMessage({ id: 'Weekly' }),
			value: selectedRow && selectedRow.weeks && selectedRow.weeks === 1 ? true : false,
			defaultValue: selectedRow && selectedRow.weeks && selectedRow.weeks === 1 ? true : false,
			name: 'Weekly',
			type: InputTypes.CHECKBOX,
			columns: 1,
			classNames: 'mr-2 ml-1 '
		}, {
			label: formatMessage({ id: 'consignment' }),
			labelClassNames: customizer.direction == 'ltr'  ? 'min-width-3-rem' : '',
			value: selectedRow && selectedRow.flag_con && selectedRow.flag_con === 1 ? true : false,
			defaultValue: selectedRow && selectedRow.flag_con && selectedRow.flag_con === 1 ? true : false,
			name: 'consignment',
			type: InputTypes.CHECKBOX,
			columns: 1,
			classNames: 'mr-2 ml-1'
		}, {
			label: formatMessage({ id: 'allowOrderSupplier' }),
			labelClassNames: '' ,
			value: selectedRow && selectedRow.allow_order && selectedRow.allow_order === 1 ? true : false,
			defaultValue: selectedRow && selectedRow.allow_order && selectedRow.allow_order === 1 ? true : false,
			name: 'allowOrderSupplier',
			type: InputTypes.CHECKBOX,
			columns: 1,
			classNames: 'mr-2 ml-1'
		},
	];

	const [fieldsToReturn, setFieldsToReturn] = React.useState<any>({ fields: editSubGroupFields });

	let drawerTabs = (): Array<DrawerTab> => [
		{
			title: '',
			tabId: '1',
			formGroup: [
				{
					formId: '1',
					fields: [...supplierModalFields],
				}
			]
		}
	];


	const genericForm = () =>
		drawerTabs().map(({ tabId, formGroup }) =>
			formGroup.map(({ title, formId, fields, additionalFields, extraFields }) => (
				<FormGroup
					key={formId}
					{...{
						title,
						fields: supplierModalFields,
						additionalFields,
						extraFields,
						id: tabId,
						formId,
						formGroupListClassName: "m-0",
						onChangeData: (tabId, formId, fields) => {
							setFieldsToReturn(fields);
						}

					}}
				/>
			))
		);


	const addEditModal: ModalInterface = {
		classNames: 'width-35-per min-width-30-rem max-width-30-rem ',
		isOpen: modalType === ModalTypes.add || modalType === ModalTypes.edit,
		toggle: () => toggleModal(ModalTypes.none),
		header: "",
		headerClassNames: 'pb-1 pt-2',
		bodyClassNames: 'pt-0 pb-0',
		body: (
			<div>
				<p className="font-medium-5 text-bold-700">{modalType === ModalTypes.add ? formatMessage({ id: 'addSupplier' }) : formatMessage({ id: 'data editing' })}</p>
				{supplierModalFields ? genericForm() : null}
			</div>
		),

		buttons: [
			{
				color: 'primary',
				onClick: () => {
					modalType === ModalTypes.add ? modalHandlers.add(fieldsToReturn) : modalHandlers.edit(fieldsToReturn)
				},
				label: modalType === ModalTypes.add ? formatMessage({ id: 'add' }) : formatMessage({ id: 'Update' }),
				disabled: false
			}
		]
	}


	const deleteModal: ModalInterface = {
		classNames: 'modal-dialog-centered modal-sm',
		isOpen: modalType === ModalTypes.delete, toggle: () => toggleModal(ModalTypes.none), header: formatMessage({ id: 'WarningBeforeDelete' }), body: (<>
			{formatMessage({ id: 'areYouSureYouWantToDeleteSupplier' })}
		</>),
		buttons: [{ color: 'primary', onClick: () => toggleModal(ModalTypes.none), label: formatMessage({ id: 'No' }) }, { color: 'primary', outline: true, onClick: () => modalHandlers.delete(), label: formatMessage({ id: 'yesDelete' }) }
		]
	}

	return <GenericModals key={`modal`} modal={modalType === ModalTypes.delete ? deleteModal : addEditModal} />;
};

export default SuppliersModalsData;
