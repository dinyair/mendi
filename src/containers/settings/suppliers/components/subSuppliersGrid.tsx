import * as React from 'react';
import { useIntl } from "react-intl"
import { fetchAndUpdateStore } from '@src/helpers/helpers';
import { SubSupplierRow, SuppliersRow } from '../interfaces';
import { AgGrid } from '@src/components';
import InputText from '@src/components/input-text';
import { ModalTypes } from '@src/components/GenericModals/interfaces';
import { Button, Col, Modal, Spinner } from "reactstrap"
import SubSuppliersModalsData from './subSuppliersModalsData';
import { deleteSubSupplier, getCatalogSubSupplier, insertSubSupplier } from '../initial-state';
import { CheckBoxGroupsOptions, CheckBoxStatus, CheckBoxSubOption } from '@src/components/CheckBoxGroupsAccordion/interfaces';
import { InputTypes } from '@src/components/form-group';
import { RootStore } from '@src/store';
import { useSelector } from 'react-redux';

interface Props {
	readonly back: (rows: any) => void;
	readonly supplierData: SuppliersRow;
	readonly groupsArr: any[];
	readonly divisionsArr: any[];
}



export const SubSuppliersGrid: React.FC<Props> = (props: Props) => {

	const { back, supplierData, groupsArr, divisionsArr } = props;
	const { formatMessage } = useIntl();
	const [modalType, setModalType] = React.useState<ModalTypes>(ModalTypes.none)

	const user = useSelector((state: RootStore) => state.user)
	const userPermissions = user ? user.permissions : null
	let URL = window.location.pathname
	const userEditFlag = userPermissions && userPermissions[URL] && userPermissions[URL].edit_flag === 1

	const [rows, setRows] = React.useState<SubSupplierRow[]>(supplierData.subSuppliers)
	const [originalRows, setOriginalRows] = React.useState<SubSupplierRow[]>(supplierData.subSuppliers)
	const subSupplierInputRef = React.useRef<any>();
	const [searchInput, setSearchInput] = React.useState<string>('')
	const [selectedRow, setSelectedRow] = React.useState<SubSupplierRow | any>(null)
	const [dataIsReady, setDataIsReady] = React.useState<Boolean>(true)
	const [newRowsLength, setNewRowsLength] = React.useState<number>(0)

	const [divisionsWithGroups, setDivisionsWithGroups] = React.useState<any[]>([])
	const originalFields = [
		{
			value: selectedRow && selectedRow.name ? selectedRow.name : '',
			defaultValue: selectedRow && selectedRow.name ? selectedRow.name : '',
			placeholder: formatMessage({ id: 'enterSubSupplierName' }),
			name: 'optionName',
			type: InputTypes.TEXT,
			columns: 4,
			error: null
		}
	]
	
	const [subSupplierModalFields, setSubSupplierModalFields] = React.useState<any>([...originalFields])

	const [fields, setFields] = React.useState<any>([
		{ text: "code", type: 'number', width: window.innerWidth * 0.2, minWidth: 100, },
		{
			text: "name", type: 'string', width: window.innerWidth * 0.4, minWidth: 200,
		},
		{
			text: "actions", type: 'string', width: window.innerWidth * 0.4, minWidth: 150, noFilter: true,
			cellRenderer: 'IconRender', renderIcons: [
				{ actions: true },
				{
					type: 'edit', title: 'Edit', onClick: (data: any) => {
						setSelectedRow(data)
						toggleModal(ModalTypes.edit)
					}, icon: 'edit.svg'
				},
				userEditFlag ?{
					type: 'delete', title: 'Delete', returnDataWithProps: true, onClick: (data: any) => {
						setSelectedRow(data.data)
						toggleModal(ModalTypes.delete)
					}, icon: 'trash.svg'
				}:null,
			],
		},
	])


	React.useEffect(() => {
		fetchData()
	}, [])

	React.useEffect(() => {
		setSubSupplierModalFields([...originalFields])
	}, [selectedRow])



	const fetchData = async () => {
		const res = await getCatalogSubSupplier(supplierData.code)
		const all_group: any[] = res;
		let divisionsWithGroups: CheckBoxGroupsOptions[] = [];

		for (var i = 0; i < res.length; i++) {
			if (divisionsArr[all_group[i].ClassesId] != undefined) all_group[i].ClassesName = divisionsArr[all_group[i].ClassesId].Name;
			if (groupsArr[all_group[i].GroupId] != undefined) all_group[i].GroupName = groupsArr[all_group[i].GroupId].Name;

			const element = all_group[i];
			if (!divisionsWithGroups[element.ClassesId]) {
				divisionsWithGroups[element.ClassesId] = {
					Id: element.ClassesId,
					Name: element.ClassesName,
					SubOptions: [{ Id: element.GroupId, Name: element.GroupName, Status: CheckBoxStatus.unchecked }]
				}
			} else {
				divisionsWithGroups[element.ClassesId].SubOptions.push({ Id: element.GroupId, Name: element.GroupName, Status: CheckBoxStatus.unchecked })
			}
		}

		setDivisionsWithGroups(divisionsWithGroups)
	}
	const toggleModal = (newType: ModalTypes) => {
		if (newType === ModalTypes.none) {
			setSelectedRow(null)
		}
		setModalType(newType)
	}

	const search = (rows: SubSupplierRow[]) => {
		const condition = subSupplierInputRef && subSupplierInputRef.current && subSupplierInputRef.current.value && subSupplierInputRef.current.value.length > 0;
		setSearchInput(condition ? subSupplierInputRef.current.value : '')
		setDataIsReady(false)

		setRows(condition ? [...rows].filter((row: SubSupplierRow) => row.code_name.includes(searchInput)) : [...originalRows])
		setTimeout(() => {
			setDataIsReady(true)
		}, 100);
	}


	const handleAdd = async (fields: any, options: CheckBoxGroupsOptions[]) => {
		let shouldClose = false
		if (fields && fields.fields) {
			const oldFields = [...fields.fields]

			if (oldFields[0].value.trim().length > 0) {
				let newId: string = `${supplierData.code}`;
				let subSuppliersLength = originalRows.length + 1 + newRowsLength
				subSuppliersLength < 10 ? newId += "0" + subSuppliersLength : newId += subSuppliersLength


				let details: any = [];
				let detail: any = [];

				options.forEach((option: CheckBoxGroupsOptions) => {
					option.SubOptions.forEach((subOption: CheckBoxSubOption) => {
						if (subOption.Status === CheckBoxStatus.checked) {
							details.push(({ ClassesId: option.Id, groupId: subOption.Id }))
							detail.push(({
								ClassesId: option.Id,
								groupId: subOption.Id,
								SubsapakId: Number(newId),
								SubsapakName: oldFields ? oldFields[0].value : selectedRow.name,
								SapakId: supplierData.code,
							}))
						}
					})
				});


				let newSubSupplier = {
					SubsapakId: Number(newId),
					SubsapakName: oldFields[0].value,
					SapakId: supplierData.code,
					detail: details
				}

				let subSupplierRow: SubSupplierRow = {
					code: newSubSupplier.SubsapakId,
					name: newSubSupplier.SubsapakName,
					supplierId: newSubSupplier.SapakId,
					code_name: `${newSubSupplier.SubsapakId} ${newSubSupplier.SubsapakName}`,
					detail: detail
				}

				await insertSubSupplier({ rec1: newSubSupplier })

				const newRows: SubSupplierRow[] = [...originalRows, subSupplierRow]
				setNewRowsLength(newRowsLength + 1)
				setSelectedRow(null)
				setRows(newRows)
				setOriginalRows(newRows)
				setSubSupplierModalFields([...originalFields])

				shouldClose = true;
			}

		}
		if (shouldClose === false) {
			const newFields = [{
				value: selectedRow && selectedRow.name ? selectedRow.name : '',
				defaultValue: selectedRow && selectedRow.name ? selectedRow.name : '',
				placeholder: formatMessage({ id: 'enterSubSupplierName' }),
				name: 'optionName',
				type: InputTypes.TEXT,
				columns: 4,
				error: { label: formatMessage({ id: 'requiredFields' }) }
			}]

			setSubSupplierModalFields([...newFields])
		}

		if (shouldClose) toggleModal(ModalTypes.none)
	}



	const handleEdit = async (fields: any, options: CheckBoxGroupsOptions[]) => {
		let shouldClose = false
		if (fields && fields.fields) {
			let oldFields = [...fields.fields]
			if (oldFields[0].value.trim().length > 0) {
				let details: any = [];
				let detail: any = [];

				options.forEach((option: CheckBoxGroupsOptions) => {
					option.SubOptions.forEach((subOption: CheckBoxSubOption) => {
						if (subOption.Status === CheckBoxStatus.checked) {
							details.push(({ ClassesId: option.Id, groupId: subOption.Id }))
							detail.push(({
								ClassesId: option.Id,
								groupId: subOption.Id,
								SubsapakId: selectedRow.code,
								SubsapakName: oldFields ? oldFields[0].value : selectedRow.name,
								SapakId: supplierData.code,
							}))
						}
					})
				});

				let editedSubSupplier = {
					SubsapakId: selectedRow.code,
					SubsapakName: oldFields ? oldFields[0].value : selectedRow.name,
					SapakId: supplierData.code,
					detail: details
				}


				await insertSubSupplier({ rec1: editedSubSupplier })

				const rows: SubSupplierRow[] = [...originalRows]
				const rowIndex = rows.findIndex((ele: SubSupplierRow) => ele.code === selectedRow.code)
				rows[rowIndex].name = oldFields ? oldFields[0].value : selectedRow.name;
				rows[rowIndex].detail = detail

				setRows(rows)
				setOriginalRows(rows)
				shouldClose = true;
			}
		}

		if (shouldClose === false) {
			const newFields = [{
				value: '',
				defaultValue: selectedRow && selectedRow.name ? selectedRow.name : '',
				placeholder: formatMessage({ id: 'enterSubSupplierName' }),
				name: 'optionName',
				type: InputTypes.TEXT,
				columns: 4,
				error: { label: formatMessage({ id: 'requiredFields' }) }
			}]

			setSubSupplierModalFields([...newFields])
		}

		if (shouldClose) toggleModal(ModalTypes.none)

	}



	const handleDelete = async () => {
		await deleteSubSupplier({ Id: selectedRow.code })

		const rows = [...originalRows].filter((ele: any) => ele.code !== selectedRow.code)
		setSelectedRow(null)
		setRows(rows)
		setOriginalRows(rows)
		toggleModal(ModalTypes.none)
	}


	const modalHandlers = {
		add: (fields: any, options: any) => handleAdd(fields, options),
		delete: () => handleDelete(),
		edit: (fields: any, options: any) => handleEdit(fields, options),
	};

	const customizer = { direction: document.getElementsByTagName('html')[0].dir };


	const pageTitle = `${formatMessage({ id: 'Suppliers' })} - ${supplierData.name}`;
	document.title = pageTitle;

	return (<>


		<div className='d-flex-column'>
			<div className='mb-1 font-medium-5 text-bold-700 text-black d-flex'>
				{<span className={`cursor-pointer mr-05 ${customizer.direction == 'ltr' ? 'rotate-180' : ''}`} onClick={() => back(originalRows)}>➜</span>} {pageTitle}
			</div>

			<div className='d-flex justify-content-between width-55-rem'>
				<div >
					<InputText searchIcon inputClassNames='width-17-rem' id={'title'} value={searchInput && searchInput.length > 0 ? searchInput : undefined} onChange={() => search(originalRows)} placeholder={formatMessage({ id: 'enterCodeOrSubSupplierName' })} inputRef={subSupplierInputRef} />
				</div>

				{userEditFlag && (
					<div>
						<Button
							color="primary"
							className="ml-1 round text-bold-400 d-flex justify-content-center align-items-center width-11-rem height-2-5-rem cursor-pointer btn-primary "
							onClick={() => { setSelectedRow(null); toggleModal(ModalTypes.add) }}>
							+ {formatMessage({ id: 'newSubSupplier' })}
						</Button>
					</div>
				)}
			</div>

		</div>

		<div className='mb-1 d-flex width-55-rem'>
			<AgGrid
				rowBufferAmount={50}
				defaultSortFieldNum={0}
				id={'code'}
				gridHeight={'70vh'}
				translateHeader
				fields={fields}
				groups={[]}
				totalRows={[]}
				resizable
				rows={rows}
				displayLoadingScreen={dataIsReady ? false : true}
				pagination
			/>
		</div>

		{modalType === ModalTypes.edit ||
			modalType === ModalTypes.delete ||
			modalType === ModalTypes.add ?
			<SubSuppliersModalsData
				validationFields={subSupplierModalFields}
				modalHandlers={modalHandlers}
				divisionsWithGroups={divisionsWithGroups}
				modalType={modalType}
				selectedRow={selectedRow}
				supplierData={supplierData}
				toggleModal={(type: ModalTypes) => { setSelectedRow(null); toggleModal(type) }}
			/> : null}

	</>)
		;

};

export default SubSuppliersGrid;
