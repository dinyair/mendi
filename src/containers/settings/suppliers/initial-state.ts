import { getRequest, postRequest } from '@src/http';
import axios from 'axios';
  


export const suppliersInitialState: any = {};

export const insertSupplier = async (obj:any) => {
    const resp = await postRequest<any>('AppInsertSapakim',obj)
    return resp
}


export const updateSupplier = async  (obj:any) => {
    const resp = await postRequest<any>('AppUpdateSapakim',obj)
    return resp
}

export const deleteSupplier = async (obj:any) => {
    const resp = await postRequest<any>('AppDeleteSapakim',obj)
    return resp
}

export const deleteSubSupplier = async (obj:any) => {
    const resp = await postRequest<any>('AppDeleteSubSapak',obj)
    return resp
}

export const insertSubSupplier = async (obj:any) => {
    const resp = await postRequest<any>('AppInsertSubSapak1',obj)
    return resp
}



export const getCatalogSubSupplier = async (supplierId:any) => {
    const resp = await getRequest<any>(`getcatalogHlaSub?sapak1=${supplierId}`)
    return resp
}