import * as React from 'react';
import { useIntl } from "react-intl"
import { RootStore } from '@src/store';
import { useSelector, useDispatch } from 'react-redux';
import { fetchAndUpdateStore } from '@src/helpers/helpers';
import { SubSupplier, SubSupplierDetail, SubSupplierRow, Supplier, SuppliersRow } from './interfaces';
import { AgGrid, Icon } from '@src/components';
import InputText from '@src/components/input-text';
import { ModalTypes } from '@src/components/GenericModals/interfaces';
import { Button, Col, Modal, Spinner } from "reactstrap"
import { SuppliersPageType } from './enums';
import SubSuppliersGrid from './components/subSuppliersGrid';
import { Divisions } from '@src/redux/actions/divisions/divisionsActions';
import { Groups } from '@src/redux/actions/groups/groupsAction';
import SuppliersModalsData from './components/suppliersModalsData';
import { deleteSupplier, insertSupplier, updateSupplier } from './initial-state';
import { InputTypes } from '@src/components/form-group';


interface Props {
}



export const Suppliers: React.FC<Props> = (props: Props) => {
	const { formatMessage } = useIntl();

	const user = useSelector((state: RootStore) => state.user)
	const userPermissions = user ? user.permissions : null
	let URL = window.location.pathname
	const userEditFlag = userPermissions && userPermissions[URL] && userPermissions[URL].edit_flag === 1

	const subSuppliers: SubSupplier[] = useSelector((state: RootStore) => state._subSuppliers)
	const suppliers: Supplier[] = useSelector((state: RootStore) => state._suppliers)
	const divisions: Divisions[] = useSelector((state: RootStore) => state._divisions)
	const groups: Groups[] = useSelector((state: RootStore) => state._groups)

	const [pageType, setPageType] = React.useState<SuppliersPageType>(SuppliersPageType.suppliers)
	const [isLoading, setIsLoading] = React.useState<Boolean>(true)
	const [dataIsReady, setDataIsReady] = React.useState<Boolean>(false)

	const [rows, setRows] = React.useState<[] | SuppliersRow[]>([])
	const [originalRows, setOriginalRows] = React.useState<[] | SuppliersRow[]>([])
	const freeSearchInputRef = React.useRef<any>();
	const [searchInput, setSearchInput] = React.useState<string>('')
	const [selectedSupplier, setSelectedSupplier] = React.useState<SuppliersRow | null>(null)

	const [modalType, setModalType] = React.useState<ModalTypes>(ModalTypes.none)

	const [newDrawerData, setDrawerData] = React.useState<any>([])


	const [groupsArr, setGroupsArr] = React.useState<[]>([])
	const [divisionsArr, setDivisionsArr] = React.useState<[]>([])

	const originalFields = [{
		disabled: modalType === ModalTypes.edit,
		label: formatMessage({ id: 'code' }),
		placeholder: formatMessage({ id: 'enterSupplierCode' }),
		value: selectedSupplier && selectedSupplier.code ? selectedSupplier.code : '',
		name: 'optionId',
		type: InputTypes.NUMBER,
		columns: 2
	},
	{
		label: formatMessage({ id: 'name' }),
		value: selectedSupplier && selectedSupplier.name ? selectedSupplier.name : '',
		placeholder: formatMessage({ id: 'enterSupplierName' }),
		name: 'optionName',
		type: InputTypes.TEXT,
		columns: 2
	}]
	const [validationFields, setValidationFields] = React.useState<any>([...originalFields])

	const [fields, setFields] = React.useState<any>([
		{ text: "code", type: 'number', width: window.innerWidth * 0.1, minWidth: 100, },
		{
			text: "name", type: 'string', width: window.innerWidth * 0.8, minWidth: 200,
			cellRenderer: 'ClickableCell', clickableCells: {
				onClick: (data: any) => {
					setPageType(SuppliersPageType.subSuppliers)
					setSelectedSupplier(data.data)
				}
			},
		},
		{
			text: "actions", type: 'string', width: window.innerWidth * 0.1, minWidth: 150, noFilter: true,
			cellRenderer: 'IconRender', renderIcons: [
				{ actions: true },
				{
					type: 'edit', title: 'Edit', onClick: (data: any) => {
						setSelectedSupplier(data)
						toggleModal(ModalTypes.edit)
					}, icon: 'edit.svg'
				},
				userEditFlag ?{
					type: 'delete', title: 'Delete', returnDataWithProps: true, onClick: (data: any) => {
						setSelectedSupplier(data.data)
						toggleModal(ModalTypes.delete)
					}, icon: 'trash.svg'
				} : null,
			],
		},
	])



	React.useEffect(() => {
		fetchData()
	}, [])
	React.useEffect(() => {
		setValidationFields([...originalFields])
	}, [selectedSupplier])



	const fetchData = async () => {
		setIsLoading(true)
		if (suppliers && suppliers.length && subSuppliers && subSuppliers.length && divisions && divisions.length && groups && groups.length) {
			setIsLoading(false)
		} else {
			await fetchAndUpdateStore(dispatch, [
				{ state: suppliers, type: 'suppliers' },
				{ state: subSuppliers, type: 'subSuppliers' },
				{ state: divisions, type: 'divisions' },
				{ state: groups, type: 'groups' },
			])
			setIsLoading(false)
		}

	}



	React.useEffect(() => {
		if (!isLoading && !dataIsReady) getRows()
	}, [isLoading])


	const getRows = () => {
		const rows: SuppliersRow[] = [];

		let subSuppliers_details = [];
		let detail: SubSupplierDetail[] = [];
		let supplierId = null;
		let subSupplierName = null;
		let subSupplierId = 0;
		for (let i = 0; i < subSuppliers.length; i++) {
			let subSupplier: SubSupplierDetail = subSuppliers[i];
			if (subSupplierId == 0) {
				subSupplierId = subSupplier.SubsapakId;
				supplierId = subSupplier.SapakId;
				subSupplierName = subSupplier.SubsapakName;
				detail = [];
				detail.push(subSupplier);
			} else {
				if (subSupplierId == subSupplier.SubsapakId) {
					detail.push(subSupplier);
				} else {
					subSuppliers_details.push({
						supplierId: supplierId,
						code: subSupplierId,
						name: subSupplierName,
						code_name: `${subSupplierId} ${subSupplierName}`,
						detail: detail
					})
					subSupplierId = subSupplier.SubsapakId;
					supplierId = subSupplier.SapakId;
					subSupplierName = subSupplier.SubsapakName;
					detail = [];
					detail.push(subSupplier);
				}
			}
		}
		if (subSupplierId != 0) {
			subSuppliers_details.push({
				supplierId: supplierId,
				code: subSupplierId,
				name: subSupplierName,
				code_name: `${subSupplierId} ${subSupplierName}`,
				detail: detail
			})
		}



		for (let index = 0; index < suppliers.length; index++) {
			const supplier: Supplier = suppliers[index];
			const subRows: any = subSuppliers_details.filter((ele: any) => ele.supplierId == supplier.Id)

			let row: SuppliersRow = {
				code: supplier.Id,
				name: supplier.Name,
				allow_order: supplier.allow_order,
				flag_con: supplier.flag_con,
				weeks: supplier.weeks,
				code_name: `${supplier.Id} ${supplier.Name}`,
				subSuppliers: subRows
			}
			rows.push(row)
		}

		setOriginalRows(rows)
		setIsLoading(false)
		setRows(rows)

		setTimeout(() => {
			setDataIsReady(true)
		}, 1000);

		const groupsArr: any = [];
		const divisionsArr: any = [];

		for (var i = 0; i < divisions.length; i++) {
			divisionsArr[divisions[i].Id] = divisions[i];
		}

		for (var i = 0; i < groups.length; i++) {
			groupsArr[groups[i].Id] = groups[i];
		}


		setGroupsArr(groupsArr)
		setDivisionsArr(divisionsArr)
	}


	const toggleModal = (newType: ModalTypes) => {
		if (newType === ModalTypes.none) {
			setSelectedSupplier(null)
			setValidationFields([...originalFields])
		}
		setModalType(newType)
	}


	
	document.title = formatMessage({ id: 'Suppliers' })
	const dispatch = useDispatch();


	const handleAdd = async (fields: any) => {
		let shouldClose = false
		let emptyCode: boolean = false;
		let emptyName: boolean = false;
		let existsCode: boolean = false;
		const oldFields = [...fields.fields]

		if (fields && fields.fields) {

			oldFields[0].value.trim().length > 0 ? emptyCode = false : emptyCode = true;
			oldFields[1].value.trim().length ? emptyName = false : emptyName = true

			originalRows.find((supplier: SuppliersRow) => supplier.code === Number(oldFields[0].value)) ? existsCode = true : existsCode = false;

			if (!emptyCode && !emptyName && !existsCode) {
				let newSupplier = {
					Id: oldFields[0].value,
					Name: oldFields[1].value,
					weeks: oldFields[2].value === 'true' ? 1 : 0,
					flag_con: oldFields[3].value === 'true' ? 1 : 0,
					allow_order: oldFields[4].value === 'true' ? 1 : 0
				}

				let supplierRow: SuppliersRow = {
					code: Number(oldFields[0].value),
					name: oldFields[1].value,
					weeks: oldFields[2].value === 'true' ? 1 : 0,
					flag_con: oldFields[3].value === 'true' ? 1 : 0,
					allow_order: oldFields[4].value === 'true' ? 1 : 0,
					subSuppliers: [],
					code_name: `${oldFields[0].value} ${oldFields[1].value}`
				}

				await insertSupplier({ rec1: newSupplier })
				search([...originalRows, supplierRow])
				shouldClose = true;
			}
		}

		if (shouldClose === false) {
			const newFields = [{
				disabled: modalType === ModalTypes.edit,
				label: formatMessage({ id: 'code' }),
				placeholder: formatMessage({ id: 'enterSupplierCode' }),
				value: oldFields[0].value,
				name: 'optionId',
				type: InputTypes.NUMBER,
				columns: 2,
				error: emptyCode ? { label: formatMessage({ id: 'requiredFields' }) } : existsCode ? { label: formatMessage({ id: 'idIsTaken' }) } : null
			},
			{
				label: formatMessage({ id: 'name' }),
				value: oldFields[1].value,
				placeholder: formatMessage({ id: 'enterSupplierName' }),
				name: 'optionName',
				type: InputTypes.TEXT,
				columns: 2,
				error: emptyName ? { label: formatMessage({ id: 'requiredFields' }) } : null
			}]

			setValidationFields([...newFields])
		}

		if (shouldClose) toggleModal(ModalTypes.none)
	}



	const handleEdit = async (fields: any) => {
		let shouldClose = false
		let emptyName: boolean = false;
		const oldFields = [...fields.fields]

		if (fields && fields.fields) {
			oldFields[1].value.trim().length ? emptyName = false : emptyName = true

			if (!emptyName) {
				let supplierId:any = selectedSupplier?.code;
				let newSupplier = {
					Id: supplierId,
					Name: oldFields[1].value,
					weeks: oldFields[2].value === 'true' || oldFields[2].value === true ? 1 : 0,
					flag_con: oldFields[3].value === 'true' || oldFields[3].value === true ? 1 : 0,
					allow_order: oldFields[4].value === 'true' || oldFields[4].value === true ? 1 : 0
				}

				let supplierRow: SuppliersRow = {
					code: supplierId,
					name: oldFields[1].value,
					weeks: oldFields[2].value === 'true' || oldFields[2].value === true ? 1 : 0,
					flag_con: oldFields[3].value === 'true' || oldFields[3].value === true ? 1 : 0,
					allow_order: oldFields[4].value === 'true' || oldFields[4].value === true ? 1 : 0,
					subSuppliers: [],
					code_name: `${supplierId} ${oldFields[1].value}`
				}

				await updateSupplier({ rec1: newSupplier })

				const newRows: SuppliersRow[] = [...originalRows]
				const rowIndex = newRows.findIndex((ele: SuppliersRow) => ele.code === supplierRow.code)
				newRows[rowIndex] = {...supplierRow};
				search(newRows)
				shouldClose = true;
			}
		}

		if (shouldClose === false) {
			const newFields = [{
				disabled: modalType === ModalTypes.edit,
				label: formatMessage({ id: 'code' }),
				placeholder: formatMessage({ id: 'enterSupplierCode' }),
				value: selectedSupplier && selectedSupplier.code ? selectedSupplier.code : '',
				name: 'optionId',
				type: InputTypes.NUMBER,
				columns: 2,
			},
			{
				label: formatMessage({ id: 'name' }),
				value: oldFields[1].value,
				placeholder: formatMessage({ id: 'enterSupplierName' }),
				name: 'optionName',
				type: InputTypes.TEXT,
				columns: 2,
				error: emptyName ? { label: formatMessage({ id: 'requiredFields' }) } : null
			}]
			setValidationFields([...newFields])
		}

		if (shouldClose) toggleModal(ModalTypes.none)
	}
	const search = (rows: any) => {
		const condition = freeSearchInputRef && freeSearchInputRef.current && freeSearchInputRef.current.value && freeSearchInputRef.current.value.length > 0 ? true : false;
		setOriginalRows(rows)
		setSearchInput(freeSearchInputRef.current.value)
		setDataIsReady(false)
		setRows(condition ? [...rows].filter((row: SuppliersRow) => row.code_name.includes(freeSearchInputRef.current.value)) : [...rows])
		setTimeout(() => {
			setDataIsReady(true)
		}, 100);
	}


	const searchOnBack = (rows: any) => {
		const condition = searchInput && searchInput != '';
		setDataIsReady(false)
		setRows(condition ? [...rows].filter((row: SuppliersRow) => row.code_name.includes(searchInput)) : [...originalRows])
		setTimeout(() => {
			setDataIsReady(true)
		}, 100);
	}
	const handleDelete = async () => {
		const Id = selectedSupplier ? selectedSupplier.code : null
		await deleteSupplier({ Id })

		const filteredRows = [...originalRows].filter((ele: SuppliersRow) => ele.code !== Id)
		setOriginalRows(filteredRows)
		search(filteredRows)
		toggleModal(ModalTypes.none)
	}
	const modalHandlers = {
		add: (fields: any) => handleAdd(fields),
		delete: () => handleDelete(),
		edit: (fields: any) => handleEdit(fields),
	};
	return (
		selectedSupplier && pageType === SuppliersPageType.subSuppliers ?

			<SubSuppliersGrid supplierData={selectedSupplier} groupsArr={groupsArr} divisionsArr={divisionsArr} back={(newRows: any) => {
				const oldRows: SuppliersRow[] = [...originalRows]
				const rowIndex = oldRows.findIndex((ele: SuppliersRow) => ele.code === selectedSupplier.code)
				oldRows[rowIndex].subSuppliers = newRows;

				setOriginalRows(oldRows)
				searchOnBack(oldRows)

				setPageType(SuppliersPageType.suppliers)
				setSelectedSupplier(null)
			}} />
			:

			<>
				<div className='d-flex-column'>
					<div className='mb-1 font-medium-5 text-bold-700 text-black d-flex'>
						{formatMessage({ id: 'Suppliers' })}
					</div>

					<div className='d-flex justify-content-between width-55-rem'>
						<div >
							<InputText searchIcon inputClassNames='width-17-rem' id={'title'} value={searchInput && searchInput.length > 0 ? searchInput : undefined}
								onChange={() => { search(originalRows) }}

								placeholder={formatMessage({ id: 'enterCodeOrSupplierName' })} inputRef={freeSearchInputRef} />
						</div>

						{userEditFlag && (
						<div>
							<Button disabled={suppliers && suppliers.length && suppliers.length > 0 ? false : true} onClick={() => { toggleModal(ModalTypes.add) }} className='round width-9-rem width-11-rem height-2-5-rem d-flex align-items-center justify-content-center' color='primary'>+ {formatMessage({ id: 'newSupplier' })}</Button>
						</div>
						)}
					</div>

				</div>

				<div className='mb-1 d-flex width-55-rem'>
					<AgGrid
						rowBufferAmount={15}
						defaultSortFieldNum={0}
						id={'code'}
						gridHeight={'70vh'}
						translateHeader
						fields={fields}
						groups={[]}
						totalRows={[]}
						rows={rows}
						cellGreenFields={['name']}
						successDark={'name'}
						pagination
						resizable
						displayLoadingScreen={dataIsReady ? false : true}
					/>
				</div>

				{modalType === ModalTypes.edit ||
					modalType === ModalTypes.delete ||
					modalType === ModalTypes.add ?
					<SuppliersModalsData
						modalHandlers={modalHandlers}
						modalType={modalType}
						selectedRow={selectedSupplier}
						validationFields={validationFields}
						toggleModal={(type: ModalTypes) => { setSelectedSupplier(null); toggleModal(type) }}

					/> : null}
			</>
	);
};

export default Suppliers;
