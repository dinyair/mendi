import { SuppliersActionTypes } from './enums';
import { suppliersInitialState } from './initial-state';
import { SubSupplier, Supplier, SuppliersAction } from './interfaces';



export const suppliersReducer = (
	state: Supplier[] = [],
	{ type, payload }: { type: any; payload: Supplier[] }): Supplier[] => {
	switch (type) {
		case SuppliersActionTypes.SET_SUPPLIERS:
			return payload;
		default:
			return state;
	}
};
export const subSuppliersReducer = (
	state: SubSupplier[] = [],
	{ type, payload }: { type: any; payload: SubSupplier[] }): SubSupplier[] => {
	switch (type) {
		case SuppliersActionTypes.SET_SUBSUPPLIERS:
			return payload;
		default:
			return state;
	}
};

