export enum SuppliersActionTypes {
	SET_SUPPLIERS = 'SET_SUPPLIERS',
	SET_SUBSUPPLIERS = 'SET_SUBSUPPLIERS',

}


export enum SuppliersPageType {
	suppliers = 'suppliers',
	subSuppliers = 'subSuppliers'
}