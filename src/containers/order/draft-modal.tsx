import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams, useHistory } from "react-router-dom";
import { Button, Modal, ModalBody, ModalFooter } from 'reactstrap';
import { ModalHeader } from "@src/components/modal";
import { sendToDraft } from "@src/containers/stock/orders/services/actions";
import { TYPES } from '@src/containers/stock/orders/services/reducer';
import { Routes } from "@utilities";
import { useIntl } from "react-intl";
import { sendOrderToDraft } from './initial-state'
import { RootStore } from '@src/store';
import {Spinner} from 'reactstrap'


interface Props {
	isOpen: boolean;
	toggleModal: () => void;
	setDisplayOrdersBySupplier: (boolean: any) => void;
	searchOrders: () => void;

}

export const DraftModal: React.FC<Props> = ({ isOpen, toggleModal, setDisplayOrdersBySupplier, searchOrders }) => {
	const itemsToOrder = useSelector((state: RootStore) => state._orderToPlace)
	const { formatMessage } = useIntl();
	const [isClicked, setIsClicked] = React.useState<boolean>(false)

	const onSendDraft = async () => {
		setIsClicked(true)
		await sendOrderToDraft(itemsToOrder)
		setDisplayOrdersBySupplier(false)
		setIsClicked(false)
		searchOrders()
	}

	return (
		<Modal isOpen={isOpen} toggle={toggleModal} className='modal-dialog-centered'>
			<ModalHeader isClicked={isClicked} onClose={toggleModal}>{formatMessage({ id: 'productIsOnSale' })}</ModalHeader>
			<ModalBody isClicked={isClicked} className='font-medium-2'>
				{formatMessage({ id: 'onSaleSkipMsg' })}
			</ModalBody>
			<ModalFooter>
				<Button disabled={isClicked} className='round' color='primary' outline onClick={toggleModal}>
					{formatMessage({ id: 'discard' })}
				</Button>
				<Button disabled={isClicked} className='round' color='primary' onClick={onSendDraft}>
					{formatMessage({ id: 'accept' })}
				</Button>
				{isClicked ? <Spinner /> : null}
			</ModalFooter>
		</Modal>
	)
}
