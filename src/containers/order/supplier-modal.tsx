import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useParams } from "react-router-dom";
import { Button, Modal, ModalBody, ModalFooter } from 'reactstrap';
import { ModalHeader } from "@src/components/modal";
import { Spinner } from 'reactstrap'
import { getAspkaDate, addOrder, sendOrderToDraft } from './initial-state'
import { useIntl } from "react-intl";
import { RootStore } from '@src/store';

import * as moment from 'moment';

interface Props {
	isOpen: boolean;
	toggleModal: () => void;
	setDisplayOrdersBySupplier: (boolean: any) => void;
	searchOrders: () => void;
	chosenSupplierId: any;
	chosenBranch: any;
}

export const SupplierModal: React.FC<Props> = ({ isOpen, toggleModal, setDisplayOrdersBySupplier, chosenSupplierId, searchOrders, chosenBranch }) => {
	// const dispatch = useDispatch();
	// const history = useHistory();
	const { formatMessage } = useIntl();
	const [isClicked, setIsClicked] = React.useState(false)
	const itemsToOrder = useSelector((state: RootStore) => state._orderToPlace)
	let userBranches:any = useSelector((state: RootStore) => state._userBranches);

	const onSendToDone = async () => {
		setIsClicked(true)
		await sendOrderToDraft(itemsToOrder)
		let rec = {
			branchId: userBranches.length === 1 ? userBranches[0].BranchId : chosenBranch,
			sapakId: chosenSupplierId,
			date1: moment(new Date()).format('YYYY-MM-DD')
		}
		const aspkaDate = await getAspkaDate(rec)
		let rec1 = {
			// sapakid: chosenSupplierId,
			// orderdate: moment(new Date()).format('YYYY-MM-DD'),
			// BranchId: userBranches.length === 1 ? userBranches[0].BranchId : chosenBranch,
			// AspakaDate: aspkaDate
			SapakId: chosenSupplierId,
			OrderDate: moment(new Date()).format('YYYY-MM-DD'),
			BranchId: userBranches.length === 1 ? userBranches[0].BranchId : chosenBranch,
			AspakaDate: aspkaDate
		}
		await addOrder(rec1)
		setDisplayOrdersBySupplier(false)
		searchOrders()
	}

	return (
		<Modal isOpen={isOpen} toggle={toggleModal} className='modal-dialog-centered'>
			<ModalHeader isClicked={isClicked} onClose={toggleModal}>{formatMessage({ id: 'warning' })}</ModalHeader>
			<ModalBody>
				{formatMessage({ id: 'noValuePromoMessage' })}
			</ModalBody>
			<ModalFooter>
				<Button className='round' color='primary' disabled={isClicked} onClick={onSendToDone}>{formatMessage({ id: 'sendAnyWay' })}</Button>
				<Button className='round' color='primary' disabled={isClicked} outline onClick={toggleModal}>{formatMessage({ id: 'goBackToFix' })}</Button>
				{isClicked ? <Spinner /> : null}
			</ModalFooter>
		</Modal>
	)
}
