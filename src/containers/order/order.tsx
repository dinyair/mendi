import * as React from 'react';
import { Icon } from '@components';
import { config } from '@src/config';
import { Button, Col } from 'reactstrap';
import { useSelector, useDispatch } from 'react-redux';
import Products, { PRODUCT_TYPES } from '@src/components/products';
import InputText from '@src/components/input-text';
import ComboBox, { CheckBoxTree } from '@src/components/combo-box';
import { Accordion } from '@src/components/accordion';
import * as moment from 'moment';
import { getSuggestedOrder, getAspkaDate, getOrders, sendOrderToDraft } from './initial-state';
import { setStockOrder } from './services/actions';
import { Spinner } from 'reactstrap';
import { useIntl } from 'react-intl';
import { setOrder } from '@src/redux/actions/order/orderActions';
import { RootStore } from '@src/store';
import { setStateOrders } from '../stock/orders/services/actions';
import './order.scss';

// export interface OrderType {
// 	name: string;
// 	barcode: string;
// 	countInStock: number;
// 	imageSrc: string;
// 	promotionalOffer?: string;
// 	salePredication?: number;
// 	numberItems: number;
// 	shelfStockContent: number;
// 	packingFactor: number;
// 	amountOrder1: number;
// 	categoryName: string;
// 	promo_p?: any
// 	promo_k?: any
// 	promo_tdate?: any
// 	promo_fdate?: any
// 	defaultUnderZero?: any
// }

export const Order: any = (props: any) => {
	const { chosenSupplierId, chosenBranch, setIsOrderDataReady, isOrderDataReady, typeOfOrder, setFetchedSupplier } =
		props;

	const { formatMessage } = useIntl();
	const dispatch = useDispatch();
	const orderProducts = useSelector((state: RootStore) => state.stockOrdersOrder);

	const [productsForSum, setProductsForSum] = React.useState<any>();

	const [productsSum, setProductsSum] = React.useState<number>();
	const [promoSum, setPromoSum] = React.useState<number>();
	const [products, updateProducts] = React.useState<any>([]);
	const [series, updateSeries] = React.useState<any>([]);
	const [activeProductType, setActiveType] = React.useState(PRODUCT_TYPES.SPECIALS);

	const [displayAll, setDisplayAll] = React.useState<boolean>(false);

	const [categories, setCateogires] = React.useState<any[]>([]);
	const [checkedCategoryIds, updateCategoryIds] = React.useState(categories ? categories : []);
	let selectedCategories = categories ? categories.filter(c => checkedCategoryIds.includes(c)) : [];
	const [checkBoxClass, setCheckBoxClass] = React.useState<any>(
		'checkbox__checkbox combobox__drop-down_custom-checkbox cursor-pointer'
	);
	const [inputValue, setInputValue] = React.useState<any>('');
	const stockOrders = useSelector(({ stockOrders }: RootStore) => stockOrders);

	React.useEffect(() => {
		loadOrder();
	}, []);

	const loadOrder = async () => {
		let rec = {
			branchId: chosenBranch,
			sapakId: chosenSupplierId,
			date1: moment(new Date()).format('YYYY-MM-DD'),
			isDraft: typeOfOrder == 'draft' ? 1 : 0
		};
		const suggestedOrderJson = await getSuggestedOrder(rec);
		const supplyDate = await getAspkaDate(rec);
		setFetchedSupplier(chosenSupplierId);

		let dateNow: string = moment(new Date()).format('YYYY-MM-DD');

		if (suggestedOrderJson[0] && suggestedOrderJson[1] && suggestedOrderJson[2] && suggestedOrderJson[3]) {

			let checkIfHasReccomendation = suggestedOrderJson[0]
				.map((i: any) => {
					if (i.amountOrder1>0) return true;
					else return null;
				}).filter((i: any) => i);

			let checkIfHasPromo = suggestedOrderJson[0]
				.map((i: any) => {
					if (i.isPromo == 1) return true;
					else return null;
				}).filter((i: any) => i);

			let orderForState = suggestedOrderJson[0].map((i: any) => {
				let AmountMaraz = i.amountOrder1;
				let BranchId = chosenBranch;
				let AmountOrderOrg = i.AmountOrderOrg;
				let AmountOrder = i.amountOrder;
				let averageDaySum = i.sumofday;
				let Ariza = i.ariza;
				let BarCode = i.BarCode;
				let lastyitra = i.lastBalance;
				let madaf = i.shelfData;
				let newyitra = i.lastBalance;
				let sales = i.averageDay;
				let SapakId = chosenSupplierId;
				let GroupId = i.GroupId;
				let OrderDate = dateNow;
				let AspakaDate = supplyDate;
				return {
					AmountMaraz,
					BranchId,
					AmountOrderOrg,
					AmountOrder,
					averageDaySum,
					Ariza,
					BarCode,
					lastyitra,
					madaf,
					newyitra,
					sales,
					SapakId,
					GroupId,
					OrderDate,
					AspakaDate
				};
			});

			const getCategories = (): string[] =>
				suggestedOrderJson &&
				suggestedOrderJson[0] &&
				suggestedOrderJson[0].reduce((acc: any, curr: any) => {
					if (!acc.includes(curr.categoryName)) {
						return [...acc, curr.categoryName];
					}
					return acc;
				}, []);

			resetAllFields();
			if (typeOfOrder != 'draft') {
				await sendOrderToDraft(orderForState);
				let newDraft: any = [];
				let newStockOrders: any = {};
				for (const key of Object.keys(stockOrders)) {
					if (!newStockOrders[key]) newStockOrders[key] = [];
					let type = stockOrders[key];
					type.forEach((i: any) => {
						if (i.SapakId != chosenSupplierId) newStockOrders[key].push(i);
						else {
							newDraft.push({
								...i,
								subOrders: orderForState,
								ordersAmount: orderForState.filter((g: any) => g.AmountOrder > 0).length
							});
						}
					});
				}

				newStockOrders['pending'].push(...newDraft);
				setOrders(newStockOrders['new'], newStockOrders['pending'], newStockOrders['delete']);
			}
			if (!checkIfHasReccomendation.length) handleCheckBoxClick();
			setCateogires(getCategories());
			updateCategoryIds(getCategories());
			dispatch(setOrder(orderForState));
			setActiveType(!checkIfHasPromo.length ? PRODUCT_TYPES.PRODUCTS : PRODUCT_TYPES.SPECIALS);
			dispatch(setStockOrder(suggestedOrderJson[1], suggestedOrderJson[2], suggestedOrderJson[3]));
			updateProducts(!suggestedOrderJson[2].length ? suggestedOrderJson[1] : suggestedOrderJson[2]);
			setProductsForSum([suggestedOrderJson[1], suggestedOrderJson[2], suggestedOrderJson[3]]);
			updateSeries(suggestedOrderJson[3]);
			setIsOrderDataReady(true);
		} else {
			resetAllFields()
		}
	};
	const setOrders = (notDoneOrders: any[], draftOrders: any[], ordersDone: any[]) => {
		dispatch(setStateOrders(notDoneOrders, draftOrders, ordersDone));
	};
	const resetAllFields = () => {
		dispatch(setOrder([]));
		dispatch(setStockOrder([], [], []));
		setCateogires([]);
		setProductsForSum([[],[],[]]);
		updateProducts([]);
		updateSeries([]);
		setCheckBoxClass('checkbox__checkbox combobox__drop-down_custom-checkbox cursor-pointer');
	};
	const resetFields = (dontUpdateProducts?: boolean) => {
		updateSeries(orderProducts[2]);
		if (!dontUpdateProducts) {
			updateProducts(activeProductType == PRODUCT_TYPES.PRODUCTS ? orderProducts[0] : orderProducts[1]);
			if (inputValue)
				onSearch(
					false,
					inputValue,
					activeProductType == PRODUCT_TYPES.PRODUCTS ? PRODUCT_TYPES.PRODUCTS : PRODUCT_TYPES.SPECIALS
				);
		}
		setProductsForSum([orderProducts[0], orderProducts[1], orderProducts[2]]);
		updateCategoryIds(categories);
	};

	const setProducts = () => {
		resetFields(true);
		setActiveType(PRODUCT_TYPES.PRODUCTS);
		updateProducts(orderProducts[0]);
	};

	const setSpecials = () => {
		resetFields(true);
		setActiveType(PRODUCT_TYPES.SPECIALS);
		updateProducts(orderProducts[1]);
		if (inputValue) onSearch(false, inputValue, PRODUCT_TYPES.SPECIALS);
	};

	const onSearch = (e: any, inputValue?: string, type?: any) => {
		let products: any = type
			? type === PRODUCT_TYPES.PRODUCTS
				? orderProducts[0]
				: orderProducts[1]
			: activeProductType === PRODUCT_TYPES.PRODUCTS
			? orderProducts[0]
			: orderProducts[1];

		if ((e.currentTarget && e.currentTarget.value) || inputValue) {
			const value = e && e.currentTarget ? e.currentTarget.value : inputValue;
			const numbers = value.match(/\d+/g);
			const strings = value.match(/\D+/g);
			const regNumbers = numbers?.map(num => new RegExp(num, 'i'));
			const regStrings = strings?.map(str => new RegExp(str, 'i'));

			const newProducts = products.filter(({ name, barcode }) => {
				let hasProduct = !value;

				if (regNumbers) {
					hasProduct = regNumbers.some(reg => reg.test(`${barcode}`));
				}
				if (!hasProduct && regStrings) {
					hasProduct = regStrings.some(reg => reg.test(name));
				}
				return hasProduct;
			});
			const regProducts = orderProducts[0].filter(({ name, barcode }) => {
				let hasProduct = !value;

				if (regNumbers) {
					hasProduct = regNumbers.some(reg => reg.test(`${barcode}`));
				}
				if (!hasProduct && regStrings) {
					hasProduct = regStrings.some(reg => reg.test(name));
				}
				return hasProduct;
			});
			const promoProductsM = orderProducts[1].filter(({ name, barcode }) => {
				let hasProduct = !value;

				if (regNumbers) {
					hasProduct = regNumbers.some(reg => reg.test(`${barcode}`));
				}
				if (!hasProduct && regStrings) {
					hasProduct = regStrings.some(reg => reg.test(name));
				}
				return hasProduct;
			});
			const newSeries = orderProducts[2].map((item: any) => {
				const newSeriesProduct = item.itemsToOrder.regularSeries.filter(({ name, barcode }) => {
					let hasProduct = !value;

					if (regNumbers) {
						hasProduct = regNumbers.some(reg => reg.test(`${barcode}`));
					}
					if (!hasProduct && regStrings) {
						hasProduct = regStrings.some(reg => reg.test(name));
					}
					return hasProduct;
				});
				const newSeriesPromo = item.itemsToOrder.promoSeries.filter(({ name, barcode }) => {
					let hasProduct = !value;

					if (regNumbers) {
						hasProduct = regNumbers.some(reg => reg.test(`${barcode}`));
					}
					if (!hasProduct && regStrings) {
						hasProduct = regStrings.some(reg => reg.test(name));
					}
					return hasProduct;
				});
				return {
					...item,
					itemsToOrder: {
						promoSeries: newSeriesPromo,
						regularSeries: newSeriesProduct
					}
				};
			});

			updateProducts(newProducts);
			updateSeries(newSeries);

			setProductsForSum([regProducts, promoProductsM, newSeries]);
		} else {
			updateProducts(products);
			updateSeries(orderProducts[2]);
			setProductsForSum([orderProducts[0], orderProducts[1], orderProducts[2]]);
		}
	};

	const onChangeCategory = (checkedOptions: string[]) => {
		let products: any = activeProductType === PRODUCT_TYPES.PRODUCTS ? orderProducts[0] : orderProducts[1];

		const regProducts = orderProducts[0].filter((product: any) => {
			const res = checkedOptions.some(option => option === product.categoryName);
			return res && product;
		});
		const promoProductsM = orderProducts[1].filter((product: any) => {
			const res = checkedOptions.some(option => option === product.categoryName);
			return res && product;
		});
		const newProducts = products.filter((product: any) => {
			const res = checkedOptions.some(option => option === product.categoryName);
			return res && product;
		});
		const newSeries = orderProducts[2].map((item: any) => {
			const newSeriesProduct = item.itemsToOrder.regularSeries.filter((product: any) => {
				const res = checkedOptions.some(option => option === product.categoryName);
				return res && product;
			});
			const newSeriesPromo = item.itemsToOrder.promoSeries.filter((product: any) => {
				const res = checkedOptions.some(option => option === product.categoryName);
				return res && product;
			});
			return {
				...item,
				itemsToOrder: {
					promoSeries: newSeriesPromo,
					regularSeries: newSeriesProduct
				}
			};
		});
		updateProducts(newProducts);
		updateSeries(newSeries);
		updateCategoryIds(checkedOptions);
		setProductsForSum([regProducts, promoProductsM, newSeries]);
	};

	React.useEffect(() => {
		if (productsForSum && productsForSum[0] && productsForSum[1] && productsForSum[2]) {
			getSum();
			getPromoSum();
		} else {
			setPromoSum(0);
			setProductsSum(0);
		}
	}, [productsForSum, products, checkBoxClass]);

	const getPromoSum = () => {
		if (productsForSum) {
			let sumA: number = 0;
			productsForSum[2].forEach((item: any) => {
				let sortedSeriesPromo = item.itemsToOrder.promoSeries.reduce(function (sum: any, order: any) {
					return sum + 1;
				}, 0);
				sumA += sortedSeriesPromo;
			});
			sumA += products
				? products.reduce(function (sum: any, order: any) {
						if (order.isPromo) return sum + 1;
						else return sum;
				  }, 0)
				: 0;
			setPromoSum(sumA);
		} else {
			setPromoSum(0);
		}
	};

	const getSum = () => {
		let sumA: number = 0;
		if (productsForSum) {
			if (!displayAll) {
				productsForSum[2].forEach((item: any) => {
					let sortedOptions = item.itemsToOrder.regularSeries.reduce(function (sum: any, order: any) {
						if (order.amountOrder1 > 0) return sum + 1;
						else return sum + 0;
					}, 0);
					let sortedSeriesPromo = item.itemsToOrder.promoSeries.reduce(function (sum: any, order: any) {
						return sum + 1;
					}, 0);
					sumA += sortedOptions + sortedSeriesPromo;
				});
				sumA += productsForSum[0].reduce(function (sum: any, order: any) {
					if (order.amountOrder1 > 0) return sum + 1;
					else return sum + 0;
				}, 0);
				sumA += productsForSum[1].reduce(function (sum: any, order: any) {
					return sum + 1;
				}, 0);
			} else {
				productsForSum[2].forEach((item: any) => {
					let sortedOptions = item.itemsToOrder.regularSeries.reduce(function (sum: any, order: any) {
						return sum + 1;
					}, 0);
					let sortedSeriesPromo = item.itemsToOrder.promoSeries.reduce(function (sum: any, order: any) {
						return sum + 1;
					}, 0);
					sumA += sortedOptions + sortedSeriesPromo;
				});
				sumA += productsForSum[0].reduce(function (sum: any, order: any) {
					return sum + 1;
				}, 0);
				sumA += productsForSum[1].reduce(function (sum: any, order: any) {
					return sum + 1;
				}, 0);
			}
		}
		setProductsSum(sumA);
	};

	const handleCheckBoxClick = () => {
		resetFields();
		setTimeout(() => {
			setDisplayAll(!displayAll);
			if (!displayAll)
				setCheckBoxClass('checkbox__checkbox combobox__drop-down_custom-checkbox checked cursor-pointer');
			else setCheckBoxClass('checkbox__checkbox combobox__drop-down_custom-checkbox cursor-pointer');
		}, 5);
	};

	return (
		<div className="edit-order">
			{isOrderDataReady ? (
				<div>
					<div className="d-flex justify-content-between">
						<div style={{ width: '100%' }}>
							<div
								className={`d-flex min-width-70-rem align-items-center mr-auto${
									window.innerWidth < 700 ? 'flex-direction-column' : ''
								}`}
							>
								<div className={`d-flex ${window.innerWidth < 700 ? 'flex-direction-column' : ''}`}>
									<InputText
										noMarginBottom
										classNames="max-width-14-rem mb-05"
										searchIcon
										onKeyDown={setInputValue}
										onChange={onSearch}
									/>
									<div className="edit-order__filters d-flex">
										<ComboBox
											caption={
												checkedCategoryIds && checkedCategoryIds.length == categories.length
													? formatMessage({ id: 'allcategoires' })
													: checkedCategoryIds && checkedCategoryIds.length
													? checkedCategoryIds.toString()
													: formatMessage({ id: 'categories' })
											}
											mix="edit-order__filter mb-0"
										>
											{(onHide, childRef) => (
												<CheckBoxTree
													childRef={childRef}
													onChange={onChangeCategory}
													onHide={onHide}
													options={categories ? categories : []}
													checkedIds={checkedCategoryIds ? checkedCategoryIds : []}
												/>
											)}
										</ComboBox>
									</div>
								</div>

								<div
									onClick={handleCheckBoxClick}
									className={`checkbox__container combobox__drop-down_label cursor-pointer text-bold-600 ${
										window.innerWidth < 700 ? 'mr-auto mb-1 mt-1' : ''
									}`}
								>
									<span className={checkBoxClass} />
									{formatMessage({ id: 'allItems' })}
								</div>
							</div>
							<div className="mb-2">
								<Button
									className="mt-05 round mr-1 edit-order__product-type-btn"
									color="primary"
									outline
									onClick={setSpecials}
									active={activeProductType === PRODUCT_TYPES.SPECIALS}
								>
									{formatMessage({ id: 'order_sale' })}
								</Button>
								<Button
									className="mt-05 round edit-order__product-type-btn"
									color="primary"
									outline
									onClick={setProducts}
									active={activeProductType === PRODUCT_TYPES.PRODUCTS}
								>
									{formatMessage({ id: 'products' })}
								</Button>
							</div>
							{activeProductType === PRODUCT_TYPES.SPECIALS ? (
								<span className="edit-order__description mb-2">
									<span className="edit-order__info-icon">
										<Icon src={config.iconsPath + 'general/info-icon.svg'} />
									</span>
									<p>
										{formatMessage({ id: 'aTotalOf' })} {promoSum} {formatMessage({ id: 'promos' })}
										, {formatMessage({ id: 'for' })} {productsSum}{' '}
										{formatMessage({ id: 'diffrentProducts' })}
									</p>
								</span>
							) : (
								<span className="edit-order__description mb-2">
									<span className="edit-order__info-icon">
										<Icon src={config.iconsPath + 'general/info-icon.svg'} />
									</span>
									<p>
										{formatMessage({ id: 'aTotalOf' })} &nbsp;
										{productsSum}
										&nbsp;
										{formatMessage({ id: 'diffrentProducts' })}
									</p>
								</span>
							)}
						</div>
					</div>
					<Products displayAll={displayAll} orders={products} type={activeProductType} />

					{series && series.length > 0 && activeProductType === PRODUCT_TYPES.PRODUCTS ? (
						series.map((series: any) => {
							if (series.itemsToOrder.regularSeries.length === 0) return null;
							let display: boolean = false;
							series.itemsToOrder.regularSeries.forEach((item: any) => {
								if (item.amountOrder1 > 0) display = true;
							});
							if (display || displayAll) {
								return (
									<div className="suggestion-products mt-1">
										<Accordion
											defaultOpen
											buttonClassName="d-flex align-items-center"
											caption={series.seriesName}
										>
											<Products
												displayAll={displayAll}
												orders={series.itemsToOrder.regularSeries}
												type={activeProductType}
											/>
										</Accordion>
									</div>
								);
							} else {
								return null;
							}
						})
					) : (
						<div>
							{series && series.length > 0
								? series.map((series: any) => {
										if (series.itemsToOrder.promoSeries.length === 0) return null;
										return (
											<div className="suggestion-products mt-1">
												<Accordion
													defaultOpen
													buttonClassName="d-flex align-items-center"
													caption={series.seriesName}
												>
													<Products
														displayAll={displayAll}
														orders={series.itemsToOrder.promoSeries}
														type={activeProductType}
													/>
												</Accordion>
											</div>
										);
								  })
								: orderProducts[2].map((series: any) => {
										if (series.itemsToOrder.regularSeries.length === 0) return null;
										let display: boolean = false;
										series.itemsToOrder.regularSeries.forEach((item: any) => {
											if (item.amountOrder1 > 0) display = true;
										});
										if (display || displayAll) {
											return (
												<div className="suggestion-products mt-1">
													<Accordion
														defaultOpen
														buttonClassName="d-flex align-items-center"
														caption={series.seriesName}
													>
														<Products
															displayAll={displayAll}
															orders={series.itemsToOrder.regularSeries}
															type={activeProductType}
														/>
													</Accordion>
												</div>
											);
										} else {
											return null;
										}
								  })}
						</div>
					)}
				</div>
			) : (
				<Spinner />
			)}
		</div>
	);
};
