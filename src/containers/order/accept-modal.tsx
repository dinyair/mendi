import * as React from 'react';
import { Button } from 'reactstrap';
import { SupplierModal } from './supplier-modal';
import { DraftModal } from './draft-modal';
import { useIntl } from 'react-intl';
import { RootStore } from '@src/store';
import { useDispatch, useSelector } from 'react-redux';
import { sendOrderToDraft } from './initial-state';
import { Spinner } from 'reactstrap';
import { deleteOrderDateBranchSupply } from '@src/containers/stock/orders/initial-state';
import * as moment from 'moment';
import { Icon } from '@components';
import { config } from '@src/config';

interface Props {
	showModal: boolean;
	setDisplayOrdersBySupplier: (boolean: any) => void;
	searchOrders: () => void;
	chosenSupplierId: any;
	chosenBranch: any;
	typeOfOrder: any;
	userBranches?: any;
}

export const AcceptModal: React.FC<Props> = ({
	showModal,
	setDisplayOrdersBySupplier,
	searchOrders,
	chosenSupplierId,
	chosenBranch,
	typeOfOrder,
	userBranches
}) => {
	const [isOpenDraft, setStateDraftModal] = React.useState(false);
	const [isOpenSupplier, setStateSupplierModal] = React.useState(false);
	const [isClicked, setIsClicked] = React.useState(false);
	const itemsToOrder = useSelector((state: RootStore) => state._orderToPlace);

	const { formatMessage } = useIntl();

	const toggleDraftModal = () => {
		if (showModal) {
			setStateDraftModal(prevValue => !prevValue);
		}
	};

	const toggleSupplierModal = async () => {
		if (showModal) {
			setStateSupplierModal(prevValue => !prevValue);
		}
	};
	const onSendDraft = async () => {
		setIsClicked(true);
		if (typeOfOrder === 'draft') {
			let rec = {
				SapakId: chosenSupplierId,
				OrderDate: moment(new Date()).format('YYYY-MM-DD'),
				BranchId: userBranches.length === 1 ? userBranches[0].BranchId : chosenBranch
			};
			await deleteOrderDateBranchSupply(rec);

			// setTimeout(async () => {
				await sendOrderToDraft(itemsToOrder);
				setDisplayOrdersBySupplier(false);
				setIsClicked(false);
				searchOrders();
			// }, 5000);
		} else {
			await sendOrderToDraft(itemsToOrder);
			setDisplayOrdersBySupplier(false);
			setIsClicked(false);
			searchOrders();
		}
	};

	return (
		<div className="d-flex">
			<SupplierModal
				chosenBranch={chosenBranch}
				chosenSupplierId={chosenSupplierId}
				isOpen={isOpenSupplier}
				toggleModal={toggleSupplierModal}
				searchOrders={searchOrders}
				setDisplayOrdersBySupplier={setDisplayOrdersBySupplier}
			/>
			<DraftModal
				isOpen={isOpenDraft}
				toggleModal={toggleDraftModal}
				searchOrders={searchOrders}
				setDisplayOrdersBySupplier={setDisplayOrdersBySupplier}
			/>
			{window.innerWidth <= 1200 ? (
				<div className='d-flex justify-content-center align-items-center'>
					{/* <button disabled={isClicked} onClick={onSendDraft} className="border-turquoise no-oultine no-bg mr-1 p-05 border-radius-1rem">
						<Icon src={config.iconsPath + 'planogram/save-green.svg'} />
					</button> */}
					<button disabled={isClicked} onClick={toggleSupplierModal} className="border-white no-oultine bg-turquoise p-05 border-radius-1rem">
						<Icon src={config.iconsPath + 'general/send-icon-white.svg'} />
					</button>
				</div>
			) : (
				<div>
					{/* <Button disabled={isClicked} onClick={onSendDraft} className="round mr-1" color="primary" outline>
						{formatMessage({ id: 'saveAsDraft' })}
					</Button> */}
					<Button disabled={isClicked} onClick={toggleSupplierModal} className="round" color="primary">
						{formatMessage({ id: 'sendToSupplier' })}
					</Button>
				</div>
			)}

			{isClicked ? <Spinner className="ml-05 mt-auto mb-auto" /> : null}
		</div>
	);
};
