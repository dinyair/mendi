import { SET_STOCK_ORDER } from './constants';


export const setStockOrder = (products: any[], promoProducts: any[],productsSeries:any[]) => ({
	type: SET_STOCK_ORDER,
	payload: [products, promoProducts,productsSeries]
})
