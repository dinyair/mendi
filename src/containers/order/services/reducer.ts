import { Reducer } from 'redux';
import { SET_STOCK_ORDER } from './constants';



export const stockOrdersOrder = (
	state: any[] = [],
	{ type, payload }: { type: any; payload: any[] }): any[] => {
	switch (type) {
		case SET_STOCK_ORDER:
			return payload;
		default:
			return state;
	}
};

