import { getRequest, postRequest } from "@src/http";


export const getSuggestedOrder = async (rec: any) => {
    const resp = await getRequest<any>(`CalculateOrderNEW?branchId=${rec.branchId}&date1=${rec.date1}&sapakId=${rec.sapakId}&draft=${rec.isDraft}`)
    return resp
}
export const getAspkaDate = async (rec: any) => {
    const resp = await getRequest<any>(`AppDateAspaka?branchId=${rec.branchId}&date1=${rec.date1}&sapakId=${rec.sapakId}`)
    return resp
}

export const getGroup = async () => {
    const resp = await getRequest<any>(`getGroup`)
    return resp
}
export const getDegems = async () => {
    const resp = await getRequest<any>(`getDegems`)
    return resp
}
export const sendOrderToDraft = async (rec1: any) => {
    const resp = await postRequest<any>('AppInsertOrdersNew', {
        rec1
    });
    return resp
}
export const addOrder = async (rec1: any) => {
    const resp = await postRequest<any>('sapak-orders/confirmPrepearedOrdersNew', {
        BranchId: rec1.BranchId,
        OrderDate: rec1.OrderDate,
        SapakId: rec1.SapakId,
        AspakaDate: rec1.AspakaDate
    });
    return resp
}

export const postAllSuppliersOrders = async (date1: any, date2: any, branchIds: any[]) => {
    const resp = await postRequest<any>('sapak-orders/all', {
        AspakaDate1: date1,
        AspakaDate2: date2,
        BranchIds: branchIds
    });
    return resp
}
export const getOrders = async (rec: any) => {
    const resp = await getRequest<any>(`AppOrdersOne?date1=${rec.AspakaDate1}&branchId=${rec.branch}&sapakId=${rec.chosenSupplierId}`)
    return resp
}