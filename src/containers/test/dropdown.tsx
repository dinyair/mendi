import * as React from 'react';
import styled from '@emotion/styled';
import { List, AutoSizer } from 'react-virtualized';
import Select, { SelectRenderer } from 'react-dropdown-select';


import { fetchAndUpdateStore } from '@src/helpers/helpers';
import { RootStore } from '@src/store';
import { useIntl } from "react-intl"
import { useDispatch, useSelector } from 'react-redux';



export const convertArrayToOptions = (rows: any[], labelField: string, valueField: string) =>
    rows && rows.length ? rows.map((ele: any) => { return { label: ele[labelField], value: ele[valueField] } }) : [];


    interface Props {
        options: any[];
        value: any[];
        isLoading?: boolean;
        searchBy?: string;
        onChange: (values: any[]) => void;
        clearable?: boolean;
        disabled?: boolean;
        placeholder?: string;
        label  ? : string;
    }

export const DropDown: React.FunctionComponent<Props> = (props: Props) => {
    const { formatMessage } = useIntl();
    document.title = '';
    const { options, value, isLoading, searchBy, onChange, clearable, disabled, placeholder, label } = props

    React.useEffect(() => {
        console.log({ props })
    }, [props])




    const customDropdownRenderer = ({ props, state, methods }:any) => {
        const regexp = new RegExp(state.search, 'i');
        let searchBy:any = props.searchBy;
        let labelField:any = props.labelField;
        const items = props.searchable && options && options.length
            ? options.filter((item: any) => regexp.test(item[searchBy] || item[labelField]))
            : options;

        const filteredSelected = JSON.stringify(items) === JSON.stringify(props.values);
        // console.log(filteredSelected)
        

        return (
            <div>
                <SearchAndToggle color={props.color}>
                    <Buttons>
                        <div>Search and select:  items : {items.length}  values { props.values.length}</div>
                        {filteredSelected ? <Button className="clear" onClick={() => methods.clearAll(items)}>
                            Clear filter
                        </Button>
                            : methods.areAllSelected() ? (
                                <Button className="clear" onClick={methods.clearAll}>
                                    Clear all 
                                </Button>
                            ) : [...props.values,...items].length < 6000  ? (
                                <React.Fragment>
                                    <Button onClick={() => {
                                        console.log([...props.values,...items].length)
                                       methods.selectAll([...props.values,...items])                                       
                                    }}>{state.search ? 'Select filter' : 'Select all'}</Button>
                                </React.Fragment>
                            ) : null}
                    </Buttons>
                    <input
                        type="text"
                        value={state.search}
                        onChange={methods.setSearch}
                        placeholder="Type anything"
                    />
                </SearchAndToggle>
                <Items>
                    <AutoSizer style={{ height: '200px' }}>
                        {({ width, height }: any) => (
                            <StyledList
                                height={height}
                                rowCount={items.length}
                                rowHeight={40}
                                width={width - 2}
                                rowRenderer={({ index, style, key }: any) => {
                                    const checked = [...state.values].some((ele: { label: string, value: string }) => {
                                        return ele.value === items[index].value
                                    });
                                    return (
                                        <Item
                                            key={key}
                                            onClick={() => methods.addItem(items[index])}
                                            style={style}
                                        >
                                            <ItemLabel>{items[index].label}</ItemLabel>
                                            <span className={`custom-checkbox ${checked ? 'checked' : ''}`}></span>
                                            {/* <input
                                                type="checkbox"
                                                onChange={() => (() => methods.addItem(items[index]))}
                                                checked={[...state.values].some((ele: { label: string, value: string }) => {
                                                    return ele.value === items[index].value
                                                })}
                                            /> */}
                                        </Item>
                                    )
                                }}
                            />
                        )}
                    </AutoSizer>
                </Items>
            </div>
        );
    };


    
    const StyledList = styled(List)`
            overflow: auto;
            height: 200px;
            max-height: 200px;
            `;
    const SearchAndToggle = styled.div`
display: flex;
flex-direction: column;
input {
  margin: 10px 10px 0;
  line-height: 30px;
  padding: 0px 20px;
  border: 1px solid #ccc;
  border-radius: 3px;
  :focus {
    outline: none;
    border: 1px solid deepskyblue;
  }
}
`;

    const Items = styled.div`
overflow: auto;
min-height: 10px;
max-height: 200px;
`;

    const Item = styled.div`
            display: flex;
            padding: 0 10px;
            align-items: center;
            cursor: pointer;
            width: 480px;
            height: 40px;
            justify-content: flex-end;
            &:hover {
            background: #f2f2f2;
            }
            `;

    const ItemLabel = styled.div`
margin: 5px 10px;
`;

    const Buttons = styled.div`
display: flex;
justify-content: space-between;
& div {
  margin: 10px 0 0 10px;
  font-weight: 600;
}
`;

    const Button = styled.button`
background: none;
border: 1px solid #555;
color: #555;
border-radius: 3px;
margin: 10px 10px 0;
padding: 3px 5px;
font-size: 10px;
text-transform: uppercase;
cursor: pointer;
outline: none;
&.clear {
  color: tomato;
  border: 1px solid tomato;
}
:hover {
  border: 1px solid deepskyblue;
  color: deepskyblue;
}
`;

    return (
        <>
            <React.Fragment>
                {label && <label className="font-medium-1 pl-1 pr-1">{label}</label>}
                <Select
                    className=""
                    dropdownRenderer={customDropdownRenderer}
                    values={value}
                    multi
                    onChange={onChange}
                    searchBy={searchBy}
                    options={options}
                    direction={'rtl'}
                    loading={isLoading}
                    dropdownHandleRenderer={({ state }) => (
                        // if dropdown is open show "–" else show "+"
                        <svg height="20" width="20" viewBox="0 0 20 20" aria-hidden="true" focusable="false" className="css-6q0nyr-Svg"><path d="M4.516 7.548c0.436-0.446 1.043-0.481 1.576 0l3.908 3.747 3.908-3.747c0.533-0.481 1.141-0.446 1.574 0 0.436 0.445 0.408 1.197 0 1.615-0.406 0.418-4.695 4.502-4.695 4.502-0.217 0.223-0.502 0.335-0.787 0.335s-0.57-0.112-0.789-0.335c0 0-4.287-4.084-4.695-4.502s-0.436-1.17 0-1.615z"></path></svg>
                    )}
                    separator
                    clearable={clearable && value && value.length && value.length > 0 ? true : false}
                    disabled={disabled}
                    searchable
                    placeholder={placeholder}
                    style={{ height: '2rem' }}
                    color={'#e6e6e6'}
                    clearRenderer={({ props, state, methods }) => {
                        console.log({ props, state, methods })

                        return (
                            // if dropdown is open show "–" else show "+"
                            <svg onClick={() => methods.clearAll()} height="14" width="14" viewBox="0 0 20 20" aria-hidden="true" focusable="false" className="css-6q0nyr-Svg"><path d="M14.348 14.849c-0.469 0.469-1.229 0.469-1.697 0l-2.651-3.030-2.651 3.029c-0.469 0.469-1.229 0.469-1.697 0-0.469-0.469-0.469-1.229 0-1.697l2.758-3.15-2.759-3.152c-0.469-0.469-0.469-1.228 0-1.697s1.228-0.469 1.697 0l2.652 3.031 2.651-3.031c0.469-0.469 1.228-0.469 1.697 0s0.469 1.229 0 1.697l-2.758 3.152 2.758 3.15c0.469 0.469 0.469 1.229 0 1.698z"></path></svg>
                        )
                    }}
                />
            </React.Fragment>
        </>
    )
};

export default DropDown;

