import { fetchAndUpdateStore } from '@src/helpers/helpers';
import { RootStore } from '@src/store';
import * as React from 'react';
import { useIntl } from "react-intl"
import { useDispatch, useSelector } from 'react-redux';
import { convertArrayToOptions, DropDown } from './dropdown';
import './index.scss';

interface Props {
}

export const blankComponent: React.FunctionComponent = (props: Props) => {
	const { formatMessage } = useIntl();
	document.title = '';

	const dispatch = useDispatch();
	const catalogFull = useSelector((state: RootStore) => state._catalogFullData);
	const catalogFullNoArchive = useSelector((state: RootStore) => state._catalogFullDataNoArchive);
	const regCatalog = useSelector((state: RootStore) => state._catalogUpdated);
	const subSuppliers = useSelector((state: RootStore) => state._subSuppliers);
	const subGroup = useSelector((state: RootStore) => state._subGroups);
	// const degems = useSelector((state: RootStore) => state._models)
	const division = useSelector((state: RootStore) => state._divisions);
	const groups = useSelector((state: RootStore) => state._groups);
	const suppliers = useSelector((state: RootStore) => state._suppliers);
	const series = useSelector((state: RootStore) => state._series);
	const models = useSelector((state: RootStore) => state._models);
	const segments = useSelector((state: RootStore) => state._segments);

	const [selectedCatalog, setSelectedCatalog] = React.useState<any[]>([])
	const [selectedGroups, setSelectedGroups] = React.useState<any[]>([])
	const [selectedSubGroups, setSelectedSubGroups] = React.useState<any[]>([])
	const [selectedDivisions, setSelectedDivisions] = React.useState<any[]>([])
	const [selectedSuppliers, setSelectedSuppliers] = React.useState<any[]>([])
	const [selectedSeries, setSelectedSeries] = React.useState<any[]>([])
	const [selectedModels, setSelectedModels] = React.useState<any[]>([])
	const [selectedSegments, setSelectedSegments] = React.useState<any[]>([])


	React.useEffect(() => {
		getData()
	}, [])

	const getData = async () => {
		await fetchAndUpdateStore(dispatch, [
			{ state: catalogFullNoArchive, type: 'catalogFullDataNoArchive' },
			{ state: groups, type: 'groups' },
			{ state: subGroup, type: 'subGroups' },
			{ state: division, type: 'divisions' },
			{ state: suppliers, type: 'suppliers' },
			{ state: subSuppliers, type: 'subSuppliers' },
			{ state: series, type: 'series' },
			{ state: models, type: 'models' },
			{ state: segments, type: 'segments' },
		]);
	}


	React.useEffect(() => {
		console.log({ catalogFullNoArchive, groups, subGroup, division, suppliers, series, models, segments, })
	}, [catalogFullNoArchive])

	return (
		<>
			{
				[{
					options: convertArrayToOptions(catalogFullNoArchive, 'Name', 'BarCode'),
					value: selectedCatalog,
					onChange: setSelectedCatalog,
					searchBy: 'value',
					isLoading: catalogFullNoArchive && catalogFullNoArchive.length ? false : true,
					disabled: catalogFullNoArchive && catalogFullNoArchive.length ? false : true,
					placeholder: "Choose catalog"
				},
				{
					options: convertArrayToOptions(groups, 'Name', 'Id'),
					value: selectedGroups,
					onChange: setSelectedGroups,
					searchBy: 'value',
					isLoading: groups && groups.length ? false : true,
					disabled: groups && groups.length ? false : true,
					placeholder: "Choose groups"
				},
				{
					options: convertArrayToOptions(subGroup, 'Name', 'Id'),
					value: selectedSubGroups,
					onChange: setSelectedSubGroups,
					searchBy: 'value',
					isLoading: subGroup && subGroup.length ? false : true,
					disabled: subGroup && subGroup.length ? false : true,
					placeholder: "Choose subGroups"
				},
				{
					options: convertArrayToOptions(division, 'Name', 'Id'),
					value: selectedDivisions,
					onChange: setSelectedDivisions,
					searchBy: 'value',
					isLoading: division && division.length ? false : true,
					disabled: division && division.length ? false : true,
					placeholder: "Choose divisions"
				},
				{
					options: convertArrayToOptions(suppliers, 'Name', 'Id'),
					value: selectedSuppliers,
					onChange: setSelectedSuppliers,
					searchBy: 'value',
					isLoading: suppliers && suppliers.length ? false : true,
					disabled: suppliers && suppliers.length ? false : true,
					placeholder: "Choose suppliers"
				},
				{
					options: convertArrayToOptions(series, 'Name', 'Id'),
					value: selectedSeries,
					onChange: setSelectedSeries,
					searchBy: 'value',
					isLoading: series && series.length ? false : true,
					disabled: series && series.length ? false : true,
					placeholder: "Choose series"
				},
				{
					options: convertArrayToOptions(models, 'Name', 'Id'),
					value: selectedModels,
					onChange: setSelectedModels,
					searchBy: 'value',
					isLoading: models && models.length ? false : true,
					disabled: models && models.length ? false : true,
					placeholder: "Choose models"
				},
				{
					options: convertArrayToOptions(segments, 'Name', 'Id'),
					value: selectedSegments,
					onChange: setSelectedSegments,
					searchBy: 'value',
					isLoading: segments && segments.length ? false : true,
					disabled: segments && segments.length ? false : true,
					placeholder: "Choose segments"
				}
				].map((item) => {
					return (
						<div style={{ width: '40rem', marginBottom: '2rem' }}>
							<DropDown
								options={item.options}
								value={item.value}
								searchBy={item.searchBy}
								onChange={item.onChange}
								clearable
								isLoading={item.isLoading}
								disabled={item.disabled}
								placeholder={item.placeholder}
								label={item.placeholder}
							/>
						</div>
					)
				})
			}



			<div style={{ width: '20rem' }}>
				<DropDown
					options={convertArrayToOptions(catalogFullNoArchive, 'Name', 'BarCode')}
					value={selectedCatalog}
					searchBy={'value'}
					onChange={setSelectedCatalog}
					clearable
					isLoading={catalogFullNoArchive && catalogFullNoArchive.length ? false : true}
					disabled={catalogFullNoArchive && catalogFullNoArchive.length ? false : true}
					placeholder="Choose catalog"
				/>
			</div>
		</>
	)
};

export default blankComponent;

