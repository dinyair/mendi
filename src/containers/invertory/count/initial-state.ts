import { getRequest, postRequest } from "@src/http";


export const getDocuments = async (date1: any, date2: any) => {
    const resp = await postRequest<any>('tablet-api/return/documentsM1ByDate', {date1,date2});
    return resp
}

export const getDocumentDetails = async (id: number) => {
    const resp = await getRequest<any>('tablet-api/return/documentM1/'+id, {});
    return resp
}
