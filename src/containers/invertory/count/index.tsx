import * as React from 'react';
import * as moment from 'moment';

import { useDispatch, useSelector } from 'react-redux';
import { TabContent, TabPane, Row, Col, Button, } from 'reactstrap';
import { Tabs, Tab } from '@src/components/tabs';

import { useIntl } from 'react-intl';

import { fetchAndUpdateStore } from '@src/helpers/helpers';
import { AgGrid, CalendarDoubleInput } from '@src/components';
import { getDocumentDetails, getDocuments, } from './initial-state';
import { InvertoryCountRow } from './interfaces';
import { RootStore } from '@src/store';

export const InvertoryCount: React.FC = () => {
	const { formatMessage } = useIntl();

	const TABS = {
		PERFORMED: formatMessage({ id: 'performedCounts' }),
		SCHEDULED: formatMessage({ id: 'scheduledCounts' })
	};

	const [date1, setDate1] = React.useState<Date>(new Date())
	const [date2, setDate2] = React.useState<Date>(moment().subtract(1 , 'months').toDate())

	React.useEffect(() => {
		console.log(moment().subtract(1 , 'months').format('YYYY-MM-DDTHH:mm'))
	}, [])

	const [activeTab, setActiveTab] = React.useState(TABS.SCHEDULED);
	const [isDataReady, setIsDataReady] = React.useState<boolean>(false)
	const suppliers: any[] = useSelector((state: RootStore) => state._suppliers)
	const branches: any[] = useSelector((state: RootStore) => state._branches)
	const logo: any = useSelector((state: RootStore) => state._logo)
	const [fields, setFields] = React.useState<any>([
		{ text: "createdAt", type: 'DateTime', width: window.innerWidth * 0.03, minWidth: 50 },
		{ text: "listName", type: 'string', width: window.innerWidth * 0.03, minWidth: 50 },
		{ text: "branchesNumber", type: 'string', width: window.innerWidth * 0.03, minWidth: 50 },
		{ text: "itemsNumber", type: 'string', width: window.innerWidth * 0.03, minWidth: 50 },
		{ text: "divisonsNumber", type: 'string', width: window.innerWidth * 0.03, minWidth: 50, },
		{ text: "suppliersNumber", type: 'string', width: window.innerWidth * 0.03, minWidth: 50, },
		{ text: "nextCountDate", type: 'DateTime', width: window.innerWidth * 0.03, minWidth: 50, },
		{
			text: "actions", type: 'string', width: window.innerWidth * 0.001, minWidth: 1, noFilter: true,
			cellRenderer: 'IconRender', renderIcons: [
				{ actions: true },
				{
					type: 'edit', title: 'edit', onClick: (data: any) => {
						// printDocument(data)
					}, icon: 'edit.svg'
				},
			],
		},
	])
	const [rows, setRows] = React.useState<any>()
	document.title = formatMessage({id:'invertoryCounts'});
	const dispatch = useDispatch();
	const [loading, setLoading] = React.useState(false);

	React.useEffect(() => {
		fetchRedux()
	}, []);

	const fetchRedux = async () => {
		if (suppliers && suppliers.length && branches && branches.length && logo && logo.length) {
			fetchData()
		} else {
			await fetchAndUpdateStore(dispatch, [
				{ state: suppliers, type: 'suppliers' },
				{ state: branches, type: 'branches' },
				{ state: logo, type: 'logo' },
			])
		}

	}

	React.useEffect(() => {
		if (suppliers && suppliers.length && branches && branches.length && logo && logo.length) {
			fetchData()
		}
	}, [suppliers, branches, logo]);


	const fetchData = async () => {
		setIsDataReady(false)
		const signatures = await getDocuments(moment(date1).format('YYYY-MM-DD'), moment(date2).format('YYYY-MM-DD'))

		const rows: any[] = [];
		for (let index = 0; index < signatures.length; index++) {
			const signature = signatures[index];
			const supplier = suppliers.find((ele: any) => ele.Id === signature.SapakId);
			const branch = branches.find((ele: any) => ele.BranchId === signature.BranchId);
			const row: InvertoryCountRow = {
				certificateNumber: signature.MlayNum,
				BranchId: branch.BranchId,
				SupplierId: supplier.Id,
				BranchName: branch.Name,
				SupplierName: supplier.Name,
				amountOfItems: signature.CountItems,
				sumOfUnitsOrWeight: null,
				applicator: signature.Name,
				logo: logo,
				applicatorSignature:'',
				receiveDate: signature.CreatedAt
			}


			rows.push(row)
		}

		// console.log(rows)
		setRows(rows)

		setTimeout(() => {
			setIsDataReady(true)
		}, 1000);
	}





	return (
		<>

			<Col md="12" sm="12" className="mb-1-5 p-0 font-medium-4 text-bold-700 text-black d-flex">
				<span>{formatMessage({id:'inventoryCounts'})}</span>
			</Col>
			{(
				<div className="mb-1-5">
					<Tabs value={TABS.SCHEDULED} onChangTab={setActiveTab}>
						<Tab caption={TABS.PERFORMED} name={TABS.PERFORMED} />
						<Tab caption={TABS.SCHEDULED} name={TABS.SCHEDULED} />
					</Tabs>
				</div>
			)}

			<TabContent activeTab={activeTab}>
				<TabPane tabId={TABS.PERFORMED}>
					{/* <h5>blank</h5> */}
				</TabPane>


				<TabPane tabId={TABS.SCHEDULED}>
					<div className='d-flex justify-content-between' style={{ width: '75%', height: '39px' }}>
						<CalendarDoubleInput
							setDate1={setDate1}
							setDate2={setDate2}
							date2={date2}
							date1={date1}
							top={'5.4rem'}
						/>

						<Button onClick={() => fetchData()} className='round width-9-rem width-11-rem height-2-5-rem d-flex align-items-center justify-content-center' color='primary'>{formatMessage({ id: 'filter' })}</Button>
					</div>



					<div className="mt-2 " style={{ width: '75%' }}>
						<AgGrid
							defaultSortFieldNum={0}
							descSort
							id={'id'}
							gridHeight={'60vh'}
							translateHeader
							fields={fields}
							groups={[]}
							totalRows={[]}
							rows={rows}
							pagination
							resizable
							displayLoadingScreen={!isDataReady}
						/>
					</div>



				</TabPane>
			</TabContent>
		</>
	);
};

export default InvertoryCount;
