
export interface InvertoryCountRow {
	certificateNumber: number;
	BranchId:number;
	BranchName: string;
	SupplierId:number;
	SupplierName: string;
	receiveDate: Date;
	amountOfItems: number;
	sumOfUnitsOrWeight: string|null;
	applicator:string;
	applicatorSignature: string;
	logo:string;
}

