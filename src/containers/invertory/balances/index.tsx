import * as React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { RootStore } from '@src/store';
import { Button, Spinner } from 'reactstrap';
import { useIntl } from 'react-intl';
import { config } from '../../../config';
import { setSubGroups, setGroups } from '@src/redux/actions/groups/groupsAction';
import { AgGrid, CalendarSingleInput, GenericDrawer, Icon } from '@components';

import { AppUpdateMinimumItem, getInvertory, getInvertoryData } from './initial-state';
import InvertoryModalsData from './modalsData';
import { ModalTypes } from '@src/components/GenericModals/interfaces';
import { setCatalogUpdated } from '@src/redux/actions/catalog/catalogActions';
import { setUserBranches } from '@src/redux/actions/userBranches/userBranchesAction';
import { setNewSuppliers } from '@src/redux/actions/suppliers/supplierAction';
import AsyncSelect from 'react-select/async';
import * as moment from 'moment';
import { filterDataDrawer } from './drawerData';
import ExportExcel from '@src/components/ExportExcel/ExportExcel';

export const InvertoryBalance: React.FunctionComponent = () => {
	const dispatch = useDispatch();
	const { formatMessage } = useIntl();
	document.title = formatMessage({ id: 'invertoryBalances' });

	const subGroups = useSelector((state: RootStore) => state._subGroups);
	const groups = useSelector((state: RootStore) => state._groups);
	const suppliers = useSelector((state: RootStore) => state._suppliers);
	const catalog = useSelector((state: RootStore) => state._catalogUpdated);
	const userBranches = useSelector((state: RootStore) => state._userBranches);

	const [isLoading, setIsLoading] = React.useState<Boolean>(true);
	const [isLoadingData, setIsLoadingData] = React.useState<Boolean>(false);
	const [isDataReady, setIsDataReady] = React.useState<boolean>(false);
	const [firstLoad, setFirstLoad] = React.useState<number>(0);
	const [lastDayInInvertory, setLastDayInInvertory] = React.useState<any>(null);
	const [classes, setClasses] = React.useState<any[]>([]);
	const [modalType, setModalType] = React.useState<ModalTypes>(ModalTypes.none);
	const [tempData, setTempData] = React.useState<any>();
	const [selectedGroups, setSelectedGroups] = React.useState<any>([]);
	const [selectedSubGroups, setSelectedSubGroups] = React.useState<any>([]);
	const [inputValue, setInputValue] = React.useState<any>(null);
	const [rows, setRows] = React.useState<any>([]);
	const [originalRows, setOriginalRows] = React.useState<any>([]);
	const [selectedRow, setSelectedRow] = React.useState<any>([]);
	const [shownFilter, setStateFilterModal] = React.useState(false);
	const [defaultAboveGridFilter, setDefaultAboveGridFilter] = React.useState<any>();
	const [date1, setDate1] = React.useState<any>(new Date());
	const [chosenBranch, setChosenBranch] = React.useState<any>([]);
	const [chosenSupplier, setChosenSupplier] = React.useState<any>([]);
	const [chosenBarcode, setChosenBarcode] = React.useState<any>([]);
	const [noConsigment, setNoConsigment] = React.useState<boolean>(true);

	const [fields, setFields] = React.useState<any>([
		{
			text: 'branch',
			type: 'string',
			width: window.innerWidth * 0.01,
			minWidth: 100,
			cellRenderer: 'ClickableCell',
			clickableCells: {
				aboveGridFilter: true
			}
		},
		{
			text: 'BarCode',
			type: 'numberDisplayZero',
			width: window.innerWidth * 0.01,
			minWidth: 100,
			cellRenderer: 'ClickableCell',
			clickableCells: {
				aboveGridFilter: true
			}
		},
		{ text: 'Name', type: 'string', width: window.innerWidth * 0.01, minWidth: 150 },
		{
			text: 'SupplierName',
			type: 'string',
			width: window.innerWidth * 0.01,
			minWidth: 150,
			cellRenderer: 'ClickableCell',
			clickableCells: {
				aboveGridFilter: true
			}
		},
		{
			text: 'CalculatedIronInvertory',
			type: 'fixedAndLocaleNumber',
			width: window.innerWidth * 0.01,
			minWidth: 150
		},
		{ text: 'ManualIronInvertory', type: 'fixedAndLocaleNumber', width: window.innerWidth * 0.01, minWidth: 150 },
		{
			text: 'Date',
			type: 'DateTime',
			filter: 'agDateColumnFilter',
			width: window.innerWidth * 0.01,
			minWidth: 100
		},
		{ text: 'InvertoryBalance', type: 'fixedAndLocaleNumber', width: window.innerWidth * 0.01, minWidth: 110 },
		{ text: 'invertoryDays', type: 'fixedAndLocaleNumber', width: window.innerWidth * 0.01, minWidth: 150 },
		{ text: 'inventoryValue', type: 'fixedAndLocaleNumber', width: window.innerWidth * 0.01, minWidth: 150 },
		{
			text: 'WeeklyAverageSales',
			type: 'fixedAndLocaleNumber',
			width: window.innerWidth * 0.01,
			minWidth: 180,
			noFilter: true,
			cellRenderer: 'ClickableCell',
			clickableCells: {
				onClick: (data: any) => {
					setSelectedRow(data.data);
					toggleModal(ModalTypes.info);
				}
			}

			// cellRenderer: 'IconRender', renderIcons: [
			// 	{ actions: true },
			// 	{
			// 		type: 'WeeklyAverageSales', title: 'WeeklyAverageSales', disabled: 'averagesFlag', value: 'WeeklyAverageSales', onClick: (data: any) => {
			// 			setSelectedRow(data)
			// 			toggleModal(ModalTypes.info)
			// 		}, icon: 'averages.svg'
			// 	},
			// ],
		}
	]);

	const fetchData = async () => {
		setIsLoading(true);

		if (
			subGroups &&
			subGroups.length &&
			groups &&
			groups.length &&
			suppliers &&
			suppliers.length &&
			catalog &&
			catalog.length &&
			userBranches &&
			userBranches.length
		) {
			setIsLoading(false);
			console.log({ catalog });
		} else {
			const res = await getInvertory();
			dispatch(setUserBranches(res.branch));
			setLastDayInInvertory(res.lastDate);
			dispatch(setNewSuppliers(res.suppliers));
			dispatch(setCatalogUpdated(res.catalog));
			setClasses(res.classes);
			dispatch(setGroups(res.group));
			dispatch(setSubGroups(res.subGroup));
			setIsLoading(false);
		}
	};

	const selectAllOption = {
		value: '<SELECT_ALL>',
		label: formatMessage({ id: 'select_all' })
	};

	React.useEffect(() => {
		setTempData({
			...tempData,
			branch: chosenBranch
				? {
						key: 'branchId',
						cannotRemove: true,
						label: chosenBranch.length && chosenBranch[0].label,
						preText: chosenBranch.length && chosenBranch[0].label,
						data: chosenBranch.length ? chosenBranch : []
				  }
				: {}
		});
	}, [chosenBranch]);
	React.useEffect(() => {
		setTempData({
			...tempData,
			SupplierName: chosenSupplier
				? {
						key: 'supplierId',
						cannotRemove: true,
						label: chosenSupplier.length && chosenSupplier[0].label,
						preText: chosenSupplier.length && chosenSupplier[0].label,
						data: chosenSupplier.length ? chosenSupplier : []
				  }
				: {}
		});
	}, [chosenSupplier]);

	React.useEffect(() => {
		fetchData();
	}, []);

	const toggleModal = (newType: ModalTypes) => {
		modalType === ModalTypes.none ? setModalType(newType) : setModalType(ModalTypes.none);
	};

	const onChangeValue = (newValue: any, name: any, props: any): void => {
		console.log({ newValue });
		console.log({ name });
		switch (name) {
			case 'branch':
				// console.log('hrere',props,newValue,name)
				if (
					props &&
					props.e &&
					((props.e.action == 'deselect-option' && props.e.option.value == selectAllOption.value) ||
						props.e.action == 'clear')
				) {
					setTempData({ ...tempData, branch: {} });
					setChosenBranch([]);
				} else {
					if (newValue) {
						let selectAll = false;
						newValue.forEach((ele: any) => {
							if (ele.value == selectAllOption.value) selectAll = true;
						});
						if (selectAll) {
							let userBranchesFiltered = userBranches.filter(
								(b: any) => b.BranchType == 0 && b.Id != 9999
							);
							let allBranchesOptions = userBranchesFiltered.map((g: any) => {
								return {
									name: 'branch',
									key: 'branchId',
									dontFilter: false,
									cannotRemove: false,
									label: g.Name,
									value: g.Id,
									preText: formatMessage({ id: 'branch' })
								};
							});
							setChosenBranch([selectAllOption, ...allBranchesOptions]);
						} else {
							setTempData({
								...tempData,
								branch: newValue
									? {
											key: 'branchId',
											cannotRemove: true,
											label: newValue.length && newValue[0].label,
											preText: newValue.length && newValue[0].label,
											data: newValue.length ? newValue : []
									  }
									: {}
							});
							setChosenBranch(
								newValue.map((e: any) => {
									return {
										name: 'branch',
										key: 'branchId',
										dontFilter: false,
										cannotRemove: false,
										label: e.label,
										value: e.value,
										preText: formatMessage({ id: 'branch' })
									};
								})
							);
						}
					} else {
						setTempData({ ...tempData, branch: {} });
						setChosenBranch([]);
					}
				}
				break;
			case 'SupplierName':
				// console.log('hrere',props,newValue,name)
				if (
					props &&
					props.e &&
					((props.e.action == 'deselect-option' && props.e.option.value == selectAllOption.value) ||
						props.e.action == 'clear')
				) {
					setTempData({ ...tempData, SupplierName: {} });
					setChosenSupplier([]);
				} else {
					if (newValue) {
						setTempData({
							...tempData,
							SupplierName: newValue
								? {
										key: 'supplierId',
										cannotRemove: true,
										label: newValue.length && newValue[0].label,
										preText: newValue.length && newValue[0].label,
										data: newValue.length ? newValue : []
								  }
								: {}
						});
						setChosenSupplier(
							newValue.map((e: any) => {
								return {
									name: 'SupplierName',
									key: 'supplierId',
									dontFilter: false,
									cannotRemove: false,
									label: e.label,
									value: e.value,
									preText: formatMessage({ id: 'SupplierName' })
								};
							})
						);
					} else {
						setTempData({ ...tempData, SupplierName: {} });
						setChosenSupplier([]);
					}
				}
				break;
			case 'groups':
				console.log(newValue);
				setSelectedGroups(newValue);
				break;
			case 'subGroups':
				setSelectedSubGroups(newValue);
				break;
			case 'date':
				setDate1(newValue);
				break;
			// case 'barcode':
			// 	if (props && props.e && (props.e.action == 'deselect-option' && props.e.option.value == selectAllOption.value || props.e.action == 'clear')) {
			// 		setTempData({ ...tempData, 'barcode': {} })
			// 		setChosenBarcode([])
			// 	} else {
			// 		if (newValue) {
			// 			setTempData({ ...tempData, 'barcode': newValue ? { key: 'Id', cannotRemove: true, label: newValue.length && newValue[0].label, preText: newValue.length && newValue[0].label, data: newValue.length ? newValue : [] } : {} })
			// 			setChosenBarcode(newValue.map((e: any) => {
			// 				return { name: 'barcode', key: 'Id', dontFilter: false, cannotRemove: false, label: e.label, value: e.value, preText: formatMessage({ id: 'BarCode' }) }
			// 			}))
			// 		} else {
			// 			setTempData({ ...tempData, 'barcode': {} })
			// 			setChosenBarcode([])
			// 		}
			// 	}
			// 	break;
		}
	};

	const loadBranchOptions = (inputValue: string, callback: any) => {
		callback(filterBranch(inputValue));
	};

	const loadSupplierOptions = (inputValue: string, callback: any) => {
		if (inputValue.length >= 2) {
			callback(filterSupplier(inputValue));
		} else {
			callback();
		}
	};
	const loadCatalogOptions = (inputValue: string, callback: any) => {
		if (inputValue.length >= 2) {
			callback(filterCatalog(inputValue));
		} else {
			callback();
		}
	};

	const filterCatalog = (inputValue: string) => {
		const regex = /\d/;
		const doesContainNumber = regex.test(inputValue);
		const catalogFiltered = catalog.filter((i: any) => {
			return i.Name.includes(inputValue) || String(i.BarCode).includes(inputValue);
		});
		let catalogMap: any[] = [];
		catalogFiltered && catalogFiltered[0]
			? (catalogMap = catalogFiltered.map((option: any, index: number) => {
					return {
						index,
						value: option.BarCode,
						supplierId: option.SapakId,
						label: `${option.Name} ${option.BarCode}`,
						name: option.Name
					};
			  }))
			: [];

		if (doesContainNumber) {
			let x = catalogMap.sort((a, b) => a.value - b.value);
			x.sort;
		} else {
			catalogMap.sort((a, b) => a.label.length - b.label.length);
		}
		return catalogMap.slice(0, 220);
	};
	const filterSupplier = (inputValue: string) => {
		const supplieresFiltered = suppliers.filter((i: any) => i.Name.includes(inputValue));

		let suppliersMap: any[] = [];
		supplieresFiltered && supplieresFiltered[0]
			? (suppliersMap = supplieresFiltered.map((option: any, index: number) => {
					return { index, value: option.Id, label: option.Name };
			  }))
			: [];
		return suppliersMap;
	};

	const filterBranch = (inputValue: string) => {
		const branchesFiltered = userBranches.filter((i: any) => i.Name.includes(inputValue));
		let branchesMap: any[] = [];
		branchesFiltered && branchesFiltered[0]
			? (branchesMap = branchesFiltered.map((option: any, index: number) => {
					return { index, value: option.BranchId, label: option.Name };
			  }))
			: [];
		return branchesMap;
	};
	const productStyles = {
		container: (base: any) => ({
			...base
		}),
		option: (provided: any, state: any) => ({
			...provided,
			...provided,

			'&:hover': {
				backgroundColor: '#31baab'
			},
			color: state.isSelected ? '#ffffff' : '#1e1e20',
			backgroundColor: state.isSelected ? '#027b7a' : '#ffffff',
			borderBottom: '1px dotted lightgray',
			padding: 10
		}),
		control: (styles: any) => ({
			width: '15rem!important'
		})
	};
	const selectStyle = {
		container: (base: any) => ({
			...base
		}),
		option: (provided: any, state: any) => ({
			...provided,

			'&:hover': {
				color: '#ffffff',
				backgroundColor: '#31baab'
			},
			color: state.isSelected ? '#ffffff' : '#1e1e20',
			backgroundColor: state.isSelected ? '#027b7a' : '#ffffff',
			borderBottom: '1px dotted lightgray',
			padding: 10
		}),
		control: () => ({
			width: 250,
			height: 50
		})
	};

	const handleInputChange = (newValue: string) => {
		setInputValue(inputValue);
		return inputValue;
	};

	const handleDrilldownFilters = (data: any) => {
		switch (data.field) {
			case 'SupplierName':
				console.log('handle supllier change', data.data.supplierId, data.value);
				// setDefaultAboveGridFilter([selectedBranch ? selectedBranch.map((b: any) => {
				// return { ...b, cannotRemove: true }
				// }) : {},  ? chosenSupplier.map((s: any) => {
				// 	return { ...s, cannotRemove: true }
				// }) : {}])
				break;
			case 'BarCode':
				console.log('handle BarCode change', data.data.BarCode, data.value);
				break;
			case 'branch':
				console.log('handle branch change', data.data.branchId, data.value);
				break;
		}
	};

	const getData = async (advancedSearch?: boolean) => {
		setRows([]);
		setIsDataReady(false);
		setIsLoadingData(true);

		// setDefaultAboveGridFilter([])
		// const currentTime = moment(selectedDate).format()

		const rows: any[] = [];
		let fetchObject: {
			date: any;
			date2: any;
			groups?: [];
			subGroups?: [];
			barCodes: [];
			branches: [];
			suppliers: [];
			noConsigment?: boolean;
		} = {
			date: '',
			date2: '',
			groups: [],
			subGroups: [],
			barCodes: [],
			branches: [],
			suppliers: [],
			noConsigment: noConsigment
		};
		try {
			let userSaleBranches = userBranches.filter((b: any) => b.BranchType === 0);

			// console.log(userSaleBranches);

			let barCodes =
				chosenBarcode && chosenBarcode.length ? chosenBarcode.map((ele: any) => parseInt(ele.value)) : [];
			let newSuppliers =
				tempData && tempData.SupplierName && tempData.SupplierName.data
					? tempData.SupplierName.data.map((ele: any) => ele.value)
					: [];

			let newBranches =
				tempData && tempData.branch && tempData.branch.data
					? tempData.branch.data
							.filter((g: any) => g.value != selectAllOption.value)
							.map((ele: any) => ele.value)
					: [];

			if (newBranches.length == 0) newBranches = userSaleBranches.map((i: any) => i.Id);

			if (newSuppliers.length === 0 && barCodes.length > 0) {
				barCodes.forEach((barcode: number) => {
					let fullBarCode = catalog.find(ele => ele.BarCode === barcode);
					let supplier = suppliers.find((ele: any) => ele.Id === fullBarCode.SapakId);

					if (supplier && !newSuppliers.includes(supplier.Id)) newSuppliers.push(supplier.Id);
				});
			}

			fetchObject = {
				date: moment(date1 ? date1 : new Date()).format('YYYY-MM-DD'),
				date2: moment(date1 ? date1 : new Date()).format('YYYY-MM-DD'),
				groups:
					tempData && tempData.groups && tempData.groups.data
						? tempData.groups.data.map((ele: any) => ele.value)
						: [],
				subGroups:
					tempData && tempData.subGroups && tempData.subGroups.data
						? tempData.subGroups.data.map((ele: any) => ele.value)
						: [],
				barCodes: barCodes,
				branches: newBranches,
				suppliers: newSuppliers,
				noConsigment: noConsigment
			};
			if (!advancedSearch) {
				delete fetchObject.noConsigment;
				delete fetchObject.groups;
				delete fetchObject.subGroups;
			}

			const res = await getInvertoryData(fetchObject);
			// console.log({ res })

			const invertory = [...res.invertory];
			const minimumItems = [...res.minimumItems];
			const averages = [...res.averages];
			const stockDays = [...res.stockDays];

			let branches: any = {};
			for (let i = 0; i < userBranches.length; i++) {
				branches[userBranches[i].Id] = userBranches[i].Name;
			}

			// console.log(branches)
			// console.log({stockDays})

			for (let index = 0; index < invertory.length; index++) {
				const element = invertory[index];
				const average = averages.find(
					(ele: any) =>
						ele.BarCode === element.BarCode &&
						ele.branchId === element.branchId &&
						ele.supplierId === element.SupplierId
				);
				const minimumItem = minimumItems.find(
					(ele: any) => ele.BarCode === element.BarCode && ele.BranchId === element.branchId
				);
				const invertoryDays = stockDays.find(
					(ele: any) => ele.BarCode === element.BarCode && ele.BranchId === element.branchId
				);

				let ddarr = element.dd.split('#');
				let mlayMiniChange: any = undefined;
				let mlayMiniDouble: any = undefined;
				let mlayMiniID: any = undefined;
				let Shakele: any = undefined;

				if (minimumItem) {
					mlayMiniChange = minimumItem.mlayMiniChange;
					mlayMiniDouble = minimumItem.mlayMini;
					mlayMiniID = minimumItem.Id;
					Shakele = minimumItem.Shakele;

					if (Shakele == 0) {
						if (mlayMiniDouble != 0) {
							mlayMiniDouble = parseInt(mlayMiniDouble);
						}
					}
					if (Shakele == 1) {
						if (mlayMiniDouble != 0) {
							var num = parseFloat(mlayMiniDouble);
							mlayMiniDouble = num.toFixed(2);
						}
					}
					if (mlayMiniChange == '-1') {
						mlayMiniChange = '';
					}
				}

				let row = {
					BarCode: element.BarCode,
					Name: element.Name,
					Date: ddarr[0],
					invertoryDays: invertoryDays ? invertoryDays.DaysLeft : null,
					InvertoryBalance: Number(ddarr[1]),
					WeeklyAverageSales: average
						? Number(Number(average.TotAvg2 + average.TotAvg8 + average.TotAvg52) / 3).toFixed(2)
						: null,
					inventoryValue: 0,
					detail: average ? average.detail : null,
					id: `${element.BarCode} ${element.SupplierName} ${branches[element.branchId]}`,
					averagesFlag: average ? true : false,
					ManualIronInvertory: mlayMiniChange ? mlayMiniChange : null,
					CalculatedIronInvertory: mlayMiniDouble ? mlayMiniDouble : null,
					ManualIronInvertoryId: mlayMiniID ? mlayMiniID : null,
					SupplierName: element.SupplierName,
					branchId: element.branchId,

					supplierId: element.SupplierId,
					branch: branches[element.branchId]
				};
				(row.inventoryValue =
					element.Sum && element.Amount && row.InvertoryBalance > 0
						? (Number.parseFloat(element.Sum) / Number.parseFloat(element.Amount)) * row.InvertoryBalance
						: 0),
					rows.push(row);
			}
		} catch (error) {
			console.log(error);
		}

		setDefaultAboveGridFilter([
			chosenBranch
				? chosenBranch.map((b: any) => {
						return { ...b, cannotRemove: true, key: 'branchId' };
				  })
				: {},
			chosenSupplier
				? chosenSupplier.map((s: any) => {
						return { ...s, cannotRemove: true, key: 'supplierId' };
				  })
				: {},
			tempData.groups && tempData.groups.data
				? tempData.groups.data.map((b: any) => {
						return {
							...b,
							cannotRemove: true,
							key: 'groupId',
							dontFilter: true,
							preText: formatMessage({ id: 'groups' })
						};
				  })
				: {}
		]);

		setRows(rows);

		setTimeout(() => {
			setFirstLoad(firstLoad + 1);
			setIsDataReady(true);
			setIsLoadingData(false);
			setOriginalRows(rows);
		}, 6);
	};

	const toggleFilterModal = () => {
		setStateFilterModal(!shownFilter);
	};

	return (
		<>
			<div className="width-75-per">
				<div className="mb-1-5 p-0 font-medium-4 text-bold-700 text-black d-flex">
					{formatMessage({ id: 'invertoryBalances' })}
				</div>
				<div className="d-flex height-2-5-rem justify-content-between mb-2">
					<div className="d-flex">
						<CalendarSingleInput
							onChange={setDate1}
							calanderId={'aaaa'}
							inputId={'fdfff'}
							// setDate2={setDate2}
							date={date1}
							// date1={date1}
							top={'1.2rem'}
						/>

						{[
							{
								name: 'branch',
								isMulti: true,
								allowSelectAll: true,
								loadOptions: loadBranchOptions,
								text: formatMessage({ id: 'trending_branch' }),
								defaultOptions:
									userBranches && userBranches[0]
										? [
												selectAllOption,
												...userBranches
													.filter(ele => ele.BranchType == 0 && ele.Id != 9999)
													.map((option: any, index: number) => {
														return {
															index,
															value: option.BranchId,
															label: option.Name,
															name: 'branch'
														};
													})
										  ]
										: [],
								// value: selectedBranch,
								value:
									tempData && tempData.branch
										? tempData.branch.data && tempData.branch.data.length && tempData.branch.data
										: null,
								styles: selectStyle
							},
							{
								name: 'supplier',
								isMulti: true,
								allowSelectAll: true,
								text: formatMessage({ id: 'chose__supplier' }),
								loadOptions: loadSupplierOptions,
								// value: selectedSupplier,
								value:
									tempData && tempData.SupplierName
										? tempData.SupplierName.data &&
										  tempData.SupplierName.data.length &&
										  tempData.SupplierName.data
										: null,
								styles: selectStyle
							},
							{
								name: 'barcode',
								isMulti: true,
								allowSelectAll: true,
								text: formatMessage({ id: 'itemSearch' }),
								loadOptions: loadCatalogOptions,
								value: chosenBarcode,
								styles: productStyles
							}
						].map(item => {
							return (
								<div key={item.text} className="ml-1">
									<AsyncSelect
										name={item.name}
										isDisabled={
											suppliers && suppliers.length > 0 && userBranches && userBranches.length > 0
												? false
												: true
										}
										isMulti={item.isMulti}
										isClearable={true}
										styles={item.styles}
										value={item.value}
										loadOptions={item.loadOptions}
										defaultOptions={item.defaultOptions}
										onInputChange={handleInputChange}
										// onKeyDown={(e) => checkIfEnter(e)}
										onChange={(e: any, actionMeta: any) => {
											if (e) {
												const { action, option, removedValue } = actionMeta;
												if (actionMeta.name === 'supplier') {
													{
														setChosenSupplier(
															e.map((e: any) => {
																return {
																	name: 'SupplierName',
																	key: 'supplierId',
																	dontFilter: false,
																	cannotRemove: false,
																	label: e.label,
																	value: e.value,
																	preText: formatMessage({ id: 'supplier' })
																};
															})
														);
													}
												} else if (actionMeta.name === 'barcode') {
													setChosenBarcode(e);
												} else {
													if (
														action === 'select-option' &&
														option.value === selectAllOption.value
													) {
														let userBranchesFiltered = userBranches.filter(
															(b: any) => b.BranchType == 0 && b.Id != 9999
														);
														let allBranchesOptions = userBranchesFiltered.map((g: any) => {
															return {
																name: 'branch',
																key: 'branchId',
																dontFilter: false,
																cannotRemove: false,
																label: g.Name,
																value: g.Id,
																preText: formatMessage({ id: 'branch' })
															};
														});
														setChosenBranch([selectAllOption, ...allBranchesOptions]);
													} else {
														if (
															action === 'deselect-option' &&
															option.value === selectAllOption.value
														) {
															setChosenBranch([]);
														} else if (
															action === 'remove-value' &&
															actionMeta.removedValue &&
															actionMeta.removedValue.value === selectAllOption.value
														) {
															setChosenBranch([]);
														} else {
															setChosenBranch(
																e.map((e: any) => {
																	return {
																		name: 'branch',
																		key: 'branchId',
																		dontFilter: false,
																		cannotRemove: false,
																		label: e.label,
																		value: e.value,
																		preText: formatMessage({ id: 'branch' })
																	};
																})
															);
														}
													}
												}
											} else {
												if (actionMeta.name === 'supplier') setChosenSupplier(null);
												else if (actionMeta.name === 'branch') setChosenBranch(null);
												else {
													setChosenBarcode(null);
												}
											}
										}}
										closeMenuOnSelect={false}
										hideSelectedOptions={false}
										classNamePrefix="select"
										noOptionsMessage={() => formatMessage({ id: 'trending_productMinimumSearch' })}
										placeholder={item.text}
									/>
								</div>
							);
						})}
					</div>

					<div className="d-flex">
						<Button
							color="primary"
							// disabled={chosenSupplier&&!chosenSupplier.length ? true : chosenBranch&&!chosenBranch.length ? true :
							// 	isLoadingData ? true : false}
							disabled={
								(chosenSupplier && chosenSupplier.length) ||
								(tempData && chosenBarcode && chosenBarcode.length && !isLoadingData)
									? false
									: true
							}
							className="ml-1 round text-bold-400 d-flex justify-content-center align-items-center width-7-rem height-2-5-rem cursor-pointer btn-primary "
							onClick={() => {
								getData();
							}}
						>
							{formatMessage({ id: 'search' })}
						</Button>

						<button
							onClick={toggleFilterModal}
							className={`p-05 ml-1 no-outline ${
								(selectedSubGroups && selectedSubGroups.length > 0) ||
								(selectedGroups && selectedGroups.length > 0)
									? 'btn-primary'
									: 'bg-gray'
							} d-flex justify-content-center align-items-center no-border round text-bold-600 height-2-5 width-10-rem`}
						>
							<Icon
								src={'../' + config.iconsPath + 'general/filter.svg'}
								style={{ height: '0.5rem', width: '0.5rem' }}
							/>

							<span className="mr-1 ml-1">{formatMessage({ id: 'advancedSearch' })}</span>
						</button>

						{firstLoad > 0 && !isLoadingData ? (
							<div className="d-flex justify-content-center align-items-center ml-05 cursor-pointer">
								<ExportExcel
									fields={fields}
									data={rows}
									direction={'ltr'}
									translateHeader={true}
									filename={document.title}
									icon={
										<Icon
											src={config.iconsPath + 'table/excel-download.svg'}
											style={{ height: '0.5rem', width: '0.5rem' }}
										/>
									}
								/>{' '}
							</div>
						) : null}
					</div>
				</div>
			</div>

			{firstLoad > 0 ? (
				<div style={{ width: '75%' }} className="mr-5 mt-1">
					<AgGrid
						rowBufferAmount={100}
						defaultSortFieldNum={0}
						autoSelectedAboveGridFilter={defaultAboveGridFilter}
						// afterAboveFilterRemovedOrAdded={afterAboveFilterRemovedOrAddedNew}
						id={'id'}
						gridHeight={'64vh'}
						translateHeader
						fields={fields}
						groups={[]}
						totalRows={[]}
						rows={rows}
						cellGreenFields={['branch', 'BarCode', 'SupplierName']}
						successDark={'all'}
						pagination
						successLight={'all'}
						resizable
						cellLightBlueFields={['WeeklyAverageSales']}
						// displayLoadingScreen={isDataReady}
						// noDataReady={!isDataReady}
						displayLoadingScreen={isLoadingData ? true : false}
						onCellEditingStopped={async (data: any, field: any, newValue: any, oldValue: any) => {
							if (oldValue !== newValue && !isNaN(newValue)) {
								let obj = {
									Id: data.ManualIronInvertoryId,
									mlayMiniChange: newValue
								};
								await AppUpdateMinimumItem({ rec1: obj });
							}
						}}
						editFields={[{ field: 'ManualIronInvertory', flag: 'ManualIronInvertoryId' }]}
					/>
				</div>
			) : isLoadingData ? (
				<div className=" ml-5 mr-5 mb-2">
					<Spinner />
				</div>
			) : null}

			{shownFilter ? (
				<GenericDrawer
					{...{
						drawerType: 'TAB',
						caption: formatMessage({ id: 'advancedSearch' }),
						titleEditable: false,
						shown: shownFilter,
						toggleModal: toggleFilterModal,
						data: filterDataDrawer(
							userBranches,
							suppliers,
							groups,
							subGroups,
							catalog,
							onChangeValue,
							tempData,
							{ setDate1, date1 },
							noConsigment,
							setNoConsigment
						),
						saveChanges: () => {
							getData(true);
						},
						saveButtonText: 'search',
						isTabsHidden: true,
						cancelDisabled: false,
						saveDisabled: chosenSupplier && chosenSupplier.length && !isLoadingData ? false : true,
						loadingBySave: false,
						tempData: tempData,
						setTempData: setTempData,
						hideZeroFilters: true
					}}
				/>
			) : null}

			{modalType === ModalTypes.info ? (
				<InvertoryModalsData
					modalHandlers={() => {}}
					modalType={modalType}
					selectedRow={selectedRow}
					toggleModal={(type: ModalTypes) => toggleModal(type)}
				/>
			) : null}
		</>
	);
};

export default InvertoryBalance;
