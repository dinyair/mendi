import * as React from 'react';
import { Button, } from "reactstrap"

import { useIntl } from "react-intl"
import { GenericModals } from '@src/components/GenericModals/index';
import { ModalInterface, ModalsInterface, ModalTypes } from '@src/components/GenericModals/interfaces';
import { AgGrid } from '@src/components';


interface Props {
    modalType: ModalTypes,
    toggleModal: (type: ModalTypes) => void;
    modalHandlers: any;
    selectedRow: any;
}

export const InvertoryModalsData: React.FC<Props> = props => {
    const {
        toggleModal,
        modalHandlers,
        selectedRow,
        modalType
    } = props;

    const { formatMessage } = useIntl();
    const [fields, setFields] = React.useState<any>([])
    const [isLoading, setIsLoading] = React.useState<Boolean>(true)

    const [rows, setRows] = React.useState<any>([])


    React.useEffect(() => {

        const newRows: any[] = [];

        if (selectedRow) {
            selectedRow.detail.forEach((detail: any, index: number) => {
                console.log(detail)
                let obj = {
                    day: getDayTitle(detail.DayWeek),
                    date: detail.Date,
                    Period1: detail.Avg2,
                    Period2: detail.Avg8,
                    Period3: detail.Avg52,
                    Average: ((detail.Avg2 + detail.Avg8 + detail.Avg52) / 3),
                    id: `${index} ${detail.Date} ${detail.Avg2} ${detail.Avg8} ${detail.Avg52}`
                }

                newRows.push(obj)
            });
            console.log(newRows)
            setRows(newRows)
            setFields([
                { text: "day", type: 'string', width: window.innerWidth * 0.03, minWidth: 10, },
                { text: "date", type: 'Date',filter: 'agDateColumnFilter', width: window.innerWidth * 0.003, minWidth: 50, },
                { text: "Period1", type: 'fixedAndLocaleNumber', width: window.innerWidth * 0.03, minWidth: 10, },
                { text: "Period2", type: 'fixedAndLocaleNumber', width: window.innerWidth * 0.03, minWidth: 10, },
                { text: "Period3", type: 'fixedAndLocaleNumber', width: window.innerWidth * 0.03, minWidth: 10, },
                { text: "Average", type: 'fixedAndLocaleNumber', width: window.innerWidth * 0.03, minWidth: 10, },
            ])
            setIsLoading(false)
        }
    }, [selectedRow])


    const getDayTitle = (day: number) => {
        let title = '';
        switch (day) {
            case 1:
                title =  formatMessage({ id: 'sunday' })
                break;
            case 2:
                title =  formatMessage({ id: 'monday' })
                break;
            case 3:
                title =  formatMessage({ id: 'tuesday' })
                break;
            case 4:
                title =  formatMessage({ id: 'wednesday' })
                break;
            case 5:
                title =  formatMessage({ id: 'thursday' })
                break;
            case 6:
                title =  formatMessage({ id: 'friday' })
                break;
            case 7:
                title =  formatMessage({ id: 'saturday' })
                break;
        }
        return title;
    }



    const infoModal: ModalInterface = {
        classNames: 'width-35-per min-width-45-rem max-width-45-rem',
        isOpen: modalType === ModalTypes.info, toggle: () => toggleModal(ModalTypes.info),
        header: <></>,
        headerClassNames: 'pb-1 pt-2 ',
        body: (
            <>
                <div className="pt-0 d-flex flex-column align-items-center justify-content-center text-black">
                    <p className='font-medium-4 text-bold-700'>  {formatMessage({ id: 'AveragesTableByDays' })}</p>
                    <p className='font-medium-2 text-bold-500'> {formatMessage({ id: 'BarCode' })} : {selectedRow && selectedRow.BarCode ? selectedRow.BarCode : ''}</p>
                </div>
                <div className="d-flex align-items-center justify-content-center">
                    {isLoading || modalType !== ModalTypes.info ? null : <AgGrid
                        rowBufferAmount={100}
                        defaultSortFieldNum={0}
                        gridHeight={'42vh'}
                        translateHeader
                        fields={fields}
                        groups={[]}
                        totalRows={['Period1', 'Period2', 'Period3','Average']}
                        rows={rows}
                        displayLoadingScreen={false}
                    />}
                </div>
            </>
        ),
        bodyClassNames: 'pb-1-5 pt-0',
        footerClassNames: 'd-none',
        buttons: []
    }


    return (
        <GenericModals key={`modal`} modal={infoModal} />
    );
};



export default InvertoryModalsData;
