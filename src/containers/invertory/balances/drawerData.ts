import { GenericDrawerData } from "@src/components/edit-drawer";
import { FilterDrawerData } from "@src/components/filter-drawer";
import { InputTypes } from "@src/components/form-group";
import { useIntl } from "react-intl"


export const filterDataDrawer = (branches: any, suppliers: any, groups: any, subGroups: any, catalog: any, onChangeValue: any, tempData: any, date:any, noConsigment : boolean, setNoConsigment : any) => {
	const { formatMessage } = useIntl();
	let editDrawerData: GenericDrawerData = {
		itemName: '',
		tabs: [{
			title: '',
			tabId: '1',
			formGroup: [{
				title: '',
				formId: '1',
				fields: [
					{
						label: formatMessage({ id: "date" }),
						placeholder: formatMessage({ id: "date" }),
						onChangeFunc: (newSelected: any, name: string,props?:any) => { onChangeValue(newSelected, name,props) },
						value: new Date(),
						name: 'date',
						type: InputTypes.SINGLE_DATE,
						// dateData: date,
						datePosition:'below',
						options: [],
						columns: 2,
						isSelected: false,
						calanderId:'dateCalendarId1',
						inputId:'dateInputID1'

					},
					{
						label: formatMessage({ id: 'trending_branch' }),
						onChangeFunc: (newSelected: any, name: string,props?:any) => { onChangeValue(newSelected, name,props) },
						// defaultValue: tempData ? tempData['branch'] ? tempData['branch'] : '' : '',
						defaultValue: tempData ? tempData.branch && tempData.branch.data && tempData.branch.data : null,
						value: '',
						name: 'branch',
						type: InputTypes.SELECT,
						selectAllOption:true,
						isMulti:true,
						options: branches ? branches.filter((ele:any) => ele.BranchType == 0).map((branch: any) => {
							return { value: branch.Id, label: branch.Name }
						}) : []
					}, {
						label: formatMessage({ id: 'SupplierName' }),
						onChangeFunc: (newSelected: any, name: string,props?:any) => { onChangeValue(newSelected, name) },
						defaultValue: tempData ? tempData.SupplierName && tempData.SupplierName.data && tempData.SupplierName.data : null,
						// defaultValue: tempData ? tempData['supplier'] ? tempData['supplier'] : '' : '',
						value: '',
						// value: '',
						isMulti:true,
						
						name: 'SupplierName',
						type: InputTypes.SELECT,
						options: suppliers ? suppliers.map((supplier: any) => {
							return { value: supplier.Id, label: supplier.Name }
						}) : []
					},
					{
						label: formatMessage({ id: 'noConsignment' }),
						value: noConsigment ? true : false,
						onChangeFunc: () =>  setNoConsigment(!noConsigment) ,
						name: 'checkbox',
						type: InputTypes.CHECKBOX,
						columns: 2,
						classNames: 'mr-2 ml-2'
					},
					{
						label: formatMessage({ id: 'groups' }),
						onChangeFunc: (newSelected: any, name: string) => { onChangeValue(newSelected, name) },
						defaultValue: tempData ? tempData.groups && tempData.groups.data && tempData.groups.data : null,
						value: '',
						isMulti: true,
						name: 'groups',
						type: InputTypes.SELECT,
						options: groups ? groups.map((group: any) => {
							return { value: group.Id, label: group.Name }
						}) : [],
					},
					{
						label: formatMessage({ id: 'subGroups' }),
						onChangeFunc: (newSelected: any, name: string) => { onChangeValue(newSelected, name) },
						value: '',
						defaultValue: tempData ? tempData.subGroups && tempData.subGroups.data && tempData.subGroups.data : null,
						isMulti: true,
						name: 'subGroups',
						type: InputTypes.SELECT,
						options: subGroups ? subGroups.map((subGroups: any) => {
							return { value: subGroups.Id, label: subGroups.Name }
						}) : [],
					},
					// {
					// 	label: formatMessage({ id: "item" }),
					// 	value: formatMessage({ id: "itemSearch" }),
					// 	placeholder: formatMessage({ id: "itemSearch" }),
					// 	defaultValue: tempData ? tempData.barcode && tempData.barcode.data && tempData.barcode.data.map((i: any) => {
					// 		return {
					// 			value: String(i.value), label: i.label
					// 		}
					// 	}) : null,
					// 	name: 'barcode',
					// 	type: InputTypes.SELECT,
					// 	isMulti: true,
					// 	options: catalog.map((item: any,index:number) => {
					// 		return {
					// 			value: String(item.BarCode), label: `${item.Name} ${item.BarCode}`
					// 		}
					// 	}),
					// 	isSelected: false,
						
					// },
					// {
					// 	label: formatMessage({ id: 'date' }),
					// 	onChangeFunc: (newSelected: any, name: string) => { onChangeValue(newSelected, name) },
					// 	value: date ? date : null,
					// 	name: 'date',
					// 	type: InputTypes.SINGLE_DATE,
					// 	calanderId: 'calnder4',
					// 	inputId: 'input4',
					// },
				]
			},]
		}]
	}
	return editDrawerData
}
