
export enum CATALOG_TYPES {
    GROUPS = 'groups',
    SUBGROUPS = 'subGroups',
    DIVISIONS = 'divisions',
    SERIES = 'series',
    SEGMENTS = 'segments',
    MODELS = 'models',
    LAYOUTS = 'layouts'
}
export enum updateRoutes {
    divison = 'AppUpdateClasses',
    group = 'AppUpdateGroup',
    subGroup = 'AppUpdateSubGroup',
    layout = 'AppUpdateLayout',
    segment = 'UpdateSegment',
    model = 'UpdateModel',
    series = 'AppUpdateDegem'
}

export enum insertRoutes {
    divison = 'AppInsertClasses',
    group = 'AppInsertGroup',
    subGroup = 'AppInsertSubGroup',
    layout = 'AppInsertLayout',
    segment = 'InsertSegment',
    model = 'InsertModel',
    series = 'AppInsertDegems'
}

export enum deleteRoutes {
    divison = 'AppDeleteClasses',
    group = 'AppDeleteGroup',
    subGroup = 'AppDeleteSubGroup',
    layout = 'AppDeleteLayout',
    segment = 'DeleteSegment',
    model = 'DeleteModel',
    series = 'AppDeleteDegem'
}


