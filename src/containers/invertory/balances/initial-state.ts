import { getRequest, postRequest } from "@src/http";
import { deleteRoutes, insertRoutes, updateRoutes } from "./services/enums";


export const genericRequest = async (route: updateRoutes | insertRoutes | deleteRoutes, obj: any) => {
    const resp = await postRequest<any>(route, obj);
    return resp
}

export const getLastDateInMlay = async () => {
    const resp = await getRequest<any>(`NewLastDateInMlay`);
    return resp
}


export const getInvertory = async () => {
    const resp = await getRequest<any>(`getInvertory`);
    return resp
}

export const getInvertoryData = async (obj:any) => {
    const resp = await postRequest<any>(`getInvertoryData`, obj);
    return resp
}

export const getNewLastMlay = async (obj: any) => {
    const resp = await postRequest<any>( "/NewLastMlay", obj);
    return resp
}

export const getMlayAvg = async (obj: any) => {
    const resp = await postRequest<any>( "/getMlayAvg", obj);
    return resp
}

export const AppUpdateMinimumItem = async (obj: any) => {
    const resp = await postRequest<any>( "/AppUpdateMinimumItem", obj);
    return resp
}


export const getMinimumItems = async (obj: any) => {
    let route = '/AppMinimumItems?'
    if(obj.sapak){
        route += `sapak=${obj.sapak}`
    }
    if(obj.branch){
        route += `&branchId=${obj.branch}`
    }

    const resp = await getRequest<any>( route);
    return resp
}

