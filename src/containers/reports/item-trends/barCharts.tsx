import * as React from 'react';
import Chart from "react-apexcharts"
import * as moment from 'moment';
import { getChart1, getChart2 } from './initial-state'
import { useIntl, FormattedMessage } from "react-intl"

interface Props {
    readonly type: string,
    readonly categories: any,
    readonly tickAmount: number,
    readonly date1?: any,
    readonly chosenProduct: any,
    readonly chosenBranch: any,
    readonly checkboxReplacment: boolean,
    readonly updateChart: boolean,
    readonly displayDataLabels?: boolean,
    readonly onWeekSalesReady: (weekSales: number) => {}
}

export const BarChart: any = (props: Props) => {
    const { formatMessage } = useIntl();

    const [data, setData] = React.useState<any[]>([])
    const { type, categories, tickAmount, date1, chosenProduct, chosenBranch, checkboxReplacment, updateChart, displayDataLabels, onWeekSalesReady } = props
    const [newCategoires, setNewCategoires] = React.useState<any>(categories?categories:[])
    const sortDataByWeek = async () => {
        let startDate: any
        if (typeof date1 != 'string') startDate = moment(new Date(date1)).format('YYYY/MM/DD')
        else startDate = moment(new Date('2021/01/01')).format('YYYY/MM/DD')

        let rec = {
            barcode: String(chosenProduct),
            branchId: chosenBranch,
            date1: startDate,
            date2: null,
            sub_bar: checkboxReplacment
        }
        const postResponse = await getChart1(rec)
        const weeklyAvg = postResponse.map((day: any) => {
            return day.a
        })

        onWeekSalesReady(((weeklyAvg.reduce((a: number, b: number) => a + b))));
        setData(weeklyAvg)
    }
    const sortDataByMonth = async () => {
        let rec = {
            barcode: String(chosenProduct),
            branchId: chosenBranch,
            sub_bar: checkboxReplacment
        }
        const postResponse = await getChart2(rec)
        const monthly = postResponse.map((day: any) => {
            return day.s
        })
        setNewCategoires(postResponse.map((p:any)=>{
            let newMonthName = ''
            switch(p.m){
                case "ינואר":newMonthName=formatMessage({ id: 'January-short' }); break;
                case "פברואר":newMonthName=formatMessage({ id: 'February-short' }); break;
                case "מרץ":newMonthName=formatMessage({ id: 'March-short' }); break;
                case "אפריל":newMonthName=formatMessage({ id: 'April-short' }); break;
                case "מאי":newMonthName=formatMessage({ id: 'May-short' }); break;
                case "יוני":newMonthName=formatMessage({ id: 'June-short' }); break;
                case "יולי":newMonthName=formatMessage({ id: 'July-short' }); break;
                case "אוגוסט":newMonthName=formatMessage({ id: 'August-short' }); break;
                case "ספטמבר":newMonthName=formatMessage({ id: 'September-short' }); break;
                case "אוקטובר":newMonthName=formatMessage({ id: 'October-short' }); break;
                case "נובמבר":newMonthName=formatMessage({ id: 'November-short' }); break;
                case "דצמבר":newMonthName=formatMessage({ id: 'December-short' }); break;
            }
            return newMonthName
        }))
        setTimeout(() => {
        setData(monthly)
            
        }, 1);
    }


    React.useEffect(() => {
        if (updateChart) {
            if (type === 'week') sortDataByWeek()
            else sortDataByMonth()
        }
    }, [updateChart])

    const options = {
        chart: {
            background: '#f3f3f5',
            events: {
                dataPointSelection: (event: any, chartContext: any, config: any) => {
                    // this will print mango, apple etc. when clicked on respective datapoint
                    // alert(config.dataPointIndex)
                }
            },
            stacked: false,
            toolbar: { show: false }
        },

        plotOptions: {
            bar: {
                borderRadius: 7,

                dataLabels: {
                    position: 'top'
                },
                columnWidth: "80%"
            }
        },
        fill: {
            type: "gradient",
            gradient: {
                shade: 'dark',
                shadeIntensity: 0.5,
                gradientToColors: ['#44b1e3', '#4475e3'], // optional, if not defined - uses the shades of same color in series
                inverseColors: false,
                opacityFrom: 1,
                opacityTo: 1,
                stops: [0, 50, 100],
                colorStops: []
            }
        },
        dataLabels: {
            enabled: displayDataLabels,
            formatter: function (val: number) {
                if (val > 0) return String(val.toLocaleString())
                else return ''
            },
            style: {
                colors: ['white']
            },
        },
        grid: {
            borderColor: '#D8D8D8',
            padding: {
                left: 0,
                right: 0
            }
        },
        legend: {
            show: true,
            position: "top",
            horizontalAlign: "left",
            offsetX: 0,
            fontSize: "14px",
            fontWeight: 'bold',
            markers: {
                radius: 50,
                width: 10,
                height: 10
            }
        },
        xaxis: {
            labels: {
                formatter: function (val: number) {
                    return String(val.toLocaleString())
                },
                style: {
                    colors: 'black'
                }
            },
            axisTicks: {
                show: false
            },
            categories: newCategoires,
            axisBorder: {
                show: false
            }
        },
        yaxis: {
            tickAmount: tickAmount,
            labels: {
                formatter: function (val: number) {
                    let fixedVal = val.toFixed(0)
                    return String(fixedVal.toLocaleString())
                },
                offsetX: -30,
                style: {
                    color: '#000000'
                }
            }
        },
        tooltip: {
            custom: function({series, seriesIndex, dataPointIndex, w}) {
                return '<div class="arrow_box p-05 text-bold-700">' +
                  '<span>' + `${String(series[seriesIndex][dataPointIndex].toLocaleString(undefined, {
                    minimumFractionDigits: 0,
                    maximumFractionDigits: 0
                }))}` + '</span>' +
                  '</div>'
              },
            enabled: displayDataLabels ? false : true,
            x: { show: false },
            y: {
                show: true,
                formatter: (val: number) => {
                    return String(val.toLocaleString(undefined, {
                        minimumFractionDigits: 0,
                        maximumFractionDigits: 0
                    }))
                }
            }, z: {
                show: true,
                formatter: undefined,
                title: 'Size: '
            },
            style: {
              fontSize: '12px',
              fontFamily: undefined,
            },
            marker: {
                show: false,
            },
        }
    }
    const series = [{ name: '', data }]
    return (
        <Chart
            options={options}
            series={series}
            type="bar"
            height={'260vh'}
            width={"100%"}
        />
    );
};

export default BarChart;
