import * as React from 'react';
import * as moment from 'moment';
import { RootStore } from '@src/store';
import { Table, AgGrid } from '@components';
import { useSelector, useDispatch } from 'react-redux';
import { useIntl, FormattedMessage } from 'react-intl';
import { Spinner, Button } from 'reactstrap';
import { config } from '@src/config';
import { postTrending, getMatriza } from './initial-state';
import { CalendarDoubleInput } from '@components';
import { Icon } from '@components';
import { fetchAndUpdateStore } from '@src/helpers/helpers';
import AsyncSelect from 'react-select/async';
import BarChart from './barCharts';
import ExportExcel from '@src/components/ExportExcel/ExportExcel';
import classnames from 'classnames';
import './index.scss';
import SweetAlert from 'react-bootstrap-sweetalert';
import { Row, Col } from "reactstrap"

export const ItemTrends: React.FunctionComponent = () => {
	const { formatMessage } = useIntl();
	document.title = formatMessage({ id: 'trending_searchProduct' });

	const dispatch = useDispatch();

	const catalog = useSelector((state: RootStore) => state._catalogUpdated);
	const userBranches = useSelector((state: RootStore) => state._userBranches);

	const [productImage, setProductImage] = React.useState<any>();
	const [productBarCode, setBarCode] = React.useState<any>();
	const [searchedProduct, setSearchedProduct] = React.useState<any>(formatMessage({ id: 'trending_searchProduct' }));
	const [searchedProductName, setSearchedProductName] = React.useState<any>();
	let beginingOfYear: any = moment().startOf('year').format('YYYY/MM/DD');
	let beginingOfYearDateType = new Date(beginingOfYear);

	const [date1, setDate1] = React.useState<any>(beginingOfYearDateType);
	const [date2, setDate2] = React.useState<any>(new Date());

	const [inputValue, setInputValue] = React.useState<any>(null);
	const [chosenBranch, setChosenBranch] = React.useState<any>(null);
	const [chosenProduct, setChosenProduct] = React.useState<any>();
	const [checkboxReplacment, setCheckboxReplacment] = React.useState<any>(true);
	const [checkboxSeries, setCheckboxSeries] = React.useState<any>(false);

	const [showWeekly, setShowWeekly] = React.useState<boolean>(false);
	const [displayAllSales, setDisplayAllSales] = React.useState<any>(false);
	const [allSales, setAllSales] = React.useState<any>();
	const [weeklySales, setWeeklySales] = React.useState<any>();

	const [showProductModal, setShowProductModal] = React.useState<boolean>(false);

	const [loadSpinner, setLoadSpinner] = React.useState<boolean>(false);
	const [isGridDataReady, setIsGridDataReady] = React.useState<boolean>(false);
	const [defaultGrid, setDefaultGrid] = React.useState<boolean>(true);

	const [firstLoad, setFirstLoad] = React.useState<boolean>(true);

	const [updateChart, setUpdateChart] = React.useState<boolean>(false);

	const [rows, setRows] = React.useState<any>();
	const [fields, setFields] = React.useState<any>();

	React.useEffect(() => {
		fetchAndUpdateStore(dispatch, [
			{ state: catalog, type: 'catalog' },
			{ state: userBranches, type: 'userBranches' }
		]);
	}, []);
	React.useEffect(() => {

		console.log(date1,date2)
	}, [date1,date2]);

	React.useEffect(() => {
		setIsGridDataReady(false);
		setRows(null);

		if (defaultGrid) {
			setFieldsDefault();
		} else if (!defaultGrid && userBranches.length > 0) {
			setFieldsMatriza();
		}
		handleSearch();
	}, [defaultGrid]);

	const setFieldsDefault = () => {
		setFields([
			{
				text: 'day',
				type: 'string',
				width: window.innerWidth * 0.03,
				minWidth: 50,
				filter: 'agMultiColumnFilter',
				filterParams: {
					filters: [
						{ filter: 'agTextColumnFilter' },
						{ filter: 'agSetColumnFilter', filterParams: { suppressMiniFilter: true } }
					]
				}
			},
			{
				text: 'DateBuy',
				type: 'Date',
				width: window.innerWidth * 0.9,
				minWidth: 160,
				filter: 'agDateColumnFilter',
				cellRenderer: 'TooltipRender',
				customToolTip: [
					{
						if: 'flag_green',
						value: '1',
						customTooltipTitle: formatMessage({ id: 'inventoryCountDone' })
					}
				]
			},
			{ text: 'buy', type: 'fixedAndLocaleNumber', width: window.innerWidth * 0.1, minWidth: 100 },
			{
				text: 'return',
				type: 'fixedNumber',
				width: window.innerWidth * 0.1,
				minWidth: 100
			},
			{
				text: 'transfer',
				type: 'fixedAndLocaleNumber',
				width: window.innerWidth * 0.1,
				minWidth: 100,
				cellRenderer: 'TooltipRender',
				customToolTip: [
					{
						if: 'flag',
						value: '1',
						customTooltipTitle: formatMessage({ id: 'destroyDone' })
					}
				]
			},
			{
				text: 'sale',
				type: 'fixedAndLocaleNumber',
				width: window.innerWidth * 0.1,
				minWidth: 100,
				cellRenderer: 'TooltipRender',
				customToolTip: [
					{
						if: 'flag2',
						value: '1',
						customTooltipTitle: formatMessage({ id: 'AsOfNow' })
					},
					{
						if: 'virt_sale',
						value: '1',
						customTooltipTitle: formatMessage({ id: 'expectedSalesBased' })
					}
				]
			},
			{
				text: 'yitra',
				type: 'fixedAndLocaleNumber',
				totalRow: 'last',
				width: window.innerWidth * 0.1,
				minWidth: 100,
				cellRenderer: 'TooltipRender',
				customToolTip: [
					{
						if: 'flag_green',
						value: '1',
						customTooltipTitle: formatMessage({ id: 'inventoryCountDone' })
					}
				]
			},
			{
				text: 'price',
				totalRow: 'average',
				type: 'fixedAndLocaleNumber',
				width: window.innerWidth * 0.1,
				minWidth: 100
			}
		]);
	};
	const setFieldsMatriza = (showWeekly?: boolean) => {
		let matrizaDefColumns: any = [];
		if (showWeekly) {
			matrizaDefColumns = [
				{
					text: 'Date',
					type: 'DateRange',
					tooltip: false,
					pinned: document.getElementsByTagName('html')[0].dir === 'ltr' ? 'left' : 'right',
					filter: false,
					width: window.innerWidth * 0.1,
					minWidth: 150
				}
			];
		} else {
			matrizaDefColumns = [
				{
					text: 'day',
					type: 'string',
					pinned: document.getElementsByTagName('html')[0].dir === 'ltr' ? 'left' : 'right',
					filter: 'agMultiColumnFilter',
					filterParams: {
						filters: [
							{ filter: 'agTextColumnFilter' },
							{ filter: 'agSetColumnFilter', filterParams: { suppressMiniFilter: true } }
						]
					},
					width: window.innerWidth * 0.04,
					minWidth: 70
				},
				{
					text: 'DateBuy',
					type: 'FullDate',

					pinned: document.getElementsByTagName('html')[0].dir === 'ltr' ? 'left' : 'right',
					filter: false,
					width: window.innerWidth * 0.065,
					minWidth: 75
				}
			];
		}
		const branchesMap = userBranches.map((branch: any) => {
			let branchNameSize = branch.Name.length;
			return {
				text: branch.Name,
				branchId: branch.BranchId,
				type: 'matrizaNumber',
				width: window.innerWidth * 0.05,
				minWidth: 100 + branchNameSize
			};
		});
		setFields([...matrizaDefColumns, ...branchesMap]);
	};
	React.useEffect(() => {
		if (rows && defaultGrid) {
			let sales: any = 0;
			rows.forEach((row: any) => {
				if (row.sale) {
					sales += row.sale;
				}
			});
			const allSalesFullNumber = Math.floor(sales).toLocaleString();
			setAllSales(`${formatMessage({ id: 'trending_allsales' })} - ${allSalesFullNumber}`);
		} else {
			setAllSales(null);
		}
	}, [rows]);

	const filterBranch = (inputValue: string) => {
		const branchesFiltered = userBranches.filter((i: any) => i.Name.includes(inputValue));
		let branchesMap: any[] = [];
		branchesFiltered && branchesFiltered[0]
			? (branchesMap = branchesFiltered.map((option: any, index: number) => {
				return { index, value: option.BranchId, label: option.Name };
			}))
			: [];
		return branchesMap;
	};
	const filterCatalog = (inputValue: string) => {
		const regex = /\d/;
		const doesContainNumber = regex.test(inputValue);
		const catalogFiltered = catalog.filter((i: any) => {
			return i.Name.includes(inputValue) || String(i.BarCode).includes(inputValue);
		});
		let catalogMap: any[] = [];
		catalogFiltered && catalogFiltered[0]
			? (catalogMap = catalogFiltered.map((option: any, index: number) => {
				return {
					index,
					value: option.BarCode,
					label: `${option.Name} ${option.BarCode}`,
					name: option.Name
				};
			}))
			: [];

		if (doesContainNumber) {
			let x = catalogMap.sort((a, b) => a.value - b.value);
			x.sort;
		} else {
			catalogMap.sort((a, b) => a.label.length - b.label.length);
		}
		return catalogMap.slice(0, 220);
	};
	const handleInputChange = (newValue: string) => {
		setInputValue(inputValue);
		return inputValue;
	};
	const loadCatalogOptions = (inputValue: string, callback: any) => {
		if (inputValue.length >= 3) {
			callback(filterCatalog(inputValue));
		} else {
			callback();
		}
	};
	const loadBranchOptions = (inputValue: string, callback: any) => {
		callback(filterBranch(inputValue));
	};
	const branchStyles = {
		container: (base: any) => ({
			...base
		}),
		option: (provided: any, state: any) => ({
			...provided,

			'&:hover': {
				color: '#ffffff',
				backgroundColor: '#31baab'
			},
			color: state.isSelected ? '#ffffff' : '#1e1e20',
			backgroundColor: state.isSelected ? '#027b7a' : '#ffffff',
			borderBottom: '1px dotted lightgray',
			padding: 10
		}),
		control: () => ({
			width: 200,
			height: 50
		})
	};
	const productStyles = {
		container: (base: any) => ({
			...base
		}),
		option: (provided: any, state: any) => ({
			...provided,
			...provided,

			'&:hover': {
				backgroundColor: '#31baab'
			},
			color: state.isSelected ? '#ffffff' : '#1e1e20',
			backgroundColor: state.isSelected ? '#027b7a' : '#ffffff',
			borderBottom: '1px dotted lightgray',
			padding: 10
		}),
		control: (styles: any) => ({
			width: '15rem!important'
		})
	};

	const checkIfEnter = (e: any) => {
		if (e.which === 13) handleSearch();
	};
	const whatDayLetter = (date: any) => {
		const dateString = new Date(date);
		let day: any;

		switch (dateString.getDay()) {
			case 0:
				day = formatMessage({ id: 'Sunday' });
				break;
			case 1:
				day = formatMessage({ id: 'Monday' });
				break;
			case 2:
				day = formatMessage({ id: 'Tuesday' });
				break;
			case 3:
				day = formatMessage({ id: 'Wednesday' });
				break;
			case 4:
				day = formatMessage({ id: 'Thursday' });
				break;
			case 5:
				day = formatMessage({ id: 'Friday' });
				break;
			case 6:
				day = formatMessage({ id: 'Saturday' });
				break;
		}
		return day;
	};
	const handleSearch = () => {
		if (chosenProduct) {
			setLoadSpinner(true);
			setUpdateChart(true);
		}
		if (defaultGrid) {
			handleDefaultSearch();
		} else {
			handleMatrizaSearch();
		}
	};
	const handleMatrizaSearch = async () => {
		let date1matriza;
		let date2matriza;
		let recM;

		if (date1) date1matriza = moment(new Date(date1)).format('YYYY/MM/DD');
		else date1matriza = moment(new Date('2021/01/01')).format('YYYY/MM/DD');

		if (date2) date2matriza = moment(new Date(date2)).format('YYYY/MM/DD');
		else date2matriza = moment(new Date()).format('YYYY/MM/DD');

		if (chosenProduct) {
			recM = {
				barcode: String(chosenProduct),
				branchId: chosenBranch,
				date1: date1matriza,
				date2: date2matriza,
				sub_bar: checkboxReplacment,
				series: checkboxSeries
			};
			const matrizaRowData = await getMatriza(recM);

			let mDates: any[] = [];
			matrizaRowData.forEach((item: any) => {
				if (!mDates.includes(item.DateBuy)) mDates.push(item.DateBuy);
			});
			let newMatrizaRowData: any[] = [];
			mDates.forEach(DateBuy => {
				const whatDay = whatDayLetter(DateBuy);
				let day: any = {};
				day['day'] = whatDay;
				let dateData: any = {};
				matrizaRowData.forEach((info: any) => {
					if (info.DateBuy === DateBuy) {
						let branchName: any;
						userBranches.forEach((branch: any) => {
							if (branch.BranchId === info.BranchId) branchName = branch.Name;
						});
						dateData[branchName] = Math.floor(info.Amount);
					}
				});
				newMatrizaRowData.push({ DateBuy, ...day, ...dateData });
			});
			let sortedByWeeks: any = [];
			let sortedByWeeksFinal: any = [];

			if (showWeekly) {
				setFieldsMatriza(showWeekly);
				let weeklySales: any = {};
				newMatrizaRowData.forEach((row: any, index: number) => {
					const yearWeek = moment(row.DateBuy).week();
					if (!weeklySales[yearWeek]) weeklySales[yearWeek] = {};
					for (let [key, value] of Object.entries(row)) {
						if (typeof value != 'string') {
							if (weeklySales[yearWeek][key]) {
								weeklySales[yearWeek][key] = weeklySales[yearWeek][key] += value;
							} else {
								weeklySales[yearWeek] = { ...weeklySales[yearWeek], [key]: value };
							}
						} else if (key === 'DateBuy' && !weeklySales[yearWeek][key]) {
							weeklySales[yearWeek] = {
								...weeklySales[yearWeek],
								[key]: new Date(moment(value).format('MM.DD.YYYY'))
							};
						}
					}
				});
				// let weeklySalesLength = Object.keys(weeklySales).length;
				for (const [index, [key, value]] of Object.entries(Object.entries(weeklySales))) {
					let val: any = value;
					let endDate = moment().day('Friday').week(Number(key)).format('DD.MM.YY');
					let startDate = moment().day('Sunday').week(Number(key)).format('DD.MM.YY');
					if (Number(index) == 0) {
						startDate = moment(val['DateBuy']).format('DD.MM.YY');
					}

					let finalDateRange = `${endDate} - ${startDate}`;
					sortedByWeeks.push({ ...val, Date: finalDateRange });
				}

				sortedByWeeksFinal = [
					...sortedByWeeks.sort((a: any, b: any) => {
						return a.DateBuy - b.DateBuy;
					})
				];
			} else setFieldsMatriza();

			// 0526206724
			// 0526202255

			if (matrizaRowData.length) {
				setRows([]);
				setRows(!showWeekly ? newMatrizaRowData : sortedByWeeks);
				setFirstLoad(false);
				setLoadSpinner(false);
				setIsGridDataReady(true);
				setUpdateChart(false);
			} else {
				setRows([]);
				setLoadSpinner(false);
				setIsGridDataReady(true);
				setUpdateChart(false);
			}
			setSearchedProduct(`${formatMessage({ id: 'trending_searchProduct' })} - ${searchedProductName}`);
			setProductImage(config.productsImages + chosenProduct + '.jpg');
			setBarCode(chosenProduct);
		}
	};

	const toggleShowProductModal = () => {
		setShowProductModal(!showProductModal);
	};

	const handleDefaultSearch = async () => {
		let date1default;
		let date2default;
		let rec;

		if (date1) date1default = moment(new Date(date1)).subtract(1, 'days').format('YYYY/MM/DD');
		else date1default = moment(new Date('2021/01/01')).subtract(1, 'days').format('YYYY/MM/DD');

		if (date2) date2default = moment(new Date(date2)).format('YYYY-MM-DD');
		else date2default = moment(new Date()).format('YYYY-MM-DD');

		if (chosenProduct) {
			rec = {
				barCode: String(chosenProduct),
				branchId: chosenBranch,
				fromDate: date1default,
				toDate: date2default,
				subBar: checkboxReplacment,
				series: checkboxSeries
			};
			const postResponse = await postTrending(rec);
			const postMapped = postResponse.map((item: any) => {
				let DateBuy = item.DateBuy;
				if (!DateBuy.includes('06:00:00')) {
					DateBuy = item.DateBuy;
					let newDateBuy = new Date(moment(item.DateBuy).format('YYYY-MM-DD HH:mm:ss'));
					newDateBuy.setHours(7);
					DateBuy = newDateBuy;
				}
				let buy = item.buy ? item.buy : 0;
				let sale = item.sale ? item.sale : 0;
				let yitra = item.yitra ? item.yitra : 0;
				let transfer = item.transfer ? item.transfer : 0;
				let price = item.price ? item.price : 0;
				let day = formatMessage({ id: item.day });
				return {
					...item,
					day,
					buy,
					sale,
					yitra,
					transfer,
					price,
					DateBuy,
					return: item.return ? item.return : 0
				};
			});
			console.log({postResponse})
			if (postResponse.length) {
				setRows([]);
				setRows(postMapped);
				setFirstLoad(false);
				setLoadSpinner(false);
				setIsGridDataReady(true);
				setUpdateChart(false);
			} else {
				setRows([]);
				setLoadSpinner(false);
				setIsGridDataReady(true);
				setUpdateChart(false);
			}

			setSearchedProduct(`${formatMessage({ id: 'trending_searchProduct' })} - ${searchedProductName}`);

			setProductImage(config.productsImages + chosenProduct + '.jpg');
			setBarCode(chosenProduct);
			setDisplayAllSales(true);
		}
	};
	const noImg = () => {
		let imgRef: any = document.getElementById(String(productImage))
		if (imgRef) imgRef.src = `${config.imagesPath}noImg/noimg.png`
	}

	return (
		<>
			<Row className="d-flex itemTrends" key={'itemtrends'}>
				
				<Col md="12" lg={!firstLoad ? "7" : "12"}
					className={classnames('width-100-per gridFather', {
						// 'width-75-per': defaultGrid,
						// 'width-95-per': !defaultGrid
					})}
				>
					{!firstLoad ? (
						<div className="d-flex-column mb-08">
							<div className="d-flex ml-05">
								<div>
									<h2 className="mr-2 align-self-center text-bold-600 mb-0">{searchedProduct}</h2>
									<span className="text-bold-700">
										{formatMessage({ id: 'BarCode' })}: {productBarCode}
									</span>
								</div>
								<img
									onClick={toggleShowProductModal}
									className="productImage cursor-pointer"
									src={productImage}
									id={String(productImage)} 
									onError={noImg} 
								/>
							</div>
						</div>
					) : (
						<div className="d-flex-column mb-08">
							<div className="d-flex ml-05">
								<div>
									<h2 className="mr-2 align-self-center text-bold-600 mb-0">{searchedProduct}</h2>
								</div>
							</div>
						</div>
					)}

					{showProductModal ? (
						<SweetAlert
							custom
							showCloseButton
							title=""
							showCancel={false}
							showConfirm={false}
							onCancel={toggleShowProductModal}
							onConfirm={() => { }}
							customClass={'width-10-per min-width-25-rem'}
						>
							<h2 className="text-bold-600">{searchedProductName}</h2>
							<h3 className="mb-3">
								{formatMessage({ id: 'BarCode' })}: {productBarCode}
							</h3>
							<img className="productImageModal mb-2" src={productImage} alt="" />
						</SweetAlert>
					) : null}

					<div className="flex-wrap width-100-per min-height-4-vh mb-2-vh d-flex align-items-center" >
						<CalendarDoubleInput
							setDate1={setDate1}
							setDate2={setDate2}
							date2={date2}
							date1={date1}
							top={'0'}
						/>

						{[
							{
								name: 'branch',
								isMulti: false,
								allowSelectAll: false,
								loadOptions: loadBranchOptions,
								text: formatMessage({ id: 'trending_branch' }),
								defaultOptions:
									userBranches && userBranches[0]
										? userBranches.map((option: any, index: number) => {
											return { index, value: option.BranchId, label: option.Name };
										})
										: [],
								onChange: null,
								styles: branchStyles
							},
							{
								name: 'product',
								isMulti: false,
								allowSelectAll: false,
								text: formatMessage({ id: 'trending_product' }),
								loadOptions: loadCatalogOptions,
								defaultOptions: false,
								styles: productStyles
							}
						].map(item => {
							return (
								<div key={item.text} className="ml-1">
									<AsyncSelect
										name={item.name}
										isDisabled={
											catalog && catalog.length > 0 && userBranches && userBranches.length > 0
												? loadSpinner
													? true
													: false
												: true
										}
										isClearable={true}
										styles={item.styles}
										loadOptions={item.loadOptions}
										defaultOptions={item.defaultOptions}
										onInputChange={handleInputChange}
										onKeyDown={e => checkIfEnter(e)}
										onChange={(e: any, actionMeta: any) => {
											if (e) {
												if (actionMeta.name === 'product') {
													setChosenProduct(e.value);
													setSearchedProductName(e.name);
												} else {
													setChosenBranch(e.value);
												}
											} else {
												if (actionMeta.name === 'product') setChosenProduct(null);
												else setChosenBranch(null);
											}
										}}
										classNamePrefix="select"
										noOptionsMessage={() => formatMessage({ id: 'trending_productMinimumSearch' })}
										placeholder={item.text}
									/>
								</div>
							);
						})}
						{[
							{
								text: formatMessage({ id: 'trending_replament' }),
								isActive: checkboxReplacment,
								type: 'checkbox',
								onChange: () => setCheckboxReplacment(!checkboxReplacment)
							},
							{
								text: formatMessage({ id: 'trending_series' }),
								// grid: 'matriza',
								type: 'checkbox',
								isActive: checkboxSeries,
								onChange: () => setCheckboxSeries(!checkboxSeries)
							},
							{
								text: formatMessage({ id: 'trending_showweekly' }),
								grid: 'matriza',
								isActive: showWeekly,
								type: 'checkbox',
								onChange: () => setShowWeekly(!showWeekly)
							}
						].map(item => {
							if (item.grid === 'matriza' && defaultGrid) return null;
							let className: string = 'checkBoxBorder cursor-pointer';
							if (item.isActive) className = 'checkBoxSelected checkBoxBorder cursor-pointer';
							return (
								<div
									key={item.text}
									className="elipsis d-flex justify-content-center align-items-center ml-2"
									onClick={item.onChange}
								>
									<div className={className}></div>
									<label className="mr-05 font-medium-2 text-bold-500" key={item.text}>
										{item.text}
									</label>
								</div>
							);
						})}
						<div className="divider ml-1 mr-1"></div>

						{/* <div onClick={handleSearch}

						className={classnames("bg-turquoise height-2-5-rem d-flex justify-content-center align-items-center width-7-rem position-relative font-medium-1 cursor-pointer py-05 border-2 px-2 text-white mr-05 border-radius-2rem", {
							"border-turquoise": false

						})}>
						{<FormattedMessage id="trending_sortButton" />}
					</div> */}
						<Button
							color="primary"
							disabled={catalog && catalog.length > 0 && chosenProduct && !loadSpinner ? false : true}
							className="ml-1 round text-bold-400 d-flex justify-content-center align-items-center width-7-rem height-2-rem cursor-pointer btn-primary "
							onClick={handleSearch}
						>
							<FormattedMessage id="trending_sortButton" />
						</Button>
						{!firstLoad && (
							<div className="d-flex justify-content-center align-items-center ml-05 cursor-pointer">
								<ExportExcel
									fields={fields}
									data={rows}
									direction={'ltr'}
									translateHeader={true}
									filename={defaultGrid ? 'item-tracking-report' : 'item-tracking-report-matric'}
									icon={<Icon src={config.iconsPath + 'table/excel-download.svg'} />}	/>
							</div>
						)}

						{!firstLoad && (
							<div className="d-flex ml-auto">
								{[
									{
										text: 'trending_movments',
										isSelected: defaultGrid,
										onClick: () => setDefaultGrid(true)
									},
									{
										text: 'trending_matriza',
										isSelected: !defaultGrid,
										onClick: () => setDefaultGrid(false)
									}
								].map((button: any) => (
									<div
										onClick={button.onClick}
										key={button.text}
										className={classnames(
											'bg-gray position-relative font-small-3 cursor-pointer border-2 px-2 text-black text-bold-700 mr-05 border-radius-2rem gridSwitchButton',
											{
												'border-turquoise': button.isSelected
											}
										)}
									>
										{<FormattedMessage id={button.text} />}
									</div>
								))}
							</div>
						)}
					</div>

					{loadSpinner ? (
						<div className="mt-1-vh ml-05">
							<Spinner />
						</div>
					) : (
						<div className="mt-1-5vh">
							{defaultGrid ? (
								<div>
									{rows ? (
										<AgGrid
										noSort
											resizable
											defaultSortFieldNum={1}
											rowBufferAmount={55}
											gridHeight={'75vh'}
											floatFilter
											translateHeader
											fields={fields}
											groups={[]}
											totalRows={['buy', 'sale', 'yitra', 'transfer', 'return', 'price']}
											rows={rows}
											checkboxFirstColumn={false}
											cellRedFields={['transfer']}
											danger={'flag'}
											cellGreenFields={['DateBuy', 'yitra']}
											successDark={'flag_green'}
											yellowField={'sale'}
											yellowFlag={'virt_sale'}
											blueField={'sale'}
											blueFlag={'flag2'}
										/>
									) : null}
								</div>
							) : (
								<div>
									{rows ? (
										<AgGrid
											noSort
											resizable
											rowBufferAmount={50}
											defaultSortFieldNum={1}
											gridHeight={'76vh'}
											translateHeader
											floatFilter
											fields={fields}
											groups={[]}
											rows={rows}
											cellBgRedMatriza
											checkboxFirstColumn={false}
											totalRows={userBranches.map((branch: any) => {
												return branch.Name;
											})}
										/>
									) : null}
								</div>
							)}
						</div>
					)}
				</Col>
				{defaultGrid && isGridDataReady && !loadSpinner && (
					<Col md="12" lg="4" className="charts diFlex  min-width-25-rem">
						<div className="ml-1-5 aboveChartsTitle d-flex justify-content-end mt-auto">
							<div className="width-100-per d-flex flex-direction-row mt-auto">
								{displayAllSales && defaultGrid ? (
									<h3 className="allSales text-bold-700">{allSales}</h3>
								) : null}
								{displayAllSales && defaultGrid ? (
									<h3 className="text-bold-700 ml-1">{weeklySales}</h3>
								) : null}
							</div>
						</div>
						<div className="barChartsContainer">
							<div className="bg-gray chartFather align-items-center">
								<h3 className="mr-auto text-bold-700 ml-1">
									{formatMessage({ id: 'trending_avg-sales-days' })}
								</h3>
								<BarChart
									displayDataLabels
									updateChart={updateChart}
									type={'week'}
									date1={date1 ? date1 : moment(new Date('2021/01/01')).format('YYYY/MM/DD')}
									chosenProduct={chosenProduct}
									chosenBranch={chosenBranch}
									checkboxReplacment={checkboxReplacment}
									categories={[
										formatMessage({ id: 'sunday' }),
										formatMessage({ id: 'monday' }),
										formatMessage({ id: 'tuesday' }),
										formatMessage({ id: 'wednesday' }),
										formatMessage({ id: 'thursday' }),
										formatMessage({ id: 'friday' }),
										formatMessage({ id: 'saturday' })
									]}
									tickAmount={8}
									onWeekSalesReady={(sales: number) => {
										setWeeklySales(
											`${formatMessage({ id: 'weekly-avg-sales' })} - ${Math.floor(
												sales
											).toLocaleString()}`
										);
									}}
								/>
							</div>
							<div className="bg-gray chartFather align-items-center mt-1-vh">
								<h3 className="mr-auto ml-1 text-bold-700">
									{formatMessage({ id: 'trending_sales-month' })}
								</h3>
								<BarChart
									displayDataLabels={false}
									updateChart={updateChart}
									type={'month'}
									chosenProduct={chosenProduct}
									chosenBranch={chosenBranch}
									checkboxReplacment={checkboxReplacment}
									categories={[
										formatMessage({ id: 'January-short' }),
										formatMessage({ id: 'February-short' }),
										formatMessage({ id: 'March-short' }),
										formatMessage({ id: 'April-short' }),
										formatMessage({ id: 'May-short' }),
										formatMessage({ id: 'June-short' }),
										formatMessage({ id: 'July-short' }),
										formatMessage({ id: 'August-short' }),
										formatMessage({ id: 'September-short' }),
										formatMessage({ id: 'October-short' }),
										formatMessage({ id: 'November-short' }),
										formatMessage({ id: 'December-short' })
									]}
								/>
							</div>
						</div>
					</Col>
				)}
			</Row>
		</>
	);
};
export default ItemTrends;
