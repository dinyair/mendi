import { getRequest, postRequest } from "@src/http";

export const postTrending = async (rec: any) => {
    const resp = await postRequest<any>('manage/reports/daily-barcode/new', {
        rec
    });
    return resp
}
export const getMatriza = async (rec: any,) => {
    const resp = await getRequest<any>(`AppSaleAllBranch?barCode=${rec.barcode}&date1=${rec.date1}&date2=${rec.date2}&sidra=${rec.series}&sub_bar=${rec.sub_bar}`, {});
    return resp
}
export const getCatalogUpdated = async () => {
    const resp = await getRequest<any>(`getcatalog`, {});
    return resp
}

export const getChart1 = async (rec: any) => {
    const { date1, date2, barcode, branchId, sub_bar } = rec
    let resp: any
    if (branchId) resp = await getRequest<any>(`AppDoChart1?date1=${date1}&date2=${date2}&barcode=${barcode}&branchId=${branchId}&sub_bar=${sub_bar}`, {});
    else resp = await getRequest<any>(`AppDoChart1?date1=${date1}&date2=${date2}&barcode=${barcode}&sub_bar=${sub_bar}`, {});

    return resp
}
export const getChart2 = async (rec: any) => {
    const { barcode, branchId, sub_bar } = rec
    let resp: any
    if (branchId) resp = await getRequest<any>(`AppDoChart2?barcode=${barcode}&branchId=${branchId}&sub_bar=${sub_bar}`, {});
    else resp = await getRequest<any>(`AppDoChart2?barcode=${barcode}&sub_bar=${sub_bar}`, {});
    return resp
}
export const getBranch = async () => {
    const resp = await getRequest<any>(`getBranch`, {});
    return resp
}