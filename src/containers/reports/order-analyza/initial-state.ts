import { getRequest, postRequest } from "@src/http";

// export const fetchOrderStatistics = async (rec: any) => {
//     const resp = await postRequest<any>('getoStatNew', {
//         rec
//     });
//     return resp
// }
export const fetchOrderStatistics = async (rec: any) => {
    const resp = await postRequest<any>('getOrderStatistic', {
        rec
    });
    return resp
}
export const fetchOrderReview = async (rec1: any) => {
    const resp = await postRequest<any>('getRepSales1New', {
        rec1
    });
    return resp
}

