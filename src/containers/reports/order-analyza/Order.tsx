import * as React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { RootStore } from '@src/store';
import { Button } from "reactstrap"
import { Table, AgGrid } from '@components';
import { Spinner } from 'reactstrap'
import { fetchOrderStatistics } from './initial-state'
import * as moment from 'moment';
import { useIntl, FormattedMessage } from "react-intl";

interface Props {
    orderStatistics: any
    isLoading: boolean
    dataWithIdKey: any
    setActiveTab: any
    TABS: any
    setOrderNumClicked: any
    filterData: any
    setDefaultAboveGridFilter: any
    defaultAboveGridFilter: any
    chosenSupplier: any
    chosenBranch: any
    advancedSearch: boolean
    handleSearch: any
    setTempData: (data: any) => void
    setChosenBranch: (data: any) => void
    setChosenSupplier: (data: any) => void
    setRows: (rows: any) => void
    setFields: (fields: any) => void
    rows: any[]
    fields: any[]
    setIsGridLoading: (b: boolean) => void
    isGridLoading?: any
    originalSearch?:any
    firstLoad:boolean
}
export const Order: React.FC<Props> = (props: Props) => {
    const { formatMessage } = useIntl();

    const { defaultAboveGridFilter, orderStatistics, isLoading, dataWithIdKey, setActiveTab, TABS, setOrderNumClicked, filterData,
        setDefaultAboveGridFilter, chosenSupplier, chosenBranch, advancedSearch, handleSearch,
         originalSearch,setIsGridLoading, isGridLoading, setTempData, setChosenBranch, setChosenSupplier, setRows, setFields, rows, fields,firstLoad } = props;

    // const [firstLoad, setFirstLoad] = React.useState<boolean>(true)


    React.useEffect(() => {
        setFields([
            { text: "Date", type: 'FullDate', filter: 'agDateColumnFilter',width: window.innerWidth * 0.3, minWidth: 70, },
            {
                text: "orderNumber", type: 'numberDisplayZero', width: window.innerWidth * 0.3, minWidth: 70, cellRenderer: 'ClickableCell', clickableCells: {
                    aboveGridFilter: true, customAboveGridFilter: (props: any) => { setIsGridLoading(true) ;setOrderNumClicked(props.value); setActiveTab(TABS.ITEM) }
                },
            },
            {
                text: "supplier", type: 'string', width: window.innerWidth * 0.3, minWidth: 70, cellRenderer: 'ClickableCell', clickableCells: {
                    aboveGridFilter: true
                },
            },
            {
                text: "branch", type: 'string', width: window.innerWidth * 0.3, minWidth: 70, cellRenderer: 'ClickableCell', clickableCells: {
                    aboveGridFilter: true
                },
            },
            { text: "items_in_order", type: 'numberDisplayZero', width: window.innerWidth * 0.3, minWidth: 70, },
            { text: "approved_items", type: 'string', width: window.innerWidth * 0.3, minWidth: 70, },
            { text: "intervented_items", type: 'numberDisplayZero', width: window.innerWidth * 0.3, minWidth: 70, },
            { text: "ordered_items", type: 'numberDisplayZero', width: window.innerWidth * 0.3, minWidth: 70, },
            { text: "items_provided", type: 'numberDisplayZero', width: window.innerWidth * 0.3, minWidth: 70, },
            { text: "P_orderedItems", type: 'percentage', width: window.innerWidth * 0.3, minWidth: 100, },
            { text: "P_recievedReccomendation", type: 'percentage', width: window.innerWidth * 0.3, minWidth: 150, },
            { text: "P_supplyFromOrder", type: 'percentage', width: window.innerWidth * 0.3, minWidth: 150, },
        ])
    }, [])


    React.useEffect(() => {
        if (filterData) {
            handleSearch(true);
        }
    }, [filterData])

    React.useEffect(() => {
        if (typeof orderStatistics == 'object') {
            getOrderStat()
        }
    }, [orderStatistics])

    React.useEffect(() => {
        if (fields && (chosenBranch && chosenBranch.length || chosenSupplier && chosenSupplier.length)) {
            setIsGridLoading(false)
            setTimeout(() => {
                setIsGridLoading(true)
            }, 1);
        }
    }, [fields])

    const getOrderStat = async () => {
        setIsGridLoading(true)
        let sortForGrid = orderStatistics.map((order: any) => {
            let Date = order.orderDate
            let orderNumber = order.orderNum
            let supplierId = order.supplierId
            let branchId = order.branchId

            let supplier = dataWithIdKey.suppliers[order.supplierId] ? dataWithIdKey.suppliers[order.supplierId] : dataWithIdKey.subSuppliers[order.supplierId]
            let branch = dataWithIdKey.userBranches[order.branchId]

            let items_in_order = order.itemsInOrder
            let approved_items = order.approvedItems
            let intervented_items = order.totalItemsChanged
            let ordered_items = order.orderedItems
            let items_provided = order.itemsProvided
            let P_orderedItems = Math.round(order.P_orderedItems)
            let P_recievedReccomendation = Math.round(order.P_recievedReccomendation)
            let P_supplyFromOrder = Math.round(order.P_supplyFromOrder)
            return { Date, branchId,supplierId,orderNumber, supplier, branch, items_in_order, approved_items, intervented_items, ordered_items, items_provided, P_orderedItems, P_recievedReccomendation, P_supplyFromOrder }
        })

        setRows(sortForGrid)
        setIsGridLoading(false)

        setTimeout(() => {
            if(advancedSearch){
                createAboveFilterFromDrawer(sortForGrid)
            }else setDefaultAboveGridFilter([chosenBranch ? chosenBranch.map((b: any) => {
                return { ...b, cannotRemove: true }
            }) : {}, chosenSupplier ? chosenSupplier.map((s: any) => {
                return { ...s, cannotRemove: true }
            }) : {}])
     }, 1);

    }

    const createAboveFilterFromDrawer = (rows:any) => {

        let aboveFilter: any = []
        let tabs = filterData.newData.tabs[0]
        tabs.formGroup.forEach((fGroup: any) => {
            if (fGroup.fields && fGroup.fields.length) {
                fGroup.fields.forEach((field: any) => {
                    switch (field.name) {
                        case 'date':
                            break;
                        case 'OrderNum':
                            if (field.value.length){
                                // aboveFilter.push([{ dontFilter: true, key:'OrderNum',name: 'OrderNum', preText: formatMessage({ id: 'OrderNum' }), label: field.value, value: field.value }])

                                let getOrderData = rows.find((i:any)=>i.orderNumber == field.value )
                                if(getOrderData){
                                let orderBranch = getOrderData.branch
                                let orderBranchId = getOrderData.branchId
                                let orderSupplier = getOrderData.supplier
                                let orderSupplierId = getOrderData.supplierId
                                aboveFilter.push([{ dontFilter: true, key:'OrderNum',name: 'OrderNum', preText: formatMessage({ id: 'OrderNum' }), label: field.value, value: field.value }])
                                aboveFilter.push([{ cannotRemove: true,name: 'branch',key:'branchId', preText: formatMessage({ id: 'branch' }), value: orderBranchId, label: orderBranch }])
                                aboveFilter.push([{ cannotRemove: true, name: 'supplier',key:'supplierId', preText: formatMessage({ id: 'supplier' }), value: orderSupplierId, label: orderSupplier }])
                                }
                            } 
                            break;
                        case 'branch':
                            if (field.defaultValue && field.defaultValue.length) {
                                let valueArr: any = []
                                field.defaultValue.forEach((value: any) => {
                                    valueArr.push({ name: 'branch',key:'branchId', cannotRemove: true, label: value.label, value: value.value, preText: formatMessage({ id: 'branch' }) })
                                })
                                aboveFilter.push(valueArr)
                            } else if (typeof field.value != 'string') {
                                let valueArr: any = []
                                field.value.forEach((value: any) => {
                                    valueArr.push({ name: 'branch', key:'branchId',cannotRemove: true, label: value.label, value: value.value, preText: formatMessage({ id: 'branch' }) })
                                })
                                aboveFilter.push(valueArr)
                            }
                            break;
                        case 'supplier':
                            if (field.defaultValue && field.defaultValue.length) {
                                let valueArr: any = []
                                field.defaultValue.forEach((value: any) => {
                                    valueArr.push({ name: 'supplier', key:'supplierId',cannotRemove: true, label: value.label, value: value.value, preText: formatMessage({ id: 'supplier' }) })
                                })
                                aboveFilter.push(valueArr)
                            } else if (typeof field.value != 'string') {
                                let valueArr: any = []
                                field.value.forEach((value: any) => {
                                    valueArr.push({ name: 'supplier',key:'supplierId', cannotRemove: true, label: value.label, value: value.value, preText: formatMessage({ id: 'supplier' }) })
                                })
                                aboveFilter.push(valueArr)
                            }
                            break;
                        default:
                            break;
                    }
                })
            }
        })
        setDefaultAboveGridFilter(aboveFilter)
    }

    const afterAboveFilterRemovedOrAddedNew = (data: any, type: string) => {
        let newTempData: any = {}
        data.forEach((d: any) => {
            newTempData[d.field] = { name: d.field, preText: formatMessage({ id: d.field }), data: [...d.data] }
        })
        if (newTempData && newTempData.branch && newTempData.branch.data) setChosenBranch(newTempData.branch.data.map((i: any) => {
            return { name: 'branch', key: 'branchId',dontFilter: false, cannotRemove: true, label: i.label, value: i.value, preText: formatMessage({ id: 'branch' }) }
        }))
        else setChosenBranch(null)
        if (newTempData && newTempData.supplier && newTempData.supplier.data) setChosenSupplier(newTempData.supplier.data.map((i: any) => {
            return { name: 'supplier', key: 'supplierId',dontFilter: false, cannotRemove: true, label: i.label, value: i.value, preText: formatMessage({ id: 'supplier' }) }
        }))
        else setChosenSupplier(null)

        setTempData(newTempData)

        // if (type != 'addedFilter') {
        //     let newAboveGridFilter: any = []
        //     addToAbove(newTempData, 'branch', newAboveGridFilter, 'branch', 'branch', true, true)
        //     addToAbove(newTempData, 'supplier', newAboveGridFilter, 'supplier', 'supplier', true, true)
        //     setDefaultAboveGridFilter(newAboveGridFilter)

        // }

    }
    const addToAbove = (arr: any, subName: any, newArr: any, name: string, preText: string, filter: boolean, cannotRemove: boolean) => {
        if (arr[subName] && arr[subName].data && arr[subName].data.length) {
            let temp: any = []
            arr[subName].data.forEach((i: any) => {
                temp.push({ name: name, dontFilter: filter, cannotRemove, label: i.label, value: i.value, preText: formatMessage({ id: preText }) })
            })
            newArr.push(temp)
        }
    }

    return (<>
        <div className='width-100-per'>
            {!firstLoad ?
                <AgGrid
                    successDark={'all'}
                    cellGreenFields={['orderNumber', 'supplier', 'branch']}
                    autoSelectedAboveGridFilter={defaultAboveGridFilter}
                    afterAboveFilterRemovedOrAdded={afterAboveFilterRemovedOrAddedNew}
                    translateHeader
                    resizable
                    gridHeight={'70vh'}
                    floatFilter
                    fields={fields}
                    groups={[]}
                    totalRows={[]}
                    rows={rows}
                    checkboxFirstColumn={false}
                    pagination
                    displayLoadingScreen={isGridLoading}
                />
                :
                <div>
                    {isGridLoading ? <Spinner /> : null}
                </div>
            }

        </div>
    </>);
};

export default Order;
