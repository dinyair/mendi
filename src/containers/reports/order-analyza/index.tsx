import * as React from 'react';
import * as moment from 'moment';
import { RootStore } from '@src/store';
import { useSelector, useDispatch } from 'react-redux';
import { useIntl, FormattedMessage } from "react-intl";
import { Col, Button } from 'reactstrap';
import { CalendarDoubleInput } from '@components';
import { CustomInput } from 'reactstrap'
import { fetchAndUpdateStore } from '@src/helpers/helpers'
import AsyncSelect from "react-select/async";
import { fetchOrderStatistics, fetchOrderReview } from './initial-state'
// import { EditDrawer } from '@src/components/edit-drawer';
import { orderDrawerData, itemDrawerData } from './mockData';
import { Icon } from '@components';
import { config } from '@src/config'
import ExportExcel from '@src/components/ExportExcel/ExportExcel';
import { GenericDrawer } from '@src/components/generic-drawer';

import Item from './Item'
import Order from './Order'

import './index.scss';

export const OrderAnalyze: React.FunctionComponent = () => {
	const { formatMessage } = useIntl();
	document.title = formatMessage({id: 'order-analyze'})

	const dispatch = useDispatch();
	const catalog = useSelector((state: RootStore) => state._catalogUpdated)
	const catalogFullData = useSelector((state: RootStore) => state._catalogFullData)
	const userBranches = useSelector((state: RootStore) => state._userBranches)
	const suppliers = useSelector((state: RootStore) => state._suppliers)
	const subSuppliers = useSelector((state: RootStore) => state._subSuppliers)
	const divisions = useSelector((state: RootStore) => state._divisions)
	const subGroups = useSelector((state: RootStore) => state._subGroups)
	const series = useSelector((state: RootStore) => state._series)
	const groups = useSelector((state: RootStore) => state._groups)


	const TABS = {
		ORDER: formatMessage({ id: 'orderSwitch' }),
		ITEM: formatMessage({ id: 'itemSwitch' })
	};

	const [activeTab, setActiveTab] = React.useState(TABS.ORDER);
	let beginingOfWeek: any = moment().subtract(1, 'weeks').format('YYYY/MM/DD')
	let beginingOfWeekDateType = new Date(beginingOfWeek)

	const [date1, setDate1] = React.useState<any>(beginingOfWeekDateType)
	const [date2, setDate2] = React.useState<any>(new Date())
	const [inputValue, setInputValue] = React.useState<any>(null);
	const [chosenBranch, setChosenBranch] = React.useState<any>([])
	const [chosenSupplier, setChosenSupplier] = React.useState<any>([])
	const [chosenGroup, setChosenGroup] = React.useState<any>([])
	const [orderStatistics, setOrderStatistics] = React.useState<any>(false)
	const [orderReview, setOrderReview] = React.useState<any>(false)
	const [isLoading, setIsLoading] = React.useState<boolean>(false)
	const [dataWithIdKey, setDataWithIdKey] = React.useState<any>()
	const [orderNumClicked, setOrderNumClicked] = React.useState<any>()

	const [showItemModal, setShowItemModal] = React.useState(false);
	const [showOrderModal, setShowOrderModal] = React.useState(false);
	const [filterData, setFilterData] = React.useState<any>()
	const [advancedSearch, setAdvancedSearch] = React.useState<any>()
	const [defaultAboveGridFilter, setDefaultAboveGridFilter] = React.useState<any>()
	const [tempData, setTempData] = React.useState<any>([])
	const [isGridLoading, setIsGridLoading] = React.useState<boolean>(false)
	const [rows, setRows] = React.useState<any>([])
	const [fields, setFields] = React.useState<any>([])
	const [originalSearch,setOriginalSearch]= React.useState<any>([])
    const [firstLoad, setFirstLoad] = React.useState<boolean>(true)

	React.useEffect(() => {
		fetchAndUpdateStore(dispatch, [
			{ state: catalog, type: 'catalog' },
			{ state: userBranches, type: 'userBranches' },
			{ state: suppliers, type: 'suppliers' },
			{ state: subSuppliers, type: 'subSuppliers' },
			{ state: subGroups, type: 'subGroups' },
			{ state: divisions, type: 'divisions' },
			{ state: series, type: 'series' },
			{ state: groups, type: 'groups' },
			{ state: catalogFullData, type: 'catalogFullData' }
		])
	}, [])
	React.useEffect(() => {
		setTempData({ ...tempData, 'branch': chosenBranch ? { cannotRemove: true, label: chosenBranch.length && chosenBranch[0].label, preText: chosenBranch.length && chosenBranch[0].label, data: chosenBranch.length ? chosenBranch : [] } : {} })
	}, [chosenBranch])

	React.useEffect(() => {
		setTempData({ ...tempData, 'supplier': chosenSupplier ? { cannotRemove: true, label: chosenSupplier.length && chosenSupplier[0].label, preText: chosenSupplier.length && chosenSupplier[0].label, data: chosenSupplier.length ? chosenSupplier : [] } : {} })
	}, [chosenSupplier])


	const toggleOrderModal = () => {
		setShowOrderModal(!showOrderModal);
	}
	const toggleItemModal = () => {
		setShowItemModal(!showItemModal);
	}
	const saveSideBarChanges = (newData: any) => {
		setIsLoading(true)
		setFilterData({ newData, activeTab })
		setIsLoading(false)
	}

	const handleInputChange = (newValue: string) => {
		setInputValue(inputValue);
		return inputValue;
	};
	const checkIfEnter = (e: any) => {
		// if (e.which === 13) handleSearch()
	}
	const loadSupplierOptions = (inputValue: string, callback: any) => {
		if (inputValue.length >= 2) {
			callback(filterSupplier(inputValue));
		} else {
			callback()
		}
	};
	const loadBranchOptions = (inputValue: string, callback: any) => {
		callback(filterBranch(inputValue));
	}
	const filterBranch = (inputValue: string) => {
		const branchesFiltered = userBranches.filter((i: any) =>
			i.Name.includes(inputValue)
		);
		let branchesMap: any[] = []
		branchesFiltered && branchesFiltered[0] ? branchesMap = branchesFiltered.map((option: any, index: number) => {
			return { index, value: option.BranchId, label: option.Name }
		}) : []
		return branchesMap
	};
	const filterSupplier = (inputValue: string) => {
		const supplieresFiltered = suppliers.filter((i: any) =>
			i.Name.includes(inputValue)
		);
		let subSuppliersRemoveD = []

		subSuppliersRemoveD = subSuppliers.filter((subS, index, self) =>
			index === self.findIndex((t) => (
				t.SubsapakName === subS.SubsapakName
			))
		)
		const subSupplieresFiltered = subSuppliersRemoveD.filter((i: any) =>
			i.SubsapakName.includes(inputValue)
		);
		let suppliersMap: any[] = []
		supplieresFiltered && supplieresFiltered[0] ? suppliersMap = supplieresFiltered.map((option: any, index: number) => {
			return { index, value: option.Id, label: option.Name, name: 'supplier' }
		}) : []
		let subSuppliersMap: any = []
		subSupplieresFiltered && subSupplieresFiltered[0] ? subSuppliersMap = subSupplieresFiltered.map((sub: any) => {
			return { value: sub.SubsapakId, label: sub.SubsapakName, name: 'supplier' }
		}) : []
		let allSuppliers = [...suppliersMap, ...subSuppliersMap]
		return allSuppliers
	};
	const selectStyle = {
		container: (base: any) => ({
			...base,
		}),
		option: (provided: any, state: any) => ({
			...provided,

			"&:hover": {
				color: "#ffffff",
				backgroundColor: "#31baab"
			},
			color: state.isSelected ? '#ffffff' : '#1e1e20',
			backgroundColor: state.isSelected ? '#027b7a' : '#ffffff',
			borderBottom: '1px dotted lightgray',
			padding: 10,
		}),
		control: () => ({
			width: 200,
			height: 50
		}),
	}

	React.useEffect(() => {
		setFirstLoad(true)
		if (chosenBranch && chosenBranch.length || chosenSupplier && chosenSupplier.length) {
			setIsGridLoading(true)
			setTempData({ 'branch': chosenBranch ? {key:'branchId', cannotRemove: true, label: chosenBranch.length && chosenBranch[0].label, 
				preText: chosenBranch.length && chosenBranch[0].label, data: chosenBranch.length ? chosenBranch : [] } : {}, 'supplier': chosenSupplier ? {
					 cannotRemove: true,key:'supplierId', label: chosenSupplier.length && chosenSupplier[0].label, preText: chosenSupplier.length && chosenSupplier[0].label, data: chosenSupplier.length ? chosenSupplier : [] } : {} })
			handleSearch()
		}
	}, [activeTab])


	const handleSearch = async (isAdvanced?: boolean) => {
		setIsGridLoading(true)
		setIsLoading(true)
		// setDefaultAboveGridFilter([])
		let branch = chosenBranch;
		let supplier = chosenSupplier
		let _suppliers: any = {}
		let _subSuppliers: any = {}
		let _userBranches: any = {}
		let _catalog: any = {}
		suppliers.forEach((supplier: any) => _suppliers[supplier.Id] = supplier.Name)
		subSuppliers.forEach((subSupplier: any) => _subSuppliers[subSupplier.SubsapakId] = subSupplier.Name)
		userBranches.forEach((branch: any) => _userBranches[branch.BranchId] = branch.Name)
		if (catalog && catalog.length) catalog.forEach((item: any) => _catalog[item.BarCode] = item.Name)
		setDataWithIdKey({ catalog: _catalog, suppliers: _suppliers, userBranches: _userBranches, subSuppliers: _subSuppliers })

		let newS = supplier && supplier[0] ? supplier.map((s: any) => {
			if (_suppliers[s.value]) { return { ...s } }
			else { return { ...s, value: _subSuppliers[s.value] ? _subSuppliers[s.value].SapakId: 0, subSid: s.value } }
		}) : []

		if (activeTab === TABS.ORDER) {
			setOrderNumClicked(null)
			let rec = {
				fromDate: moment(date1).format('YYYY-MM-DD'),
				toDate: moment(date2).format('YYYY-MM-DD'),
				branchId: branch && branch.length ? branch.map((b: any) => b.value) : null,
				supplierId: newS && newS.length ? newS.map((b: any) => b.value) : null,
				OrderNum: isAdvanced && tempData && tempData.OrderNum && tempData.OrderNum.data ? tempData.OrderNum.data[0].value: null
			}
			// let rec = {
			// 	fdate: moment(date1).format('YYYY-MM-DD'),
			// 	tdate: moment(date2).format('YYYY-MM-DD'),
			// 	snif: branch && branch.length ? branch.map((b: any) => b.value) : null,
			// 	sapak: newS && newS.length ? newS.map((b: any) => b.value) : null,
			// 	OrderNum: isAdvanced && tempData && tempData.OrderNum && tempData.OrderNum.data ? tempData.OrderNum.data[0].value: null
			// }
			let orderStatistics = await fetchOrderStatistics(rec)
			setOriginalSearch(rec)
			setAdvancedSearch(isAdvanced ? true : false)
			setOrderStatistics(orderStatistics)
			setIsLoading(false)
		} else {

			let rec1 = {
				sapak: newS && newS.length ? newS.map((b: any) => b.subSid ? b.subSid : b.value) : null,
				snif: branch && branch.length ? branch.map((b: any) => b.value) : null,
				date1: moment(date1).format('YYYY-MM-DD'),
				date2: moment(date2).format('YYYY-MM-DD'),
				weeks: 1,
				mainsapak: newS && newS.length ? newS.map((b: any) => b.value) : null,
				allord: false,
				OrderNum: isAdvanced && tempData && tempData.OrderNum && tempData.OrderNum.data ? tempData.OrderNum.data[0].value: null
			}
			setOriginalSearch(rec1)
			let orderReview = await fetchOrderReview(rec1)

			setAdvancedSearch(isAdvanced ? true : false)
			setOrderReview(orderReview)
			setIsLoading(false)
		}
		setFirstLoad(false)
	}
	const updateSelectedDefault = (newData: any, type: string) => {
		let _suppliers: any = {}
		let _subSuppliers: any = {}
		let _userBranches: any = {}
		let _catalog: any = {}
		suppliers.forEach((supplier: any) => _suppliers[supplier.Id] = supplier.Name)
		subSuppliers.forEach((subSupplier: any) => _subSuppliers[subSupplier.SubsapakId] = subSupplier.Name)
		userBranches.forEach((branch: any) => _userBranches[branch.BranchId] = branch.Name)
		catalog.forEach((item: any) => _catalog[item.BarCode] = item)
		let dataKey = { catalog: _catalog, suppliers: _suppliers, userBranches: _userBranches, subSuppliers: _subSuppliers }

		switch (type) {
			case 'branch': setChosenBranch(newData ? newData.map((g:any)=>{return {...g, key: 'branchId'}}):[]); break;
			case 'supplier': setChosenSupplier(newData ? newData.map((g:any)=>{return {...g, key: 'supplierId'}}):[]); break;
			case 'groups': {
				setChosenGroup(newData?newData:[]); break;
			}
			case 'barcode':{
				if(newData){
					setTempData({...tempData, 'barcode': { name: 'barcode', preText: 'barcode', data: newData && typeof newData != 'string' ? Array.isArray(newData) ? newData.map((val: any) => { return { label: val.label, value: val.value,name: 'barcode' } }) : [{ label: newData.label, value: newData.label, name:'barcode' }] : [{ label: newData, value: newData, name:'barcode' }] } })
				let newsSelectedBarcode = newData.map((i:any)=>{
					let productData = dataKey.catalog[i.value]
					let supplierId = productData.SapakId
					let supplierName = dataKey.suppliers[supplierId] ? dataKey.suppliers[supplierId] : dataKey.subSuppliers[supplierId]
					return {value: supplierId ,label: supplierName,name:'supplier',key:'branchId' }
				})
				let selectedSuppliersByBarcode = [...chosenSupplier, ...newsSelectedBarcode].filter((v,i,a)=>a.findIndex(t=>(t.value === v.value))===i)
				setTimeout(() => {
				setChosenSupplier(selectedSuppliersByBarcode)
				}, 5);
			   }else{
				let newTempData = tempData
				newTempData['barcode'] = {}
				setTempData(newTempData)
			   }
			break;
			} 
			case 'date':{
				console.log({newData})
			}
		}
	}

	return (
		<>
			<div className='d-flex-column'>
				<div  className='mb-08 text-bold-700 text-black d-flex'>
				<h2 className="mr-2 align-self-center text-bold-600 mb-0">{formatMessage({ id: 'order-analyze' })}</h2>
				</div>

				<div  className='mb-2 ml-1 d-flex'>
					<div className='text-bold-600 mr-1 d-flex justify-content-center align-items-center'>
						<span>{formatMessage({ id: 'orderSwitch' })}</span>
						<CustomInput checked={activeTab === TABS.ORDER ? false : true} className='orderAnalyza__switch ml-05 mr-05' bsSize='sm' onClick={() => { setOrderNumClicked(null); setActiveTab(activeTab === TABS.ORDER ? TABS.ITEM : TABS.ORDER) }} type="switch" id={"5"} name={'test'} />
						<span>{formatMessage({ id: 'itemSwitch' })}</span>
					</div>
					<CalendarDoubleInput
						setDate1={setDate1}
						setDate2={setDate2}
						date2={date2}
						date1={date1}
						top={'0'}
					/>
					{[
						{
							name: 'branch',
							isMulti: false,
							allowSelectAll: false,
							loadOptions: loadBranchOptions,
							text: formatMessage({ id: 'branches' }),
							defaultOptions: userBranches && userBranches[0] ? userBranches.map((option: any, index: number) => {
								return { index, value: option.BranchId, label: option.Name, name: 'branch' }
							}) : [],
							onChange: null,
							value: tempData && tempData.branch ? tempData.branch.data && tempData.branch.data.length && tempData.branch.data : null,
							styles: selectStyle
						},
						{
							name: 'supplier',
							isMulti: false,
							allowSelectAll: false,
							text: formatMessage({ id: 'Suppliers' }),
							loadOptions: loadSupplierOptions,
							defaultOptions: [],
							value: tempData && tempData.supplier ? tempData.supplier.data && tempData.supplier.data.length && tempData.supplier.data : null,
							styles: selectStyle
						},
					].map(item => {
						return (
							<div key={item.text} className='ml-1'>
								<AsyncSelect
									name={item.name}
									isDisabled={suppliers && suppliers.length > 0 && userBranches && userBranches.length > 0 ? false : true}
									isMulti={true}
									isClearable={true}
									styles={item.styles}
									value={item.value}
									loadOptions={item.loadOptions}
									defaultOptions={item.defaultOptions}
									onInputChange={handleInputChange}
									onKeyDown={(e) => checkIfEnter(e)}
									onChange={(e: any, actionMeta: any) => {
										if (e) {
											if (actionMeta.name === 'supplier') {
												setChosenSupplier(e.map((e: any) => {
													return { name: 'supplier', key:'supplierId',dontFilter: false, cannotRemove: true, label: e.label, value: e.value, preText: formatMessage({ id: 'supplier' }) }
												}))
											} else {
												setChosenBranch(e.map((e: any) => {
													return { name: 'branch', key:'branchId',dontFilter: false, cannotRemove: true, label: e.label, value: e.value, preText: formatMessage({ id: 'branch' }) }
												}))
											}
										} else {
											if (actionMeta.name === 'supplier') setChosenSupplier(null)
											else setChosenBranch(null)
										}
									}}
									closeMenuOnSelect={false}
									hideSelectedOptions={false}
									classNamePrefix='select'
									noOptionsMessage={() => formatMessage({ id: 'trending_productMinimumSearch' })}
									placeholder={item.text}
								/>
							</div>
						)
					})}
					<button onClick={()=>{activeTab === TABS.ORDER ? toggleOrderModal() : toggleItemModal()}} className='p-05 ml-1 no-outline no-border round text-bold-600 height-2-5-rem width-9-rem'>
						<span>
							{formatMessage({ id: 'advancedSearch' })}
						</span>
					</button>
					<Button
						color="primary"
						disabled={activeTab === TABS.ORDER ? chosenSupplier && chosenSupplier.length && !isLoading || chosenBranch && chosenBranch.length && !isLoading ? false : true :
							catalog && catalog.length && chosenSupplier && chosenSupplier.length && !isLoading && catalogFullData && catalogFullData.length || catalog && catalog.length && chosenBranch && chosenBranch.length && !isLoading && catalogFullData && catalogFullData.length ? false : true}
						className="ml-1 round text-bold-400 d-flex justify-content-center align-items-center width-7-rem height-2-5-rem cursor-pointer btn-primary "
						onClick={() => { setOrderNumClicked(null); handleSearch() }}>
						<FormattedMessage id="search" />
					</Button>
					{rows && rows.length ?
						<div className="d-flex justify-content-center align-items-center ml-05 cursor-pointer">
							<ExportExcel fields={fields} data={rows} direction={'ltr'} translateHeader={true}
								icon={<Icon src={config.iconsPath + "table/excel-download.svg"}/>} />
						</div>
						: null}
				</div>
			</div>

			<Col md="12" sm="12" className='mt-2 mb-2 ml-1 d-flex'>
				{activeTab === TABS.ORDER ?
					<Order defaultAboveGridFilter={defaultAboveGridFilter}
						chosenSupplier={chosenSupplier} chosenBranch={chosenBranch}
						setDefaultAboveGridFilter={setDefaultAboveGridFilter}
						filterData={filterData} setOrderNumClicked={setOrderNumClicked} setActiveTab={setActiveTab}
						TABS={TABS} dataWithIdKey={dataWithIdKey} isLoading={isLoading} orderStatistics={orderStatistics}
						advancedSearch={advancedSearch} handleSearch={handleSearch} setChosenBranch={setChosenBranch} setChosenSupplier={setChosenSupplier} setTempData={setTempData}
						setRows={setRows}
						setFields={setFields}
						rows={rows}
						fields={fields}
						isGridLoading={isGridLoading}
						setIsGridLoading={setIsGridLoading}
						originalSearch={originalSearch}
						firstLoad={firstLoad} />
					:
					<Item
						chosenBranch={chosenBranch}
						chosenSupplier={chosenSupplier}
						advancedSearch={advancedSearch}
						setChosenBranch={setChosenBranch}
						setChosenSupplier={setChosenSupplier}
						handleSearch={handleSearch}
						catalogFullData={catalogFullData}
						filterData={filterData}
						setOrderNumClicked={setOrderNumClicked}
						orderNumClicked={orderNumClicked}
						dataWithIdKey={dataWithIdKey}
						isLoading={isLoading}
						isGridLoading={isGridLoading}
						setIsGridLoading={setIsGridLoading}
						orderReview={orderReview}
						defaultAboveGridFilter={defaultAboveGridFilter}
						setDefaultAboveGridFilter={setDefaultAboveGridFilter}
						setTempData={setTempData}
						setRows={setRows}
						setFields={setFields}
						rows={rows}
						fields={fields}
						originalSearch={originalSearch}
						firstLoad={firstLoad}
					/>
				}
			</Col>

			{showOrderModal ?
						<GenericDrawer {...{
							drawerType: 'TAB',
							caption: formatMessage({ id: 'advancedSearch' }),
							shown: showOrderModal,
							toggleModal: toggleOrderModal,
							data: orderDrawerData(tempData, { userBranches, suppliers, subSuppliers, dataWithIdKey }, { setDate1, setDate2, date1, date2 }, updateSelectedDefault) ,
							saveChanges: saveSideBarChanges,
							tempData: tempData,
							setTempData: setTempData,
							isTabsHidden: true,
							titleEditable: false,
							hideZeroFilters: false,
							saveButtonText: 'search',
						saveDisabled: chosenBranch && chosenBranch.length || chosenSupplier && chosenSupplier.length ||tempData && tempData.OrderNum && tempData.OrderNum.data.length ? false : true
						}} />
					: null }

				{showItemModal ?<GenericDrawer {...{
				drawerType: 'TAB',
				caption: formatMessage({ id: 'advancedSearch' }),
				shown: showItemModal,
				toggleModal: toggleItemModal,
				data: itemDrawerData(tempData, { catalog, userBranches, suppliers, groups, chosenGroup, divisions, subGroups, series, dataWithIdKey, subSuppliers }, { setDate1, setDate2, date1, date2 }, updateSelectedDefault),
				saveChanges: saveSideBarChanges,
				tempData: tempData,
				setTempData: setTempData,
				isTabsHidden: true,
				titleEditable: false,
				hideZeroFilters: false,
				saveButtonText: 'search',
				saveDisabled: catalogFullData && catalogFullData.length && ( chosenBranch && chosenBranch.length || chosenSupplier && chosenSupplier.length || 
					tempData && tempData.OrderNum && tempData.OrderNum.data.length) ? false: true
			}} />:null }	



		</>)
}

export default OrderAnalyze;