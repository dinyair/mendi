import * as React from 'react';
import {  AgGrid } from '@components';
import { Spinner } from 'reactstrap'
import { useIntl, FormattedMessage } from "react-intl";

interface Props {
	isLoading: boolean
	orderReview: any
	dataWithIdKey: any
	setOrderNumClicked: any
	orderNumClicked: any
	filterData: any
	catalogFullData: any
	setChosenBranch: any
	setChosenSupplier: any
	handleSearch: any
	advancedSearch: boolean
	chosenSupplier: any
	chosenBranch: any
	defaultAboveGridFilter: any
	setDefaultAboveGridFilter: any
	// afterAboveFilterRemovedOrAdded: (data: any, type: string) => void
	setTempData: (data: any) => void
	setRows: (rows: any) => void
	setFields: (fields: any) => void
	rows: any[]
	fields: any[]
	setIsGridLoading: (b: boolean) => void
	isGridLoading?: any
	originalSearch?:any
	firstLoad:boolean
}

export const Item: React.FC<Props> = (props: Props) => {
	const { defaultAboveGridFilter, setDefaultAboveGridFilter, isGridLoading, setIsGridLoading, orderReview, dataWithIdKey, orderNumClicked,
		filterData, catalogFullData, setChosenBranch, setChosenSupplier, handleSearch, advancedSearch, chosenSupplier, chosenBranch, originalSearch,
		setTempData, setRows, setFields, rows, fields,firstLoad } = props

	const { formatMessage } = useIntl();


	React.useEffect(() => {
		if (orderNumClicked && orderNumClicked.length > 0) setDefaultAboveGridFilter([{ key:'OrderNum',field: 'OrderNum', preText: formatMessage({ id: 'OrderNum' }), data: [orderNumClicked] }])
	}, [orderNumClicked])

	React.useEffect(() => {
		if (filterData) {
			handleSearch(true);
		}
	}, [filterData])

	// React.useEffect(() => {
	// 	// if (fields && (chosenBranch && chosenBranch.length || chosenSupplier && chosenSupplier.length)) {
	// 	// 	setIsGridLoading(false)
	// 	// 	setTimeout(() => {
	// 	// 		setIsGridLoading(true)
	// 	// 	}, 1);
	// 	// }
	// }, [fields])


	React.useEffect(() => {
		if (typeof orderReview == 'object') {
			sortData()
		}
	}, [orderReview])
	
	const setFieldsItems = (isNotFull?:boolean) =>{
		setFields([
			{
				text: "barcode", type: 'string', width: window.innerWidth * 0.3, minWidth: 110,
				cellRenderer: 'ClickableCell', clickableCells: {
					aboveGridFilter: true
				}, filter: 'agMultiColumnFilter', filterParams: {
					filters: [{ filter: 'agTextColumnFilter' }, { filter: 'agSetColumnFilter', filterParams: { suppressMiniFilter: true } }]
				}
			},
			{
				text: "productName", type: 'string', width: window.innerWidth * 0.3, minWidth: 100, filter: 'agMultiColumnFilter', filterParams: {
					filters: [{ filter: 'agTextColumnFilter' }, { filter: 'agSetColumnFilter', filterParams: { suppressMiniFilter: true } }]
				}
			},
			{
				text: "branch", type: 'string', width: window.innerWidth * 0.3, minWidth: 70, filter: 'agMultiColumnFilter', filterParams: {
					filters: [{ filter: 'agTextColumnFilter' }, { filter: 'agSetColumnFilter', filterParams: { suppressMiniFilter: true } }]
				}, cellRenderer: 'ClickableCell', clickableCells: {
					aboveGridFilter: true
				},
			},
			{
				text: "supplier", type: 'string', width: window.innerWidth * 0.3, minWidth: 90, cellRenderer: 'ClickableCell', clickableCells: {
					aboveGridFilter: true
				}, filter: 'agMultiColumnFilter', filterParams: {
					filters: [{ filter: 'agTextColumnFilter' }, { filter: 'agSetColumnFilter', filterParams: { suppressMiniFilter: true } }]
				}
			},
			{
				text: "OrderDate", type: 'Date', width: window.innerWidth * 0.3, minWidth: 90, cellRenderer: 'ClickableCell', clickableCells: {
					aboveGridFilter: true
				}, filter: 'agMultiColumnFilter', filterParams: {
					filters: [{ filter: 'agTextColumnFilter' }, { filter: 'agSetColumnFilter', filterParams: { suppressMiniFilter: true } }]
				}
			},
			{
				text: "OrderNum", type: 'numberDisplayZero', width: window.innerWidth * 0.3, minWidth: 90, cellRenderer: 'ClickableCell', clickableCells: {
					aboveGridFilter: [{field:'branch',id:'branchId'}, {field:'supplier',id:'supplierId'}], defaultSelected: orderNumClicked ? true : false
				}, filter: 'agMultiColumnFilter', filterParams: {
					filters: [{ filter: 'agTextColumnFilter' }, { filter: 'agSetColumnFilter', filterParams: { suppressMiniFilter: true } }]
				}
			},
			{ text: "packingFactor", type: 'string', width: window.innerWidth * 0.3, minWidth: 90, },
			{ text: "Shelf", type: 'numberDisplayZero', width: window.innerWidth * 0.3, minWidth: 60, },
			{ text: "stockInOrder", type: 'numberDisplayZero', width: window.innerWidth * 0.3, minWidth: 90, },
			{ text: "stockOnTheWay", type: 'numberDisplayZero', width: window.innerWidth * 0.3, minWidth: 90, },
			{ text: "expectedSales", type: 'numberDisplayZero', width: window.innerWidth * 0.3, minWidth: 90, },
			{ text: "Reccomendation", type: 'numberDisplayZero', width: window.innerWidth * 0.3, minWidth: 90, },
			{ text: "Ordered", type: 'numberDisplayZero', width: window.innerWidth * 0.3, minWidth: 90, },
			{ text: "Supplied", type: 'numberDisplayZero', width: window.innerWidth * 0.3, minWidth: 90, },
			{ text: "Sold", redHeader: isNotFull,type: 'numberDisplayZero', width: window.innerWidth * 0.3, minWidth: 70, },
			{ text: "P_Prediction", type: 'percentage', width: window.innerWidth * 0.3, minWidth: 90, },
			{ text: "P_Reccomendation", type: 'percentage', width: window.innerWidth * 0.3, minWidth: 90, },
			{ text: "P_Supply", type: 'percentage', width: window.innerWidth * 0.3, minWidth: 90, },
			{ text: "stockBeforeOrderClose", type: 'numberDisplayZero', width: window.innerWidth * 0.3, minWidth: 70, },
			{ text: "yitra", type: 'numberDisplayZero', width: window.innerWidth * 0.3, minWidth: 90, },
		])
	}

	const sortData = () => {
		setIsGridLoading(true)
		let _catalogFullData: any = {}
		catalogFullData.forEach((pro: any) => {
			_catalogFullData[pro.BarCode] = pro
		})

		if(orderReview[orderReview.length-1] && orderReview[orderReview.length-1].not_full == 1 ){
			setFieldsItems(true)
		}else{
			setFieldsItems(false)
		}

		let orderReviewMap = orderReview.map((item: any) => {
			let barcode = item.BarCode;
			let productName = dataWithIdKey.catalog[barcode]
			let branch = dataWithIdKey.userBranches[item.BranchId]
			let supplier = dataWithIdKey.suppliers[item.SapakId] ? dataWithIdKey.suppliers[item.SapakId] : dataWithIdKey.subSuppliers[item.SapakId]
			let supplierId = item.SapakId
			let branchId = item.BranchId
			let OrderDate = item.OrderDate;
			let OrderNum = item.OrderNum;
			let packingFactor = item.Ariza;
			let Shelf = item.madaf
			let stockInOrder = Math.round(item.lastyitra)
			let stockOnTheWay = item.AmountOrderWay
			let expectedSales = item.Zefi
			let Reccomendation = item.AmountOrderOld
			let Ordered = item.AmountOrder
			let Supplied = item.Bamount
			let Sold = Math.round(item.Samount)
			let P_Prediction = Math.round((item.Ahuz / 1) * 100)
			let P_Reccomendation = Math.round((item.AhuzM / 1) * 100)
			let P_Supply = Math.round((item.AhuzS / 1) * 100)
			let stockBeforeOrderClose = Math.round(item.MyYitra)
			let yitra = Math.round(item.YitraMlay)
			let allBarcodeData = _catalogFullData[item.BarCode]
			let otherData = [{ subGroupId: allBarcodeData.SubGroupId, groupId: allBarcodeData.GroupId, divisionId: allBarcodeData.ClassesId, seriesId: allBarcodeData.DegemId }]
			return {
				branchId, supplierId, barcode, productName, branch, supplier, OrderDate, OrderNum, packingFactor, Shelf, stockOnTheWay, expectedSales, stockInOrder, Reccomendation, Ordered, Supplied, Sold,
				P_Prediction, P_Reccomendation, P_Supply, stockBeforeOrderClose, yitra,subGroupId: allBarcodeData.SubGroupId, groupId: allBarcodeData.GroupId, divisionId: allBarcodeData.ClassesId, seriesId: allBarcodeData.DegemId, otherData
			}
		})

		setRows(orderReviewMap)
		// setRows(advancedSearch ? advancedFilter : orderReviewMap ? orderReviewMap : [])
		setTimeout(() => {
			setIsGridLoading(false)
		}, 1);


			setTimeout(() => {	
		if (orderNumClicked && orderNumClicked > 0) {
			let getOrderData = orderReviewMap.find((i:any)=>i.OrderNum == orderNumClicked )
			let orderBranch = getOrderData.branch
			let orderBranchId = getOrderData.branchId
			let orderSupplier = getOrderData.supplier
			let orderSupplierId = getOrderData.supplierId
			setDefaultAboveGridFilter([...[ [{ cannotRemove: true,name: 'branch',key:'branchId', preText: formatMessage({ id: 'branch' }), value: orderBranchId, label: orderBranch }],
			[{ cannotRemove: true, name: 'supplier',key:'supplierId', preText: formatMessage({ id: 'supplier' }), value: orderSupplierId, label: orderSupplier }],
				[{ name: 'OrderNum', key:'OrderNum',preText: formatMessage({ id: 'OrderNum' }), value: orderNumClicked, label: orderNumClicked }]]])
		}
		else if(advancedSearch){
			setDefaultAboveGridFilter([])
			createAboveFilterFromDrawer()
		} else{
			setDefaultAboveGridFilter([chosenBranch ? chosenBranch : {}, chosenSupplier ? chosenSupplier : {}])
		}
	}, 1);
	}

	const createAboveFilterFromDrawer = () => {
		let aboveFilter: any = []
		let tabs = filterData.newData.tabs[0]
		tabs.formGroup.forEach((fGroup: any) => {
			if (fGroup.fields && fGroup.fields.length) {
				fGroup.fields.forEach((field: any) => {
					switch (field.name) {
						case 'date':
							break;
						case 'OrderNum':
							if (field.value.length) aboveFilter.push([{ name: 'OrderNum',key: 'OrderNum', preText: formatMessage({ id: 'OrderNum' }), label: field.value, value: field.value }])
							break;
						case 'barcode':
							if (typeof field.value != 'string' && field.value) {
								let valueArr: any = []
								field.value.forEach((value: any) => {
									valueArr.push({ name: 'barcode', label: Number(value.value),key:'barcode', value: value.value, preText: formatMessage({ id: 'barcode' }) })
								})
								aboveFilter.push(valueArr)
							}
							break;
						case 'branch':
							if (field.defaultValue && field.defaultValue.length) {
								let valueArr: any = []
								field.defaultValue.forEach((value: any) => {
									valueArr.push({ name: 'branch', key: 'branchId',cannotRemove: true, label: value.label, value: value.value, preText: formatMessage({ id: 'branch' }) })
								})
								aboveFilter.push(valueArr)
							} else if (typeof field.value != 'string') {
								let valueArr: any = []
								field.value.forEach((value: any) => {
									valueArr.push({ name: 'branch', key:'branchId',cannotRemove: true, label: value.label, value: value.value, preText: formatMessage({ id: 'branch' }) })
								})
								aboveFilter.push(valueArr)
							}
							break;
						case 'supplier':
							if (field.defaultValue && field.defaultValue.length) {
								let valueArr: any = []
								field.defaultValue.forEach((value: any) => {
									valueArr.push({ name: 'supplier', cannotRemove: true, key: 'supplierId',label: value.label, value: value.value, preText: formatMessage({ id: 'supplier' }) })
								})
								aboveFilter.push(valueArr)
							} else if (typeof field.value != 'string') {
								let valueArr: any = []
								field.value.forEach((value: any) => {
									valueArr.push({ name: 'supplier', cannotRemove: true, key: 'supplierId',label: value.label, value: value.value, preText: formatMessage({ id: 'supplier' }) })
								})
								aboveFilter.push(valueArr)
							}
							break;
						case "division":
							if (typeof field.value != 'string' && field.value) {
								let valueArr: any = []
								field.value.forEach((value: any) => {
									valueArr.push({ name: 'division',  key: 'divisionId', label: value.label, value: value.value, preText: formatMessage({ id: 'division' }) })
								})
								aboveFilter.push(valueArr)
							}
							break;
						case "groups":
							if (typeof field.value != 'string' && field.value) {
								let valueArr: any = []
								field.value.forEach((value: any) => {
									valueArr.push({ name: 'groups', key: 'groupId' , label: value.label, value: value.value, preText: formatMessage({ id: 'group' }) })
								})
								aboveFilter.push(valueArr)
							}
							break;
						case "subgroups":
							if (typeof field.value != 'string' && field.value) {
								let valueArr: any = []
								field.value.forEach((value: any) => {
									valueArr.push({ name: 'subgroup', key:'subgroupId', label: value.label, value: value.value, preText: formatMessage({ id: 'subgroup' }) })
								})
								aboveFilter.push(valueArr)
							}
							break;
						case "series":
							if (typeof field.value != 'string' && field.value) {
								let valueArr: any = []
								field.value.forEach((value: any) => {
									valueArr.push({ name: 'series', key: 'seriesId',label: value.label, value: value.value, preText: formatMessage({ id: 'series' }) })
								})
								aboveFilter.push(valueArr)
							}
							break;
						default:
							break;
					}
				})
			}
		})
		setDefaultAboveGridFilter(aboveFilter)
	}

	const afterAboveFilterRemovedOrAddedNew = (data: any, type: string) => {
		let newTempData: any = {}
		data.forEach((d: any) => {
			newTempData[d.field] = { name: d.field, preText: formatMessage({ id: d.field }), data: [...d.data] }
		})
		if (newTempData && newTempData.branch && newTempData.branch.data) setChosenBranch(newTempData.branch.data.map((i: any) => {
			return { name: 'branch', key:'branchId',dontFilter: false, cannotRemove: true, label: i.label, value: i.value, preText: formatMessage({ id: 'branch' }) }
		}))
		else setChosenBranch(null)
		if (newTempData && newTempData.supplier && newTempData.supplier.data) setChosenSupplier(newTempData.supplier.data.map((i: any) => {
			return { name: 'supplier',key:'supplierId', dontFilter: false, cannotRemove: true, label: i.label, value: i.value, preText: formatMessage({ id: 'supplier' }) }
		}))
		else setChosenSupplier(null)
		setTempData(newTempData)

		// if (type != 'addedFilter') {
		// 	let newRows = firstSearchedRows.filter((row: any) => {
		// 		let trueAm: number = 0
		// 		let totalChecks: number = 0
		// 		if (newTempData.branch) {
		// 			totalChecks++
		// 			newTempData.branch.data.forEach((d: any) => { if (d.value == row.branchId) trueAm++ })
		// 		}
		// 		if (newTempData.supplier) {
		// 			totalChecks++
		// 			newTempData.supplier.data.forEach((d: any) => { if (d.value == row.supplierId) trueAm++ })
		// 		}
		// 		if (newTempData.groups) {
		// 			totalChecks++
		// 			newTempData.groups.data.forEach((d: any) => { if (d.value == row.otherData[0].groupId) trueAm++ })
		// 		}
		// 		if (newTempData.subgroups) {
		// 			totalChecks++
		// 			newTempData.subgroups.data.forEach((d: any) => { if (d.value == row.otherData[0].subGroupId) trueAm++ })
		// 		}
		// 		if (newTempData.division) {
		// 			totalChecks++
		// 			newTempData.division.data.forEach((d: any) => { if (d.value == row.otherData[0].divisionId) trueAm++ })
		// 		}
		// 		if (newTempData.series) {
		// 			totalChecks++
		// 			newTempData.series.data.forEach((d: any) => { if (d.value == row.otherData[0].seriesId) trueAm++ })
		// 		}
		// 		if (newTempData.barcode) {
		// 			totalChecks++
		// 			newTempData.barcode.data.forEach((d: any) => { if (Number(d.value) == Number(row.barcode)) trueAm++ })
		// 		}
		// 		if (newTempData.OrderNum) {
		// 			totalChecks++
		// 			if ( newTempData.OrderNum.data[0].value == row.OrderNum ) trueAm++
		// 		}
		// 		if (newTempData.OrderDate) {
		// 			totalChecks++
		// 			if (newTempData.OrderDate.data[0].value == row.OrderDate) trueAm++
		// 		}
		// 		return trueAm === totalChecks
		// 	})

		// 	let newAboveGridFilter: any = []
		// 	addToAbove(newTempData, 'branch', newAboveGridFilter, 'branch', 'branch', false, originalSearch && originalSearch.snif && originalSearch.snif.length ? true : false)
		// 	addToAbove(newTempData, 'supplier', newAboveGridFilter, 'supplier', 'supplier', false, originalSearch && originalSearch.sapak && originalSearch.sapak.length ? true : false)
		// 	addToAbove(newTempData, 'groups', newAboveGridFilter, 'group', 'group', false, false)
		// 	addToAbove(newTempData, 'subgroups', newAboveGridFilter, 'subgroup', 'subgroup', false, false)
		// 	addToAbove(newTempData, 'division', newAboveGridFilter, 'division', 'division', false, false)
		// 	addToAbove(newTempData, 'series', newAboveGridFilter, 'series', 'series', false, false)
		// 	addToAbove(newTempData, 'barcode', newAboveGridFilter, 'barcode', 'barcode', false, false)
		// 	addToAbove(newTempData, 'OrderDate', newAboveGridFilter, 'OrderDate', 'OrderDate', false, false, 'Date')
		// 	addToAbove(newTempData, 'OrderNum', newAboveGridFilter, 'OrderNum', 'OrderNum', false, false)

		// 	setDefaultAboveGridFilter(newAboveGridFilter)
		// 	setRows(newRows)
		// }
	}
	const addToAbove = (arr: any, subName: any, newArr: any, name: string, preText: string, filter: boolean, cannotRemove: boolean, type?: string) => {
		if (arr[subName] && arr[subName].data && arr[subName].data.length) {
			let temp: any = []
			arr[subName].data.forEach((i: any) => {
				temp.push({ type: type, name: name, dontFilter: filter, cannotRemove, label: i.label, value: i.value, preText: formatMessage({ id: preText }) })
			})
			newArr.push(temp)
		}
	}

	return (<>
		<div className='width-100-per'>
			{!firstLoad ?
				<AgGrid
					successDark={'all'}
					cellGreenFields={['barcode', 'branch', 'supplier', 'OrderDate', 'OrderNum']}
					autoSelectedAboveGridFilter={defaultAboveGridFilter}
					afterAboveFilterRemovedOrAdded={afterAboveFilterRemovedOrAddedNew}
					dontFilterAbove={true}
					translateHeader
					resizable
					gridHeight={'70vh'}
					floatFilter
					fields={fields}
					groups={[]}
					totalRows={[]}
					rows={rows}
					checkboxFirstColumn={false}
					pagination
					displayLoadingScreen={isGridLoading}
				/>
				: <div>
					{isGridLoading ? <Spinner /> : null}
				</div>
			}
		</div>
	</>);
};

export default Item;
