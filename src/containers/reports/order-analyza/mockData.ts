import { InputTypes } from "@src/components/form-group";
import { useIntl } from "react-intl"



export const orderDrawerData = (tempData: any, data: any, date: any, updateSelectedDefault: (newData: any, type: string) => void) => {
	const { userBranches, suppliers, subSuppliers, dataWithIdKey } = data

	const { formatMessage } = useIntl();

	let subSuppliersRemoveDup = []
	subSuppliersRemoveDup = subSuppliers.filter((subS: any, index: any, self: any) =>
		index === self.findIndex((t: any) => (
			t.SubsapakName === subS.SubsapakName
		))
	)
	let drawerData: any = {
		tabs: [{
			tabId: '1',
			title: 'Item details',
			formGroup: [
				{
					formId: '1',
					fields: [
						{
							label: formatMessage({ id: "date" }),
							placeholder: formatMessage({ id: "date" }),
							value: date,
							name: 'date',
							type: InputTypes.DATE,
							// dateData: date,
							options: [],
							columns: 2,
							isSelected: false,
							calanderId: 'dateCalendarId1',
							inputId: 'dateInputID1'


						}, {
							label: formatMessage({ id: "OrderNum" }),
							placeholder: formatMessage({ id: "OrderNum" }),
							value: tempData ? tempData.OrderNum && tempData.OrderNum.data ? String(tempData.OrderNum.data[0].label) : '' : '',
							// value: tempData ? tempData.OrderNum ? tempData.OrderNum : '' : '',
							name: 'OrderNum',
							// defaultValue: tempData ? tempData.orderNum : null,
							type: InputTypes.NUMBER,
							options: [],
							columns: 2,
							isSelected: false

						}
					]
				}, {
					title: null,
					formId: '2',
					fields: [
						{
							label: formatMessage({ id: "supplier" }),
							value: formatMessage({ id: "supplier" }),
							placeholder: formatMessage({ id: "supplier" }),
							onChangeFunc: (newSelected: any, name: string) => updateSelectedDefault(newSelected, name),
							defaultValue: tempData ? tempData.supplier && tempData.supplier.data && tempData.supplier.data : null,
							name: 'supplier',
							isMulti: true,
							type: InputTypes.SELECT,
							options: [...suppliers.map((item: any) => { return { value: item.Id, label: item.Name, name: 'supplier' } }), ...subSuppliersRemoveDup.map((sub: any) => {
								return { value: sub.SubsapakId, label: sub.SubsapakName, name: 'supplier' }
							})],
							columns: 2,
							isSelected: false

						},
						{
							label: formatMessage({ id: "branch" }),
							value: formatMessage({ id: "allBranches" }),
							placeholder: formatMessage({ id: "allBranches" }),
							defaultValue: tempData ? tempData.branch && tempData.branch.data && tempData.branch.data : null,
							onChangeFunc: (newSelected: any, name: string) => updateSelectedDefault(newSelected, name),
							name: 'branch',
							isMulti: true,
							type: InputTypes.SELECT,
							options: userBranches.map((item: any) => { return { value: item.BranchId, label: item.Name, name: 'branch' } }),
							columns: 2,
							isSelected: false

						},
					]
				}]
		}]
	}
	return drawerData
}

const getRowsByKeyAndId = (rows: any, tempData: any) => {
	let supplierIds: any = []
	let groupsIds: any = []
	let subGroupIds: any = []
	let divisionIds: any = []
	let seriesIds: any = []
	// let barcodesIds: any = []

	let selectedSuppliers = tempData && tempData.supplier && tempData.supplier.data && tempData.supplier.data.length ? tempData.supplier.data.map((i: any) => { return i.value }) : []
	let selectedDivsions = tempData && tempData.division && tempData.division.data && tempData.division.data.length ? tempData.division.data.map((i: any) => { return i.value }) : []
	let selectedGroups = tempData && tempData.groups && tempData.groups.data && tempData.groups.data.length ? tempData.groups.data.map((i: any) => { return i.value }) : []
	let selectedSubGroups = tempData && tempData.subgroups && tempData.subgroups.data && tempData.subgroups.data.length ? tempData.subgroups.data.map((i: any) => { return i.value }) : []



	for (let i = 0; i < rows.length; i++) {
		let product = rows[i]
		let supplierId = product.SapakId
		let groupId = product.GroupId
		let subGroupId = product.SubGroupId
		let divisionId = product.ClassesId
		let seriesId = product.DegemId


		if ((selectedSuppliers.length && selectedSuppliers.includes(supplierId)) || !selectedSuppliers.length) {
			if (!divisionIds.includes(divisionId)) divisionIds.push(divisionId)
		}

		if ((!selectedDivsions.length && divisionIds.includes(divisionId)) || selectedDivsions.includes(divisionId)) {
			if (!groupsIds.includes(groupId)) groupsIds.push(groupId)
		}

		if ((!selectedGroups.length && groupsIds.includes(groupId)) || selectedGroups.includes(groupId)) {
			if (!subGroupIds.includes(subGroupId)) subGroupIds.push(subGroupId)
		}

		if ((!selectedSubGroups.length && subGroupIds.includes(subGroupId)) || selectedSubGroups.includes(subGroupId)) {
			if (!seriesIds.includes(seriesId)) seriesIds.push(seriesId)
		}

		if(seriesIds.includes(seriesId) && subGroupIds.includes(subGroupId) && groupsIds.includes(groupId) && divisionIds.includes(divisionId)){
			if (!supplierIds.includes(supplierId)) supplierIds.push(supplierId)
		}

	}

	return { supplierIds, groupsIds, subGroupIds, divisionIds, seriesIds }
}


export const itemDrawerData = (tempData: any, data: any, date: any, updateSelectedDefault: (newData: any, type: string) => void) => {
	let { catalog, userBranches, suppliers, groups, chosenGroup, divisions, subGroups, series, subSuppliers } = data

	let newOptions = getRowsByKeyAndId(catalog, tempData)

	catalog = catalog.filter((i: any) => {
		if(newOptions.supplierIds.includes(i.SapakId) &&
		newOptions.groupsIds.includes(i.GroupId) &&
		newOptions.subGroupIds.includes(i.SubGroupId) &&
		newOptions.seriesIds.includes(i.DegemId) &&
		newOptions.divisionIds.includes(i.ClassesId)) return true
		else return false
	})


	suppliers = suppliers.filter((i: any) => { return newOptions.supplierIds.includes(i.Id) })
	groups = groups.filter((i: any) => { return newOptions.groupsIds.includes(i.Id) })
	subGroups = subGroups.filter((i: any) => { return newOptions.subGroupIds.includes(i.Id) })
	series = series.filter((i: any) => { return newOptions.seriesIds.includes(i.Id) })
	divisions = divisions.filter((i: any) => { return newOptions.divisionIds.includes(i.Id) })


	let _suppliers: any = {}
	let _subSuppliers: any = {}
	let _userBranches: any = {}
	let _catalog: any = {}
	suppliers.forEach((supplier: any) => _suppliers[supplier.Id] = supplier.Name)
	subSuppliers.forEach((subSupplier: any) => _subSuppliers[subSupplier.SubsapakId] = subSupplier)
	userBranches.forEach((branch: any) => _userBranches[branch.BranchId] = branch.Name)
	catalog.forEach((item: any) => _catalog[item.BarCode] = item.Name)


	let subSuppliersRemoveDup = subSuppliers.filter((subS: any, index: any, self: any) =>
		index === self.findIndex((t: any) => (
			t.SubsapakName === subS.SubsapakName
		))
	)

	const { formatMessage } = useIntl();

	let drawerData: any = {
		tabs: [{
			tabId: '1',
			title: 'Item details',
			formGroup:
				[
					{
						title: '',
						formId: '1',
						fields: [
							{
								label: formatMessage({ id: "date" }),
								placeholder: formatMessage({ id: "date" }),
								value: date,
								name: 'date',
								type: InputTypes.DATE,
								// dateData: date,
								options: [],
								columns: 2,
								isSelected: false,
								calanderId: 'dateCalendarId',
								inputId: 'dateInputID'


							},
							{
								label: formatMessage({ id: "OrderNum" }),
								placeholder: formatMessage({ id: "OrderNum" }),
								value: tempData ? tempData.OrderNum && tempData.OrderNum.data ? String(tempData.OrderNum.data[0].label) : '' : '',
								name: 'OrderNum',
								// defaultValue: tempData ? tempData.orderNum : null,
								type: InputTypes.NUMBER,
								options: [],
								columns: 2,
								isSelected: false

							}
						]

					},
					{
						title: '',
						formId: '2',
						fields: [
							{
								label: formatMessage({ id: "item" }),
								value: formatMessage({ id: "itemSearch" }),
								placeholder: formatMessage({ id: "itemSearch" }),
								onChangeFunc: (newSelected: any, name: string) => updateSelectedDefault(newSelected, name),
								dontChangeTemp: true,
								defaultValue: tempData ? tempData.barcode && tempData.barcode.data && tempData.barcode.data.map((i: any) => {
									return {
										value: String(i.value), label: i.label
									}
								}) : null,
								name: 'barcode',
								type: InputTypes.SELECT,
								isMulti: true,
								options: catalog.map((item: any) => {
									return {
										value: String(item.BarCode), label: `${item.Name} ${item.BarCode}`
									}
								}),
								columns: 2,
								isSelected: false


							}, {
								label: formatMessage({ id: "branch" }),
								value: formatMessage({ id: "allBranches" }),
								placeholder: formatMessage({ id: "allBranches" }),
								defaultValue: tempData ? tempData.branch && tempData.branch.data && tempData.branch.data : null,
								onChangeFunc: (newSelected: any, name: string) => updateSelectedDefault(newSelected, name),
								name: 'branch',
								isMulti: true,
								type: InputTypes.SELECT,
								options: userBranches.map((item: any) => { return { value: item.BranchId, label: item.Name, name: 'branch' } }),
								columns: 2,
								isSelected: false

							}, {
								label: formatMessage({ id: "supplier" }),
								value: formatMessage({ id: "supplier" }),
								placeholder: formatMessage({ id: "supplier" }),
								onChangeFunc: (newSelected: any, name: string) => updateSelectedDefault(newSelected, name),
								defaultValue: tempData ? tempData.supplier && tempData.supplier.data && tempData.supplier.data : null,
								name: 'supplier',
								isMulti: true,
								type: InputTypes.SELECT,
								options: [...suppliers.map((item: any) => { return { value: item.Id, label: item.Name, name: 'supplier' } }), ...subSuppliersRemoveDup.map((sub: any) => {
									return { value: sub.SubsapakId, label: sub.SubsapakName, name: 'supplier' }
								})],
								isSelected: false

							}, {
								label: formatMessage({ id: "division" }),
								value: formatMessage({ id: "allDivisions" }),
								placeholder: formatMessage({ id: "allDivisions" }),
								defaultValue: tempData ? tempData.division && tempData.division.data && tempData.division.data : null,
								name: 'division',
								isMulti: true,
								type: InputTypes.SELECT,
								options: divisions.map((item: any) => { return { value: item.Id, label: item.Name } }),
								columns: 2,
								isSelected: false

							},

							{
								label: formatMessage({ id: "group" }),
								value: formatMessage({ id: "allGroups" }),
								placeholder: formatMessage({ id: "allGroups" }),
								defaultValue: tempData ? tempData.groups && tempData.groups.data && tempData.groups.data : null,
								name: 'groups',
								isMulti: true,
								type: InputTypes.SELECT,
								onChangeFunc: (newSelected: any, name: string) => updateSelectedDefault(newSelected, name),
								options: groups.map((item: any) => { return { value: item.Id, label: item.Name } }),
								columns: 2,
								isSelected: false

							}, {
								label: formatMessage({ id: "subgroup" }),
								value: formatMessage({ id: "allSubGroups" }),
								placeholder: formatMessage({ id: "allSubGroups" }),
								defaultValue: tempData ? tempData.subgroups && tempData.subgroups.data && tempData.subgroups.data : null,
								name: 'subgroups',
								isMulti: true,
								type: InputTypes.SELECT,
								options: 
									subGroups && subGroups.length ?
										subGroups.map((option: any, index: number) => {
											return { index, value: option.Id, label: option.Name }
										})
										: [],
								columns: 2,
								isSelected: false

							},
							{
								label: formatMessage({ id: "series" }),
								value: formatMessage({ id: "allSeries" }),
								placeholder: formatMessage({ id: "allSeries" }),
								onChangeFunc: (newSelected: any, name: string) => updateSelectedDefault(newSelected, name),
								defaultValue: tempData ? tempData.series && tempData.series.data && tempData.series.data : null,
								name: 'series',
								isMulti: true,
								type: InputTypes.SELECT,
								options: series.map((item: any) => { return { value: item.Id, label: item.Name } }),
								columns: 2,
								isSelected: false

							}
						]
					},
					{
						title: '',
						formId: '3',
						fields: [
							{
								label: formatMessage({ id: "stockType" }),
								value: formatMessage({ id: "allStockTypes" }),
								placeholder: formatMessage({ id: "allStockTypes" }),
								name: 'stockType',
								isMulti: true,
								type: InputTypes.SELECT,
								options: [],
								columns: 2,
								isSelected: false


							}, {
								label: formatMessage({ id: "holidays" }),
								value: formatMessage({ id: "allHolidays" }),
								placeholder: formatMessage({ id: "allHolidays" }),
								name: 'holidays',
								isMulti: true,
								type: InputTypes.SELECT,
								options: [],
								columns: 2,
								isSelected: false
							},
							{
								label: formatMessage({ id: "seasons" }),
								value: formatMessage({ id: "allSeasons" }),
								placeholder: formatMessage({ id: "allSeasons" }),
								name: 'seasons',
								isMulti: true,
								type: InputTypes.SELECT,
								options: [],
								columns: 2,
								isSelected: false
							}
						]
					}
				]

		}]
	}
	return drawerData
}






