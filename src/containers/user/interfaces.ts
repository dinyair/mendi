import { UserActionTypes } from './enums';

export interface UserInterface {
	id: any;
	name: string;
	company_id: number;
	email: string
	active: boolean;
	permissions: any;
	db: string;
	lang: string | null
	snif: number | undefined;
	snif_arr: any[number]
}

export interface UsersActionInterface {
	type: UserActionTypes;
	payload: any;
}



export interface UsersActionInterface {
	type: UserActionTypes;
	payload: any;
}

export interface UserActionInterface {
	type: UserActionTypes;
	payload: any;
}




