import { rejects } from "assert";
import { getRequest, postRequest } from "@src/http";
import Cookies from 'universal-cookie';
import * as moment from 'moment';

const cookies = new Cookies();


export const logOut =async (_callback?: Function, keepCookies ? : boolean) => {
	serverLogout().then(res => {
		cookies.remove('user')
		cookies.remove('token')
	    cookies.remove('connect.sid')
	})

	// localStorage.removeItem('user')
	// localStorage.removeItem('token')
	if (_callback) _callback()
}


export const serverLogout = () => new Promise((resolve, reject) => {
	getRequest('logout')
		.then((res: any) => {
			if (res.status !== 'error') resolve(res)
		})
})

export const checkResetPasswordDiff = (user:any) => {
	let resetDate = moment(user.passwordResetDate, "YYYY-MM-DD")
	let now = moment();
	return now.diff(resetDate, 'days');
}

export const isAuth = () => {
	const user = cookies.get('user')
	const token = cookies.get('token')
	return user && token ? user : undefined
}


export const getBranches = () => new Promise((resolve, reject) => {
	getRequest('planogram/data/branches')
		.then((res: any) => {
			if (res.status !== 'error') resolve(res)
		})
})

export const getGroups = () => new Promise((resolve, reject) => {
	getRequest('getGroup')
		.then((res: any) => {
			if (res.status !== 'error') resolve(res)
		})
})

export const getSubGroups = () => new Promise((resolve, reject) => {
	getRequest('getSubGroup')
		.then((res: any) => {
			if (res.status !== 'error') resolve(res)
		})
})

export const getNewPlano = ({ copy, updatedb, newplano, aisle_id, date_flag, fromDate, toDate, branches, layoutId, groups, subgroups }: any) => new Promise((resolve, reject) => {
	postRequest('getNewPlano', {
		copy,
		updatedb,
		aisle_id,
		newplano,
		date_flag,
		fromDate,
		toDate,
		branches,
		layoutId,
		groups: subgroups && subgroups.length ? null : groups,
		subgroups: subgroups ? subgroups : null
	})
		.then((res: any) => { if (res) { resolve(res) } else reject() })
})


export const getCatalog = () => new Promise((resolve, reject) => {
	getRequest('planogram/catalog')
		.then((res: any) => {
			if (res.status !== 'error') resolve(res)
		})
})

export const getAllSuppliers = () => new Promise((resolve, reject) => {
	getRequest('AppSapakim')
		.then((res: any) => {
			if (res.status !== 'error') resolve(res)
		})
})
export const getAllSubSuppliers = () => new Promise((resolve, reject) => {
	getRequest('AppGetSubSapak')
		.then((res: any) => {
			if (res.status !== 'error') resolve(res)
		})
})

export const userInitialState: any[] = [

];
