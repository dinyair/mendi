import { userInitialState } from './initial-state';
import { UserInterface, UserActionInterface } from './interfaces';
import { UserActionTypes } from './enums';



export default (state = userInitialState, { type, payload }: any | UserActionInterface): any | UserInterface[] => {
	switch (type) {
		
		case UserActionTypes.SET_USER:
			return payload
		case UserActionTypes.SET_USER_PERMISSIONS:
			return {...state, permissions: payload}


		default:
			return state;
	}
};

