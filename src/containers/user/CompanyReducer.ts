import { Divisions } from '@src/redux/actions/divisions/divisionsActions';
import { Groups } from '@src/redux/actions/groups/groupsAction';
import { CompanyActionTypes } from './enums';

export const userBranchesReducer = (
	state: any[] = [],
	{ type, payload }: { type: any; payload: any[] }): any[] => {
	switch (type) {
		case CompanyActionTypes.SET__USER__BRANCHES:
			return payload;
		default:
			return state;
	}
};


export const divisionsReducer = (
	state: Divisions[] = [],
	{ type, payload }: { type: any; payload: Divisions[] }): Divisions[] => {
	switch (type) {
		case CompanyActionTypes.SET__DIVISIONS:
			return payload;
		default:
			return state;
	}
};
export const seriesReducer = (
	state: any[] = [],
	{ type, payload }: { type: any; payload: any[] }): any[] => {
	switch (type) {
		case CompanyActionTypes.SET__SERIES:
			return payload;
		default:
			return state;
	}
};


export const layoutsReducer = (
	state: any[] = [],
	{ type, payload }: { type: any; payload: any[] }): any[] => {
	switch (type) {
		case CompanyActionTypes.SET_LAYOUTS:
			return payload;
		default:
			return state;
	}
};


export const modelsReducer = (
	state: any[] = [],
	{ type, payload }: { type: any; payload: any[] }): any[] => {
	switch (type) {
		case CompanyActionTypes.SET_MODELS:
			return payload;
		default:
			return state;
	}
};


export const branchesReducer = (
	state: any[] = [],
	{ type, payload }: { type: any; payload: any[] }): any[] => {
	switch (type) {
		case CompanyActionTypes.SET_BRANCHES:
			return payload;
		default:
			return state;
	}
};

export const storesReducer = (
	state: any[] = [],
	{ type, payload }: { type: any; payload: any[] }): any[] => {
	switch (type) {
		case CompanyActionTypes.SET_STORES:
			return payload;
		default:
			return state;
	}
};


export const catalogReducer = (
	state: any[] = [],
	{ type, payload }: { type: any; payload: any[] }): any[] => {
	switch (type) {
		case CompanyActionTypes.SET_CATALOG:
			return payload;
		default:
			return state;
	}
};

export const catalogUpdatedReducer = (
	state: any[] = [],
	{ type, payload }: { type: any; payload: any[] }): any[] => {
	switch (type) {
		case CompanyActionTypes.SET_CATALOGUP:
			return payload;
		default:
			return state;
	}
};
export const catalogFullDataReducer = (
	state: any[] = [],
	{ type, payload }: { type: any; payload: any[] }): any[] => {
	switch (type) {
		case CompanyActionTypes.SET_CATALOG_FULL_DATA:
			return payload;
		default:
			return state;
	}
};
export const catalogFullDataNoArchiveReducer = (
	state: any[] = [],
	{ type, payload }: { type: any; payload: any[] }): any[] => {
	switch (type) {
		case CompanyActionTypes.SET_CATALOG_FULL_DATA_NO_ARCHIVE:
			return payload;
		default:
			return state;
	}
};

export const orderReducer = (
	state: any[] = [],
	{ type, payload }: { type: any; payload: any[] }): any[] => {
	switch (type) {
		case CompanyActionTypes.SET_ORDER:
			return payload;
		default:
			return state;
	}
};
export const groupsReducer = (
	state: Groups[] = [],
	{ type, payload }: { type: any; payload: Groups[] }): Groups[] => {
	switch (type) {
		case CompanyActionTypes.SET_GROUPS:
			return payload;
		default:
			return state;
	}
};


export const segmentsReducer = (
	state: any[] = [],
	{ type, payload }: { type: any; payload: any[] }): any[] => {
	switch (type) {
		case CompanyActionTypes.SET_SEGMENTS:
			return payload;
		default:
			return state;
	}
};

export const subgroupsReducer = (
	state: any[] = [],
	{ type, payload }: { type: any; payload: any[] }): any[] => {
	switch (type) {
		case CompanyActionTypes.SET_SUBGROUPS:
			return payload;
		default:
			return state;
	}
};


export const logoReducer = (
	state: string = '',
	{ type, payload }: { type: any; payload: string }): string => {
	switch (type) {
		case CompanyActionTypes.SET_LOGO:
			return payload;
		default:
			return state;
	}
};
