import * as React from 'react';
import { AgGrid } from '@components';
import { useIntl } from "react-intl"
import { config } from '@src/config'
import { Icon } from '@components';
import AsyncSelect from "react-select/async";
import Spinner from "@vuexy/spinner/Loading-spinner"
import ExportExcel from '@src/components/ExportExcel/ExportExcel';
import { getAppAspakaItemAll, getCatalogHla, AppDeleteAspakaItem, AppUpdateAspakaItem, getCatalogHlaSub, getSubSupplierGroups } from '../initial-state'
import SupplyItemsModalsData from './supplyItemsModalsData';
import { ModalTypes } from '@src/components/GenericModals/interfaces';
import ImportExcelModal from '../../../../components/import-excel-modal/import-excel-modal';
import { useSelector } from 'react-redux';
import { RootStore } from '@src/store';


interface Props {
	readonly back: () => void;
	readonly supplier: any;
	readonly catalogItems: any;
	readonly appAspakaItems: any;
	readonly appSubSapakAllow: any;
	readonly onUpdateSupplyItem: () => void
}

export const SupplyItemsGrid: React.FC<Props> = (props: Props) => {
	const { formatMessage } = useIntl();
	const { back, supplier } = props;
	const [isOpenModal, setStateModal] = React.useState(false);
	const [isOpenErrorModal, setStateErrorModal] = React.useState(false);

	const user = useSelector((state: RootStore) => state.user)
	const userPermissions = user ? user.permissions : null
	let URL = window.location.pathname
	const userEditFlag = userPermissions && userPermissions[URL] && userPermissions[URL].edit_flag === 1

	const [modalType, setModalType] = React.useState<ModalTypes>(ModalTypes.none)


	const [isDataReady, setIsDataReady] = React.useState<boolean>(false)

	const [inputValue, setInputValue] = React.useState<any>(null);
	const [existProduct, setExistProduct] = React.useState<any>(null);

	const [originalRows, setOriginalRows] = React.useState<any[]>([])
	const [catalogItems, setCatalogItems] = React.useState<any[]>([])
	const [chosenProduct, setChosenProduct] = React.useState<any>(null);

	const [filteredCatalogItems, setfilteredCatalogItems] = React.useState<any>()
	const [rows, setRows] = React.useState<any>()
	const [fields, setFields] = React.useState<any>()

	const supplierData: any = props.supplier;


	React.useEffect(() => {
		getRows()
	}, [])


	const getRows = async () => {
		const catalogItems: any[] = props.catalogItems
		const appAspakaItem: any[] = props.appAspakaItems
		const appSubSapakAllow: any[] = props.appSubSapakAllow

		setCatalogItems(catalogItems)



		const aspakaDays = supplierData.detail.map((ele: any) => ele.AspakaDay)
		// console.log({catalogItems})

		// console.log({aspakaDays})


		if (!supplierData.ClassesId && !supplierData.groupId) {
			const filteredCatalogItemsArr = catalogItems.filter(ele => ele.SapakId === supplierData.SupplierId)
			// console.info({ filteredCatalogItemsArr })
			setfilteredCatalogItems(filteredCatalogItemsArr)
		} else {
			const subSupplierGroups = appSubSapakAllow.filter((ele: any) => ele.SubsapakId === supplierData.SubSupplierId).map(ele => ele.groupId)

			let filteredSubCatalog: any[] = [];
			let combinedSubCatalog: any[] = [];
			for (let index = 0; index < subSupplierGroups.length; index++) {
				const groupId = subSupplierGroups[index]
				filteredSubCatalog = catalogItems.filter((ele: any) => ele.GroupId === groupId && ele.SapakId === supplierData.SupplierId);
				combinedSubCatalog = [...combinedSubCatalog, ...filteredSubCatalog]
			}

			// console.log({combinedSubCatalog})
			// console.log({filterCatalog})
			// console.info({ combinedSubCatalog })
			setfilteredCatalogItems(combinedSubCatalog)
		}

		// console.log({appAspakaItem})
		// console.log({supplierData})


		const filteredSupplyItems = appAspakaItem.filter(ele => ele.BranchId == supplierData['BranchId'] && (ele.SupplierId == supplierData.SupplierId || ele.SupplierId == supplierData.SubSupplierId))

		const rows = filteredSupplyItems.map(ele => {
			return {
				BarCode: ele.BarCode,
				itemName: ele.BarCodeName,
				BranchId: ele.BranchId,
				SupplierId: ele.SupplierId,
				SupplierName: ele.SupplierName,
				Sunday: ele.day_1,
				Monday: ele.day_2,
				Tuesday: ele.day_3,
				Wednesday: ele.day_4,
				Thursday: ele.day_5,
				Friday: ele.day_6,
				Saturday: ele.day_7,
				SundayDisabled: aspakaDays.find((ele: number) => ele === 1) ? false : true,
				MondayDisabled: aspakaDays.find((ele: number) => ele === 2) ? false : true,
				TuesdayDisabled: aspakaDays.find((ele: number) => ele === 3) ? false : true,
				WednesdayDisabled: aspakaDays.find((ele: number) => ele === 4) ? false : true,
				ThursdayDisabled: aspakaDays.find((ele: number) => ele === 5) ? false : true,
				FridayDisabled: aspakaDays.find((ele: number) => ele === 6) ? false : true,
				SaturdayDisabled: aspakaDays.find((ele: number) => ele === 7) ? false : true,
			}
		})
		console.info({ rows })


		// All the rows

		setFieldsDefault()

		setOriginalRows(rows)

		setRows(rows);

		setIsDataReady(true)
	}



	const setFieldsDefault = () => {
		setFields([
			{ text: "BarCode", type: 'string', width: window.innerWidth * 0.03, minWidth: 100, },
			{ text: "itemName", type: 'string', width: window.innerWidth * 0.03, minWidth: 200, },
			{
				text: "Sunday", type: 'CheckBoxCell', width: window.innerWidth * 0.03, minWidth: 30,
				cellRenderer: 'CheckBoxCell', checkBoxCell: {
					value: 'Sunday', isDisabled: userEditFlag ? 'SundayDisabled' : true, onChange: (data: any) =>
						handleCheckBoxChange(data)
				},
			},
			{
				text: "Monday", type: 'CheckBoxCell', width: window.innerWidth * 0.03, minWidth: 30,
				cellRenderer: 'CheckBoxCell', checkBoxCell: {
					value: 'Monday', isDisabled: userEditFlag ? 'MondayDisabled' : true, onChange: (data: any) =>
						handleCheckBoxChange(data)

				},
			},
			{
				text: "Tuesday", type: 'CheckBoxCell', width: window.innerWidth * 0.03, minWidth: 30,
				cellRenderer: 'CheckBoxCell', checkBoxCell: {
					value: 'Tuesday', isDisabled: userEditFlag ? 'TuesdayDisabled' : true, onChange: (data: any) => {
						handleCheckBoxChange(data)
					}
				},
			},
			{
				text: "Wednesday", type: 'CheckBoxCell', width: window.innerWidth * 0.03, minWidth: 30,
				cellRenderer: 'CheckBoxCell', checkBoxCell: {
					value: 'Wednesday', isDisabled: userEditFlag ? 'WednesdayDisabled' : true, onChange: (data: any) => {
						handleCheckBoxChange(data)
					}
				},
			},
			{
				text: "Thursday", type: 'CheckBoxCell', width: window.innerWidth * 0.03, minWidth: 30,
				cellRenderer: 'CheckBoxCell', checkBoxCell: {
					value: 'Thursday', isDisabled: userEditFlag ? 'ThursdayDisabled' : true, onChange: (data: any) => {
						handleCheckBoxChange(data)
					}
				},
			},
			{
				text: "Friday", type: 'CheckBoxCell', width: window.innerWidth * 0.03, minWidth: 30,
				cellRenderer: 'CheckBoxCell', checkBoxCell: {
					value: 'Friday', isDisabled: userEditFlag ? 'FridayDisabled' : true, onChange: (data: any) => {
						handleCheckBoxChange(data)
					}
				},
			},
			{
				text: "Saturday", type: 'CheckBoxCell', width: window.innerWidth * 0.03, minWidth: 30,
				cellRenderer: 'CheckBoxCell', checkBoxCell: {
					value: 'Saturday', isDisabled: userEditFlag ? 'SaturdayDisabled' : true, onChange: (data: any) => {
						handleCheckBoxChange(data)
					}
				},
			},
			{
				text: "blankText", type: 'string', width: window.innerWidth * 0.001, minWidth: 1, noFilter: true,
				cellRenderer: 'IconRender', renderIcons: [
					{ actions: true },
					userEditFlag ?{ type: 'delete', title: 'Delete', onClick: (data: any) => { deleteRow(data) }, returnDataWithProps: true, icon: 'trash.svg' }: null,
				],
			},
		])
	}

	const deleteRow = async (data: any) => {
		// console.info({ data })
		const rows = data.node.parent.allLeafChildren.map((ele: any) => ele.data)
		const row = data.data;
		const filteredRows = [...rows].filter(ele => ele.BarCode !== row.BarCode)



		/* 
			Send delete request to api
		*/

		const deleteAspakaItemOBJ = {
			BarCode: row.BarCode,
			BranchId: row.BranchId
		}
		// console.log({ deleteAspakaItemOBJ })


		let res = await AppDeleteAspakaItem(deleteAspakaItemOBJ)

		props.onUpdateSupplyItem()
		setIsDataReady(false)
		setRows(null)
		setFields([])
		setTimeout(() => {
			setRows(filteredRows)
			setOriginalRows(filteredRows)
			setFieldsDefault()

			setIsDataReady(true)
		}, 1);


	}



	// const toggleErrorModal = () => {
	// 	setStateErrorModal(prevValue => !prevValue);
	// }

	const handleAddModal = async (data: any) => {
		if (data.product) {
			const product = data.product;


			const newRows = [...rows];
			// console.log({ newRows })
			// console.log({prodBar : product.Barcode})
			// console.log({elBar : newRows[0].Barcode})

			const isRowExist = newRows.findIndex(ele => ele.BarCode === product.BarCode)

			if (isRowExist === -1) {

				let Sunday = false, Monday = false, Tuesday = false, Wednesday = false, Thursday = false, Friday = false, Saturday = false;

				// console.info({ data: supplierData.detail })
				for (let index = 0; index < supplierData.detail.length; index++) {
					const element = supplierData.detail[index];

					switch (element.AspakaDay) {
						case 1: Sunday = true; break;
						case 2: Monday = true; break;
						case 3: Tuesday = true; break;
						case 4: Wednesday = true; break;
						case 5: Thursday = true; break;
						case 6: Friday = true; break;
						case 7: Saturday = true; break;
					}
				}

				/* Send data to api */


				const objToAdd = {
					BarCode: product.BarCode, BranchId: supplierData.BranchId, days: [0, Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday]
				}

				await AppUpdateAspakaItem({ rec1: objToAdd });

				const newRow = {
					BarCode: product.BarCode,
					BranchId: supplierData.BranchId, itemName:
						product.Name, SupplierId: supplierData.SupplierId,
					SupplierName: supplierData.SupplierName,
					Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday,
					SundayDisabled: Sunday ? false : true,
					MondayDisabled: Monday ? false : true,
					TuesdayDisabled: Tuesday ? false : true,
					WednesdayDisabled: Wednesday ? false : true,
					ThursdayDisabled: Thursday ? false : true,
					FridayDisabled: Friday ? false : true,
					SaturdayDisabled: Saturday ? false : true,
				}
				newRows.push(newRow)

				// console.info(newRows)

				props.onUpdateSupplyItem()

				setOriginalRows(newRows)
				setRows(null)
				setIsDataReady(false)

				setTimeout(() => {
					setRows(newRows)
					setIsDataReady(true)
					toggleModal(ModalTypes.none)

				}, 1);
			} else {
				// Product already exist
				setExistProduct({ BarCode: product.BarCode, Name: product.Name })
				// setStateErrorModal(true)
				console.log('product exists')
				toggleModal(ModalTypes.none)

				setTimeout(() => {
					toggleModal(ModalTypes.error)
				}, 1);
			}
		}

	}
	const handleCheckBoxChange = async (data: any) => {
		const rows = data.node.parent.allLeafChildren.map((ele: any) => ele.data)
		const changedIndex = rows.findIndex((ele: any) => ele.BarCode == data.data.BarCode)
		rows[changedIndex][data.field] = !data.value

		const row = rows[changedIndex]

		// console.log(rows[changedIndex])

		const objToUpdate = {
			BarCode: row.BarCode, BranchId: row.BranchId, days: [0, row.Sunday, row.Monday, row.Tuesday, row.Wednesday, row.Thursday, row.Friday, row.Saturday]
		}


		/*
		// Send the changed row to the api
		// console.info({rows})
		*/

		await AppUpdateAspakaItem({ rec1: objToUpdate });
		// console.log({ objToUpdate })

		setOriginalRows(rows)
		setRows(null)
		setIsDataReady(false)

		setTimeout(() => {
			setRows(rows)
			setIsDataReady(true)
		}, 1);
	}


	const handleFilter = () => {
		setRows([])
		setIsDataReady(false)
		// console.info({ chosenProduct })
		let filteredRows = [...originalRows];

		// console.info(filteredRows)
		if (chosenProduct) {
			filteredRows = filteredRows.filter(ele => ele.BarCode === chosenProduct)
		}


		setTimeout(() => {
			setRows(filteredRows)
			setIsDataReady(true)
		}, 1);
	}
	const productStyles = {
		container: (base: any) => ({
			...base,
		}),
		option: (provided: any, state: any) => ({
			...provided,
			...provided,

			"&:hover": {
				backgroundColor: "#31baab"
			},
			color: state.isSelected ? '#ffffff' : '#1e1e20',
			backgroundColor: state.isSelected ? '#027b7a' : '#ffffff',
			borderBottom: '1px dotted lightgray',
			padding: 10,
		}),
	}



	const loadCatalogOptions = (inputValue: string, callback: any) => {
		if (inputValue.length >= 3) {
			callback(filterCatalog(inputValue));
		} else {
			callback()
		}
	};


	const filterCatalog = (inputValue: string) => {
		const regex = /\d/;
		const doesContainNumber = regex.test(inputValue)

		const catalogFiltered = rows.filter((i: any) => {
			return i.itemName.includes(inputValue) || String(i.BarCode).includes(inputValue)
		});
		let catalogMap: any[] = []
		catalogFiltered && catalogFiltered[0] ? catalogMap = catalogFiltered.map((option: any, index: number) => {
			return { index, value: option.BarCode, label: `${option.itemName} ${option.BarCode}`, name: option.itemName }
		}) : []

		if (doesContainNumber) {
			catalogMap.sort((a, b) => a.value - b.value)
		} else {
			catalogMap.sort((a, b) => a.label.length - b.label.length)
		}

		return catalogMap.slice(0, 220)
	};

	const handleInputChange = (newValue: string) => {
		setInputValue(inputValue);
		return inputValue;
	};

	const checkIfEnter = (e: any) => {
		if (e.which === 13) handleFilter()
	}

	const modalHandlers = {
		add: (data: any) => handleAddModal(data),
		error: () => toggleModal(ModalTypes.none),
		import: (data: any) => handleImportExcel(data)
	};


	const handleImportExcel = async (data: any) => {
		let newRows = [...rows];
		const rowsFromExcel: any[] = [...data];
		const rowsActionDelete: any[] = [];
		const rowsActionImport: any[] = [];

		const rejectedRows: any[] = [];

		const rowsToAddAPI: any[] = [];
		const rowsToDeleteAPI: any[] = [];
		rowsFromExcel.forEach((row) => {
			// action 1 === import // action 2 === delete
			if (row.action === 1) {
				rowsActionImport.push(row)
			} else if (row.action === 2) {
				rowsActionDelete.push(row)
			} else {
				rejectedRows.push({ ...row, error: 'פעולה שאינה נכונה' })
			}
		})

		// console.log(rowsActionDelete)
		// console.log(rowsActionImport)
		console.log(supplierData)
		rowsActionImport.forEach((row) => {
			let Sunday = false, Monday = false, Tuesday = false, Wednesday = false, Thursday = false, Friday = false, Saturday = false;

			switch (row.supplyDay) {
				case 1: Sunday = true; break;
				case 2: Monday = true; break;
				case 3: Tuesday = true; break;
				case 4: Wednesday = true; break;
				case 5: Thursday = true; break;
				case 6: Friday = true; break;
				case 7: Saturday = true; break;
			}
			const product = catalogItems.find((catItem: any) => catItem.BarCode === row.BarCode)
			const detail = supplierData.detail.find((ele: any) => ele.AspakaDay === row.supplyDay)
			// console.log(row)
			// console.log(product)
			if (product && detail) {
				const newRow = {
					BarCode: row.BarCode,
					BranchId: row.BranchId,
					itemName: product.Name,
					SupplierId: supplierData.SupplierId,
					SupplierName: supplierData.SupplierName,
					Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday,
					SundayDisabled: Sunday ? false : true,
					MondayDisabled: Monday ? false : true,
					TuesdayDisabled: Tuesday ? false : true,
					WednesdayDisabled: Wednesday ? false : true,
					ThursdayDisabled: Thursday ? false : true,
					FridayDisabled: Friday ? false : true,
					SaturdayDisabled: Saturday ? false : true,
				}
				newRows.push(newRow)

				const rowToAddAPI = {
					BarCode: row.BarCode, BranchId: row.BranchId, days: [0, Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday]
				}
				rowsToAddAPI.push(rowToAddAPI)

			} else if (!product) {
				rejectedRows.push({ ...row, error: 'מוצר לא קיים' })
			} else if (!detail) {
				rejectedRows.push({ ...row, error: `לא קיים יום אספקה ${formatMessage({ id: `day${row.supplyDay}` })} לספק זה` })
			}

			// console.log(objToUpdate)
		})

		rowsActionDelete.forEach((row) => {

			// console.log(row)
			const aspakaItem = [...props.appAspakaItems].find((item: any) => item.BranchId === row.BranchId && item.BarCode === row.BarCode)


			if (aspakaItem) {
				rowsToDeleteAPI.push(row)
				newRows = [...newRows].filter((ele: any) => ele.BarCode !== row.BarCode && ele.BranchId !== row.BranchId)
			} else {
				rejectedRows.push({ ...row, error: `לא קיים יום אספקה ${formatMessage({ id: `day${row.supplyDay}` })} למוצר זה` })
			}
			// console.log(aspakaItem)
		})
		// console.log(rowsToDeleteAPI)
		// console.info(rowsToAddAPI)
		// console.log(rejectedRows)

		await Promise.all(rowsToDeleteAPI.map(async (row: any) => {
			const deleteAspakaItemOBJ = {
				BarCode: row.BarCode,
				BranchId: row.BranchId
			}
			// console.log({ deleteAspakaItemOBJ })


			await AppDeleteAspakaItem(deleteAspakaItemOBJ)
		}))

		await Promise.all(rowsToAddAPI.map(async (row: any) => {
			await AppUpdateAspakaItem({ rec1: row });
		}))



		// console.info(newRows)

		props.onUpdateSupplyItem()

		setOriginalRows(newRows)
		setRows(null)
		setIsDataReady(false)

		setTimeout(() => {
			setRows(newRows)
			setIsDataReady(true)
			toggleModal(ModalTypes.none)

		}, 1);
	}

	const toggleModal = (newType: ModalTypes) => {
		console.log(newType)
		setModalType(newType)
	}




	const main = (
		<div className='catalog width-85-per'>

			<div className='catalog__title mb-2'> <span className="cursor-pointer mr-05" onClick={() => back()}>➜</span>  {supplierData['BranchName']}, {supplierData['SupplierName']}</div>

			{rows ?
				<div className="d-flex catalog__AddItemExcel height-2-5-rem justify-content-between">
					<div className="d-flex">
						{[

							{
								name: 'product',
								isMulti: false,
								allowSelectAll: false,
								text: formatMessage({ id: 'catalog__input' }),
								loadOptions: loadCatalogOptions,
								defaultOptions: false,
								styles: productStyles

							},
						].map(item => {
							return (
								<div key={item.text}>
									<AsyncSelect
										name={item.name}
										isDisabled={rows.length > 0 ? false : true}
										isClearable={true}
										styles={item.styles}
										loadOptions={item.loadOptions}
										defaultOptions={item.defaultOptions}
										onInputChange={handleInputChange}
										onKeyDown={(e) => checkIfEnter(e)}
										onChange={(e: any, actionMeta: any) => {
											// console.info({ e, actionMeta })
											if (e) {
												if (actionMeta.action === "select-option") {
													setChosenProduct(e.value)
												}
											} else {
												if (actionMeta.action === "clear") {
													setChosenProduct(null)
												}
											}
										}}
										classNamePrefix='select'
										noOptionsMessage={() => formatMessage({ id: 'trending_productMinimumSearch' })}
										placeholder={item.text}
									/>
								</div>
							)
						})}
						<button disabled={rows.length == 0} className='btn btn-primary round d-flex justify-content-center align-items-center height-2-5-rem  width-7-rem mr-2 ml-2' onClick={handleFilter} >
							{formatMessage({ id: 'filter' })}
						</button>
					</div>



					<div className="d-flex justify-content-center align-items-center ml-05 cursor-pointer font-medium-1 text-black text-bold-600 p-05">
						{userEditFlag && (
						<button className='btn btn-primary round height-2-5-rem d-flex justify-content-center align-items-center width-7-rem mr-2 ml-2' onClick={() => toggleModal(ModalTypes.add)} >
							{formatMessage({ id: 'add' })}
						</button>
						)}
						{userEditFlag && (<div className="d-flex">
							{rows && isDataReady ?
								<>
									<div className="d-flex justify-content-center align-items-center ml-05 cursor-pointer">
										<ExportExcel fields={fields} data={rows ?? []} direction={'ltr'} translateHeader={true} filename={document.title}
											icon={<Icon src={config.iconsPath + "table/excel-download.svg"} />} />
									</div>

									<div onClick={() => toggleModal(ModalTypes.import)} className="cursor-pointer d-flex justify-content-center align-items-center ml-05 cursor-pointer">
										<Icon src={config.iconsPath + "table/excel-upload.svg"} />
									</div>
								</>
								: null}
						</div>)}
					</div>
				</div> : null
			}



			<div style={{ width: '80%' }} className=' mr-5 mt-3 catalogGrid'>
				{
					rows && isDataReady ?
						<AgGrid
							rowBufferAmount={300}
							defaultSortFieldNum={0}
							id={'id'}
							gridHeight={'70vh'}
							floatFilter={false}
							translateHeader
							fields={fields}
							groups={[]}
							totalRows={rows.length}
							rows={rows}
							checkboxFirstColumn={false}
							pagination
							resizable
						/>
						:
						<div className='catalogLoadingSpinner'>
							<Spinner />
						</div>
				}


				{modalType === ModalTypes.add ||
					modalType === ModalTypes.error ?
					<SupplyItemsModalsData
						modalHandlers={modalHandlers}
						modalType={modalType}
						supplierData={supplierData}
						items={filteredCatalogItems}
						toggleModal={(type: ModalTypes) => toggleModal(type)} />
					: modalType === ModalTypes.import ?
						<ImportExcelModal
							modalHandlers={modalHandlers}
							toggleModal={(type: ModalTypes) => toggleModal(type)}
							indexField={'BarCode'}
							wantedFields={['BranchId',
								'orderDay',
								'supplyDay',
								'action']}
							exampleFields={[
								{ text: "BarCode", type: 'string', width: window.innerWidth * 0.03, minWidth: 50, },
								{ text: "BranchId", type: 'string', width: window.innerWidth * 0.03, minWidth: 50, },
								{ text: "orderDay", type: 'string', width: window.innerWidth * 0.03, minWidth: 50, },
								{ text: "supplyDay", type: 'string', width: window.innerWidth * 0.03, minWidth: 50, },
								{ text: "action", type: 'string', width: window.innerWidth * 0.03, minWidth: 50, },
							]}
							exampleRows={[
								{ BarCode: '7290321', BranchId: '5', 'orderDay': '5', supplyDay: '2', action: '1' },
								{ BarCode: '7290321', BranchId: '5', 'orderDay': '5', supplyDay: '2', action: '1' }
							]}
							description={(
								<div>
									<span>BarCode - {formatMessage({ id: 'itemBarcode' })}</span> <br />
									<span>BranchId - {formatMessage({ id: 'BranchNumber' })}</span> <br />
									<span>orderDay - {formatMessage({ id: 'orderDay' })}</span> <br />
									<span>supplyDay -  {formatMessage({ id: 'supplyDay' })}</span> <br />
									<div className="d-flex">action -   <span style={{ marginRight: '3px' }}>1 = {formatMessage({ id: 'addAction' })}, </span> <span style={{ marginRight: '3px' }}> 2 = {formatMessage({ id: 'deleteAction' })}</span> </div>
								</div>)}
						/> : null}
			</div>
		</div>)


	return (
		main
	);

};


export default SupplyItemsGrid;
