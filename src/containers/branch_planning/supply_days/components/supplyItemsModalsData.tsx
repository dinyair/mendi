import * as React from 'react';

import { useIntl } from "react-intl"
import AsyncSelect from "react-select/async";
import { GenericModals } from '@src/components/GenericModals/index';
import { ModalsInterface, ModalTypes } from '@src/components/GenericModals/interfaces';
import { DropZoneField } from '../../../../components/import-excel-modal/dropZone';
import { AgGrid } from '@src/components';


interface Props {
    modalType: ModalTypes,
    toggleModal: (type: ModalTypes) => void;
    modalHandlers: any;
    // selectedRow: any;
    // filteredOptions: any,
    items?: any[]
    supplierData?: any
    description?: any
}

export const SupplyItemsModalsData: React.FC<Props> = props => {

    const {
        toggleModal,
        modalHandlers,
        modalType,
        items,
        supplierData,
        description
    } = props;

    const { formatMessage } = useIntl();

    React.useEffect(() => {
        console.log('im here')
    }, [])

    const [inputValue, setInputValue] = React.useState<any>(null);
    const [chosenProduct, setChosenProduct] = React.useState<any>(null);
    const [chosenProductName, setChosenProductName] = React.useState<any>(null);
    const [fieldsToReturn, setFieldsToReturn] = React.useState<any>(null)
    const [currentLevel, setCurrentLevel] = React.useState<number>(0);



    const productStyles = {
        container: (base: any) => ({
            ...base,
        }),
        option: (provided: any, state: any) => ({
            ...provided,
            ...provided,

            "&:hover": {
                backgroundColor: "#31baab"
            },
            color: state.isSelected ? '#ffffff' : '#1e1e20',
            backgroundColor: state.isSelected ? '#027b7a' : '#ffffff',
            borderBottom: '1px dotted lightgray',
            padding: 10,
        }),
    }

    


    const loadCatalogOptions = (inputValue: string, callback: any) => {
        if (inputValue.length >= 3) {
            callback(filterCatalog(inputValue));
        } else {
            callback()
        }
    };

    const filterCatalog = (inputValue: string) => {
        const regex = /\d/;
        const doesContainNumber = regex.test(inputValue)

        const catalogFiltered = items && items.filter((i: any) => {
            return i.Name.includes(inputValue) || String(i.BarCode).includes(inputValue)
        });
        let catalogMap: any[] = []
        catalogFiltered && catalogFiltered[0] ? catalogMap = catalogFiltered.map((option: any, index: number) => {
            return { index, value: option.BarCode, label: `${option.Name} ${option.BarCode}`, name: option.Name }
        }) : []

        if (doesContainNumber) {
            catalogMap.sort((a, b) => a.value - b.value)
        } else {
            catalogMap.sort((a, b) => a.label.length - b.label.length)
        }
		return catalogMap.slice(0,220)
    };

    const handleInputChange = (newValue: string) => {
        setInputValue(inputValue);
        console.log(inputValue)
        return inputValue;
    };


    const handleNextLevel = () => {
        setCurrentLevel(currentLevel + 1);
    }

    const handleBackLevel = (removeFile:boolean) => {
    //    removeFile && setUploadFile(null)
        setCurrentLevel(currentLevel - 1);
    }



    const modals: ModalsInterface = {
        add: [{
            isOpen: modalType === ModalTypes.add, toggle: () => toggleModal(ModalTypes.none), header: (
                <div className="text-center mt-2">
                    <p>{formatMessage({ id: 'addItemToList' })}</p>
                    <h3>{formatMessage({ id: 'addItemToListDescription' })}</h3>
                </div>
            ),
            body: (
                <div className="d-flex justify-content-center">
                    {[

                        {
                            name: 'product',
                            isMulti: false,
                            allowSelectAll: false,
                            text: `${formatMessage({ id: 'allItemsOf' })} ${supplierData && supplierData.SupplierName}`,
                            loadOptions: loadCatalogOptions,
                            defaultOptions: false,
                            styles: productStyles

                        },
                    ].map(item => {
                        return (
                            <div key={item.text}>
                                <AsyncSelect
                                    name={item.name}
                                    isDisabled={items && items.length > 0 ? false : true}
                                    isClearable={true}
                                    styles={item.styles}
                                    loadOptions={item.loadOptions}
                                    defaultOptions={item.defaultOptions}
                                    onInputChange={handleInputChange}
                                    // onKeyDown={(e) => checkIfEnter(e)}
                                    onChange={(e: any, actionMeta: any) => {
                                        if (e) {
                                            if (actionMeta.name === 'product') {
                                                setChosenProduct(e.value)
                                                setChosenProductName(e.Name)

                                            } else {
                                                // setChosenProduct(null)

                                            }
                                        }
                                    }}
                                    classNamePrefix='select'
                                    noOptionsMessage={() => formatMessage({ id: 'trending_productMinimumSearch' })}
                                    placeholder={item.text}
                                />
                            </div>
                        )
                    })}
                </div>
            ),
            buttons: [{
                color: 'primary', onClick: () => {
                    const product = items && items.find(ele => ele.BarCode == chosenProduct)
                    modalHandlers.add({ product })
                }, label:formatMessage({id: 'add'}), disabled: !chosenProduct
            },]
        },
        ],
        error: [{
            isOpen: modalType === ModalTypes.error, toggle: () => toggleModal(ModalTypes.error), header: formatMessage({ id: 'supplyDayForProductAlreadyExist' }), body: description,
            buttons: [{ color: 'primary', onClick: modalHandlers.error, label: formatMessage({id:'confirm'}) }
                // , { color: 'primary', onClick: toggleDeleteModal, label: formatMessage({id:'cancel'}) }
            ]
        }],
        delete:[],
        edit:[],
        export:[],
        import:[],
        none:[],
        info:[],

    }


    return (
        <GenericModals key={`modal${currentLevel}`} modal={modals[modalType][currentLevel]} />
    );
};



export default SupplyItemsModalsData;
