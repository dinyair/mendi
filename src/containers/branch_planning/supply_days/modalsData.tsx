import * as React from 'react';

import { useIntl } from "react-intl"
import { GenericModals } from '@src/components/GenericModals/index';
import { ModalInterface, ModalsInterface, ModalTypes } from '@src/components/GenericModals/interfaces';



interface Props {
    modalType: ModalTypes,
    toggleModal: (type: ModalTypes) => void;
    modalHandlers: any;
    modalMessage: string;
    modalTitle: string;
}

export const ModalsData: React.FC<Props> = props => {

    const {
        toggleModal,
        modalHandlers,
        modalType,
        modalMessage,
        modalTitle
    } = props;

    const { formatMessage } = useIntl();


    const [currentLevel, setCurrentLevel] = React.useState<number>(0);




    const handleNextLevel = () => {
        setCurrentLevel(currentLevel + 1);
    }

    const handleBackLevel = (removeFile: boolean) => {
        setCurrentLevel(currentLevel - 1);
    }




    const modals: ModalsInterface = {

        error: [{
            isOpen: modalType === ModalTypes.error, toggle: () => toggleModal(ModalTypes.none), header: modalTitle, body: modalMessage,
            buttons: [{ color: 'primary', onClick: modalHandlers.error, label: formatMessage({id:'confirm'}) }
            ]
        }],
        delete: [{
            isOpen: modalType === ModalTypes.delete, toggle: () => toggleModal(ModalTypes.none), header: modalTitle, body: modalMessage,
            buttons: [{ color: 'primary', onClick: () => toggleModal(ModalTypes.none), label: formatMessage({id:'No'}) }, { color: 'primary', outline: true, onClick: modalHandlers.delete, label: formatMessage({id:'yesDelete'}) }
            ]
        }],

        import: [],
        add: [],
        edit: [],
        export: [],
        none: [],
        info:[],
    }


    return (
        <GenericModals key={`modal${currentLevel}`} modal={modals[modalType][currentLevel]} />
    );
};



export default ModalsData;
