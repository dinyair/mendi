import { GenericDrawerData, DrawerTab } from "@src/components/edit-drawer";
import { InputTypes } from "@src/components/form-group";
import { useIntl } from "react-intl"


export const editDataDrawer = (data: any, userEditFlag:boolean) => {
	const { formatMessage } = useIntl();

	const trueMsg = formatMessage({ id: 'true' });
	const falseMsg = formatMessage({ id: 'false' });
	const details: any[] = data.details;

	let days = [
		formatMessage({ id: 'sunday-full' }),
		formatMessage({ id: 'monday-full' }),
		formatMessage({ id: 'tuesday-full' }),
		formatMessage({ id: 'wednesday-full' }),
		formatMessage({ id: 'thursday-full' }),
		formatMessage({ id: 'friday-full' }),
		formatMessage({ id: 'saturday-full' }),
	]


	let frequency = [
		{ value: 0, text: '1week' },
		{ value: 14, text: '2weeks' },
		{ value: 21, text: '3weeks' },
		{ value: 28, text: '4weeks' },
	]



	const field = (label: any, value: any, name: any, id: any) => {
		return {
			label,
			value,
			defaultValue: value,
			name: `${name}${id}`,
			type: InputTypes.SELECT,
			options: days,
			columns: 2,
			id: `${name}${id}`, 
			disabled: !userEditFlag
		}
	}

	const drawers: any = [];
	let countId = 1;
	if (!data || !data.detail || data.detail.length === 0) {
		// If there is no supply days
		// for (let index = 0; index < 6; index++) {
		for (let index = 0; index < 1; index++) {
			drawers.push(field(formatMessage({id: 'orderDay'}), '', 'orderDay', countId))
			drawers.push(field(formatMessage({id: 'supplyDay'}), '', 'supplyDay', countId))
			countId++;
		}
	} else {
		// If there is supply days
		for (let index = 0; index < data.detail.length; index++) {
			const element = data.detail[index];
			drawers.push(field(formatMessage({id: 'orderDay'}), days[element.OrderDay - 1], 'orderDay', countId))
			drawers.push(field(formatMessage({id: 'supplyDay'}), days[element.AspakaDay - 1], 'supplyDay', countId))
			countId++;
		}
	}

	let frequencyDay: number;
	let frequencyValue;
	// console.info({data:data.detail})
	if (data.detail && data.detail[0]) {
		frequencyDay = data.detail[0]['days_order'];
		// console.info({frequencyDay})
		frequencyValue = frequency.find(ele => ele.value === frequencyDay)
		if(!data.detail[0]['days_order']) frequencyValue=frequency[0]
		// console.info({frequencyValue})

	} else {
		frequencyValue = frequency[0]
	}

	// console.info({ frequencyValue })

	const wensInputs = [
		{
			label: formatMessage({ id: "Wensell" }),
			value: data.Wensell_flag === 0 ? falseMsg : data.Wensell_flag === 1 ? trueMsg : falseMsg,
			name: 'wensell',
			type: InputTypes.SELECT,
			options: [falseMsg, trueMsg],
			columns: 2,
			id: 'wensell',
			disabled: !userEditFlag
		}, {
			label: formatMessage({ id: "frequency" }),
			value: frequencyValue ? formatMessage({ id: frequencyValue.text }) : '',
			name: 'frequency',
			type: InputTypes.SELECT,
			options: frequency.map(ele => formatMessage({ id: ele.text })),
			columns: 2,
			id: 'frequency',
			disabled: !userEditFlag
		}]


	let drawerTabs: Array<DrawerTab> = [
		{
			title: '',
			tabId: '1',
			formGroup: [{
				formId: '1',
				fields: [

				],
				extraFields: {
					disableAddButton: !userEditFlag,
					disableDeleteButton: !userEditFlag,
					title: 'addSupplyCycle',
					max: 6,
					base: [field(formatMessage({id: 'orderDay'}), "", 'orderDay', countId), field(formatMessage({id: 'supplyDay'}), "", 'supplyDay', countId)],
					fields: [...drawers,]
				}
			},
			{
				formId: '2',
				fields: [
					...wensInputs
				],
			}

			]
		}]

	// console.info({ drawerTabs })
	// console.info({ data })



	let editDrawerTab: GenericDrawerData = {
		tabs: drawerTabs
	}
	return editDrawerTab
}






