import { getRequest, postRequest } from "@src/http";


export const getBranch = async () => {
    const resp = await getRequest<any>(`getBranch`, {});
    return resp
}

export const getAppAspaka = async () => {
    const resp = await getRequest<any>(`AppAspaka`, {});
    return resp
}

export const getAppSapakimAllow = async () => {
    const resp = await getRequest<any>(`AppSapakimAllow`, {});
    return resp
}

export const getAppSubSapakAllow = async () => {
    const resp = await getRequest<any>(`AppGetSubSapakAllow`, {});
    return resp
}

export const getAppAspakaItemAll = async () => {
    const resp = await getRequest<any>(`AppAspakaItemAll`, {});
    return resp
}

export const AppDeleteAspaka = async (obj:any) => {
    const resp = await postRequest<any>(`AppDeleteAspaka`, obj);
    return resp
}

export const AppInsertAspaka = async (obj:any) => {
    const resp = await postRequest<any>(`AppInsertAspaka`, obj);
    return resp
}

export const AppDeleteAspakaById = async (obj:any) => {
    const resp = await postRequest<any>(`AppDeleteAspakaById`, obj);
    return resp
}

export const getCatalogHla = async () => {
    const resp = await getRequest<any>(`getcatalogHla`, {});
    return resp
}

export const getCatalogHlaSub = async (SubSupplierId:any) => {
    const resp = await getRequest<any>(`getcatalogHlaSub?sapak1=${SubSupplierId}`, {});
    return resp
}



export const getSubSupplierGroups = async (supplierId:any,subSupplierId:any) => {
    const resp = await getRequest<any>(`getSubSupplierGroups?supplierId=${supplierId}&subSupplierId=${subSupplierId}`, {});
    console.info({resp})
    return resp
}



export const AppDeleteAspakaItem = async (obj:any) => {
    const resp = await getRequest<any>(`AppDeleteAspakaItem?BarCode=${obj.BarCode}&BranchId=${obj.BranchId}`);
    return resp
}


export const AppUpdateAspakaItem = async (obj:any) => {
    const resp = await postRequest<any>(`AppUpdateAspakaItem`, obj);
    return resp
}