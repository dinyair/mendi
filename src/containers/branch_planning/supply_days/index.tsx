import * as React from 'react';
import { AgGrid } from '@components';
import { useIntl } from 'react-intl';
import { config } from '@src/config';
import { Icon } from '@components';
import AsyncSelect from 'react-select/async';
import ExportExcel from '@src/components/ExportExcel/ExportExcel';
import { editDataDrawer } from './mockData';
import {
	getAppAspaka,
	getBranch,
	getAppSapakimAllow,
	getAppSubSapakAllow,
	AppDeleteAspaka,
	AppInsertAspaka,
	AppDeleteAspakaById,
	getCatalogHla,
	getAppAspakaItemAll,
	AppDeleteAspakaItem,
	AppUpdateAspakaItem
} from './initial-state';
import { SupplyItemsGrid } from './components/supplyItemsGrid';
import { GenericDrawer } from '@src/components/generic-drawer';
import { ModalTypes } from '@src/components/GenericModals/interfaces';
import ImportExcelModal from '../../../components/import-excel-modal/import-excel-modal';
import ModalsData from './modalsData';
import { Col } from 'reactstrap';
import { useSelector } from 'react-redux';
import { RootStore } from '@src/store';

export const SupplyDays: React.FunctionComponent = () => {
	const { formatMessage } = useIntl();
	document.title = formatMessage({ id: 'Supply Days' });

	const user = useSelector((state: RootStore) => state.user);
	const userPermissions = user ? user.permissions : null;
	let URL = window.location.pathname;
	const userEditFlag = userPermissions && userPermissions[URL] && userPermissions[URL].edit_flag === 1;

	const [userBranches, setUserBranches] = React.useState<any>([]);
	const [suppliers, setSuppliers] = React.useState<any>([]);
	const [subSuppliers, setSubSuppliers] = React.useState<any>([]);
	const [subSuppliersAllow, setSubSuppliersAllow] = React.useState<any>([]);
	const [uniqueSuppliers, setUniqueSuppliers] = React.useState<any>([]);
	const [isDataReady, setIsDataReady] = React.useState<boolean>(false);
	const [inputValue, setInputValue] = React.useState<any>(null);
	const [chosenBranch, setChosenBranch] = React.useState<any>();
	const [chosenSupplier, setChosenSupplier] = React.useState<any>();
	const [chosenSubSupplier, setChosenSubSupplier] = React.useState<any>();
	const [selectedSupplier, setSelectedSupplier] = React.useState<any>();
	const [originalRows, setOriginalRows] = React.useState<any[]>([]);
	const [rows, setRows] = React.useState<any>([]);
	const [rowsToDelete, setRowsToDelete] = React.useState<any>();
	const [fields, setFields] = React.useState<any>([
		{ text: 'BranchName', type: 'string', width: window.innerWidth * 0.03, minWidth: 200 },
		{
			text: 'SupplierName',
			type: 'string',
			width: window.innerWidth * 0.03,
			minWidth: 300,
			cellRenderer: 'ClickableCell',
			clickableCells: {
				byStatment: true,
				if: 'flag_detail',
				value: true,
				onClick: (data: any) => {
					setSelectedSupplier(data);
					setTimeout(() => {
						setShowSupplyDaysGrid(!showSupplyDaysGrid);
					}, 1);
				}
			}
		},
		{ text: 'Wensell', type: 'string', width: window.innerWidth * 0.03, minWidth: 50 },
		{
			text: 'actions',
			type: 'string',
			width: window.innerWidth * 0.001,
			minWidth: 50,
			noFilter: true,
			cellRenderer: 'IconRender',
			renderIcons: [
				{ actions: true },
				{
					type: 'edit',
					title: 'Edit',
					onClick: (data: any) => {
						setSelectedSupplier(data);
						setTimeout(() => {
							toggleEditModal();
						}, 1);
					},
					icon: 'edit.svg'
				},
				userEditFlag
					? {
							type: 'delete',
							title: 'Delete',
							disabled: 'flag_detail',
							returnDataWithProps: true,
							onClick: (data: any) => toggleDeleteModal(data),
							icon: 'trash.svg'
					  }
					: null
			]
		}
	]);

	const [showEditModal, setShowEditModal] = React.useState(false);
	const [showSupplyDaysGrid, setShowSupplyDaysGrid] = React.useState(false);

	const [modalMessage, setModalMessage] = React.useState<any>(null);
	const [modalTitle, setModalTitle] = React.useState<any>(null);

	const [catalogItems, setCatalogItems] = React.useState<any>();
	const [appAspakaItems, setAppAspakaItems] = React.useState<any>();
	const [modalType, setModalType] = React.useState<ModalTypes>(ModalTypes.none);

	React.useEffect(() => {
		getRows();
		setFieldsDefault();
	}, []);
	React.useEffect(() => {
		if (!showEditModal) setSelectedSupplier(null);
	}, [showEditModal]);
	React.useEffect(() => {
		handleFilter();
	}, [chosenBranch, chosenSupplier, chosenSubSupplier]);

	const getRows = async () => {
		const appSapakimAllow: any[] = await getAppSapakimAllow();
		const appSubSapakAllow: any[] = await getAppSubSapakAllow();
		const appAspaka: any[] = await getAppAspaka();
		const branchesData: any[] = await getBranch();

		// console.log(branchesData)

		const branches: any[] = branchesData.filter(ele => ele.BranchType === 0);

		setSubSuppliers([
			{ Name: 'כל הספקים', value: 0, option: 0 },
			{ Name: 'ספקים שהוגדרו', value: 1, option: 1 },
			{ Name: 'ספקים שלא הוגדרו', value: 2, option: 3 }
		]);
		setSuppliers(appSapakimAllow);
		setSubSuppliersAllow(appSubSapakAllow);
		setUserBranches(branches);

		// filter unique SubSuppliers + Suppliers
		const subSupplierMap = new Map();
		const suppliersMap = new Map();
		const uniqueSuppliers = [];

		// Create unique array of SubSuppliers
		for (const item of appSubSapakAllow) {
			if (!subSupplierMap.has(item.SubsapakId)) {
				subSupplierMap.set(item.SubsapakId, true); // set any value to Map
				suppliersMap.set(item.SapakId, true); // set any value to Map
				uniqueSuppliers.push(item);
			}
		}

		// Add Unique Suppliers To Array
		for (const item of appSapakimAllow) {
			if (!suppliersMap.has(item.Id)) {
				subSupplierMap.set(item.Id, true); // set any value to Map
				suppliersMap.set(item.Id, true); // set any value to Map
				uniqueSuppliers.push(item);
			}
		}

		setUniqueSuppliers(uniqueSuppliers);

		const rows: any[] = [];
		// console.log({ uniqueSuppliers })

		// Iterate trough the uniqueSuppliers Array
		for (let index = 0; index < uniqueSuppliers.length; index++) {
			const _supplier = uniqueSuppliers[index];
			const _sapakId = _supplier.SubsapakId ? _supplier.SubsapakId : _supplier.Id;

			// Iterate trough the branches array
			branches.forEach(ele => {
				const branch = ele;
				const detail = appAspaka.filter(ele => ele.SapakId === _sapakId && ele.BranchId == branch.BranchId);
				let Wensell = detail[0] && detail[0].Wensell;
				let Wensell_flag = Wensell;
				if (Wensell === 0) Wensell = 'false';
				else if (Wensell === 1) Wensell = 'true';

				const _supplierName = _supplier.SubsapakName ? _supplier.SubsapakName : _supplier.Name;
				const _supplierId = _supplier.SapakId ? _supplier.SapakId : _supplier.Id;
				const obj = {
					BranchName: ele.Name,
					BranchType: ele.BranchType,
					BranchId: ele.BranchId,
					SupplierId: _supplier.SapakId ? _supplier.SapakId : _supplier.Id,
					SubSupplierId: _supplier.SubsapakId ? _supplier.SubsapakId : null,
					SupplierName: _supplierName,
					detail,
					Wensell: Wensell ? formatMessage({ id: Wensell }) : '',
					Wensell_flag,
					flag_detail: detail.length > 0 ? true : false,
					id: `${ele.BranchId}:${_supplierId}:${index}`,
					groupId: _supplier.groupId,
					ClassesId: _supplier.ClassesId
				};

				rows.push(obj);
			});
		}

		// All the rows
		setOriginalRows(rows);

		setRows(rows);

		const catalogHla: any[] = await getCatalogHla();
		const appAspakaItem: any[] = await getAppAspakaItemAll();

		// console.log(appAspakaItem)
		// console.log(catalogHla)

		setAppAspakaItems(appAspakaItem);
		setCatalogItems(catalogHla);

		setIsDataReady(true);
	};

	const setFieldsDefault = () => {
		setFields([
			{ text: 'BranchName', type: 'string', width: window.innerWidth * 0.03, minWidth: 50 },
			{
				text: 'SupplierName',
				type: 'string',
				width: window.innerWidth * 0.03,
				minWidth: 50,
				cellRenderer: 'ClickableCell',
				clickableCells: {
					byStatment: true,
					if: 'flag_detail',
					value: true,
					onClick: (data: any) => {
						setSelectedSupplier(data);
						setTimeout(() => {
							setShowSupplyDaysGrid(!showSupplyDaysGrid);
						}, 1);
					}
				}
			},
			{
				text: 'Wensell',
				type: 'string',
				width: window.innerWidth * 0.03,
				minWidth: 50,
				cellRenderer: 'CenterCell'
			},
			{
				text: 'actions',
				type: 'string',
				width: window.innerWidth * 0.001,
				minWidth: 1,
				noFilter: true,
				cellRenderer: 'IconRender',
				renderIcons: [
					{ actions: true },
					{
						type: 'edit',
						title: 'Edit',
						onClick: (data: any) => {
							console.info({ data });
							setSelectedSupplier(data);
							setTimeout(() => {
								toggleEditModal();
							}, 1);
						},
						icon: 'edit.svg'
					},
					userEditFlag
						? {
								type: 'delete',
								title: 'Delete',
								disabled: 'flag_detail',
								returnDataWithProps: true,
								onClick: (data: any) => toggleDeleteModal(data),
								icon: 'trash.svg'
						  }
						: null
				]
			}
		]);
	};
	const editOnDoubleClick = (data: any) => {
		setSelectedSupplier(data);
		setTimeout(() => {
			toggleEditModal();
		}, 1);
	};

	const toggleEditModal = () => {
		setShowEditModal(!showEditModal);
	};

	const toggleDeleteModal = (data: any) => {
		const rows = data.node.parent.allLeafChildren.map((ele: any) => ele.data);
		setRowsToDelete(rows);
		setSelectedSupplier(data.data);
		setModalTitle(formatMessage({ id: 'WarningBeforeDelete' }));
		setModalMessage('בחרת למחוק ימי אספקה שהוגדרו לספק זה, האם אתה בטוח?');
		toggleModal(ModalTypes.delete);
	};

	const handleDelete = async () => {
		// console.info({ selectedSupplier })
		toggleModal(ModalTypes.none);

		if (selectedSupplier) {
			const rows = [...rowsToDelete];
			const rowIndex = rows.findIndex((ele: any) => ele.id === selectedSupplier.id);
			const selectedRow = rows[rowIndex];
			rows[rowIndex] = { ...selectedRow, detail: [], flag_detail: false, Wensell: '', Wensell_flag: 0 };

			const deleteAspakaOBJ = {
				SapakId: selectedRow.SubSupplierId ? selectedRow.SubSupplierId : selectedRow.SupplierId,
				BranchId: selectedRow.BranchId
			};

			await AppDeleteAspaka({ rec: deleteAspakaOBJ });

			setOriginalRows(rows);
			setRows([]);
			setIsDataReady(false);

			setTimeout(() => {
				setRows(rows);
				setIsDataReady(true);
			}, 1);
		}
	};

	const filterBranch = (inputValue: string) => {
		const branchesFiltered = userBranches.filter((i: any) => i.Name.includes(inputValue));
		let branchesMap: any[] = [];
		branchesFiltered && branchesFiltered[0]
			? (branchesMap = branchesFiltered.map((option: any, index: number) => {
					return { index, value: option.BranchId, label: option.Name };
			  }))
			: [];
		return branchesMap;
	};

	const filterSuppliers = (inputValue: string) => {
		const suppliersFiltered = uniqueSuppliers.filter(
			(i: any) =>
				(i.SubsapakName && i.SubsapakName.includes(inputValue)) || (i.Name && i.Name.includes(inputValue))
		);

		const suppliersMap = suppliersFiltered.map((option: any, index: number) => {
			return {
				index,
				value: option.SubsapakId ? option.SubsapakId : option.Id,
				label: option.Name ? option.Name : option.SubsapakName
			};
		});

		return suppliersMap;
	};

	const days = [
		formatMessage({ id: 'א' }),
		formatMessage({ id: 'ב' }),
		formatMessage({ id: 'ג' }),
		formatMessage({ id: 'ד' }),
		formatMessage({ id: 'ה' }),
		formatMessage({ id: 'ו' }),
		formatMessage({ id: 'ש' })
	];

	const saveSideBarChanges = async (newData: any) => {
		// console.log({newData})
		const savedDataDays: any[] = [
			...newData.tabs[0].formGroup[0].extraFields.fields,
			...newData.tabs[0].formGroup[0].extraFields.base
		];
		const supplyDays: any = [];
		const orderDays: any = [];

		// console.log({ savedDataDays });

		let isDataOk = true;

		for (let i = 0; i < savedDataDays.length / 2; i = i + 2) {
			const element: any = savedDataDays[i];
			const element2: any = savedDataDays[i + 1];

			// If 1 input of the row is empty and the second is full...
			if ((element.value && !element2.value) || (element2.value && !element.value)) {
				isDataOk = false;
				setModalTitle('תקלה');
				if (element.value === '') {
					setModalMessage(formatMessage({ id: 'cannotInsertSupplyDayOnly' }));
				} else {
					setModalMessage(formatMessage({ id: 'cannotInsertOrderDayOnly' }));
				}
				toggleModal(ModalTypes.error);
				break;
			}
		}

		if (isDataOk) {
			for (let i = 0; i < savedDataDays.length; i++) {
				const element: any = savedDataDays[i];
				let value = element.value && element.value.value ? element.value.value : element.value;
				const options = element.options;

				let valueIndex = options.findIndex((ele: any) => ele == value);

				if (valueIndex > -1) {
					if (element.name.includes('orderDay')) {
						// console.info({ valueIndex: valueIndex + 1 })
						const isExists = orderDays.findIndex((ele: any) => ele == valueIndex + 1);

						if (isExists == -1) {
							orderDays.push(valueIndex + 1);
						} else {
							// If the order day already exists
							isDataOk = false;
							setModalTitle('תקלה');
							setModalMessage(
								`${formatMessage({ id: 'orderDay' })} ${days[valueIndex]} ${formatMessage({
									id: 'toTheSupplier'
								})} - ${selectedSupplier.SupplierName} | ${selectedSupplier.BranchName} ${formatMessage(
									{ id: 'alreadyExist' }
								)}`
							);
							toggleModal(ModalTypes.error);

							break;
						}
					} else if (element.name.includes('supplyDay')) {
						// console.info({ valueIndex: valueIndex + 1 })
						const isExists = supplyDays.findIndex((ele: any) => ele == valueIndex + 1);

						if (isExists == -1) {
							supplyDays.push(valueIndex + 1);
						} else {
							// If the supply day already exists
							isDataOk = false;
							setModalTitle('תקלה');
							setModalMessage(
								`${formatMessage({ id: 'supplyDay' })} ${days[valueIndex]} ${formatMessage({
									id: 'toTheSupplier'
								})} - ${selectedSupplier.SupplierName} | ${selectedSupplier.BranchName} ${formatMessage(
									{ id: 'alreadyExist' }
								)}`
							);
							toggleModal(ModalTypes.error);

							break;
						}
					}
				}
			}
		}

		// console.log(orderDays)
		// console.log(supplyDays)

		if (isDataOk) {
			let wensellValue: any = newData.tabs[0].formGroup[1].fields[0].value;
			let frequencyValue: string = newData.tabs[0].formGroup[1].fields[1].value.value;

			let wensellOptions: any[] = newData.tabs[0].formGroup[1].fields[0].options;
			let frequencyOptions: any[] = newData.tabs[0].formGroup[1].fields[1].options;

			let wensell = wensellOptions.findIndex(ele => {
				let neww = typeof wensellValue =='string'? wensellValue: wensellValue&&wensellValue.value ?wensellValue.value : -1
				return ele === neww
			});

			let frequency: number | null = frequencyOptions.findIndex(ele => ele === frequencyValue);

			if (frequency === -1) frequency = 0;
			if (frequency === 0) frequency = 0;
			else if (frequency === 1) frequency = 14;
			else if (frequency === 2) frequency = 21;
			else if (frequency === 3) frequency = 28;
			// console.log({ orderDays })
			// console.log({ supplyDays })
			// console.log({ wensell })
			// console.log({ frequency })

			/* Send them to api */

			// console.info({ rows })
			// console.info({ selected: selectedSupplier.SupplierId })
			const supplierIndex = rows.findIndex((ele: any) => ele == selectedSupplier);
			// const supplierIndexOgRows = originalRows.findIndex((ele: any) => ele == selectedSupplier)

			const newOgRows = [...originalRows];
			const newRows = [...rows];

			const detail = supplyDays.map((ele: any, index: number) => {
				return {
					AspakaDay: ele,
					BranchId: selectedSupplier.BranchId,
					// Id: "?????",
					OrderDay: orderDays[index],
					SapakId: selectedSupplier.SubSupplierId
						? selectedSupplier.SubSupplierId
						: selectedSupplier.SupplierId,
					Wensell: wensell,
					days_order: frequency
					// dd: frequency
				};
			});

			detail.sort(function (a: any, b: any) {
				return a.OrderDay - b.OrderDay;
			});
			// console.info({ detail })

			try {
				/// DELETE FROM API
				const deleteAspakaOBJ = {
					SapakId: selectedSupplier.SubSupplierId
						? selectedSupplier.SubSupplierId
						: selectedSupplier.SupplierId,
					BranchId: selectedSupplier.BranchId
				};

				let res = [];
				// console.log({ deleteAspakaOBJ });

				// console.log({ detail });
				const del = await AppDeleteAspaka({ rec: deleteAspakaOBJ });
				res.push(del);
				for (let index = 0; index < detail.length; index++) {
					const element = detail[index];
					let resp = await AppInsertAspaka({ rec1: element });
					// console.log({ resp });
					res.push(resp);
				}

				// console.error({ detail })
				// console.error(res)
			} catch (error) {
				console.error(error);
			}

			const newRow = rows[supplierIndex];
			newRow['detail'] = detail;
			newRow['flag_detail'] = detail.length > 0 ? true : false;
			newRow['Wensell_flag'] = wensell;

			if (wensell === 0) {
				newRow['Wensell'] = formatMessage({ id: 'false' });
			}
			if (wensell === 1) {
				newRow['Wensell'] = formatMessage({ id: 'true' });
			}

			// console.log(newRows);
			newRows[supplierIndex] = newRow;
			newOgRows[supplierIndex] = newRow;
			setOriginalRows(newOgRows);
			setRows([]);
			setIsDataReady(false);

			setTimeout(() => {
				setRows(newRows);
				setIsDataReady(true);
			}, 1);
		}
	};

	const handleInputChange = (newValue: string) => {
		setInputValue(inputValue);
		return inputValue;
	};

	const loadBranchOptions = (inputValue: string, callback: any) => {
		callback(filterBranch(inputValue));
	};

	const loadSuppliersOptions = (inputValue: string, callback: any) => {
		callback(filterSuppliers(inputValue));
	};

	const checkIfEnter = (e: any) => {
		if (e.which === 13) handleFilter();
	};

	const branchStyles = {
		container: (base: any) => ({
			...base
		}),
		option: (provided: any, state: any) => ({
			...provided,

			'&:hover': {
				color: '#ffffff',
				backgroundColor: '#31baab'
			},
			color: state.isSelected ? '#ffffff' : '#1e1e20',
			backgroundColor: state.isSelected ? '#027b7a' : '#ffffff',
			borderBottom: '1px dotted lightgray',
			padding: 10
		}),
		control: () => ({
			width: 200,
			height: 50
		})
	};

	const handleFilter = () => {
		setRows([]);

		let filteredRows = [...originalRows];
		if (chosenBranch) {
			filteredRows = filteredRows.filter(ele => ele.BranchId === chosenBranch.value);
		}

		if (chosenSupplier) {
			filteredRows = filteredRows.filter(
				ele => ele.SupplierId === chosenSupplier.value || ele.SubSupplierId === chosenSupplier.value
			);
		}

		if (chosenSubSupplier) {
			switch (chosenSubSupplier.value) {
				case 1:
					filteredRows = filteredRows.filter(ele => ele.flag_detail === true);
					break;
				case 2:
					filteredRows = filteredRows.filter(ele => ele.flag_detail === false);
					break;
				default:
					break;
			}
		}

		setTimeout(() => {
			setRows(filteredRows);
		}, 100);
	};

	const updateSupplyItemsDays = async () => {
		const appAspakaItem: any[] = await getAppAspakaItemAll();
		setAppAspakaItems(appAspakaItem);
	};

	const modalHandlers = {
		add: (data: any) => {},
		error: (data: any) => {
			toggleModal(ModalTypes.none);
		},
		delete: (data: any) => handleDelete(),
		edit: (data: any) => {},
		import: (data: any) => handleImportExcel(data)
	};

	const handleImportExcel = async (data: any) => {
		const rowsFromExcel: any[] = [...data];
		const rowsActionDelete: any[] = [];
		const rowsActionImport: any[] = [];

		const rejectedRows: any[] = [];
		const rowsToAddAPI: any[] = [];
		const rowsToDeleteAPI: any[] = [];

		rowsFromExcel.forEach(row => {
			// action 1 === import // action 2 === delete
			if (row.action === 1) {
				rowsActionImport.push(row);
			} else if (row.action === 2) {
				rowsActionDelete.push(row);
			} else {
				rejectedRows.push({ ...row, error: 'פעולה שאינה נכונה' });
			}
		});

		// console.log(rowsActionDelete)
		// console.log(rowsActionImport)
		// console.log(supplierData)
		rowsActionImport.forEach(row => {
			const product = catalogItems.find((catItem: any) => catItem.BarCode === row.BarCode);

			if (product) {
				// console.log(product)
				// console.log(uniqueSuppliers)
				// console.log(rows)
				const supplierData = [...rows].find((supplier: any) => supplier.SupplierId === product.SapakId);
				// console.log(supplierData)

				if (supplierData && supplierData.detail && supplierData.detail.length) {
					const detail = supplierData.detail.find((ele: any) => ele.AspakaDay === row.supplyDay);

					if (detail) {
						let Sunday = false,
							Monday = false,
							Tuesday = false,
							Wednesday = false,
							Thursday = false,
							Friday = false,
							Saturday = false;

						switch (row.supplyDay) {
							case 1:
								Sunday = true;
								break;
							case 2:
								Monday = true;
								break;
							case 3:
								Tuesday = true;
								break;
							case 4:
								Wednesday = true;
								break;
							case 5:
								Thursday = true;
								break;
							case 6:
								Friday = true;
								break;
							case 7:
								Saturday = true;
								break;
						}

						const rowToAddAPI = {
							BarCode: row.BarCode,
							BranchId: row.BranchId,
							days: [0, Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday]
						};
						rowsToAddAPI.push(rowToAddAPI);
					} else if (!detail) {
						rejectedRows.push({
							...row,
							error: `לא קיים יום אספקה ${formatMessage({ id: `day${row.supplyDay}` })} לספק זה`
						});
					}
				} else if (!supplierData) {
					rejectedRows.push({ ...row, error: 'ספק לא קיים' });
				}
			} else if (!product) {
				rejectedRows.push({ ...row, error: 'מוצר לא קיים' });
			}
		});

		rowsActionDelete.forEach(row => {
			const aspakaItem = [...appAspakaItems].find(
				(item: any) => item.BranchId === row.BranchId && item.BarCode === row.BarCode
			);

			if (aspakaItem) {
				rowsToDeleteAPI.push(row);
			} else {
				rejectedRows.push({
					...row,
					error: `לא קיים יום אספקה ${formatMessage({ id: `day${row.supplyDay}` })} למוצר זה`
				});
			}
		});

		// console.log({rowsToDeleteAPI})
		// console.info({rowsToAddAPI})
		// console.log({rejectedRows})

		await Promise.all(
			rowsToDeleteAPI.map(async (row: any) => {
				const deleteAspakaItemOBJ = {
					BarCode: row.BarCode,
					BranchId: row.BranchId
				};
				// console.log({ deleteAspakaItemOBJ })
				await AppDeleteAspakaItem(deleteAspakaItemOBJ);
			})
		);

		await Promise.all(
			rowsToAddAPI.map(async (row: any) => {
				await AppUpdateAspakaItem({ rec1: row });
			})
		);

		await updateSupplyItemsDays();
		toggleModal(ModalTypes.none);
	};

	const toggleModal = (newType: ModalTypes) => {
		setModalType(newType);
	};
	const splitStringToNumber = (str:string) =>{
        var patt1 = /[0-9]/g;
        var patt2 = /[a-zA-Z]/g;
        var letters = str.match(patt2);
        var digits = str.match(patt1);
		return digits
	}

	const customValidation = (data: any) => {
		let shouldRequestWensellAction = false
		let blockNextStage = false
		let newData = data;
		newData.forEach((tab: any, indexLayout: number) => {
			tab.formGroup.forEach((fields: any, formIndex: number) => {
				if (fields && fields.extraFields && fields.extraFields.fields) {
					let tempId:any = ''
					let tempValue:any =''
					fields.extraFields.fields.forEach((extraField: any, extraIndex: number) => {
						let stringSpliitedToNumber:any = splitStringToNumber(extraField.id)
						if(tempId == stringSpliitedToNumber[0] &&
							 (tempValue && tempValue.value && extraField.value.value && 
							 tempValue.value == extraField.value.value)){
							shouldRequestWensellAction = true
						}

						tempId=stringSpliitedToNumber[0]
						tempValue=extraField.value
					});
				}
				fields.fields.forEach((field: any, index: number) => {
					if(field.id == 'wensell' && typeof field.value == 'string' && shouldRequestWensellAction && field.value != formatMessage({ id: 'true' })){

						blockNextStage=true
						field.error = {label:'giveThisFieldAttention'}
					}else{
						field.error = null
					}
				});
			});
		});
		return blockNextStage ? newData : true;
	};

	const mainGrid = (
		<>
			<div className="d-flex-column">
				<div className="mb-1 font-medium-5 text-bold-700 text-black p-0">
					{formatMessage({ id: 'Supply Days' })}
				</div>

				<div className="d-flex justify-content-between width-60-per p-0">
					<div className="d-flex">
						{[
							{
								name: 'Branch',
								isMulti: false,
								allowSelectAll: false,
								loadOptions: loadBranchOptions,
								text: formatMessage({ id: 'branch' }),
								defaultOptions:
									userBranches && userBranches[0]
										? userBranches.map((option: any, index: number) => {
												return { index, value: option.BranchId, label: option.Name };
										  })
										: [],
								onChange: null,
								defaultValue: chosenBranch,
								styles: branchStyles
							},

							{
								name: 'Supplier',
								isMulti: false,
								allowSelectAll: false,
								loadOptions: loadSuppliersOptions,
								text: formatMessage({ id: 'Supplier' }),
								defaultOptions:
									suppliers && suppliers[0]
										? suppliers.map((option: any, index: number) => {
												return { index, value: option.Id, label: option.Name };
										  })
										: [],
								onChange: null,
								defaultValue: chosenSupplier,
								styles: branchStyles
							},
							{
								name: 'SubSupplier',
								isMulti: false,
								allowSelectAll: false,
								loadOptions: () => {},
								text: formatMessage({ id: 'stock_suppliers' }),
								defaultOptions:
									subSuppliers && subSuppliers[0]
										? subSuppliers.map((option: any, index: number) => {
												return { index, value: option.value, label: option.Name };
										  })
										: [],
								onChange: null,
								defaultValue: chosenSubSupplier,
								styles: branchStyles
							}
						].map((item, index) => {
							return (
								<div key={item.text} className={index != 0 ? 'ml-1' : ''}>
									<AsyncSelect
										name={item.name}
										isDisabled={!isDataReady}
										isClearable={true}
										styles={item.styles}
										loadOptions={item.loadOptions}
										defaultOptions={item.defaultOptions}
										onInputChange={handleInputChange}
										onKeyDown={e => checkIfEnter(e)}
										defaultValue={item.defaultValue}
										onChange={(e: any, actionMeta: any) => {
											if (e) {
												// console.info({ e, actionMeta })
												if (actionMeta.action === 'select-option') {
													if (actionMeta.name === 'Branch') {
														// סינון לפי סניף
														setChosenBranch(e);
													} else if (actionMeta.name === 'Supplier') {
														// סינון לפי שם ספק
														setChosenSupplier(e);
													} else if (actionMeta.name === 'SubSupplier') {
														// סינון לפי סוג ספק
														setChosenSubSupplier(e);
													}
												}
											} else {
												if (actionMeta.action === 'clear') {
													if (actionMeta.name === 'Branch') {
														// סינון לפי סניף
														setChosenBranch(null);
													} else if (actionMeta.name === 'Supplier') {
														// סינון לפי שם ספק
														setChosenSupplier(null);
													} else if (actionMeta.name === 'SubSupplier') {
														// סינון לפי סוג ספק
														setChosenSubSupplier(null);
													}
												}
											}
										}}
										classNamePrefix="select"
										noOptionsMessage={() => formatMessage({ id: 'trending_productMinimumSearch' })}
										placeholder={item.text}
									/>
								</div>
							);
						})}
					</div>

					<div className="d-flex">
						{rows && isDataReady ? (
							<>
								<div className="d-flex justify-content-center align-items-center ml-05 cursor-pointer">
									<ExportExcel
										fields={fields}
										data={rows}
										direction={'ltr'}
										translateHeader={true}
										filename={document.title}
										icon={<Icon src={config.iconsPath + 'table/excel-download.svg'} />}
									/>
								</div>

								<div
									onClick={() => toggleModal(ModalTypes.import)}
									className="cursor-pointer d-flex justify-content-center align-items-center ml-05 cursor-pointer"
								>
									<Icon src={config.iconsPath + 'table/excel-upload.svg'} />
								</div>
							</>
						) : null}
					</div>
				</div>
			</div>
			<div className="width-60-per mr-5 mt-2">
				<AgGrid
					onRowDoubleClick={editOnDoubleClick}
					rowBufferAmount={100}
					defaultSortFieldNum={0}
					id={'id'}
					gridHeight={'70vh'}
					translateHeader
					fields={fields}
					groups={[]}
					totalRows={[]}
					rows={rows}
					cellGreenFields={['SupplierName']}
					successDark={'flag_detail'}
					pagination
					resizable
					displayLoadingScreen={!isDataReady}
				/>

				{selectedSupplier ? (
					<GenericDrawer
						{...{
							drawerType: 'TAB',
							caption: `${selectedSupplier.SupplierName} | ${selectedSupplier.BranchName}`,
							shown: showEditModal,
							toggleModal: toggleEditModal,
							data: editDataDrawer(selectedSupplier, userEditFlag),
							saveChanges: saveSideBarChanges,
							isTabsHidden: true,
							saveDisabled: !userEditFlag,
							titleEditable: false,
							hideZeroFilters: true,
							customFieldsValdtion: data => {
								return customValidation(data);
							},
							required: []
						}}
					/>
				) : null}

				{modalType === ModalTypes.error || modalType === ModalTypes.delete ? (
					<ModalsData
						modalHandlers={modalHandlers}
						modalType={modalType}
						modalMessage={modalMessage}
						modalTitle={modalTitle}
						toggleModal={(type: ModalTypes) => toggleModal(type)}
					/>
				) : modalType == ModalTypes.import ? (
					<ImportExcelModal
						modalHandlers={modalHandlers}
						toggleModal={(type: ModalTypes) => toggleModal(type)}
						indexField={'BarCode'}
						wantedFields={['BranchId', 'orderDay', 'supplyDay', 'action']}
						exampleFields={[
							{ text: 'BarCode', type: 'string', width: window.innerWidth * 0.03, minWidth: 50 },
							{ text: 'BranchId', type: 'string', width: window.innerWidth * 0.03, minWidth: 50 },
							{ text: 'orderDay', type: 'string', width: window.innerWidth * 0.03, minWidth: 50 },
							{ text: 'supplyDay', type: 'string', width: window.innerWidth * 0.03, minWidth: 50 },
							{ text: 'action', type: 'string', width: window.innerWidth * 0.03, minWidth: 50 }
						]}
						exampleRows={[
							{ BarCode: '7290321', BranchId: '5', orderDay: '5', supplyDay: '2', action: '1' },
							{ BarCode: '7290321', BranchId: '5', orderDay: '5', supplyDay: '2', action: '1' }
						]}
						description={
							<div>
								<span>BarCode - {formatMessage({ id: 'itemBarcode' })}</span> <br />
								<span>BranchId - {formatMessage({ id: 'BranchNumber' })}</span> <br />
								<span>orderDay - {formatMessage({ id: 'orderDay' })}</span> <br />
								<span>supplyDay - {formatMessage({ id: 'supplyDay' })}</span> <br />
								<div className="d-flex">
									action -{' '}
									<span style={{ marginRight: '3px' }}>
										1 = {formatMessage({ id: 'addAction' })},{' '}
									</span>{' '}
									<span style={{ marginRight: '3px' }}>
										{' '}
										2 = {formatMessage({ id: 'deleteAction' })}
									</span>{' '}
								</div>
							</div>
						}
					/>
				) : null}
			</div>{' '}
		</>
	);

	if (showSupplyDaysGrid == false) {
		return mainGrid;
	} else {
		return (
			<SupplyItemsGrid
				back={() => {
					// console.log(chosenBranch)
					// console.log(chosenSupplier)
					// console.log(chosenSubSupplier)
					setShowSupplyDaysGrid(!showSupplyDaysGrid);
					handleFilter();
					// setRows(originalRows)
				}}
				onUpdateSupplyItem={updateSupplyItemsDays}
				supplier={selectedSupplier}
				catalogItems={catalogItems}
				appAspakaItems={appAspakaItems}
				appSubSapakAllow={subSuppliersAllow}
			/>
		);
	}
};

export default SupplyDays;
