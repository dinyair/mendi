import * as React from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { RootStore } from '@src/store';
import { useSelector, useDispatch } from 'react-redux';
import { getLogo } from '@src/containers/stock/orders/initial-state';
import { config } from '@src/config';
import classnames from "classnames"

interface Props {}

export const Home: React.FC<Props> = (props: Props) => {
	const { formatMessage } = useIntl();
	const user = useSelector((state: RootStore) => state.user);
	document.title = formatMessage({ id: 'Dashboard' });
	const customizer = { direction: document.getElementsByTagName("html")[0].dir }

	let networks = [
		{ value: 'nyocha', label: 'יוחננוף' },
		{ value: 'mahyan', label: 'מעיין 2000' },
		{ value: 'nsuper', label: 'סופר סופר' },
		{ value: 'nbonus', label: 'סופר בונוס' },
		{ value: 'nkeshet', label: 'קשת טעמים' },
		{ value: 'nviktory', label: 'ויקטורי' },
		{ value: 'nbitan', label: 'יינות ביתן' }
	];

	let currentDB = networks.filter(n => n.value === user.db)[0];

	return (
		<div className='width-100-per min-height-85-vh d-flex'>
			<div
				className={classnames('d-flex m-auto', {
					'flex-direction-row-reverse': customizer.direction == 'ltr' &&  window.innerWidth >= 1024,
					'flex-direction-column-reverse': window.innerWidth < 1024
				})}
			>
				<div className="d-flex justify-content-center align-items-center width-40-per m-auto">
					{currentDB && currentDB.value && (
						<img className='max-width-30-vw' src={`${config.imagesPath}customers/` + currentDB.value + '.png'} />
					)}
				</div>

				<div
					className={`bg-darkgray ${
						window.innerWidth < 1024
							? 'm-auto-7-rem width-65-per height-02-rem'
							: 'ml-5 mr-5 width-0-2-rem height-auto'
					} `}
				></div>

				<div className="d-flex justify-content-center align-items-center width-100-per">
					<img className='width-100-per' src={`${config.imagesPath}` + 'algoretail-bydata.svg'} />
				</div>
			</div>
		</div>
	);
};

export default Home;
