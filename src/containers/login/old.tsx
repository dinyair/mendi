import * as React from 'react';
import { useDispatch } from 'react-redux';
import { LoginUser, RegisterUser, checkAuth } from './initial-state'
import { useHistory } from 'react-router-dom';
import { UserActionTypes } from '@containers/user/enums';
import { UserInterface } from './interfaces';
import * as AuthActions from "../../redux/actions/auth/authActions"
import { setUser } from "@src/planogram/shared/store/auth/auth.actions";
import { store } from "@src/planogram/shared/store";
import { setAuth, removeAuth } from "@src/planogram/shared/store/auth/auth.actions";
import brandLogo from '../../assets/images/brand-logo.png';
import "./LoginScreen.scss";
import AsyncSelect from "react-select/async";
import { Routes } from '@src/utilities';
import Cookies from 'universal-cookie';
import { useIntl } from 'react-intl';
import { TabContent, TabPane, Row, Col, Button, } from 'reactstrap';
import { Tabs, Tab } from '@src/components/tabs';
import { Icon } from '@src/components';
import { config } from '@src/config';

export const Login: React.FunctionComponent = () => {
	const dispatch = useDispatch();
	const [submitted, setSubmitted] = React.useState<boolean>(false);
	const [username, setUsername] = React.useState<string>('');
	const [password, setPassword] = React.useState<string>('');
	const [firstName, setFirstName] = React.useState<string>('');
	const [lastName, setLastName] = React.useState<string>('');
	const [message, setMessage] = React.useState<string>('');

	const [loader, SetLoader] = React.useState<boolean>(false);
	const history = useHistory();
	const [View, setVi2ew] = React.useState<string>('login');
	const [network, setNetwork] = React.useState(null);
	const url = window.location.href;
	const urlElements = url.split('/');
	const cookies = new Cookies();


	const { formatMessage } = useIntl();
	document.title = formatMessage({ id: View })

	const LOGIN_TABS = {
		PASSWORD: formatMessage({ id: 'LoginWithPassword' }),
		SMS: formatMessage({ id: 'LoginWithSMS' }),
	};

	const [activeTab, setActiveTab] = React.useState(LOGIN_TABS.PASSWORD);


	const handleKeypress = (e: any) => {
		if (e.key === 'Enter') {
			View === 'login' && handleLogin(username, password)
		}
	};


	const handleChangeView = () => {
		setFirstName('')
		setLastName('')
		setUsername('')
		setPassword('')
		setView(View == 'login' ? 'register' : 'login')
	}


	const handleLogin = async (username: string, password: string) => {
		if (username && password) {
			try {
				let User: any = await LoginUser(username, password, network)
				if (User) {
					store.dispatch(setAuth(User, cookies.get('token')));
					setUser(User);
					dispatch(AuthActions.setUser(User)); history.push('/');
				}

			} catch (error) {
				setMessage(formatMessage({ id: 'Username and/or Password are invalid' }))
			}
		}
	}



	const isAdminLogin = '/' + urlElements[3] === Routes.ADMIN_LOGIN;


	const newLoginView = (
		<>
			<div className="app-login d-flex justify-content-center flex-column" onKeyDown={handleKeypress} tabIndex={0}>
				<div className="d-flex align-self-center justify-content-center bg-gray p-5 border-radius-10">
					<div className="d-flex flex-column align-items-center">
						<Icon className="mb-3" src={config.imagesPath + 'algoretail-icon.svg'} />
						{(
							<div className="mb-1-5">
								<Tabs value={LOGIN_TABS.PASSWORD} onChangTab={setActiveTab}>
									<Tab caption={LOGIN_TABS.PASSWORD} name={LOGIN_TABS.PASSWORD} />
									<Tab caption={LOGIN_TABS.SMS} name={LOGIN_TABS.SMS} />
								</Tabs>
							</div>
						)}


						<TabContent activeTab={activeTab}>
							<TabPane tabId={LOGIN_TABS.PASSWORD}>
								<div className="d-flex align-self-center justify-content-center">
									Password
								</div>
							</TabPane>

							<TabPane tabId={LOGIN_TABS.SMS}>
								<div className="d-flex align-self-center justify-content-center">
									SMS
								</div>
							</TabPane>
						</TabContent>
					</div>

				</div>
			</div>
		</>
	)

	const loginView = (
		<div className="app-login" onKeyDown={handleKeypress} tabIndex={0}>
			<div className="login-container">
				<img src={brandLogo} className={'img-fluid'} style={{ minHeight: "200px" }} alt="Algoretail Logo" />
				<div className={'mt-2'}>

					<div className={'form-group input-row' + (submitted && !username ? ' has-error' : '')}>
						<label htmlFor="username" className={"col-sm-2 col-form-label"}>{formatMessage({ id: 'username' })}:</label>
						<input type="text" className="form-control" name="username" value={username} onChange={(e) => setUsername(e.target.value)} />
						{/* {submitted && !username &&
							<div className="help-block">Username is required</div>
						} */}
					</div>
					<div className={'form-group input-row' + (submitted && !password ? ' has-error' : '')}>
						<label htmlFor="password" className={"col-sm-2 col-form-label"}>{formatMessage({ id: 'password' })}:</label>
						<input type="password" className="form-control" name="password" value={password} onChange={(e) => setPassword(e.target.value)} />

						{/* {submitted && !password &&
							<div className="help-block">Password is required</div>
						} */}
					</div>


					{isAdminLogin ?
						<AsyncSelect
							id="Network"
							onChange={(option: any) => setNetwork(option.value)}
							components={{ IndicatorsContainer: (props: any) => <div {...props} /> }}
							className='width-20-rem margin-auto pb-1'
							defaultOptions={[
								{ value: "nyocha", label: "יוחננוף" },
								{ value: "mahyan", label: "מעיין 2000" },
								{ value: "nsuper", label: "סופר סופר" },
								{ value: "nbonus", label: "סופר בונוס" },
								{ value: "nkeshet", label: "קשת טעמים" },
								{ value: "nviktory", label: "ויקטורי" },
								{ value: "nbitan", label: "יינות ביתן" }
							]}
							// isClearable={true}
							styles={{
								control: provided => ({
									...provided,
									paddingRight: 5,

								})
							}}
							theme={theme => ({
								...theme,
								borderRadius: 20,
								spacing: {
									baseUnit: 3.5,
									controlHeight: 30,
									menuGutter: 8,
								},
								colors: {
									...theme.colors,
									primary: '#31baab'
								}
							})}
						/>
						: null}
					<div className="form-group input-row">
						<button disabled={!isAdminLogin ? (!password || !username) : (!password || !username || !network)} className="btn btn-primary btn-block"
							onClick={() => handleLogin(username, password)}>{formatMessage({ id: 'login' })}</button>


						{loader &&
							<img src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
						}

						{/* <button className="btn btn-primary" style={{display: 'block', margin: '3vh 0'}}
									 onClick={ handleChangeView }>register now!</button> */}
					</div>



					{/* case of error in login */}
					{message &&
						<div className="text-center mb-2">{message}</div>}


				</div>
			</div>
		</div>
	)


	const RegisterView = (
		<div className="col-md-6 col-md-offset-3" onKeyDown={handleKeypress} tabIndex={0}>
			<h2>Register</h2>
			<div>
				<div className={'form-group' + (submitted && !username ? ' has-error' : '')}>
					<label htmlFor="username">Username</label>
					<input type="text" className="form-control" name="username" value={username} onChange={(e) => setUsername(e.target.value)} />
					{submitted && !username &&
						<div className="help-block">Email is required</div>
					}
				</div>


				<div className={'form-group' + (submitted && !firstName ? ' has-error' : '')}>
					<label htmlFor="firstName">First name</label>
					<input type="text" className="form-control" name="firstName" value={firstName} onChange={(e) => setFirstName(e.target.value)} />
					{submitted && !firstName &&
						<div className="help-block">First name is required</div>
					}
				</div>


				<div className={'form-group' + (submitted && !lastName ? ' has-error' : '')}>
					<label htmlFor="lastName">Last name</label>
					<input type="text" className="form-control" name="lastName" value={lastName} onChange={(e) => setLastName(e.target.value)} />
					{submitted && !lastName &&
						<div className="help-block">Last name is required</div>
					}
				</div>

				<div className={'form-group' + (submitted && !password ? ' has-error' : '')}>
					<label htmlFor="password">Password</label>
					<input type="password" className="form-control" name="password" value={password} onChange={(e) => setPassword(e.target.value)} />
					{submitted && !password &&
						<div className="help-block">Password is required</div>
					}
				</div>
				<div className="form-group">
					<button className="btn btn-primary"
						onClick={async () => {
							let User: any = await RegisterUser(firstName, lastName, username, password, 1, 1)
							if (User) dispatch(AuthActions.setUser(User)); history.push('/');
						}}>register</button>
					{loader &&
						<img src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
					}

					<button className="btn btn-primary" style={{ display: 'block', margin: '3vh 0' }}
						onClick={handleChangeView}>login now!</button>
				</div>
			</div>
		</div>
	)



	return View == 'login' ?
		loginView
		// newLoginView
		: RegisterView
};

export default Login;