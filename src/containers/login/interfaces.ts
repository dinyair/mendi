import { UserActionTypes } from '@containers/user/enums';

export interface UserInterface {
	id: any;
	first_name: string;
	last_name: string;
	company_id: number;
	email: string
}

export interface UsersActionInterface {
	type: UserActionTypes;
	payload: any;
}



export interface LoginDetails  {
	username: null | string,
	 password : string | null,
	  phoneNumber  : string | null, 
	  newPassword:  string | null, 
	  repeatPassword: string | null

}