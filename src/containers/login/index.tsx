import * as React from 'react';
import "./LoginScreen.scss";
import { Routes } from '@src/utilities';
import { useIntl } from 'react-intl';
import { TabContent, TabPane } from 'reactstrap';
import { Tabs, Tab } from '@src/components/tabs';
import { config } from '@src/config';
import { Icon } from '@src/components';
import LoginPasswordTab from './components/tabs/password-tab';
import LoginSmsTab from './components/tabs/sms-tab';
import { ArrowRight } from 'react-feather';
import ReCAPTCHA from "react-google-recaptcha";
import ResetPasswordTab from './components/tabs/reset-password-tab';

import { useHistory, useLocation } from 'react-router';
import { useSelector } from 'react-redux';
import { RootStore } from '@src/store';
import { checkResetPasswordDiff, isAuth } from '../user/initial-state';

export const Login: React.FunctionComponent = () => {
	const { formatMessage } = useIntl();
	document.title = formatMessage({ id: 'login' })
	const url = window.location.href;

	const LOGIN_TABS = {
		PASSWORD: formatMessage({ id: 'LoginWithPassword' }),
		SMS: formatMessage({ id: 'LoginWithSMS' }),
	};

	const [network, setNetwork] = React.useState<any>(null);
	const [activeTab, setActiveTab] = React.useState(LOGIN_TABS.PASSWORD);
	const [isLoading, setIsLoading] = React.useState<boolean>(false);
	const [isResetPassword, setIsResetPassword] = React.useState<boolean>(false);
	const [isAdminLogin, setIsAdminLogin] = React.useState<boolean>(false);
	const location = useLocation();
	const [daysLeftToRenew, setDaysLeftToRenew] = React.useState<number | null>(null);

	

	const [isSMSPasswordSent, setIsSMSPasswordSent] = React.useState<boolean>(false);
	const [isLoggedIn, setIsLoggedIn] = React.useState<boolean>(false);
	const direction = document.getElementsByTagName("html")[0].dir;
	const [enterKeyPressed, setEnterKeyPressed] = React.useState<boolean>(false);

	const [showTabs, setShowTabs] = React.useState<boolean>(true);
	const _reCaptchaRef = React.createRef<any>();

	const handleKeypress = (e: any) => {
		if (e.key === 'Enter' && !isLoading) {
			setEnterKeyPressed(true)
			setTimeout(() => setEnterKeyPressed(false), 1);
		}
	};


	React.useEffect(() => {
		checkRoute();
	}, [location])


	React.useEffect(() => {
		if( _reCaptchaRef && _reCaptchaRef.current ) _reCaptchaRef.current.reset();
	}, [])


	const checkRoute = () => {
		const urlElements = url.split('/');
		console.log(urlElements)
		if ('/' + urlElements[3] === Routes.ADMIN_LOGIN && isAuth() === undefined) {
			setIsAdminLogin(true)
		}
		else if ('/' + urlElements[3] + '/' + urlElements[4] === Routes.RESET_PASSWORD) {
			const user = isAuth()
			if (user) {
				const diff = checkResetPasswordDiff(user)
				const daysLeft = 90 - diff >= 0 ? 90 - diff : 0;
				if (diff > 80) {
					setShowTabs(true)
					setDaysLeftToRenew(daysLeft)
					setIsResetPassword(true)
				} else {
					// in case of less than 80 days 
					setDaysLeftToRenew(null)
					setIsResetPassword(true)
				}
			}
		}
	}

	return (
		<>
			<div className="h-100 d-flex align-items-center mobile-background" onKeyDown={handleKeypress} tabIndex={0}>
				<div className="app-login d-flex align-self-center justify-content-center">
					<div className={`login-container bg-gray d-flex flex-column ${!showTabs ? 'pt-1' : 'pt-5'} pb-5`}>
						{!showTabs ? <div className="d-flex justify-content-start mt-1" style={{ marginBottom: '4px' }}>
							<button
								onClick={() => {
									if (!isLoading) {
										setIsSMSPasswordSent(false); setShowTabs(true);
									}
								}}
								className={`back-btn no-outline border-0 bg-transparent outline-0 ${direction === 'ltr' ? 'rotate-180' : ''}`}
							>
								<ArrowRight size={22} />
							</button>
						</div> : null}

						<Icon className={`mb-18px margin-0-auto`} src={config.imagesPath + 'algoretail-icon.svg'} />
						{showTabs ? (
							<div className="mb-1-5 d-flex align-content-center justify-content-center">
								<Tabs value={activeTab} disableChangeTab={isLoading || isLoggedIn || isResetPassword} onChangTab={setActiveTab}>
									<Tab navClassName="p-1" caption={LOGIN_TABS.PASSWORD} name={LOGIN_TABS.PASSWORD} />
									<Tab navClassName="p-1" caption={LOGIN_TABS.SMS} name={LOGIN_TABS.SMS} />
								</Tabs>
							</div>
						) : null
						}


						<div className="d-flex align-content-center justify-content-center">
							{isResetPassword ? <>
								<>
									{/* Reset Password Tab */}
									<ResetPasswordTab setIsLoading={setIsLoading}
										enterKeyPressed={enterKeyPressed} isLoading={isLoading} isLoggedIn={isLoggedIn}
										activeTab={activeTab} daysLeftToRenew={daysLeftToRenew}
									/>
								</>
							</> :

								<TabContent activeTab={activeTab}>
									<TabPane tabId={LOGIN_TABS.PASSWORD}>
										{/* Password Tab */}
										<div className="d-flex flex-column align-self-center justify-content-center ">
											<LoginPasswordTab
												isAdminLogin={isAdminLogin}
												isLoading={isLoading}
												networkValue={network}
												onChangeNetwork={setNetwork}
												setIsLoading={setIsLoading}
												enterKeyPressed={enterKeyPressed}
												activeTab={activeTab === LOGIN_TABS.PASSWORD}
												recaptchaRef={_reCaptchaRef}
											/>
										</div>
									</TabPane>

									<TabPane tabId={LOGIN_TABS.SMS}>
										{/* SMS Tab */}
										<div className="d-flex flex-column align-self-center justify-content-center">
											<LoginSmsTab
												direction={direction}
												isAdminLogin={isAdminLogin}
												isLoading={isLoading}
												isSMSPasswordSent={isSMSPasswordSent}
												networkValue={network}
												setIsSMSPasswordSent={setIsSMSPasswordSent}
												onChangeNetwork={setNetwork}
												setIsLoading={setIsLoading}
												enterKeyPressed={enterKeyPressed}
												activeTab={activeTab === LOGIN_TABS.SMS}
												setShowTabs={setShowTabs}
												recaptchaRef={_reCaptchaRef}
											/>
										</div>
									</TabPane>
								</TabContent>}
						</div>
					</div>
				</div>
			</div>
			<ReCAPTCHA
				size='invisible'
				ref={_reCaptchaRef}
				sitekey={config.captchaKey}
				onExpired={() => { _reCaptchaRef.current.reset() }}
				onChange={() => { _reCaptchaRef.current.reset() }}
			/>
		</>
	)
};

export default Login;