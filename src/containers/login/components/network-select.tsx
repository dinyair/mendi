import * as React from 'react';
import AsyncSelect from "react-select/async";

interface Props {
    readonly onChange : (network:string) => void;
    readonly value : any;
}

export const NetworkSelect: React.FunctionComponent<Props> = (props:Props) => {
    const { onChange,value } = props;
    const [network, setNetwork] = React.useState(null);


    const optionsStyles = {
        container: (base: any) => ({
            ...base,
            textAlign: 'right',
        }),
        option: (provided: any, state: any) => ({
            ...provided,
            "&:hover": {
                color: "#ffffff",
                backgroundColor: "#31baab"
            },
            color: state.isSelected ? '#ffffff' : '#1e1e20',
            backgroundColor: state.isSelected ? '#027b7a' : '#ffffff',
            borderBottom: '1px dotted lightgray',
            padding: 10,
            textAlign: (/[\u0590-\u05FF]/).test(state.options[0].label) ? 'right' : 'left',
        }),
        control: (base: any, state: any) => ({
            ...base,
            boxShadow: state.menuIsOpen ? '0 0 0 0.5px #31baab' : 'none',
        }),
        placeholder: (provided: any) => ({
            ...provided,
            textAlign: document.getElementsByTagName("html")[0].dir === 'ltr' ? 'left' : 'right',
            fontWeight: 600
        }),
        singleValue: (provided: any, state: any) => ({
            ...provided,
            textAlign: 'right',
            fontWeight: 600
        }),
        valueContainer: (provided: any, state: any) => {
            return {
                ...provided,
                justifyContent: state.hasValue ? 'flex-end' : '',
                fontWeight: 600
            }
        }
    }
    
    
    return (
        <>
            <AsyncSelect
                id="Network"
                onChange={(option: any) => {
                    setNetwork(option)
                    onChange(option)
                }}
                className='width-4-rem margin-auto pb-1'
                defaultOptions={[
                    { value: "nyocha", label: "יוחננוף" },
                    { value: "mahyan", label: "מעיין 2000" },
                    { value: "nsuper", label: "סופר סופר" },
                    { value: "nbonus", label: "סופר בונוס" },
                    { value: "nkeshet", label: "קשת טעמים" },
                    { value: "nviktory", label: "ויקטורי" },
                    { value: "nbitan", label: "יינות ביתן" }
                ]}
                value={value}
                loadOptions={() => { }}
                // isClearable={true}
                styles={optionsStyles}
                theme={theme => ({
                    ...theme,
                    borderRadius: 20,
                    spacing: {
                        baseUnit: 3.5,
                        controlHeight: 30,
                        menuGutter: 8,
                    },
                    colors: {
                        ...theme.colors,
                        primary: '#31baab'
                    }
                })}

            />
        </>
    )
};

export default NetworkSelect;




 
/* Dont delete the code below!!! */

// const IconOption = (props: any) => (
//     <Option {...props}>
//         {props.data.label}

//         <img
//             className="ml-1"
//             src={`http://localhost:8080/assets/images/customers/nyocha.png`}
//             style={{ width: 36 }}
//             alt={props.data.label}
//         />
//     </Option>
// );

// const CustomSelectValue = (props:any) => (
//     <div className="d-flex justify-content-end">
//         {props.data.label}

//         <img
//             className="ml-1"
//             src={`http://localhost:8080/assets/images/customers/nyocha.png`}
//             style={{ width: 36 }}
//             alt={props.data.label}
//         />
//     </div>
// )