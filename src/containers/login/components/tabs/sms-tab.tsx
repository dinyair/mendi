import * as React from 'react';
import NetworkSelect from '../network-select';
import { useIntl } from 'react-intl';
import InputText from '@src/components/input-text';
import { LoginDetails } from '../../interfaces';
import { Button } from 'reactstrap';
import { checkPhoneNumber, smsLogin, } from '../../initial-state'
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { setUser } from "@src/planogram/shared/store/auth/auth.actions";
import { store } from "@src/planogram/shared/store";
import { setAuth } from "@src/planogram/shared/store/auth/auth.actions";
import Cookies from 'universal-cookie';
import * as AuthActions from "../../../../redux/actions/auth/authActions"
import * as moment from 'moment';
import CountDownTimer from '../../../../components/CountDownTimer/countdown-timer';
import InputNumber from '@src/components/input-number';
import { Icon } from '@src/components';
import { config } from '@src/config';

interface Props {
    readonly onChangeNetwork: (network: any) => void;
    readonly networkValue: any;
    readonly isSMSPasswordSent: boolean;
    readonly setIsSMSPasswordSent: (isResetPasswordSent: boolean) => void;
    readonly isAdminLogin: boolean;
    readonly isLoading: boolean;
    readonly setIsLoading: (isLoading: boolean) => void;
    readonly setShowTabs: (showTabs: boolean) => void;
    readonly direction: string;
    readonly enterKeyPressed: boolean;
    readonly activeTab: boolean;
    readonly recaptchaRef: any;
}

export const LoginSmsTab: React.FunctionComponent<Props> = (props: Props) => {
    const { onChangeNetwork, networkValue, isSMSPasswordSent, setIsSMSPasswordSent, isAdminLogin, isLoading, setIsLoading, direction,
        enterKeyPressed, activeTab, recaptchaRef, setShowTabs
    } = props;
    const { formatMessage } = useIntl();
    const dispatch = useDispatch();
    const history = useHistory();
    const cookies = new Cookies();

    const [loginDetails, setLoginDetails] = React.useState<LoginDetails>({ username: null, password: null, phoneNumber: null, newPassword: null, repeatPassword: null })
    const [inputsErrors, setInputsErrors] = React.useState<any>({ username: null, password: null, phoneNumber: null, newPassword: null, repeatPassword: null });
    const [countdownTime, setCountdownTime] = React.useState<number | undefined>();
    const [smsCode, setSmsCode] = React.useState({ digit1: null, digit2: null, digit3: null, digit4: null })
    const [smsError, setSmsError] = React.useState<any>(null);
    const [verifiedNumber, setVerifiedNumber] = React.useState<boolean>(false);

    const digit1Input = React.useRef<any>();
    const digit2Input = React.useRef<any>();
    const digit3Input = React.useRef<any>();
    const digit4Input = React.useRef<any>();
	const isDev = process.env.NODE_ENV === 'development';


    React.useEffect(() => {
        if (enterKeyPressed && activeTab) {
            if (!isSMSPasswordSent && validPhoneNumber()) handleSendSMSPassword()
            else if (isSMSPasswordSent && smsCode.digit1 && smsCode.digit2 && smsCode.digit3 && smsCode.digit4) handleSMSLogin()
            else if (isSMSPasswordSent && countdownTime === 0) sendNewSms()
        }
    }, [enterKeyPressed])

    const smsFields = [{
        id: 'phoneNumber',
        label: formatMessage({ id: 'PhoneNumber' }),
        value: loginDetails.phoneNumber,
        type: 'number',
        icon: <Icon src={config.iconsPath + `login/phone.svg`} />
    },
    ]

    const smsCodeFields = [{
        id: 'digit1',
        value: smsCode.digit1,
        type: 'number',
        noPlaceholder: true,
        inputRef: digit1Input
    }, {
        id: 'digit2',
        value: smsCode.digit2,
        type: 'number',
        noPlaceholder: true,
        inputRef: digit2Input
    }, {
        id: 'digit3',
        value: smsCode.digit3,
        type: 'number',
        noPlaceholder: true,
        inputRef: digit3Input
    }, {
        id: 'digit4',
        value: smsCode.digit4,
        type: 'number',
        noPlaceholder: true,
        inputRef: digit4Input
    },
    ]

    const form = (fields: any) =>
    (<div className="d-flex flex-column">
        {fields.map((item: any, index: number) => {
            return (
                <div className="width-18-rem mb-1">
                    {/* <label className="font-medium-1 pl-1 pr-1" htmlFor={item.id}>{item.label}</label> */}
                    <InputText
                        id={item.id}
                        onChange={(e) => {
                            if (e.target.value.trim().length === 0 || e.target.value == '') setLoginDetails({ ...loginDetails, [item.id]: null })
                            else setLoginDetails({ ...loginDetails, [item.id]: e.target.value })
                        }}
                        value={item.value || ""}
                        placeholder={item.label}
                        inputClassNames="pl-1-5 pr-1-5 input-placeholer-black"
                        classNames={inputsErrors[item.id] && inputsErrors[item.id].length > 0 ? 'mb-0' : 'mb-1'}
                        inputType={item.type}
                        customIcon={item.icon}
                        inputRef={item.inputRef || null}
                        noMarginRight
                        noMarginBottom
                        error={inputsErrors[item.id] && inputsErrors[item.id].length > 0 ? true : false}
                        errorLabel={item.id != 'username' ? inputsErrors[item.id] : null}
                    />
                </div>
            )
        })
        }
    </div>)


    const smsCodeForm = () =>
    (<div className={`d-flex margin-0-auto ${direction === 'rtl' ? 'flex-row-reverse' : 'flex-row'}`}>
        {smsCodeFields.map((item: any, index: number) => {
            return (
                <div className="mr-1 ml-1">
                    <InputNumber
                        id={item.id}
                        onChange={(e) => {
                            if (e.target.value.trim().length === 0 || e.target.value === '' || isNaN(Number(e.target.value))) setSmsCode({ ...smsCode, [item.id]: null })
                            else {
                                let newValue = e.target.value;
                                if (newValue.length > 1) newValue = newValue.slice(newValue.length - 1, newValue.length)
                                setSmsCode({ ...smsCode, [item.id]: newValue })
                                switch (index + 1) {
                                    case 1: digit2Input.current.focus(); break;
                                    case 2: digit3Input.current.focus(); break;
                                    case 3: digit4Input.current.focus(); break;
                                }
                            }
                        }}
                        value={item.value || ""}
                        placeholder={item.label}
                        classNames={inputsErrors[item.id] && inputsErrors[item.id].length > 0 ? 'mb-0' : 'mb-1'}
                        inputRef={item.inputRef || null}
                        error={inputsErrors[item.id] && inputsErrors[item.id].length > 0 ? true : false}
                        errorLabel={inputsErrors[item.id] || null}
                        oneDigitInput
                        onFocusEvent={(focus: boolean) => {
                            if (focus) {
                                switch (index + 1) {
                                    case 1: digit1Input.current.select(); break;
                                    case 2: digit2Input.current.select(); break;
                                    case 3: digit3Input.current.select(); break;
                                    case 4: digit4Input.current.select(); break;
                                }
                            }
                        }}
                        inputClassNames="input-placeholer-black"
                        validateNumbersOnly
                    />
                </div>
            )
        })
        }
    </div>)


    const validPhoneNumber = () => {
        const value: any = loginDetails.phoneNumber;
        return /(?=.*\d)/.test(value) && value.length > 9;
    }


    const handleSendSMSPassword = async () => {
        let captchaToken = !isDev ? await recaptchaRef.current.executeAsync() : true;
        if (captchaToken) {
            setIsLoading(true)
            const number: any = loginDetails.phoneNumber;
            const res: any = await checkPhoneNumber(number)
            setIsLoading(false)
            if (res.validate) setVerifiedNumber(true)
            else setVerifiedNumber(false)
            setCountdownTime(60);
            setShowTabs(false)
            setIsSMSPasswordSent(true);
        }
    }


    const handleSMSLogin = async () => {
        if (smsCode.digit1 !== null && smsCode.digit2 !== null && smsCode.digit3 !== null && smsCode.digit4 !== null) {
            setIsLoading(true)

            const phoneNumber: any = loginDetails.phoneNumber;
            let code = `${smsCode.digit1}${smsCode.digit2}${smsCode.digit3}${smsCode.digit4}`
            try {
                const User: any = await smsLogin(phoneNumber, code, networkValue && networkValue.value ? networkValue.value : null)
                if (User.error) {
                    setSmsError(formatMessage({ id: User.error }))
                }
                else if (User) {
                    const { passwordResetDate } = User;
                    let resetDate = moment(passwordResetDate, "YYYY-MM-DD")
                    let now = moment();
                    let diff = now.diff(resetDate, 'days');

                    store.dispatch(setAuth(User, cookies.get('token')));
                    setUser(User);
                    dispatch(AuthActions.setUser(User));

                    if (diff > 80) history.push('/login/reset');
                    else history.push('/');
                }
            } catch (error) {
                setSmsError(formatMessage({ id: 'Unable to authenticate' }))
            } finally {
                setIsLoading(false)
            }
        }
    }

    const sendNewSms = () => {
        if (!isLoading) {
            setSmsError(null)
            setSmsCode({ digit1: null, digit2: null, digit3: null, digit4: null })
            if (verifiedNumber) handleSendSMSPassword()
            else setCountdownTime(10);
        }
    }


    return (
        !isSMSPasswordSent ?
            <div className="">
                <>
                    {isAdminLogin ? <NetworkSelect onChange={(network: any) => onChangeNetwork(network)} value={networkValue} /> : null}
                    {form(smsFields)}
                    <Button disabled={!isLoading && validPhoneNumber() ? false : true} onClick={() => handleSendSMSPassword()} style={{ marginTop: "3.72rem" }} className='round mt-05 height-2-5-rem d-flex justify-content-center align-items-center width-18-rem' color='primary'>{formatMessage({ id: 'login' })}</Button>
                </>
            </div>
            :
            <>
                <p className="mt-1-1 text-center">{formatMessage({ id: 'smsVerifyCodeSent' })} <br />{formatMessage({ id: 'pleaseEnter4DigitsCode' })}  </p>
                {smsCodeForm()}
                {<div className={`text-bold-600 font-small-2 text-danger min-height-1-rem max-height-1-rem  text-center ${!smsError ? 'visibility-hidden' : 'visibility-shown'}`}>{smsError}</div>}
                {countdownTime && countdownTime > 0 ?
                    (<p className={`text-center`}>{formatMessage({ id: 'IfYouDidntReceiveTheCode' })}
                        <CountDownTimer seconds={countdownTime} onTimerEnd={() => {
                            setCountdownTime(0)
                        }} /> {formatMessage({ id: 'seconds' })}</p>)
                    :
                    (<p className="p-075 text-center">
                        <span onClick={sendNewSms} className={`${isLoading ? 'cursor-default' : 'cursor-pointer'} text-turquoise`}> {formatMessage({ id: 'click here' })}</span> {formatMessage({ id: 'to send another code' })}
                    </p>)}
                <Button disabled={!isLoading && smsCode.digit1 && smsCode.digit2 && smsCode.digit3 && smsCode.digit4 ? false : true} onClick={() => handleSMSLogin()} className='mt-1-2-5 round height-2-5-rem d-flex justify-content-center align-items-center align-self-center width-18-rem' color='primary'>{formatMessage({ id: 'login' })}</Button>
            </>
    )
};

export default LoginSmsTab;


