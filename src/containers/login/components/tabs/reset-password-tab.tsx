import * as React from 'react';
import { useIntl } from 'react-intl';
import InputText from '@src/components/input-text';
import { LoginDetails } from '../../interfaces';
import { Button } from 'reactstrap';
import PasswordRulesPopUp from '../password-rules-popup';
import { ValidatePasswordWithErrors } from '@src/helpers/password-validation';
import { LoginUser, } from '../../initial-state'
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { setUser } from "@src/planogram/shared/store/auth/auth.actions";
import { store } from "@src/planogram/shared/store";
import { setAuth } from "@src/planogram/shared/store/auth/auth.actions";
import Cookies from 'universal-cookie';
import * as AuthActions from "../../../../redux/actions/auth/authActions"
import CountDownTimer from '../../../../components/CountDownTimer/countdown-timer';
import { changeUserPassword } from '@src/containers/settings/users/initial-state';
import { Icon } from '@src/components';
import { config } from '@src/config';
import { RootStore } from '@src/store';

interface Props {
    readonly isLoading: boolean;
    readonly setIsLoading: (isLoading: boolean) => void;
    readonly isLoggedIn: boolean;
    readonly enterKeyPressed: boolean;
    readonly activeTab: any;
    readonly daysLeftToRenew: number | null;
}

export const ResetPasswordTab: React.FunctionComponent<Props> = (props: Props) => {
    const { isLoading, isLoggedIn, enterKeyPressed, setIsLoading, activeTab, daysLeftToRenew } = props;
    const [isResetPassword, setIsResetPassword] = React.useState<boolean>(false);
    const { formatMessage } = useIntl();
    const [inputsErrors, setInputsErrors] = React.useState<any>({ username: null, password: null, phoneNumber: null, newPassword: null, repeatPassword: null });
    const [isResetPasswordFocused, setIsResetPasswordFocused] = React.useState<boolean>(false);
    const [countdownTime, setCountdownTime] = React.useState<number | undefined>(10);
    const dispatch = useDispatch();
    const history = useHistory();
    const cookies = new Cookies();
    const [loginDetails, setLoginDetails] = React.useState<LoginDetails>({ username: null, password: null, phoneNumber: null, newPassword: null, repeatPassword: null })

    const user = useSelector((state: RootStore) => state.user)

    React.useEffect(() => {
        if (enterKeyPressed && activeTab) {
            if (!isResetPassword && daysLeftToRenew != null) setIsResetPassword(true)
            if (isResetPassword) checkIfNewPasswordsValid()
        }
    }, [enterKeyPressed])

    const resetPasswordFields = [
        {
            id: 'newPassword',
            label: formatMessage({ id: 'NewPassword' }),
            value: loginDetails.newPassword,
            type: 'password',
            icon: <Icon src={config.iconsPath + `login/password.svg`} />
        }, {
            id: 'repeatPassword',
            label: formatMessage({ id: 'RepeatNewPassword' }),
            value: loginDetails.repeatPassword,
            type: 'password',
            icon: <Icon src={config.iconsPath + `login/repeat-password.svg`} />
        }]


    const form = (fields: any) =>
    (<div className="d-flex flex-column mb-1">
        {fields.map((item: any, index: number) => {
            return (
                <div className="width-18-rem">
                    {/* <label className="font-medium-1 pl-1 pr-1" htmlFor={item.id}>{item.label}</label> */}
                    <InputText
                        id={item.id}
                        onChange={(e) => {
                            if (e.target.value.trim().length === 0 || e.target.value == '') setLoginDetails({ ...loginDetails, [item.id]: null })
                            else setLoginDetails({ ...loginDetails, [item.id]: e.target.value })
                        }}
                        value={item.value || ""}
                        placeholder={item.label}
                        inputClassNames="pl-1-5 pr-1-5 input-placeholer-black"
                        classNames={inputsErrors[item.id] && inputsErrors[item.id].length > 0 ? 'mb-0' : 'mb-1'}
                        inputType={item.type}
                        customIcon={item.icon}
                        inputRef={item.inputRef || null}
                        noMarginRight
                        noMarginBottom
                        isFocused={(isFocused) => index === 0 ? setIsResetPasswordFocused(isFocused) : null}
                        error={inputsErrors[item.id] && inputsErrors[item.id].length > 0 ? true : false}
                        errorLabel={item.id != 'username' ? inputsErrors[item.id] : null}
                    />
                </div>
            )
        })
        }
    </div>)


    const checkIfNewPasswordsValid = () => {
        const { repeatPassword, newPassword, password } = loginDetails;
        let hasError: boolean = false;
        if (repeatPassword != newPassword) {
            setInputsErrors({ ...inputsErrors, repeatPassword: formatMessage({ id: "passwordsDoNotMatch" }) })
            hasError = true;
        }

        const validationError = ValidatePasswordWithErrors(repeatPassword || '')
        if (validationError && !hasError) {
            setInputsErrors({ ...inputsErrors, repeatPassword: formatMessage({ id: validationError }) })
            hasError = true;
        }

        if (!hasError) {
            setInputsErrors({ ...inputsErrors, repeatPassword: null })
            handleResetPassword()
        }
    }

    const handleResetPassword = async () => {
        setIsLoading(true)
        const request: any = await changeUserPassword(user.id, loginDetails.newPassword || '')
        if (request.res && request.res.affectedRows > 0) {
            let User: any = await LoginUser(request.username, loginDetails.newPassword || '')
            if (User) {
                store.dispatch(setAuth(User, cookies.get('token')));
                setUser(User);
                dispatch(AuthActions.setUser(User));
                history.push('/');
            }
        }
        else if (request.error) setInputsErrors({ ...inputsErrors, repeatPassword: formatMessage({ id: 'cannotUseOldPassword' }) })
        setIsLoading(false)
    }

    const handleTimerEnd = async () => {
        if (daysLeftToRenew == null || daysLeftToRenew > 0) history.push('/');
    }

    return (
        !isResetPassword && daysLeftToRenew != null ?
            <div className="d-flex flex-column justify-content-center">
                    <div className="text-black text-bold-600  p-05">
                        <div className="bg-turquoise-light border-radius-10 d-flex flex-row">
                            <div className={`ml-05-6 mt-1-2-rem`} >
                                <Icon className="" src={config.iconsPath + `login/info.svg`} />
                            </div>

                            <div>
                                <p className="pt-1 pl-05 mb-0 pr-1" style={{ marginTop: '4px' }}>
                                    <span> {formatMessage({ id: 'ThereAre' })} {daysLeftToRenew} {formatMessage({ id: 'daysLeftToRenewYourPassword' })}</span> </p>
                                <p className="pb-1 pl-05  mb-0">
                                    <span className="text-turquoise underline cursor-pointer text-bold-700" onClick={() => { setIsResetPassword(true); }}>{formatMessage({ id: 'click here' })}</span> {formatMessage({ id: 'toChangeYourPassword' })}
                                </p></div>
                        </div>
                        {daysLeftToRenew > 0 ?
                            <p className="p-05">
                                {formatMessage({ id: 'youAreBeingRedirectedToTheSystemWithin' }) + ' '}
                                {countdownTime ? <CountDownTimer seconds={countdownTime} onTimerEnd={handleTimerEnd} /> : null}
                                {' ' + formatMessage({ id: 'seconds' })}
                            </p>
                            : <p className="min-height-0-7-5-rem"> </p>
                        }
                    </div>
                <Button disabled={true} onClick={() => { }}
                    className={`${isLoggedIn ? "mt-0" : "mt-05"}  round  height-2-5-rem d-flex justify-content-center align-items-center align-self-center width-18-rem`}
                    color='primary'>{formatMessage({ id: 'login' })}</Button>
            </div>
            :
            <div className="d-flex flex-column justify-content-center">
                {isResetPasswordFocused ? <PasswordRulesPopUp /> : null}
                {form(resetPasswordFields)}
                <Button disabled={!isLoading ? false : true} onClick={() => checkIfNewPasswordsValid()} className='mt-05 round height-2-5-rem d-flex justify-content-center align-items-center align-self-center width-18-rem' color='primary'>{formatMessage({ id: 'ResetPassword' })}</Button>
            </div>

    )
};

export default ResetPasswordTab;


