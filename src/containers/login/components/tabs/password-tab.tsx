import * as React from 'react';
import NetworkSelect from '../network-select';
import { useIntl } from 'react-intl';
import InputText from '@src/components/input-text';
import { LoginDetails } from '../../interfaces';
import { Button, } from 'reactstrap';
import { LoginUser, } from '../../initial-state'
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { setUser } from "@src/planogram/shared/store/auth/auth.actions";
import { store } from "@src/planogram/shared/store";
import { setAuth } from "@src/planogram/shared/store/auth/auth.actions";
import Cookies from 'universal-cookie';
import * as AuthActions from "../../../../redux/actions/auth/authActions"
import * as moment from 'moment';
import { Icon } from '@src/components';
import { config } from '@src/config';

interface Props {
    readonly onChangeNetwork: (network: any) => void;
    readonly networkValue: any;
    readonly isAdminLogin: boolean;
    readonly isLoading: boolean;
    readonly setIsLoading: (isLoading: boolean) => void;
    readonly enterKeyPressed: boolean;
    readonly activeTab: boolean;
    readonly recaptchaRef: any;
}

export const LoginPasswordTab: React.FunctionComponent<Props> = (props: Props) => {
    const { onChangeNetwork, networkValue, isAdminLogin, isLoading, setIsLoading,
        enterKeyPressed, activeTab, recaptchaRef } = props;
    const { formatMessage } = useIntl();
    const [loginDetails, setLoginDetails] = React.useState<LoginDetails>({ username: null, password: null, phoneNumber: null, newPassword: null, repeatPassword: null })
    const [inputsErrors, setInputsErrors] = React.useState<any>({ username: null, password: null, phoneNumber: null, newPassword: null, repeatPassword: null });
    const dispatch = useDispatch();
    const history = useHistory();
    const cookies = new Cookies();
	const isDev = process.env.NODE_ENV === 'development';

    React.useEffect(() => {
        if (enterKeyPressed && activeTab) {
            handleLogin()
        }
    }, [enterKeyPressed])

    const loginFields = [{
        id: 'username',
        label: formatMessage({ id: 'username' }),
        value: loginDetails.username,
        type: 'text',
        icon: <Icon src={config.iconsPath + `login/username.svg`} />
    },
    {
        id: 'password',
        label: formatMessage({ id: 'password' }),
        value: loginDetails.password,
        type: 'password',
        icon: <Icon src={config.iconsPath + `login/password.svg`} />
    }]


    const form = (fields: any) =>
    (<div className="d-flex flex-column mb-1">
        {fields.map((item: any, index: number) => {
            return (
                <div className="width-18-rem">
                    {/* <label className="font-medium-1 pl-1 pr-1" htmlFor={item.id}>{item.label}</label> */}
                    <InputText
                        id={item.id}
                        onChange={(e) => {
                            if (e.target.value.trim().length === 0 || e.target.value == '') setLoginDetails({ ...loginDetails, [item.id]: null })
                            else setLoginDetails({ ...loginDetails, [item.id]: e.target.value })
                        }}
                        value={item.value || ""}
                        placeholder={item.label}
                        inputClassNames="pl-1-5 pr-1-5 input-placeholer-black"
                        classNames={inputsErrors[item.id] && inputsErrors[item.id].length > 0 ? 'mb-0' : 'mb-1'}
                        inputType={item.type}
                        customIcon={item.icon}
                        inputRef={item.inputRef || null}
                        noMarginRight
                        noMarginBottom
                        error={inputsErrors[item.id] && inputsErrors[item.id].length > 0 ? true : false}
                        errorLabel={item.id != 'username' ? inputsErrors[item.id] : null}
                    />
                </div>
            )
        })
        }
    </div>)



    const handleLogin = async () => {
        const { username, password } = loginDetails;
        let captchaToken: any;
        setInputsErrors({ ...inputsErrors, username: null, password: null })

        if (!username || !password) {
            let errorLabel = formatMessage({ id: 'Username and/or Password are invalid' });
            setInputsErrors({ ...inputsErrors, username: errorLabel, password: errorLabel })
        } else {
            if( recaptchaRef && recaptchaRef.current ) {
                await recaptchaRef.current.reset();
                captchaToken = !isDev ? await recaptchaRef.current.executeAsync() : true;
            }
        }

        if (username && password && captchaToken) {
            setIsLoading(true)
            try {
                let User: any = await LoginUser(username, password, networkValue && networkValue.value ? networkValue.value : null)

                if (User) {
                    const { passwordResetDate } = User;
                    let resetDate = moment(passwordResetDate, "YYYY-MM-DD")
                    let now = moment();
                    let diff = now.diff(resetDate, 'days');


                    store.dispatch(setAuth(User, cookies.get('token')));
                    setUser(User);
                    dispatch(AuthActions.setUser(User));

                    if (diff > 80) {
                        history.push('/login/reset');
                    }
                    else {
                        history.push('/');
                    }
                }
            } catch (error) {
                console.log(error)
                let errorLabel = formatMessage({ id: 'Username and/or Password are invalid' });
                setInputsErrors({ ...inputsErrors, username: errorLabel, password: errorLabel })
            } finally {
                setIsLoading(false)
            }
        }
    }

    return (
        <>
            <div className="d-flex flex-column justify-content-center">
                <div>
                    {isAdminLogin ? <NetworkSelect onChange={(network: any) => onChangeNetwork(network)} value={networkValue} /> : null}
                    {form(loginFields)}


                    <Button disabled={!isLoading ? false : true} onClick={() => handleLogin()}
                        className={`mt-05 round  height-2-5-rem d-flex justify-content-center align-items-center align-self-center width-18-rem`}
                        color='primary'>{formatMessage({ id: 'login' })}</Button>
                </div>
            </div>


        </>
    )
};

export default LoginPasswordTab;


