import * as React from 'react';
import { BlankPopUp, Icon } from '@src/components'
import { useIntl } from 'react-intl';
import { config } from '@src/config';
import "../LoginScreen.scss";

interface Props {
}

export const PasswordRulesPopUp: React.FunctionComponent<Props> = (props: Props) => {
    const direction = document.getElementsByTagName("html")[0].dir;
    const { formatMessage } = useIntl();

    return (
        <>
            <BlankPopUp
                noXbutton
                onOutsideClick={() => { }}
                bgBlueDark
                className={`password-rules width-19-rem height-18-rem text-white`}>
                <div className=' pr-2  m-auto-t-1-5 font-bold-400  '>

                    <Icon className="ml-1 position-absolute position-top-1-4-5 position-left-0-8-5" src={config.iconsPath + `login/info.svg`} />
                    <div className="pl-3">
                        <p>
                            <b>  {formatMessage({ id: 'at_least_eight_digits' })}.</b> <br />
                            <span >{formatMessage({ id: 'in_addition_it_must_contain_one' })}</span>
                            <span > {formatMessage({ id: 'of_all_the_following_character_types' })}</span>:
                        </p>

                        <ul>
                            <li>
                                {formatMessage({ id: 'capital_letters' })}
                                <span>({formatMessage({ id: 'capital_letters_examples' })}...)</span>
                            </li>
                            <li>
                                {formatMessage({ id: 'small_letters' })}
                                <span>({formatMessage({ id: 'small_letters_examples' })}...)</span>
                            </li>
                            <li>
                                {formatMessage({ id: 'digits' })}
                                <span>({formatMessage({ id: 'digits_examples' })}...)</span>
                            </li>
                            <li>
                                {formatMessage({ id: 'special_symbols' })}
                                <span>({formatMessage({ id: 'special_symbols_examples' })}...)</span>
                            </li>
                            <li>
                                {formatMessage({ id: 'cannotUseOldPassword' })}
                            </li>
                        </ul>
                    </div>
                </div>
            </BlankPopUp>
        </>
    )
};

export default PasswordRulesPopUp;