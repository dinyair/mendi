import axios from 'axios';
import { config } from '../../config';
import { getRequest, postRequest } from "@src/http";
import Cookies from 'universal-cookie';

const cookies = new Cookies();

export const getToken = () => {
	return cookies.get('token')
}

export const checkAuth = (user?: any, access_token?: string) => new Promise((resolve, reject) => {
	getRequest<{ user?: any, access_token?: string }>('auth/out-refresh', { withCredentials: true })
		.then((res: any) => {
			resolve(res)
		})
})

export const LoginUser = (username: string, password: string, netw?: string | null) => new Promise((resolve, reject) => {
	postRequest(netw ? 'auth/admin-login' : 'auth/login', { username, password, netw })
		.then((res: any) => {
			console.log(res)
			const { user, access_token, err } = res
			const obj = { ...user }
			delete obj.permissions
			let expires = new Date((Date.now() + ((86400 * 1000)) ))
			cookies.set('token', access_token, { path: '/', expires, secure: true });
			cookies.set('user', JSON.stringify(obj), { path: '/', expires, secure: true });

			resolve(user)
		}).catch(err => reject(err))
})

export const ReLoginAdmin = (netw: string ) => new Promise((resolve, reject) => {
	postRequest('admin-relogin', {  netw },{ headers: { Authorization: `Bearer ${getToken()}` }})
		.then((res: any) => {
			const { user, access_token, err } = res
			const obj = { ...user }
			delete obj.permissions
			let expires = new Date((Date.now() + ((86400 * 1000)) ))
			cookies.set('token', access_token, { path: '/', expires, secure: true });
			cookies.set('user', JSON.stringify(obj), { path: '/', expires, secure: true });

			resolve(obj)
		}).catch(err => reject(err))
})


export const RegisterUser = (first_name: string, last_name: string, email: string, password: string, company_id: number, country_id: number) => new Promise((resolve, reject) => {
	axios.post(`${config.ipServer}/auth/register`, {
		first_name,
		last_name,
		email,
		password,
		company_id,
		country_id,
	})
		.then(res => {
			const { user, toekn, err } = res.data
			if (err) reject(err)
			else {
				const obj = { ...user }
				delete obj.permissions
				// localStorage.setItem('user', JSON.stringify(user));
				let expires = new Date((Date.now() + ((86400 * 1000)) ))
				cookies.set('user', JSON.stringify(obj), { path: '/', expires, secure: true });
				resolve(user)
			}
		}).catch(err => reject('Error'))
})


export const checkPhoneNumber = (phoneNumber?: string) => new Promise((resolve, reject) => {
	getRequest<{ valid: boolean }>('sms/' + phoneNumber)
		.then((res: any) => {
			resolve(res)
		})
})



export const smsLogin = (phoneNumber: string, smsCode: string, netw: string|null) => new Promise((resolve, reject) => {
	postRequest(`${config.ipServer}/auth/sms-login`, {
		phoneNumber,
		smsCode,
		netw
	})
		.then((res: any) => {
			const { user, access_token, error } = res
			if (error) {
				console.log(error)
				resolve({error})
			} else {
				const obj = { ...user }
				delete obj.permissions
				let expires = new Date((Date.now() + ((86400 * 1000)) ))
				cookies.set('token', access_token, { path: '/', expires, secure: true });
				cookies.set('user', JSON.stringify(obj), { path: '/', expires, secure: true });
				resolve(user)
			}
		}).catch(err => reject(err))
})


