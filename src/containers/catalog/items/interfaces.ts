import { SuppliersActionTypes } from './enums';
import { Attachment } from '@src/components/attachments/interfaces';

export interface SuppliersAction {
	type: SuppliersActionTypes;
	payload: any;
}

export interface SupplierGroup {
	supplier_title: any;
	suppliers: {
		canban: any[];
		default: Supplier[];
	};
}
export interface Supplier {
	readonly pos: number;
	readonly id: number;
	readonly company_id: string;
	readonly supplier_name: string,
	readonly color: string;
	readonly type: string;
	readonly contact_name: string;
	readonly supplier_job_title: string;
	readonly supplier_job_title_id: number;
	readonly supplier_unit_type: string;
	readonly supplier_unit_type_id: number;
	readonly supplier_unit_cost: number;
	readonly 'percentage1': number;
	readonly 'percentage2': number;
	readonly 'percentage3': number;
	readonly start_date: string | Date;
	readonly end_date: string | Date;
	readonly phone: string;
	readonly email: string;
	readonly status: string;
	readonly comments: string,
	readonly attachments: Attachment[]
}

export interface ColumnType {
	columnId: string;
	label: string;
}

interface Page {
	page: number;
	pageSize: number;
}

export interface ColumnValues {
	[columnId: string]: string | number | boolean;
}

export interface DataRow {
	entityId: string;
	columnValues: ColumnValues;
}

interface DataTableResult {
	page: Page;
	dataRows: DataRow[];
}

export interface DataSet {
	totalCount: number;
	data: DataTableResult;
	columns: Array<ColumnType>;
}
