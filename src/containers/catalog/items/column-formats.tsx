// import * as React from 'react';
// import {Clock, Edit2, Trash2} from 'react-feather';
// import {EditDrawer} from '@src/components/edit-drawer';
// import {FilterDrawer} from '@src/components/filter-drawer';
// import {ColumnValues} from './interfaces';
// import {GenericDrawerData} from '@src/components/edit-drawer/interfaces';
// import {dataDrawer, filterDataDrawer} from './mockData';


// interface Props {
// 	columnType: string;
// 	row: ColumnValues;
// 	updateRow: (data: GenericDrawerData) => void;
// 	children: React.ReactNode;
// }

// export const ColumnFormats: React.FC<Props> = props => {
// 	const {columnType, row, children, updateRow, saveFilterChanges} = props;
// 	const [shown, setStateModal] = React.useState(false);
// 	const [shownFilter, setStateFilterModal] = React.useState(false);

// 	const toggleModal = () => {
// 		setStateModal(prevValue => !prevValue);
// 	}

// 	const toggleFilterModal = () => {
// 		setStateFilterModal(prevValue => !prevValue);
// 	}

// 	const onEditRow = () => {
// 		toggleModal();
// 	}

// 	const onFilterRow = () => {
// 		toggleFilterModal();
// 	}

// 	if (columnType === 'actions') {
// 		const prepareData = {...row};
// 		delete prepareData.actions;

// 		return (
// 			<div className='action-buttons'>
// 				<button onClick={onEditRow} className='action-buttons__edit'>
// 					<Edit2 size={16} />
// 				</button>
// 				<button className='action-buttons__delete'>
// 					<Trash2 size={16} color='red' />
// 				</button>
// 				<button onClick={onFilterRow} className='action-buttons__log'>
// 					<Clock size={16} color='violet' />
// 				</button>
// 				<EditDrawer {...{
// 					caption: '750 vanilla pillows',
// 					shown,
// 					toggleModal,
// 					data: dataDrawer,
// 					saveChanges: updateRow
// 				}}/>
// 				<FilterDrawer {...{
// 					caption: 'Filter a table',
// 					shown: shownFilter,
// 					toggleModal: toggleFilterModal,
// 					data: filterDataDrawer,
// 					saveChanges: saveFilterChanges
// 				}}/>
// 			</div>
// 		)
// 	}

// 	return (<>{children}</>);
// }
