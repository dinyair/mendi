import * as React from 'react';
import * as moment from 'moment';
import { Modal, ModalBody, ModalFooter, Button } from 'reactstrap';
import { ModalHeader } from '@src/components/modal';
import { useSelector, useDispatch } from 'react-redux';
import ExportExcel from '@src/components/ExportExcel/ExportExcel';
import { Spinner } from 'reactstrap';
import { AgGrid } from '@components';
import { RootStore } from '@src/store';
import { toMap } from '@src/helpers/helpers';
import { useIntl } from 'react-intl';
import { Icon } from '@components';
import { config } from '@src/config';
import { editDataDrawer, filterDataDrawer, newProductDrawerData } from './drawerData';
import { GenericDrawer } from '@src/components/generic-drawer';
import InputText from '@src/components/input-text';
import EditHistoryPopup from './EditHistoryPopup';
import { fetchAndUpdateStore } from '@src/helpers/helpers';
import { editItemRequest, addItemRequest, deleteItemRequest, getUnitsSizes } from './initial-state';
import SweetAlert from 'react-bootstrap-sweetalert';
import classnames from 'classnames';

import './catalog.scss';
import { isArray } from 'util';

export const Catalog: React.FunctionComponent = () => {
	const { formatMessage } = useIntl();
	document.title = formatMessage({ id: 'Items' });

	const user = useSelector((state: RootStore) => state.user);
	const userPermissions = user ? user.permissions : null;
	let URL = window.location.pathname;
	const userEditFlag = userPermissions && userPermissions[URL] && userPermissions[URL].edit_flag === 1;

	const dispatch = useDispatch();
	const catalogFull = useSelector((state: RootStore) => state._catalogFullData);
	const catalogFullNoArchive = useSelector((state: RootStore) => state._catalogFullDataNoArchive);
	const regCatalog = useSelector((state: RootStore) => state._catalogUpdated);
	const subSuppliers = useSelector((state: RootStore) => state._subSuppliers);
	const subGroup = useSelector((state: RootStore) => state._subGroups);
	// const degems = useSelector((state: RootStore) => state._models)
	const division = useSelector((state: RootStore) => state._divisions);
	const groups = useSelector((state: RootStore) => state._groups);
	const suppliers = useSelector((state: RootStore) => state._suppliers);
	const series = useSelector((state: RootStore) => state._series);
	const models = useSelector((state: RootStore) => state._models);
	const segments = useSelector((state: RootStore) => state._segments);

	const [advancedSearch, setAdvancedSearch] = React.useState<boolean>(false);
	const [tempData, setTempData] = React.useState<any>();
	const [shownFilter, setStateFilterModal] = React.useState(false);
	const [shownEdit, setStateEditModal] = React.useState(false);
	const [shownAdd, setStateAddModal] = React.useState(false);
	const [isAreYouSureModalOpen, setAreYouSureModal] = React.useState(false);
	const [itemToDeleteClicked, setItemToDeleteClicked] = React.useState<any>();
	const [editHistory, setEditHistory] = React.useState<any>();
	const [sweetAlert, setSweetAlert] = React.useState<any>();
	const [isLoading, setIsLoading] = React.useState<boolean>(false);
	const [chosenGroup, setChosenGroup] = React.useState<any>();
	const freeSearchInputRef = React.useRef<any>();
	const [rowInEdit, setRowInEdit] = React.useState<any>([]);
	const [isDataReady, setIsDataReady] = React.useState<boolean>(false);
	const [originalRows, setOriginalRows] = React.useState<any[]>([]);
	const [unitSizes, setUnitSizes] = React.useState<any[]>([]);
	const [rows, setRows] = React.useState<any>([]);
	const [fields, setFields] = React.useState<any>([
		{
			text: 'BarCode',
			type: 'number',
			width: window.innerWidth * 0.05,
			minWidth: 70
		},
		{ text: 'itemName', type: 'string', width: window.innerWidth * 0.05, minWidth: 70 },
		{
			text: 'supplierName',
			type: 'string',
			width: window.innerWidth * 0.05,
			minWidth: 70,
			filter: 'agMultiColumnFilter',
			filterParams: { filters: [{ filter: 'agSetColumnFilter', filterParams: { suppressMiniFilter: true } }] }
		},
		{ text: 'divisionName', type: 'string', width: window.innerWidth * 0.05, minWidth: 50 },
		{
			text: 'groupName',
			type: 'string',
			width: window.innerWidth * 0.05,
			minWidth: 50,
			filter: 'agMultiColumnFilter',
			filterParams: { filters: [{ filter: 'agSetColumnFilter', filterParams: { suppressMiniFilter: true } }] }
		},
		{ text: 'subGroupName', type: 'string', width: window.innerWidth * 0.05, minWidth: 50 },
		{ text: 'seriesName', type: 'string', width: window.innerWidth * 0.05, minWidth: 50 },
		{ text: 'inventoryType', type: 'string', width: window.innerWidth * 0.03, minWidth: 25 },
		{
			text: 'actions',
			type: 'string',
			centerHeader: true,
			width: window.innerWidth * 0.001,
			minWidth: 1,
			noFilter: true,
			cellRenderer: 'IconRender',
			renderIcons: [
				{ actions: true },
				{
					type: 'edit',
					title: 'Edit',
					onClick: (data: any) => handleActionClick('edit', data),
					icon: 'edit.svg'
				},
				userEditFlag
					? {
							type: 'delete',
							title: 'Delete',
							onClick: (data: any) => handleActionClick('delete', data),
							icon: 'trash.svg'
					  }
					: null,
				{
					type: 'Edithistory',
					title: 'Edithistory',
					onClick: (data: any) => handleActionClick('editHistory', data),
					icon: 'iconClock.svg'
				}
			]
		}
	]);

	React.useEffect(() => {
		fetchAndUpdateStore(dispatch, [
			{ state: subGroup, type: 'subGroups' },
			{ state: division, type: 'divisions' },
			{ state: suppliers, type: 'suppliers' },
			{ state: subSuppliers, type: 'subSuppliers' },
			{ state: groups, type: 'groups' },
			{ state: series, type: 'series' },
			{ state: models, type: 'models' },
			{ state: segments, type: 'segments' },
			{ state: catalogFullNoArchive, type: 'catalogFullDataNoArchive' },
			{ state: catalogFull, type: 'catalogFullData' },
			{ state: regCatalog, type: 'catalog' }
		]);
		setTimeout(() => {
			catalogRowData(catalogFullNoArchive);
		}, 100);
	}, []);
	React.useEffect(() => {
		setTimeout(() => {
			catalogRowData(catalogFullNoArchive);
		}, 100);
	}, [catalogFullNoArchive]);

	React.useEffect(() => {
		setTimeout(() => {
			catalogRowData(catalogFullNoArchive);
		}, 100);
	}, []);

	const filterCancel = () => {
		setIsLoading(true);
		setTimeout(() => {
			catalogRowData(catalogFullNoArchive);
		}, 1);
	};
	const catalogRowData = async (arr: any, isAdvanced?: boolean) => {
		setIsDataReady(false);
		let unitS = await getUnitsSizes();
		setUnitSizes(unitS);
		if (isAdvanced) {
			setAdvancedSearch(true);
			if (arr && !arr.length) {
				setRows([]);
				setTimeout(() => {
					// setIsDataReady(true)
				}, 50);
			}
		} else {
			setAdvancedSearch(false);
			setTempData({});
		}
		if (arr.length) {
			let catalogWithData = createRowsForCatalog(arr);
			setIsLoading(false);
			setOriginalRows(catalogWithData);
			setRows(catalogWithData);
			setIsDataReady(true);
		}
	};
	const createRowsForCatalog = (arr: any) => {
		if(arr && arr.length){
		let _suppliers: any = {};
		let _subSuppliers: any = {};
		let _subGroup: any = {};
		let _groups: any = {};
		let _division: any = {};
		let _series: any = {};
		let _models: any = {};
		let _segments: any = {};

		for (var i = 0; i < division.length; i++) {
			_division[division[i].Id] = division[i].Name;
		}
		for (var i = 0; i < series.length; i++) {
			_series[series[i].Id] = series[i].Name;
		}
		for (var i = 0; i < subGroup.length; i++) {
			_subGroup[subGroup[i].Id] = subGroup[i].Name;
		}
		for (var i = 0; i < suppliers.length; i++) {
			_suppliers[suppliers[i].Id] = suppliers[i].Name;
		}
		for (var i = 0; i < subSuppliers.length; i++) {
			if (_subSuppliers[subSuppliers[i].SubsapakId] == undefined) {
				_subSuppliers[subSuppliers[i].SubsapakId] = 1;
				_suppliers[subSuppliers[i].SubsapakId] = subSuppliers[i].SubsapakName;
			}
		}
		for (var i = 0; i < groups.length; i++) {
			_groups[groups[i].Id] = groups[i].Name;
		}
		for (var i = 0; i < models.length; i++) {
			_models[models[i].Id] = models[i].Name;
		}
		for (var i = 0; i < segments.length; i++) {
			_segments[segments[i].Id] = segments[i].Name;
		}

		const catalogWithData = arr.map((item: any) => {
			const divisionName = _division[item.ClassesId];
			const supplierName = _suppliers[item.SapakId];
			const groupName = _groups[item.GroupId];
			const subGroupName = _subGroup[item.SubGroupId];
			const seriesName = _series[item.DegemId];
			const segmentName = _segments[item.SegmentId];
			const modelName = _models[item.ModelId];
			let BarCode = Number(item.BarCode);
			let inventoryType: string = '';
			const itemName = item.Name;
			switch (item.statusCalMlay) {
				case 0:
					inventoryType = formatMessage({ id: 'Regular' });
					break;
				case 1:
					inventoryType = formatMessage({ id: 'Daily' });
					break;
				case 2:
					inventoryType = formatMessage({ id: 'Weekly' });
					break;
				case 3:
					inventoryType = formatMessage({ id: 'Short valid time' });
					break;
				case 4:
					inventoryType = formatMessage({ id: 'Inner' });
					break;
			}
			return {
				BarCode,
				segmentName,
				modelName,
				itemName,
				divisionName,
				supplierName,
				groupName,
				subGroupName,
				seriesName,
				inventoryType,
				resetOfData: { ...item }
			};
		});
		return catalogWithData;
	}else return []
	};
	React.useEffect(() => {
		if (
			freeSearchInputRef &&
			freeSearchInputRef.current &&
			freeSearchInputRef.current.value &&
			freeSearchInputRef.current.value.length
		)
			onFilterTextBoxChanged();
	}, [originalRows]);

	const getStatusId = (type: string) => {
		switch (type) {
			case formatMessage({ id: 'Regular' }):
				return 0;
			case formatMessage({ id: 'Daily' }):
				return 1;
			case formatMessage({ id: 'Weekly' }):
				return 2;
			case formatMessage({ id: 'Short valid time' }):
				return 3;
			case formatMessage({ id: 'Inner' }):
				return 4;
			default:
				return 0;
		}
	};
	const getUnitId = (type: string) => {
		switch (type) {
			case formatMessage({ id: '-cm' }):
				return 1;
			case formatMessage({ id: '-meter' }):
				return 2;
			case formatMessage({ id: '-mililiter' }):
				return 3;
			case formatMessage({ id: '-liter' }):
				return 4;
			case formatMessage({ id: '-gram' }):
				return 5;
			case formatMessage({ id: '-kg' }):
				return 6;
			case formatMessage({ id: '-unit' }):
				return 7;
			default:
				return null;
		}
	};

	const areYouSureModal = [
		{
			headerClasses: 'margin-auto',
			isOpen: isAreYouSureModalOpen,
			toggle: () => setAreYouSureModal(false),
			header: <span className="font-weight-bold">{formatMessage({ id: 'warning' })}</span>,
			body: (
				<>
					<p> {formatMessage({ id: 'deletedForGoodItem' })} </p> <p>{formatMessage({ id: 'areYouSure' })}</p>
				</>
			),
			buttons: [
				{
					color: 'primary',
					onClick: () => {
						deleteItem();
						setAreYouSureModal(false);
					},
					label: 'yesIamSure',
					outline: true
				},
				{ color: 'primary', onClick: () => setAreYouSureModal(false), label: 'noBack' }
			]
		}
	];
	const returnModal = (modal: any) => {
		return (
			<Modal
				isOpen={modal.isOpen}
				toggle={modal.toggle}
				className="modal-dialog-centered modal-md text-center width-10-per min-width-25-rem"
			>
				<ModalHeader onClose={modal.toggle} classNames={modal.headerClasses}>
					<h5 className="modal-title">{modal.header}</h5>
				</ModalHeader>
				<ModalBody>{modal.body}</ModalBody>
				<ModalFooter>
					<div className="m-auto">
						{modal.buttons.map((ele: any, index: number) => (
							<Button
								outline={ele.outline ? true : false}
								className="round ml-05 mr-05"
								key={index}
								color={ele.color}
								onClick={ele.onClick}
								disabled={ele.disabled}
							>
								{formatMessage({ id: ele.label })}
							</Button>
						))}
					</div>
				</ModalFooter>
			</Modal>
		);
	};

	const handleActionClick = (type: any, data: any) => {
		switch (type) {
			case 'edit':
				editItem(data);
				break;
			case 'delete':
				setItemToDeleteClicked(data);
				setAreYouSureModal(true);
				break;
			case 'editHistory':
				setEditHistory(data);
				break;
		}
	};
	const deleteItem = async () => {
		if (itemToDeleteClicked && itemToDeleteClicked.resetOfData)
			await deleteItemRequest(itemToDeleteClicked.resetOfData.Id);
		setRows(rows.filter((g: any) => g.BarCode != itemToDeleteClicked.BarCode));
		setSweetAlert('deletedSucc');
		setTimeout(() => {
			setSweetAlert(false);
		}, 1000);
		fetchAndResetCatalog();
	};
	const editItem = (data: any) => {
		setRowInEdit(data);
		setTimeout(() => {
			toggleEditModal();
		}, 1);
	};

	const toggleEditModal = () => {
		setStateEditModal(!shownEdit);
	};
	const toggleAddModal = () => {
		setStateAddModal(!shownAdd);
	};

	const includingOnlyWithout = (type: any) => {
		switch (type) {
			case formatMessage({ id: 'Including' }): {
				return -1;
			}
			case formatMessage({ id: 'Yes' }): {
				return 1;
			}
			case formatMessage({ id: 'No' }): {
				return 0;
			}
			default:
				return -1;
		}
	};

	const toggleFilterModal = () => {
		setStateFilterModal(!shownFilter);
	};

	const onFilter = () => {
		toggleFilterModal();
	};
	const onFilterTextBoxChanged = () => {
		setIsDataReady(false);

		let filteredRowData = originalRows.filter((row: any) => {
			let isFound: boolean = false;
			let freeSearchFields = ['BarCode', 'itemName'];
			freeSearchFields.forEach((field: any) => {
				let searchField = String(row[field]);
				if (searchField && searchField.includes(String(freeSearchInputRef.current.value))) isFound = true;
			});
			return isFound;
		});
		setRows(filteredRowData);
		setTimeout(() => {
			setIsDataReady(true);
		}, 100);
	};

	const updateSelectedDefault = (newData: any, type: string) => {
		switch (type) {
			case 'group':
				setChosenGroup(newData);
				break;
		}
	};
	const saveFilterChanges = (newData: any) => {
		setIsLoading(true);

		let newRows = catalogFull.filter((item: any, index: number) => {
			let totalChecked = 0;
			let totalTrue = 0;
			let layouts = newData.layouts;
			layouts.forEach((layout: any) => {
				layout.formGroup.forEach((fgroup: any) => {
					fgroup.fields.forEach((field: any) => {
						let fieldValue = field.value ? field.value : field.defaultValue;
						switch (field.name) {
							case 'packing-factor': {
								if (fieldValue && fieldValue.length) {
									totalChecked++;
									if (fieldValue == item.SizeAriza) totalTrue++;
								}
								break;
							}
							case 'shaquille':
								let valueShaq: any =
									fieldValue && fieldValue.value === formatMessage({ id: 'Yes' })
										? 1
										: fieldValue && fieldValue.value == formatMessage({ id: 'No' })
										? 0
										: 2;
								totalChecked++;

								if (valueShaq == 1 && item.Shakele == 1) totalTrue++;
								else if (valueShaq == 0 && item.Shakele == 0) totalTrue++;
								else if (valueShaq == 2) totalTrue++;
								break;
							case 'supplier': {
								if (typeof fieldValue != 'string' && fieldValue) {
									totalChecked++;
									fieldValue.forEach((sup: any) => {
										if (sup.value == item.SapakId) totalTrue++;
									});
								}
								break;
							}
							case 'division':
								if (typeof fieldValue != 'string' && fieldValue) {
									totalChecked++;
									fieldValue.forEach((sup: any) => {
										if (sup.value == item.ClassesId) totalTrue++;
									});
								}
								break;
							case 'group':
								if (typeof fieldValue != 'string' && fieldValue) {
									totalChecked++;
									fieldValue.forEach((sup: any) => {
										if (sup.value == item.GroupId) totalTrue++;
									});
								}
								break;
							case 'subgroup':
								if (typeof fieldValue != 'string' && fieldValue) {
									totalChecked++;
									fieldValue.forEach((sup: any) => {
										if (sup.value == item.SubGroupId) totalTrue++;
									});
								}
								break;
							case 'series':
								if (typeof fieldValue != 'string' && fieldValue) {
									totalChecked++;
									fieldValue.forEach((sup: any) => {
										if (sup.value == item.DegemId) totalTrue++;
									});
								}
								break;
							case 'archives':
								totalChecked++;
								let value: any =
									fieldValue && fieldValue.value === formatMessage({ id: 'Yes' })
										? 1
										: fieldValue && fieldValue.value == formatMessage({ id: 'No' })
										? 0
										: 2;
								if (value === 2) totalTrue++;
								else if (item.Archives == value) totalTrue++;
								break;
							case 'sale_block':
								totalChecked++;
								let value1: any =
									fieldValue && fieldValue.value === formatMessage({ id: 'Yes' })
										? 0
										: fieldValue && fieldValue.value == formatMessage({ id: 'No' })
										? 1
										: 2;
								if (value1 === 2) totalTrue++;
								else if (value1 == 0 && item.Bdate_sale != null) {
									totalTrue++;
								} else if (value1 == 1 && item.Bdate_sale == null) {
									totalTrue++;
								}
								break;
							case 'purchase-block':
								totalChecked++;
								let value2: any =
									fieldValue && fieldValue.value === formatMessage({ id: 'Yes' })
										? 0
										: fieldValue && fieldValue.value == formatMessage({ id: 'No' })
										? 1
										: 2;
								if (value2 === 2) totalTrue++;
								else if (value2 == 0 && item.Bdate_buy != null) {
									totalTrue++;
								} else if (value2 == 1 && item.Bdate_buy == null) {
									totalTrue++;
								}
								break;
							case 'atomic':
								totalChecked++;
								let value3: any =
									fieldValue && fieldValue.value === formatMessage({ id: 'Yes' })
										? 0
										: fieldValue && fieldValue.value == formatMessage({ id: 'No' })
										? 1
										: 2;
								if (value3 === 2) totalTrue++;
								else if (value3 == 0 && item.Arc_date_in != null) {
									totalTrue++;
								} else if (value3 == 1 && item.Arc_date_in == null) {
									totalTrue++;
								}
								break;
							case 'inventory-type':
								if (typeof fieldValue != 'string' && fieldValue) {
									totalChecked++;
									fieldValue.forEach((itype: any) => {
										if (getStatusId(itype.value) == item.statusCalMlay) totalTrue++;
									});
								}
								break;
							case 'service-product':
								let valueService: any =
									fieldValue && fieldValue.value === formatMessage({ id: 'Yes' })
										? 1
										: fieldValue && fieldValue.value == formatMessage({ id: 'No' })
										? 0
										: 2;
								totalChecked++;
								if (valueService == 1 && item.ServiceItem == 1) totalTrue++;
								else if (valueService == 0 && item.ServiceItem == 0) totalTrue++;
								else if (valueService == 2) totalTrue++;
								break;
							case 'hamez':
								let valueHamez: any =
									fieldValue && fieldValue.value === formatMessage({ id: 'Yes' })
										? 1
										: fieldValue && fieldValue.value == formatMessage({ id: 'No' })
										? 0
										: 2;
								totalChecked++;
								if (valueHamez == 1 && item.flagLeaven == 1) totalTrue++;
								else if (valueHamez == 0 && item.flagLeaven == 0) totalTrue++;
								else if (valueHamez == 2) totalTrue++;
								break;
							// case 'box':
							// 	let val3 = fieldValue ? 1 : 0
							// 	totalChecked++
							// 	if (val3 == item.Ariza) totalTrue++
							// 	break;
							case 'brand':
								if (typeof fieldValue != 'string' && fieldValue) {
									totalChecked++;
									fieldValue.forEach((brand: any) => {
										if (brand.value == item.ModelId) totalTrue++;
									});
								}
								break;
							case 'segment':
								if (typeof fieldValue != 'string' && fieldValue) {
									totalChecked++;
									fieldValue.forEach((brand: any) => {
										if (brand.value == item.SegmentId) totalTrue++;
									});
								}
								break;
							case 'content':
								if (fieldValue && fieldValue.length) {
									totalChecked++;
									if (Number(fieldValue) == Number(item.techula)) totalTrue++;
								}
								break;
						}
					});
				});
			});
			return totalChecked === totalTrue;
		});
		newRows.length ? catalogRowData(newRows, true) : catalogRowData([], true);
		setTimeout(() => {
			setIsLoading(false);
		}, 100);
	};

	const saveEditItem = async (newData: any, caption: any) => {
		let rec1: any = {};
		for (let [key, value] of Object.entries(rowInEdit.resetOfData)) {
			if (key != 'InPlanogram' && key != 'maharaz') rec1[key] = value;
		}
		rec1.Name = caption.caption;
		newData.tabs.forEach((item: any) => {
			if (item.formGroup) {
				item.formGroup.forEach((group: any) => {
					if (group.fields) {
						group.fields.forEach((field: any) => {
							switch (field.name) {
								case 'barcode':
									rec1.BarCode = String(field.value);
									break;
								case 'shaquille':
									rec1.Shakele = field.value ? 1 : 0;
									break;
								// case 'packing-factor':
								// 	rec1.SizeAriza = field.value
								// 	break;
								case 'supplier':
									rec1.SapakId =
										typeof field.value != 'string' && field.value
											? field.value.value
											: (field.defaultValue &&
													field.defaultValue[0] &&
													typeof field.value === 'string') ||
											  (field.value && field.defaultValue && field.defaultValue[0])
											? field.defaultValue[0].value
											: 0;
									break;
								case 'division':
									rec1.ClassesId =
										typeof field.value != 'string' && field.value
											? field.value.value
											: (field.defaultValue &&
													field.defaultValue[0] &&
													typeof field.value === 'string') ||
											  (field.value && field.defaultValue && field.defaultValue[0])
											? field.defaultValue[0].value
											: 0;
									break;
								case 'group':
									rec1.GroupId =
										typeof field.value != 'string' && field.value
											? field.value.value
											: (field.defaultValue &&
													field.defaultValue[0] &&
													typeof field.value === 'string') ||
											  (field.value && field.defaultValue && field.defaultValue[0])
											? field.defaultValue[0].value
											: 0;
									break;
								case 'subgroup':
									rec1.SubGroupId =
										typeof field.value != 'string' && field.value
											? field.value.value
											: (field.defaultValue &&
													field.defaultValue[0] &&
													typeof field.value === 'string') ||
											  (field.value && field.defaultValue && field.defaultValue[0])
											? field.defaultValue[0].value
											: 0;
									break;
								case 'series':
									rec1.DegemId =
										typeof field.value != 'string' && field.value
											? field.value.value
											: (field.defaultValue &&
													field.defaultValue[0] &&
													typeof field.value === 'string') ||
											  (field.value && field.defaultValue && field.defaultValue[0])
											? field.defaultValue[0].value
											: 0;
									break;
								case 'date-of-establishment':
									rec1.Create_Date =
									field.value											? moment(field.value).format('YYYY-MM-DD')
											: null;
									break;
								case 'active':
									rec1.Archives =
										field.value && field.value.value === formatMessage({ id: 'yes' })
											? 0
											: field.value && field.value.value === formatMessage({ id: 'no' })
											? 1
											: field.defaultValue && field.defaultValue[0]
											? field.defaultValue[0].value === formatMessage({ id: 'yes' })
												? 0
												: field.defaultValue[0].value === formatMessage({ id: 'no' })
											: 1;
									break;
								case 'blocked-sale-date':
									rec1.Bdate_sale =
									field.value											? moment(field.value).format('YYYY-MM-DD')
											: null;
									break;
									break;
								case 'purchase-blocked-date':
									rec1.Bdate_buy =
									field.value											? moment(field.value).format('YYYY-MM-DD')
											: null;
									break;
								case 'inventory-type':
									rec1.statusCalMlay =
										typeof field.value != 'string' && field.value
											? getStatusId(field.value.value)
											: field.defaultValue && field.defaultValue[0]
											? field.defaultValue[0].value
											: null;
									break;
								case 'service-product':
									rec1.ServiceItem = field.value ? 1 : 0;
									break;
								case 'depreciation':
									rec1.p_loss = field.value;
									break;
								case 'leaven':
									rec1.flagLeaven = field.value ? 1 : 0;
									break;
								case 'units-in-carton':
									rec1.Ariza = field.value ? field.value : 0;
									break;
								case 'box':
									rec1.maharaz = field.value ? true : false;
									break;
								case 'depth':
									if (field.value) {
										rec1.length = field.value;
									}
									if (
										field.selectValue &&
										field.selectValue.length &&
										Array.isArray(field.selectValue)
									) {
										rec1.lengthSize = Number(field.selectValue[0].value);
									}
									break;
								case 'width':
									if (field.value) {
										rec1.width = field.value;
									}
									if (
										field.selectValue &&
										field.selectValue.length &&
										Array.isArray(field.selectValue)
									) {
										rec1.widthSize = Number(field.selectValue[0].value);
									}
									break;
								case 'height':
									if (field.value) {
										rec1.height = field.value;
									}
									if (
										field.selectValue &&
										field.selectValue.length &&
										Array.isArray(field.selectValue)
									) {
										rec1.heightSize = Number(field.selectValue[0].value);
									}
									break;
								case 'weight':
									if (field.value) {
										rec1.weightGross = field.value;
									}
									if (
										field.selectValue &&
										field.selectValue.length &&
										Array.isArray(field.selectValue)
									) {
										rec1.weightGrossSize = Number(field.selectValue[0].value);
									}
									break;
								case 'content':
									if (field.value) {
										rec1.techula = field.value;
									}
									if (
										field.selectValue &&
										field.selectValue.length &&
										Array.isArray(field.selectValue)
									) {
										rec1.techulaSize = Number(field.selectValue[0].value);
									}
									break;
								case 'segment':
									rec1.SegmentId =
										typeof field.value != 'string' && field.value
											? field.value.value
											: (field.defaultValue &&
													field.defaultValue[0] &&
													typeof field.value === 'string') ||
											  (field.value && field.defaultValue && field.defaultValue[0])
											? field.defaultValue[0].value
											: 0;
									break;
								case 'brand':
									rec1.ModelId =
										typeof field.value != 'string' && field.value
											? field.value.value
											: (field.defaultValue &&
													field.defaultValue[0] &&
													typeof field.value === 'string') ||
											  (field.value && field.defaultValue && field.defaultValue[0])
											? field.defaultValue[0].value
											: 0;
									break;
							}
						});
					}
				});
			}
		});
		await editItemRequest(rec1);
		setSweetAlert('editSucc');
		setTimeout(() => {
			setSweetAlert(false);
		}, 1500);
		let newOriginalRows = originalRows.filter((i: any) => {
			return i.BarCode != rowInEdit.BarCode;
		});
		let newRows = rows.filter((i: any) => {
			return i.BarCode != rowInEdit.BarCode;
		});
		let newRow = createRowsForCatalog([rec1]);
		setRows([...newRow, ...newRows]);
		setOriginalRows([...newRow, ...newOriginalRows]);
		setRowInEdit(null);
	};

	function processText(inputText: any) {
		var output: any = [];
		var json = inputText.split(' ');
		json.forEach(function (item: any) {
			output.push(item.replace(/\'/g, '').split(/(\d+)/).filter(Boolean));
		});
		let newOutput = [];

		if (output[0].includes('.')) {
			newOutput.push(`${output[0][0]}${output[0][1]}${output[0][2]}`);
			newOutput.push(output[0][3]);
			output = [];
			output.push(newOutput);
		}
		return output;
	}

	const saveAddItem = async (newData: any, caption: any) => {
		let rec1: any = {};
		rec1.Name = caption.caption;
		newData.tabs.forEach((item: any) => {
			if (item.formGroup) {
				item.formGroup.forEach((group: any) => {
					if (group.fields) {
						group.fields.forEach((field: any) => {
							switch (field.name) {
								case 'barcode':
									rec1.CatalogId = String(field.value);
									rec1.BarCode = String(field.value);
									break;
								case 'shaquille':
									rec1.Shakele = field.value ? 1 : 0;
									break;
								case 'packing-factor':
									if (field.value && field.value.length) rec1.SizeAriza = field.value;
									break;
								case 'supplier':
									if (typeof field.value != 'string' && field.value) rec1.SapakId = field.value.value;
									break;
								case 'division':
									if (typeof field.value != 'string' && field.value)
										rec1.ClassesId = field.value.value;
									break;
								case 'group':
									if (typeof field.value != 'string' && field.value) rec1.GroupId = field.value.value;
									break;
								case 'subgroup':
									if (typeof field.value != 'string' && field.value)
										rec1.SubGroupId = field.value.value;
									break;
								case 'series':
									if (typeof field.value != 'string' && field.value) rec1.DegemId = field.value.value;
									break;
								case 'date-of-establishment':
									if (field.value)
										rec1.Create_Date = moment(field.value).format('YYYY-MM-DD');
									break;
								case 'active':
									if (field.value)
										rec1.Archives = field.value.value === formatMessage({ id: 'yes' }) ? 0 : 1;
									break;
								case 'blocked-sale-date':
									if (field.value)
										rec1.Bdate_sale = moment(field.value).format('YYYY-MM-DD');

									break;
								case 'purchase-blocked-date':
									if (field.value)
										rec1.Bdate_buy = moment(field.value).format('YYYY-MM-DD');
									break;
								case 'inventory-type':
									if (typeof field.value != 'string' && field.value)
										rec1.statusCalMlay = getStatusId(field.value.value);
									break;
								case 'service-product':
									rec1.ServiceItem = field.value ? 1 : 0;
									break;
								case 'depreciation':
									if (field.value) rec1.p_loss = field.value;
									break;
								case 'leaven':
									rec1.flagLeaven = field.value ? 1 : 0;
									break;
								case 'units-in-carton':
									rec1.Ariza = field.value ? field.value : null;
									break;
								case 'box':
									rec1.maharaz = field.value ? true : false;
									break;
								case 'depth':
									if (field.value) {
										rec1.length = field.value;
									}
									if (
										field.selectValue &&
										field.selectValue.length &&
										Array.isArray(field.selectValue)
									) {
										rec1.lengthSize = Number(field.selectValue[0].value);
									}
									break;
								case 'width':
									if (field.value) {
										rec1.width = field.value;
									}
									if (
										field.selectValue &&
										field.selectValue.length &&
										Array.isArray(field.selectValue)
									) {
										rec1.widthSize = Number(field.selectValue[0].value);
									}

									break;
								case 'height':
									if (field.value) {
										rec1.height = field.value;
									}
									if (
										field.selectValue &&
										field.selectValue.length &&
										Array.isArray(field.selectValue)
									) {
										rec1.heightSize = Number(field.selectValue[0].value);
									}
									break;
								case 'weight':
									if (field.value) {
										rec1.weightGross = field.value;
									}
									if (
										field.selectValue &&
										field.selectValue.length &&
										Array.isArray(field.selectValue)
									) {
										rec1.weightGrossSize = Number(field.selectValue[0].value);
									}
									break;
								case 'content':
									if (field.value) {
										rec1.techula = field.value;
									}
									if (
										field.selectValue &&
										field.selectValue.length &&
										Array.isArray(field.selectValue)
									) {
										rec1.techulaSize = Number(field.selectValue[0].value);
									}
									break;
								case 'segment':
									if (typeof field.value != 'string' && field.value)
										rec1.SegmentId = field.value.value;
									break;
								case 'brand':
									if (typeof field.value != 'string' && field.value) rec1.ModelId = field.value.value;
									break;
							}
						});
					}
				});
			}
		});
		await addItemRequest(rec1);
		setSweetAlert('addSucc');
		setTimeout(() => {
			setSweetAlert(false);
		}, 1500);
		let newOriginalRows = originalRows.filter((i: any) => {
			return i.BarCode != rowInEdit.BarCode;
		});
		let newRows = rows.filter((i: any) => {
			return i.BarCode != rowInEdit.BarCode;
		});
		let newRow = createRowsForCatalog([rec1]);
		setRows([...newRow, ...newRows]);
		setOriginalRows([...newRow, ...newOriginalRows]);
		// fetchAndResetCatalog()
	};
	const fetchAndResetCatalog = async () => {
		setIsDataReady(false);
		await fetchAndUpdateStore(dispatch, [
			{ state: catalogFullNoArchive, type: 'catalogFullDataNoArchive', updateAnyway: true },
			{ state: catalogFull, type: 'catalogFullData', updateAnyway: true },
			{ state: regCatalog, type: 'catalog', updateAnyway: true }
		]);
		setIsDataReady(true);
	};

	return (
		<div className="catalog">
			<div className="catalog__title mb-2">{formatMessage({ id: 'Items' })}</div>

			<div className="d-flex catalog__AddItemExcel height-2-5-rem justify-content-between">
				<div className="d-flex">
					<div className="mb-4 freeSearchInput">
						<InputText
							disabled={originalRows && !originalRows.length ? true : false}
							searchIcon
							onChange={onFilterTextBoxChanged}
							placeholder={formatMessage({ id: 'catalog__input' })}
							inputRef={freeSearchInputRef}
						/>
					</div>

					<button
						onClick={onFilter}
						className={`p-05 ml-1 no-outline ${
							!advancedSearch ? 'bg-gray' : 'btn-primary'
						} d-flex justify-content-center align-items-center  no-border round  text-bold-400 height-2-5 width-7-rem`}
					>
						<Icon
							src={'../' + config.iconsPath + 'general/filter.svg'}
							style={{ height: '0.5rem', width: '0.5rem' }}
						/>

						<span className="mr-1 ml-1">{formatMessage({ id: 'filter' })}</span>
					</button>

					{/* {catalogFull && catalogFull.length ? <button className='no-border no-outline ml-1 catalog__filterButton round height-2-5-rem bg-gray width-7-rem d-flex justify-content-center align-items-center' disabled={catalogFull && catalogFull.length ? false : true} onClick={filterCancel}>
						{formatMessage({ id: 'allItemss' })}
					</button> : null} */}

					{isLoading ? (
						<div className="ml-1">
							<Spinner />
						</div>
					) : null}
				</div>
				{userEditFlag && (
					<div className="d-flex">
						<Button
							disabled={originalRows && !originalRows.length ? true : false}
							onClick={toggleAddModal}
							className="round mr-05 d-flex justify-content-center align-items-center"
							color="primary"
						>
							{formatMessage({ id: 'newItem' })}
						</Button>
						{isDataReady ? (
							<div className="d-flex justify-content-center align-items-center ml-05 cursor-pointer">
								<ExportExcel
									fields={fields}
									data={rows}
									direction={'ltr'}
									translateHeader={true}
									icon={
										<Icon
											src={config.iconsPath + 'table/excel-download.svg'}
											style={{ height: '0.5rem', width: '0.5rem' }}
										/>
									}
								/>
							</div>
						) : null}
					</div>
				)}
			</div>

			<div style={{ width: '80%' }} className="mt-1 catalogGrid">
				<AgGrid
					onRowDoubleClick={editItem}
					resizable
					rowBufferAmount={300}
					defaultSortFieldNum={0}
					id={'BarCode'}
					gridHeight={'70vh'}
					floatFilter={false}
					translateHeader
					fields={fields}
					groups={[]}
					totalRows={[]}
					rows={rows}
					checkboxFirstColumn={false}
					pagination
					displayLoadingScreen={!isDataReady}
				/>

				{shownEdit ? (
					<GenericDrawer
						drawerType={'TAB'}
						caption={rowInEdit && rowInEdit.itemName ? rowInEdit.itemName : ''}
						shown={shownEdit}
						isCaptionText={true}
						validateCaption={'this input is required and more then 3'}
						required={['barcode']}
						toggleModal={toggleEditModal}
						data={editDataDrawer(
							rowInEdit,
							unitSizes,
							suppliers,
							groups,
							series,
							division,
							subGroup,
							regCatalog,
							toMap(regCatalog.filter((i:any) => String(i.BarCode) !== String(rowInEdit && rowInEdit.BarCode?rowInEdit.BarCode:'') ), "BarCode"),
							models,
							segments,
							chosenGroup,
							updateSelectedDefault
						)}
						saveChanges={saveEditItem}
						modalBeforeClose={true}
						titleEditable
					/>
				) : null}
				{shownAdd ? (
					<GenericDrawer
						drawerType={'TAB'}
						caption={`${formatMessage({ id: 'itemName' })} *`}
						validateCaption={'this input is required and more then 3'}
						required={['barcode']}
						shown={shownAdd}
						toggleModal={toggleAddModal}
						data={newProductDrawerData(
							unitSizes,
							suppliers,
							groups,
							series,
							division,
							subGroup,
							regCatalog,
							toMap(regCatalog, "BarCode"),
							models,
							segments,
							chosenGroup,
							updateSelectedDefault
						)}
						saveChanges={saveAddItem}
						modalBeforeClose={true}
						titleEditable
					/>
				) : null}

				{shownFilter ? (
					<GenericDrawer
						{...{
							drawerType: 'ACCORDION',
							caption: formatMessage({ id: 'filterAtable' }),
							shown: shownFilter,
							toggleModal: toggleFilterModal,
							data: filterDataDrawer(
								suppliers,
								groups,
								division,
								subGroup,
								series,
								regCatalog,
								models,
								segments,
								chosenGroup,
								updateSelectedDefault,
								tempData
							),
							saveChanges: saveFilterChanges,
							saveButtonText: 'filter',
							onCancelButton: filterCancel,
							cancelDisabled: catalogFull && catalogFull.length ? false : true,
							saveDisabled: catalogFull && catalogFull.length ? false : true,
							loadingBySave: catalogFull && catalogFull.length ? false : true,
							tempData: tempData,
							setTempData: setTempData
						}}
					/>
				) : null}
				{isAreYouSureModalOpen && returnModal(areYouSureModal[0])}
				{editHistory ? (
					<EditHistoryPopup
						editHistory={editHistory}
						models={models}
						segments={segments}
						onOutsideClick={setEditHistory}
					/>
				) : null}

				<SweetAlert
					success
					title={sweetAlert ? formatMessage({ id: sweetAlert }) : ''}
					show={sweetAlert ? true : false}
					closeOnClickOutside
					confirmBtnCssClass="display-hidden"
					onConfirm={() => setSweetAlert(false)}
				></SweetAlert>
			</div>
		</div>
	);
};

export default Catalog;
