import { GenericDrawerData } from "@src/components/edit-drawer";
import { FilterDrawerData } from "@src/components/filter-drawer";
import { InputTypes } from "@src/components/form-group";
import { useIntl } from "react-intl"

export const editDataDrawer = (rowData: any, unitSizes: any, suppliers: any, groups: any, series: any, division: any, subGroups: any, catalog: any, productsMap:any, models: any, segments: any, chosenGroup: any, updateSelectedDefault: (newData: any, type: string) => void) => {
	function reformatDate(dateStr: any) {
		let dArr = dateStr.split("-");
		return dArr[1] + "-" + dArr[2] + "-" + dArr[0];
	}


	const { formatMessage } = useIntl();
	let itemGroup = rowData.resetOfData.GroupId ? [{ value: rowData.resetOfData.GroupId, label: rowData.groupName }] : null
	let itemSubgroup = rowData.resetOfData.SubGroupId ? [{ value: rowData.resetOfData.SubGroupId, label: rowData.subGroupName }] : null
	let itemDivision = rowData.resetOfData.ClassesId ? [{ value: rowData.resetOfData.ClassesId, label: rowData.divisionName }] : null
	let itemSeries = rowData.resetOfData.DegemId ? [{ value: rowData.resetOfData.DegemId, label: rowData.seriesName }] : null
	let itemSupplier = rowData.resetOfData.SapakId ? [{ value: rowData.resetOfData.SapakId, label: rowData.supplierName }] : null
	let itemShaquiilie = rowData.resetOfData.Shakele ? true : false
	let itemAriza = rowData.resetOfData.SizeAriza
	let itemArizaCarton = rowData.resetOfData.Ariza


	let itemCreateDate = rowData.resetOfData.Create_Date ? reformatDate(rowData.resetOfData.Create_Date) : null
	let itemBlockedSale = rowData.resetOfData.Bdate_sale ? reformatDate(rowData.resetOfData.Bdate_sale) : null
	let itemBlockedBuy = rowData.resetOfData.Bdate_buy ? reformatDate(rowData.resetOfData.Bdate_buy) : null
	let itemModel = rowData.resetOfData.ModelId ? [{ value: rowData.resetOfData.ModelId, label: rowData.modelName }] : null
	let itemSegment = rowData.resetOfData.SegmentId ? [{ value: rowData.resetOfData.SegmentId, label: rowData.segmentName }] : null
	let itemArchive = rowData.resetOfData.Archives === 0 ? true : false
	let subGroupsIds = chosenGroup && catalog && catalog.length ? catalog.map((catalog: any) => chosenGroup.value == catalog.GroupId ? catalog.SubGroupId : undefined).filter((a: any) => a) :
		itemGroup && catalog && catalog.length ? catalog.map((catalog: any) => itemGroup && itemGroup[0].value == catalog.GroupId ? catalog.SubGroupId : undefined).filter((a: any) => a) : null
	let inventoryType = [{ value: rowData.resetOfData.statusCalMlay, label: rowData.inventoryType }]
	let itemServiceItem = rowData.resetOfData.ServiceItem ? true : false
	let itemLeaven = rowData.resetOfData.flagLeaven ? true : false
	let itemIsAriza = rowData.resetOfData.maharaz ? true : false


	let weightUnitsOptions = unitSizes.map((i: any) => {
		if(i.unitType == 1) return { value: i.Id, label: i.Name  }
		else return null
	}).filter((g:any)=>g)

	let depthHeightWidthOptions = unitSizes.map((i: any) => {
		if(i.unitType == 3) return { value: i.Id, label: i.Name  }
		else return null
	}).filter((g:any)=>g)

	let contentOptions = unitSizes.map((i: any) => {
		if(i.unitType == 2) return { value: i.Id, label: i.Name  }
		else return null
	}).filter((g:any)=>g)


	let unitsVal: any = {}
	unitSizes.forEach((i: any) => {
		unitsVal[i.Id] = i.Name
	})




	// let weightUnits = [formatMessage({ id: '-gram' }),formatMessage({ id: '-kg' })]
	// let heightDepthUnits = [formatMessage({ id: '-cm' }),formatMessage({ id: '-meter' })]
	// let containerUnits = [formatMessage({ id: '-mililiter' }),formatMessage({ id: '-liter' })]

	let itemHeight = rowData.resetOfData.height
	let itemHeightSize = [{ value: rowData.resetOfData.heightSize, label: unitsVal[rowData.resetOfData.heightSize] }]
	let itemWidth = rowData.resetOfData.width
	let itemWidthSize = rowData.resetOfData.widthSize ? [{ value: rowData.resetOfData.widthSize, label: unitsVal[rowData.resetOfData.widthSize] }] : []
	let itemContent = rowData.resetOfData.techula
	let itemContentSize = rowData.resetOfData.techulaSize ? [{ value: rowData.resetOfData.techulaSize, label: unitsVal[rowData.resetOfData.techulaSize] }] : []
	let itemDepth = rowData.resetOfData.length
	let itemDepthSize = rowData.resetOfData.lengthSize ? [{ value: rowData.resetOfData.lengthSize, label: unitsVal[rowData.resetOfData.lengthSize] }] : []
	let itemWeight = rowData.resetOfData.weightGross
	let itemWeightSize = rowData.resetOfData.weightGrossSize ? [{ value: rowData.resetOfData.weightGrossSize, label: unitsVal[rowData.resetOfData.weightGrossSize] }] : []
	let itemDepreciation = rowData.resetOfData.p_loss



	let editDrawerData: GenericDrawerData = {
		itemName: rowData.itemName,
		tabs: [{
			title: formatMessage({ id: 'item-details-status' }),
			tabId: '1',
			formGroup: [{
				title: formatMessage({ id: 'item-details' }),
				formId: '1',
				fields: [{
					label: formatMessage({ id: 'barcode' }),
					value: String(rowData.BarCode),
					name: 'barcode',
					type: InputTypes.NUMBER,
					onBlur: (value:number)=> {return productsMap[value] || false }
				}, {
					label: formatMessage({ id: 'shaquille' }),
					value: itemShaquiilie,
					name: "shaquille",
					type: InputTypes.CHECKBOX
				},
				// {
				// 	label: formatMessage({ id: 'packingFactor' }),
				// 	value: itemAriza,
				// 	name: 'packing-factor',
				// 	type: InputTypes.NUMBER
				// }, 
				{
					label: formatMessage({ id: 'supplier' }),
					value: '',
					defaultValue: itemSupplier,
					name: 'supplier',
					type: InputTypes.SELECT,
					options: suppliers ? suppliers.map((supplier: any) => {
						return { value: supplier.Id, label: supplier.Name }
					}) : []
				}, {
					label: formatMessage({ id: 'division' }),
					value: '',
					defaultValue: itemDivision,
					name: 'division',
					type: InputTypes.SELECT,
					options: division ? division.map((item: any) => { return { value: item.Id, label: item.Name } }) : [],

				}, {
					label: formatMessage({ id: 'group' }),
					value: '',
					name: 'group',
					defaultValue: itemGroup,
					onChangeFunc: (newSelected: any, name: string) => updateSelectedDefault(newSelected, name),
					type: InputTypes.SELECT,
					options: groups ? groups.map((item: any) => { return { value: item.Id, label: item.Name } }) : [],
				}, {
					label: formatMessage({ id: 'subgroup' }),
					value: '',
					defaultValue: itemSubgroup,
					name: 'subgroup',
					type: InputTypes.SELECT,
					options: subGroupsIds ?
						subGroups && subGroups.length ?
							subGroups.filter((sg: any) => subGroupsIds.includes(sg.Id)).map((option: any, index: number) => {
								return { index, value: option.Id, label: option.Name }
							})
							: [] : [],
				}, {
					label: formatMessage({ id: 'series' }),
					defaultValue: itemSeries,
					value: '',
					name: 'series',
					type: InputTypes.SELECT,
					options: series ? series.map((item: any) => { return { value: item.Id, label: item.Name } }) : [],
				}],
				// additionalFields: {
				// 	title: formatMessage({ id: 'choseKosher' }),
				// 	fields: [
				// {
				// 	label: 'Type of kosher',
				// 	value: '',
				// 	name: 'type-of-kosher',
				// 	type: InputTypes.CHECKBOX
				// }, {
				// 	label: 'Type of kosher 1',
				// 	value: '',
				// 	name: 'type-of-kosher-1',
				// 	type: InputTypes.CHECKBOX
				// }, {
				// 	label: 'Type of kosher 2',
				// 	value: '',
				// 	name: 'type-of-kosher-2',
				// 	type: InputTypes.CHECKBOX
				// }, {
				// 	label: 'Type of kosher 3',
				// 	value: '',
				// 	name: 'type-of-kosher-3',
				// 	type: InputTypes.CHECKBOX
				// }, {
				// 	label: 'Type of kosher 4',
				// 	value: '',
				// 	name: 'type-of-kosher-4',
				// 	type: InputTypes.CHECKBOX
				// }, {
				// 	label: 'Type of kosher 5',
				// 	value: '',
				// 	name: 'type-of-kosher-5',
				// 	type: InputTypes.CHECKBOX
				// }, {
				// 	label: 'Type of kosher 6',
				// 	value: '',
				// 	name: 'type-of-kosher-6',
				// 	type: InputTypes.CHECKBOX
				// }, {
				// 	label: 'Type of kosher 7',
				// 	value: '',
				// 	name: 'type-of-kosher-7',
				// 	type: InputTypes.CHECKBOX
				// }, {
				// 	label: 'Type of kosher 8',
				// 	value: '',
				// 	name: 'type-of-kosher-8',
				// 	type: InputTypes.CHECKBOX
				// }, {
				// 	label: 'Type of kosher 9',
				// 	value: '',
				// 	name: 'type-of-kosher-9',
				// 	type: InputTypes.CHECKBOX
				// }, {
				// 	label: 'Type of kosher 10',
				// 	value: '',
				// 	name: 'type-of-kosher-10',
				// 	type: InputTypes.CHECKBOX
				// }, {
				// 	label: 'Type of kosher 11',
				// 	value: '',
				// 	name: 'type-of-kosher-11',
				// 	type: InputTypes.CHECKBOX
				// }
				// 	]
				// }
			}, {
				title: formatMessage({ id: 'product_status' }),
				formId: '2',
				fields: [{
					label: formatMessage({ id: 'date_created' }),
					value: itemCreateDate ? itemCreateDate : '',
					name: 'date-of-establishment',
					onChangeFunc: (newSelected: any, name: string) => updateSelectedDefault(newSelected, name),
					type: InputTypes.SINGLE_DATE,
					calanderId: 'calnder1',
					inputId: 'input1',
				}, {
					label: formatMessage({ id: 'Active' }),
					value: '',
					defaultValue: itemArchive ? [{ label: formatMessage({ id: 'yes' }), value: formatMessage({ id: 'yes' }) }] : [{ label: formatMessage({ id: 'no' }), value: formatMessage({ id: 'no' }) }],
					name: 'active',
					type: InputTypes.SELECT,
					options: [formatMessage({ id: 'yes' }), formatMessage({ id: 'no' })],
					columns: 1
				},
				//  {
				// 	label: formatMessage({ id: 'automatic' }),
				// 	value: '',
				// 	name: 'automatic',
				// 	type: InputTypes.CHECKBOX,
				// 	columns: 1
				// },
				{
					label: formatMessage({ id: 'blockedSaleDate' }),
					value: itemBlockedSale ? itemBlockedSale : '',
					onChangeFunc: (newSelected: any, name: string) => updateSelectedDefault(newSelected, name),
					name: 'blocked-sale-date',
					type: InputTypes.SINGLE_DATE,
					calanderId: 'calnder2',
					inputId: 'input2',
				}, {
					label: formatMessage({ id: 'purchaseBlockedDate' }),
					value: itemBlockedBuy ? itemBlockedBuy : '',
					onChangeFunc: (newSelected: any, name: string) => updateSelectedDefault(newSelected, name),
					name: 'purchase-blocked-date',
					type: InputTypes.SINGLE_DATE,
					calanderId: 'calnder3',
					inputId: 'input3',
				}]
			}]
		}, {
			title: formatMessage({ id: 'properties' }),
			tabId: '2',
			formGroup: [{
				title: formatMessage({ id: 'item_properties' }),
				formId: '1',
				fields: [{
					label: formatMessage({ id: 'invetnroy-type' }),
					defaultValue: inventoryType,
					value: '',
					name: 'inventory-type',
					type: InputTypes.SELECT,
					options: [formatMessage({ id: 'Regular' }), formatMessage({ id: 'Daily' }), formatMessage({ id: 'Weekly' }), formatMessage({ id: 'Short valid time' }), formatMessage({ id: 'Inner' })]
				}, {
					label: formatMessage({ id: 'serviceProduct' }),
					value: itemServiceItem,
					name: 'service-product',
					type: InputTypes.CHECKBOX,
					columns: 1,
				},
				// {
				// 	label: 'March',
				// 	value: '',
				// 	name: 'march',
				// 	type: InputTypes.CHECKBOX,
				// 	columns: 1

				// },
				{
					label: formatMessage({ id: 'depreciation_p' }),
					value: itemDepreciation ? itemDepreciation : 0,
					name: 'depreciation',
					type: InputTypes.NUMBER,
				}, {
					label: formatMessage({ id: 'leaven' }),
					value: itemLeaven,
					name: 'leaven',
					type: InputTypes.CHECKBOX,
					fieldClassNames: 'mb-2-5'
				}, {
					label: formatMessage({ id: 'seasons' }),
					value: '',
					name: 'seasons',
					type: InputTypes.SELECT,
					options: []
				}, {
					label: formatMessage({ id: 'holidays' }),
					value: '',
					name: 'holidays',
					type: InputTypes.SELECT,
					options: []
				}, {
					label: formatMessage({ id: 'age_rest' }),
					value: '',
					name: 'age-restriction',
					type: InputTypes.SELECT,
					options: []
				}, {
					label: formatMessage({ id: 'saleLimitTime' }),
					value: '',
					name: 'time-limit-for-sale',
					type: InputTypes.SELECT,
					options: []
				}]
			}, {
				title: formatMessage({ id: 'packingFactors' }),
				formId: '2',
				fields: [{
					label: formatMessage({ id: 'unitsCarton' }),
					value: itemArizaCarton ? itemArizaCarton : 0,
					name: 'units-in-carton',
					type: InputTypes.NUMBER,
					disabled: false
				}, {
					label: formatMessage({ id: 'cartonsOnSurface' }),
					value: '',
					name: 'cartons-on-surface',
					type: InputTypes.NUMBER,
					disabled: true
				}, {
					label: formatMessage({ id: 'unitsInCarton' }),
					value: '',
					name: 'units-in-cardboard',
					type: InputTypes.NUMBER,
					disabled: true
				}, {
					label: formatMessage({ id: 'Box' }),
					value: itemIsAriza,
					name: 'box',
					type: InputTypes.CHECKBOX,
					columns: 2
				},
				]
			}]
		}, {
			title: formatMessage({ id: 'planogram' }),
			tabId: '3',
			formGroup: [{
				title: formatMessage({ id: 'item-dimensions' }),
				formId: '1',
				fields: [{
					label: formatMessage({ id: 'depth' }),
					value: itemDepth ? itemDepth: 0,
					defaultValue: itemDepth ? itemDepth: 0,
					name: 'depth',
					type: InputTypes.NUMBER,
					options: depthHeightWidthOptions,
					selectValue: itemDepthSize,
					selectName: 'depthSize',
					optionsRequired: true,
					// defaultValue: itemDepthSize,
					separator: true

				}, {
					label: formatMessage({ id: 'width' }),
					value: itemWidth ? itemWidth: 0,
					defaultValue: itemWidth ? itemWidth: 0,
					name: 'width',
					type: InputTypes.NUMBER,
					options: depthHeightWidthOptions,
					selectValue: itemWidthSize,
					selectName: 'widthSize',
					optionsRequired: true,
					separator: true
				}, {
					label: formatMessage({ id: 'height' }),
					value: itemHeight ? itemHeight: 0,
					defaultValue: itemHeight ? itemHeight: 0,
					name: 'height',
					type: InputTypes.NUMBER,
					options: depthHeightWidthOptions,
					selectValue: itemHeightSize,
					selectName: 'heightSize',
					optionsRequired: true,
					separator: true
				}, {
					label: formatMessage({ id: 'weight' }),
					value: itemWeight ? itemWeight : 0,
					defaultValue: itemWeight ? itemWeight: 0,
					name: 'weight',
					type: InputTypes.NUMBER,
					options: weightUnitsOptions,
					selectValue: itemWeightSize,
					selectName: 'weightSize',
					optionsRequired: true,
					separator: true
				}, {
					label: formatMessage({ id: 'content' }),
					value: itemContent ? itemContent: 0,
					defaultValue: itemContent ? itemContent: 0,
					name: 'content',
					type: InputTypes.NUMBER,
					options: contentOptions,
					selectValue: itemContentSize,
					selectName: 'contentSize',
					optionsRequired: true,
					separator: true
				}]
			}, {
				title: formatMessage({ id: 'moreDetails' }),
				formId: '2',
				fields: [{
					label: formatMessage({ id: 'segment' }),
					value: '',
					defaultValue: itemSegment,
					name: 'segment',
					type: InputTypes.SELECT,
					options: segments ? segments.map((item: any) => { return { value: item.Id, label: item.Name } }) : [],
				}, {
					label: formatMessage({ id: 'brand' }),
					value: '',
					defaultValue: itemModel,
					name: 'brand',
					type: InputTypes.SELECT,
					options: models ? models.map((item: any) => { return { value: item.Id, label: item.Name } }) : [],
				}, {
					label: formatMessage({ id: 'Arr' }),
					value: '',
					name: 'arrr',
					type: InputTypes.SELECT,
					options: []
				}]
			}]
		}]
	}
	return editDrawerData
}
export const newProductDrawerData = (unitSizes:any,suppliers: any, groups: any, series: any, division: any, subGroups: any, catalog: any, productsMap:any, models: any, segments: any, chosenGroup: any, updateSelectedDefault: (newData: any, type: string) => void) => {
	const { formatMessage } = useIntl();

	
		let weightUnitsOptions = unitSizes.map((i: any) => {
			if(i.unitType == 1) return { value: i.Id, label: i.Name  }
			else return null
		}).filter((g:any)=>g)
	
		let depthHeightWidthOptions = unitSizes.map((i: any) => {
			if(i.unitType == 3) return { value: i.Id, label: i.Name  }
			else return null
		}).filter((g:any)=>g)
	
		let contentOptions = unitSizes.map((i: any) => {
			if(i.unitType == 2) return { value: i.Id, label: i.Name  }
			else return null
		}).filter((g:any)=>g)
	


		let unitsVal: any = {}
		unitSizes.forEach((i: any) => {
			unitsVal[i.Id] = i.Name
		})

	let subGroupsIds = chosenGroup && catalog && catalog.length ? catalog.map((catalog: any) => chosenGroup.value == catalog.GroupId ? catalog.SubGroupId : undefined).filter((a: any) => a) : []
	let editDrawerData: GenericDrawerData = {
		itemName: '',
		tabs: [{
			title: formatMessage({ id: 'item-details-status' }),
			tabId: '1',
			formGroup: [{
				title: formatMessage({ id: 'item-details' }),
				formId: '1',
				fields: [{
					label: `${formatMessage({ id: 'barcode' })}*`,
					value: '',
					name: 'barcode',
					type: InputTypes.NUMBER,
					onBlur: (value:number)=> { return productsMap[value] || false }
				}, {
					label: formatMessage({ id: 'shaquille' }),
					value: false,
					name: 'shaquille',
					type: InputTypes.CHECKBOX
				},
				//  {
				// 	label: formatMessage({ id: 'packingFactor' }),
				// 	value: '',
				// 	name: 'packing-factor',
				// 	type: InputTypes.NUMBER
				// }, 
				{
					label: formatMessage({ id: 'supplier' }),
					value: '',
					name: 'supplier',
					type: InputTypes.SELECT,
					options: suppliers ? suppliers.map((supplier: any) => {
						return { value: supplier.Id, label: supplier.Name }

					}) : []
				}, {
					label: formatMessage({ id: 'division' }),
					value: '',
					name: 'division',
					type: InputTypes.SELECT,
					options: division ? division.map((item: any) => { return { value: item.Id, label: item.Name } }) : [],

				}, {
					label: formatMessage({ id: 'group' }),
					value: '',
					name: 'group',
					onChangeFunc: (newSelected: any, name: string) => updateSelectedDefault(newSelected, name),
					type: InputTypes.SELECT,
					options: groups ? groups.map((item: any) => { return { value: item.Id, label: item.Name } }) : [],
				}, {
					label: formatMessage({ id: 'subgroup' }),
					value: '',
					name: 'subgroup',
					type: InputTypes.SELECT,
					options: subGroupsIds ?
						subGroups && subGroups.length ?
							subGroups.filter((sg: any) => subGroupsIds.includes(sg.Id)).map((option: any, index: number) => {
								return { index, value: option.Id, label: option.Name }
							})
							: [] : [],
				}, {
					label: formatMessage({ id: 'series' }),
					value: '',
					name: 'series',
					type: InputTypes.SELECT,
					options: series ? series.map((item: any) => { return { value: item.Id, label: item.Name } }) : [],
				}],
				// additionalFields: {
				// 	title: formatMessage({ id: 'choseKosher' }),
				// 	fields: [
				// {
				// 	label: 'Type of kosher',
				// 	value: '',
				// 	name: 'type-of-kosher',
				// 	type: InputTypes.CHECKBOX
				// }, {
				// 	label: 'Type of kosher 1',
				// 	value: '',
				// 	name: 'type-of-kosher-1',
				// 	type: InputTypes.CHECKBOX
				// }, {
				// 	label: 'Type of kosher 2',
				// 	value: '',
				// 	name: 'type-of-kosher-2',
				// 	type: InputTypes.CHECKBOX
				// }, {
				// 	label: 'Type of kosher 3',
				// 	value: '',
				// 	name: 'type-of-kosher-3',
				// 	type: InputTypes.CHECKBOX
				// }, {
				// 	label: 'Type of kosher 4',
				// 	value: '',
				// 	name: 'type-of-kosher-4',
				// 	type: InputTypes.CHECKBOX
				// }, {
				// 	label: 'Type of kosher 5',
				// 	value: '',
				// 	name: 'type-of-kosher-5',
				// 	type: InputTypes.CHECKBOX
				// }, {
				// 	label: 'Type of kosher 6',
				// 	value: '',
				// 	name: 'type-of-kosher-6',
				// 	type: InputTypes.CHECKBOX
				// }, {
				// 	label: 'Type of kosher 7',
				// 	value: '',
				// 	name: 'type-of-kosher-7',
				// 	type: InputTypes.CHECKBOX
				// }, {
				// 	label: 'Type of kosher 8',
				// 	value: '',
				// 	name: 'type-of-kosher-8',
				// 	type: InputTypes.CHECKBOX
				// }, {
				// 	label: 'Type of kosher 9',
				// 	value: '',
				// 	name: 'type-of-kosher-9',
				// 	type: InputTypes.CHECKBOX
				// }, {
				// 	label: 'Type of kosher 10',
				// 	value: '',
				// 	name: 'type-of-kosher-10',
				// 	type: InputTypes.CHECKBOX
				// }, {
				// 	label: 'Type of kosher 11',
				// 	value: '',
				// 	name: 'type-of-kosher-11',
				// 	type: InputTypes.CHECKBOX
				// }
				// 	]
				// }
			}, {
				title: formatMessage({ id: 'product_status' }),
				formId: '2',
				fields: [{
					label: formatMessage({ id: 'date_created' }),
					value:'',

					name: 'date-of-establishment',
					onChangeFunc: (newSelected: any, name: string) => updateSelectedDefault(newSelected, name),
					type: InputTypes.SINGLE_DATE,
					calanderId: 'calnder4',
					inputId: 'input4',
				}, {
					label: formatMessage({ id: 'Active' }),
					value: '',
					name: 'active',
					type: InputTypes.SELECT,
					defaultValue: [{ value: formatMessage({ id: 'yes' }), label: formatMessage({ id: 'yes' }) }],
					options: [formatMessage({ id: 'yes' }), formatMessage({ id: 'no' })],
					columns: 1
				}, {
					label: formatMessage({ id: 'automatic' }),
					value: '',
					name: 'automatic',
					type: InputTypes.CHECKBOX,
					columns: 1
				}, {
					label: formatMessage({ id: 'blockedSaleDate' }),
					value:'',
					onChangeFunc: (newSelected: any, name: string) => updateSelectedDefault(newSelected, name),
					name: 'blocked-sale-date',
					type: InputTypes.SINGLE_DATE,
					calanderId: 'calnder5',
					inputId: 'input5',
				}, {
					label: formatMessage({ id: 'purchaseBlockedDate' }),
					value: '',
					onChangeFunc: (newSelected: any, name: string) => updateSelectedDefault(newSelected, name),
					name: 'purchase-blocked-date',
					type: InputTypes.SINGLE_DATE,
					calanderId: 'calnder6',
					inputId: 'input6',

				}]
			}]
		}, {
			title: formatMessage({ id: 'properties' }),
			tabId: '2',
			formGroup: [{
				title: formatMessage({ id: 'item_properties' }),
				formId: '1',
				fields: [{
					label: formatMessage({ id: 'invetnroy-type' }),
					value: '',
					name: 'inventory-type',
					type: InputTypes.SELECT,
					options: [formatMessage({ id: 'Regular' }), formatMessage({ id: 'Daily' }), formatMessage({ id: 'Weekly' }), formatMessage({ id: 'Short valid time' }), formatMessage({ id: 'Inner' })]
				}, {
					label: formatMessage({ id: 'serviceProduct' }),
					value: '',
					name: 'service-product',
					type: InputTypes.CHECKBOX,
					columns: 1
				},
				// {
				// 	label: 'March',
				// 	value: '',
				// 	name: 'march',
				// 	type: InputTypes.CHECKBOX,
				// 	columns: 1

				// },
				{
					label: formatMessage({ id: 'depreciation_p' }),
					value: '',
					name: 'depreciation',
					type: InputTypes.NUMBER,
				}, {
					label: formatMessage({ id: 'leaven' }),
					value: '',
					name: 'leaven',
					type: InputTypes.CHECKBOX,
					fieldClassNames: 'mb-2-5'
				}, {
					label: formatMessage({ id: 'seasons' }),
					value: '',
					name: 'seasons',
					type: InputTypes.SELECT,
					options: []
				}, {
					label: formatMessage({ id: 'holidays' }),
					value: '',
					name: 'holidays',
					type: InputTypes.SELECT,
					options: []
				}, {
					label: formatMessage({ id: 'age_rest' }),
					value: '',
					name: 'age-restriction',
					type: InputTypes.SELECT,
					options: []
				}, {
					label: formatMessage({ id: 'saleLimitTime' }),
					value: '',
					name: 'time-limit-for-sale',
					type: InputTypes.SELECT,
					options: []
				}, 
				]
			}, {
				title: formatMessage({ id: 'packingFactors' }),
				formId: '2',
				fields: [{
					label: formatMessage({ id: 'unitsCarton' }),
					value: '',
					name: 'units-in-carton',
					type: InputTypes.NUMBER,
					disabled: false
				}, {
					label: formatMessage({ id: 'cartonsOnSurface' }),
					value: '',
					name: 'cartons-on-surface',
					type: InputTypes.NUMBER,
					disabled: true
				}, {
					label: formatMessage({ id: 'unitsInCarton' }),
					value: '',
					name: 'units-in-cardboard',
					type: InputTypes.NUMBER,
					disabled: true
				},
				{
					label: formatMessage({ id: 'Box' }),
					value: '',
					name: 'box',
					type: InputTypes.CHECKBOX,
					columns: 2
				},
			]
			}]
		}, {
			title: formatMessage({ id: 'planogram' }),
			tabId: '3',
			formGroup: [{
				title: formatMessage({ id: 'item-dimensions' }),
				formId: '1',
				fields: [{
					label: formatMessage({ id: 'depth' }),
					value: 0,
					defaultValue: 0,
					name: 'depth',
					type: InputTypes.NUMBER,
					options: depthHeightWidthOptions,
					selectValue: null,
					selectName: 'depthSize',
					optionsRequired: true,
					separator: true

				}, {
					label: formatMessage({ id: 'width' }),
					value: 0,
					defaultValue: 0,
					name: 'width',
					type: InputTypes.NUMBER,
					options: depthHeightWidthOptions,
					selectValue: null,
					selectName: 'widthSize',
					optionsRequired: true,
					separator: true
				}, {

					label: formatMessage({ id: 'height' }),
					value: 0,
					defaultValue: 0,
					name: 'height',
					type: InputTypes.NUMBER,
					options: depthHeightWidthOptions,
					selectValue: null,
					selectName: 'heightSize',
					optionsRequired: true,
					separator: true
				}, {
					label: formatMessage({ id: 'weight' }),
					value: 0,
					defaultValue: 0,
					name: 'weight',
					type: InputTypes.NUMBER,
					options: weightUnitsOptions,
					selectValue: null,
					selectName: 'weightSize',
					optionsRequired: true,
					separator: true
				}, {
					label: formatMessage({ id: 'content' }),
					value: 0,
					defaultValue: 0,
					name: 'content',
					type: InputTypes.NUMBER,
					options: contentOptions,
					selectValue: null,
					selectName: 'contentSize',
					optionsRequired: true,
					separator: true
				}]
			}, {
				title: formatMessage({ id: 'moreDetails' }),
				formId: '2',
				fields: [{
					label: formatMessage({ id: 'segment' }),
					value: '',
					name: 'segment',
					type: InputTypes.SELECT,
					options: segments ? segments.map((item: any) => { return { value: item.Id, label: item.Name } }) : [],
				}, {
					label: formatMessage({ id: 'brand' }),
					value: '',
					name: 'brand',
					type: InputTypes.SELECT,
					options: models ? models.map((item: any) => { return { value: item.Id, label: item.Name } }) : [],
				}, {
					label: formatMessage({ id: 'Arr' }),
					value: '',
					name: 'arrr',
					type: InputTypes.SELECT,
					options: []
				}]
			}]
		}]
	}
	return editDrawerData
}
export const filterDataDrawer = (suppliers: any, groups: any, division: any, subGroups: any, series: any, catalog: any, models: any, segments: any, chosenGroup: any, updateSelectedDefault: (newData: any, type: string) => void, tempData: any) => {
	const { formatMessage } = useIntl();
	let subGroupsIds = chosenGroup && chosenGroup.length && catalog && catalog.length ? catalog.map((catalog: any) => chosenGroup.map((group: any) => group.value).includes(catalog.GroupId) ? catalog.SubGroupId : undefined).filter(a => a) : []


	let filterDataDrawers: FilterDrawerData = {
		layouts: [{
			title: formatMessage({ id: 'filterByItem' }),
			layoutId: '1',
			formGroup: [{
				formId: '1',
				fields: [
					// 	{
					// 	label: formatMessage({ id: 'packingFactor' }),
					// 	value: tempData ? tempData['packing-factor'] && tempData['packing-factor'].data ? tempData['packing-factor'].data[0].label : '' : '',
					// 	name: 'packing-factor',
					// 	type: InputTypes.NUMBER,
					// 	columns: 1
					// },

					{
						label: formatMessage({ id: 'supplier' }),
						value: '',
						defaultValue: tempData ? tempData.supplier && tempData.supplier.data && tempData.supplier.data : null,
						placeholder: formatMessage({ id: 'Allsupplier' }),
						name: 'supplier',
						type: InputTypes.SELECT,
						isMulti: true,
						options: suppliers ? suppliers.map((supplier: any) => {
							return { value: supplier.Id, label: supplier.Name }
						}) : []
					},

					{
						label: formatMessage({ id: 'division' }),
						value: '',
						defaultValue: tempData ? tempData.division && tempData.division.data && tempData.division.data : null,
						placeholder: formatMessage({ id: 'allDivisions' }),
						name: 'division',
						isMulti: true,
						type: InputTypes.SELECT,
						options: division ? division.map((division: any) => {
							return { value: division.Id, label: division.Name }
						}) : []
					}, {
						label: formatMessage({ id: 'group' }),
						value: '',
						defaultValue: tempData ? tempData.group && tempData.group.data && tempData.group.data : null,
						placeholder: formatMessage({ id: 'Allgroups' }),
						name: 'group',
						onChangeFunc: (newSelected: any, name: string) => updateSelectedDefault(newSelected, name),
						isMulti: true,
						type: InputTypes.SELECT,
						options: subGroups && groups ? groups.map((group: any) => {
							return { value: group.Id, label: group.Name }
						}) : []
					}, {
						label: formatMessage({ id: 'subGroup' }),
						value: '',
						isMulti: true,
						defaultValue: tempData ? tempData.subgroup && tempData.subgroup.data && tempData.subgroup.data : null,
						placeholder: formatMessage({ id: 'allsubgroups' }),
						name: 'subgroup',
						type: InputTypes.SELECT,
						options: subGroupsIds ?
							subGroups && subGroups.length ?
								subGroups.filter((sg: any) => subGroupsIds.includes(sg.Id)).map((option: any, index: number) => {
									return { index, value: option.Id, label: option.Name }
								})
								: [] : [],
					}, {
						label: formatMessage({ id: 'series' }),
						value: '',
						isMulti: true,
						defaultValue: tempData ? tempData.series && tempData.series.data && tempData.series.data : null,
						placeholder: formatMessage({ id: 'allseries' }),
						name: 'series',
						type: InputTypes.SELECT,
						options: series ? series.map((series: any) => {
							return { value: series.Id, label: series.Name }
						}) : []
					}, {
						label: formatMessage({ id: 'purchase_block' }),
						value: '',
						defaultValue: tempData ? tempData['purchase_block'] && tempData['purchase_block'].data && tempData['purchase_block'].data[0] : null,
						name: 'purchase_block',
						type: InputTypes.SELECT,
						disabled: false,
						options: [formatMessage({ id: 'Including' }), formatMessage({ id: 'Yes' }), formatMessage({ id: 'No' })],
						firstSelected: tempData && tempData.purchase_block && tempData.purchase_block.data ? false : true
					}, {
						label: formatMessage({ id: 'sale_block' }),
						value: '',
						name: 'sale_block',
						defaultValue: tempData && tempData.sale_block && tempData.sale_block.data ? tempData.sale_block.data[0] : null,
						type: InputTypes.SELECT,
						options: [formatMessage({ id: 'Including' }), formatMessage({ id: 'Yes' }), formatMessage({ id: 'No' })],
						firstSelected: tempData && tempData.sale_block && tempData.sale_block.data ? false : true
					}, {
						label: formatMessage({ id: 'archive' }),
						value: '',
						name: 'archives',
						defaultValue: tempData && tempData.archives && tempData.archives.data ? tempData.archives.data[0] : null,
						type: InputTypes.SELECT,
						options: [formatMessage({ id: 'No' }), formatMessage({ id: 'Yes' }), formatMessage({ id: 'Including' })],
						firstSelected: tempData && tempData.archives && tempData.archives.data ? false : true
					}, {
						label: formatMessage({ id: 'a.auto' }),
						value: '',
						defaultValue: tempData && tempData.atomic && tempData.atomic.data ? tempData.atomic.data[0] : null,
						name: 'atomic',
						type: InputTypes.SELECT,
						options: [formatMessage({ id: 'Including' }), formatMessage({ id: 'Yes' }), formatMessage({ id: 'No' })],
						firstSelected: tempData && tempData.atomic && tempData.atomic.data ? false : true
					},
					{
						label: formatMessage({ id: 'shaquille' }),
						value: '',
						defaultValue: tempData && tempData.shaquille && tempData.shaquille.data ? tempData.shaquille.data[0] : null,
						name: 'shaquille',
						type: InputTypes.SELECT,
						options: [formatMessage({ id: 'Including' }), formatMessage({ id: 'Yes' }), formatMessage({ id: 'No' })],
						firstSelected: tempData && tempData.shaquille && tempData.shaquille.data ? false : true,
					},]
			}]
		}, {
			title: formatMessage({ id: 'filterByItemProp' }),
			layoutId: '2',
			formGroup: [{
				formId: '1',
				fields: [{
					label: formatMessage({ id: 'invetnroy-type' }),
					value: '',
					name: 'inventory-type',
					isMulti: true,
					defaultValue: tempData ? tempData['inventory-type'] && tempData['inventory-type'].data && tempData['inventory-type'].data : null,
					type: InputTypes.SELECT,
					columns: 2,
					options: [formatMessage({ id: 'Regular' }), formatMessage({ id: 'Daily' }), formatMessage({ id: 'Weekly' }), formatMessage({ id: 'Short valid time' }), formatMessage({ id: 'Inner' })]
				}, {
					label: formatMessage({ id: 'service_product' }),
					value: '',
					defaultValue: tempData && tempData['service-product'] && tempData['service-product'].data ? tempData['service-product'].data[0] : null,
					name: 'service-product',
					type: InputTypes.SELECT,
					options: [formatMessage({ id: 'Including' }), formatMessage({ id: 'Yes' }), formatMessage({ id: 'No' })],
					firstSelected: tempData && tempData['service-product'] && tempData['service-product'].data ? false : true,
				},
				//  {
				// 	label: formatMessage({ id: 'Box' }),
				// 	value: tempData ? tempData['box'] && tempData['box'].data ? tempData['box'].data[0].label === 'true' ? 'true' : 'false' : '' : '',
				// 	name: 'box',
				// 	type: InputTypes.CHECKBOX,
				// 	columns: 1
				// }, 
				{
					label: formatMessage({ id: 'hamez' }),
					value: '',
					defaultValue: tempData && tempData['hamez'] && tempData['hamez'].data ? tempData['hamez'].data[0] : null,
					name: 'hamez',
					type: InputTypes.SELECT,
					options: [formatMessage({ id: 'Including' }), formatMessage({ id: 'Yes' }), formatMessage({ id: 'No' })],
					firstSelected: tempData && tempData['hamez'] && tempData['hamez'].data ? false : true,
				}, {
					label: formatMessage({ id: 'seasons' }),
					value: '',
					name: 'seasons',
					type: InputTypes.SELECT,
					options: []
				}, {
					label: formatMessage({ id: 'holidays' }),
					value: '',
					name: 'holidays',
					type: InputTypes.SELECT,
					options: []
				}, {
					label: formatMessage({ id: 'age_rest' }),
					value: '',
					name: 'age-restriction',
					type: InputTypes.SELECT,
					options: []
				}, {
					label: formatMessage({ id: 'sellHourLimit' }),
					value: '',
					name: 'time-limit-for-sale',
					type: InputTypes.SELECT,
					options: []
				}],
				// additionalFields: {
				// 	title: formatMessage({ id: 'chose_kosher' }),
				// 	fields: [{
				// 		label: 'Type of kosher',
				// 		value: '',
				// 		name: 'type-of-kosher',
				// 		type: InputTypes.CHECKBOX
				// 	}, {
				// 		label: 'Type of kosher 1',
				// 		value: '',
				// 		name: 'type-of-kosher-1',
				// 		type: InputTypes.CHECKBOX
				// 	}, {
				// 		label: 'Type of kosher 2',
				// 		value: '',
				// 		name: 'type-of-kosher-2',
				// 		type: InputTypes.CHECKBOX
				// 	}, {
				// 		label: 'Type of kosher 3',
				// 		value: '',
				// 		name: 'type-of-kosher-3',
				// 		type: InputTypes.CHECKBOX
				// 	}, {
				// 		label: 'Type of kosher 4',
				// 		value: '',
				// 		name: 'type-of-kosher-4',
				// 		type: InputTypes.CHECKBOX
				// 	}, {
				// 		label: 'Type of kosher 5',
				// 		value: '',
				// 		name: 'type-of-kosher-5',
				// 		type: InputTypes.CHECKBOX
				// 	}, {
				// 		label: 'Type of kosher 6',
				// 		value: '',
				// 		name: 'type-of-kosher-6',
				// 		type: InputTypes.CHECKBOX
				// 	}, {
				// 		label: 'Type of kosher 7',
				// 		value: '',
				// 		name: 'type-of-kosher-7',
				// 		type: InputTypes.CHECKBOX
				// 	}, {
				// 		label: 'Type of kosher 8',
				// 		value: '',
				// 		name: 'type-of-kosher-8',
				// 		type: InputTypes.CHECKBOX
				// 	}, {
				// 		label: 'Type of kosher 9',
				// 		value: '',
				// 		name: 'type-of-kosher-9',
				// 		type: InputTypes.CHECKBOX
				// 	}, {
				// 		label: 'Type of kosher 10',
				// 		value: '',
				// 		name: 'type-of-kosher-10',
				// 		type: InputTypes.CHECKBOX
				// 	}, {
				// 		label: 'Type of kosher 11',
				// 		value: '',
				// 		name: 'type-of-kosher-11',
				// 		type: InputTypes.CHECKBOX
				// 	}]
				// }
			}]
		}, {
			title: formatMessage({ id: 'filterByPlanogram' }),
			layoutId: '3',
			formGroup: [{
				formId: '1',
				fields: [{
					label: formatMessage({ id: 'content' }),
					value: tempData ? tempData['content'] && tempData['content'].data ? tempData['content'].data[0].label : '' : '',
					name: 'content',
					type: InputTypes.NUMBER
				}, {
					label: formatMessage({ id: 'segment' }),
					value: formatMessage({ id: 'allSegments' }),
					name: 'segment',
					isMulti: true,
					defaultValue: tempData ? tempData['segment'] && tempData['segment'].data && tempData['segment'].data : null,
					type: InputTypes.SELECT,
					options: segments ? segments.map((segment: any) => {
						return { value: segment.Id, label: segment.Name }
					}) : []
				}, {
					label: formatMessage({ id: 'brand' }),
					value: formatMessage({ id: 'allBrands' }),
					name: 'brand',
					defaultValue: tempData ? tempData['brand'] && tempData['brand'].data && tempData['brand'].data : null,
					isMulti: true,

					type: InputTypes.SELECT,
					options: models ? models.map((brand: any) => {
						return { value: brand.Id, label: brand.Name }
					}) : []
				}, {
					label: formatMessage({ id: 'array' }),
					value: '',
					name: 'arr',
					type: InputTypes.SELECT,
					options: []
				}]
			}]
		}]
	}
	return filterDataDrawers
}
