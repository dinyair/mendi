import * as React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { RootStore } from '@src/store';
import { Button } from "reactstrap"
import { Table, AgGrid, BlankPopUp } from '@components';
import { fetchEditHistory } from './initial-state'
import { useIntl } from "react-intl"

interface Props {
    onOutsideClick: any
    editHistory: any
    models: any
    segments: any
}

export const EditHistoryPopup: React.FC<Props> = (props: Props) => {
    const { onOutsideClick, editHistory, models, segments } = props
    const dispatch = useDispatch();
    const { formatMessage } = useIntl();

    const [isDataReady, setIsDataReady] = React.useState<boolean>(false)
    const [rows, setRows] = React.useState<any>([])
    const [fields, setFields] = React.useState<any>([
        {
            text: "action", type: 'string', centerHeader: true, width: window.innerWidth * 0.05, minWidth: 70,
        },
        { text: "updateDate", type: 'Date', centerHeader: true, width: window.innerWidth * 0.05, minWidth: 70, },
        { text: "time", type: 'string', centerHeader: true, width: window.innerWidth * 0.05, minWidth: 50, },
        {
            text: "barcode", type: 'number', centerHeader: true, width: window.innerWidth * 0.05, minWidth: 70,
            filter: 'agMultiColumnFilter', filterParams: { filters: [{ filter: 'agSetColumnFilter', filterParams: { suppressMiniFilter: true } }] }
        },
        {
            text: "itemName", type: 'string', centerHeader: true, width: window.innerWidth * 0.05, minWidth: 50,
            filter: 'agMultiColumnFilter', filterParams: { filters: [{ filter: 'agSetColumnFilter', filterParams: { suppressMiniFilter: true } }] }
        },
        { text: "segment", type: 'string', centerHeader: true, width: window.innerWidth * 0.05, minWidth: 50, },
        { text: "brand", type: 'string', centerHeader: true, width: window.innerWidth * 0.05, minWidth: 50, },
        {
            text: "actions", type: 'string', centerHeader: true, width: window.innerWidth * 0.001, minWidth: 1, noFilter: true,
            cellRenderer: 'IconRender', renderIcons: [
                { actions: true },
                { type: 'edit', title: 'Edit', onClick: (data: any) => console.log('edit', data), icon: 'edit.svg', },
            ],
        },
    ])
    React.useEffect(() => {
        if (editHistory) {
            getHistoryEditData()
        }
    }, [editHistory])
    const getHistoryEditData = async () => {
        let history = await fetchEditHistory(editHistory.BarCode)
        if (!history.length) {
            setIsDataReady(true)
        } else {
            let _models: any = {}
            let _segments: any = {}
            for (var i = 0; i < models.length; i++) {
                _models[models[i].Id] = models[i].Name;
            }
            for (var i = 0; i < segments.length; i++) {
                _segments[segments[i].Id] = segments[i].Name;
            }

            let newRows = history.map((i: any) => {
                let dateAndtime = i.ts.split(' ')
                let updateDate = dateAndtime[0]
                let time = `${dateAndtime[1].split(":")[0]}:${dateAndtime[1].split(":")[1]}`
                let action = formatMessage({ id: i.Action })
                let itemName = i.Name
                const segment = _segments[i.SegmentId]
                const brand = _models[i.ModelId]
                return { barcode: i.BarCode, updateDate, time, action, itemName, segment, brand, restOfDate: { ...i } }
            })
            setRows(newRows)
            setIsDataReady(true)
        }
    }

    return (<BlankPopUp className={'left-20-per'}
        onOutsideClick={onOutsideClick}
        style={{ width: '70rem', height: rows ? rows.length+5 * 3.9 + 'rem' : '15rem', top: '4rem' }}>
        <div className='d-flex-column width-90-per m-auto-t-1-5 text-black font-bold-400'>
            <AgGrid
                resizable
                rowBufferAmount={300}
                defaultSortFieldNum={0}
                id={'fd'}
                gridHeight={rows ? rows.length+5 * 3.4 + 'rem': '13rem'}
                floatFilter={false}
                translateHeader
                fields={fields}
                groups={[]}
                totalRows={[]}
                rows={rows}
                checkboxFirstColumn={false}
                noDataReady={!isDataReady}
            />
        </div>
    </BlankPopUp>);
};

export default EditHistoryPopup;
