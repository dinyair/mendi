import * as React from 'react';
import {ColumnFormats} from './column-formats';
import {DataRow, ColumnType, ColumnValues} from './interfaces';

interface Props {
	row: DataRow;
	columns: ColumnType[];
	saveChanges: (data: any) => void;
}

export const Row: React.FC<Props> = ({row, columns, saveChanges, saveFilterChanges}) => {
	const [currRow, setRow] = React.useState(row.columnValues);
	const updateRow = (newRow: ColumnValues) => {
		saveChanges(newRow);
		setRow({...row.columnValues, ...newRow});
	}

	return (
		<tr>
			{columns.map(({columnId}) => (
				<td key={`${currRow.entityId}-${columnId}`}>
					<ColumnFormats columnType={columnId} updateRow={updateRow} saveFilterChanges={saveFilterChanges} row={currRow}>
						{currRow[columnId]}
					</ColumnFormats>
				</td>
			))}
		</tr>
	);
};
