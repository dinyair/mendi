import { SuppliersActionTypes } from './enums';
import { suppliersInitialState } from './initial-state';
import { Supplier, SuppliersAction, SupplierGroup } from './interfaces';

export default (state = suppliersInitialState, { type, payload }: SuppliersAction): SupplierGroup => {

	switch (type) {
		case SuppliersActionTypes.SET_SUPPLIERS:
			return {
				...state,
				suppliers: {
					...state._suppliers,
					default: payload
				}
			}

		case SuppliersActionTypes.SET_SUPPLIERS_GROUP:
			return payload;

		case SuppliersActionTypes.SET_SUPPLIER_TITLE:
			const { key, value } = payload
				const newTitles = (titles: any, key: string, value: any) => {
					let newTitles:any = titles
					newTitles[key] = value
					return newTitles
				}
				return { ...state,
						supplier_title: newTitles(state.supplier_title, key, value)
					   };

					   case SuppliersActionTypes.DELETE_CATEGORY_COLUMN:
							const { key } = payload
							const newItem = (item: any) => {
								var newItem:any = {}
								Object.keys(item).map((field: string, index: number) => {
									if( field == key ){
										return
									}
									newItem[field] = item[field]
								})
								return newItem
							}

							return {
								...state,
								suppliers: {
									...state._suppliers,
									default: state._suppliers.default.map((supplier: any) => {
										return {
											...newItem(supplier)
										}
									})
								}
							};




					 case SuppliersActionTypes.ADD_SUPPLIER_COLUMNS:
							return {
										...state,
										suppliers: {
											...state._suppliers,
											default: state._suppliers.default.map((supplier: any) => {
												return {
													...supplier,
													[payload.key]: payload.value
												}
											})
										}
									};


		default:
			return state;
	}
};
