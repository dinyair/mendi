import { getRequest, postRequest } from "@src/http";

export const getCatalogUpdated = async () => {
    const resp = await getRequest<any>(`getcatalog`, {});
    return resp
}
export const getSubGroup = async () => {
    const resp = await getRequest<any>(`getSubGroup`, {});
    return resp
}
export const getDivision = async () => {
    const resp = await getRequest<any>(`getClasses`, {});
    return resp
}
export const getUnitsSizes = async () => {
    const resp = await getRequest<any>(`getUnitSizeNew`, {});
    return resp
}
export const getDegems = async () => {
    const resp = await getRequest<any>(`getDegems`)
    return resp
}
export const getSecondCatalog = async () => {
    const resp = await getRequest<any>(`getCatalogA?arc1=22`)
    return resp
}
export const getGroup = async () => {
    const resp = await getRequest<any>(`getGroup`)
    return resp
}
export const getSuppliers = async () => {
    const resp = await getRequest<any>('AppSapakim')
    return resp
}
export const getSubSuppliers = async () => {
    const resp = await getRequest<any>('AppGetSubSapak')
    return resp
}
export const fetchEditHistory = async (barcode: any) => {
    const resp = await getRequest<any>(`getcatalogHist?barcode=${barcode}`)
    return resp
}

export const editItemRequest = async (rec1: any) => {
    const resp = await postRequest<any>('AppUpdateCatalog', {
        rec1
    });
    return resp
}
export const addItemRequest = async (rec1: any) => {
    const resp = await postRequest<any>('AppInsertCatalog', {
        rec1
    });
    return resp
}
export const deleteItemRequest = async (Id: any) => {
    const resp = await postRequest<any>('AppDeleteCatalog', {
        Id
    });
    return resp
}

// BarCode: "1234569999991"
// CatalogId: "1234569999991"
// Name: "בדיקה בדיקהה"

// AppInsertCatalog // post
// rec1: {Name: "בדיקות - אביחי - בדיקות", BarCode: "123999999999", Shakele: 1, statusCalMlay: 1,…}
// BarCode: "123999999999"
// CatalogId: "123999999999"
// Name: "בדיקות - אביחי - בדיקות"
// Shakele: 1
// statusCalMlay: 1

 // DELETE NOT WOKRING // AppDeleteCatalog // POST //  Id: 303001