import { postRequest } from "@src/http";
import { deleteRoutes, insertRoutes, updateRoutes } from "./services/enums";


export const genericRequest = async (route: updateRoutes | insertRoutes | deleteRoutes, obj: any) => {
    const resp = await postRequest<any>(route, obj);
    return resp
}
