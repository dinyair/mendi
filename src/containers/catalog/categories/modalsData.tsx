import * as React from 'react';
import { Button } from 'reactstrap';

import { useIntl } from 'react-intl';
import AsyncSelect from 'react-select/async';
import { FormGroup, InputTypes } from '@src/components/form-group';
import { DrawerTab } from '@src/components/edit-drawer/interfaces';
import { useSelector } from 'react-redux';
import { RootStore } from '@src/store';
import { CATALOG_TYPES } from './services/enums';
import ExportExcel from '@src/components/ExportExcel/ExportExcel';
import { GenericModals } from '@src/components/GenericModals/index';
import { ModalInterface, ModalsInterface, ModalTypes } from '@src/components/GenericModals/interfaces';

interface Props {
	modalType: ModalTypes;
	saveDisabled?: boolean,
	toggleModal: (type: ModalTypes) => void;
	modalHandlers: any;
	selectedRow: any;
	filteredOptions: any;
	newDrawerData: any;
	setDrawerData: (data: any) => void;
}

export const ModalsData: React.FC<Props> = props => {
	const { toggleModal, modalHandlers, selectedRow, filteredOptions, modalType, newDrawerData, setDrawerData, saveDisabled } = props;

	const { formatMessage } = useIntl();

	const subGroups = useSelector((state: RootStore) => state._subGroups);
	const layouts = useSelector((state: RootStore) => state._layouts);
	const [row, setRow] = React.useState<any>(null);
	const [fieldsToReturn, setFieldsToReturn] = React.useState<any>(null);
	const [currentLevel, setCurrentLevel] = React.useState<number>(0);
	const [selectedGroup, setSelectedGroup] = React.useState<any>(null);
	const [fieldName, setFieldName] = React.useState<any>(null);

	const singleTitles = [
		`${formatMessage({ id: 'className' })}`,
		`${formatMessage({ id: 'groupName' })}`,
		`${formatMessage({ id: 'subGroupsName' })}`,
		`${formatMessage({ id: 'seriesName' })}`,
		`${formatMessage({ id: 'layoutName' })}`,
		`${formatMessage({ id: 'segmentName' })}`,
		`${formatMessage({ id: 'brandName' })}`
	];



	React.useEffect(() => {
		if (selectedRow) {
			const row = selectedRow;
			setRow(row);

			switch (row.type) {
				case CATALOG_TYPES.DIVISIONS:
					setFieldName(singleTitles[0]);
					break;
				case CATALOG_TYPES.GROUPS:
					setFieldName(singleTitles[1]);
					break;
				case CATALOG_TYPES.SUBGROUPS:
					setFieldName(singleTitles[2]);
					break;
				case CATALOG_TYPES.LAYOUTS:
					setFieldName(singleTitles[4]);
					break;
				case CATALOG_TYPES.MODELS:
					setFieldName(singleTitles[6]);
					break;
				case CATALOG_TYPES.SEGMENTS:
					setFieldName(singleTitles[5]);
					break;
				case CATALOG_TYPES.SERIES:
					setFieldName(singleTitles[3]);
					break;
			}
		}
	}, []);

	const categoriesGroups = [
		{ type: CATALOG_TYPES.DIVISIONS, label: formatMessage({ id: 'divisons' }) },
		{ type: CATALOG_TYPES.GROUPS, label: formatMessage({ id: 'groups' }) },
		{ type: CATALOG_TYPES.SUBGROUPS, label: formatMessage({ id: 'subGroups' }) },
		{ type: CATALOG_TYPES.SERIES, label: formatMessage({ id: 'Serieses' }) },
		{ type: CATALOG_TYPES.LAYOUTS, label: formatMessage({ id: 'layouts' }) },
		{ type: CATALOG_TYPES.SEGMENTS, label: formatMessage({ id: 'segments' }) },
		{ type: CATALOG_TYPES.MODELS, label: formatMessage({ id: 'models' }) }
	];

	const loadGroupOptions = (inputValue: string, callback: any) => {
		callback();
	};

	const selectStyles = {
		container: (base: any) => ({
			...base
		}),
		option: (provided: any, state: any) => ({
			...provided,

			'&:hover': {
				color: '#ffffff',
				backgroundColor: '#31baab'
			},
			color: state.isSelected ? '#ffffff' : '#1e1e20',
			backgroundColor: state.isSelected ? '#027b7a' : '#ffffff',
			borderBottom: '1px dotted lightgray',
			padding: 10
		}),
		control: (provided: any) => ({
			...provided,
			width: '25rem!important',
			borderRadius: '2vh!important',
			maxHeight: '1.5rem',
			alignItems: 'baseline',
			height: '1.5rem'
		})
	};

	const groupSelector = (
		<AsyncSelect
			name={'groupSelect'}
			isMulti={false}
			isClearable={true}
			styles={selectStyles}
			loadOptions={loadGroupOptions}
			defaultOptions={categoriesGroups.map((option: any, index: number) => {
				return { index, value: option.type, label: option.label };
			})}
			defaultValue={selectedGroup}
			className="width-25-rem pb-1"
			width="300px"
			onInputChange={() => { }}
			onChange={(e: any, actionMeta: any) => {
				// console.log(e)
				//     console.log(actionMeta)
				if (e) {
					if (actionMeta.name === 'groupSelect') {
						let fieldName = '';
						setSelectedGroup(e);
						switch (e.value) {
							case CATALOG_TYPES.DIVISIONS:
								setFieldName(singleTitles[0]);
								fieldName = singleTitles[0];
								break;
							case CATALOG_TYPES.GROUPS:
								setFieldName(singleTitles[1]);
								fieldName = singleTitles[1];
								break;
							case CATALOG_TYPES.SUBGROUPS:
								setFieldName(singleTitles[2]);
								fieldName = singleTitles[2];
								break;
							case CATALOG_TYPES.LAYOUTS:
								setFieldName(singleTitles[4]);
								fieldName = singleTitles[4];
								break;
							case CATALOG_TYPES.MODELS:
								setFieldName(singleTitles[6]);
								fieldName = singleTitles[6];
								break;
							case CATALOG_TYPES.SEGMENTS:
								setFieldName(singleTitles[5]);
								fieldName = singleTitles[5];
								break;
							case CATALOG_TYPES.SERIES:
								setFieldName(singleTitles[3]);
								fieldName = singleTitles[3];
								break;
						}

						setDrawerData(drawerTabs(addModalDefaultField(fieldName), addModalFields(fieldName)));
					}
				} else if (actionMeta.action == 'clear') {
					console.log('clear');
					setFieldName(null);
					setSelectedGroup(null);
				}
			}}
			classNamePrefix="select"
			placeholder={
				modalType === ModalTypes.export
					? formatMessage({ id: 'Choose which data to export' })
					: formatMessage({ id: 'choose where to add' })
			}
		/>
	);

	const handleNextLevel = () => {
		setCurrentLevel(currentLevel + 1);
	};

	const handleBackLevel = () => {
		setFieldsToReturn(null);
		setCurrentLevel(currentLevel - 1);
	};

	const findRelatedLayout = (row: any) => {
		const subGroupIndex = subGroups.findIndex(ele => ele.Id == row.id);
		const subGroup = subGroups[subGroupIndex];
		let layout: any;
		if (subGroup && subGroup.LayoutId) {
			layout = layouts.find(ele => ele.Id === subGroup.LayoutId);
		}
		return layout ? { label: layout.Name, value: layout.Id } : null;
	};

	const findRelatedSubGroups = (row: any) => {
		const relatedSubGroups = subGroups.filter(ele => ele.LayoutId === row.id);

		const arr = relatedSubGroups.map((ele: any) => {
			return { label: ele.Name, value: ele.Id };
		});
		return arr.length > 0 ? arr : null;
	};

	let codeNameFields = [
		{
			disabled: true,
			label: formatMessage({ id: 'code' }),
			value: `${selectedRow && selectedRow.row ? selectedRow.row.id : ''}`,
			name: 'optionId',
			type: InputTypes.NUMBER,
			columns: 2
		},
		{
			label: fieldName ? fieldName : '',
			value: selectedRow && selectedRow.row && selectedRow.row.value ? selectedRow.row.value : '',
			name: 'optionName',
			type: InputTypes.TEXT,
			columns: 2
		}
	];

	let editSubGroupFields = [
		...codeNameFields,
		{
			label: formatMessage({ id: 'layouts' }),
			value: selectedRow ? findRelatedLayout(selectedRow.row) : null,
			placeholder: `${formatMessage({ id: 'choosing' })} ${formatMessage({ id: 'subGroups' })}`,
			name: 'select',
			type: InputTypes.SELECT,
			options:
				layouts && layouts.length
					? layouts.map((layout: any) => {
						return { label: layout.Name, value: layout.Id };
					})
					: [],
			columns: 2
		},
		{
			label: formatMessage({ id: 'Service products' }),
			value: selectedRow && selectedRow.row.Is_service === 1 ? true : false,
			name: 'checkbox',
			type: InputTypes.CHECKBOX,
			columns: 2,
			classNames: 'mr-2 ml-2'
		}
	];

	let editLayoutFields = [
		...codeNameFields,
		{
			label: formatMessage({ id: 'subGroups' }),
			isMulti: true,
			value: selectedRow ? findRelatedSubGroups(selectedRow.row) : null,
			placeholder: `${formatMessage({ id: 'choosing' })} ${formatMessage({ id: 'layout' })}`,
			name: 'select',
			type: InputTypes.SELECT,
			options:
				subGroups && subGroups.length
					? subGroups.map((layout: any) => {
						return { label: layout.Name, value: layout.Id };
					})
					: [],
			columns: 4
		}
	];

	let addNewFields = (fieldName: string) => {
		return [
			{
				label: '',
				value: '',
				defaultValue: '',
				placeholder: `${(formatMessage({ id: 'insert' }))} ${formatMessage({ id: 'code' })}`,
				name: 'optionId',
				type: InputTypes.NUMBER,
				columns: 2
			},
			{
				label: '',
				value: '',
				defaultValue: '',
				placeholder: `${(formatMessage({ id: 'insert' }))} ${fieldName}`,
 				name: 'optionName',
				type: InputTypes.TEXT,
				columns: 2
			}
		];
	};

	let addModalFields = (fieldName: string) => {
		let addModal = {
			title: 'addAnotherValue+',
			max: 3,
			base: [...addNewFields(fieldName)],
			fields: []
		};
		return addModal;
	};
	const addModalDefaultField = (fieldName: string) => {
		return [
			{
				label: formatMessage({ id: 'code' }),
				value: '',
				defaultValue: '',
				placeholder: `${(formatMessage({ id: 'insert' }))} ${formatMessage({ id: 'code' })}`,
				name: 'optionId',
				type: InputTypes.NUMBER,
				columns: 2
			},
			{
				label: fieldName ? fieldName : '',
				value: '',
				defaultValue: '',
				placeholder: `${(formatMessage({ id: 'insert' }))} ${fieldName}`,
				name: 'optionName',
				type: InputTypes.TEXT,
				columns: 2
			}
		];
	};

	let drawerTabs = (fields: any, extraFields: any): Array<DrawerTab> => [
		{
			title: '',
			tabId: '1',
			formGroup: [
				{
					formId: '1',
					fields: [...fields],
					extraFields
				}
			]
		}
	];

	const genericForm = (fields: any, extraFields?: any) =>
		drawerTabs([...fields], extraFields).map(({ tabId, formGroup }) =>
			formGroup.map(({ title, formId, fields, additionalFields, extraFields }) => (
				<FormGroup
					key={formId}
					{...{
						title,
						fields: fieldsToReturn ? fieldsToReturn.fields : fields,
						additionalFields,
						extraFields,
						id: tabId,
						formId,
						formGroupListClassName: "m-0",
						onChangeData: (tabId, formId, fields) => {
							console.log(fields)
							setFieldsToReturn(fields);
						}
					}}
				/>
			))
		);

	const modals: ModalsInterface = {
		export: [
			{
				isOpen: modalType === ModalTypes.export,
				toggle: () => toggleModal(ModalTypes.export),
				header: "",
				headerClassNames: 'pb-1 pt-2',
				classNames: 'width-35-per min-width-30-rem max-width-30-rem',
				body: (
					<>
						<p className="font-medium-5 text-bold-700">{formatMessage({ id: 'Which data would you like to download?' })}</p>
						<p className="mb-2 font-medium-2">{formatMessage({ id: 'Select the table you want to download' })}</p>
						<div className="d-flex align-items-center justify-content-center">{groupSelector}</div>
					</>
				),
				footerChilds: [
					selectedGroup && selectedGroup.value && filteredOptions[selectedGroup.value].length > 0 ? (
						<ExportExcel
							filename={selectedGroup.label}
							fields={[
								{ text: 'id', type: 'number' },
								{ text: 'value', type: 'string' }
							]}
							data={filteredOptions[selectedGroup.value]}
							direction={'ltr'}
							translateHeader={true}
							icon={
								<Button
									className="round"
									color="primary"
									onClick={modalHandlers.export}
									disabled={false}
								>
									{formatMessage({ id: 'download' })}
								</Button>
							}
						/>
					) : (
						<Button className="round" color="primary" onClick={() => { }} disabled={true}>
							{formatMessage({ id: 'no data to download' })}
						</Button>
					)
				]
			}
		],
		edit: [
			{
				classNames: 'width-35-per min-width-30-rem max-width-30-rem',
				isOpen: modalType === ModalTypes.edit,
				toggle: () => toggleModal(ModalTypes.edit),
				header: formatMessage({ id: 'data editing' }),
				body:
					selectedRow && selectedRow.type === CATALOG_TYPES.LAYOUTS
						? genericForm(editLayoutFields)
						: selectedRow && selectedRow.type === CATALOG_TYPES.SUBGROUPS
							? genericForm(editSubGroupFields)
							: genericForm(codeNameFields),
				buttons: [
					{
						disabled: saveDisabled,
						color: 'primary',
						onClick: () => {
							modalHandlers.edit(fieldsToReturn);
						},
						label: formatMessage({ id: 'updateRowData' })
					}
				]
			}
		],
		delete: [
			{
				classNames: 'width-35-per min-width-30-rem max-width-30-rem',
				isOpen: modalType === ModalTypes.delete,
				toggle: () => toggleModal(ModalTypes.delete),
				header: formatMessage({ id: 'warning' }),
				body: formatMessage({ id: 'deleteRowMsg' }),
				buttons: [
					{ color: 'primary', onClick: modalHandlers.delete, label: formatMessage({ id: 'delete' }) }
					// , { color: 'primary', onClick: toggleDeleteModal, label: 'cancel' }
				]
			}
		],

		add: [
			{
				classNames: 'width-35-per min-width-30-rem max-width-30-rem',
				isOpen: modalType === ModalTypes.add,
				toggle: () => toggleModal(ModalTypes.add),
				header: "",
				headerClassNames: 'pb-1 pt-2',
				body: (
					<div>
						<p className="font-medium-5 text-bold-700">{formatMessage({ id: 'where to add data' })}</p>
						<p className="mb-3 font-medium-2">{formatMessage({ id: 'choose the table to add new data' })}</p>

						<div className="d-flex align-items-center justify-content-center">{groupSelector}</div>
					</div>
				),
				buttons: [
					{
						color: 'primary',
						onClick: handleNextLevel,
						label: formatMessage({ id: 'toAddData' }),
						disabled: !selectedGroup
					}
				]
			},
			{
				classNames: 'd-flex width-35-per min-width-30-rem max-width-30-rem',
				isOpen: modalType === ModalTypes.add,
				toggle: () => toggleModal(ModalTypes.add),
				header: '',
				headerClassNames: 'pb-1 pt-2',
				// back: () => {
				// 	setSelectedGroup(null), handleBackLevel();
				// },
				body: (
					<div>
						<p className="font-medium-5 text-bold-700">{formatMessage({ id: 'add data to' })}{selectedGroup && selectedGroup.label ? selectedGroup.label : ''
						}</p>
						<p className="mb-3 font-medium-2">{formatMessage({ id: 'You can add one or more data at the same time' })}</p>
						{newDrawerData &&
							newDrawerData.map(({ tabId, formGroup }) =>
								formGroup.map(({ title, formId, fields, additionalFields, extraFields }) => (
									<FormGroup
										forceDelete={true}
										key={formId}
										extraFieldsClassName="no-padding no-margin"
										formGroupListClassName="mt-1"
										{...{
											title,
											fields,
											additionalFields,
											extraFields,
											id: tabId,
											formId,
											onChangeData: (tabId, formId, fields) => {
												setFieldsToReturn({ tabId, formId, fields });
											}
										}}
									/>
								))
							)}
						{/* {genericForm([], addModalFields)} */}
					</div>
				),
				buttons: [
					{
						color: 'primary',
						onClick: () => {
							modalHandlers.add(selectedGroup, fieldsToReturn);
						},
						label: formatMessage({ id: 'add' }),
						disabled: !fieldsToReturn
					}
				]
			}
		],
		error: [],
		import: [],
		none: [],
		info: []
	};

	return <GenericModals key={`modal${currentLevel}`} modal={modals[modalType][currentLevel]} />;
};

export default ModalsData;
