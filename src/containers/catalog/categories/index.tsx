import * as React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { RootStore } from '@src/store';
import { Button, Modal, Spinner } from "reactstrap"
import { useIntl } from "react-intl"
import { fetchAndUpdateStore } from '@src/helpers/helpers';
import { config } from '../../../config'
import { Sorting } from '@src/helpers/helpers'

import { setDivisions } from '@src/redux/actions/divisions/divisionsActions'
import { setSeries } from '@src/redux/actions/series/seriesAction'
import { setSubGroups, setGroups } from '@src/redux/actions/groups/groupsAction'
import { setSegments } from '@src/redux/actions/segments/segmentsActions'
import { setModels, setNewLayouts } from '@src/planogram/shared/store/system/data/data.actions';
import { Icon } from '@components';

import { GenericCard } from '@src/components/generic-card';
import { HortzionalScroller } from '@src/components/hortzionalScroller';
// import CategoriesModals from './Categories-Modals';
import { genericRequest, } from './initial-state';
import { deleteRoutes, insertRoutes, CATALOG_TYPES, updateRoutes } from './services/enums';
import InputText from '@src/components/input-text';
import ModalsData from './modalsData';
import { ModalTypes } from '@src/components/GenericModals/interfaces';


export const Categories: React.FunctionComponent = () => {
	const dispatch = useDispatch()

	const user = useSelector((state: RootStore) => state.user)
	const userPermissions = user ? user.permissions : null
	let URL = window.location.pathname
	const userEditFlag = userPermissions && userPermissions[URL] && userPermissions[URL].edit_flag === 1

	const subGroups = useSelector((state: RootStore) => state._subGroups)
	const series = useSelector((state: RootStore) => state._series)
	const divisions = useSelector((state: RootStore) => state._divisions)
	const segments = useSelector((state: RootStore) => state._segments)
	const groups = useSelector((state: RootStore) => state._groups)
	const models = useSelector((state: RootStore) => state._models)
	const layouts = useSelector((state: RootStore) => state._layouts)

	const [isLoading, setIsLoading] = React.useState<Boolean>(true)
	const [isDataReady, setIsDataReady] = React.useState<boolean>(false)


	const { formatMessage } = useIntl();
	document.title = formatMessage({id: 'Categories'})



	// const [rowToDelete, setRowToDelete] = React.useState<any>(null)
	// const [rowToEdit, setRowToEdit] = React.useState<any>(null)
	const [selectedRow, setSelectedRow] = React.useState<any>(null)
	const [searchInput, setSearchInput] = React.useState<string | null>(null)
	const freeSearchInputRef = React.useRef<any>();

	const [modalType, setModalType] = React.useState<ModalTypes>(ModalTypes.none)
	
	const [newDrawerData,setDrawerData]= React.useState<any>([])

	React.useEffect(() => {
		console.log({newDrawerData})
	}, newDrawerData)

	
	React.useEffect(() => {
		fetchData()
	}, [])

	const fetchData = async () => {
		setIsLoading(true)
		await fetchAndUpdateStore(dispatch, [
			{ state: divisions, type: 'divisions' },
			{ state: groups, type: 'groups' },
			{ state: subGroups, type: 'subGroups' },
			{ state: segments, type: 'segments' },
			{ state: series, type: 'series' },
			{ state: layouts, type: 'layouts' },
			{ state: models, type: 'models' },
		])
		setIsLoading(false)
	}



	const toggleModal = (newType: ModalTypes) => {
		modalType === ModalTypes.none ? setModalType(newType) : setModalType(ModalTypes.none)
	}


	const addField = (state: any) => [...state].map(ele => {
		return {
			id: ele.Id,
			value: ele.Name,
			Name_ID: `${ele.Id} ${ele.Name}`,
			Is_service: ele.Is_service ? ele.Is_service : null
		}
	})

	const returnFilteredState = (id: number, state: any) => [...state].filter(ele => ele.Id !== id);

	const handleDelete = async () => {
		switch (selectedRow.type) {
			case CATALOG_TYPES.DIVISIONS:
				await genericRequest(deleteRoutes.divison, { Id: selectedRow.row.id })
				dispatch(setDivisions(returnFilteredState(selectedRow.row.id, divisions)))
				break;
			case CATALOG_TYPES.GROUPS:
				await genericRequest(deleteRoutes.group, { Id: selectedRow.row.id })
				dispatch(setGroups(returnFilteredState(selectedRow.row.id, groups)))
				break;
			case CATALOG_TYPES.SUBGROUPS:
				await genericRequest(deleteRoutes.subGroup, { Id: selectedRow.row.id })
				dispatch(setSubGroups(returnFilteredState(selectedRow.row.id, subGroups)))
				break;
			case CATALOG_TYPES.LAYOUTS:
				await genericRequest(deleteRoutes.layout, { Id: selectedRow.row.id })
				dispatch(setNewLayouts(returnFilteredState(selectedRow.row.id, layouts)))
				break;
			case CATALOG_TYPES.MODELS:
				await genericRequest(deleteRoutes.model, { Id: selectedRow.row.id })
				dispatch(setModels(returnFilteredState(selectedRow.row.id, models)))
				break;
			case CATALOG_TYPES.SEGMENTS:
				await genericRequest(deleteRoutes.segment, { Id: selectedRow.row.id })
				dispatch(setSegments(returnFilteredState(selectedRow.row.id, segments)))
				break;
			case CATALOG_TYPES.SERIES:
				await genericRequest(deleteRoutes.series, { Id: selectedRow.row.id })
				dispatch(setSeries(returnFilteredState(selectedRow.row.id, series)))
				break;
		}

		setSelectedRow(null)
		toggleModal(ModalTypes.none)
	}


	const handleEdit = async (fields: any) => {
		console.log(fields)
		if (fields) {
			const newName = fields.fields[1].value;

			if (newName) {
				let rowIndex: number;
				let newRows: any[] = [];
				switch (selectedRow.type) {
					case CATALOG_TYPES.DIVISIONS:
						rowIndex = divisions.findIndex((ele) => ele.Id === selectedRow.row.id)
						newRows = [...divisions]
						newRows[rowIndex].Name = newName;
						await genericRequest(updateRoutes.divison, { rec1: newRows[rowIndex] })
						dispatch(setDivisions(newRows))
						break;

					case CATALOG_TYPES.GROUPS:
						rowIndex = groups.findIndex((ele: any) => ele.Id === selectedRow.row.id)
						newRows = [...groups]
						newRows[rowIndex].Name = newName;
						await genericRequest(updateRoutes.group, { rec1: newRows[rowIndex] })
						dispatch(setGroups(newRows))
						break;

					case CATALOG_TYPES.SUBGROUPS:
						rowIndex = subGroups.findIndex((ele: any) => ele.Id === selectedRow.row.id)
						newRows = [...subGroups]
						newRows[rowIndex].LayoutId = fields.fields[2] && fields.fields[2].value && fields.fields[2].value.value ? fields.fields[2].value.value : null;
						newRows[rowIndex].Name = newName;
						newRows[rowIndex].Is_service = fields.fields[3] && fields.fields[3].value === 'true' ? 1 : fields.fields[3] && fields.fields[3].value === 'false' ? 0 : null
						// await AppUpdateSubGroup({ rec1: newRows[rowIndex] })
						await genericRequest(updateRoutes.subGroup, { rec1: newRows[rowIndex] })
						dispatch(setSubGroups(newRows))
						break;

					case CATALOG_TYPES.LAYOUTS:
						rowIndex = layouts.findIndex((ele: any) => ele.Id === selectedRow.row.id)
						newRows = [...layouts]
						newRows[rowIndex].Name = newName;
						const oldSubGroups = fields.fields[2].defaultValue ? fields.fields[2].defaultValue.map((ele: any) => ele.value) : []
						const newSubGroups = fields.fields[2].value ? fields.fields[2].value.map((ele: any) => ele.value) : []
						const newSubGroupsArray = [...subGroups];
						await genericRequest(updateRoutes.layout, { rec1: newRows[rowIndex] })

						oldSubGroups.forEach(async (oldSubGroupID: number) => {
							if (newSubGroups.findIndex((ele: number) => ele === oldSubGroupID) > 0) {
								// console.log(oldSubGroupID, 'exists')
								// case of subgroup already exists .. no need to handle
							} else {
								// console.log(oldSubGroupID, 'delete')
								// case of subgroup need to delete from layout api
								const index = newSubGroupsArray.findIndex(ele => ele.Id === oldSubGroupID)
								newSubGroupsArray[index].LayoutId = null;
								await genericRequest(updateRoutes.subGroup, { rec1: newSubGroupsArray[index] })
							}
						});

						newSubGroups.forEach(async (newSubGroupID: number) => {
							if (oldSubGroups.findIndex((ele: number) => ele === newSubGroupID) > 0) {
								// console.log(newSubGroupID, 'exists')
								// case of subgroup already exists .. no need to handle
							} else {
								// console.log(newSubGroupID, 'add')
								// case of subgroup need to add to layout api
								const index = newSubGroupsArray.findIndex(ele => ele.Id === newSubGroupID)
								newSubGroupsArray[index].LayoutId = selectedRow.row.id;
								await genericRequest(updateRoutes.subGroup, { rec1: newSubGroupsArray[index] })
							}
						});

						dispatch(setSubGroups(newSubGroupsArray))
						dispatch(setNewLayouts(newRows))
						break;

					case CATALOG_TYPES.MODELS:
						rowIndex = models.findIndex((ele: any) => ele.Id === selectedRow.row.id)
						newRows = [...models]
						newRows[rowIndex].Name = newName;
						await genericRequest(updateRoutes.model, { rec1: newRows[rowIndex] })
						dispatch(setModels(newRows))
						break;

					case CATALOG_TYPES.SEGMENTS:
						rowIndex = segments.findIndex((ele: any) => ele.Id === selectedRow.row.id)
						newRows = [...segments]
						newRows[rowIndex].Name = newName;
						await genericRequest(updateRoutes.segment, { rec1: newRows[rowIndex] })
						dispatch(setSegments(newRows))
						break;
					case CATALOG_TYPES.SERIES:
						rowIndex = series.findIndex((ele: any) => ele.Id === selectedRow.row.id)
						newRows = [...series]
						newRows[rowIndex].Name = newName;
						await genericRequest(updateRoutes.series, { rec1: newRows[rowIndex] })
						dispatch(setSeries(newRows))
						break;
				}
			}
		}

		setSelectedRow(null)
		toggleModal(ModalTypes.none)

	}

	let drawerTabs = (fields: any, extraFields: any): any => [
        {
            title: '',
            tabId: '1',
            formGroup: [{
                formId: '1',
                fields: [
                    ...fields
                ],
                extraFields
            },
            ]
        }]

	const getValidateNewRows = (state: any, extraFields: any,fields:any,newDrawer:any) => {
		let addRows = [];
		let newDrawerData = newDrawer
		if(extraFields && extraFields.length) for (let index = 0; index < extraFields.length; index = index + 2) {
			const field1 = extraFields[index].value;
			const field2 = extraFields[index + 1].value;
			newDrawerData.fields.extraFields.fields[index].error = false
			newDrawerData.fields.extraFields.fields[index+1].error = false
			if (field1  && !isNaN(field1)) {
				if(!field2) newDrawerData.fields.extraFields.fields[index+1].error = {label:formatMessage({ id: 'requiredFields' })}
				const idExistsOnState = state.findIndex((ele: any) => ele.Id === parseInt(field1))
				const idExistsNewRows = addRows.findIndex((ele: any) => ele.Id === parseInt(field1))
				if (idExistsOnState === -1 && idExistsNewRows === -1) {
					if(field2){
						const rec = {
							Id: parseInt(field1),
							Name: field2
						}
						addRows.push(rec)
					}else newDrawerData.fields.extraFields.fields[index+1].error = {label:formatMessage({ id: 'idIsTaken' })}
				}else {
					newDrawerData.fields.extraFields.fields[index].error = {label:formatMessage({ id: 'idIsTaken' })}
				}
			}else{
				if(!field1) newDrawerData.fields.extraFields.fields[index].error = {label:formatMessage({ id: 'requiredFields' })}
				if(!field2) newDrawerData.fields.extraFields.fields[index+1].error = {label:formatMessage({ id: 'requiredFields' })}
			}
		}
		for (let index = 0; index < fields.length; index = index + 2) {
			const field1 = fields[index].value;
			const field2 = fields[index + 1].value;
			newDrawerData.fields.fields[index].error = false
			newDrawerData.fields.fields[index+1].error = false
			if (field1  && !isNaN(field1)) {
				if(!field2) newDrawerData.fields.fields[index+1].error = {label:formatMessage({ id: 'requiredFields' })}
				const idExistsOnState = state.findIndex((ele: any) => ele.Id === parseInt(field1))
				const idExistsNewRows = addRows.findIndex((ele: any) => ele.Id === parseInt(field1))
				if (idExistsOnState === -1 && idExistsNewRows === -1) {
					if(field2){
						const rec = {
							Id: parseInt(field1),
							Name: field2
						}
						addRows.push(rec)
					}else newDrawerData.fields.fields[index+1].error = {label:formatMessage({ id: 'idIsTaken' })}
				}else {
					newDrawerData.fields.fields[index].error = {label:formatMessage({ id: 'idIsTaken' })}
				}
			}else{
				if(!field1) newDrawerData.fields.fields[index].error = {label:formatMessage({ id: 'requiredFields' })}
				if(!field2) newDrawerData.fields.fields[index+1].error = {label:formatMessage({ id: 'requiredFields' })}
			}
		}
		return addRows.length == ((extraFields.length/2)+(fields.length/2)) ? addRows : newDrawerData
	}


	const handleAddOption = async (group: any, fields: any) => {
		let newDrawer = fields
		const extraFields = fields.fields.extraFields.fields
		const regularfields = fields.fields.fields
		const groupName = group.value;

		let newRows: any[] = [];
		let addRows: any = [];
		let shouldClose = false
		switch (groupName) {
			case CATALOG_TYPES.DIVISIONS:
				addRows = getValidateNewRows(divisions, extraFields,regularfields,newDrawer)
				if(addRows.tabId) setDrawerData(drawerTabs(addRows.fields.fields,addRows.fields.extraFields)) 
				else{
					shouldClose=true
					newRows = [...divisions, ...addRows]

					await Promise.all(addRows.map(async (row: any) => {
						await genericRequest(insertRoutes.divison, { rec1: row })
					}))
					dispatch(setDivisions(newRows))
				}
				break;
			case CATALOG_TYPES.GROUPS:
				addRows = getValidateNewRows(groups, extraFields,regularfields,newDrawer)
				if(addRows.tabId) setDrawerData(drawerTabs(addRows.fields.fields,addRows.fields.extraFields)) 
				else{
					shouldClose=true
					newRows = [...groups, ...addRows]

					await Promise.all(addRows.map(async (row: any) => {
						await genericRequest(insertRoutes.group, { rec1: row })
					}))
				dispatch(setGroups(newRows))
			}
				break;
			case CATALOG_TYPES.SUBGROUPS:
				addRows = getValidateNewRows(subGroups, extraFields,regularfields,newDrawer)
				if(addRows.tabId) setDrawerData(drawerTabs(addRows.fields.fields,addRows.fields.extraFields)) 
				else{
					shouldClose=true
					newRows = [...subGroups, ...addRows]

					await Promise.all(addRows.map(async (row: any) => {
						await genericRequest(insertRoutes.subGroup, { rec1: row })
					}))
					dispatch(setSubGroups(newRows))
				}
				break;
			case CATALOG_TYPES.LAYOUTS:
				addRows = getValidateNewRows(layouts, extraFields,regularfields,newDrawer)
				if(addRows.tabId) setDrawerData(drawerTabs(addRows.fields.fields,addRows.fields.extraFields)) 
				else{
					shouldClose=true
					newRows = [...layouts, ...addRows]

					await Promise.all(addRows.map(async (row: any) => {
						await genericRequest(insertRoutes.layout, { rec1: row })
					}))
				dispatch(setNewLayouts(newRows))
			}
				break;
			case CATALOG_TYPES.MODELS:
				addRows = getValidateNewRows(models, extraFields,regularfields,newDrawer)
				if(addRows.tabId) setDrawerData(drawerTabs(addRows.fields.fields,addRows.fields.extraFields)) 
				else{
					shouldClose=true
					newRows = [...models, ...addRows]

					await Promise.all(addRows.map(async (row: any) => {
						await genericRequest(insertRoutes.model, { rec1: row })
					}))
				dispatch(setModels(newRows))
			}
				break;
			case CATALOG_TYPES.SEGMENTS:
				addRows = getValidateNewRows(segments, extraFields,regularfields,newDrawer)
				if(addRows.tabId) setDrawerData(drawerTabs(addRows.fields.fields,addRows.fields.extraFields)) 
				else{
					shouldClose=true
					newRows = [...segments, ...addRows]

					await Promise.all(addRows.map(async (row: any) => {
						await genericRequest(insertRoutes.segment, { rec1: row })
					}))
									dispatch(setSegments(newRows))

				}
				break;
			case CATALOG_TYPES.SERIES:
				addRows = getValidateNewRows(series, extraFields,regularfields,newDrawer)
				if(addRows.tabId) setDrawerData(drawerTabs(addRows.fields.fields,addRows.fields.extraFields)) 
				else{
					shouldClose=true
					newRows = [...series, ...addRows]

					await Promise.all(addRows.map(async (row: any) => {
						await genericRequest(insertRoutes.series, { rec1: row })
					}))
					dispatch(setSeries(newRows))

				}
				break;
		}
		if(shouldClose) toggleModal(ModalTypes.none)
	}


	const buttons = [
		{ action: (row: any, type: any) => { setSelectedRow({ row, type }), toggleModal(ModalTypes.edit) }, icon: 'edit-icon', class: 'card__edit' },
		userEditFlag ?{ action: (row: any, type: any) => { setSelectedRow({ row, type }), toggleModal(ModalTypes.delete) }, icon: 'delete-icon', class: 'card__trash' } :null
	]

	const getHeaders = (categoryName: string) => [{ title: formatMessage({ id: 'code' }), field: 'id', fieldType: 'number', classNames: 'mr-2 min-width-3-rem' }
		, { title: categoryName, field: 'value', fieldType: 'string', classNames: 'min-width-5-rem' }]

	const onFilterTextBoxChanged = () => {
		setSearchInput(freeSearchInputRef.current.value)
	}


	const singleTitles = [
		`${formatMessage({ id: 'className' })}`,
		`${formatMessage({ id: 'groupName' })}`,
		`${formatMessage({ id: 'subGroupsName' })}`,
		`${formatMessage({ id: 'seriesName' })}`,
		`${formatMessage({ id: 'layoutName' })}`,
		`${formatMessage({ id: 'segmentName' })}`,
		`${formatMessage({ id: 'brandName' })}`
	]


	// addField(Sorting(divisions, { field: 'id', type: 'number', asc: false, times: 0 })),
	const filteredOptions: any = {
		divisions: searchInput ? addField(divisions).filter(ele => ele.Name_ID.includes(searchInput)) : addField(Sorting(divisions, { field: 'Id', type: 'number', asc: true, times: 0 })),
		groups: searchInput ? addField(groups).filter(ele => ele.Name_ID.includes(searchInput)) : addField(Sorting(groups, { field: 'Id', type: 'number', asc: true, times: 0 })),
		subGroups: searchInput ? addField(subGroups).filter(ele => ele.Name_ID.includes(searchInput)) : addField(Sorting(subGroups, { field: 'Id', type: 'number', asc: true, times: 0 })),
		series: searchInput ? addField(series).filter(ele => ele.Name_ID.includes(searchInput)) : addField(Sorting(series, { field: 'Id', type: 'number', asc: true, times: 0 })),
		layouts: searchInput ? addField(layouts).filter(ele => ele.Name_ID.includes(searchInput)) :addField(Sorting(layouts, { field: 'Id', type: 'number', asc: true, times: 0 })),
		models: searchInput ? addField(models).filter(ele => ele.Name_ID.includes(searchInput)) : addField(Sorting(models, { field: 'Id', type: 'number', asc: true, times: 0 })),
		segments: searchInput ? addField(segments).filter(ele => ele.Name_ID.includes(searchInput)) : addField(Sorting(segments, { field: 'Id', type: 'number', asc: true, times: 0 }))
	}

	const modalHandlers = {
		add: (groups: any, fields: any) => handleAddOption(groups, fields),
		delete: () => handleDelete(),
		edit: (fields: any) => handleEdit(fields),
		export: () => toggleModal(ModalTypes.none)
	};



	return (<>
		<div className='catalog width-75-per'>
			<div className='catalog__title mb-2'>{formatMessage({ id: 'Categories' })}</div>
			<div className="d-flex catalog__AddItemExcel height-2-5-rem justify-content-between mb-3">
				{!isLoading ?
					<div className="d-flex catalog__AddItemExcel height-2-5-rem justify-content-between">
						<div className="d-flex">
							<div className="max-width-15-rem">
								<InputText searchIcon id={'title'} onChange={onFilterTextBoxChanged} placeholder={formatMessage({ id: 'search' })} inputRef={freeSearchInputRef} />
							</div>
							{userEditFlag && (<>
							<Button onClick={() => toggleModal(ModalTypes.add)} className='round mr-05 d-flex align-items-center' color='primary'>{formatMessage({ id: 'newItem' })}</Button>
							{!isLoading && <div onClick={() => toggleModal(ModalTypes.export)} className="d-flex justify-content-center align-items-center ml-05 cursor-pointer">
								<Icon src={config.iconsPath + "table/excel-download.svg"} />
							</div>}
							</>
							)}
						</div>
					</div> : <Spinner />}
			</div>
		</div>


		{!isLoading ?
			<HortzionalScroller childs={
				[
					<GenericCard
						title={formatMessage({ id: 'divisons' })}
						type={CATALOG_TYPES.DIVISIONS}
						options={filteredOptions['divisions']}
						optionsLength={divisions.length}
						allowSorting
						displayId
						searchProp="Name_ID"
						buttons={buttons}
						headers={getHeaders(singleTitles[0])}
					/>
					,
					<GenericCard
						title={formatMessage({ id: 'groups' })}
						type={CATALOG_TYPES.GROUPS}
						options={filteredOptions[CATALOG_TYPES.GROUPS]}
						optionsLength={groups.length}
						allowSorting
						displayId
						searchProp="Name_ID"
						buttons={buttons}
						headers={getHeaders(singleTitles[1])}
					/>,
					<GenericCard
						title={formatMessage({ id: 'subGroups' })}
						type={CATALOG_TYPES.SUBGROUPS}
						options={filteredOptions[CATALOG_TYPES.SUBGROUPS]}
						optionsLength={subGroups.length}
						allowSorting
						displayId
						searchProp="Name_ID"
						buttons={buttons}
						headers={getHeaders(singleTitles[2])}
					/>
					,
					<GenericCard
						title={formatMessage({ id: 'Serieses' })}
						type={CATALOG_TYPES.SERIES}
						options={filteredOptions[CATALOG_TYPES.SERIES]}
						optionsLength={series.length}
						allowSorting
						displayId
						searchProp="Name_ID"
						buttons={buttons}
						headers={getHeaders(singleTitles[3])}
					/>
					,
					<GenericCard
						title={formatMessage({ id: 'layouts' })}
						type={CATALOG_TYPES.LAYOUTS}
						options={filteredOptions[CATALOG_TYPES.LAYOUTS]}
						optionsLength={layouts.length}
						allowSorting
						displayId
						searchProp="Name_ID"
						buttons={buttons}
						headers={getHeaders(singleTitles[4])}
					/>
					,
					<GenericCard
						title={formatMessage({ id: 'segments' })}
						type={CATALOG_TYPES.SEGMENTS}
						options={filteredOptions[CATALOG_TYPES.SEGMENTS]}
						optionsLength={segments.length}
						allowSorting
						displayId
						searchProp="Name_ID"
						buttons={buttons}
						headers={getHeaders(singleTitles[5])}
					/>
					,
					<GenericCard
						title={formatMessage({ id: 'models' })}
						type={CATALOG_TYPES.MODELS}
						options={filteredOptions[CATALOG_TYPES.MODELS]}
						optionsLength={models.length}
						allowSorting
						displayId
						searchProp="Name_ID"
						buttons={buttons}
						headers={getHeaders(singleTitles[6])}
					/>
				]} />

			: null}


		{modalType === ModalTypes.export ||
			modalType === ModalTypes.edit ||
			modalType === ModalTypes.delete ||
			modalType === ModalTypes.add ?
			<ModalsData
			setDrawerData={setDrawerData}
			newDrawerData={newDrawerData}
				modalHandlers={modalHandlers}
				selectedRow={selectedRow}
				filteredOptions={filteredOptions}
				modalType={modalType}
				saveDisabled={!userEditFlag}
				toggleModal={(type: ModalTypes) => toggleModal(type)}
			/> : null}

	</>);
};



export default Categories;
