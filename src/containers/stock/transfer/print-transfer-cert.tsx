

import * as React from "react";
import * as moment from 'moment';
import { FormattedMessage } from "react-intl"

type Props = {
    text?: string,
    data: any
};

export class PrintOrder extends React.PureComponent<Props> {
    private tableHeaders = [
        '#',
        <FormattedMessage id='BarCode' />,
        <FormattedMessage id='name' />,
        <FormattedMessage id='amountOfItems' />,
        <FormattedMessage id='goremIruz' />,
        <FormattedMessage id='amountMaraz' />,
    ];

    constructor(props: Props) {
        super(props);
    }

    public render() {
        return (
            <div id="dinos" className='print__element' style={{ width: '100%', margin: 'auto', direction: 'rtl' }}>
                <div style={{ width: '87%', margin: '2rem auto 2rem auto', display: 'flex', justifyContent: 'space-between' }}>
                    {this.props.data && this.props.data['logo'] &&
                        <img style={{ width: '35%' }} src={'https://grid.algoretail.co.il/assets/images/' + this.props.data.logo} />
                    }
                    <span style={{ color: 'black', fontSize: '1.5rem' }}><FormattedMessage id="IssueDate" />: {moment(this.props.data.transferDate).format('DD/MM/YY HH:mm')}</span>
                </div>
                <div style={{ width: '87%', margin: '2rem auto 2rem auto', display: 'flex', justifyContent: 'center', alignContent: 'center' }}>
                    <span style={{ fontSize: '3rem', color: 'black', fontWeight: 'bolder' }}><FormattedMessage id="Transfer Certificate" />: {this.props.data.certificateNumber}</span>

                </div>
                <div style={{ width: '87%', margin: 'auto auto 2rem auto', display: 'flex', justifyContent: 'space-between' }}>
                    <div style={{ display: 'flex', flexDirection: 'column' }} >
                        <span style={{ fontSize: '1.5rem', color: 'black' }}><span style={{ fontWeight: 'bolder' }}><FormattedMessage id="returnFromBranch" /> :</span> {this.props.data.returnFromBranch}  ({this.props.data.returnFromBranchId})</span>
                        <span style={{ fontSize: '1.5rem', color: 'black' }}><span style={{ fontWeight: 'bolder' }}><FormattedMessage id="returnToBranch" />:</span> {this.props.data.returnToBranch}  ({this.props.data.returnToBranchId})</span>
                    </div>
                </div>
                <table style={{ margin: 'auto', width: '88%' }}>
                    <thead>
                        <tr>
                            {this.tableHeaders.map((title: any) => <td style={{ color: 'black', fontWeight: 'bold', padding: '0.7rem', border: '0.2px solid black' }} className='print__tableHeader' >{title}</td>)}
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.props.data && this.props.data.details && this.props.data.details.map((detail: any, index: number) => {
                                return (
                                    <tr>
                                        <td style={{ color: 'black', padding: '0.7rem', border: '0.2px solid black' }} > {index + 1}</td>
                                        <td style={{ color: 'black', padding: '0.7rem', border: '0.2px solid black' }} > {detail.BarCode}</td>
                                        <td style={{ color: 'black', padding: '0.7rem', border: '0.2px solid black' }} >{detail.BarCodeName} </td>
                                        <td style={{ color: 'black', padding: '0.7rem', border: '0.2px solid black' }} >{detail.Amount ? detail.Amount.toFixed(2) : null} </td>
                                        <td style={{ color: 'black', padding: '0.7rem', border: '0.2px solid black' }} >{detail.PackageSize}</td>
                                        <td style={{ color: 'black', padding: '0.7rem', border: '0.2px solid black' }} >{detail.AmountPackage ? detail.AmountPackage.toFixed(2) : null}</td>

                                    </tr>
                                )
                            })
                        }
                    </tbody>
                    <tfoot>
                        <tr>
                            <td style={{ fontWeight: 'bolder', backgroundColor: 'lightgray', color: 'black', padding: '0.7rem', border: '0.2px solid black' }}></td>
                            <td style={{ fontWeight: 'bolder', backgroundColor: 'lightgray', color: 'black', padding: '0.7rem', border: '0.2px solid black' }}></td>
                            <td style={{ fontWeight: 'bolder', backgroundColor: 'lightgray', color: 'black', padding: '0.7rem', border: '0.2px solid black' }}></td>
                            <td style={{ fontWeight: 'bolder', backgroundColor: 'lightgray', color: 'black', padding: '0.7rem', border: '0.2px solid black' }}><FormattedMessage id="aTotalOf" />: {this.props.data && this.props.data.details && this.props.data.details.map((ele: any) => ele.Amount ? ele.Amount : 0).reduce((a: any, b: any) => a + b).toFixed(2)}</td>
                            <td style={{ fontWeight: 'bolder', backgroundColor: 'lightgray', color: 'black', padding: '0.7rem', border: '0.2px solid black' }}></td>
                            <td style={{ fontWeight: 'bolder', backgroundColor: 'lightgray', color: 'black', padding: '0.7rem', border: '0.2px solid black' }}><FormattedMessage id="aTotalOf" />: {this.props.data && this.props.data.details && this.props.data.details.map((ele: any) => ele.AmountPackage ? ele.AmountPackage : 0).reduce((a: any, b: any) => a + b).toFixed(2)}</td>
                        </tr>
                    </tfoot>
                </table>

                <div style={{ width: '87%', marginTop: '5rem', marginRight: 'auto', marginLeft: 'auto', display: 'flex', justifyContent: 'space-between' }}>
                    <div style={{ display: 'flex', flexDirection: 'column' }} >
                        <span style={{ fontSize: '1.5rem', color: 'black', fontWeight: 'bolder' }}><FormattedMessage id="storeKeeperSignature" />: {this.props.data.returnerName}</span>
                        <img style={{ marginTop: '1rem' }} src={this.props.data.ApplicatorSignature} />
                    </div>

                    <div style={{ display: 'flex', flexDirection: 'column' }} >
                        <span style={{ fontSize: '1.5rem', color: 'black', fontWeight: 'bolder' }}><FormattedMessage id="driverSignature" />: {this.props.data.transfererName}</span>
                        <img style={{ marginTop: '1rem' }} src={this.props.data.TransfererSignature} />
                    </div>
                </div>
            </div >
        );
    }
}