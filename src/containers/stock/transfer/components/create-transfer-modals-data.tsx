import * as React from 'react';

import { useIntl } from "react-intl"
import { GenericModals } from '@src/components/GenericModals/index';
import { ModalInterface, ModalsInterface, ModalTypes } from '@src/components/GenericModals/interfaces';
import InputText from '@src/components/input-text';
import { AgGrid } from '@src/components';
import { SignatureBox } from '@src/components/signature-box/signature-box';

interface Props {
    modalType: ModalTypes,
    toggleModal: (type: ModalTypes) => void;
    modalHandlers: any;
    fromBranch?: string;
    toBranch?: string;
    rows: any;
}

export const CreateTransferModalsData: React.FC<Props> = props => {

    const {
        toggleModal,
        modalHandlers,
        modalType,
        fromBranch,
        toBranch,
        rows
    } = props;


    const { formatMessage } = useIntl();
    const [storeKeeperName, setStoreKeeperName] = React.useState<any>()
    const [storeKeeperSignature, setStoreKeeperSignature] = React.useState<any>()
    const [driverName, setDriverName] = React.useState<any>()
    const [driverSignature, setDriverSignature] = React.useState<any>()
    const storeKeeperInputRef = React.useRef<any>();
    const driverInputRef = React.useRef<any>();
    const fields = [
        { text: "itemDescription", type: 'string', width: window.innerWidth * 0.03, minWidth: 150, },
        { text: "packingFactor", type: 'string', width: window.innerWidth * 0.03, minWidth: 150, },
        { text: "packingFactorQuantity", type: 'string', width: window.innerWidth * 0.03, minWidth: 150, },
        { text: "unitsQuantity", type: 'string', width: window.innerWidth * 0.03, minWidth: 150, }
    ];

    const deleteModal: ModalInterface = {
        isOpen: modalType === ModalTypes.delete, toggle: () => toggleModal(ModalTypes.none), header: formatMessage({ id: 'WarningBeforeDelete' }), body: <p> {formatMessage({ id: 'areYouSureYouWantToDeleteItemFromList' })}</p>,
        buttons: [
            { color: 'primary', onClick: () => toggleModal(ModalTypes.none), label: formatMessage({ id: 'no' }) },
            { color: 'primary', onClick: modalHandlers.delete, label: formatMessage({ id: 'yesDelete' }), outline: true }
        ]
    }

    const [gridHeight, setGridHeight] = React.useState<number>(rows.length >= 10 ? 3.68 + (2.15 * 10) : rows.length >= 2 ? 3.68 + (2.15 * rows.length) : 3.68 + (2.15 * 2))

    const addModal: ModalInterface = {
        isOpen: modalType === ModalTypes.add, toggle: () => toggleModal(ModalTypes.none),
        classNames: 'min-width-50-rem',
        header: <div className="pl-1 pr-1">
            <p className="mr-1 ml-1 mt-1 mb-0-5">{formatMessage({ id: 'confirmTransferCertificate' })}</p>
            <p className="mr-1 ml-1">{formatMessage({ id: 'from-' })} {fromBranch} {formatMessage({ id: 'to-' })} {toBranch}</p>
        </div>,
        body: <>
            <div className="pl-1 pr-1 mb-3">
                <AgGrid
                    defaultSortFieldNum={0}
                    descSort
                    id={'certificateNumber'}
                    gridHeight={`${gridHeight}rem`}
                    translateHeader
                    fields={[...fields]}
                    groups={[]}
                    totalRows={[]}
                    rows={rows}
                    resizable
                    customFieldsDirection={'ltr'}
                    customRowHeight={30}
                />
            </div>

            <div className='d-flex flex-row justify-content-between pl-1'>
                {[
                    {
                        id: 'storeKeeperName',
                        label: formatMessage({ id: 'storeKeeperName' }),
                        state: setStoreKeeperName,
                        value: storeKeeperName,
                        inputRef: storeKeeperInputRef,
                        signatureLabel: formatMessage({ id: 'storeKeeperSignature' }),
                        signatureState: setStoreKeeperSignature
                    },
                    {
                        id: 'driverName',
                        label: formatMessage({ id: 'driverName' }),
                        state: setDriverName,
                        value: driverName,
                        inputRef: driverInputRef,
                        signatureLabel: formatMessage({ id: 'driverSignature' }),
                        signatureState: setDriverSignature
                    }
                ].map((item) => {
                    return (
                        <div className="width-20-rem">
                            <label className="font-medium-1 pl-1 pr-1" htmlFor={item.id}>{item.label}</label>
                            <InputText
                                id={item.id}
                                onChange={(e) => {
                                    if (e.target.value.trim().length === 0 || e.target.value == '') item.state(undefined)
                                    else item.state(e.target.value)
                                }}
                                value={item.value}
                                placeholder={item.label}
                                inputRef={item.inputRef}
                                inputClassNames="pl-1-5 pr-1-5"
                            />

                            <div className="d-flex justify-content-start align-items-start width-100-per pl-05 pr-05">
                                <SignatureBox
                                    onChange={(url: string | null) => item.signatureState(url)}
                                    height={'150px'}
                                    width={'250px'}
                                    label={item.signatureLabel}
                                />
                            </div>
                        </div>
                    )
                })}

            </div>
        </>,
        buttons: [
            {
                color: 'primary',
                onClick: () => modalHandlers.add({ applicatorName: storeKeeperName, applicatorSignature: storeKeeperSignature, transfererName: driverName, transfererSignature: driverSignature }),
                label: formatMessage({ id: 'confirm' }),
                disabled: (storeKeeperName && storeKeeperName.trim().length > 0) &&
                    (driverName && driverName.trim().length > 0) &&
                    (driverSignature && driverSignature.trim().length > 0) &&
                    (storeKeeperSignature && storeKeeperSignature.trim().length > 0) ? false : true
            }
        ]
    }

    return (
        <GenericModals key={`createReturnModal`} modal={modalType === ModalTypes.delete ? deleteModal : addModal} />
    );
};



export default CreateTransferModalsData;
