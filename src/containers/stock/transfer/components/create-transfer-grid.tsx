import * as React from 'react';
import { AgGrid, Icon } from '@components';
import { useIntl } from "react-intl"
import AsyncSelect from "react-select/async";
import { ModalTypes } from '@src/components/GenericModals/interfaces';
import { Col, Button, } from 'reactstrap';
import CreateTransferModalsData from './create-transfer-modals-data';
import { postTransferCertificate } from '../initial-state';
import InputNumber from '@src/components/input-number';
import { config } from '@src/config';

interface Props {
	readonly back: (clearSelections: boolean) => void;
	readonly fromBranch: any;
	readonly toBranch: any;
	readonly catalogItems: any;
}

export const CreateTransferGrid: React.FC<Props> = (props: Props) => {
	const { formatMessage } = useIntl();
	const { back, fromBranch, toBranch, catalogItems } = props;

	const [modalType, setModalType] = React.useState<ModalTypes>(ModalTypes.none)
	const [isDataReady, setIsDataReady] = React.useState<boolean>(true)
	const [chosenProduct, setChosenProduct] = React.useState<any>(null);
	const [chosenProductFull, setChosenProductFull] = React.useState<any>(null);
	const [rows, setRows] = React.useState<any>()
	const [fields, setFields] = React.useState<any>([
		{ text: "itemDescription", type: 'string', width: window.innerWidth * 0.03, minWidth: 175, },
		{ text: "packingFactor", type: 'string', width: window.innerWidth * 0.03, minWidth: 175, },
		{ text: "packingFactorQuantity", type: 'string', width: window.innerWidth * 0.03, minWidth: 175, },
		{ text: "unitsQuantity", type: 'string', width: window.innerWidth * 0.03, minWidth: 175, },
		{
			text: "deleteAction", type: 'string', width: window.innerWidth * 0.001, minWidth: 75, noFilter: true,
			cellRenderer: 'IconRender', renderIcons: [
				{ actions: true },
				{
					type: 'delete', title: 'Delete', onClick: (data: any) => {
						setRowToDelete(data)
						toggleModal(ModalTypes.delete)
					}, icon: 'trash.svg'
				},
			],
		},
	])
	const customizer = { direction: document.getElementsByTagName("html")[0].dir }


	const [unitsNumber, setUnitsNumber] = React.useState<any>()
	const [packingNumber, setPackingNumber] = React.useState<any>()
	const unitsInputRef = React.useRef<any>();
	const packingFactorInputRef = React.useRef<any>();
	const [rowToDelete, setRowToDelete] = React.useState<any>()



	const productStyles = {
		container: (base: any) => ({
			...base,
		}),
		option: (provided: any, state: any) => ({
			...provided,
			"&:hover": {
				backgroundColor: "#31baab"
			},
			color: state.isSelected ? '#ffffff' : '#1e1e20',
			backgroundColor: state.isSelected ? '#027b7a' : '#ffffff',
			borderBottom: '1px dotted lightgray',
			padding: 10,
			textAlign:(/[\u0590-\u05FF]/).test(state.options[0].label) ? 'right' : 'left'
		}),
		placeholder: (provided: any, state: any) => ({
			...provided,
			textAlign: document.getElementsByTagName("html")[0].dir === 'ltr' ? 'left' : 'right',
		}),
	}

	const loadCatalogOptions = (inputValue: string, callback: any) => {
		if (inputValue.length >= 3) {
			callback(filterCatalog(inputValue));
		} else {
			callback()
		}
	};


	const filterCatalog = (inputValue: string) => {
		const regex = /\d/;
		const doesContainNumber = regex.test(inputValue)

		const catalogFiltered = catalogItems.filter((i: any) => {
			return i.Name.includes(inputValue) || String(i.BarCode).includes(inputValue)
		});
		let catalogMap: any[] = []
		catalogFiltered && catalogFiltered[0] ? catalogMap = catalogFiltered.map((option: any, index: number) => {
			return { index, value: option.BarCode, label: `${option.Name} ${option.BarCode}`, name: option.Name }
		}) : []

		if (doesContainNumber) {
			catalogMap.sort((a, b) => a.value - b.value)
		} else {
			catalogMap.sort((a, b) => a.label.length - b.label.length)
		}

		return catalogMap.slice(0, 220)
	};




	const toggleModal = (newType: ModalTypes) => {
		setModalType(newType)
	}

	const handleAddRow = () => {
		setIsDataReady(false)

		const newRow = {
			itemDescription: `${chosenProductFull.BarCode} ${chosenProductFull.Name}`,
			packingFactor: chosenProductFull && chosenProductFull.Ariza && chosenProductFull.Ariza > 0 ? chosenProductFull.Ariza : null,
			packingFactorQuantity: packingNumber,
			unitsQuantity: unitsNumber,
			BarCode: chosenProductFull.BarCode,
			Id: rows ? [...rows].length : [].length
		}

		const newRows = rows ? [...rows] : []
		newRows.push(newRow)

		setRows(newRows)
		setChosenProduct(null)
		setChosenProductFull(null)
		setUnitsNumber(null)
		setPackingNumber(null)
		unitsInputRef.current.value = null;
		packingFactorInputRef.current.value = null;

		setTimeout(() => {
			setIsDataReady(true)
		}, 1);
	}


	const modalHandlers = {
		add: (data: any) => handleAddModal(data),
		delete: () => handleDeleteRow(),
	};


	const handleDeleteRow = async () => {
		setIsDataReady(false)
		const newRows = [...rows.filter((ele: any) => ele.Id !== rowToDelete.Id)]
		setRows(newRows)

		setTimeout(() => {
			setIsDataReady(true)
		}, 1);

		toggleModal(ModalTypes.none)
	}


	const handleAddModal = async (data: any) => {
		const products = rows.map((ele: any) => {
			return {
				BarCode: ele.BarCode,
				Amount: ele.unitsQuantity,
				AmountPackage: ele.packingFactorQuantity,
				PackageSize: ele.packingFactor
			}
		})
		const newObj = {
			...data,
			referenceNumber: null,
			toBranchId: toBranch.value,
			fromBranchId: fromBranch.value,
			products
		}
		// console.log(newObj)
		await postTransferCertificate(newObj)

		toggleModal(ModalTypes.none)
		back(true)
	}


	const handleKeypress = (e: any) => {
		if (e.key === 'Enter') {
			chosenProduct && unitsNumber ? handleAddRow() : null
		}
	};


	return (
		fromBranch && toBranch && catalogItems ?
			<>
				<div onKeyDown={handleKeypress} >
					<Col md="12" sm="12" className="mb-1-5 p-0 font-medium-4 text-bold-700 text-black d-flex">
						<span className={`cursor-pointer mr-05 ${customizer.direction === 'ltr' ? 'rotate-180' : ''}`} onClick={() => back(false)}>➜</span>
						{formatMessage({ id: 'transferGoods' })} - {formatMessage({ id: 'from-' })} {fromBranch && fromBranch.label ? fromBranch.label : ''} {formatMessage({ id: 'to-' })} {toBranch && toBranch.label ? toBranch.label : ''}
					</Col>

					<div className='d-flex justify-content-between' style={{ width: '60rem', height: '39px' }}>
						<div className='d-flex justify-content-start'>
							{[
								{
									name: 'product',
									text: formatMessage({ id: 'catalog__input' }),
									loadOptions: loadCatalogOptions,
									styles: productStyles,
									value: chosenProduct ? chosenProduct : null,
								},
							].map(item => {
								return (
									<div key={item.text} >
										<AsyncSelect
											name={item.name}
											value={item.value}
											isDisabled={catalogItems.length > 0 ? false : true}
											isClearable={true}
											styles={item.styles}
											loadOptions={item.loadOptions}
											onChange={(e: any, actionMeta: any) => {
												if (e) {
													if (actionMeta.action === "select-option") {
														const item = catalogItems.find((ele: any) => ele.BarCode === e.value)
														setChosenProduct(e)
														setChosenProductFull(item)
													}
												} else {
													if (actionMeta.action === "clear") {
														setChosenProduct(null)
														setChosenProductFull(null)
														setUnitsNumber(null)
														setPackingNumber(null)
														unitsInputRef.current.value = null;
														packingFactorInputRef.current.value = null;
													}
												}
											}}
											classNamePrefix='select'
											noOptionsMessage={() => formatMessage({ id: 'trending_productMinimumSearch' })}
											placeholder={item.text}
											className='height-2-5-rem'
										/>
									</div>
								)
							})}

							<div className='ml-1'>
								<InputNumber disabled={chosenProductFull && chosenProductFull.Ariza && chosenProductFull.Ariza > 0 ? false : true}
									id='packingFactor'
									onChange={(e) => {
										if (e.target.value.trim().length === 0 || isNaN(Number(e.target.value))) {
											setPackingNumber(undefined)
											setUnitsNumber(undefined)
											unitsInputRef.current.value = null;
											packingFactorInputRef.current.value = null;
										} else {
											const packageSize = chosenProductFull && chosenProductFull.Ariza && chosenProductFull.Ariza > 0 ? chosenProductFull.Ariza : 1;
											setPackingNumber(Number(e.target.value))
											setUnitsNumber(Number(e.target.value) * packageSize)
										}
									}} value={packingNumber} placeholder={formatMessage({ id: 'packingFactorQuantity' })}
									inputRef={packingFactorInputRef}
									hideFocus={chosenProduct ? false : true}
									inputHeight={'2.6rem'}
									classNames="mr-1"
								/>
							</div>

							<div className='mr-1'>
								<InputNumber disabled={chosenProductFull ? false : true}
									id='units'
									onChange={(e) => {
										if (e.target.value.trim().length === 0 || isNaN(Number(e.target.value))) {
											setPackingNumber(undefined)
											setUnitsNumber(undefined)
											unitsInputRef.current.value = null;
											packingFactorInputRef.current.value = null;
										} else {
											const packageSize = chosenProductFull && chosenProductFull.Ariza && chosenProductFull.Ariza > 0 ? chosenProductFull.Ariza : 1;
											if (chosenProductFull && chosenProductFull.Ariza && chosenProductFull.Ariza > 0) {
												setPackingNumber(Number(e.target.value) / packageSize)
											}
											setUnitsNumber(Number(e.target.value))
										}
									}} value={unitsNumber} placeholder={formatMessage({ id: 'unitsQuantity' })}
									inputRef={unitsInputRef}
									hideFocus={chosenProduct ? false : true}
									inputHeight={'2.6rem'}
									classNames="mr-1"
								/>
							</div>
						</div>

						<Button
							color="primary"
							disabled={(chosenProduct && unitsNumber && (packingNumber || !chosenProductFull.Ariza || chosenProductFull.Ariza === 0)) ? false : true}
							className="ml-1 round text-bold-400 d-flex justify-content-center align-items-center width-9-rem height-2-9-rem cursor-pointer btn-primary "
							onClick={() => handleAddRow()}>
							+ {formatMessage({ id: "addItem" })}
						</Button>
					</div>


					<div className="mt-2 " style={{ width: '60rem' }}>
						<AgGrid
							defaultSortFieldNum={0}
							descSort
							id={'Id'}
							gridHeight={'60vh'}
							translateHeader
							fields={fields}
							groups={[]}
							totalRows={[]}
							rows={rows}
							resizable
							displayLoadingScreen={!isDataReady}
							customFieldsDirection={'ltr'}
						/>

						<div className="d-flex justify-content-end">
							<div className={`d-flex width-8-rem mt-1 ${rows && rows.length > 0 ? 'cursor-pointer' : 'cursor-default'}`} onClick={() => { rows && rows.length > 0 ? toggleModal(ModalTypes.add) : null }}>
								<Icon className="mr-05" src={config.iconsPath + `general/send-icon-${rows && rows.length > 0 ? 'green' : 'gray'}.svg`} />
								<span className={`mr-05 ${rows && rows.length > 0 ? 'text-turquoise' : 'text-gray opacity-07'}`}>{formatMessage({ id: "sendCertificate" })}</span>
							</div>
						</div>
					</div>
				</div>


				{modalType === ModalTypes.add ||
					modalType === ModalTypes.delete ?
					<CreateTransferModalsData
						modalHandlers={modalHandlers}
						modalType={modalType}
						fromBranch={fromBranch.label}
						toBranch={toBranch.label}
						rows={rows}
						toggleModal={(type: ModalTypes) => toggleModal(type)} /> : null}
			</> : null
	);

};


export default CreateTransferGrid;
