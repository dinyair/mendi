
export interface TransferRow {
	certificateNumber: number;
	transferDate: Date;
	returnFromBranch: string;
	returnFromBranchId: number;
	returnToBranch: string;
	returnToBranchId: number;
	SupplierName: string | null;
	SupplierId:number | null;
	amountOfItems: string | null;
	sumOfUnitsOrWeight: string | null;
	transfererName: string;
	returnerName: string;
	logo:string;
	TransfererSignature:string;
	ApplicatorSignature: string;
}

