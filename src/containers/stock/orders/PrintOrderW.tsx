import * as React from 'react';
import * as moment from 'moment';
import { FormattedMessage } from 'react-intl';
import { Button } from 'reactstrap';

interface Props {
	data: any;
}

export const PrintOrderW: React.FC<Props> = (props: Props) => {
	const { data } = props;
console.log({data})
	return (
		<>
			{data ? (
				<div>
					<div className="width-100-per d-flex justify-content-center mt-1">
						<Button
							color="primary"
							disabled={false}
							className="printButtonInOverlay ml-1 round text-bold-400 d-flex justify-content-center align-items-center width-7-rem height-2-rem cursor-pointer btn-primary "
							onClick={() => {
								window.print();
							}}
						>
							<FormattedMessage id="print" />
						</Button>
					</div>
					<div>
						<div
							style={{
								width: '87%',
								margin: '2rem auto 2rem auto',
								display: 'flex',
								justifyContent: 'space-between'
							}}
						>
							{data && data['logo'] && (
								<img style={{ width: '35%' }} src={data['logo']} alt={data.BranchName} />
							)}
							<span style={{ color: 'black', fontSize: '1.3rem' }}>
								<FormattedMessage id="orderDate" /> {moment(data.OrderDate).format('DD/MM/YYYY')}
							</span>
						</div>
						<div
							style={{
								width: '87%',
								margin: 'auto auto 2rem auto',
								display: 'flex',
								justifyContent: 'space-between'
							}}
						>
							<div style={{ display: 'flex', flexDirection: 'column' }}>
								<span style={{ fontSize: '1.5rem', color: 'black', fontWeight: 'bolder' }}>
									<FormattedMessage id="branch" />: {data.BranchName} ({data.BranchId})
								</span>
								<span style={{ fontSize: '1.5rem', color: 'black' }}>
									<FormattedMessage id="Supplier" />: {data.sapakName} ({data.SapakId})
								</span>
							</div>
							<div style={{ display: 'flex', flexDirection: 'column' }}>
								<span style={{ fontSize: '1.5rem', color: 'black', fontWeight: 'bolder' }}>
									<FormattedMessage id="orderNumPrint" /> {data.OrderNum}
								</span>
								<span style={{ fontSize: '1.5rem', color: 'black', fontWeight: 'bolder' }}>
									<FormattedMessage id="supplyDate" /> {moment(data.AspkaDate).format('DD/MM/YYYY')}
								</span>
							</div>
						</div>
						<table style={{ margin: 'auto', width: '88%' }}>
							<thead>
								<tr>
									<td
										style={{
											color: 'black',
											fontWeight: 'bold',
											padding: '0.7rem',
											border: '0.2px solid black'
										}}
										className="print__tableHeader"
									>
										#
									</td>
									<td
										style={{
											color: 'black',
											fontWeight: 'bold',
											padding: '0.7rem',
											border: '0.2px solid black'
										}}
										className="print__tableHeader"
									>
										<FormattedMessage id="BarCode" />
									</td>
									<td
										style={{
											color: 'black',
											fontWeight: 'bold',
											padding: '0.7rem',
											border: '0.2px solid black'
										}}
										className="print__tableHeader"
									>
										<FormattedMessage id="itemName" />
									</td>
									<td
										style={{
											color: 'black',
											fontWeight: 'bold',
											padding: '0.7rem',
											border: '0.2px solid black'
										}}
										className="print__tableHeader"
									>
										<FormattedMessage id="amountMaraz" />
									</td>
									<td
										style={{
											color: 'black',
											fontWeight: 'bold',
											padding: '0.7rem',
											border: '0.2px solid black'
										}}
										className="print__tableHeader"
									>
										<FormattedMessage id="goremIruz" />
									</td>
									<td
										style={{
											color: 'black',
											fontWeight: 'bold',
											padding: '0.7rem',
											border: '0.2px solid black'
										}}
										className="print__tableHeader"
									>
										<FormattedMessage id="singleAmountPrint" />
									</td>
								</tr>
							</thead>
							<tbody>
								{data &&
									data.subOrder &&
									data.subOrder.map((order: any, index: number) => {
										return (
											<tr>
												<td
													style={{
														color: 'black',
														padding: '0.7rem',
														border: '0.2px solid black'
													}}
												>
													{' '}
													{index + 1}
												</td>
												<td
													style={{
														color: 'black',
														padding: '0.7rem',
														border: '0.2px solid black'
													}}
												>
													{' '}
													{order.BarCode}
												</td>
												<td
													style={{
														color: 'black',
														padding: '0.7rem',
														border: '0.2px solid black'
													}}
												>
													{order.productName
														? order.productName
														: order.BarCodeName
														? order.BarCodeName
														: ''}{' '}
												</td>
												<td
													style={{
														color: 'black',
														padding: '0.7rem',
														border: '0.2px solid black'
													}}
												>
													{order.Ariza ? (order.AmountMaraz > 0 ? order.AmountMaraz  : 0) : ''}{' '}
												</td>
												<td
													style={{
														color: 'black',
														padding: '0.7rem',
														border: '0.2px solid black'
													}}
												>
													{order.Ariza}{' '}
												</td>
												<td
													style={{
														color: 'black',
														padding: '0.7rem',
														border: '0.2px solid black'
													}}
												>
													{order.AmountOrder}
												</td>
											</tr>
										);
									})}
							</tbody>
							<tfoot>
								<tr>
									<td
										style={{
											fontWeight: 'bolder',
											backgroundColor: 'lightgray',
											color: 'black',
											padding: '0.7rem',
											border: '0.2px solid black'
										}}
									></td>
									<td
										style={{
											fontWeight: 'bolder',
											backgroundColor: 'lightgray',
											color: 'black',
											padding: '0.7rem',
											border: '0.2px solid black'
										}}
									>
										<FormattedMessage id="sumOrderPrint" />
									</td>
									<td
										style={{
											fontWeight: 'bolder',
											backgroundColor: 'lightgray',
											color: 'black',
											padding: '0.7rem',
											border: '0.2px solid black'
										}}
									>
										<FormattedMessage id="marazim" />:
									</td>
									<td
										style={{
											fontWeight: 'bolder',
											backgroundColor: 'lightgray',
											color: 'black',
											padding: '0.7rem',
											border: '0.2px solid black'
										}}
									>
										{data && data['marazSum'] ? Number(data['marazSum']).toLocaleString() : 0}
									</td>
									<td
										style={{
											fontWeight: 'bolder',
											backgroundColor: 'lightgray',
											color: 'black',
											padding: '0.7rem',
											border: '0.2px solid black'
										}}
									>
										<FormattedMessage id="singles" />:
									</td>
									<td
										style={{
											fontWeight: 'bolder',
											backgroundColor: 'lightgray',
											color: 'black',
											padding: '0.7rem',
											border: '0.2px solid black'
										}}
									>
										{data && data['singleSum'] ? Number(data['singleSum']).toLocaleString() : 0}
									</td>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			) : null}
		</>
	);
};

export default PrintOrderW;
