import { SET_ORDERS, SEND_TO_DRAFT, SEND_TO_DONE, BACK_TO_START } from './constants';
import { TYPES } from './reducer';

export type Action = {
	currentType: string | string[];
	targetType: string;
	optionId: string;
	type: string;
};

export const setStateOrders = (notDoneOrders: any[],draftOrders:any[], ordersDone: any[]) => ({
	type: SET_ORDERS,
	payload: [notDoneOrders,draftOrders, ordersDone]
	// optionId,
	// currentType,
	// targetType: TYPES.PENDING
})
export const sendToDraft = (optionId: string, currentType: string | string[]): Action => ({
	type: SEND_TO_DRAFT,
	optionId,
	currentType,
	targetType: TYPES.PENDING
})

export const sendToDone = (optionId: string, currentType: string | string[]): Action => ({
	type: SEND_TO_DONE,
	optionId,
	currentType,
	targetType: TYPES.DELETE
})

export const backToStart = (optionId: string, currentType: string): Action => ({
	type: BACK_TO_START,
	optionId,
	currentType,
	targetType: TYPES.NEW
})
