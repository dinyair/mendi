export const SET_ORDERS = 'SET_ORDERS'
export const SEND_TO_DRAFT = 'SEND_TO_DRAFT';
export const SEND_TO_DONE = 'SEND_TO_DONE';
export const BACK_TO_START = 'BACK_TO_START';
export const SET_STOCK_ORDER = "SET_STOCK_ORDER"
