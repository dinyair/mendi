import { Reducer } from 'redux';
import { SET_ORDERS, SEND_TO_DRAFT, SEND_TO_DONE, BACK_TO_START,SET_STOCK_ORDER } from './constants';
import { Action } from './actions';
import { Option } from './interfaces';

const options: Option[] = [];


export const TYPES = {
	NEW: 'new',
	PENDING: 'pending',
	DELETE: 'delete',
}

const defaultState = {
	[TYPES.NEW]: [],
	[TYPES.PENDING]: [],
	[TYPES.DELETE]: [],
};

export type State = {
	[key: string]: Option[];
}

const updateState = (state: State, action: Action) => {
	console.log(action)
	if (Array.isArray(action.currentType)) {
		// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
		// @ts-ignore
		const { option, type } = action.currentType.reduce((acc, curr) => {
			const res = state[curr].find(({ id }) => id === action.optionId);
			return res ? { option: res, type: curr } : acc;
		}, {});

		if (type && type !== action.targetType) {
			return ({
				...state,
				[type]: state[type].filter(({ id }) => id !== action.optionId),
				[action.targetType]: option ? [...state[action.targetType], option] : state[action.targetType]
			})
		}
		return state;
	} else {
		const option = state[action.currentType].find(({ id }) => id === action.optionId);

		return ({
			...state,
			[action.currentType]: state[action.currentType].filter(({ id }) => id !== action.optionId),
			[action.targetType]: option ? [...state[action.targetType], option] : state[action.targetType]
		})
	}
};
export const stockOrders = (state = defaultState, action: any) => {

	switch (action.type) {
		case SET_ORDERS: {
			return { ...state, [TYPES.NEW]: action.payload[0], [TYPES.PENDING]: action.payload[1], [TYPES.DELETE]: action.payload[2] }
		}
		case SEND_TO_DRAFT: {
			return updateState(state, action);
		}
		case SEND_TO_DONE: {
			return updateState(state, action);
		}
		case BACK_TO_START: {
			return updateState(state, action);
		}
		default: {
			return state;
		}
	}
}