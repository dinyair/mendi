import './index.scss';

import * as React from 'react';
import AsyncSelect from 'react-select/async';
import { Spinner } from 'reactstrap';
import { AgGrid } from '@components';
import { RootStore } from '@src/store';
import { useIntl, FormattedMessage } from 'react-intl';
import { Row, Col, Progress, Button } from 'reactstrap';
import { config } from '@src/config';
import { Icon } from '@components';
import { CalendarDoubleInput } from '@components';
import { useSelector } from 'react-redux';
import {
	postAllSuppliersOrders,
	getAllSupppliersForToday,
	getUsersNames,
	deleteOrderByOrderId,
	deleteOrderDateBranchSupply,
	getLogo
} from './initial-state';

import * as moment from 'moment';
import classnames from 'classnames';
import { ModalTypes } from '@src/components/GenericModals/interfaces';
import DeleteModalData from '@src/planogram/modals/deleteModalData';

const PreviousOrders = (props: any) => {
	const { setPrintData } = props;
	const { formatMessage } = useIntl();
	const customizer = { direction: document.getElementsByTagName('html')[0].dir };
	const [loading, setLoading] = React.useState(false);
	const [modalType, setModalType] = React.useState<ModalTypes>(ModalTypes.none);

	const user = useSelector((state: RootStore) => state.user);
	const userPermissions = user ? user.permissions : null;
	let URL = window.location.pathname;
	const userEditFlag = userPermissions && userPermissions[URL] && userPermissions[URL].edit_flag === 1;
	const catalog = useSelector((state: RootStore) => state._catalogUpdated);
	const suppliersRegaular = useSelector((state: RootStore) => state._suppliers);
	const subSuppliers = useSelector((state: RootStore) => state._subSuppliers);
	const userBranches = useSelector((state: RootStore) => state._userBranches);

	const [suppliers, setSuppliers] = React.useState<any>([]);
	const [firstLoad, setFirstLoad] = React.useState<boolean>(true);
	const [mainPage, setMainPage] = React.useState<boolean>(true);
	const [date1, setDate1] = React.useState<any>(new Date());
	const [date2, setDate2] = React.useState<any>(new Date());

	const [inputValue, setInputValue] = React.useState<any>(null);

	const [chosenBranch, setChosenBranch] = React.useState<any>();
	const [chosenSupplier, setChosenSupplier] = React.useState<any>();
	const [chosenStatus, setChosenStatus] = React.useState<any>();

	const [loadSpinner, setLoadSpinner] = React.useState<boolean>(false);
	const [progressBar, setProgressBar] = React.useState<any[]>([]);

	const [chosenOrderNumber, setChosenOrderNumber] = React.useState<any>(0);
	const [chosenBranchName, setChosenBranchName] = React.useState<any>('');
	const [rowToDelete, setRowToDelete] = React.useState<any>(null);

	// React.useEffect(() => { // מבוטל כי הקריאות לדאטא מגיע מדף המלצה להזמנה כרגע
	// 	fetchAndUpdateStore(dispatch, [
	// 		{ state: userBranches, type: 'userBranches' },
	// 		{ state: suppliersRegaular, type: 'suppliers' },
	// 		{ state: subSuppliers, type: 'subSuppliers' }
	// 	])
	// }, [])

	React.useEffect(() => {
		if (
			suppliersRegaular &&
			suppliersRegaular.length &&
			subSuppliers &&
			subSuppliers.length &&
			Array.isArray(subSuppliers)
		) {
			const newSub = subSuppliers
				.filter((v, i, a) => a.findIndex(t => t.SubsapakId === v.SubsapakId) === i)
				.map((item: any) => {
					return { Name: item.SubsapakName, Id: item.SapakId, subId: item.SubsapakId };
				});

			let uniqueSuppliers = [...suppliersRegaular];
			newSub.forEach(subSupplier => {
				uniqueSuppliers = uniqueSuppliers.filter(supplier => supplier.Id !== subSupplier.Id);
			});
			const allSuppliers = [...uniqueSuppliers, ...newSub];

			setSuppliers(allSuppliers);
		}
	}, [suppliersRegaular, subSuppliers]);

	const handleActionClick = (type: string, data: any) => {
		switch (type) {
			case 'delete':
				setRowToDelete(data);
				toggleModal(ModalTypes.delete);
				break;
			case 'print':
				printOrder(data);
				break;
			case 'view':
				prevOrderSubGroup(data);
				break;
		}
	};

	// React.useEffect(() => {
	// 	if(printData && printData.BranchId && activeTab==TABS.PREVIOUS_ORDERS){
	// 		setTimeout(() => {
	// 		window.print()
	// 		setTimeout(function () { setPrintData(null);window.close(); }, 100);
	// 		}, 100);
	// 	}
	// }, [printData]);

	const printOrder = async (data: any) => {
		let logo = await getLogo();
		let marazSum: number = 0;
		let singleSum: number = 0;

		data['logo'] = `https://grid.algoretail.co.il/assets/images/${logo}`;

		let finalData = data;
		finalData.sapakName = finalData.SapakName;

		finalData.subOrder = finalData.Orders
			? finalData.Orders.map((row: any) => {
					let AmountMaraz = row.AmountMaraz;
					let AmountOrder = row.AmountOrder;
					let AmountOrderSub = row.AmountMaraz;

					if (row.userId == null && row.AmountOrder != row.AmountMaraz * row.Ariza) {
						AmountMaraz = row.AmountOrder;
						AmountOrderSub = row.AmountOrder;
						AmountOrder = row.AmountOrder * row.Ariza;
					}
					if (AmountOrder < 1) return null;
					let singleAmount = AmountOrder;
					return { ...row, singleAmount, AmountOrderSub, AmountOrder, AmountMaraz };
			  }).filter((g: any) => g)
			: [];

		finalData.subOrder.forEach((order: any) => {
			marazSum += order.AmountMaraz > 0 ? order.AmountMaraz : 0;
			singleSum += order.AmountOrder;
		});

		data['marazSum'] = marazSum.toFixed(0);
		data['singleSum'] = singleSum.toFixed(0);

		setPrintData(finalData);
	};

	const [rows, setRows] = React.useState<any>();
	const [fields, setFields] = React.useState<any>([
		{
			text: 'OrderDate',
			type: 'DateTime',
			filter: 'agDateColumnFilter',
			width: window.innerWidth * 0.01,
			minWidth: 20
		},
		{
			text: 'AspakaDate',
			type: 'FullDate',
			filter: 'agDateColumnFilter',
			width: window.innerWidth * 0.01,
			minWidth: 20
		},
		{ text: 'OrderNum', type: 'string', width: window.innerWidth * 0.04, minWidth: 20 },
		{
			text: 'SapakName',
			type: 'string',
			width: window.innerWidth * 0.04,
			minWidth: 20,
			filter: 'agMultiColumnFilter',
			filterParams: {
				filters: [
					{ filter: 'agTextColumnFilter' },
					{ filter: 'agSetColumnFilter', filterParams: { suppressMiniFilter: true } }
				]
			}
		},
		{
			text: 'BranchName',
			type: 'string',
			width: window.innerWidth * 0.04,
			minWidth: 20,
			filter: 'agMultiColumnFilter',
			filterParams: {
				filters: [
					{ filter: 'agTextColumnFilter' },
					{ filter: 'agSetColumnFilter', filterParams: { suppressMiniFilter: true } }
				]
			}
		},
		{
			text: 'AmountOrder',
			type: 'string',
			width: window.innerWidth * 0.01,
			minWidth: 18
		},
		{
			text: 'status',
			type: 'string',
			width: window.innerWidth * 0.01,
			minWidth: 18,
			noFilter: true,
			cellRenderer: 'IconRender',
			renderIcons: [
				{ status: true },
				{ type: 'done', title: 'done', icon: 'view.svg' },
				{ type: 'notDone', title: 'not done', icon: 'print.svg' },
				{ type: 'deleted', title: 'deleted', icon: 'trash.svg' },
				{ type: 'draft', title: 'draft', icon: 'trash.svg' }
			]
		},
		{
			text: 'OrderedBy',
			type: 'string',
			width: window.innerWidth * 0.01,
			minWidth: 18
		},
		{
			text: 'actions',
			type: 'string',
			width: window.innerWidth * 0.04,
			minWidth: 18,
			noFilter: true,
			cellRenderer: 'IconRender',
			renderIcons: [
				{ actions: true, onlyViewForDeleted: true },
				{
					type: 'view',
					title: 'View',
					onClick: (data: any) => handleActionClick('view', data),
					icon: 'view.svg'
				},
				{
					type: 'print',
					title: 'Print',
					onClick: (data: any) => handleActionClick('print', data),
					icon: 'print.svg'
				},
				userEditFlag
					? {
							type: 'delete',
							title: 'Delete',
							onClick: (data: any) => handleActionClick('delete', data),
							icon: 'trash.svg'
					  }
					: null
			]
		}
	]);

	const [subRows, setSubrows] = React.useState<any>();
	const [subFields, setSubFields] = React.useState<any>([
		{ text: 'BarCode', type: 'number', width: window.innerWidth * 0.01, minWidth: 20 },
		{ text: 'BarCodeName', type: 'string', width: window.innerWidth * 0.01, minWidth: 20 },
		{ text: 'lastyitra', type: 'number', width: window.innerWidth * 0.04, minWidth: 20 },
		{ text: 'newyitra', type: 'number', width: window.innerWidth * 0.04, minWidth: 20 },
		{ text: 'averageDaySum', type: 'number', width: window.innerWidth * 0.04, minWidth: 20 },
		{ text: 'AmountOrderSub', type: 'number', width: window.innerWidth * 0.04, minWidth: 20 },
		{ text: 'Ariza', type: 'number', width: window.innerWidth * 0.04, minWidth: 20 },
		{ text: 'singleAmount', type: 'number', width: window.innerWidth * 0.04, minWidth: 20 }
	]);

	const toggleModal = (newType: ModalTypes) => {
		setModalType(newType);
	};

	const deleteOrder = async () => {
		const data = { ...rowToDelete };
		toggleModal(ModalTypes.none);
		if (data.OrderNum) {
			await deleteOrderByOrderId(data.OrderNum);
		} else {
			let OrderDate = data.OrderDate;
			let SapakId = data.SapakId;
			let BranchId = data.BranchId;
			let rec = { OrderDate, SapakId, BranchId };
			await deleteOrderDateBranchSupply(rec);
		}
		setRows([]);
		setRowToDelete(null);
	};
	const prevOrderSubGroup = (data: any) => {
		setLoadSpinner(true);
		setMainPage(false);
		if (data.BranchName) setChosenBranchName(data.BranchName);

		if (data.OrderNum) {
			setChosenOrderNumber(data.OrderNum);
		} else {
			setChosenOrderNumber(null);
		}
		console.log(data.Orders);
		let subData = data.Orders.map((row: any) => {
			if (row.AmountOrder && row.AmountOrder > 0) {
				let AmountMaraz = row.AmountMaraz;
				let AmountOrder = row.AmountOrder;
				let AmountOrderSub = row.AmountMaraz;

				if (row.userId == null && row.AmountOrder != row.AmountMaraz * row.Ariza) {
					AmountMaraz = row.AmountOrder;
					AmountOrderSub = row.AmountOrder;
					AmountOrder = row.AmountOrder * row.Ariza;
				}
				let singleAmount = AmountOrder;
				return { ...row, singleAmount, AmountOrderSub, AmountOrder, AmountMaraz };
			} else return null;
		}).filter((g: any) => g);

		console.log(subData);
		setSubrows(subData);
		setLoadSpinner(false);
	};

	const filterSuppliers = (inputValue: string) => {
		const suppliersFiltered = suppliers.filter((i: any) => i.Name.includes(inputValue));
		let suppliersMap: any[] = [];
		suppliersFiltered && suppliersFiltered[0]
			? (suppliersMap = suppliersFiltered.map((option: any, index: number) => {
					return { index, value: option.Id, label: option.Name, subValue: option.subId };
			  }))
			: [];
		suppliersMap.sort((a, b) => a.label.length - b.label.length);
		return suppliersMap;
	};
	const handleSupplierInputChange = () => {
		setInputValue(inputValue);
		return inputValue;
	};
	const loadSuppliersOptions = (inputValue: string, callback: any) => {
		if (inputValue.length >= 3) {
			callback(filterSuppliers(inputValue));
		} else {
			callback();
		}
	};

	const filterBranches = (inputValue: string) => {
		const branchesFiltered = userBranches.filter(i => i.Name.includes(inputValue));
		let branchesMap: any[] = [];
		branchesFiltered && branchesFiltered[0]
			? (branchesMap = branchesFiltered.map((option: any, index: number) => {
					// if (option.BranchId === 9999) return null
					return { index, value: option.BranchId, label: option.Name };
			  }))
			: [];
		return branchesMap;
	};

	const loadBranchesOptions = (inputValue: string, callback: any) => {
		callback(filterBranches(inputValue));
	};
	const selectStyle = {
		container: (base: any) => ({
			...base
		}),
		option: (provided: any, state: any) => ({
			...provided,
			...provided,

			'&:hover': {
				backgroundColor: '#31baab'
			},
			color: state.isSelected ? '#ffffff' : '#1e1e20',
			backgroundColor: state.isSelected ? '#027b7a' : '#ffffff',
			borderBottom: '1px dotted lightgray',
			padding: 5
		}),
		control: () => ({
			width: 150,
			height: 50
		})
	};
	const suppliersStyle = {
		container: (base: any) => ({
			...base
		}),
		option: (provided: any, state: any) => ({
			...provided,
			...provided,

			'&:hover': {
				backgroundColor: '#31baab'
			},
			color: state.isSelected ? '#ffffff' : '#1e1e20',
			backgroundColor: state.isSelected ? '#027b7a' : '#ffffff',
			borderBottom: '1px dotted lightgray',
			padding: 5
		}),
		control: () => ({
			width: 250,
			height: 50
		})
	};

	const handleSearch = async () => {
		setLoadSpinner(true);
		let _userBranches: any = {};
		userBranches.forEach((branch: any) => (_userBranches[branch.BranchId] = branch.Name));
		let AspakaDate1: string = moment(date1).format('YYYY-MM-DD');
		let AspakaDate2: string = moment(date2).format('YYYY-MM-DD');
		let BranchIds: any[] =
			chosenBranch && chosenBranch[0]
				? chosenBranch.map((branch: any) => {
						return branch.value;
				  })
				: [];
		const allSuppliersOrders = await postAllSuppliersOrders(AspakaDate1, AspakaDate2, BranchIds);

		const dayNum = new Date().getDay() + 1;
		let suggestedOrders = []

		let today = new Date();
		today.setHours(0, 0, 0, 0);
		let date2Hours = new Date(AspakaDate2)
		date2Hours.setHours(0,0,0,0)
		if(date2Hours>=today){
			suggestedOrders = await getAllSupppliersForToday(dayNum);
		}

		let usersNames = await getUsersNames();
		let userNamesList: any = {};
		if (usersNames && usersNames.length > 0) {
			usersNames.forEach((i: any) => {
				userNamesList[i.id] = i.name;
			});
		}
		let sOrdersFiltered = suggestedOrders.filter((o: any) => {
			let shouldAdd: boolean = false;
			chosenBranch
				? chosenBranch.forEach((cB: any) => {
						cB.value == o.BranchId ? (shouldAdd = true) : null;
				  })
				: _userBranches[o.BranchId]
				? (shouldAdd = true)
				: null;
			return shouldAdd;
		});

		var groupedOrdersMap: any = [];
		var sumOrders: any = [];
		let _suppliers: any = {};
		for (var i = 0; i < suppliers.length; i++) {
			_suppliers[suppliers[i].Id] = suppliers[i].Name;
		}
		var _subSuppliers: any = {};
		for (var i = 0; i < subSuppliers.length; i++) {
			if (_subSuppliers[subSuppliers[i].SubsapakId] == undefined) {
				_subSuppliers[subSuppliers[i].SubsapakId] = 1;
				_suppliers[subSuppliers[i].SubsapakId] = subSuppliers[i].SubsapakName;
			}
		}
		let _branches: any = {};
		for (var i = 0; i < userBranches.length; i++) {
			_branches[userBranches[i].BranchId] = userBranches[i].Name;
		}
		let _catalog: any = {};
		for (var i = 0; i < catalog.length; i++) {
			_catalog[catalog[i].BarCode] = catalog[i].Name;
		}

		for (var index = 0; index < allSuppliersOrders.length; index++) {
			var order: any = allSuppliersOrders[index];
			var key = order.OrderNum + '_' + order.SapakId + '_' + order.BranchId + '_' + order.AspakaDate;
			if (groupedOrdersMap[key] == null) groupedOrdersMap[key] = [];
			groupedOrdersMap[key].push(order);
		}
		for (var key in groupedOrdersMap) {
			var _orders = groupedOrdersMap[key];
			for (var i = 0; i < _orders.length; i++) {
				var order = _orders[i];
				var branchName = _branches[order.BranchId];
				var sapakName = _suppliers[order.SapakId];
				var productName = _catalog[order.BarCode];
				_orders[i].Name = productName ? productName : order.BarCode;
				_orders[i].BarCodeName = productName ? productName : order.BarCode;
				_orders[i].SapakName = sapakName ? sapakName : order.SapakId;
				_orders[i].BranchName = branchName ? branchName : order.BranchId;
			}
			var _sumOrder: any = {
				isDeleted: null,
				BarCode: null,
				OrderNum: null,
				AspakaDate: null,
				OrderDate: null,
				SapakId: null,
				BranchId: null,
				Orders: []
			};
			for (var i = 0; i < _orders.length; i++) {
				var order = _orders[i];
				let orderDate = new Date(order.OrderDate).setHours(0, 0, 0, 0);
				let today = new Date().setHours(0, 0, 0, 0);
				let status = order.OrderNum ? 'done' : today <= orderDate ? 'draft' : 'notdone';
				_sumOrder.status = status;
				_sumOrder.isDeleted = order.delFlag && order.delFlag === 1 ? true : false;
				_sumOrder.BarCode = order.BarCode;
				_sumOrder.OrderNum = order.OrderNum;
				_sumOrder.AspakaDate = order.AspakaDate;
				_sumOrder.OrderDate = order.newts ? order.newts : order.OrderDate;
				_sumOrder.SapakId = order.SapakId;
				_sumOrder.SapakName = order.SapakName;
				_sumOrder.BranchId = order.BranchId;
				_sumOrder.BranchName = order.BranchName;
				_sumOrder.OrderedBy = userNamesList[order.userId] ? userNamesList[order.userId] : '';
				_sumOrder.Orders.push(order);
			}
			sumOrders.push(_sumOrder);
		}

		let _chosenSupplier: any = {};
		if (chosenSupplier && chosenSupplier.length)
			chosenSupplier.forEach((supplier: any) => {
				if (supplier.subValue) _chosenSupplier[supplier.subValue] = supplier.subValue;
				_chosenSupplier[supplier.value] = supplier.value;
			});

		let _chosenStatus: any = {};
		if (chosenStatus && chosenStatus.length)
			chosenStatus.forEach((status: any) => {
				_chosenStatus[status.value] = status.value;
			});

		let sumOrdersFixed = sumOrders
			.map((order: any) => {
				if ((_chosenStatus && _chosenStatus[order.status]) || !chosenStatus) {
					if ((_chosenSupplier && _chosenSupplier[order.SapakId]) || !chosenSupplier) {
						let status: any;

						if (order.OrderNum && !order.isDeleted) {
							status = {};
							status['done'] = '#6ec482';
						} else if (!order.OrderNum && !order.isDeleted) {
							let orderDate = new Date(order.OrderDate).setHours(0, 0, 0, 0);
							let today = new Date().setHours(0, 0, 0, 0);
							if (today <= orderDate) {
								status = {};
								status['draft'] = '#f7c185';
							} else {
								status = {};
								status['notdone'] = '#d03639';
							}
						} else if (!order.OrderNum && !order.isDeleted) {
							status = {};
							status['notdone'] = '#d03639';
						} else if ((!order.OrderNum && order.isDeleted) || (order.OrderNum && order.isDeleted)) {
							status = {};
							status['deleted'] = '#d8d8dc';
						}

						let AmountOrder = order.Orders.filter((g: any) => g.AmountOrder && g.AmountOrder > 0).length;
						return { ...order, AmountOrder, status };
					}
				}
				return '';
			})
			.filter((r: any) => r);

		console.log({ sumOrdersFixed });

		let onlyUserBranches = sumOrdersFixed.filter((o: any) => {
			return _userBranches[o.BranchId] ? true : false;
		});

		let suggestedOrderFinalFilter = sOrdersFiltered.filter((suggestOrder: any) => {
			let add: boolean = true;
			if (suggestOrder.days_order && suggestOrder.days_order > 7 && suggestOrder.lastOrderDate) {
				let today = moment(new Date());
				let lastOrderDateM = moment(new Date(suggestOrder.lastOrderDate));
				let daysDiffrence = today.diff(lastOrderDateM, 'days');
				let isTodayOrderDay = daysDiffrence / suggestOrder.days_order;
				if (Math.floor(isTodayOrderDay) != isTodayOrderDay) add = false;
			}
			(chosenBranch && chosenBranch.length ? sumOrdersFixed : onlyUserBranches).forEach((sumOrder: any) => {
				if (moment(sumOrder.OrderDate).format('YYYY-MM-DD') == moment(new Date()).format('YYYY-MM-DD')) {
					if (suggestOrder.BranchId == sumOrder.BranchId && suggestOrder.Id == sumOrder.SapakId) add = false;
				}
			});
			return add;
		});

		let ordersDone: number = 0;
		let outOfOrders: number = suggestedOrderFinalFilter ? suggestedOrderFinalFilter.length : 0;
		onlyUserBranches.forEach((o: any) => {
			if (o.OrderNum) {
				ordersDone++;
				outOfOrders++;
			} else {
				outOfOrders++;
			}
		});

		setProgressBar([ordersDone, outOfOrders]);
		setFirstLoad(false);
		setRows(chosenBranch && chosenBranch.length ? sumOrdersFixed : onlyUserBranches);
		setTimeout(() => {
			setLoadSpinner(false);
		}, 1);
	};
	const progBar = (
		<div>
			<div className="width-300 ml-1 mt-1">
				<Progress value={(100 / progressBar[1]) * progressBar[0]} />
				<div className="d-flex">
					<span className="text-success">
						{progressBar && progressBar[0]} {formatMessage({ id: 'progress_performed' })}
					</span>
					<span className="ml-03">
						{formatMessage({ id: 'progress_outof' })} {progressBar && progressBar[1]}{' '}
						{formatMessage({ id: 'progress_orders' })}
					</span>
				</div>
			</div>
			<span className="divider-small"></span>
		</div>
	);
	return (
		<div key={'PrevOrderss'}>
			{mainPage ? (
				<div>
					<Row>
						<Col md="9" sm="4" className="d-flex margin-right-auto mainContainer">
							<CalendarDoubleInput
								setDate1={setDate1}
								setDate2={setDate2}
								date2={date2}
								date1={date1}
								top={'-4.6rem'}
							/>
							{[
								{
									name: 'suppliers',
									isMulti: true,
									allowSelectAll: true,
									text: formatMessage({ id: 'stock_suppliers' }),
									loadOptions: loadSuppliersOptions,
									handleInputChange: handleSupplierInputChange,
									defaultOptions: false,
									styles: suppliersStyle,
									defaultValue: chosenSupplier
										? chosenSupplier.map((supplier: any) => {
												return {
													label: supplier.label,
													value: supplier.value,
													index: supplier.index
												};
										  })
										: null
								},
								{
									name: 'branches',
									isMulti: true,
									allowSelectAll: true,
									text: formatMessage({ id: 'stock_branches' }),
									loadOptions: loadBranchesOptions,
									defaultOptions:
										userBranches && userBranches[0]
											? userBranches.map((option: any, index: number) => {
													return { index, value: option.BranchId, label: option.Name };
											  })
											: [],
									styles: selectStyle,
									handleInputChange: null,
									defaultValue: chosenBranch
										? chosenBranch.map((branch: any) => {
												return {
													label: branch.label,
													value: branch.value,
													index: branch.index
												};
										  })
										: null
								},
								{
									name: 'status',
									isMulti: true,
									allowSelectAll: true,
									text: formatMessage({ id: 'Order status' }),
									loadOptions: null,
									defaultOptions: [
										{ value: 'done', label: formatMessage({ id: 'Completed' }) },
										{ value: 'notdone', label: formatMessage({ id: 'notdone' }) },
										{ value: 'draft', label: formatMessage({ id: 'drafted' }) },
										{ value: 'deleted', label: formatMessage({ id: 'deleted' }) }
									],
									styles: selectStyle,
									handleInputChange: null,
									defaultValue: chosenStatus
										? chosenStatus.map((status: any) => {
												return {
													label: status.label,
													value: status.value,
													index: status.index
												};
										  })
										: null
								}
							].map((item: any) => {
								return (
									<AsyncSelect
										hideSelectedOptions={false}
										name={item.name}
										isMulti={item.isMulti}
										allowSelectAll={item.allowSelectAll}
										isDisabled={Boolean(
											!userBranches || !userBranches.length || !suppliers || !suppliers.length
										)}
										isClearable={true}
										closeMenuOnSelect={item.isMulti ? false : true}
										styles={item.styles}
										loadOptions={item.loadOptions}
										defaultOptions={item.defaultOptions}
										onInputChange={item.handleSupplierInputChange}
										className="ml-2"
										classNamePrefix="select"
										defaultValue={item.defaultValue}
										// onKeyDown={(e) => checkIfEnter(e)}
										onChange={(e: any, actionMeta: any) => {
											if (e) {
												if (actionMeta.name === 'branches') {
													setChosenBranch(e);
												} else if (actionMeta.name === 'suppliers') {
													setChosenSupplier(e);
												} else if (actionMeta.name === 'status') {
													setChosenStatus(e);
												}
											} else {
												if (actionMeta.name === 'branches') setChosenBranch(null);
												else if (actionMeta.name === 'suppliers') setChosenSupplier(null);
												else if (actionMeta.name === 'status') setChosenStatus(null);
											}
										}}
										noOptionsMessage={() => formatMessage({ id: 'trending_productMinimumSearch' })}
										placeholder={item.text}
									/>
								);
							})}

							<Button
								color="primary"
								disabled={catalog && catalog.length > 0 && !loadSpinner ? false : true}
								className="ml-1 round text-bold-400 d-flex justify-content-center align-items-center width-7-rem height-2-5-rem cursor-pointer btn-primary "
								onClick={handleSearch}
							>
								<FormattedMessage id="trending_sortButton" />
							</Button>

							{rows && rows.length >= 0 && !loadSpinner && <div className="firstProgBar">{progBar}</div>}
						</Col>
					</Row>

					{rows && rows.length >= 0 && !loadSpinner && <div className="secondProgBar">{progBar}</div>}
					{loadSpinner && firstLoad ? (
						<div className="mt-1">
							<Spinner />
						</div>
					) : null}
					<div className="mt-2 catalogGrid" style={{ width: '75%' }}>
						{firstLoad ? null : (
							<AgGrid
								resizable
								descSort
								defaultSortFieldNum={0}
								deletedFlag={'isDeleted'}
								gridHeight={'60vh'}
								translateHeader
								fields={fields}
								groups={[]}
								totalRows={[]}
								rows={rows}
								checkboxFirstColumn={false}
								pagination
								displayLoadingScreen={loadSpinner}
							/>
						)}

						{
							<DeleteModalData
								modalHandlers={() => {
									deleteOrder();
								}}
								modalType={modalType}
								message={formatMessage({ id: 'areyousure' })}
								toggleModal={(type: ModalTypes) => toggleModal(type)}
							/>
						}
					</div>
				</div>
			) : (
				<div>
					<Row>
						<Col md="3" sm="1" className="margin-right-auto cursor-pointer">
							<div className="margin-right-auto d-flex mr-1">
								<div
									onClick={() => {
										setMainPage(true);
										setLoadSpinner(false);
									}}
								>
									<Icon
										src={config.iconsPath + 'table/arrow-right.svg'}
										className={classnames('backArrow align-self-center', {
											'rotate-180': customizer.direction == 'ltr'
										})}
										style={{ height: '15rem', width: '15rem' }}
									/>
								</div>
								<h3 className="align-self-center ml-05 font-weight-bold">
									{/* {chosenOrderNumber ? ( */}
									<div className="d-flex">
										<span className="textNoWrap">
											{formatMessage({ id: 'branch' })} - {chosenBranchName}
										</span>
										{chosenOrderNumber ? (
											<span className="textNoWrap">
												, {formatMessage({ id: 'order number' })} {chosenOrderNumber}
											</span>
										) : (
											''
										)}
									</div>
									{/* ) : ( */}
									{/* <div>{formatMessage({ id: 'order number' })} 00000000</div> */}
									{/* )} */}
								</h3>
							</div>
						</Col>
					</Row>
					<Row>
						<Col md="3" sm="1" className="ml-3 mt-05 mb-05">
							<span className="font-medium-2">
								{chosenOrderNumber ? (
									<span>{formatMessage({ id: 'orderdone' })} </span>
								) : (
									<span>{formatMessage({ id: 'ordernotdone' })}</span>
								)}
							</span>
						</Col>
					</Row>
					<Row>
						<div className="ml-1" style={{ width: '75%' }}>
							<AgGrid
								resizable
								gridHeight={'60vh'}
								floatFilter
								translateHeader
								fields={subFields}
								groups={[]}
								totalRows={[]}
								rows={subRows}
								checkboxFirstColumn={false}
								freeSearch={{
									rows: subRows,
									fieldsToSearch: ['BarCode', 'BarCodeName'],
									title: formatMessage({ id: 'input_placeholder' })
								}}
								displayLoadingScreen={loadSpinner}
							/>
						</div>
					</Row>
				</div>
			)}
			{/* {printData ? <PrintOrderW data={printData}/> : null} */}
		</div>
	);
};
export default PreviousOrders;
