import * as React from 'react';
import classnames from 'classnames';
import AsyncSelect from 'react-select/async';
import * as moment from 'moment';

import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { TabContent, TabPane, Row, Col, Progress } from 'reactstrap';
import { ArrowRight } from 'react-feather';
import { Tabs, Tab } from '@src/components/tabs';
import { StockCard } from '@src/components/stock-card';
import { TYPES } from './services/reducer';
import { RootStore } from '@src/store';
import { useIntl } from 'react-intl';
import PreviousOrders from './prev-orders';
import { sendToDraft, sendToDone, backToStart, setStateOrders } from './services/actions';
import { getSupppliersForToday, getSuppliersOrders, postAllSuppliersOrders, addOrder } from './initial-state';
import { Spinner } from 'reactstrap';
import { Order } from '../../order/order';
import { AcceptModal } from '../../order/accept-modal';
import { fetchAndUpdateStore } from '@src/helpers/helpers';
import { CompanyActionTypes } from '@src/containers/user/enums';
import { PrintOrderW } from './PrintOrderW';
import { BlankPopUp } from '@components';

export const StockOrders: React.FC = () => {
	const { formatMessage } = useIntl();
	const customizer = { direction: document.getElementsByTagName('html')[0].dir };

	const user = useSelector((state: RootStore) => state.user);
	const userPermissions = user ? user.permissions : null;
	let URL = window.location.pathname;
	const userEditFlag = userPermissions && userPermissions[URL] && userPermissions[URL].edit_flag === 1;

	const TABS = {
		RECOMMENDATION: formatMessage({ id: 'stock_recommendation' }),
		PREVIOUS_ORDERS: formatMessage({ id: 'stock_prev' })
	};

	const history = useHistory();
	const [activeTab, setActiveTab] = React.useState(userEditFlag ? TABS.RECOMMENDATION : TABS.PREVIOUS_ORDERS);
	const [isOpenModal, setStateModal] = React.useState(false);

	document.title = activeTab;
	const [printData, setPrintData] = React.useState<any>(null);
	const [chosenBranch, setChosenBranch] = React.useState<any>(null);
	const [isDataReady, setIsDataReady] = React.useState<boolean>(false);
	const [loadingSpinner, setLoadingSpinner] = React.useState<boolean>(false);
	const stockOrders = useSelector(({ stockOrders }: RootStore) => stockOrders);
	const subSuppliers = useSelector((state: RootStore) => state._subSuppliers);
	const suppliers = useSelector((state: RootStore) => state._suppliers);
	const catalog = useSelector((state: RootStore) => state._catalogUpdated);
	let userBranches: any = useSelector((state: RootStore) => state._userBranches);
	const degems = useSelector((state: RootStore) => state._models);
	const groups = useSelector((state: RootStore) => state._groups);

	const [displayOrdersBySupplier, setDisplayOrdersBySupplier] = React.useState<any>(false);
	const [chosenSupplierId, setChosenSupplierId] = React.useState<number>();
	const [chosenSupplierName, setChosenSupplierName] = React.useState<string>();
	const [typeOfOrder, setTypeOfOrder] = React.useState<string>('def');
	const [isOrderDataReady, setIsOrderDataReady] = React.useState<any>(false);

	const [currentFetchedSupplier, setFetchedSupplier] = React.useState<any>();

	const dispatch = useDispatch();
	const ordersCount = React.useMemo(
		() => stockOrders[TYPES.NEW].length + stockOrders[TYPES.PENDING].length + stockOrders[TYPES.DELETE].length,
		[stockOrders]
	);

	React.useEffect(() => {
		const getData = async () => {
			await fetchAndUpdateStore(dispatch, [
				{ state: userBranches, updateAnyway: true, type: 'userBranches' },
				{ state: catalog, type: 'catalog' },
				{ state: degems, type: 'models' },
				{ state: suppliers, type: 'suppliers' },
				{ state: subSuppliers, type: 'subSuppliers' },
				{ state: groups, type: 'groups' }
			]);
		};
		getData();
	}, []);

	React.useEffect(() => {
		if (isOrderDataReady && currentFetchedSupplier != chosenSupplierId) setIsOrderDataReady(false);
	}, [isOrderDataReady]);

	React.useEffect(() => {
		if (printData && (printData.branchId || printData.BranchId)) {
			document.body.scrollTop = 0; // For Safari
			document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
		}
	}, [printData]);

	React.useEffect(() => {
		if (!displayOrdersBySupplier) setIsOrderDataReady(false);
	}, [displayOrdersBySupplier]);

	React.useEffect(() => {
		if (
			(chosenBranch || (userBranches && userBranches.length === 1)) &&
			catalog &&
			catalog.length &&
			suppliers &&
			suppliers.length &&
			subSuppliers &&
			subSuppliers.length
		) {
			searchOrders();
		}
	}, [chosenBranch, catalog, userBranches, suppliers, subSuppliers]);

	const ordersIsDoneCount = React.useMemo(() => stockOrders[TYPES.DELETE].length, [stockOrders]);

	const setOrders = (notDoneOrders: any[], draftOrders: any[], ordersDone: any[]) => {
		dispatch(setStateOrders(notDoneOrders, draftOrders, ordersDone));
	};

	const onSendDraft = (optionId: string, type: string) => {
		dispatch(sendToDraft(optionId, type));
	};

	// const onSendToDone = async (optionId: any, type: string) => {
	// 	let rec1 = {
	// 		sapakid: optionId,
	// 		orderdate: moment(new Date()).format('YYYY-MM-DD')
	// 	};
	// 	await addOrder(rec1);
	// 	searchOrders();
	// };
	const onBackToStart = (optionId: string, type: string) => {
		dispatch(backToStart(optionId, type));
	};

	const editOrder = (option: any) => {
		setChosenSupplierId(option.SapakId);
		dispatch({
			type: CompanyActionTypes.SET_CHOSEN_SUPPLIER,
			payload: option.SapakId
		});
		setChosenSupplierName(option.value);
		setTypeOfOrder('def');
		setTimeout(() => {
			setDisplayOrdersBySupplier(true);
		}, 50);
	};

	const editOrderDraft = (option: any) => {
		setChosenSupplierId(option.SapakId);
		dispatch({
			type: CompanyActionTypes.SET_CHOSEN_SUPPLIER,
			payload: option.SapakId
		});
		setChosenSupplierName(option.value);
		setTypeOfOrder('draft');
		setTimeout(() => {
			setDisplayOrdersBySupplier(true);
		}, 50);
	};

	const filterBranch = (inputValue: string) => {
		const branchesFiltered = userBranches.filter(i => i.Name.includes(inputValue));
		let branchesMap: any[] = [];
		branchesFiltered && branchesFiltered[0]
			? (branchesMap = branchesFiltered.map((option: any, index: number) => {
					return { index, value: option.BranchId, label: option.Name };
			  }))
			: [];
		return branchesMap;
	};

	const loadBranchOptions = (inputValue: string, callback: any) => {
		callback(filterBranch(inputValue));
	};

	const selectStyle = {
		container: (base: any) => ({
			...base
		}),
		option: (provided: any, state: any) => ({
			...provided,

			'&:hover': {
				color: '#ffffff',
				backgroundColor: '#31baab'
			},
			color: state.isSelected ? '#ffffff' : '#1e1e20',
			backgroundColor: state.isSelected ? '#027b7a' : '#ffffff',
			borderBottom: '1px dotted lightgray',
			padding: 10
		}),
		control: () => ({
			width: 230,
			height: 50
		})
	};

	const searchOrders = async () => {
		setLoadingSpinner(true);
		setIsDataReady(false);

		let branch: number;
		if (userBranches.length === 1) branch = userBranches[0].BranchId;
		else branch = chosenBranch.value;

		const date = moment(new Date()).format('YYYY-MM-DD');
		const dayNum = new Date().getDay() + 1;
		let AspakaDate1: string = moment(new Date()).format('YYYY-MM-DD');
		let AspakaDate2: string = moment(new Date()).format('YYYY-MM-DD');
		let rec: any = { dayNum, branch, date };

		const allSuppliersOrders = await postAllSuppliersOrders(AspakaDate1, AspakaDate2, [branch]);
		let todaySupplies = await getSupppliersForToday(rec);
		let doneOrders = await getSuppliersOrders(rec);

		let _catalog: any = {};
		for (var i = 0; i < catalog.length; i++) {
			_catalog[catalog[i].BarCode] = catalog[i].Name;
		}

		var groupedOrdersMap: any = [];
		var sumOrders: any = [];
		for (var index = 0; index < allSuppliersOrders.length; index++) {
			var order: any = allSuppliersOrders[index];
			var key = order.OrderNum + '_' + order.SapakId + '_' + order.BranchId + '_' + order.AspakaDate;
			if (groupedOrdersMap[key] == null) groupedOrdersMap[key] = [];
			groupedOrdersMap[key].push(order);
		}
		for (var key in groupedOrdersMap) {
			var _orders = groupedOrdersMap[key];
			var _sumOrder: any = {
				isDeleted: null,
				BarCode: null,
				OrderNum: null,
				AspakaDate: null,
				OrderDate: null,
				SapakId: null,
				BranchId: null,
				Orders: []
			};
			for (var i = 0; i < _orders.length; i++) {
				var order = _orders[i];
				let orderDate = new Date(order.OrderDate).setHours(0, 0, 0, 0);
				let today = new Date().setHours(0, 0, 0, 0);
				let status = order.OrderNum ? 'done' : today <= orderDate ? 'draft' : 'notdone';
				_sumOrder.status = status;
				_sumOrder.isDeleted = order.delFlag && order.delFlag === 1 ? true : false;
				_sumOrder.BarCode = order.BarCode;
				_sumOrder.OrderNum = order.OrderNum;
				_sumOrder.AspakaDate = order.AspakaDate;
				_sumOrder.OrderDate = order.OrderDate;
				_sumOrder.SapakId = order.SapakId;
				_sumOrder.SapakName = order.SapakName;
				_sumOrder.BranchId = order.BranchId;
				_sumOrder.BranchName = order.BranchName;
				_sumOrder.Orders.push(order);
			}
			sumOrders.push(_sumOrder);
		}

		let _suppliers: any = {};
		for (var i = 0; i < suppliers.length; i++) {
			_suppliers[suppliers[i].Id] = suppliers[i].Name;
		}
		var _subSuppliers: any = {};
		for (var i = 0; i < subSuppliers.length; i++) {
			if (_subSuppliers[subSuppliers[i].SubsapakId] == undefined) {
				_subSuppliers[subSuppliers[i].SubsapakId] = 1;
				_subSuppliers[subSuppliers[i].SubsapakId] = subSuppliers[i].SubsapakName;
			}
		}
		let ordersToBeMade = todaySupplies && todaySupplies.length ? todaySupplies
			.map((order: any) => {
				let SapakId = order.Id;
				let value = order.Name ? order.Name : _subSuppliers[SapakId];
				let lastOrderDate = order.lastOrderDate;
				let orderFreq = order.days_order;
				let isDone = false;
				if (orderFreq && orderFreq > 7 && lastOrderDate) {
					let today = moment(new Date());
					let lastOrderDateM = moment(new Date(lastOrderDate));
					let daysDiffrence = today.diff(lastOrderDateM, 'days');
					let isTodayOrderDay = daysDiffrence / orderFreq;
					if (Math.floor(isTodayOrderDay) != isTodayOrderDay) isDone = true;
				}
				doneOrders.forEach((doneOrder: any) => {
					if (doneOrder.SapakId === SapakId && doneOrder.real === 1) isDone = true;
				});
				if (isDone) return '';
				else return { id: SapakId, value, SapakId };
			})
			.filter((o: any) => o): [];

		let draftOrders: any[] = [];

		let ordersDone =doneOrders&&doneOrders.length? doneOrders
			.map((doneOrder: any) => {
				let sapakName: any = _suppliers[doneOrder.SapakId]
					? _suppliers[doneOrder.SapakId]
					: _subSuppliers[doneOrder.SapakId];
				let SapakId = doneOrder.SapakId;
				let isOrderDone = true;
				let OrderNum: any;
				let subOrders: any = [];
				let BranchId: any;
				let BranchName: any;
				let branchN: number;
				if (userBranches.length === 1) branchN = userBranches[0].BranchId;
				else branchN = chosenBranch.value;
				userBranches.forEach((branch: any) => {
					if (branch.BranchId === branchN) {
						BranchId = branch.BranchId;
						BranchName = branch.Name;
					}
				})

				sumOrders.forEach((sumOrder: any) => {
					if (sumOrder.SapakId === doneOrder.SapakId && !sumOrder.isDeleted) OrderNum = sumOrder.OrderNum;

					if (sumOrder.SapakId === doneOrder.SapakId && !sumOrder.OrderNum) {
						isOrderDone = false;
						let ordersAmount = sumOrder.Orders.map((i: any) => {
							if (i.AmountOrder > 0) return i;
							else return null;
						}).filter((g: any) => g);

						draftOrders.push({
							...doneOrder,
							sapakName,
							value: sapakName,
							SapakId,
							id: SapakId,
							ordersAmount: ordersAmount.length,
							subOrders: sumOrder.Orders
						});
					}
					if (sumOrder.SapakId === doneOrder.SapakId && sumOrder.OrderNum) {
						subOrders.push(sumOrder.Orders);
					}
				});
				let subOrder =
					subOrders &&
					subOrders[0] &&
					subOrders[0].map((item: any) => {
						let productName = _catalog[item.BarCode];
						return { ...item, productName };
					});
				if (!isOrderDone) return '';
				else
					return {
						...doneOrder,
						sapakName,
						value: sapakName,
						SapakId,
						id: SapakId,
						OrderNum,
						subOrder,
						BranchId,
						BranchName
					};
			})
			.filter((o: any) => o && o.OrderNum)
			.filter((v: any, i: any, a: any) => a.findIndex((t: any) => t.OrderNum === v.OrderNum) === i):[]

		const draftsFiltered = draftOrders && draftOrders.length ? draftOrders.reduce((acc, current) => {
			const x = acc.find((item: any) => item.SapakId === current.SapakId);
			if (!x) {
				return acc.concat([current]);
			} else {
				return acc;
			}
		}, []):[]

		setOrders(ordersToBeMade, draftsFiltered, ordersDone);

		setLoadingSpinner(false);
		setIsDataReady(true);
	};

	return (
		<>
			<Col md="12" sm="12" className="mb-1-5 p-0 font-medium-4 text-bold-700 text-black d-flex">
				{displayOrdersBySupplier && activeTab === formatMessage({ id: 'stock_recommendation' }) ? (
					<span className="d-flex align-items-center">
						<button
							onClick={() => setDisplayOrdersBySupplier(false)}
							className={classnames(
								'back-btn no-outline border-0 bg-white outline-0 d-flex justify-content-center align-items-center',
								{
									'rotate-180': customizer.direction == 'ltr'
								}
							)}
						>
							<ArrowRight size={22} />
						</button>
						<span>
							{formatMessage({ id: 'recomdationOrder' })} {chosenSupplierName ? chosenSupplierName : ''}
						</span>
					</span>
				) : (
					<span>
						{formatMessage({ id: 'azmnot-sapaking' })}{' '}
						{userBranches && userBranches.length === 1 && <span> - {userBranches[0].Name}</span>}
					</span>
				)}
				{displayOrdersBySupplier &&
					isOrderDataReady &&
					activeTab === formatMessage({ id: 'stock_recommendation' }) && (
						<div className="ml-auto mr-3">
							<AcceptModal
								userBranches={userBranches}
								typeOfOrder={typeOfOrder}
								chosenBranch={
									userBranches.length === 1
										? userBranches[0].BranchId
										: chosenBranch
										? chosenBranch.value
										: null
								}
								chosenSupplierId={chosenSupplierId}
								showModal={true}
								searchOrders={searchOrders}
								setDisplayOrdersBySupplier={setDisplayOrdersBySupplier}
							/>
						</div>
					)}
			</Col>
			{!displayOrdersBySupplier && (
				<div className="mb-1-5">
					<Tabs value={userEditFlag ? TABS.RECOMMENDATION : TABS.PREVIOUS_ORDERS} onChangTab={setActiveTab}>
						{userEditFlag && <Tab caption={TABS.RECOMMENDATION} name={TABS.RECOMMENDATION} />}
						<Tab caption={TABS.PREVIOUS_ORDERS} name={TABS.PREVIOUS_ORDERS} />
					</Tabs>
				</div>
			)}

			<TabContent activeTab={activeTab}>
				<TabPane
					// className='overflow-x-hidden'
					tabId={TABS.RECOMMENDATION}
				>
					{!displayOrdersBySupplier ? (
						<div>
							{userBranches && userBranches.length > 1 ? (
								<Row className="mb-2">
									{catalog && catalog.length ? (
										<Col xl="2" lg="12" md="12" sm="8" xs="12">
											<AsyncSelect
												classNamePrefix="select"
												name={formatMessage({ id: 'chooseBranch' })}
												isDisabled={userBranches && userBranches[0] ? false : true}
												isClearable={true}
												styles={selectStyle}
												loadOptions={loadBranchOptions}
												defaultOptions={
													userBranches && userBranches[0]
														? userBranches.map((option: any, index: number) => {
																return {
																	index,
																	value: option.BranchId,
																	label: option.Name
																};
														  })
														: []
												}
												// onKeyDown={(e) => checkIfEnter(e)}
												onChange={(e: any, actionMeta: any) => {
													if (e) {
														// setChosenBranch(null)
														setTimeout(() => {
															setChosenBranch(e);
														}, 50);
													} else {
														setChosenBranch(null);
														setIsDataReady(false);
													}
												}}
												noOptionsMessage={() =>
													formatMessage({ id: 'trending_productMinimumSearch' })
												}
												placeholder={
													chosenBranch
														? chosenBranch.label
														: formatMessage({ id: 'chooseBranch' })
												}
											/>
										</Col>
									) : (
										<Spinner />
									)}

									<Col xl="2" lg="12" md="12" sm="8" xs="12">
										{isDataReady && (
											<div className="width-300 mt-1">
												<Progress value={(100 / ordersCount) * ordersIsDoneCount} />
												<div className="d-flex">
													<span className="text-success progress__text">
														{formatMessage({ id: 'progress_performed' })}{' '}
														{ordersIsDoneCount}
													</span>
													<span className="progress__text ml-03">
														{formatMessage({ id: 'progress_outof' })} {ordersCount}{' '}
														{formatMessage({ id: 'progress_orders' })}
													</span>
												</div>
											</div>
										)}
									</Col>
								</Row>
							) : (
								<div>{userBranches && userBranches.length === 1 ? null : <Spinner />}</div>
							)}
							{userBranches && userBranches.length === 1 && isDataReady ? (
								<div className="width-300 mt-1">
									<Progress value={(100 / ordersCount) * ordersIsDoneCount} />
									<div className="d-flex">
										<span className="text-success progress__text">
											{formatMessage({ id: 'progress_performed' })} {ordersIsDoneCount}
										</span>
										<span className="progress__text ml-03">
											{formatMessage({ id: 'progress_outof' })} {ordersCount}{' '}
											{formatMessage({ id: 'progress_orders' })}
										</span>
									</div>
								</div>
							) : null}

							{isDataReady ? (
								<div>
									<Row className="mb-1">
										<Col xl="4" lg="12" md="12" sm="12">
											<StockCard
												setDisplayOrdersBySupplier={setDisplayOrdersBySupplier}
												chosenSupplierId={chosenSupplierId}
												chosenBranch={
													userBranches.length === 1
														? userBranches[0].BranchId
														: chosenBranch
														? chosenBranch.value
														: null
												}
												searchOrders={searchOrders}
												title={formatMessage({ id: 'stock_orderstoplace' })}
												type={TYPES.NEW}
												options={stockOrders[TYPES.NEW]}
												sendToDraft={onSendDraft}
												sendToDone={() => {}}
												editOrder={editOrder}
											/>
										</Col>
										<Col xl="4" lg="12" md="12" sm="12">
											<StockCard
												setDisplayOrdersBySupplier={setDisplayOrdersBySupplier}
												chosenSupplierId={chosenSupplierId}
												chosenBranch={
													userBranches.length === 1
														? userBranches[0].BranchId
														: chosenBranch
														? chosenBranch.value
														: null
												}
												searchOrders={searchOrders}
												title={formatMessage({ id: 'stock_draftorders' })}
												type={TYPES.PENDING}
												options={stockOrders[TYPES.PENDING]}
												sendToDone={() => {}}
												editOrder={editOrderDraft}
											/>
										</Col>
										<Col xl="4" lg="12" md="12" sm="12">
											<StockCard
												title={formatMessage({ id: 'stock_orderToPlace' })}
												type={TYPES.DELETE}
												searchOrders={searchOrders}
												options={stockOrders[TYPES.DELETE]}
												backToStart={onBackToStart}
												setPrintData={setPrintData}
											/>
										</Col>
									</Row>
								</div>
							) : loadingSpinner ? (
								<Spinner />
							) : null}
						</div>
					) : (
						<Order
							setFetchedSupplier={setFetchedSupplier}
							typeOfOrder={typeOfOrder}
							setIsOrderDataReady={setIsOrderDataReady}
							isOrderDataReady={isOrderDataReady}
							chosenSupplierId={chosenSupplierId}
							chosenBranch={userBranches.length === 1 ? userBranches[0].BranchId : chosenBranch.value}
						/>
					)}
				</TabPane>
				<TabPane tabId={TABS.PREVIOUS_ORDERS}>
					<PreviousOrders setPrintData={setPrintData} />
				</TabPane>
			</TabContent>
			{printData && printData.subOrder ? (
				<BlankPopUp bigX className="print__element_orders" onOutsideClick={setPrintData}>
					<PrintOrderW data={printData} />
				</BlankPopUp>
			) : null}
		</>
	);
};

export default StockOrders;
