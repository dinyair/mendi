import * as React from 'react';
import { Button, Modal, ModalBody, ModalFooter } from 'reactstrap';
import { Icon } from '@components';
import { config } from '@src/config';
import {ModalHeader} from "@src/components/modal";
import Flatpickr from "react-flatpickr";
import Select from "react-select";
import { useIntl, FormattedMessage } from "react-intl";
import './add-order-modal.scss';

interface Props {
	isOpenModal: boolean;
	toggleModal: () => void;
}

type FlatpickrDate = string | Date | number | ReadonlyArray<string | Date | number>;

const options = [
	{ value: "vanilla", label: "Vanilla" },
	{ value: "Dark Chocolate", label: "Dark Chocolate" },
	{ value: "chocolate", label: "Chocolate" },
	{ value: "strawberry", label: "Strawberry" },
	{ value: "salted-caramel", label: "Salted Caramel" }
];

export const AddOrderModal: React.FC<Props> = ({ isOpenModal, toggleModal }) => {
	const { formatMessage } = useIntl();

	const onChangeDateThisOrder = (date: FlatpickrDate) => {
		console.log(date);
	};

	const onChangeDateNextOrder = (date: FlatpickrDate) => {
		console.log(date);
	};

	return (
		<Modal isOpen={isOpenModal} toggle={toggleModal} className='modal-dialog-centered add-order-modal'>
			<ModalHeader onClose={toggleModal}>
				<h3>{formatMessage({ id: 'stock_chooseSupplier' })}</h3>
				<p>{formatMessage({ id: 'stock_suppliersWithOrderNotToday' })}</p>
			</ModalHeader>
			<ModalBody>
				<Select className='add-order-modal__select' options={options} />
				<div className='add-order-modal__date'>
					<Flatpickr className='w-100 mt-1 p-50 form-control bg-white' value={new Date()} onChange={onChangeDateThisOrder} />
					<span className='add-order-modal__date_icon'>
						<Icon src={config.iconsPath+"calander/icons-date.svg"}/>
					</span>
				</div>
				<div className='add-order-modal__date'>
					<Flatpickr className='w-100 mt-1 p-50 form-control bg-white' value={new Date()} onChange={onChangeDateNextOrder} />
					<span className='add-order-modal__date_icon'>
						<Icon src={config.iconsPath+"calander/icons-date.svg"}/>
					</span>
				</div>
			</ModalBody>
			<ModalFooter>
				<Button className='round' color='primary' onClick={toggleModal}>{formatMessage({id:'accept'})}</Button>
			</ModalFooter>
		</Modal>
	);
};

