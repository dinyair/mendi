import { getRequest, postRequest } from "@src/http";

export const getSupppliersForToday = async (rec: any) => {
    const resp = await getRequest<any>(`getBranchSuppliersToday?branchId=${rec.branch}&day=${rec.dayNum}`)
    return resp
}
export const getAllSupppliersForToday = async (dayNum: any) => {
    const resp = await getRequest<any>(`getBranchSuppliersToday?day=${dayNum}`)
    return resp
}
export const getUsersNames = async () => {
    const resp = await getRequest<any>(`manage/users/getUsersNamesOnly`)
    return resp
}
export const getSuppliersOrders = async (rec: any) => {
    const resp = await getRequest<any>(`getSapakOrders?branchId=${rec.branch}&date1=${rec.date}`)
    return resp
}

export const getAllSuppliers = async () => {
    const resp = await getRequest<any>('AppSapakim')
    return resp
}
export const getAllSubSuppliers = async () => {
    const resp = await getRequest<any>('AppGetSubSapak')
    return resp
}
export const postAllSuppliersOrders = async (date1: any, date2: any, branchIds: any[]) => {
    const resp = await postRequest<any>('sapak-orders/all', {
        AspakaDate1: date1,
        AspakaDate2: date2,
        BranchIds: branchIds
    });
    return resp
}
export const deleteOrderByOrderId = async (OrderNum: any) => {
    const resp = await getRequest<any>(`AppDeleteOrderByOrderNum?OrderNum=${OrderNum}`, {});
    return resp
}
export const deleteOrderDateBranchSupply = async (rec: any) => {
    const resp = await getRequest<any>(`AppDeleteOrderByDateSuppBranch?OrderDate=${rec.OrderDate}&SapakId=${rec.SapakId}&BranchId=${rec.BranchId}`, {});
    return resp
}
export const getBranch = async () => {
    const resp = await getRequest<any>(`getBranch`, {});
    return resp
}
export const addOrder = async (rec1: any) => {
    const resp = await postRequest<any>('sapak-orders/all', {
        sapakid: rec1.sapakid,
        orderdate: rec1.orderdate
    });
    return resp
}
export const getLogo = async () => {
    const resp = await getRequest<any>(`getLogo`, {});
    return resp
}