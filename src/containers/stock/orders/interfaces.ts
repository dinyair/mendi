import { SuppliersActionTypes } from './enums';

export interface Action {
	type: SuppliersActionTypes;
	payload: any;
}

