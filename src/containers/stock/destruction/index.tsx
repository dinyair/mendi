import * as React from 'react';
import { TabContent, TabPane, Row, Col, Button, } from 'reactstrap';
import { Tabs, Tab } from '@src/components/tabs';
import { useIntl } from 'react-intl';
import { AgGrid, CalendarDoubleInput, Icon } from '@src/components';
import { getDocumentDetails, getDocuments } from './initial-state';
import * as moment from 'moment';
import { DestructionRow } from './interfaces';
import { useDispatch, useSelector } from 'react-redux';
import { RootStore } from '@src/store';
import { fetchAndUpdateStore } from '@src/helpers/helpers';
import { PrintDestructionCert } from './print-destruction-cert';
import { useReactToPrint } from 'react-to-print';
import { config } from '@src/config';
import AsyncSelect from "react-select/async";
import CreateDestructionGrid from './components/create-destruction-grid';

export const DestructionStock: React.FC = () => {
	const { formatMessage } = useIntl();
	const user = useSelector((state: RootStore) => state.user)
	const userPermissions = user ? user.permissions : null
	let URL = window.location.pathname
	const userEditFlag = userPermissions && userPermissions[URL] && userPermissions[URL].edit_flag === 1
	
	const TABS = {
		MAIN: formatMessage({ id: 'destructionGoods' }),
		CERTIFICATES: formatMessage({ id: 'destructionCertificates' }),
		CREATE_DESCTRUCTION: formatMessage({ id: 'doDestruction' }),

	};

	const [date1, setDate1] = React.useState<any>(new Date())
	const [date2, setDate2] = React.useState<any>(new Date())
	const [activeTab, setActiveTab] = React.useState(userEditFlag ? TABS.MAIN : TABS.CERTIFICATES);
	const [isDataReady, setIsDataReady] = React.useState<boolean>(false)
	const suppliers: any[] = useSelector((state: RootStore) => state._suppliers)
	const branches: any[] = useSelector((state: RootStore) => state._branches)
	const logo: any = useSelector((state: RootStore) => state._logo)
	const catalogFullNoArchive = useSelector((state: RootStore) => state._catalogFullDataNoArchive);

	const [fields, setFields] = React.useState<any>([
		{ text: "certificateNumber", type: 'string', width: window.innerWidth * 0.03, minWidth: 50 },
		{ text: "destructionDate", type: 'DateTime', width: window.innerWidth * 0.03, minWidth: 50 },
		{ text: "BranchName", type: 'string', width: window.innerWidth * 0.03, minWidth: 50 },
		{ text: "SupplierName", type: 'string', width: window.innerWidth * 0.03, minWidth: 50 },
		{ text: "amountOfItems", type: 'string', width: window.innerWidth * 0.03, minWidth: 50, },
		{ text: "sumOfUnitsOrWeight", type: 'string', width: window.innerWidth * 0.03, minWidth: 50, },
		{ text: "destructionerName", type: 'string', width: window.innerWidth * 0.03, minWidth: 50, },
		{
			text: "actions", type: 'string', width: window.innerWidth * 0.001, minWidth: 1, noFilter: true,
			cellRenderer: 'IconRender', renderIcons: [
				{ actions: true },
				{
					type: 'print', title: 'Print', onClick: (data: any) => {
						printDocument(data)
					}, icon: 'print.svg'
				},
			],
		},
	])
	const [rows, setRows] = React.useState<any>()
	document.title = activeTab;
	const dispatch = useDispatch();
	const [printData, setPrintData] = React.useState<any[]>([])
	const componentRef = React.useRef(null);
	const [loading, setLoading] = React.useState(false);
	const onBeforeGetContentResolve = React.useRef<(() => void) | null>(null);

	const [chosenBranch, setChosenBranch] = React.useState<any>()


	React.useEffect(() => {
		fetchRedux()
	}, []);

	const fetchRedux = async () => {
		// try {
		// 	// console.log(window.innerWidth)
		// 	// await document.body.requestFullscreen();
		// 	// await screen.orientation.lock("landscape");
		// } catch (error) {
		// 	console.log(error)
		// }

		if (suppliers && suppliers.length && branches && branches.length && logo && logo.length) {
			fetchData()
		} else {
			await fetchAndUpdateStore(dispatch, [
				{ state: suppliers, type: 'suppliers' },
				{ state: branches, type: 'branches' },
				{ state: logo, type: 'logo' },
				{ state: catalogFullNoArchive, type: 'catalogFullDataNoArchive' },
			])
		}

	}

	React.useEffect(() => {
		if (suppliers && suppliers.length && branches && branches.length && logo && logo.length) {
			fetchData()
		}
	}, [suppliers, branches, logo]);


	const bufferToDataImage = (buffer: any) => {
		if (buffer == null)
			return "";
		var enc = new TextDecoder("utf-8");
		buffer = new Uint8Array(buffer);
		return enc.decode(buffer);
	}


	const fetchData = async () => {
		setIsDataReady(false)
		const documents = await getDocuments(moment(date1).format('YYYY-MM-DD'), moment(date2).format('YYYY-MM-DD'))

		// console.log({ logo })
		// console.log({ branches })
		// console.log({ suppliers })
		// console.log({ documents })

		const rows: any[] = [];
		for (let index = 0; index < documents.length; index++) {
			const document = documents[index];
			// const supplier = suppliers.find((ele: any) => ele.Id === document.SapakId);
			const branch = branches.find((ele: any) => ele.BranchId === document.BranchId);
			let sumOfUnitsOrWeight: string = `${document.SumAmountPackage > 0 ? `${document.SumAmount} (${document.SumAmountPackage.toFixed(2)} ${formatMessage({ id: 'packages' })})` : document.SumAmount}`
			const row: DestructionRow = {
				certificateNumber: document.TrashNum,
				destructionDate: document.CreatedAt,
				BranchId: branch.BranchId,
				SupplierId: null,
				BranchName: branch.Name,
				SupplierName: null,
				amountOfItems: document.CountItems,
				sumOfUnitsOrWeight: sumOfUnitsOrWeight,
				destructionerName: document.ApplicatorName,
				supervisorName: document.SupervisorName,
				SupervisorSignature: bufferToDataImage(document.SupervisorSignature.data),
				ApplicatorSignature: bufferToDataImage(document.ApplicatorSignature.data),
				logo: logo
			}
			rows.push(row)
		}

		setRows(rows)


		setTimeout(() => {
			setIsDataReady(true)
		}, 1000);
	}

	const reactToPrintContent = React.useCallback(() => {
		return componentRef.current;
	}, [componentRef.current]);

	const handleAfterPrint = React.useCallback(() => {
	}, []);

	const handleBeforePrint = React.useCallback(() => {
	}, []);

	const handleOnBeforeGetContent = React.useCallback(() => {
		setLoading(true);
		return new Promise<void>((resolve) => {
			onBeforeGetContentResolve.current = resolve;
			setLoading(false);
			resolve();
		});
	}, [setLoading]);

	const handlePrint = useReactToPrint({
		content: reactToPrintContent,
		documentTitle: activeTab,
		onBeforeGetContent: handleOnBeforeGetContent,
		onBeforePrint: handleBeforePrint,
		onAfterPrint: handleAfterPrint,
		removeAfterPrint: true,
	});

	const printDocument = async (data: any) => {
		const details = await getDocumentDetails(data.certificateNumber)
		let finalData = { ...data, details }

		setPrintData(finalData)
		setTimeout(() => { if (handlePrint) handlePrint() }, 100);
	}

	const loadBranchOptions = (inputValue: string, callback: any) => {
		callback(filterBranch(inputValue));
	}

	const filterBranch = (inputValue: string) => {
		const branchesFiltered = branches.filter((i: any) =>
			i.Name.includes(inputValue)
		);
		let branchesMap: any[] = []
		branchesFiltered && branchesFiltered[0] ? branchesMap = branchesFiltered.map((option: any, index: number) => {
			return { index, value: option.BranchId, label: option.Name }
		}) : []
		return branchesMap
	};

	const selectStyle = {
		container: (base: any) => ({
			...base,
		}),
		option: (provided: any, state: any) => ({
			...provided,
			"&:hover": {
				color: "#ffffff",
				backgroundColor: "#31baab"
			},
			color: state.isSelected ? '#ffffff' : '#1e1e20',
			backgroundColor: state.isSelected ? '#027b7a' : '#ffffff',
			borderBottom: '1px dotted lightgray',
			padding: 10,
			textAlign: (/[\u0590-\u05FF]/).test(state.options[0].label) ? 'right' : 'left'
		}),
		control: () => ({
			width: 200,
			height: 50
		}),
		placeholder: (provided: any, state: any) => ({
			...provided,
			textAlign: document.getElementsByTagName("html")[0].dir === 'ltr' ? 'left' : 'right',
		}),
	}

	return (
		[TABS.MAIN, TABS.CERTIFICATES].includes(activeTab) ?
			<>
				<div className='print__container'>
					<PrintDestructionCert ref={componentRef} data={printData} />
				</div>

				<Col md="12" sm="12" className="mb-1-5 p-0 font-medium-4 text-bold-700 text-black d-flex">
					<span>{activeTab}</span>
				</Col>
				{(
					<div className="mb-1-5">
						<Tabs value={activeTab} onChangTab={setActiveTab}>
							{userEditFlag && (<Tab caption={TABS.MAIN} name={TABS.MAIN} />)}
							<Tab caption={TABS.CERTIFICATES} name={TABS.CERTIFICATES} />
						</Tabs>
					</div>
				)}

				<TabContent activeTab={activeTab}>
					<TabPane tabId={TABS.MAIN}>
						<div className='d-flex justify-content-start' style={{ width: '75%', height: '39px' }}>
							{[
								{
									name: 'Branch',
									loadOptions: loadBranchOptions,
									text: formatMessage({ id: 'chooseBranch' }),
									defaultOptions: branches && branches[0] ? branches.filter((ele) => ele.BranchType == 0 && ele.Id != 9999).map((option: any, index: number) => {
										return { index, value: option.BranchId, label: option.Name, name: 'branch' }
									}) : [],
									defaultValue: chosenBranch,
									styles: selectStyle,
								},
							].map((item, index) => {
								return (
									<div key={item.text} className={index > 0 ? 'ml-1' : ''} >
										<AsyncSelect
											name={item.name}
											isDisabled={suppliers && suppliers.length > 0 && branches && branches.length > 0 ? false : true}
											isClearable={true}
											styles={item.styles}
											defaultValue={item.defaultValue}
											loadOptions={item.loadOptions}
											defaultOptions={item.defaultOptions}
											onChange={(e: any, actionMeta: any) => {
												if (e) {
													if (actionMeta.action === 'select-option' && actionMeta.name === 'Branch') setChosenBranch(e)
												} else {
													if (actionMeta.action === 'clear' && actionMeta.name === 'Branch') setChosenBranch(null);
												}
											}}
											closeMenuOnSelect={true}
											hideSelectedOptions={false}
											classNamePrefix='select'
											noOptionsMessage={() => formatMessage({ id: 'trending_productMinimumSearch' })}
											placeholder={item.text}
										/>
									</div>
								)
							})}

							<Button
								color="primary"
								disabled={chosenBranch && isDataReady ? false : true}
								className="ml-1 round text-bold-400 d-flex justify-content-center align-items-center width-9-rem height-2-5-rem cursor-pointer btn-primary "
								onClick={() => setActiveTab(TABS.CREATE_DESCTRUCTION)}>
								{formatMessage({ id: "doDestruction" })}
							</Button>

							<Icon className="d-none" src={config.iconsPath + `general/send-icon-green.svg`} />
							<Icon className="d-none" src={config.iconsPath + `general/send-icon-gray.svg`} />
						</div>
					</TabPane>


					<TabPane tabId={TABS.CERTIFICATES}>
						<div className='d-flex justify-content-between' style={{ width: '75%', height: '39px' }}>
							<CalendarDoubleInput
								setDate1={setDate1}
								setDate2={setDate2}
								date2={date2}
								date1={date1}
								top={'-4.6rem'}
							/>
							<Button onClick={() => fetchData()} className='round width-9-rem width-11-rem height-2-5-rem d-flex align-items-center justify-content-center' color='primary'>{formatMessage({ id: 'filter' })}</Button>
						</div>



						<div className="mt-2 " style={{ width: '75%' }}>
							<AgGrid
								defaultSortFieldNum={0}
								descSort
								id={'certificateNumber'}
								gridHeight={'60vh'}
								translateHeader
								fields={fields}
								groups={[]}
								totalRows={[]}
								rows={rows}
								pagination
								resizable
								displayLoadingScreen={!isDataReady}
								customFieldsDirection={'ltr'}
							/>
						</div>

					</TabPane>
				</TabContent>
		</> :
			<CreateDestructionGrid back={(clearSelections: boolean) => {
				if (clearSelections) {
					setChosenBranch(null)
					fetchData()
				}
				setActiveTab(TABS.MAIN)
			}} chosenBranch={chosenBranch} catalogItems={catalogFullNoArchive} />
	);
};

export default DestructionStock;
