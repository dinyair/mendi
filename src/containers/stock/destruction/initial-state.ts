import { getRequest, postRequest } from "@src/http";

export const getDocuments = async (date1: any, date2: any) => {
    const resp = await postRequest<any>('tablet-api/trash/documents', {date1,date2});
    return resp
}

export const getDocumentDetails = async (id: number) => {
    const resp = await getRequest<any>('tablet-api/trash/document/'+id, {});
    return resp
}


export const postTrashCertificate = async (obj:any) => {
    const resp = await postRequest<any>('/tablet-api/trash/', {...obj});
    return resp
}
