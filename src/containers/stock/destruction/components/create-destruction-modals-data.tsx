import * as React from 'react';

import { useIntl } from "react-intl"
import { GenericModals } from '@src/components/GenericModals/index';
import { ModalInterface, ModalsInterface, ModalTypes } from '@src/components/GenericModals/interfaces';
import InputText from '@src/components/input-text';
import { AgGrid } from '@src/components';
import { SignatureBox } from '@src/components/signature-box/signature-box';

interface Props {
    modalType: ModalTypes,
    toggleModal: (type: ModalTypes) => void;
    modalHandlers: any;
    branchName?: string;
    rows: any;
}

export const CreateDestructionModalsData: React.FC<Props> = props => {

    const {
        toggleModal,
        modalHandlers,
        modalType,
        branchName,
        rows
    } = props;


    const { formatMessage } = useIntl();
    const [supervisorName, setSupervisorName] = React.useState<any>()
    const [supervisorSignature, setSupervisorSignature] = React.useState<any>()
    const [applicatorName, setApplicatorName] = React.useState<any>()
    const [applicatorSignature, setApplicatorSignature] = React.useState<any>()
    const supervisorInputRef = React.useRef<any>();
    const applicatorInputRef = React.useRef<any>();
    const fields = [
        { text: "itemDescription", type: 'string', width: window.innerWidth * 0.03, minWidth: 150, },
        { text: "packingFactor", type: 'string', width: window.innerWidth * 0.03, minWidth: 150, },
        { text: "packingFactorQuantity", type: 'string', width: window.innerWidth * 0.03, minWidth: 150, },
        { text: "unitsQuantity", type: 'string', width: window.innerWidth * 0.03, minWidth: 150, }
    ];

    const deleteModal: ModalInterface = {
        isOpen: modalType === ModalTypes.delete, toggle: () => toggleModal(ModalTypes.none), header: formatMessage({ id: 'WarningBeforeDelete' }), body: <p> {formatMessage({ id: 'areYouSureYouWantToDeleteItemFromList' })}</p>,
        buttons: [
            { color: 'primary', onClick: () => toggleModal(ModalTypes.none), label: formatMessage({ id: 'no' }) },
            { color: 'primary', onClick: modalHandlers.delete, label: formatMessage({ id: 'yesDelete' }), outline: true }
        ]
    }

    const [gridHeight, setGridHeight] = React.useState<number>(rows.length >= 10 ? 3.68 + (2.15 * 10) : rows.length >= 2 ? 3.68 + (2.15 * rows.length) : 3.68 + (2.15 * 2))

    const addModal: ModalInterface = {
        isOpen: modalType === ModalTypes.add, toggle: () => toggleModal(ModalTypes.none),
        classNames: 'min-width-50-rem',
        header: <div className="pl-1 pr-1">
            <p className="mr-1 ml-1 mt-1 mb-0-5">{formatMessage({ id: 'confirmDestructionCertificate' })}</p>
            <p className="mr-1 ml-1">{branchName}</p>
        </div>,
        body: <>
            <div className="pl-1 pr-1 mb-3">
                <AgGrid
                    defaultSortFieldNum={0}
                    descSort
                    id={'certificateNumber'}
                    gridHeight={`${gridHeight}rem`}
                    translateHeader
                    fields={[...fields]}
                    groups={[]}
                    totalRows={[]}
                    rows={rows}
                    resizable
                    customFieldsDirection={'ltr'}
                    customRowHeight={30}
                />
            </div>

            <div className='d-flex flex-row justify-content-between pl-1'>
                {[
                    {
                        id: 'applicatorName',
                        label: formatMessage({ id: 'applicatorName' }),
                        state: setApplicatorName,
                        value: applicatorName,
                        inputRef: applicatorInputRef,
                        signatureLabel: formatMessage({ id: 'applicatorSignature' }),
                        signatureState: setApplicatorSignature
                    },
                    {
                        id: 'supervisorName',
                        label: formatMessage({ id: 'supervisorName' }),
                        state: setSupervisorName,
                        value: supervisorName,
                        inputRef: supervisorInputRef,
                        signatureLabel: formatMessage({ id: 'superviserSignature' }),
                        signatureState: setSupervisorSignature
                    }
                ].map((item) => {
                    return (
                        <div className="width-20-rem">
                            <label className="font-medium-1 pl-1 pr-1" htmlFor={item.id}>{item.label}</label>
                            <InputText
                                id={item.id}
                                onChange={(e) => {
                                    if (e.target.value.trim().length === 0 || e.target.value == '') item.state(undefined)
                                    else item.state(e.target.value)
                                }}
                                value={item.value}
                                placeholder={item.label}
                                inputRef={item.inputRef}
                                inputClassNames="pl-1-5 pr-1-5"
                            />

                            <div className="d-flex justify-content-start align-items-start width-100-per pl-05 pr-05">
                                <SignatureBox
                                    onChange={(url: string | null) => item.signatureState(url)}
                                    height={'150px'}
                                    width={'250px'}
                                    label={item.signatureLabel}
                                />
                            </div>
                        </div>
                    )
                })}

            </div>
        </>,
        buttons: [
            {
                color: 'primary',
                onClick: () => modalHandlers.add({ supervisorName, supervisorSignature, applicatorName, applicatorSignature }),
                label: formatMessage({ id: 'confirm' }),
                disabled: (supervisorName && supervisorName.trim().length > 0) &&
                    (applicatorName && applicatorName.trim().length > 0) &&
                    (applicatorSignature && applicatorSignature.trim().length > 0) &&
                    (supervisorSignature && supervisorSignature.trim().length > 0) ? false : true
            }
        ]
    }

    return (
        <GenericModals key={`createDestructionModal`} modal={modalType === ModalTypes.delete ? deleteModal : addModal} />
    );
};



export default CreateDestructionModalsData;
