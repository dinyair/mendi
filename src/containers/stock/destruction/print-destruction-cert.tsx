

import * as React from "react";
import * as moment from 'moment';
import { FormattedMessage } from "react-intl"

type Props = {
    data: any
};

export class PrintDestructionCert extends React.PureComponent<Props> {
    private tableHeaders = [
        '#',
        <FormattedMessage id='BarCode' />,
        <FormattedMessage id='name' />,
        <FormattedMessage id='amountOfItems' />,
        <FormattedMessage id='goremIruz' />,
        <FormattedMessage id='amountMaraz' />,
    ];

    constructor(props: Props) {
        super(props);
    }

    public render() {
        return (


            <div className='print__element' style={{ width: '100%', margin: 'auto', direction: 'rtl' }}>
                <div style={{ width: '87%', margin: '2rem auto 2rem auto', display: 'flex', justifyContent: 'space-between' }}>
                    {this.props.data && this.props.data['logo'] &&
                        <img style={{ width: '35%' }} src={'https://grid.algoretail.co.il/assets/images/' + this.props.data.logo} alt={this.props.data.BranchName} />
                    }
                    <span style={{ color: 'black', fontSize: '1.5rem' }}><FormattedMessage id="IssueDate" />: {moment(this.props.data.destructionDate).format('DD/MM/YY HH:mm')}</span>
                </div>
                <div style={{ width: '87%', margin: '2rem auto 2rem auto', display: 'flex', justifyContent: 'center', alignContent: 'center' }}>
                    <span style={{ fontSize: '3rem', color: 'black', fontWeight: 'bolder' }}><FormattedMessage id="Trash Certificate" />: {this.props.data.certificateNumber}</span>

                </div>
                <div style={{ width: '87%', margin: 'auto auto 2rem auto', display: 'flex', justifyContent: 'space-between' }}>
                    <div style={{ display: 'flex', flexDirection: 'column' }} >
                        <span style={{ fontSize: '1.5rem', color: 'black' }}><span style={{ fontWeight: 'bolder' }}><FormattedMessage id="branch" />:</span> {this.props.data.BranchName} ({this.props.data.BranchId})</span>
                        {/* <span style={{ fontSize: '1.5rem', color: 'black' }}><FormattedMessage id="Supplier" />: {this.props.data.SupplierName} ({this.props.data.SupplierId})</span> */}
                    </div>
                </div>


                <table style={{ margin: 'auto', width: '88%' }}>
                    <thead>
                        <tr>
                            {this.tableHeaders.map((title: any) => <td style={{ color: 'black', fontWeight: 'bold', padding: '0.7rem', border: '0.2px solid black' }} className='print__tableHeader' >{title}</td>)}
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.props.data && this.props.data.details && this.props.data.details.map((detail: any, index: number) => {
                                return (
                                    <tr>
                                        <td style={{ color: 'black', padding: '0.7rem', border: '0.2px solid black' }} > {index + 1}</td>
                                        <td style={{ color: 'black', padding: '0.7rem', border: '0.2px solid black' }} > {detail.BarCode}</td>
                                        <td style={{ color: 'black', padding: '0.7rem', border: '0.2px solid black' }} >{detail.BarCodeName} </td>
                                        <td style={{ color: 'black', padding: '0.7rem', border: '0.2px solid black' }} >{detail.Amount.toFixed(2)} </td>
                                        <td style={{ color: 'black', padding: '0.7rem', border: '0.2px solid black' }} >{detail.PackageSize ? detail.PackageSize : ''}</td>
                                        <td style={{ color: 'black', padding: '0.7rem', border: '0.2px solid black' }} >{detail.AmountPackage.toFixed(2)}</td>

                                    </tr>
                                )
                            })
                        }

                        <tr>
                            <td style={{ fontWeight: 'bolder', backgroundColor: 'lightgray', color: 'black', padding: '0.7rem', border: '0.2px solid black' }}></td>
                            <td style={{ fontWeight: 'bolder', backgroundColor: 'lightgray', color: 'black', padding: '0.7rem', border: '0.2px solid black' }}></td>
                            <td style={{ fontWeight: 'bolder', backgroundColor: 'lightgray', color: 'black', padding: '0.7rem', border: '0.2px solid black' }}></td>
                            <td style={{ fontWeight: 'bolder', backgroundColor: 'lightgray', color: 'black', padding: '0.7rem', border: '0.2px solid black' }}><FormattedMessage id="aTotalOf" />: {this.props.data && this.props.data.details && this.props.data.details.map((ele: any) => ele.Amount).reduce((a: any, b: any) => a + b).toFixed(2)}</td>
                            <td style={{ fontWeight: 'bolder', backgroundColor: 'lightgray', color: 'black', padding: '0.7rem', border: '0.2px solid black' }}></td>
                            <td style={{ fontWeight: 'bolder', backgroundColor: 'lightgray', color: 'black', padding: '0.7rem', border: '0.2px solid black' }}><FormattedMessage id="aTotalOf" />: {this.props.data && this.props.data.details && this.props.data.details.map((ele: any) => ele.AmountPackage).reduce((a: any, b: any) => a + b).toFixed(2)}</td>
                        </tr>
                    </tbody>
                </table>

                <div style={{ width: '87%', marginTop: '5rem', marginRight: 'auto', marginLeft: 'auto', display: 'flex', justifyContent: 'space-between' }}>
                    <div style={{ display: 'flex', flexDirection: 'column' }} >
                        <span style={{ fontSize: '1.5rem', color: 'black', fontWeight: 'bolder' }}><FormattedMessage id="applicatorSignature" />: {this.props.data.destructionerName}</span>
                        <img style={{ marginTop: '1rem' }} src={this.props.data.ApplicatorSignature} />
                    </div>

                    <div style={{ display: 'flex', flexDirection: 'column' }} >
                        <span style={{ fontSize: '1.5rem', color: 'black', fontWeight: 'bolder' }}><FormattedMessage id="superviserSignature" />: {this.props.data.supervisorName}</span>
                        <img style={{ marginTop: '1rem' }} src={this.props.data.SupervisorSignature} />
                    </div>
                </div>
            </div >
        );
    }
}