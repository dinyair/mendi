
export interface DestructionRow {
	certificateNumber: number;
	destructionDate: Date;
	BranchName: string;
	SupplierName: string | null;
	SupplierId:number | null;
	BranchId:number;
	amountOfItems: string | null;
	sumOfUnitsOrWeight: string | null;
	destructionerName: string;
	supervisorName: string;
	logo:string;
	SupervisorSignature:string;
	ApplicatorSignature: string;
}