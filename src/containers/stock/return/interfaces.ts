
export interface ReturnRow {
	certificateNumber: number;
	returnDate: Date;
	BranchName: string;
	SupplierName: string;
	SupplierId:number;
	BranchId:number;
	amountOfItems: string | null;
	sumOfUnitsOrWeight: string | null;
	returnerName: string;
	transfererName: string;
	logo:string;
	TransfererSignature:string;
	ApplicatorSignature: string;
}

