import {  getRequest, postRequest } from "@src/http";

export const getDocuments = async (date1: any, date2: any) => {
    const resp = await postRequest<any>('tablet-api/return/documents', {date1,date2});
    return resp
}

export const getDocumentDetails = async (id: number) => {
    const resp = await getRequest<any>('tablet-api/return/document/'+id, {});
    return resp
}

export const postReturnCertificate = async (obj:any) => {
    const resp = await postRequest<any>('/tablet-api/return/', {...obj});
    return resp
}


