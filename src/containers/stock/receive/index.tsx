import * as React from 'react';

import { useDispatch, useSelector } from 'react-redux';
import { TabContent, TabPane, Row, Col, Button, Spinner, } from 'reactstrap';
import { Tabs, Tab } from '@src/components/tabs';

import { useIntl } from 'react-intl';

import { fetchAndUpdateStore } from '@src/helpers/helpers';
import { AgGrid, CalendarDoubleInput, CalendarSingleInput, Icon } from '@src/components';
import { getEdiBranchesByDate, getEdiCatalogItems, getEdiOrdersRecievedRowsByDate, getEdiRecieveRowsByDate, getEdiRefNumberVerify, getEdiRowOrders, getEdiRows, getEdiSuppliersByDate, getRemarks, getSignatureReceivedOrders, getSignatures } from './initial-state';
import * as moment from 'moment';
import { EdiRow, RecieveRow } from './interfaces';
import { RootStore } from '@src/store';
import { PrintReceiveCert } from './print-receive-cert';
import { useReactToPrint } from 'react-to-print';
import AsyncSelect from "react-select/async";
import InputNumber from '@src/components/input-number';
import CreateReceiveGrid from './components/create-receive-grid';
import { selectStyle } from './components/helpers';
import { config } from '@src/config';
import CreateReceiveEDIGrid from './components/create-receive-edi-grid';

export const RecieveStock: React.FC = () => {
	const { formatMessage } = useIntl();
	const user = useSelector((state: RootStore) => state.user)
	const userPermissions = user ? user.permissions : null
	let URL = window.location.pathname
	const userEditFlag = userPermissions && userPermissions[URL] && userPermissions[URL].edit_flag === 1
	
	const TABS = {
		MAIN: formatMessage({ id: 'ReceiveStock' }),
		CERTIFICATES: formatMessage({ id: 'ReceiveCertificates' }),
		CREATE_RECEIVE: formatMessage({ id: 'doReceive' }),
		CREATE_RECEIVE_EDI: formatMessage({ id: 'doReceiveEDI' }),
	};

	const [date1, setDate1] = React.useState<any>(new Date())
	const [date2, setDate2] = React.useState<any>(new Date())
	const [activeTab, setActiveTab] = React.useState(userEditFlag ? TABS.MAIN : TABS.CERTIFICATES);
	const [isDataReady, setIsDataReady] = React.useState<boolean>(false)
	const suppliers: any[] = useSelector((state: RootStore) => state._suppliers)
	const subSuppliers: any[] = useSelector((state: RootStore) => state._subSuppliers)
	const branches: any[] = useSelector((state: RootStore) => state._branches)
	const logo: any = useSelector((state: RootStore) => state._logo)
	const [fields, setFields] = React.useState<any>([
		{ text: "certificateNumber", type: 'string', width: window.innerWidth * 0.03, minWidth: 50 },
		{ text: "referenceNumber", type: 'string', width: window.innerWidth * 0.03, minWidth: 50 },
		{ text: "receiveDate", type: 'DateTime', width: window.innerWidth * 0.03, minWidth: 50 },
		{ text: "BranchName", type: 'string', width: window.innerWidth * 0.03, minWidth: 50 },
		{ text: "SupplierName", type: 'string', width: window.innerWidth * 0.03, minWidth: 50 },
		{ text: "amountOfItems", type: 'string', width: window.innerWidth * 0.03, minWidth: 50, },
		{ text: "sumOfUnitsOrWeight", type: 'string', width: window.innerWidth * 0.03, minWidth: 50, },
		{ text: "goodsReceiver", type: 'string', width: window.innerWidth * 0.03, minWidth: 50, },
		{
			text: "EDI", type: 'CheckBoxCell', width: window.innerWidth * 0.03, minWidth: 30,
			cellRenderer: 'CheckBoxCell', checkBoxCell: {
				value: 'EDI',
				forceDisable: true,
			},
		},
		{
			text: "actions", type: 'string', width: window.innerWidth * 0.001, minWidth: 1, noFilter: true,
			cellRenderer: 'IconRender', renderIcons: [
				{ actions: true },
				{
					type: 'print', title: 'Print', onClick: (data: any) => {
						printDocument(data)
					}, icon: 'print.svg'
				},
			],
		},
	])
	const [rows, setRows] = React.useState<any>()
	document.title = activeTab;
	const dispatch = useDispatch();
	const [printData, setPrintData] = React.useState<any[]>([])
	const [remarks, setRemarks] = React.useState<any[]>([])
	const componentRef = React.useRef(null);
	const [loading, setLoading] = React.useState(false);
	const onBeforeGetContentResolve = React.useRef<(() => void) | null>(null);
	const [isEdiDataReady, setIsEdiDataReady] = React.useState<boolean>(false);
	const [ediRows, setEdiRows] = React.useState<any>();
	const [ediFields, setEdiFields] = React.useState<any>([
		{ text: "ediNumber", type: 'string', width: window.innerWidth * 0.03, minWidth: 50 },
		{ text: "date", type: 'DateTime', width: window.innerWidth * 0.03, minWidth: 50 },
		{ text: "BranchName", type: 'string', width: window.innerWidth * 0.03, minWidth: 50 },
		{ text: "SupplierName", type: 'string', width: window.innerWidth * 0.03, minWidth: 50 },
		{ text: "amountOfItems", type: 'string', width: window.innerWidth * 0.03, minWidth: 50, },
		{
			text: "actions", type: 'string', width: window.innerWidth * 0.001, minWidth: 1, noFilter: true,
			cellRenderer: 'IconRender', renderIcons: [
				{ actions: true },
				{
					type: 'stock', title: 'doReceive', onClick: (data: any) => {
						getEDIRowsData(data)
					}, icon: 'stock.svg'
				},
			],
		},
	]);
	const [ediDate, setEdiDate] = React.useState<Date>(new Date())
	const [ediBranches, setEdiBranches] = React.useState<any>();
	const [ediSuppliers, setEdiSuppliers] = React.useState<any>();
	const [chosenEdiBranch, setChosenEdiBranch] = React.useState<any>();
	const [chosenEdiSupplier, setChosenEdiSupplier] = React.useState<any>();
	const [ediRefNumber, setEdiRefNumber] = React.useState<any>();
	const [ediVerifyError, setEdiVerifyError] = React.useState<any>();
	const [ediCatalog, setEdiCatalog] = React.useState<any>();
	const [ediDataOrders, setEdiDataOrders] = React.useState<any>();
	const [loadingRecieveOrderes, setLoadingRecieveOrders] = React.useState<boolean>(false);
	const [selectedEdiRow, setSelectedEdiRow] = React.useState<any>();
	const [ediOrderRows, setEdiOrderRows] = React.useState<any>();
	const refNumberInputRef = React.useRef<any>();


	const getEDIRowsData = async (data: any) => {
			setIsEdiDataReady(false)
			setSelectedEdiRow(data);
			const orders = await getEdiRowOrders(data.ediNumber)

			const fullOrders: any = orders.map((ele: any) => {
				return {
					...ele,
					remark: null,
					receivedPackingFactor: null,
					isSaved: false,
					isChanged: false,
					isNewItem: false,
					savedRemark: null,
					savedReceive: null,
					AmountStatus: 2,
				}
			})
			setEdiOrderRows(fullOrders);
			setTimeout(() => { setActiveTab(TABS.CREATE_RECEIVE_EDI); setIsEdiDataReady(true); }, 4);
	}

	React.useEffect(() => {
		fetchRedux()
	}, []);

	const fetchRedux = async () => {
		if (suppliers && suppliers.length && branches && branches.length && logo && logo.length) {
			setRemarks(await getRemarks())
			fetchData()
		} else {
			setRemarks(await getRemarks())
			await fetchAndUpdateStore(dispatch, [
				{ state: suppliers, type: 'suppliers' },
				{ state: branches, type: 'branches' },
				{ state: subSuppliers, type: 'subSuppliers' },
				{ state: logo, type: 'logo' },
			])
		}
	}

	React.useEffect(() => {
		if (suppliers && suppliers.length && branches && branches.length && Array.isArray(branches) && logo && logo.length) {
			fetchData()
		}
	}, [suppliers, branches, logo]);


	React.useEffect(() => {
		if (suppliers && suppliers.length && branches && branches.length && logo && logo.length && subSuppliers && subSuppliers.length) {
			fetchEdiRows()
		}
	}, [suppliers, branches, logo, subSuppliers]);

	const fetchEdiRows = async () => {
		setIsEdiDataReady(false)
		setEdiBranches(await getEdiBranchesByDate(ediDate))
		const res = await getEdiRows()

		const rows: EdiRow[] = [];
		for (let index = 0; index < res.length; index++) {
			const doc = res[index];
			const supplier = suppliers.find((ele: any) => ele.Id === doc.SupplierId);
			const branch = branches.find((ele: any) => ele.BranchId === doc.BranchId);
			const row: EdiRow = {
				ediNumber: doc.EdiNum,
				date: doc.CreatedAt,
				BranchName: branch.Name,
				branchId: branch.BranchId,
				SupplierName: supplier.Name,
				supplierId: supplier.Id,
				amountOfItems: doc.ItemsCount,
			}

			rows.push(row)
		}


		setEdiRows(rows)
		setTimeout(() => {
			setIsEdiDataReady(true)
		}, 100);
	}

	const bufferToDataImage = (buffer: any) => {
		if (buffer == null)
			return "";
		var enc = new TextDecoder("utf-8");
		buffer = new Uint8Array(buffer);
		return enc.decode(buffer);
	}


	const fetchData = async () => {
		setIsDataReady(false)
		const signatures = await getSignatures(moment(date1).format('YYYY-MM-DD'), moment(date2).format('YYYY-MM-DD'))
		const rows: any[] = [];
		for (let index = 0; index < signatures.length; index++) {
			const signature = signatures[index];
			const supplier = suppliers.find((ele: any) => ele.Id === signature.SapakId);
			const branch = branches.find((ele: any) => ele.BranchId === signature.BranchId);
			const row: RecieveRow = {
				certificateNumber: signature.ReceiveNum,
				referenceNumber: signature.SapakReferenceNum,
				receiveDate: signature.ts,
				BranchId: branch.BranchId,
				SupplierId: supplier.Id,
				BranchName: branch.Name,
				SupplierName: supplier.Name,
				amountOfItems: null,
				sumOfUnitsOrWeight: null,
				goodsReceiver: signature.user_name ? signature.user_name === 'auto' ? formatMessage({ id: 'system' }) : signature.user_name : '',
				EDI: signature.edi_close === 1 ? true : false,
				EDI_Disabled: true,
				providerName: signature.SapakPersonName ? signature.SapakPersonName === 'Auto' ? formatMessage({ id: 'system' }) : signature.SapakPersonName : '',
				recieverPhoneNumber: signature.user_tel,
				logo: logo,
				providerSignature: bufferToDataImage(signature.ProviderSignature.data),
				recieverSignature: bufferToDataImage(signature.ReceiverSignature.data),
				additionalText: signature.AdditionalText,
				providerCarNumber: signature.SapakCarNumber,
				remarks: remarks,
				boxesNumber: signature.maraz,
				surfacesNumber: signature.mistch
			}

			rows.push(row)
		}

		setRows(rows)

		setTimeout(() => {
			setIsDataReady(true)
		}, 100);
	}


	const reactToPrintContent = React.useCallback(() => {
		return componentRef.current;
	}, [componentRef.current]);

	const handleAfterPrint = React.useCallback(() => {
	}, []);

	const handleBeforePrint = React.useCallback(() => {
	}, []);

	const handleOnBeforeGetContent = React.useCallback(() => {
		setLoading(true);
		return new Promise<void>((resolve) => {
			onBeforeGetContentResolve.current = resolve;
			setLoading(false);
			resolve();
		});
	}, [setLoading]);

	const handlePrint = useReactToPrint({
		content: reactToPrintContent,
		documentTitle: activeTab,
		onBeforeGetContent: handleOnBeforeGetContent,
		onBeforePrint: handleBeforePrint,
		onAfterPrint: handleAfterPrint,
		removeAfterPrint: true,
	});

	const printDocument = async (data: any) => {
		const details = await getSignatureReceivedOrders(data.certificateNumber)
		console.log({details})
		let finalData = { ...data, details }
		setPrintData(finalData)
		setTimeout(() => { if (handlePrint) handlePrint() }, 100);
	}


	const loadEdiBranchOptions = (inputValue: string, callback: any) => {
		callback(filterEdiBranch(inputValue));
	}

	const loadEdiSupplierOptions = (inputValue: string, callback: any) => {
		callback(filterEdiSupplier(inputValue));
	};

	const filterEdiSupplier = (inputValue: string) => {
		const supplieresFiltered = ediSuppliers.filter((i: any) =>
			i.Name.includes(inputValue)
		);

		let suppliersMap: any[] = []
		supplieresFiltered && supplieresFiltered[0] ? suppliersMap = supplieresFiltered.map((option: any, index: number) => {
			return { index, value: option.Id, label: option.Name }
		}) : []
		return suppliersMap
	};

	const filterEdiBranch = (inputValue: string) => {
		const branchesFiltered = ediBranches.filter((i: any) =>
			i.Name.includes(inputValue)
		);
		let branchesMap: any[] = []
		branchesFiltered && branchesFiltered[0] ? branchesMap = branchesFiltered.map((option: any, index: number) => {
			return { index, value: option.Id, label: option.Name }
		}) : []
		return branchesMap
	};


	const handleDoReceive = async () => {
		setLoadingRecieveOrders(true)
		const verify = await getEdiRefNumberVerify(chosenEdiBranch.value, chosenEdiSupplier.value, ediRefNumber)
		if (verify && verify.data && verify.data.verified === true) {
			setEdiVerifyError(null)
			let branchId: any = chosenEdiBranch.value;
			let supplierId: any = chosenEdiSupplier.value
			let date: any = ediDate;
			const data: any = await new Promise(function (resolve, reject) {
				let fetchedOrders: any = [];
				getEdiRecieveRowsByDate(date, branchId, supplierId).then(function (orders: any) {
					if (supplierId == 3003) {
						for (let i = 0; i < orders.length; i++) {
							if (orders[i].Ariza != null) {
								orders[i].AmountOrder = orders[i].AmountOrder * orders[i].Ariza
							}
						}
					}
					fetchedOrders = orders;
					return getEdiOrdersRecievedRowsByDate(date, branchId, supplierId);
				}).then(function (ordersReceived: any) {
					let collector: any = [];
					for (let i = 0; i < fetchedOrders.length; i++) {
						let order = fetchedOrders[i];
						let orderReceivedRecords = [];
						let remarkPresent = false;
						for (let s = 0; s < ordersReceived.length; s++) {
							let _order = ordersReceived[s];
							if (_order.BarCode === order.BarCode) {
								if (_order.RemarkId != null) {
									remarkPresent = true;
									break;
								}
								orderReceivedRecords.push(_order);
							}
						}
						if (remarkPresent)
							continue;
						if (orderReceivedRecords.length > 0) {
							let newAmountOrder = Math.round(order.AmountOrder);
							for (let j = 0; j < orderReceivedRecords.length; j++) {
								let _received = orderReceivedRecords[j];
								if (_received.AmountReceive != null)
									newAmountOrder -= Math.round(_received.AmountReceive);
							}
							if (newAmountOrder >= 1) {
								order.AmountOrder = newAmountOrder;
								order.AmountReceive = null;
								collector.push(order);
							}
						} else
							collector.push(order);
					}
					getEdiCatalogItems(supplierId).then(function (catalog: any) {
						setEdiCatalog(catalog)
						resolve(collector);
					});
				}).catch(reject);
			});


			const fullOrders: any = data.map((ele: any) => {
				return {
					...ele,
					itemDescription: `${ele.BarCodeName}`,
					remark: null,
					Ordered: ele.AmountOrder,
					receivedPackingFactor: null,
					isSaved: false,
					isChanged: false,
					isNewItem: false,
					savedRemark: null,
					savedReceive: null,
					AmountStatus: 2,
				}
			})
			setEdiDataOrders(fullOrders)

			setTimeout(() => {
				setActiveTab(TABS.CREATE_RECEIVE)
				setLoadingRecieveOrders(false)
			}, 100);
		} else {
			setEdiVerifyError(formatMessage({ id: 'refNumberAlreadyExists' }))
			setLoadingRecieveOrders(false)
		}
	}

	const clearStates = (states: React.Dispatch<any>[]) => states.forEach((state: React.Dispatch<any>) => state(null));

	return (
		[TABS.MAIN, TABS.CERTIFICATES].includes(activeTab) ?
			<>
				<div className='print__container'>
					<PrintReceiveCert ref={componentRef} data={printData} />
				</div>

				<Col md="12" sm="12" className="mb-1-5 p-0 font-medium-4 text-bold-700 text-black d-flex">
					<span>{activeTab}</span>
				</Col>
				{(
					<div className="mb-1-5">
						<Tabs value={activeTab} onChangTab={setActiveTab}>
							{userEditFlag && (<Tab caption={TABS.MAIN} name={TABS.MAIN} /> )}
							<Tab caption={TABS.CERTIFICATES} name={TABS.CERTIFICATES} />
						</Tabs>
					</div>
				)}

				<TabContent activeTab={activeTab}>
					<TabPane tabId={TABS.MAIN}>
						<div className='d-flex justify-content-between' style={{ width: '60rem', }}>
							<div className="d-flex justify-content-start align-items-center">
								<CalendarSingleInput
									onChange={async (date: any) => {
										setEdiDate(date)
										clearStates([setChosenEdiBranch, setChosenEdiSupplier, setEdiRefNumber, setEdiBranches, setEdiSuppliers, setEdiVerifyError, setEdiCatalog])
										refNumberInputRef.current.value = null;
										setEdiBranches(await getEdiBranchesByDate(date))
									}}
									calanderId={'aaaa'}
									inputId={'fdfff'}
									date={ediDate}
									top={'7.5vh'}
									classNames="width-9-rem mr-1"
								/>

								{[
									{
										name: 'Branch',
										loadOptions: loadEdiBranchOptions,
										text: formatMessage({ id: 'chooseBranch' }),
										defaultOptions: ediBranches && ediBranches[0] ? ediBranches.map((option: any, index: number) => {
											return { index, value: option.Id, label: option.Name, name: 'branch' }
										}) : [],
										value: chosenEdiBranch,
										styles: selectStyle,
										disabled: ediBranches && ediBranches.length > 0 && !loadingRecieveOrderes ? false : true
									},
									{
										name: 'Supplier',
										text: formatMessage({ id: 'chose__supplier' }),
										loadOptions: loadEdiSupplierOptions,
										defaultOptions: ediSuppliers && ediSuppliers[0] ? ediSuppliers.map((option: any, index: number) => {
											return { index, value: option.Id, label: option.Name, name: 'branch' }
										}) : [],
										value: chosenEdiSupplier,
										styles: selectStyle,
										disabled: ediSuppliers && ediSuppliers.length > 0 && chosenEdiBranch && !loadingRecieveOrderes ? false : true
									},
								].map((item, index) => {
									return (
										<div key={item.text} className={index > 0 ? 'ml-1' : ''} >
											<AsyncSelect
												// <AsyncSelectCopy
												name={item.name}
												isDisabled={item.disabled}
												isClearable={true}
												styles={item.styles}
												value={item.value}
												loadOptions={item.loadOptions}
												defaultOptions={item.defaultOptions}
												onChange={async (e: any, actionMeta: any) => {
													if (e) {
														if (actionMeta.action === 'select-option') {
															if (actionMeta.name === 'Branch') {
																clearStates([setEdiSuppliers, setEdiRefNumber, setChosenEdiSupplier])
																refNumberInputRef.current.value = null;
																setChosenEdiBranch(e);
																setEdiSuppliers(await getEdiSuppliersByDate(ediDate, e.value));
															}
															else if (actionMeta.name === 'Supplier') setChosenEdiSupplier(e)
														}
													} else {
														if (actionMeta.action === 'clear') {
															if (actionMeta.name === 'Branch') {
																clearStates([setChosenEdiBranch, setChosenEdiSupplier, setEdiRefNumber, setEdiVerifyError, setEdiSuppliers, setEdiCatalog])
																refNumberInputRef.current.value = null;
															}
															else if (actionMeta.name === 'Supplier') {
																clearStates([setChosenEdiSupplier, setEdiRefNumber, setEdiVerifyError, setEdiCatalog])
																refNumberInputRef.current.value = null;
															}
														}
													}
												}}
												closeMenuOnSelect={true}
												hideSelectedOptions={false}
												classNamePrefix='select'
												noOptionsMessage={() => formatMessage({ id: 'trending_productMinimumSearch' })}
												placeholder={item.text}
											/>
										</div>
									)
								})}


								<div className='ml-1'>
									<InputNumber
										disabled={chosenEdiSupplier && !loadingRecieveOrderes && !loadingRecieveOrderes ? false : true}
										id='referenceNumber'
										onChange={(e) => {
											if (e.target.value.trim().length === 0 || isNaN(Number(e.target.value))) {
												setEdiRefNumber(undefined)
												refNumberInputRef.current.value = null;
											} else {
												setEdiVerifyError(null);
												setEdiRefNumber(Number(e.target.value))
											}
										}} value={ediRefNumber} placeholder={formatMessage({ id: 'referenceNumber' })}
										inputRef={refNumberInputRef}
										hideFocus={true}
										inputHeight={'2.2rem'}
										error={ediVerifyError ? true : false}
										errorLabel={ediVerifyError ? ediVerifyError : null}
										classNames={loadingRecieveOrderes ? "mr-05" : "mr-1"}
									/>
								</div>

								{loadingRecieveOrderes ? <Spinner /> : null}


							</div>

							<Button
								disabled={chosenEdiBranch && chosenEdiBranch.value && chosenEdiSupplier && chosenEdiSupplier.value && ediRefNumber && !loadingRecieveOrderes && isEdiDataReady ? false : true}
								onClick={handleDoReceive} className='round width-9-rem width-11-rem height-2-5-rem d-flex align-items-center justify-content-center' color='primary'>{formatMessage({ id: 'doReceive' })}
							</Button>

							<Icon className="d-none" src={config.iconsPath + `general/send-icon-green.svg`} />
							<Icon className="d-none" src={config.iconsPath + `general/send-icon-gray.svg`} />
						</div>

						<Col md="12" sm="12" className={`mb-05 p-0 font-medium-4 text-bold-700 text-black d-flex ${ediVerifyError ? 'mt-05' : 'mt-1-5'}`}>
							<span>{formatMessage({ id: 'Receive Stock EDI' })}</span>
						</Col>


						<div className="mt-2 " style={{ width: '60rem' }}>
							<AgGrid
								defaultSortFieldNum={0}
								descSort
								id={'ediNumber'}
								gridHeight={'60vh'}
								translateHeader
								fields={ediFields}
								groups={[]}
								totalRows={[]}
								rows={ediRows}
								pagination
								resizable
								displayLoadingScreen={!isEdiDataReady}
							/>
						</div>
					</TabPane>


					<TabPane tabId={TABS.CERTIFICATES}>
						<div className='d-flex justify-content-between' style={{ width: '75%', height: '39px' }}>
							<CalendarDoubleInput
								setDate1={setDate1}
								setDate2={setDate2}
								date2={date2}
								date1={date1}
								top={'7.5vh'}
							/>

							<Button onClick={() => fetchData()} className='round width-9-rem width-11-rem height-2-5-rem d-flex align-items-center justify-content-center' color='primary'>{formatMessage({ id: 'filter' })}</Button>
						</div>

						<div className="mt-2 " style={{ width: '75%' }}>
							<AgGrid
								defaultSortFieldNum={0}
								descSort
								id={'certificateNumber'}
								gridHeight={'60vh'}
								translateHeader
								fields={fields}
								groups={[]}
								totalRows={[]}
								rows={rows}
								pagination
								resizable
								displayLoadingScreen={!isDataReady}
							/>
						</div>
					</TabPane>
				</TabContent>
			</>
			: activeTab === TABS.CREATE_RECEIVE ?

				<CreateReceiveGrid back={async (clearSelections: boolean) => {
					if (clearSelections) {
						setEdiDate(new Date())
						clearStates([setChosenEdiBranch, setChosenEdiSupplier, setEdiRefNumber, setEdiBranches, setEdiSuppliers, setEdiVerifyError, setEdiCatalog])
						setEdiBranches(await getEdiBranchesByDate(new Date()))
						fetchData()
					}
					setActiveTab(TABS.MAIN)
				}}
					date={ediDate}
					ediRefNumber={ediRefNumber}
					remarks={remarks}
					orderRows={ediDataOrders}
					chosenSupplier={chosenEdiSupplier} chosenBranch={chosenEdiBranch} catalogItems={ediCatalog ? ediCatalog : null} />

				: activeTab === TABS.CREATE_RECEIVE_EDI ?
					<CreateReceiveEDIGrid
						back={async (clearSelections: boolean) => {
							console.log(clearSelections)
							if (clearSelections) {
								setActiveTab(TABS.CERTIFICATES)
								fetchData()
							} else {
								setActiveTab(TABS.MAIN)
							}
						}}
						orderRows={ediOrderRows}
						selectedRow={selectedEdiRow}
						remarks={remarks}
					/>
					: <> </>
	);
};

export default RecieveStock;
