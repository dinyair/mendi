import * as React from 'react';
import { useIntl } from "react-intl"
import { config } from '@src/config'
import { Icon } from '@components';
import AsyncSelect from "react-select/async";
import { ModalTypes } from '@src/components/GenericModals/interfaces';
import { Col, Button, Spinner, } from 'reactstrap';
import InputNumber from '@src/components/input-number';
import CreateReceiveModalsData from './create-receive-modals-data';
import { filterCatalog, filterRemarkOptions, filterRowsCatalog, filterStatusOptions, productStyles, remarkStyles } from './helpers';
import { sendRecieveEDIToApprove, sendRecieveToApprove } from '../initial-state';
import CreateReceiveEDIModalsData from './create-receive-edi-modals-data';

// import { postReturnCertificate } from '../initial-state';

interface Props {
	readonly back: (clearSelections: boolean) => void;
	readonly selectedRow: any;
	readonly orderRows: any;
	readonly remarks: any;
}

export const CreateReceiveEDIGrid: React.FC<Props> = (props: Props) => {
	const { formatMessage } = useIntl();
	const { back, selectedRow, orderRows, remarks, } = props;

	const [modalType, setModalType] = React.useState<ModalTypes>(ModalTypes.none)
	const [chosenProduct, setChosenProduct] = React.useState<any>(null);
	const [chosenProductFull, setChosenProductFull] = React.useState<any>(null);
	const [rows, setRows] = React.useState<any>(orderRows)
	const [fields] = React.useState<any>(["itemDescription", "Ordered", "Supplied", "receivedPackingFactor", "remark", "confirm"])
	const [unitsNumber, setUnitsNumber] = React.useState<any>()
	const [packingNumber, setPackingNumber] = React.useState<any>()
	const [rowToChange, setRowToChange] = React.useState<any>()
	const [amountToSet, setAmountToSet] = React.useState<any>()
	const [filterByBarCode, setfilterByBarcode] = React.useState<any>(null);
	const statusOptions = [formatMessage({ id: 'Supplied' }), formatMessage({ id: 'PartlySupplied' }), formatMessage({ id: 'NotSupplied' })]
	const [filterByStatus, setfilterByStatus] = React.useState<any>({
		index: 2,
		label: statusOptions[2],
		name: statusOptions[2],
		value: statusOptions[2]
	});
	const [isLoading, setIsLoading] = React.useState<boolean>(false);
	const customizer = { direction: document.getElementsByTagName("html")[0].dir }
	const unitsInputRef = React.useRef<any>();
	const packingFactorInputRef = React.useRef<any>();
	const receivedAmountInputRef = React.useRef<any>();

	const loadRemarkOptions = (inputValue: string, callback: any) => {
		callback(filterRemarkOptions(inputValue, remarks));
	};

	const loadRowsCatalogOptions = (inputValue: string, callback: any) => {
		if (inputValue.length >= 1) callback(filterRowsCatalog(inputValue, rows));
		else callback()
	};

	const loadStatusOptions = (inputValue: string, callback: any) => {
		callback(filterStatusOptions(inputValue, statusOptions));
	};

	const toggleModal = (newType: ModalTypes) => {
		setModalType(newType)
	}

	const changeRowAmountReceive = (value: any, rowIndex: number) => {
		let newEdiRows = [...rows]
		newEdiRows[rowIndex].AmountReceive = value;
		newEdiRows[rowIndex].isChanged = true;
		setRows(newEdiRows)
	}

	const handleConfirmButton = (data: any) => {
		const rowIndex = rows.findIndex((ele: any) => ele.Id === data.Id)
		const newEdiRows = [...rows]
		let row = newEdiRows[rowIndex];

		// case of new row without data
		if (row.isSaved == false && data.AmountReceive == null && data.remark == null) newEdiRows[rowIndex].AmountReceive = newEdiRows[rowIndex].Amount;
		// case of row value change
		if (row.isSaved && row.isChanged) newEdiRows[rowIndex].isSaved = true
		// case of row delete confirm
		else newEdiRows[rowIndex].isSaved = !data.isSaved

		newEdiRows[rowIndex].isChanged = false
		newEdiRows[rowIndex].savedReceive = data.AmountReceive;
		newEdiRows[rowIndex].savedRemark = data.remark && data.remark.label ? data.remark.label : null;


		if (row.Amount == row.savedReceive && row.savedRemark == null && row.isSaved) newEdiRows[rowIndex].AmountStatus = 0;
		else if ((row.Amount !== row.savedReceive || row.savedRemark != null) && row.isSaved) newEdiRows[rowIndex].AmountStatus = 1;
		else newEdiRows[rowIndex].AmountStatus = 2;
		setRows(newEdiRows)
	}



	const handleKeypress = (e: any) => {
		if (e.key === 'Enter') {
			// chosenProduct && unitsNumber ? handleAddNewRow() : null
		}
	};

	const modalHandlers = {
		add: (data: any) => handleAddModal(data),
		info: () => toggleModal(ModalTypes.none)
	};

	const handleAddModal = async (data: any) => {
		console.log(data)
		const products = data.rows.map((row: any) => {
			const RemarkId = row.savedRemark != null ? remarks.find((remark: any) => remark.Name === row.savedRemark)?.Id : null;
			return {
				BarCode: row.BarCode,
				SupplierId: row.SupplierId,
				BranchId: row.BranchId,
				Ariza: row.Ariza,
				AmountReceive: row.savedReceive,
				RemarkId: RemarkId,
				OrderNum: row.OrderNum ? row.OrderNum : null,
				DeliveryDate: row.DeliveryDate,
				EdiNum: row.EdiNum,
				PartlySupplied : row.AmountStatus 
			}
		})
		const newObj = {
			...data,
			rows: products
		}

		// /* SEND DATA TO API */
		try {
			const res = await sendRecieveEDIToApprove(newObj)
		} catch (error) {
			console.log(error)
		}
		toggleModal(ModalTypes.none)
		back(true)
	}

	const clearStates = (states: React.Dispatch<any>[]) => states.forEach((state: React.Dispatch<any>) => state(null));

	const savedRowsLength = rows && rows.length > 0 ? rows.filter((ele: any) => ele.isSaved === true).length : null
	const isCertificateReady = savedRowsLength === rows.length;
	return (

		<>
			<div onKeyDown={handleKeypress} >
				<Col md="12" sm="12" className="mb-1-5 p-0 font-medium-4 text-bold-700 text-black d-flex">
					<span className={`cursor-pointer mr-05 ${customizer.direction === 'ltr' ? 'rotate-180' : ''}`} onClick={() => back(false)}>➜</span>
					{formatMessage({ id: 'Receive Stock EDI' })} - {selectedRow && selectedRow.SupplierName ? selectedRow.SupplierName : ''}
					, {formatMessage({ id: 'certificateNumber' })} - {selectedRow && selectedRow.ediNumber ? selectedRow.ediNumber : ''}
				</Col>


				{/* First controllers row */}
				<div className='d-flex justify-content-between mt-1 width-60-rem height-39'>
					<div className='d-flex justify-content-start'>
						{[
							{
								name: 'product',
								text: formatMessage({ id: 'catalog__input' }),
								loadOptions: loadRowsCatalogOptions,
								styles: productStyles,
								value: filterByBarCode ? filterByBarCode : null,
								defaultOptions: rows ? rows.map((option: any, index: number) => {
									return { index, value: option.BarCode, label: `${option.BarCodeName} ${option.BarCode}`, name: option.BarCodeName }
								}) : []
							},
							{
								name: 'status',
								text: formatMessage({ id: 'status' }),
								loadOptions: loadStatusOptions,
								styles: productStyles,
								value: filterByStatus ? filterByStatus : null,
								defaultOptions: statusOptions.map((option: any, index: number) => {
									return { index, value: option, label: option, name: option }
								})
							},
						].map((item: any, index: number) => {
							return (
								<div key={item.text} >
									<AsyncSelect
										name={item.name}
										value={item.value}
										isDisabled={false}
										isClearable={true}
										styles={item.styles}
										loadOptions={item.loadOptions}
										defaultOptions={item.defaultOptions}
										onChange={(e: any, actionMeta: any) => {
											if (actionMeta.name === 'status') {
												if (e) {
													if (actionMeta.action === "select-option") setfilterByStatus(e)
												} else {
													if (actionMeta.action === "clear") setfilterByStatus(null)
												}
											} else {
												if (e) {
													if (actionMeta.action === "select-option") setfilterByBarcode(e)
												} else {
													if (actionMeta.action === "clear") setfilterByBarcode(null)
												}
											}
										}}
										classNamePrefix='select'
										noOptionsMessage={() => formatMessage({ id: 'noResults' })}
										placeholder={item.text}
										className={`height-2-5-rem ${index > 0 ? 'ml-1' : ''}`}
									/>
								</div>
							)
						})}
					</div>
				</div>


				{/* Table */}
				<div className="mt-2 width-65-rem">
					<div className="table-responsive ag-root-wrapper-body overflow-auto d-inline-block overflow-auto height-60-vh" >
						<table className="table table-bordered mb-0">
							<thead>
								<tr>
									{fields.map((field: string, index: number) =>
										<th key={'thead' + index} className="zindex-1 position-top-0 bg-white customHeaderLabel text-center position-sticky p-085" >{formatMessage({ id: field })}</th>
									)}
								</tr>
							</thead>
							<tbody >
								{
									!isLoading ?
										(rows ? rows.filter((ele: any) => {
											if (filterByBarCode && !filterByStatus) return (ele.BarCode === filterByBarCode.value)
											else if (!filterByBarCode && filterByStatus) {
												let status = filterByStatus.index
												return (ele.AmountStatus === status)
											}
											else if (filterByBarCode && filterByStatus) {
												let status = filterByStatus.index
												return (ele.AmountStatus === status && filterByBarCode.value === ele.BarCode)
											}
											return true
										}) : [])
											.map((row: any, index: number) => {
												const rowIndex = rows.findIndex((ele: any) => ele.Id === row.Id)
												return <tr key={'tr' + rowIndex} className={`${index % 2 === 0 ? "bg-first-row" : "bg-white"} height-4-5-rem`} >
													<td className="d-flex flex-column justify-content-center font-medium-2 text-bold-600 p-05 max-height-4-5-rem">
														<div>{row.BarCodeName ? row.BarCodeName : null}
															<br />
															{row.BarCode}
														</div>
													</td>
													<td className="font-medium-2 text-bold-600 text-center p-05 bg-light-yellow  height-4-5-rem">{row.Ariza != null ? row.AmountOrder * row.Ariza : ''}</td>
													<td className="d-flex flex-column justify-content-center font-medium-2 text-bold-600 p-05 text-center bg-light-yellow  height-4-5-rem">
														<div className="">{row.Amount ? row.Amount : ' '}</div>
														<div className="height-1-7-5-rem">{row.AmountMaraz ? `(${row.AmountMaraz} ${formatMessage({ id: 'packages' })})` : ``}</div>
													</td>
													<td className="width-15-rem  p-05">
														{!isLoading ? <InputNumber
															key={'inputUnits' + rowIndex}
															disabled={false}
															id={'units' + rowIndex}
															onChange={(e) => {
																if (e.target.value.trim().length === 0 || isNaN(Number(e.target.value))) {
																	changeRowAmountReceive(null, rowIndex)
																} else {
																	if (row.Amount < Number(e.target.value)) {
																		changeRowAmountReceive(row.Amount, rowIndex)
																		toggleModal(ModalTypes.info)
																	} else {
																		changeRowAmountReceive(Number(e.target.value), rowIndex)
																	}
																}
															}}
															value={row.AmountReceive != null ? row.AmountReceive : null}
															placeholder={formatMessage({ id: 'unitsQuantity' })}
															inputRef={receivedAmountInputRef}
															hideFocus={true}
															inputHeight={'2.6rem'}
														/> : null}
													</td>
													<td className="width-15-rem p-05">
														<AsyncSelect
															name={'remark'}
															key={'inputRemarks' + rowIndex}
															value={row.remark}
															isClearable={true}
															styles={remarkStyles}
															loadOptions={loadRemarkOptions}
															defaultOptions={remarks && remarks[0] ? remarks.map((option: any, index: number) => {
																return { index, value: option.Id, label: option.Name, name: 'remark' }
															}) : []}
															isDisabled={row.editRemark === false || row.Amount === row.AmountReceive || row.AmountReceive == null ? true : false}
															onChange={(e: any, actionMeta: any) => {
																if (e) {
																	let newEdiRows = [...rows]
																	newEdiRows[rowIndex].remark = e;
																	newEdiRows[rowIndex].RemarkId = e.value;
																	newEdiRows[rowIndex].isChanged = true;
																	setRows(newEdiRows)
																} else {
																	let newEdiRows = [...rows]
																	newEdiRows[rowIndex].remark = null;
																	newEdiRows[rowIndex].RemarkId = null;
																	newEdiRows[rowIndex].isChanged = true;
																	setRows(newEdiRows)
																}
															}}
															classNamePrefix='select'
															noOptionsMessage={() => formatMessage({ id: 'noResults' })}
															placeholder={formatMessage({ id: 'chooseRemark' })}
															className='height-2-6-rem margin-0-auto'
														/>
													</td>
													<td className="p-05">
														<div className="d-flex justify-content-center">
															<Button className={`round height-2-6-rem width-5-rem d-flex align-items-center justify-content-center ${row.isSaved && !row.isChanged ? 'bg-white' : ''}`} outline={row.isSaved && !row.isChanged ? true : false} color='primary' onClick={(e) => {
																e.preventDefault()
																handleConfirmButton(row)
															}}>{row.isSaved && !row.isChanged ? formatMessage({ id: 'Confirmed' }) : formatMessage({ id: 'Confirm' })}</Button>
														</div>
													</td>
												</tr>
											}
											) : null}
							</tbody>
						</table>
					</div>

					<div className="d-flex justify-content-end">
						<div className={`d-flex width-8-rem mt-1 ${isCertificateReady ? 'cursor-pointer' : 'cursor-default'}`} onClick={() => {
							isCertificateReady ? toggleModal(ModalTypes.add) : null
						}}>
							<Icon className="mr-05" src={config.iconsPath + `general/send-icon-${isCertificateReady ? 'green' : 'gray'}.svg`} />
							<span className={`mr-05 ${isCertificateReady ? 'text-turquoise' : 'text-gray opacity-07'}`}>{formatMessage({ id: "sendCertificate" })}</span>
						</div>
					</div>
				</div>
			</div>


			{[ModalTypes.add, ModalTypes.info].includes(modalType) ?
				<CreateReceiveEDIModalsData
					modalHandlers={modalHandlers}
					modalType={modalType}
					selectedRow={selectedRow}
					rows={rows.filter((ele: any) => ele.isSaved === true)}
					toggleModal={(type: ModalTypes) => toggleModal(type)} /> : null}
		</>
	);
};


export default CreateReceiveEDIGrid;
