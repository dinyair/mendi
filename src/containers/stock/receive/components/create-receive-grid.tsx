import * as React from 'react';
import { useIntl } from "react-intl"
import { config } from '@src/config'
import { Icon } from '@components';
import AsyncSelect from "react-select/async";
import { ModalTypes } from '@src/components/GenericModals/interfaces';
import { Col, Button, Spinner, } from 'reactstrap';
import InputNumber from '@src/components/input-number';
import CreateReceiveModalsData from './create-receive-modals-data';
import { filterCatalog, filterRemarkOptions, filterRowsCatalog, filterStatusOptions, productStyles, remarkStyles } from './helpers';
import { sendRecieveToApprove } from '../initial-state';

// import { postReturnCertificate } from '../initial-state';

interface Props {
	readonly back: (clearSelections: boolean) => void;
	readonly chosenSupplier: any;
	readonly chosenBranch: any;
	readonly catalogItems: any;
	readonly orderRows: any;
	readonly remarks: any;
	readonly ediRefNumber: any;
	readonly date: any;
}

export const CreateReceiveGrid: React.FC<Props> = (props: Props) => {
	const { formatMessage } = useIntl();
	const { back, chosenSupplier, chosenBranch, catalogItems, orderRows, remarks, ediRefNumber, date } = props;

	const [modalType, setModalType] = React.useState<ModalTypes>(ModalTypes.none)
	const [chosenProduct, setChosenProduct] = React.useState<any>(null);
	const [chosenProductFull, setChosenProductFull] = React.useState<any>(null);
	const [rows, setRows] = React.useState<any>(orderRows)
	const [fields] = React.useState<any>(["itemDescription", "Ordered", "receivedPackingFactor", "remark", "confirm"])
	const [unitsNumber, setUnitsNumber] = React.useState<any>()
	const [packingNumber, setPackingNumber] = React.useState<any>()
	const [rowToChange, setRowToChange] = React.useState<any>()
	const [amountToSet, setAmountToSet] = React.useState<any>()
	const [filterByBarCode, setfilterByBarcode] = React.useState<any>(null);
	const statusOptions = [formatMessage({ id: 'Supplied' }), formatMessage({ id: 'PartlySupplied' }), formatMessage({ id: 'NotSupplied' })]
	const [filterByStatus, setfilterByStatus] = React.useState<any>({
		index: 2,
		label: statusOptions[2],
		name: statusOptions[2],
		value: statusOptions[2]
	});
	const [isLoading, setIsLoading] = React.useState<boolean>(false);
	const customizer = { direction: document.getElementsByTagName("html")[0].dir }
	const unitsInputRef = React.useRef<any>();
	const packingFactorInputRef = React.useRef<any>();
	const receivedAmountInputRef = React.useRef<any>();

	const loadCatalogOptions = (inputValue: string, callback: any) => {
		if (inputValue.length >= 1) callback(filterCatalog(inputValue, catalogItems));
		else callback()
	};

	const loadRemarkOptions = (inputValue: string, callback: any) => {
		callback(filterRemarkOptions(inputValue, remarks));
	};

	const loadRowsCatalogOptions = (inputValue: string, callback: any) => {
		if (inputValue.length >= 1) callback(filterRowsCatalog(inputValue, rows));
		else callback()
	};

	const loadStatusOptions = (inputValue: string, callback: any) => {
		callback(filterStatusOptions(inputValue, statusOptions));
	};

	const toggleModal = (newType: ModalTypes) => {
		setModalType(newType)
	}

	const changeRowAmountReceive = (value: any, rowIndex: number) => {
		let newEdiRows = [...rows]
		newEdiRows[rowIndex].AmountReceive = value;
		newEdiRows[rowIndex].isChanged = true;
		setRows(newEdiRows)
	}

	const handleConfirmButton = (data: any) => {
		const rowIndex = rows.findIndex((ele: any) => ele.Id === data.Id)
		const newEdiRows = [...rows]
		let row = newEdiRows[rowIndex];

		// case of new row without data
		if (row.isSaved == false && data.AmountReceive == null && data.remark == null) newEdiRows[rowIndex].AmountReceive = newEdiRows[rowIndex].AmountOrder;
		// case of row value change
		if (row.isSaved && row.isChanged) newEdiRows[rowIndex].isSaved = true
		// case of row delete confirm
		else newEdiRows[rowIndex].isSaved = !data.isSaved

		newEdiRows[rowIndex].isChanged = false
		newEdiRows[rowIndex].savedReceive = data.AmountReceive;
		newEdiRows[rowIndex].savedRemark = data.remark && data.remark.label ? data.remark.label : null;


		if (row.AmountOrder == row.savedReceive && row.savedRemark == null && row.isSaved) newEdiRows[rowIndex].AmountStatus = 0;
		else if ((row.AmountOrder !== row.savedReceive || row.savedRemark != null) && row.isSaved) newEdiRows[rowIndex].AmountStatus = 1;
		else newEdiRows[rowIndex].AmountStatus = 2;

		setRows(newEdiRows)
	}


	const handleAddNewRow = () => {
		const newRow = {
			AmountOrder: 0,
			Ordered: formatMessage({ id: 'notOrdered' }),
			AmountReceive: Number(unitsInputRef.current.value),
			AmountStatus: 0,
			Ariza: chosenProductFull.Ariza,
			BarCode: chosenProductFull.BarCode,
			BarCodeName: chosenProductFull.Name,
			itemDescription: chosenProductFull.Name,
			ReceiveNum: null,
			RemarkId: null,
			BranchId: chosenBranch.value,
			SapakId: chosenSupplier.value,
			Id: rows ? [...rows].length : [].length,
			isSaved: true,
			savedReceive: Number(unitsInputRef.current.value),
			savedRemark: null,
			isChanged: false,
			editRemark: false,
			isNewItem: true,
			AspakaDate: orderRows[0].AspakaDate,
		}

		const newRows = rows ? [...rows] : []
		newRows.push(newRow)
		setRows(newRows)
		clearStates([setChosenProduct, setChosenProductFull, setUnitsNumber, setPackingNumber])
		unitsInputRef.current.value = null;
		packingFactorInputRef.current.value = null;
	}

	const handleKeypress = (e: any) => {
		if (e.key === 'Enter') {
			chosenProduct && unitsNumber ? handleAddNewRow() : null
		}
	};

	const modalHandlers = {
		add: (data: any) => handleAddModal(data),
		info: (confirm: boolean) => {
			if (confirm) {
				changeRowAmountReceive(amountToSet, rowToChange)
				toggleModal(ModalTypes.none)
			} else {
				changeRowAmountReceive(null, rowToChange)
				toggleModal(ModalTypes.none)
			}
			clearStates([setRowToChange, setAmountToSet])
		}
	};

	const handleAddModal = async (data: any) => {
		// console.log(data.rows)
		const products = data.Rows.map((ele: any) => {
			console.log(ele)
			const RemarkId = ele.savedRemark != null ? remarks.find((remark: any) => remark.Name === ele.savedRemark)?.Id : null;
			return {
				BarCode: ele.BarCode,
				SapakId: ele.SapakId,
				BranchId: ele.BranchId,
				Ariza: ele.Ariza,
				AspakaDate: ele.AspakaDate,
				AmountReceive: ele.savedReceive,
				RemarkId: RemarkId,
				ReceiveNum: null,
				OrderNum: ele.OrderNum ? ele.OrderNum : null,
				PartlySupplied : ele.AmountStatus 
				// Amount: ele.unitsQuantity,
				// AmountPackage: ele.packingFactorQuantity,
				// PackageSize: ele.packingFactor
			}
		})
		const newObj = {
			...data,
			Rows: products
		}

		// console.log({ newObj })

		/* SEND DATA TO API */
		try {
			const res = await sendRecieveToApprove(newObj)
		} catch (error) {
			console.log(error)
		}
		toggleModal(ModalTypes.none)
		back(true)
	}

	const clearStates = (states: React.Dispatch<any>[]) => states.forEach((state: React.Dispatch<any>) => state(null));

	const rowsToSendLength = rows.filter((ele: any) => ele.isSaved === true).length
	return (
		chosenSupplier && catalogItems ?
			<>
				<div onKeyDown={handleKeypress} >
					<Col md="12" sm="12" className="mb-1-5 p-0 font-medium-4 text-bold-700 text-black d-flex">
						<span className={`cursor-pointer mr-05 ${customizer.direction === 'ltr' ? 'rotate-180' : ''}`} onClick={() => back(false)}>➜</span> {formatMessage({ id: 'ReceiveStock' })} - {chosenSupplier && chosenSupplier.label ? chosenSupplier.label : ''}
						, {formatMessage({ id: 'certificateNumber' })} - {ediRefNumber ? ediRefNumber : ''}
					</Col>


					{/* First controllers row */}
					<div className='d-flex justify-content-between width-60-rem height-39'>
						<div className='d-flex justify-content-start'>
							{[
								{
									name: 'product',
									text: formatMessage({ id: 'catalog__input' }),
									loadOptions: loadCatalogOptions,
									styles: productStyles,
									value: chosenProduct ? chosenProduct : null,
									defaultOptions: catalogItems ? catalogItems.map((option: any, index: number) => {
										return { index, value: option.BarCode, label: `${option.Name} ${option.BarCode}`, name: option.Name }
									}) : []
								},
							].map(item => {
								return (
									<div key={item.text} >
										<AsyncSelect
											name={item.name}
											value={item.value}
											isDisabled={catalogItems.length > 0 ? false : true}
											isClearable={true}
											styles={item.styles}
											loadOptions={item.loadOptions}
											defaultOptions={item.defaultOptions}
											onChange={(e: any, actionMeta: any) => {
												if (e) {
													if (actionMeta.action === "select-option") {
														const item = catalogItems.find((ele: any) => ele.BarCode === e.value)
														setChosenProduct(e)
														setChosenProductFull(item)
													}
												} else {
													if (actionMeta.action === "clear") {
														setChosenProduct(null)
														setChosenProductFull(null)
														setUnitsNumber(null)
														setPackingNumber(null)
														unitsInputRef.current.value = null;
														packingFactorInputRef.current.value = null;
													}
												}
											}}
											classNamePrefix='select'
											noOptionsMessage={() => formatMessage({ id: 'noResults' })}
											placeholder={item.text}
											className='height-2-5-rem'
										/>
									</div>
								)
							})}

							<div className='ml-1'>
								<InputNumber disabled={chosenProductFull && chosenProductFull.Ariza && chosenProductFull.Ariza > 0 ? false : true}
									id='packingFactor'
									onChange={(e) => {
										if (e.target.value.trim().length === 0 || isNaN(Number(e.target.value))) {
											setPackingNumber(undefined)
											setUnitsNumber(undefined)
											unitsInputRef.current.value = null;
											packingFactorInputRef.current.value = null;
										} else {
											const packageSize = chosenProductFull && chosenProductFull.Ariza && chosenProductFull.Ariza > 0 ? chosenProductFull.Ariza : 1;
											setPackingNumber(Number(e.target.value))
											setUnitsNumber(Number(e.target.value) * packageSize)
										}
									}} value={packingNumber} placeholder={formatMessage({ id: 'packingFactorQuantity' })}
									inputRef={packingFactorInputRef}
									hideFocus={chosenProduct ? false : true}
									inputHeight={'2.6rem'}
									classNames="mr-1"
								/>
							</div>

							<div className='mr-1'>
								<InputNumber
									disabled={chosenProductFull ? false : true}
									id='units'
									onChange={(e) => {
										if (e.target.value.trim().length === 0 || isNaN(Number(e.target.value))) {
											setPackingNumber(undefined)
											setUnitsNumber(undefined)
											unitsInputRef.current.value = null;
											packingFactorInputRef.current.value = null;
										} else {
											const packageSize = chosenProductFull && chosenProductFull.Ariza && chosenProductFull.Ariza > 0 ? chosenProductFull.Ariza : 1;
											if (chosenProductFull && chosenProductFull.Ariza && chosenProductFull.Ariza > 0) {
												setPackingNumber(Number(e.target.value) / packageSize)
											}
											setUnitsNumber(Number(e.target.value))
										}
									}} value={unitsNumber} placeholder={formatMessage({ id: 'unitsQuantity' })}
									inputRef={unitsInputRef}
									hideFocus={chosenProduct ? false : true}
									inputHeight={'2.6rem'}
									classNames="mr-1"
								/>
							</div>
						</div>

						<Button
							color="primary"
							disabled={(chosenProduct && unitsNumber && (packingNumber || !chosenProductFull.Ariza || chosenProductFull.Ariza === 0)) ? false : true}
							className="ml-1 round text-bold-400 d-flex justify-content-center align-items-center width-9-rem height-2-9-rem cursor-pointer btn-primary "
							onClick={() => handleAddNewRow()}>
							+ {formatMessage({ id: "addItem" })}
						</Button>
					</div>


					{/* Second controllers row */}
					<div className='d-flex justify-content-between mt-1 width-60-rem height-39'>
						<div className='d-flex justify-content-start'>
							{[
								{
									name: 'product',
									text: formatMessage({ id: 'catalog__input' }),
									loadOptions: loadRowsCatalogOptions,
									styles: productStyles,
									value: filterByBarCode ? filterByBarCode : null,
									defaultOptions: rows ? rows.map((option: any, index: number) => {
										return { index, value: option.BarCode, label: `${option.BarCodeName} ${option.BarCode}`, name: option.BarCodeName }
									}) : []
								},
								{
									name: 'status',
									text: formatMessage({ id: 'status' }),
									loadOptions: loadStatusOptions,
									styles: productStyles,
									value: filterByStatus ? filterByStatus : null,
									defaultOptions: statusOptions.map((option: any, index: number) => {
										return { index, value: option, label: option, name: option }
									})
								},
							].map((item: any, index: number) => {
								return (
									<div key={item.text} >
										<AsyncSelect
											name={item.name}
											value={item.value}
											isDisabled={catalogItems.length > 0 ? false : true}
											isClearable={true}
											styles={item.styles}
											loadOptions={item.loadOptions}
											defaultOptions={item.defaultOptions}
											onChange={(e: any, actionMeta: any) => {
												if (actionMeta.name === 'status') {
													if (e) {
														if (actionMeta.action === "select-option") setfilterByStatus(e)
													} else {
														if (actionMeta.action === "clear") setfilterByStatus(null)
													}
												} else {
													if (e) {
														if (actionMeta.action === "select-option") setfilterByBarcode(e)
													} else {
														if (actionMeta.action === "clear") setfilterByBarcode(null)
													}
												}
											}}
											classNamePrefix='select'
											noOptionsMessage={() => formatMessage({ id: 'noResults' })}
											placeholder={item.text}
											className={`height-2-5-rem ${index > 0 ? 'ml-1' : ''}`}
										/>
									</div>
								)
							})}
						</div>
					</div>


					{/* Table */}
					<div className="mt-2 width-60-rem">
						<div className="table-responsive ag-root-wrapper-body overflow-auto d-inline-block overflow-auto height-60-vh" >
							<table className="table table-bordered mb-0">
								<thead>
									<tr>
										{fields.map((field: string, index: number) =>
											<th key={'thead' + index} className="zindex-1 position-top-0 bg-white customHeaderLabel text-center position-sticky p-085" >{formatMessage({ id: field })}</th>
										)}
									</tr>
								</thead>
								<tbody >
									{
										!isLoading ? (rows ? rows.filter((ele: any) => {
											if (filterByBarCode && !filterByStatus) return (ele.BarCode === filterByBarCode.value)
											else if (!filterByBarCode && filterByStatus) {
												let status = filterByStatus.index
												return (ele.AmountStatus === status)
											}
											else if (filterByBarCode && filterByStatus) {
												let status = filterByStatus.index
												return (ele.AmountStatus === status && filterByBarCode.value === ele.BarCode)
											}
											return true
										}) : []).map((row: any, index: number) => {
											const rowIndex = rows.findIndex((ele: any) => ele.Id === row.Id)
											return <tr key={'tr' + rowIndex} className={row.AmountOrder === 0 && row.isNewItem ? "bg-selected-row" : index % 2 === 0 ? "bg-first-row" : "bg-white"} >
												<td className="d-flex flex-column font-medium-2 text-bold-600 p-05">
													<div className="">{row.BarCodeName}</div>
													<div>{row.BarCode}</div>
												</td>
												<td className={`font-medium-2 text-bold-600 text-center p-05 ${row.AmountOrder === 0 && row.isNewItem ? "bg-selected-row" : "bg-light-yellow"}`}>{row.AmountOrder === 0 && row.isNewItem ? formatMessage({ id: 'notOrdered' }) : row.AmountOrder}</td>
												<td className="width-15-rem  p-05">
													{!isLoading ? <InputNumber
														key={'inputUnits' + rowIndex}
														disabled={false}
														id={'units' + rowIndex}
														onChange={(e) => {
															if (e.target.value.trim().length === 0 || isNaN(Number(e.target.value))) {
																changeRowAmountReceive(null, rowIndex)
															} else {
																if (row.AmountOrder < Number(e.target.value) && row.isNewItem === false) {
																	receivedAmountInputRef.current.value = null;
																	setRowToChange(rowIndex)
																	setAmountToSet(Number(e.target.value))
																	toggleModal(ModalTypes.info)
																} else {
																	changeRowAmountReceive(Number(e.target.value), rowIndex)
																}
															}
														}}
														value={row.AmountReceive != null && !isLoading ? row.AmountReceive : null}
														placeholder={formatMessage({ id: 'unitsQuantity' })}
														inputRef={receivedAmountInputRef}
														hideFocus={true}
														inputHeight={'2.6rem'}
													/> : null}
												</td>
												<td className="width-15-rem p-05">
													<AsyncSelect
														name={'remark'}
														key={'inputRemarks' + rowIndex}
														value={row.remark}
														isClearable={true}
														styles={remarkStyles}
														loadOptions={loadRemarkOptions}
														defaultOptions={remarks && remarks[0] ? remarks.map((option: any, index: number) => {
															return { index, value: option.Id, label: option.Name, name: 'remark' }
														}) : []}
														isDisabled={row.editRemark === false || row.AmountOrder === row.AmountReceive || row.AmountReceive == null ? true : false}
														onChange={(e: any, actionMeta: any) => {
															if (e) {
																let newEdiRows = [...rows]
																newEdiRows[rowIndex].remark = e;
																newEdiRows[rowIndex].RemarkId = e.value;
																newEdiRows[rowIndex].isChanged = true;
																setRows(newEdiRows)
															} else {
																let newEdiRows = [...rows]
																newEdiRows[rowIndex].remark = null;
																newEdiRows[rowIndex].RemarkId = null;
																newEdiRows[rowIndex].isChanged = true;
																setRows(newEdiRows)
															}
														}}
														classNamePrefix='select'
														noOptionsMessage={() => formatMessage({ id: 'noResults' })}
														placeholder={formatMessage({ id: 'chooseRemark' })}
														className='height-2-6-rem margin-0-auto'
													/>
												</td>
												<td className="p-05">
													<div className="d-flex justify-content-center">
														<Button className={`round height-2-6-rem width-5-rem d-flex align-items-center justify-content-center ${row.isSaved && !row.isChanged ? 'bg-white' : ''}`} outline={row.isSaved && !row.isChanged ? true : false} color='primary' onClick={(e) => {
															e.preventDefault()
															handleConfirmButton(row)
														}}>{row.isSaved && !row.isChanged ? formatMessage({ id: 'Confirmed' }) : formatMessage({ id: 'Confirm' })}</Button>
													</div>
												</td>
											</tr>
										}
										) : null}
								</tbody>
							</table>
						</div>

						<div className="d-flex justify-content-end">
							<div className={`d-flex width-8-rem mt-1 ${rowsToSendLength > 0 ? 'cursor-pointer' : 'cursor-default'}`} onClick={() => {
								rowsToSendLength > 0 ? toggleModal(ModalTypes.add) : null
							}}>
								<Icon className="mr-05" src={config.iconsPath + `general/send-icon-${rowsToSendLength ? 'green' : 'gray'}.svg`} />
								<span className={`mr-05 ${rowsToSendLength > 0 ? 'text-turquoise' : 'text-gray opacity-07'}`}>{formatMessage({ id: "sendCertificate" })}</span>
							</div>
						</div>
					</div>
				</div>


				{modalType === ModalTypes.add ||
					modalType === ModalTypes.info ?
					<CreateReceiveModalsData
						modalHandlers={modalHandlers}
						modalType={modalType}
						supplier={chosenSupplier}
						branch={chosenBranch}
						rows={rows.filter((ele: any) => ele.isSaved === true)}
						refNumber={ediRefNumber}
						date={date}
						toggleModal={(type: ModalTypes) => toggleModal(type)} /> : null}
			</> : null
	);
};


export default CreateReceiveGrid;
