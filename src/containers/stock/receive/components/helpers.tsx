export const filterRemarkOptions = (inputValue: string,rows:any) => {
    const remarksFiltered = rows.filter((i: any) =>
        i.Name.includes(inputValue)
    );
    let remarksMap: any[] = []
    remarksFiltered && remarksFiltered[0] ? remarksMap = remarksFiltered.map((option: any, index: number) => {
        return { index, value: option.Id, label: option.Name }
    }) : []
    return remarksMap
};

export const filterCatalog = (inputValue: string, rows:any) => {
    const regex = /\d/;
    const doesContainNumber = regex.test(inputValue)

    const catalogFiltered = rows.filter((i: any) => {
        return i.Name.includes(inputValue) || String(i.BarCode).includes(inputValue)
    });
    let catalogMap: any[] = []
    catalogFiltered && catalogFiltered[0] ? catalogMap = catalogFiltered.map((option: any, index: number) => {
        return { index, value: option.BarCode, label: `${option.Name} ${option.BarCode}`, name: option.Name }
    }) : []
    if (doesContainNumber) {
        catalogMap.sort((a, b) => a.value - b.value)
    } else {
        catalogMap.sort((a, b) => a.label.length - b.label.length)
    }
    return catalogMap.slice(0, 220)
};

export const filterRowsCatalog = (inputValue: string, rows:any) => {
    const regex = /\d/;
    const doesContainNumber = regex.test(inputValue)

    const catalogFiltered = rows.filter((i: any) => {
        return i.BarCodeName.includes(inputValue) || String(i.BarCode).includes(inputValue)
    });
    let catalogMap: any[] = []
    catalogFiltered && catalogFiltered[0] ? catalogMap = catalogFiltered.map((option: any, index: number) => {
        return { index, value: option.BarCode, label: `${option.BarCodeName} ${option.BarCode}`, name: option.BarCodeName }
    }) : []
    if (doesContainNumber) {
        catalogMap.sort((a, b) => a.value - b.value)
    } else {
        catalogMap.sort((a, b) => a.label.length - b.label.length)
    }
    return catalogMap.slice(0, 220)
};


export const filterStatusOptions = (inputValue: string,rows:any) => {
    const statusFiltered = rows.filter((i: any) =>
        i.includes(inputValue)
    );
    let statusesMap: any[] = []
    statusFiltered && statusFiltered[0] ? statusesMap = statusFiltered.map((option: any, index: number) => {
        return { index, value: option, label: option, name: option }
    }) : []
    return statusesMap
};

export const productStyles = {
    container: (base: any) => ({
        ...base,
        textAlign: 'left'

    }),
    menu: (provided: any) => ({
        ...provided,
        zIndex: 777
    }),
    option: (provided: any, state: any) => ({
        ...provided,
        "&:hover": {
            backgroundColor: "#31baab"
        },
        color: state.isSelected ? '#ffffff' : '#1e1e20',
        backgroundColor: state.isSelected ? '#027b7a' : '#ffffff',
        borderBottom: '1px dotted lightgray',
        padding: 10,
        textAlign: (/[\u0590-\u05FF]/).test(state.options[0].label) ? 'right' : 'left'
    }),
    placeholder: (provided: any) => ({
        ...provided,
        textAlign: document.getElementsByTagName("html")[0].dir === 'ltr' ? 'left' : 'right',
        fontWeight: 600
    }),
    singleValue: (provided: any) => ({
        ...provided,
        fontWeight: 600
    }),
}

export const selectStyle = {
    container: (base: any) => ({
        ...base,
    }),
    option: (provided: any, state: any) => ({
        ...provided,
        "&:hover": {
            color: "#ffffff",
            backgroundColor: "#31baab"
        },
        color: state.isSelected ? '#ffffff' : '#1e1e20',
        backgroundColor: state.isSelected ? '#027b7a' : '#ffffff',
        borderBottom: '1px dotted lightgray',
        padding: 10,
        textAlign: (/[\u0590-\u05FF]/).test(state.options[0].label) ? 'right' : 'left'
    }),
    menu: (base: any) => ({
        ...base,
        zIndex: 9999,
    }),
    control: () => ({
        width: 200,
        height: 50
    }),
    placeholder: (provided: any) => ({
        ...provided,
        textAlign: document.getElementsByTagName("html")[0].dir === 'ltr' ? 'left' : 'right',
        fontWeight: 600
    }),
    singleValue: (provided: any) => ({
        ...provided,
        fontWeight: 600
    }),
}


export const remarkStyles = {
    container: (base: any) => ({
        ...base,
        width: 170
    }),
    option: (provided: any, state: any) => ({
        ...provided,
        "&:hover": {
            color: "#ffffff",
            backgroundColor: "#31baab"
        },
        color: state.isSelected ? '#ffffff' : '#1e1e20',
        backgroundColor: state.isSelected ? '#027b7a' : '#ffffff',
        borderBottom: '1px dotted lightgray',
        padding: 10,
        textAlign: (/[\u0590-\u05FF]/).test(state.options[0].label) ? 'right' : 'left',
    }),
    control: (base: any, state: any) => ({
        ...base,
        boxShadow: state.menuIsOpen ? '0 0 0 1px #31baab' : 'none',
        width: 150,
        height: 50
    }),
    placeholder: (provided: any) => ({
        ...provided,
        textAlign: document.getElementsByTagName("html")[0].dir === 'ltr' ? 'left' : 'right',
        fontWeight: 600
    }),
    singleValue: (provided: any) => ({
        ...provided,
        fontWeight: 600
    }),
}
