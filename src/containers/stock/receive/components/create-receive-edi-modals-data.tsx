import * as React from 'react';

import { useIntl } from "react-intl"
import { GenericModals } from '@src/components/GenericModals/index';
import { ModalInterface, ModalsInterface, ModalTypes } from '@src/components/GenericModals/interfaces';
import InputText from '@src/components/input-text';
import { AgGrid } from '@src/components';
import { SignatureBox } from '@src/components/signature-box/signature-box';
import InputNumber from '@src/components/input-number';

interface Props {
    modalType: ModalTypes,
    toggleModal: (type: ModalTypes) => void;
    selectedRow: any;
    modalHandlers: any;
    rows: any;
}

export const CreateReceiveEDIModalsData: React.FC<Props> = props => {
    
    const { toggleModal, modalHandlers, modalType, rows, selectedRow } = props;

    const { formatMessage } = useIntl();
    const [storeKeeperName, setStoreKeeperName] = React.useState<any>()
    const [storeKeeperSignature, setStoreKeeperSignature] = React.useState<any>()
    const [driverName, setDriverName] = React.useState<any>()
    const [driverSignature, setDriverSignature] = React.useState<any>()
    const [carNumber, setCarNumber] = React.useState<any>();
    const [additionalText, setAdditionalText] = React.useState<any>(null);
    const storeKeeperInputRef = React.useRef<any>();
    const driverInputRef = React.useRef<any>();
    const carNumberInputRef = React.useRef<any>();
    const additionalTextInputRef = React.useRef<any>();

    const checkIfEnter = (e: any) => {
        if (e.which === 13 && modalType === ModalTypes.info) modalHandlers.info() 
    }

    const infoModal: ModalInterface = {
        isOpen: modalType === ModalTypes.info, toggle: () => toggleModal(ModalTypes.none),
        header: formatMessage({ id: 'WarningExceedingTheQuantityOrdered' }),
        body: <p> {formatMessage({ id: 'UnableToSetLargerQuantityThanTheQuantitySentInTheEDICertificate' })}</p>,
        buttons: [
            { color: 'primary', onClick: () => modalHandlers.info(), label: formatMessage({ id: 'Receive' }) },
        ]
    }

    const addModal: ModalInterface = {
        isOpen: modalType === ModalTypes.add, toggle: () => toggleModal(ModalTypes.none),
        classNames: 'min-width-50-rem',
        header: <div className="pl-1 pr-1 mr-1 ml-1">
            <p className="mt-1 mb-0-5">{formatMessage({ id: 'confirmReceiveEDICertificate' })}</p>
            {formatMessage({ id: 'from-' })} {selectedRow && selectedRow.SupplierName ? selectedRow.SupplierName : ''}
            , {formatMessage({ id: 'certificateNumber' })} - {selectedRow && selectedRow.ediNumber ? selectedRow.ediNumber : ''}
        </div>,
        body: <>
            <div className='d-flex flex-row justify-content-between pl-1 pr-1'>
                {[
                    {
                        id: 'storeKeeperName',
                        label: formatMessage({ id: 'storeKeeperName' }),
                        placeholder: formatMessage({ id: 'storeKeeperName' }),
                        state: setStoreKeeperName,
                        value: storeKeeperName,
                        inputRef: storeKeeperInputRef,
                        signatureLabel: formatMessage({ id: 'storeKeeperSignature' }),
                        signatureState: setStoreKeeperSignature
                    },
                    {
                        id: 'driverName',
                        label: formatMessage({ id: 'driverDetails' }),
                        placeholder: formatMessage({ id: 'driverName' }),
                        state: setDriverName,
                        value: driverName,
                        inputRef: driverInputRef,
                        signatureLabel: formatMessage({ id: 'driverSignature' }),
                        signatureState: setDriverSignature
                    }
                ].map((item, index) => {
                    return (
                        <div className="width-20-rem">
                            {index > 0 ?
                                <div>
                                    <label className="font-medium-1 pl-1 pr-1" htmlFor={item.id}>{item.label}</label>
                                    <InputText
                                        id={item.id}
                                        onChange={(e) => {
                                            if (e.target.value.trim().length === 0 || e.target.value == '') item.state(undefined)
                                            else item.state(e.target.value)
                                        }}
                                        value={item.value}
                                        placeholder={item.placeholder}
                                        inputRef={item.inputRef}
                                        inputClassNames="pl-1-5 pr-1-5"
                                        noMarginBottom
                                        noMarginRight
                                    />
                                </div> : null
                            }


                            {index > 0 ?
                                <InputNumber
                                    id={'carNumber'}
                                    onChange={(e) => {
                                        if (e.target.value.trim().length === 0 || e.target.value == '') setCarNumber(undefined)
                                        else setCarNumber(e.target.value)
                                    }}
                                    value={carNumber} placeholder={formatMessage({ id: 'carNumber' })}
                                    inputRef={carNumberInputRef}
                                    hideFocus
                                    inputHeight={'2.4rem'}
                                    classNames="mt-1 mb-1"
                                /> : <div className="mt-8-4-rem"> </div>}

                            <div className="d-flex justify-content-start align-items-start width-100-per pl-05 pr-05 mr-2 mb-1">
                                <SignatureBox
                                    onChange={(url: string | null) => item.signatureState(url)}
                                    height={'150px'}
                                    width={'250px'}
                                    label={item.signatureLabel}
                                />
                            </div>
                        </div>
                    )
                })}


            </div>

            <div className="pr-1 pl-1 ">
                <label className="font-medium-1 pl-1 pr-1 mt-1" htmlFor={'freeText'}>{formatMessage({ id: 'moreDetails' })}</label>

                <InputText
                    id={'freeText'}
                    onChange={(e) => {
                        if (e.target.value.trim().length === 0 || e.target.value == '') setAdditionalText(undefined)
                        else setAdditionalText(e.target.value)
                    }}
                    value={additionalText}
                    placeholder={' '}
                    inputRef={additionalTextInputRef}
                    inputClassNames="p-1 width-100-per no-resize height-5-rem"
                    noMarginBottom
                    noMarginRight
                    textarea
                />

            </div>
        </>,
        buttons: [
            {
                color: 'primary',
                onClick: () => {
                    const data = {
                        // applicatorName: storeKeeperName,
                        rows: rows,
                        receiverSignature: storeKeeperSignature,
                        providerPersonName: driverName,
                        providerSignature: driverSignature,
                        providerCarNumber: carNumber,
                        additionalText: additionalText ? additionalText : null,
                        ediNum : selectedRow.ediNumber
                    }
                    modalHandlers.add(data)
                },
                label: formatMessage({ id: 'confirm' }),
                disabled:
                    //  (storeKeeperName && storeKeeperName.trim().length > 0) &&
                    (driverName && driverName.trim().length > 0) &&
                        (carNumber && carNumber.trim().length > 0) &&
                        (driverSignature && driverSignature.trim().length > 0) &&
                        (storeKeeperSignature && storeKeeperSignature.trim().length > 0) ? false : true
            }
        ]
    }

    return (
        <GenericModals key={`createReturnModal`} onKeyPress={checkIfEnter} modal={modalType === ModalTypes.info ? infoModal : addModal} />
    );
};



export default CreateReceiveEDIModalsData;
