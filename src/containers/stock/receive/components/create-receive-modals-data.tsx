import * as React from 'react';

import { useIntl } from "react-intl"
import { GenericModals } from '@src/components/GenericModals/index';
import { ModalInterface, ModalsInterface, ModalTypes } from '@src/components/GenericModals/interfaces';
import InputText from '@src/components/input-text';
import { AgGrid } from '@src/components';
import { SignatureBox } from '@src/components/signature-box/signature-box';
import InputNumber from '@src/components/input-number';

interface Props {
    modalType: ModalTypes,
    toggleModal: (type: ModalTypes) => void;
    modalHandlers: any;
    supplier?: any;
    branch?: any;
    refNumber?: number;
    rows: any;
    date: any;
}

export const CreateReceiveModalsData: React.FC<Props> = props => {

    const { toggleModal, modalHandlers, modalType, supplier, branch, rows, refNumber, date } = props;


    const { formatMessage } = useIntl();
    const [storeKeeperName, setStoreKeeperName] = React.useState<any>()
    const [storeKeeperSignature, setStoreKeeperSignature] = React.useState<any>()
    const [driverName, setDriverName] = React.useState<any>()
    const [driverSignature, setDriverSignature] = React.useState<any>()
    const [boxesNumber, setBoxesNumber] = React.useState<any>(null);
    const [surfacesNumber, setSurfacesNumber] = React.useState<any>(null);
    const [carNumber, setCarNumber] = React.useState<any>();
    const [additionalText, setAdditionalText] = React.useState<any>(null);

    const boxesNumberInputRef = React.useRef<any>();
    const surfacesNumberInputRef = React.useRef<any>();
    const storeKeeperInputRef = React.useRef<any>();
    const driverInputRef = React.useRef<any>();
    const carNumberInputRef = React.useRef<any>();
    const additionalTextInputRef = React.useRef<any>();



    const fields = [
        { text: "BarCode", type: 'string', width: window.innerWidth * 0.03, minWidth: 100, },
        { text: "itemDescription", type: 'string', width: window.innerWidth * 0.03, minWidth: 200, },
        { text: "Ordered", type: 'string', width: window.innerWidth * 0.03, minWidth: 75, },
        { text: "receivedPackingFactor", type: 'string', width: window.innerWidth * 0.03, minWidth: 140, },
        { text: "remark", type: 'string', width: window.innerWidth * 0.03, minWidth: 150, }
    ];

    const infoModal: ModalInterface = {
        isOpen: modalType === ModalTypes.info, toggle: () => toggleModal(ModalTypes.none),
        header: formatMessage({ id: 'WarningExceedingTheQuantityOrdered' }),
        body: <p> {formatMessage({ id: 'youHaveReceivedAnItemWithALargerQuanitityThenOrdered' })}</p>,
        buttons: [
            { color: 'primary', onClick: () => modalHandlers.info(false), label: formatMessage({ id: 'no' }) },
            { color: 'primary', onClick: () => modalHandlers.info(true), label: formatMessage({ id: 'yesReceive' }), outline: true }
        ]
    }

    const [gridHeight, setGridHeight] = React.useState<number>(rows.length >= 10 ? 3.68 + (2.15 * 10) : rows.length >= 2 ? 3.68 + (2.15 * rows.length) : 3.68 + (2.15 * 2))

    const [currentLevel, setCurrentLevel] = React.useState<number>(0);

    const [rowsToShow, setRowsToShow] = React.useState<any>(rows.map((ele: any) => {
        return {
            ...ele,
            receivedPackingFactor: ele.savedReceive,
            remark: ele.savedRemark ? ele.savedRemark : ''
        }
    }))

    const handleNextLevel = () => {
        setCurrentLevel(currentLevel + 1);
    }

    const handleBackLevel = (removeFile: boolean) => {
        setCurrentLevel(currentLevel - 1);
    }


    const addModal: ModalInterface[] = [{
        isOpen: modalType === ModalTypes.add, toggle: () => toggleModal(ModalTypes.none),
        classNames: 'min-width-55-rem',
        header: <div className="pl-1 pr-1">
            <p className="mr-1 ml-1 mt-1 mb-0-5">{formatMessage({ id: 'confirmReceiveCertificate' })}</p>
            <p className="m r-1 ml-1">{formatMessage({ id: 'from-' })} {supplier && supplier.label ? supplier.label : ''}, {formatMessage({ id: 'certificateNumber' })}: {refNumber ? refNumber : ''}</p>
        </div>,
        body: <>
            <div className="pl-1 pr-1 mb-1">
                <AgGrid
                    defaultSortFieldNum={0}
                    descSort
                    id={'certificateNumber'}
                    gridHeight={`${gridHeight}rem`}
                    translateHeader
                    fields={[...fields]}
                    groups={[]}
                    totalRows={[]}
                    rows={rowsToShow}
                    resizable
                    customFieldsDirection={'ltr'}
                    customRowHeight={30}
                />
            </div>

            <div className="d-flex justify-content-between">
                {[
                    {
                        id: 'surfacesNumber',
                        label: formatMessage({ id: 'surfacesNumber' }),
                        state: setSurfacesNumber,
                        value: surfacesNumber,
                        inputRef: surfacesNumberInputRef,
                    },
                    {
                        id: 'boxesNumber',
                        label: formatMessage({ id: 'boxesNumber' }),
                        state: setBoxesNumber,
                        value: boxesNumber,
                        inputRef: boxesNumberInputRef,
                    }
                ].map((item) => {
                    return (
                        <div className="width-20-rem">
                            <label className="font-medium-1 pl-1 pr-1" htmlFor={item.id}>{item.label}</label>
                            <InputNumber
                                id={item.id}
                                onChange={(e) => {
                                    if (e.target.value.trim().length === 0 || e.target.value == '') item.state(undefined)
                                    else item.state(Number(e.target.value))
                                }}
                                value={item.value} placeholder={item.label}
                                inputRef={item.inputRef}
                                hideFocus={true}
                                inputHeight={'2.6rem'}
                                classNames="mr-1"
                            />
                        </div>
                    )
                })}
            </div>
        </>,
        buttons: [
            {
                color: 'primary',
                onClick: () => handleNextLevel(),
                label: formatMessage({ id: 'continue' }),
                disabled: false
            }
        ]
    },
    {
        isOpen: modalType === ModalTypes.add, toggle: () => toggleModal(ModalTypes.none),
        classNames: 'min-width-50-rem',
        header: <div className="pl-1 pr-1">
            <p className="mr-1 ml-1 mt-1 mb-0-5">{formatMessage({ id: 'confirmReceiveCertificate' })}</p>
            <p className="m r-1 ml-1">{formatMessage({ id: 'from-' })} {supplier && supplier.label ? supplier.label : ''}, {formatMessage({ id: 'certificateNumber' })}: {refNumber ? refNumber : ''}</p>
        </div>,
        body: <>
            <div className='d-flex flex-row justify-content-between pl-1 pr-1'>
                {[
                    {
                        id: 'storeKeeperName',
                        label: formatMessage({ id: 'storeKeeperName' }),
                        placeholder: formatMessage({ id: 'storeKeeperName' }),
                        state: setStoreKeeperName,
                        value: storeKeeperName,
                        inputRef: storeKeeperInputRef,
                        signatureLabel: formatMessage({ id: 'storeKeeperSignature' }),
                        signatureState: setStoreKeeperSignature
                    },
                    {
                        id: 'driverName',
                        label: formatMessage({ id: 'driverDetails' }),
                        placeholder: formatMessage({ id: 'driverName' }),
                        state: setDriverName,
                        value: driverName,
                        inputRef: driverInputRef,
                        signatureLabel: formatMessage({ id: 'driverSignature' }),
                        signatureState: setDriverSignature
                    }
                ].map((item, index) => {
                    return (
                        <div className="width-20-rem">
                            {index > 0 ?
                                <div>
                                    <label className="font-medium-1 pl-1 pr-1" htmlFor={item.id}>{item.label}</label>
                                    <InputText
                                        id={item.id}
                                        onChange={(e) => {
                                            if (e.target.value.trim().length === 0 || e.target.value == '') item.state(undefined)
                                            else item.state(e.target.value)
                                        }}
                                        value={item.value}
                                        placeholder={item.placeholder}
                                        inputRef={item.inputRef}
                                        inputClassNames="pl-1-5 pr-1-5"
                                        noMarginBottom
                                        noMarginRight
                                    />
                                </div> : null
                            }


                            {index > 0 ?
                                <InputNumber
                                    id={'carNumber'}
                                    onChange={(e) => {
                                        if (e.target.value.trim().length === 0 || e.target.value == '') setCarNumber(undefined)
                                        else setCarNumber(e.target.value)
                                    }}
                                    value={carNumber} placeholder={formatMessage({ id: 'carNumber' })}
                                    inputRef={carNumberInputRef}
                                    hideFocus
                                    inputHeight={'2.4rem'}
                                    classNames="mt-1 mb-1"
                                /> : <div className="mt-8-4-rem"> </div>}

                            <div className="d-flex justify-content-start align-items-start width-100-per pl-05 pr-05 mr-2 mb-1">
                                <SignatureBox
                                    onChange={(url: string | null) => item.signatureState(url)}
                                    height={'150px'}
                                    width={'250px'}
                                    label={item.signatureLabel}
                                />
                            </div>
                        </div>
                    )
                })}


            </div>

            <div className="pr-1 pl-1 ">
                <label className="font-medium-1 pl-1 pr-1 mt-1" htmlFor={'freeText'}>{formatMessage({ id: 'moreDetails' })}</label>

                <InputText
                    id={'freeText'}
                    onChange={(e) => {
                        if (e.target.value.trim().length === 0 || e.target.value == '') setAdditionalText(undefined)
                        else setAdditionalText(e.target.value)
                    }}
                    value={additionalText}
                    placeholder={' '}
                    inputRef={additionalTextInputRef}
                    inputClassNames="p-1 width-100-per no-resize height-5-rem"
                    noMarginBottom
                    noMarginRight
                    textarea
                />

            </div>
        </>,
        buttons: [
            {
                color: 'primary',
                onClick: () => {
                    const data = {
                        // applicatorName: storeKeeperName,
                        Rows: rowsToShow,
                        ReceiverSignature: storeKeeperSignature,
                        ProviderPersonName: driverName,
                        ProviderSignature: driverSignature,
                        ProviderCarNumber: carNumber,
                        mistch: surfacesNumber ? surfacesNumber : null,
                        maraz: boxesNumber ? boxesNumber : null,
                        AdditionalText: additionalText ? additionalText : null,
                        ReceiveDate: date,
                        SapakReferenceNum: refNumber,
                        BranchId: branch.value,
                        SapakId: supplier.value
                    }
                    modalHandlers.add(data)
                },
                label: formatMessage({ id: 'confirm' }),
                disabled:
                    //  (storeKeeperName && storeKeeperName.trim().length > 0) &&
                    (driverName && driverName.trim().length > 0) &&
                        (carNumber && carNumber.trim().length > 0) &&
                        (driverSignature && driverSignature.trim().length > 0) &&
                        (storeKeeperSignature && storeKeeperSignature.trim().length > 0) ? false : true
            }
        ]
    }]


    return (
        <GenericModals key={`createReturnModal`} modal={modalType === ModalTypes.info ? infoModal : addModal[currentLevel]} />
    );
};



export default CreateReceiveModalsData;
