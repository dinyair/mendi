
export interface RecieveRow {
	certificateNumber: number;
	referenceNumber: number;
	receiveDate: Date;
	BranchName: string;
	SupplierName: string;
	SupplierId:number;
	BranchId:number;
	amountOfItems: string | null;
	sumOfUnitsOrWeight: string | null;
	goodsReceiver: string;
	EDI: boolean;
	EDI_Disabled: boolean;
	providerName: string;
	recieverPhoneNumber : string
	logo:string;
	providerSignature:string;
	recieverSignature: string;
	additionalText: string;
	providerCarNumber:string;
	remarks : any[],
	boxesNumber:  any;
	surfacesNumber:any;
}

export interface EdiRow {
	ediNumber: number;
	date:string;
	BranchName:string;
	branchId:number;
	SupplierName:string;
	supplierId:number;
	amountOfItems:number;
	remark?:string;
}
