

import * as React from "react";
import * as moment from 'moment';
import { FormattedMessage } from "react-intl"

type Props = {
    text?: string,
    data: any
};

export class PrintReceiveCert extends React.PureComponent<Props> {
    private tableHeaders = [
        '#',
        <FormattedMessage id='BarCode' />,
        <FormattedMessage id='name' />,
        <FormattedMessage id='received' />,
        <FormattedMessage id='reservation' />,
    ];

    constructor(props: Props) {
        super(props);
    }

    public render() {
        return (
            <div className='print__element' style={{ width: '100%', margin: 'auto', direction: 'rtl' }}>
                <div style={{ width: '87%', margin: '2rem auto 2rem auto', display: 'flex', justifyContent: 'space-between' }}>
                    {this.props.data && this.props.data['logo'] &&
                        <img style={{ width: '35%' }} src={'https://grid.algoretail.co.il/assets/images/' + this.props.data.logo} alt={this.props.data.BranchName} />
                    }
                    <span style={{ color: 'black', fontSize: '1.5rem' }}><FormattedMessage id="IssueDate" />: {moment(this.props.data.receiveDate).format('DD/MM/YY HH:mm')}</span>
                </div>
                <div style={{ width: '87%', margin: '2rem auto 2rem auto', display: 'flex', justifyContent: 'center', alignContent: 'center' }}>
                    <span style={{ fontSize: '3rem', color: 'black', fontWeight: 'bolder' }}><FormattedMessage id="ReceiveCertificate" />: {this.props.data.certificateNumber}</span>
                </div>
                <div style={{ width: '87%', margin: '2rem auto 2rem auto', display: 'flex', justifyContent: 'center', alignContent: 'center' }}>
                    <span style={{ fontSize: '2rem', color: 'black', fontWeight: 'bolder' }}><FormattedMessage id="reference" />: {this.props.data.referenceNumber}</span>
                </div>
                <div style={{ width: '87%', margin: 'auto auto 2rem auto', display: 'flex', justifyContent: 'space-between' }}>
                    <div style={{ display: 'flex', flexDirection: 'column' }} >
                    <span style={{ fontSize: '1.5rem', color: 'black' }}><span style={{ fontWeight: 'bolder' }}><FormattedMessage id="branch" />:</span> {this.props.data.BranchName} ({this.props.data.BranchId})</span>
                    <span style={{ fontSize: '1.5rem', color: 'black' }}><span style={{ fontWeight: 'bolder' }}><FormattedMessage id="Supplier" />:</span> {this.props.data.SupplierName} ({this.props.data.SupplierId})</span>
                    </div>
                </div>
                <table style={{ margin: 'auto', width: '88%' }}>
                    <thead>
                        <tr>
                            {this.tableHeaders.map((title: any) => <td style={{ color: 'black', fontWeight: 'bold', padding: '0.7rem', border: '0.2px solid black' }} className='print__tableHeader' >{title}</td>)}
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.props.data && this.props.data.details && this.props.data.details.map((detail: any, index: number) => {
                                return (
                                    <tr style={{ backgroundColor: detail.PartlySupplied && detail.PartlySupplied === 1 ?  "#FFBBBB" : ""}}>
                                        <td style={{ color: 'black', padding: '0.7rem', border: '0.2px solid black' }} > {index + 1}</td>
                                        <td style={{ color: 'black', padding: '0.7rem', border: '0.2px solid black' }} > {detail.BarCode}</td>
                                        <td style={{ color: 'black', padding: '0.7rem', border: '0.2px solid black' }} >{detail.BarCodeName} </td>
                                        <td style={{ color: 'black', padding: '0.7rem', border: '0.2px solid black' }} >{detail.AmountReceive.toFixed(2)} </td>
                                        <td style={{ color: 'black', padding: '0.7rem', border: '0.2px solid black' }} >{detail.RemarkId ? this.props.data.remarks.find((ele: any) => ele.Id === detail.RemarkId).Name : ''}</td>

                                    </tr>
                                )
                            })
                        }

                        <tr>
                            <td style={{ fontWeight: 'bolder', backgroundColor: 'lightgray', color: 'black', padding: '0.7rem', border: '0.2px solid black' }}></td>
                            <td style={{ fontWeight: 'bolder', backgroundColor: 'lightgray', color: 'black', padding: '0.7rem', border: '0.2px solid black' }}></td>
                            <td style={{ fontWeight: 'bolder', backgroundColor: 'lightgray', color: 'black', padding: '0.7rem', border: '0.2px solid black' }}></td>
                            <td style={{ fontWeight: 'bolder', backgroundColor: 'lightgray', color: 'black', padding: '0.7rem', border: '0.2px solid black' }}><FormattedMessage id="aTotalOf" />: {this.props.data && this.props.data.details && this.props.data.details.map((ele: any) => ele.AmountReceive).reduce((a: any, b: any) => a + b).toFixed(2)}</td>
                            <td style={{ fontWeight: 'bolder', backgroundColor: 'lightgray', color: 'black', padding: '0.7rem', border: '0.2px solid black' }}></td>
                            {/* <td style={{ fontWeight: 'bolder', backgroundColor: 'lightgray', color: 'black', padding: '0.7rem', border: '0.2px solid black' }}><FormattedMessage id="aTotalOf" />: {this.props.data && this.props.data.details && this.props.data.details.map((ele: any) => ele.AmountPackage).reduce((a: any, b: any) => a + b).toFixed(2)}</td> */}
                        </tr>
                    </tbody>
                </table>



                {this.props.data.providerName !== null || this.props.data.providerCarNumber !== null || this.props.data.additionalText !== null ?
                    <div style={{ width: '87%', marginTop: '5rem', marginRight: 'auto', marginLeft: 'auto', display: 'flex', justifyContent: 'start' }}>
                        {this.props.data.providerName !== null ? <div style={{ display: 'flex', flexDirection: 'column' }} >
                            <span style={{ fontSize: '1.5rem', color: 'black', fontWeight: 'bolder' }}><FormattedMessage id="driverName" />: {this.props.data.providerName}</span>
                        </div> : null}

                        {this.props.data.providerCarNumber !== null ? <div style={{ display: 'flex', flexDirection: 'column', marginRight: this.props.data.providerName !== null ? '2rem' : '0rem' }} >
                            <span style={{ fontSize: '1.5rem', color: 'black', fontWeight: 'bolder' }}><FormattedMessage id="carNumber" />: {this.props.data.providerCarNumber}</span>
                        </div> : null}

                        {this.props.data.additionalText !== null ? <div style={{ display: 'flex', flexDirection: 'column', marginRight: this.props.data.providerCarNumber !== null || this.props.data.providerName !== null ? '2rem' : '0rem' }} >
                            <span style={{ fontSize: '1.5rem', color: 'black', fontWeight: 'bolder' }}><FormattedMessage id="additionalRemarks" />: {this.props.data.additionalText}</span>
                        </div> : null}
                    </div> : null
                }

                {
                    // this.props.data.surfacesNumber !== null || this.props.data.boxesNumber !== null  ?
                    <div style={{ width: '87%', marginTop: '2rem', marginRight: 'auto', marginLeft: 'auto', display: 'flex', justifyContent: 'start' }}>
                        {
                            // this.props.data.surfacesNumber !== null ? 
                            <div style={{ display: 'flex', flexDirection: 'column' }} >
                                <span style={{ fontSize: '1.5rem', color: 'black', fontWeight: 'bolder' }}><FormattedMessage id="surfacesNumber" />: {this.props.data.surfacesNumber}</span>
                            </div>
                            // : null
                        }

                        {
                            // this.props.data.boxesNumber !== null ?
                            <div style={{ display: 'flex', flexDirection: 'column', marginRight: '2rem' }} >
                                <span style={{ fontSize: '1.5rem', color: 'black', fontWeight: 'bolder' }}><FormattedMessage id="boxesNumber" />: {this.props.data.boxesNumber}</span>
                            </div>
                            // : null
                        }
                    </div>
                    //  : null
                }

                {this.props.data.recieverSignature !== null && this.props.data.recieverSignature !== '' ? <div style={{ width: '87%', marginTop: '5rem', marginRight: 'auto', marginLeft: 'auto', display: 'flex', justifyContent: 'space-between' }}>
                    <div style={{ display: 'flex', flexDirection: 'column' }} >
                        <span style={{ fontSize: '1.5rem', color: 'black', fontWeight: 'bolder' }}><FormattedMessage id="providerSignature" />:
                            {/* {this.props.data.providerName} */}
                        </span>
                        <img style={{ marginTop: '1rem' }} src={this.props.data.providerSignature} />
                    </div>

                    <div style={{ display: 'flex', flexDirection: 'column' }} >
                        <span style={{ fontSize: '1.5rem', color: 'black', fontWeight: 'bolder' }}><FormattedMessage id="storeKeeperSignature" />:
                            {/* {this.props.data.goodsReceiver} */}
                        </span>
                        <img style={{ marginTop: '1rem' }} src={this.props.data.recieverSignature} />
                    </div>
                </div> : null}

            </div >
        );
    }
}