import { getRequest, postRequest } from "@src/http";

export const getRemarks = async () => {
    const resp = await postRequest<any>('sapak-receive/remarks', {});
    return resp
}

export const getSignatures = async (date1: any, date2: any) => {
    const resp = await postRequest<any>('receive-manage/signatures', {date1,date2});
    return resp
}

export const getSignatureReceivedOrders = async (id:number) => {
    const resp = await postRequest<any>('receive-manage/signatureReceivedOrders', {ReceiveNum:id});
    return resp
}

export const getEdiRows = async () => {
    const resp = await getRequest<any>('tablet-api/pallet-receive/ordersj', {});
    return resp
}

export const getEdiRowOrders = async (ediNumber:number) =>{
    const resp = await getRequest<any>(`tablet-api/pallet-receive/newedi/${ediNumber}/orders`, {});
    return resp
}


export const sendRecieveEDIToApprove = async (data:any) => {
    const resp = await postRequest<any>('/tablet-api/pallet-receive/approve', data);
    return resp
}


export const getEdiBranchesByDate = async (date:any) => {
    const resp = await postRequest<any>('sapak-receive/branches', {AspakaDate:date});
    return resp
}

export const getEdiSuppliersByDate = async (date:any, branchId: number) => {
    const resp = await postRequest<any>('sapak-receive/sapakim', {AspakaDate:date, BranchIds: branchId});
    return resp
}

export const getEdiRefNumberVerify = async (branchId:number, supplierId:number,refNumber:number) => {
    const resp = await postRequest<any>('sapak-receive/reference/verify', {BranchId:branchId, SapakId: supplierId, SapakReferenceNum:refNumber});
    return resp
}


export const getEdiRecieveRowsByDate = async (date:any, branchId: number, supplierId:number) => {
    const resp = await postRequest<any>('sapak-receive/all', {AspakaDate:date, BranchId: branchId, SapakId:supplierId});
    return resp
}

export const getEdiOrdersRecievedRowsByDate = async (date:any, branchId: number, supplierId:number) => {
    const resp = await postRequest<any>('sapak-receive/orders/received', {AspakaDate:date, BranchId: branchId, SapakId:supplierId});
    return resp
}

export const getEdiCatalogItems = async (supplierId: number) => {
    const resp = await postRequest<any>('sapak-receive/catalogsapak', {sapak1:supplierId});
    return resp
}

export const sendRecieveToApprove = async (data:any) => {
    const resp = await postRequest<any>('sapak-receive/approve2', data);
    return resp
}


