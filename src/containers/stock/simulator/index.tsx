import * as React from 'react';
import * as moment from 'moment';
import { RootStore } from '@src/store';
import { Table, AgGrid, Icon, CalendarSingleInput } from '@components';
import { useSelector, useDispatch } from 'react-redux';
import { useIntl, FormattedMessage } from 'react-intl';
import { Spinner, Button } from 'reactstrap';
import { config } from '@src/config';
import { postTrending, getReccomendation } from './initial-state';
import { fetchAndUpdateStore } from '@src/helpers/helpers';
import AsyncSelect from 'react-select/async';
import BarChart from './barCharts';
import SweetAlert from 'react-bootstrap-sweetalert';
import classnames from 'classnames';
import DotRoute from './DotRoute';
import './index.scss';
import { Tooltip } from '@material-ui/core';

export const StockSimulator: React.FunctionComponent = () => {
	const { formatMessage } = useIntl();
	document.title = formatMessage({ id: 'order-simulator' });

	const dispatch = useDispatch();

	const catalog = useSelector((state: RootStore) => state._catalogUpdated);
	const userBranches = useSelector((state: RootStore) => state._userBranches);

	const [productImage, setProductImage] = React.useState<any>();
	const [productBarCode, setBarCode] = React.useState<any>();
	const [searchedProduct, setSearchedProduct] = React.useState<any>(formatMessage({ id: 'simulator-item' }));
	const [searchedProductName, setSearchedProductName] = React.useState<any>();
	let beginingOfYear: any = moment().startOf('year').format('YYYY/MM/DD');
	let beginingOfYearDateType = new Date(beginingOfYear);

	const [dateSimulator, setDateSimulator] = React.useState<any>(beginingOfYearDateType);
	const [date1, setDate1] = React.useState<any>(beginingOfYearDateType);
	const [date2, setDate2] = React.useState<any>(new Date());

	const [inputValue, setInputValue] = React.useState<any>(null);
	const [chosenBranch, setChosenBranch] = React.useState<any>(null);
	const [chosenSupplier, setChosenSupplier] = React.useState<any>(null);
	const [chosenProduct, setChosenProduct] = React.useState<any>();
	const [checkboxReplacment, setCheckboxReplacment] = React.useState<any>(true);
	const [supplierSupplyDays, setSupplierSupplyDays] = React.useState<any>([]);
	const [displayAllSales, setDisplayAllSales] = React.useState<any>(false);
	const [allSales, setAllSales] = React.useState<any>();
	const [weeklySales, setWeeklySales] = React.useState<any>();
	const [allDataBarCode, setAllDataBarCode] = React.useState<any>();
	const [averageRows, setAverageRows] = React.useState<any>([]);
	const [showProductModal, setShowProductModal] = React.useState<boolean>(false);
	const [subBarCodes, setSubBarCodes] = React.useState<any>(null);

	const [loadSpinner, setLoadSpinner] = React.useState<boolean>(false);
	const [isGridDataReady, setIsGridDataReady] = React.useState<boolean>(false);

	const [firstLoad, setFirstLoad] = React.useState<boolean>(true);

	const [updateChart, setUpdateChart] = React.useState<boolean>(false);
	const [subRowData, setSubRowsData] = React.useState<any>([]);
	const [activeDots, setActiveDots] = React.useState<any>([]);
	// const [activeDots, setActiveDots] = React.useState(['#31baab','#ff705d','green','#663300'])

	const [rows, setRows] = React.useState<any>();
	const [fields, setFields] = React.useState<any>([
		{
			text: 'day',
			type: 'string',
			width: window.innerWidth * 0.03,
			minWidth: 50,
			filter: 'agMultiColumnFilter',
			filterParams: {
				filters: [
					{ filter: 'agTextColumnFilter' },
					{ filter: 'agSetColumnFilter', filterParams: { suppressMiniFilter: true } }
				]
			}
		},
		{
			text: 'DateBuy',
			type: 'Date',
			width: window.innerWidth * 0.9,
			minWidth: 160,
			filter: 'agDateColumnFilter',
			cellRenderer: 'TooltipRender',
			customToolTip: [
				{
					if: 'flag_green',
					value: '1',
					customTooltipTitle: formatMessage({ id: 'inventoryCountDone' })
				}
			]
		},
		{ text: 'buy', type: 'fixedAndLocaleNumber', width: window.innerWidth * 0.1, minWidth: 100 },
		{
			text: 'return',
			type: 'fixedNumber',
			width: window.innerWidth * 0.1,
			minWidth: 100
		},
		{
			text: 'transfer',
			type: 'fixedAndLocaleNumber',
			width: window.innerWidth * 0.1,
			minWidth: 100,
			cellRenderer: 'TooltipRender',
			customToolTip: [
				{
					if: 'flag',
					value: '1',
					customTooltipTitle: formatMessage({ id: 'destroyDone' })
				}
			]
		},
		{
			text: 'sale',
			type: 'fixedAndLocaleNumber',
			width: window.innerWidth * 0.1,
			minWidth: 100,
			cellRenderer: 'TooltipRender',
			customToolTip: [
				{
					if: 'flag2',
					value: '1',
					customTooltipTitle: formatMessage({ id: 'AsOfNow' })
				},
				{
					if: 'virt_sale',
					value: '1',
					customTooltipTitle: formatMessage({ id: 'expectedSalesBased' })
				}
			]
		},
		{
			text: 'yitra',
			type: 'fixedAndLocaleNumber',
			totalRow: 'last',
			width: window.innerWidth * 0.1,
			minWidth: 100,
			cellRenderer: 'TooltipRender',
			customToolTip: [
				{
					if: 'flag_green',
					value: '1',
					customTooltipTitle: formatMessage({ id: 'inventoryCountDone' })
				}
			]
		},
		{
			text: 'price',
			totalRow: 'average',
			type: 'fixedAndLocaleNumber',
			width: window.innerWidth * 0.1,
			minWidth: 100
		}
	]);

	React.useEffect(() => {
		fetchAndUpdateStore(dispatch, [
			{ state: catalog, type: 'catalog' },
			{ state: userBranches, type: 'userBranches' }
		]);
	}, []);

	React.useEffect(() => {
		if (rows) {
			let sales: any = 0;
			rows.forEach((row: any) => {
				if (row.sale) {
					sales += row.sale;
				}
			});
			const allSalesFullNumber = Math.floor(sales).toLocaleString();
			setAllSales(`${formatMessage({ id: 'trending_allsales' })} - ${allSalesFullNumber}`);
		} else {
			setAllSales(null);
		}
	}, [rows]);

	const filterBranch = (inputValue: string) => {
		const branchesFiltered = userBranches.filter((i: any) => i.Name.includes(inputValue));
		let branchesMap: any[] = [];
		branchesFiltered && branchesFiltered[0]
			? (branchesMap = branchesFiltered.map((option: any, index: number) => {
					return { index, value: option.BranchId, label: option.Name };
			  }))
			: [];
		return branchesMap;
	};
	const filterCatalog = (inputValue: string) => {
		const regex = /\d/;
		const doesContainNumber = regex.test(inputValue);
		const catalogFiltered = catalog.filter((i: any) => {
			return i.Name.includes(inputValue) || String(i.BarCode).includes(inputValue);
		});
		let catalogMap: any[] = [];
		console.log('111', catalog);
		catalogFiltered && catalogFiltered[0]
			? (catalogMap = catalogFiltered.map((option: any, index: number) => {
					return {
						index,
						value: option.BarCode,
						supplierId: option.SapakId,
						label: `${option.Name} ${option.BarCode}`,
						name: option.Name
					};
			  }))
			: [];

		if (doesContainNumber) {
			let x = catalogMap.sort((a, b) => a.value - b.value);
			x.sort;
		} else {
			catalogMap.sort((a, b) => a.label.length - b.label.length);
		}
		return catalogMap.slice(0, 220);
	};
	const handleInputChange = (newValue: string) => {
		setInputValue(inputValue);
		return inputValue;
	};
	const loadCatalogOptions = (inputValue: string, callback: any) => {
		if (inputValue.length >= 3) {
			callback(filterCatalog(inputValue));
		} else {
			callback();
		}
	};
	const loadBranchOptions = (inputValue: string, callback: any) => {
		callback(filterBranch(inputValue));
	};
	const branchStyles = {
		container: (base: any) => ({
			...base
		}),
		option: (provided: any, state: any) => ({
			...provided,

			'&:hover': {
				color: '#ffffff',
				backgroundColor: '#31baab'
			},
			color: state.isSelected ? '#ffffff' : '#1e1e20',
			backgroundColor: state.isSelected ? '#027b7a' : '#ffffff',
			borderBottom: '1px dotted lightgray',
			padding: 10
		}),
		control: () => ({
			width: 200,
			height: 50
		})
	};
	const productStyles = {
		container: (base: any) => ({
			...base
		}),
		option: (provided: any, state: any) => ({
			...provided,
			...provided,

			'&:hover': {
				backgroundColor: '#31baab'
			},
			color: state.isSelected ? '#ffffff' : '#1e1e20',
			backgroundColor: state.isSelected ? '#027b7a' : '#ffffff',
			borderBottom: '1px dotted lightgray',
			padding: 10
		}),
		control: styles => ({
			width: '15rem!important'
		})
	};

	const checkIfEnter = (e: any) => {
		if (e.which === 13) handleSearch();
	};

	const handleSearch = () => {
		if (chosenProduct) {
			setLoadSpinner(true);
			setUpdateChart(true);
			handleDefaultSearch();
		}
	};

	const toggleShowProductModal = () => {
		setShowProductModal(!showProductModal);
	};

	const whatDayLetter = (dayNumber: any) => {
		let day = '';
		switch (dayNumber) {
			case 1:
				day = 'sunday-full';
				break;
			case 2:
				day = 'monday-full';
				break;
			case 3:
				day = 'tuesday-full';
				break;
			case 4:
				day = 'wednesday-full';
				break;
			case 5:
				day = 'thursday-full';
				break;
			case 6:
				day = 'friday-full';
				break;
			case 7:
				day = 'saturday-full';
				break;
		}
		return day;
	};
	const handleDefaultSearch = async () => {
		setSubRowsData([]);
		let date1default;
		let date2default;
		let rec;

		if (date1) date1default = moment(new Date(date1)).subtract(1, 'days').format('YYYY/MM/DD');
		else date1default = moment(new Date('2021/01/01')).subtract(1, 'days').format('YYYY/MM/DD');

		if (date2) date2default = moment(new Date(date2)).format('YYYY-MM-DD');
		else date2default = moment(new Date()).format('YYYY-MM-DD');

		if (chosenProduct) {
			rec = {
				barcode: String(chosenProduct),
				branchId: chosenBranch,
				date1: date1default,
				date2: date2default,
				sub_bar: checkboxReplacment,
				series: false
			};
			let rec1 = {
				branchId: chosenBranch,
				sapakId: chosenSupplier,
				date1: moment(new Date()).format('YYYY-MM-DD'),
				isDraft: 0,
				barcode: chosenProduct
			};
			const postResponse = await postTrending(rec);
			const getOrderReccomendation = await getReccomendation(rec1);

			if (getOrderReccomendation && getOrderReccomendation[0] && getOrderReccomendation[0][0]) {
				let avgData = [];
				for (let [key, value] of Object.entries(getOrderReccomendation[0][0].additionalData.avgData)) {
					avgData.push(value);
				}
				setAverageRows(
					avgData.map((i: any) => {
						let day = formatMessage({ id: whatDayLetter(i.dayofweek) });
						let average = i.averageDay;
						let average52 = i.avg52;
						let average8 = i.avg8;
						let average2 = i.avg2;
						return { day, average, average52, average8, average2 };
					})
				);

				setAllDataBarCode(getOrderReccomendation[0][0]);
				let newBarcodeData = getOrderReccomendation[0][0].additionalData;
				let supplyDays = getOrderReccomendation[0][0].additionalData.supplierSupplyDays;
				supplyDays.forEach((i: any, index: number) => {
					let day = newBarcodeData[`orderDay2Supply-${index}`];
					newBarcodeData[`orderDay2Supply-${index}`] = `${formatMessage({
						id: whatDayLetter(day.from)
					})} ${formatMessage({ id: 'to' })} ${formatMessage({ id: whatDayLetter(day.to) })}`;
				});
				setSupplierSupplyDays(supplyDays);
				setSubRowsData([newBarcodeData]);

				let subBars = getOrderReccomendation[0][0].additionalData.subBarCode;
				let subBarsArr = [];
				if (subBars) {
					for (const key of Object.keys(subBars)) {
						console.log(key, subBars[key]);
						if (String(key).includes('br') && String(key) != 'br1' && String(subBars[key].length)) {
							subBarsArr.push(subBars[key]);
						}
					}
				}
				console.log({ subBarsArr });
				setSubBarCodes(subBarsArr);
			}
			// orderDay2Supply

			const postMapped = postResponse.map((item: any) => {
				let buy = item.buy ? item.buy : 0;
				let sale = item.sale ? item.sale : 0;
				let yitra = item.yitra ? item.yitra : 0;
				let transfer = item.transfer ? item.transfer : 0;
				let price = item.price ? item.price : 0;
				let day = formatMessage({ id: item.day });
				return { ...item, day, buy, sale, yitra, transfer, price, return: item.return ? item.return : 0 };
			});
			if (postResponse.length) {
				// setRows([]);
				setRows(postMapped);
				setFirstLoad(false);
				setIsGridDataReady(true);
				setUpdateChart(false);
			} else {
				setRows([]);
				setIsGridDataReady(true);
				setUpdateChart(false);
			}

			setSearchedProduct(`${formatMessage({ id: 'simulator-item' })} - ${searchedProductName}`);

			setProductImage(config.productsImages + chosenProduct + '.jpg');
			setBarCode(chosenProduct);
			setDisplayAllSales(true);
			setLoadSpinner(false);
		}
	};
	const dotData = (color: string, opacity?: boolean, noMargin?: boolean) => {
		return (
			<div
				className={classnames(
					'border-white border-1 cursor-pointer oval rounded zindex-10002  badge-up text-bold-600 text-white height-0-9-rem width-0-9-rem mt-auto mb-auto',
					{ 'opacity-05': opacity, 'ml-auto': !noMargin, 'mr-1': !noMargin }
				)}
				style={{
					backgroundColor: color,
					top: '-0.2rem',
					right: '-0.5rem',
					border: '0.9rem solid white'
				}}
			>
				&nbsp;
			</div>
		);
	};
	const tableClick = (color: string, activeDotsForEach?: any, isSub?: boolean) => {
		let newActiveDots: any = activeDotsForEach ? activeDotsForEach : activeDots;
		if (!isSub) {
			if (newActiveDots.includes(color)) {
				setActiveDots(newActiveDots.filter((g: any) => g != color));
				return newActiveDots.filter((g: any) => g != color);
			} else {
				setActiveDots([...newActiveDots, color]);
				return [...newActiveDots, color];
			}
		}
	};

	// if (subRowData && subRowData[0]) {
	// 	console.log(moment(subRowData[0].supplyDate, 'YYYY-MM-DD').diff(moment(new Date(), 'YYYY-MM-DD'), 'days') + 1);
	// }
	return (
		<>
			<div className="d-flex-column" key={'itemtrends'}>
				<div className="d-flex mb-2">
					<div>
						<div className="d-flex width-100-per">
							{!firstLoad ? (
								<div className="d-flex-column mb-08">
									<div className="d-flex ml-05">
										<div>
											<h2 className="mr-2 align-self-center text-bold-600 mb-0">
												{searchedProduct}
											</h2>
											<span className="text-bold-600">
												{formatMessage({ id: 'BarCode' })}: {productBarCode}
											</span>
											{subRowData && subRowData.length && subRowData[0].isOrdered ? (
												<div className="d-flex-row">
													<span className="d-flex">
														<span>
															{formatMessage({ id: 'status' })}:{' '}
															<span className="text-bold-600">
																{formatMessage({ id: 'ordered' })} -{' '}
																{moment(subRowData[0].orderDate).format('DD.MM')}
															</span>
														</span>
														<span className="ml-05">{dotData('green', false, true)} </span>
													</span>
													<span>
														<span className="mr-03">{formatMessage({ id: 'units' })}:</span>
														<span className="text-bold-600">
															{allDataBarCode.ariza > 0
																? subRowData[0].orderedAmount * allDataBarCode.ariza
																: subRowData[0].orderedAmount}
														</span>
													</span>
													{allDataBarCode.ariza > 0 && (
														<span className="ml-1">
															<span className="mr-03">
																{formatMessage({ id: 'boxs' })}:
															</span>{' '}
															<span className="text-bold-600">
																{subRowData[0].orderedAmount}
															</span>
														</span>
													)}
												</div>
											) : null}
										</div>
										<img
											onClick={toggleShowProductModal}
											className="productImage cursor-pointer"
											src={productImage}
										/>
									</div>
								</div>
							) : (
								<div className="d-flex-column mb-08">
									<div className="d-flex ml-05">
										<div>
											<h2 className="mr-2 align-self-center text-bold-600 mb-0">
												{searchedProduct}
											</h2>
										</div>
									</div>
								</div>
							)}
							<div className="height-4-vh mb-2-vh d-flex align-items-center">
								{[
									{
										name: 'branch',
										isMulti: false,
										allowSelectAll: false,
										loadOptions: loadBranchOptions,
										text: formatMessage({ id: 'trending_branch' }),
										defaultOptions:
											userBranches && userBranches[0]
												? userBranches.map((option: any, index: number) => {
														return { index, value: option.BranchId, label: option.Name };
												  })
												: [],
										onChange: null,
										styles: branchStyles
									},
									{
										name: 'product',
										isMulti: false,
										allowSelectAll: false,
										text: formatMessage({ id: 'trending_product' }),
										loadOptions: loadCatalogOptions,
										defaultOptions: false,
										styles: productStyles
									}
								].map(item => {
									return (
										<div key={item.text} className="ml-1">
											<AsyncSelect
												name={item.name}
												isDisabled={
													catalog &&
													catalog.length > 0 &&
													userBranches &&
													userBranches.length > 0
														? loadSpinner
															? true
															: false
														: true
												}
												isClearable={true}
												styles={item.styles}
												loadOptions={item.loadOptions}
												defaultOptions={item.defaultOptions}
												onInputChange={handleInputChange}
												onKeyDown={e => checkIfEnter(e)}
												onChange={(e: any, actionMeta: any) => {
													if (e) {
														if (actionMeta.name === 'product') {
															setChosenSupplier(e.supplierId);
															setChosenProduct(e.value);
															setSearchedProductName(e.name);
														} else {
															setChosenBranch(e.value);
														}
													} else {
														if (actionMeta.name === 'product') {
															setChosenSupplier(null);
															setChosenProduct(null);
														} else setChosenBranch(null);
													}
												}}
												classNamePrefix="select"
												noOptionsMessage={() =>
													formatMessage({ id: 'trending_productMinimumSearch' })
												}
												placeholder={item.text}
											/>
										</div>
									);
								})}

								{[
									{
										text: formatMessage({ id: 'trending_replament' }),
										isActive: checkboxReplacment,
										type: 'checkbox',
										onChange: () => setCheckboxReplacment(!checkboxReplacment)
									}
								].map(item => {
									let className: string = 'checkBoxBorder cursor-pointer';
									if (item.isActive) className = 'checkBoxSelected checkBoxBorder cursor-pointer';
									return (
										<div
											key={item.text}
											className="elipsis d-flex justify-content-center align-items-center ml-2"
											onClick={item.onChange}
										>
											<div className={className}></div>
											<label className="mr-05 font-medium-1 text-bold-600" key={item.text}>
												{item.text}
											</label>
										</div>
									);
								})}

								<div className="divider ml-1 mr-1"></div>
								{/* <div>
								<CalendarSingleInput
									onChange={setDateSimulator}
									calanderId={'aaaa'}
									inputId={'fdfff'}
									// setDate2={setDate2}
									date={dateSimulator}
									// date1={date1}
									top={'0rem'}
								/>
								</div>
								<div className="divider ml-1 mr-1"></div> */}

								<Button
									color="primary"
									disabled={chosenBranch && chosenProduct && !loadSpinner ? false : true}
									className="ml-1 round text-bold-400 d-flex justify-content-center align-items-center width-7-rem height-2-rem cursor-pointer btn-primary "
									onClick={handleSearch}
								>
									<FormattedMessage id="trending_sortButton" />
								</Button>
							</div>
						</div>

						{showProductModal ? (
							<SweetAlert
								custom
								showCloseButton
								title=""
								showCancel={false}
								showConfirm={false}
								onCancel={toggleShowProductModal}
								onConfirm={() => {}}
								customClass={'width-10-per min-width-25-rem'}
							>
								<h2 className="text-bold-600">{searchedProductName}</h2>
								<h3 className="mb-3">
									{formatMessage({ id: 'BarCode' })}: {productBarCode}
								</h3>
								<img className="productImageModal mb-2" src={productImage} alt="" />
							</SweetAlert>
						) : null}

						{loadSpinner ? (
							<div className="mt-1-vh ml-05">
								<Spinner />
							</div>
						) : (
							<div className="mt-1-5vh">
								<div>
									{rows ? (
										<AgGrid
											customRowHeight={36}
											resizable
											headerHeight={30}
											defaultSortFieldNum={1}
											rowBufferAmount={55}
											gridHeight={'35vh'}
											floatFilter
											translateHeader
											fields={fields}
											groups={[]}
											totalRows={['buy', 'sale', 'yitra', 'transfer', 'return', 'price']}
											rows={rows}
											checkboxFirstColumn={false}
											cellRedFields={['transfer']}
											danger={'flag'}
											cellGreenFields={['DateBuy', 'yitra']}
											successDark={'flag_green'}
											yellowField={'sale'}
											yellowFlag={'virt_sale'}
											blueField={'sale'}
											blueFlag={'flag2'}
										/>
									) : null}
								</div>
							</div>
						)}
					</div>
					{isGridDataReady && !loadSpinner && (
						<div className="charts diFlex ml-5 min-width-25-rem">
							<div className="ml-1-5 d-flex justify-content-end mt-auto">
								<div className="width-100-per d-flex flex-direction-row mt-auto">
									{displayAllSales ? <h3 className="allSales text-bold-600">{allSales}</h3> : null}
									{displayAllSales ? <h3 className="text-bold-600 ml-1">{weeklySales}</h3> : null}
								</div>
							</div>
							<div className="barChartsContainer">
								<div className="bg-gray chartFather align-items-center">
									<h3 className="mr-auto text-bold-600 ml-1">
										{formatMessage({ id: 'trending_avg-sales-days' })}
									</h3>
									<BarChart
										displayDataLabels
										updateChart={updateChart}
										type={'week'}
										date1={date1 ? date1 : moment(new Date('2021/01/01')).format('YYYY/MM/DD')}
										chosenProduct={chosenProduct}
										chosenBranch={chosenBranch}
										checkboxReplacment={checkboxReplacment}
										categories={[
											formatMessage({ id: 'sunday' }),
											formatMessage({ id: 'monday' }),
											formatMessage({ id: 'tuesday' }),
											formatMessage({ id: 'wednesday' }),
											formatMessage({ id: 'thursday' }),
											formatMessage({ id: 'friday' }),
											formatMessage({ id: 'saturday' })
										]}
										tickAmount={0}
										onWeekSalesReady={(sales: number) => {
											setWeeklySales(
												`${formatMessage({ id: 'weekly-avg-sales' })} - ${Math.floor(
													sales
												).toLocaleString()}`
											);
										}}
									/>
								</div>
								<div className="bg-gray chartFather align-items-center mt-1-vh">
									<h3 className="mr-auto ml-1 text-bold-600">
										{formatMessage({ id: 'trending_sales-month' })}
									</h3>
									<BarChart
										displayDataLabels={false}
										updateChart={updateChart}
										type={'month'}
										chosenProduct={chosenProduct}
										chosenBranch={chosenBranch}
										checkboxReplacment={checkboxReplacment}
										tickAmount={0}
										categories={[
											formatMessage({ id: 'January-short' }),
											formatMessage({ id: 'February-short' }),
											formatMessage({ id: 'March-short' }),
											formatMessage({ id: 'April-short' }),
											formatMessage({ id: 'May-short' }),
											formatMessage({ id: 'June-short' }),
											formatMessage({ id: 'July-short' }),
											formatMessage({ id: 'August-short' }),
											formatMessage({ id: 'September-short' }),
											formatMessage({ id: 'October-short' }),
											formatMessage({ id: 'November-short' }),
											formatMessage({ id: 'December-short' })
										]}
									/>
								</div>
							</div>
						</div>
					)}
				</div>

				{subRowData && subRowData.length ? (
					<div className="d-flex-column max-height-38-vh overflow-x-hidden overflow-y-auto">
						<div className="width-100-per gridMain">
							<div className="gridItem">
								<span className="mt-05 text-bold-600 font-medium-2">
									{formatMessage({ id: 'item_data' })}
								</span>
								<AgGrid
									customRowHeight={36}
									resizable
									headerHeight={30}
									rowBufferAmount={55}
									gridHeight={'10vh'}
									translateHeader
									fields={[
										{
											text: 'createDate',
											type: 'Date',
											width: window.innerWidth * 0.1,
											minWidth: 100
										},
										{
											text: 'division',
											type: 'string',
											width: window.innerWidth * 0.11,
											minWidth: 110
										},
										{
											text: 'group',
											type: 'string',
											width: window.innerWidth * 0.11,
											minWidth: 110
										},
										{
											text: 'subGroup',
											type: 'string',
											width: window.innerWidth * 0.11,
											minWidth: 110
										},
										{
											text: 'series',
											cellRenderer: 'TranslateCell',
											type: 'string',
											width: window.innerWidth * 0.03,
											minWidth: 50
										},
										{
											text: 'supplier',
											type: 'string',
											width: window.innerWidth * 0.11,
											minWidth: 110
										},
										{
											text: 'packingFactor',
											type: 'string',
											width: window.innerWidth * 0.03,
											minWidth: 50
										},
										{
											text: 'isShakele',
											type: 'string',
											cellRenderer: 'TranslateCell',
											width: window.innerWidth * 0.03,
											minWidth: 50
										},
										{
											text: 'inventoryType',
											type: 'string',
											cellRenderer: 'TranslateCell',
											width: window.innerWidth * 0.03,
											minWidth: 50
										},
										{
											text: 'trash',
											type: 'string',
											width: window.innerWidth * 0.03,
											minWidth: 50
										}
									]}
									groups={[]}
									totalRows={[]}
									rows={subRowData}
									checkboxFirstColumn={false}
									cellRedFields={[]}
								/>
							</div>
							<div className="gridItem">
								<div className="customGridItem">
									<div>
										<span className="mt-05 text-bold-600 font-medium-2">
											{formatMessage({ id: 'lowSale' })}
										</span>
										<AgGrid
											customRowHeight={36}
											resizable
											headerHeight={30}
											rowBufferAmount={55}
											gridHeight={'9vh'}
											translateHeader
											fields={[
												{
													text: 'weeks',
													type: 'number',
													width: window.innerWidth * 0.03,
													minWidth: 50
												},
												{
													text: 'maxAmount',
													type: 'string',
													width: window.innerWidth * 0.03,
													minWidth: 50
												},
												{
													text: 'sMaxAmount',
													type: 'string',
													width: window.innerWidth * 0.03,
													minWidth: 50
												},
												{
													text: 'p_addition',
													type: 'percentage',
													width: window.innerWidth * 0.03,
													minWidth: 50
												}
											]}
											groups={[]}
											totalRows={[]}
											rows={subRowData}
											checkboxFirstColumn={false}
											cellRedFields={[]}
										/>
									</div>
									<div>
										<span className="mt-05 text-bold-600 font-medium-2">
											{formatMessage({ id: 'look-simulator' })}
										</span>
										<AgGrid
											customRowHeight={36}
											resizable
											rowBufferAmount={55}
											headerHeight={30}
											gridHeight={'9vh'}
											translateHeader
											fields={[
												{
													text: 'weeklySales',
													type: 'number',
													width: window.innerWidth * 0.03,
													minWidth: 50
												},
												{
													text: 'shelfAbove',
													type: 'percentage',
													width: window.innerWidth * 0.03,
													minWidth: 50
												},
												{
													text: 'shelfUnder',
													type: 'percentage',
													width: window.innerWidth * 0.03,
													minWidth: 50
												}
											]}
											groups={[]}
											totalRows={[]}
											rows={subRowData}
											checkboxFirstColumn={false}
											cellRedFields={[]}
										/>
									</div>
									<div>
										<span className="mt-05 text-bold-600 font-medium-2">
											{formatMessage({ id: 'lookCloseTerm' })}
										</span>
										<AgGrid
											customRowHeight={36}
											resizable
											rowBufferAmount={55}
											gridHeight={'9vh'}
											headerHeight={30}
											translateHeader
											fields={[
												{
													text: 'a-above',
													type: 'number',
													width: window.innerWidth * 0.03,
													minWidth: 50
												},
												{
													text: 'a-under',
													type: 'number',
													width: window.innerWidth * 0.03,
													minWidth: 50
												},
												{
													text: 'b-above',
													type: 'number',
													width: window.innerWidth * 0.03,
													minWidth: 50
												},
												{
													text: 'b-under',
													type: 'number',
													width: window.innerWidth * 0.03,
													minWidth: 50
												},
												{
													text: 'c-above',
													type: 'number',
													width: window.innerWidth * 0.03,
													minWidth: 50
												},
												{
													text: 'c-under',
													type: 'number',
													width: window.innerWidth * 0.03,
													minWidth: 50
												},
												{
													text: 'd-above',
													type: 'number',
													width: window.innerWidth * 0.03,
													minWidth: 50
												},
												{
													text: 'd-under',
													type: 'number',
													width: window.innerWidth * 0.03,
													minWidth: 50
												},
												{
													text: 'e-above',
													type: 'number',
													width: window.innerWidth * 0.03,
													minWidth: 50
												},
												{
													text: 'e-under',
													type: 'number',
													width: window.innerWidth * 0.03,
													minWidth: 50
												},
												{
													text: 'f-above',
													type: 'string',
													width: window.innerWidth * 0.03,
													minWidth: 50
												},
												{
													text: 'f-under',
													type: 'string',
													width: window.innerWidth * 0.03,
													minWidth: 50
												}
											]}
											groups={[]}
											totalRows={[]}
											rows={subRowData}
											checkboxFirstColumn={false}
											cellRedFields={[]}
										/>
									</div>
								</div>
							</div>
							<div className="gridItem">
								<span className="mt-05 text-bold-600 font-medium-2">
									{formatMessage({ id: 'supplyDays_info' })}
								</span>
								<AgGrid
									customRowHeight={36}
									resizable
									rowBufferAmount={55}
									gridHeight={'9vh'}
									headerHeight={30}
									translateHeader
									fields={[
										{
											text: 'supplier',
											type: 'string',
											width: window.innerWidth * 0.03,
											minWidth: 50
										},
										{
											text: 'isWensell',
											type: 'string',
											cellRenderer: 'TranslateCell',
											width: window.innerWidth * 0.03,
											minWidth: 50
										},
										...supplierSupplyDays.map((i: any, index: number) => {
											return {
												text: `orderDay2Supply-${index}`,
												type: 'string',
												width: window.innerWidth * 0.03,
												minWidth: 50
											};
										})
									]}
									groups={[]}
									totalRows={[]}
									rows={subRowData}
									checkboxFirstColumn={false}
									cellRedFields={[]}
								/>
							</div>
							<div className="gridItem">
								<span className="mt-05 text-bold-600 font-medium-2">
									{formatMessage({ id: 'truma_info' })}
								</span>
								<AgGrid
									customRowHeight={36}
									resizable
									headerHeight={30}
									rowBufferAmount={55}
									gridHeight={'9vh'}
									translateHeader
									fields={[
										{
											text: 't_total',
											type: 'percentage',
											width: window.innerWidth * 0.03,
											minWidth: 50
										},
										{
											text: 't_classes',
											type: 'string',
											width: window.innerWidth * 0.03,
											minWidth: 50
										},
										{
											text: 't_group',
											type: 'string',
											width: window.innerWidth * 0.03,
											minWidth: 50
										},
										{
											text: 't_segment',
											type: 'string',
											width: window.innerWidth * 0.03,
											minWidth: 50
										},
										{
											text: 't_subgroup',
											type: 'string',
											width: window.innerWidth * 0.03,
											minWidth: 50
										}
									]}
									groups={[]}
									totalRows={[]}
									rows={subRowData}
									checkboxFirstColumn={false}
									cellRedFields={[]}
								/>
							</div>
							<div className="gridItem">
								<span className="mt-05 text-bold-600 font-medium-2">
									{formatMessage({ id: 'replament_info' })}
								</span>
								<AgGrid
									customRowHeight={36}
									rowBufferAmount={55}
									gridHeight={'9vh'}
									headerHeight={30}
									translateHeader
									fields={[
										{
											text: 'br1',
											type: 'number',
											width: window.innerWidth * 0.03,
											minWidth: 50
										},
										{
											text: 'br2',
											type: 'number',
											width: window.innerWidth * 0.03,
											minWidth: 50
										},
										{
											text: 'br3',
											type: 'number',
											width: window.innerWidth * 0.03,
											minWidth: 50
										},
										{
											text: 'br4',
											type: 'number',
											width: window.innerWidth * 0.03,
											minWidth: 50
										},
										{
											text: 'br5',
											type: 'number',
											width: window.innerWidth * 0.03,
											minWidth: 50
										},
										{
											text: 'br6',
											type: 'number',
											width: window.innerWidth * 0.03,
											minWidth: 50
										},
										{
											text: 'br7',
											type: 'number',
											width: window.innerWidth * 0.03,
											minWidth: 50
										},
										{
											text: 'br8',
											type: 'number',
											width: window.innerWidth * 0.03,
											minWidth: 50
										},
										{
											text: 'br9',
											type: 'number',
											width: window.innerWidth * 0.03,
											minWidth: 50
										},
										{
											text: 'br10',
											type: 'number',
											width: window.innerWidth * 0.03,
											minWidth: 50
										}
									]}
									groups={[]}
									totalRows={[]}
									rows={subRowData && subBarCodes ? subBarCodes : []}
									checkboxFirstColumn={false}
									cellRedFields={[]}
								/>
							</div>

							<div className="gridItemSmall">
								<span className="mt-05 text-bold-600 font-medium-2">
									{formatMessage({ id: 'min_order' })}
								</span>
								<AgGrid
									customRowHeight={36}
									rowBufferAmount={55}
									gridHeight={'9vh'}
									translateHeader
									headerHeight={30}
									fields={[
										{
											text: 'stockMini',
											type: 'number',
											width: window.innerWidth * 0.03,
											minWidth: 50
										},
										{
											text: 'minimumItemsManual',
											type: 'number',
											width: window.innerWidth * 0.03,
											minWidth: 50
										}
									]}
									groups={[]}
									totalRows={[]}
									rows={subRowData}
									checkboxFirstColumn={false}
									// cellRedFields={['minimumItemsManual']}
									dangerUnderZero={'minimumItemsManual'}
								/>
							</div>
							<div className="gridItemSmall">
								<span>פרטי מבצע</span>
								<AgGrid
									customRowHeight={36}
									resizable
									rowBufferAmount={55}
									gridHeight={'9vh'}
									headerHeight={30}
									translateHeader
									fields={[
										{
											text: 'promoFromDate',
											type: 'Date',
											width: window.innerWidth * 0.03,
											minWidth: 50
										},
										{
											text: 'promoToDate',
											type: 'Date',
											width: window.innerWidth * 0.03,
											minWidth: 50
										},
										{
											text: 'promoData1',
											type: 'string',
											width: window.innerWidth * 0.03,
											minWidth: 50
										},
										{
											text: 'promoData2',
											type: 'string',
											width: window.innerWidth * 0.03,
											minWidth: 50
										}
									]}
									groups={[]}
									totalRows={[]}
									rows={allDataBarCode && allDataBarCode.isPromo ? [allDataBarCode] : []}
									checkboxFirstColumn={false}
									cellRedFields={[]}
								/>
							</div>

							<div className="gridItemAverage">
								<span className="mt-05 text-bold-600 font-medium-2">
									{formatMessage({ id: 'avg_info' })}
								</span>
								<AgGrid
									customRowHeight={36}
									rowBufferAmount={55}
									gridHeight={'26vh'}
									translateHeader
									headerHeight={30}
									noSort
									fields={[
										{
											text: 'day',
											type: 'string',
											width: window.innerWidth * 0.03,
											minWidth: 50
										},
										{
											text: 'average',
											type: 'number',
											width: window.innerWidth * 0.03,
											minWidth: 50
										},
										{
											text: 'average52',
											type: 'number',
											width: window.innerWidth * 0.03,
											minWidth: 50
										},
										{
											text: 'average8',
											type: 'number',
											width: window.innerWidth * 0.03,
											minWidth: 50
										},
										{
											text: 'average2',
											type: 'number',
											width: window.innerWidth * 0.03,
											minWidth: 50
										}
									]}
									groups={[]}
									totalRows={[]}
									rows={averageRows}
									checkboxFirstColumn={false}
									cellRedFields={[]}
								/>
							</div>

							{subRowData && subRowData.length && allDataBarCode ? (
								<div className="width-68-per ml-2">
									<div className="d-flex mt-05 ">
										<span className="text-bold-600 font-medium-2 justify-content-center">
											{formatMessage({ id: 'calculation' })}
										</span>
										<span
											onClick={() => {
												tableClick('#31baab');
											}}
											className="ml-2 d-flex justify-content-center align-items-center align-self-center"
										>
											<label className="mr-05 text-bold-600">
												{formatMessage({ id: 'dataFromTables' })} -{' '}
											</label>
											{dotData('#31baab', false, true)}
										</span>
										<span
											onClick={() => {
												tableClick('#663300');
											}}
											className="ml-2 d-flex justify-content-center align-items-center align-self-center"
										>
											<label className="mr-05 text-bold-600">
												{formatMessage({ id: 'exptecedSalesUntilSupply' })} -{' '}
											</label>
											{dotData('#663300', false, true)}
											{dotData('#663300', true)}
										</span>
										<span
											onClick={() => {
												tableClick('green');
											}}
											className=" ml-2 d-flex justify-content-center align-items-center align-self-center"
										>
											<label className="mr-05 text-bold-600">
												{formatMessage({ id: 'orderedAmountSingle' })} -{' '}
											</label>
											{dotData('green', false, true)}
											{dotData('green', true)}
										</span>
										<span
											onClick={() => {
												tableClick('#ff705d');
											}}
											className="ml-2 d-flex justify-content-center align-items-center align-self-center"
										>
											<label className="mr-05 text-bold-600">
												{formatMessage({ id: 'orderedAmountMaraz' })} -{' '}
											</label>
											{dotData('#ff705d', false, true)}
											{dotData('#ff705d', true)}
										</span>
									</div>

									<table className="height-25-vh minimalistBlack">
										<tbody>
											<tr>
												{[
													{
														preText: `${formatMessage({ id: 'stockOnTheWay' })}:${' '}`,
														text:
															subRowData[0].stockOnTheWay > 0
																? allDataBarCode.ariza > 0
																	? `${
																			subRowData[0].stockOnTheWay *
																			allDataBarCode.ariza
																	  } ${formatMessage({ id: 'units' })}`
																	: `${subRowData[0].stockOnTheWay} ${formatMessage({
																			id: 'boxs'
																	  })}`
																: 0,
														colors: [
															{
																color: '#31baab',
																opacity: false,
																dontDisplayDefault: true
															}
														],
														additinalData: 'stockOnTheWayIgnoreOnWeekly'
													},
													{
														preText: `${formatMessage({ id: 'packingFactor' })}:${' '}`,
														text: allDataBarCode.packingFactor,
														colors: [
															{
																color: '#ff705d',
																opacity: true,
																dontDisplayDefault: false
															},
															{
																color: '#31baab',
																opacity: false,
																dontDisplayDefault: true
															}
														]
													},
													{
														preText: `${formatMessage({ id: 'shelf' })}:`,
														text: allDataBarCode.shelfData,
														colors: [
															{
																color: '#31baab',
																opacity: false,
																dontDisplayDefault: true
															}
														]
													},
													{
														preText: `${formatMessage({ id: 'avg8Shelf' })}:${' '}`,
														text: subRowData[0].shelfAvg8.toFixed(2),
														colors: [
															{
																color: '#31baab',
																opacity: false,
																dontDisplayDefault: true
															}
														]
													},
													{
														preText: `${formatMessage({ id: 'p_truma' })}:`,
														text: subRowData[0].t_total,
														colors: [
															{
																color: '#31baab',
																opacity: false,
																dontDisplayDefault: true
															}
														]
													}
												].map((item: any) => {
													return (
														<td
															className="cursor-pointer"
															onClick={() => {
																if (item.colors) {
																	let activeDotsNew = activeDots;
																	item.colors.forEach((i: any) => {
																		activeDotsNew = tableClick(
																			i.color,
																			activeDotsNew,
																			i.opacity ? i.opacity : i.dontDisplayDefault
																		);
																	});
																}
															}}
														>
															<Tooltip
																placement="top-end"
																title={formatMessage({
																	id: item.additinalData
																		? item.additinalData
																		: item.preText
																})}
															>
																<div className="d-flex position-relative align-items-center">
																	{item.additinalData ? (
																		<Icon
																			src={
																				config.iconsPath +
																				'general/info-icon-small.svg'
																			}
																		/>
																	) : null}
																	<span
																		className={`${item.text < 0 ? 'text-red' : ''}`}
																	>
																		{item.preText} {item.text}
																	</span>
																	{item.colors.map((i: any) => {
																		return (
																			<DotRoute
																				opacity={i.opacity}
																				color={i.color}
																				dontDisplayDefault={
																					i.dontDisplayDefault
																				}
																				activeDots={activeDots}
																			/>
																		);
																	})}
																</div>
															</Tooltip>
														</td>
													);
												})}
											</tr>
											<tr>
												{[
													{
														preText: `${formatMessage({ id: 'supplyDate' })}${' '}`,
														text: moment(subRowData[0].supplyDate).format('DD.MM.YY'),
														colors: [
															{
																color: '#31baab',
																opacity: false,
																dontDisplayDefault: true
															}
														]
													},
													{
														preText: `${formatMessage({ id: 'supplyDay' })}:`,
														text: formatMessage({
															id: whatDayLetter(subRowData[0].supplyDay)
														}),
														colors: [
															{
																color: '#31baab',
																opacity: false,
																dontDisplayDefault: true
															}
														]
													},
													{
														preText: `${formatMessage({ id: 'daysToSupply' })}:${' '}`,
														text: `${
															moment(subRowData[0].supplyDate, 'YYYY-MM-DD').diff(moment(new Date(), 'YYYY-MM-DD'), 'days') + 1
														} ${formatMessage({ id: 'days' })}`,
														colors: [
															{
																color: 'green',
																opacity: true,
																dontDisplayDefault: false
															},
															{
																color: '#663300',
																opacity: true,
																dontDisplayDefault: false
															},
															{
																color: '#31baab',
																opacity: false,
																dontDisplayDefault: true
															}
														]
													},
													{
														preText: `${formatMessage({ id: 'nextSupplyDate' })}:`,
														text: `${moment(subRowData[0].supplyDateNext).format(
															'DD.MM.YY'
														)}`,
														colors: [
															{
																color: '#31baab',
																opacity: false,
																dontDisplayDefault: true
															}
														]
													},
													{
														preText: `${formatMessage({ id: 'daysToNextSupply' })}:${' '}`,
														text: `${
															moment(subRowData[0].supplyDateNext, 'YYYY-MM-DD').diff(
																moment(subRowData[0].supplyDate, 'YYYY-MM-DD'),
																'days'
															) + 1
														} 
													${formatMessage({ id: 'days' })}`,
														colors: [
															{
																color: '#663300',
																opacity: true,
																dontDisplayDefault: false
															},
															{
																color: '#31baab',
																opacity: false,
																dontDisplayDefault: true
															}
														]
													}
												].map((item: any) => {
													return (
														<td
															className="cursor-pointer"
															onClick={() => {
																if (item.colors) {
																	let activeDotsNew = activeDots;
																	item.colors.forEach((i: any) => {
																		activeDotsNew = tableClick(
																			i.color,
																			activeDotsNew,
																			i.opacity ? i.opacity : i.dontDisplayDefault
																		);
																	});
																}
															}}
														>
															<Tooltip
																placement="top-end"
																title={formatMessage({
																	id: item.additinalData
																		? item.additinalData
																		: item.preText
																})}
															>
																<div className="d-flex position-relative  align-items-center">
																	{item.additinalData ? (
																		<Icon
																			src={
																				config.iconsPath +
																				'general/info-icon-small.svg'
																			}
																		/>
																	) : null}
																	<span
																		className={`${item.text < 0 ? 'text-red' : ''}`}
																	>
																		{item.preText} {item.text}
																	</span>
																	{item.colors.map((i: any) => {
																		return (
																			<DotRoute
																				opacity={i.opacity}
																				color={i.color}
																				dontDisplayDefault={
																					i.dontDisplayDefault
																				}
																				activeDots={activeDots}
																			/>
																		);
																	})}
																</div>
															</Tooltip>
														</td>
													);
												})}
											</tr>
											<tr>
												{[
													{
														preText: `${formatMessage({ id: 'stockWithoutLive' })}:${' '}`,
														text: subRowData[0].balanceWithOutLiveSale.toFixed(2),
														colors: [
															{
																color: '#31baab',
																opacity: false,
																dontDisplayDefault: true
															}
														],
														additinalData: 'stockAsOfLastCount'
													},
													{
														preText: `${formatMessage({ id: 'stockWithReplacment' })}:`,
														text: subRowData[0].balanceAfterReplament.toFixed(2),
														colors: [
															{
																color: '#31baab',
																opacity: false,
																dontDisplayDefault: true
															}
														],
														additinalData: 'stockAsOfLastCountWithReplament'
													},
													{
														preText: `${formatMessage({ id: 'stockWithLive' })}:`,
														text: subRowData[0].balanceAfterLiveSale.toFixed(2),
														colors: [
															{
																color: '#31baab',
																opacity: false,
																dontDisplayDefault: true
															},
															{
																color: '#663300',
																opacity: true,
																dontDisplayDefault: false
															}
														],
														additinalData: 'stockWithLiveSales'
													},
													{
														preText: `${formatMessage({ id: 'stockAfterTrash' })}:${' '}`,
														text: subRowData[0].balanceAfterTrash.toFixed(2),
														colors: [
															{
																color: '#31baab',
																opacity: false,
																dontDisplayDefault: true
															},
															{
																color: '#663300',
																opacity: true,
																dontDisplayDefault: false
															}
														],
														additinalData: 'stockWithLiveAfterTrash'
													},

													{
														preText: `${formatMessage({
															id: 'stockWhenSupplyArrives'
														})}:${' '}`,
														text: allDataBarCode.balanceWhenStockArrive.toFixed(0),
														colors: [
															{ color: 'green', opacity: true, dontDisplayDefault: false }
														]
													}
												].map((item: any) => {
													return (
														<td
															className="cursor-pointer"
															onClick={() => {
																if (item.colors) {
																	let activeDotsNew = activeDots;
																	item.colors.forEach((i: any) => {
																		activeDotsNew = tableClick(
																			i.color,
																			activeDotsNew,
																			i.opacity ? i.opacity : i.dontDisplayDefault
																		);
																	});
																}
															}}
														>
															{' '}
															<Tooltip
																placement="top-end"
																title={formatMessage({
																	id: item.additinalData
																		? item.additinalData
																		: item.preText
																})}
															>
																<div className="d-flex position-relative  align-items-center">
																	{item.additinalData ? (
																		<Icon
																			src={
																				config.iconsPath +
																				'general/info-icon-small.svg'
																			}
																		/>
																	) : null}
																	<span
																		className={`${item.text < 0 ? 'text-red' : ''}`}
																	>
																		{item.preText} {item.text}
																	</span>
																	{item.colors.map((i: any) => {
																		return (
																			<DotRoute
																				opacity={i.opacity}
																				color={i.color}
																				dontDisplayDefault={
																					i.dontDisplayDefault
																				}
																				activeDots={activeDots}
																			/>
																		);
																	})}
																</div>
															</Tooltip>
														</td>
													);
												})}
											</tr>
											<tr>
												{[
													{
														preText: `${formatMessage({ id: 'minimumItemsCalc' })}:${' '}`,
														text: subRowData[0].stockMini,
														colors: [{}]
													},
													{
														preText: `${formatMessage({ id: 'minimumItemsManual' })}:`,
														text: subRowData[0].minimumItemsManual,
														colors: [
															{
																color: '#31baab',
																opacity: false,
																dontDisplayDefault: true
															}
														]
													},
													{
														preText: `${formatMessage({ id: 'toMinimumItems' })}:${' '}`,
														text: allDataBarCode.completionToSumMin,
														colors: [
															{ color: 'green', opacity: true, dontDisplayDefault: false }
														]
													},
													{
														preText: `${formatMessage({ id: 'expectedSalesNoTosefet' })}:`,
														text: allDataBarCode.salePredicationNoTossefet,
														colors: [
															{
																color: '#663300',
																opacity: true,
																dontDisplayDefault: false
															}
														]
													},
													{
														preText: `${formatMessage({ id: 'tosefet' })}:${' '}`,
														text: subRowData[0].Tosefet,
														colors: [
															{
																color: '#663300',
																opacity: true,
																dontDisplayDefault: false
															}
														]
													},
													{
														preText: `${formatMessage({
															id: 'exptecedSalesUntilSupply'
														})}:${' '}`,
														text: allDataBarCode.salePredication,
														colors: [
															{
																color: 'green',
																opacity: true,
																dontDisplayDefault: false
															},
															{ color: '#663300' }
														]
													}
												].map((item: any) => {
													return (
														<td
															className="cursor-pointer"
															onClick={() => {
																if (item.colors) {
																	let activeDotsNew = activeDots;
																	item.colors.forEach((i: any) => {
																		activeDotsNew = tableClick(
																			i.color,
																			activeDotsNew,
																			i.opacity ? i.opacity : i.dontDisplayDefault
																		);
																	});
																}
															}}
														>
															<Tooltip
																placement="top-end"
																title={formatMessage({
																	id: item.additinalData
																		? item.additinalData
																		: item.preText
																})}
															>
																<div className="d-flex position-relative  align-items-center">
																	{item.additinalData ? (
																		<Icon
																			src={
																				config.iconsPath +
																				'general/info-icon-small.svg'
																			}
																		/>
																	) : null}
																	<span
																		className={`${item.text < 0 ? 'text-red' : ''}`}
																	>
																		{item.preText} {item.text}
																	</span>
																	{item.colors.map((i: any) => {
																		return (
																			<DotRoute
																				opacity={i.opacity}
																				color={i.color}
																				dontDisplayDefault={
																					i.dontDisplayDefault
																				}
																				activeDots={activeDots}
																			/>
																		);
																	})}
																</div>
															</Tooltip>
														</td>
													);
												})}
											</tr>

											<tr>
												{[
													{
														preText: `${formatMessage({
															id: 'amountOrderSingleBeforeLoss'
														})}:${' '}`,
														text: subRowData[0].amountOrderSingleBeforeLoss.toFixed(2),
														colors: [
															{
																color: '#ff705d',
																opacity: true,
																dontDisplayDefault: false
															},
															{ color: 'green' }
														],
														additinalData: 'amountOrderSingleBeforeLossDesc'
													},
													{
														preText: `${formatMessage({
															id: 'orderedAmountSingle'
														})}:${' '}`,
														text: subRowData[0].amountOrderSingle.toFixed(2),
														colors: [
															{
																color: '#ff705d',
																opacity: true,
																dontDisplayDefault: false
															},
															{ color: 'green' }
														]
													},
													{
														preText: `${formatMessage({
															id: 'orderedAmountMaraz'
														})}:${' '}`,
														text: subRowData[0].amountOrderMaraz,
														colors: [
															{
																color: '#ff705d',
																opacity: false,
																dontDisplayDefault: false
															}
														],
														additinalData: 'orderedAmountMarazDesc'
													}
												].map((item: any) => {
													return (
														<td
															className="cursor-pointer"
															onClick={() => {
																if (item.colors) {
																	let activeDotsNew = activeDots;
																	item.colors.forEach((i: any) => {
																		activeDotsNew = tableClick(
																			i.color,
																			activeDotsNew,
																			i.opacity ? i.opacity : i.dontDisplayDefault
																		);
																	});
																}
															}}
														>
															<Tooltip
																placement="top-end"
																title={formatMessage({
																	id: item.additinalData
																		? item.additinalData
																		: item.preText
																})}
															>
																<div className="d-flex position-relative  align-items-center">
																	{item.additinalData ? (
																		<Icon
																			src={
																				config.iconsPath +
																				'general/info-icon-small.svg'
																			}
																		/>
																	) : null}
																	<span
																		className={`${item.text < 0 ? 'text-red' : ''}`}
																	>
																		{item.preText} {item.text}
																	</span>
																	{item.colors.map((i: any) => {
																		return (
																			<DotRoute
																				opacity={i.opacity}
																				color={i.color}
																				dontDisplayDefault={
																					i.dontDisplayDefault
																				}
																				activeDots={activeDots}
																			/>
																		);
																	})}
																</div>
															</Tooltip>
														</td>
													);
												})}
											</tr>
										</tbody>
									</table>
								</div>
							) : null}
						</div>
					</div>
				) : null}
			</div>
		</>
	);
};
export default StockSimulator;
