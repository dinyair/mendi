import * as React from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { RootStore } from '@src/store';
import { useSelector, useDispatch } from 'react-redux';
import classnames from 'classnames';

interface Props {
	color: string;
	opacity?: boolean;
	activeDots: any;
    dontDisplayDefault?:boolean
}

export const DotRoute: React.FC<Props> = (props: Props) => {
	const { color, opacity, activeDots,dontDisplayDefault } = props;
	const { formatMessage } = useIntl();
	const dispatch = useDispatch();

    
	return (
		<>
			<div
				className={classnames(
					'ml-auto mr-1 border-white border-1 cursor-pointer oval rounded zindex-10002  badge-up text-bold-600 text-white height-0-9-rem width-0-9-rem mt-auto mb-auto',
					{
						'opacity-05': opacity,
						'visibility-hidden': !activeDots.includes(color) && (opacity || dontDisplayDefault)
					}
				)}
				style={{
					backgroundColor: color,
					top: '-0.2rem',
					right: '-0.5rem',
					border: '0.9rem solid white'
				}}
			>
				&nbsp;
			</div>{' '}
		</>
	);
};

export default DotRoute;
