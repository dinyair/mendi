import * as React from 'react';
import { TabContent, TabPane, Button } from 'reactstrap';
import { useIntl } from "react-intl"

import { Tabs, Tab, } from "@src/components/tabs";
import { config } from '@src/config';
import { Drawer, SidePanel } from '@src/components/drawer';
import { FormGroup, Fields } from '@src/components/form-group';
import { GenericDrawerData, DrawerTab } from './interfaces';
import classnames from "classnames"
import { InputTypes } from "@src/components/form-group";

import './edit-drawer.scss';


interface Props {
	caption: string;
	data: GenericDrawerData;
	shown: boolean;
	toggleModal: () => void;
	saveChanges: (data: GenericDrawerData, caption?: any) => void;
	tempData?: any
	setTempData?: (tempData: any[]) => void;
	titleEditable?: boolean;
	isTabsHidden?: boolean;
	hideZeroFilters?: boolean;
	saveButtonText?: string
	saveDisabled?: boolean
	validateCaption?: string
	required?: any
	isCaptionText?: any
	modalBeforeClose?: boolean
}

export const EditDrawer: React.FC<Props> = (props) => {
	const { caption, data, shown, toggleModal, saveChanges, tempData, setTempData, titleEditable, isTabsHidden, hideZeroFilters, saveButtonText, saveDisabled, validateCaption, required, isCaptionText, modalBeforeClose } = props
	// const { tabs } = data;
    const [selectedSearch,setSelectedSearch] = React.useState<any>('')


	const [tabs, setTabs] = React.useState<any>(data.tabs)
	const inputRef = React.useRef<HTMLInputElement>(null);
	const [currentValue, setValue] = React.useState(isCaptionText ? caption : '');
	const [activeTab, setActiveTab] = React.useState(tabs[0].tabId);
	const [isEdit, setEdit] = React.useState(false);
	const [captionError, setCaptionError] = React.useState<boolean>(false)
	const [dataChanges, setDataChanged] = React.useState<number>(0)

	React.useEffect(() => {
		setTabs(data.tabs)
	}, [data])

	const { formatMessage } = useIntl();


	React.useEffect(() => {
		if (isEdit && inputRef.current) {
			inputRef.current.focus();
		}
	}, [isEdit]);

	const copyTabs: DrawerTab[] = React.useMemo(() => (
		JSON.parse(JSON.stringify(tabs))
	), [tabs]);

	const refTabs = React.useRef<{ [key: string]: DrawerTab }>(copyTabs.reduce((acc, curr) => ({
		...acc,
		[curr.tabId]: curr
	}), {}));

	const onChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
		setDataChanged(dataChanges + 1)
		setValue(e.target.value);
	}

	const onCanEdit = (): void => {
		console.log('here!!')
		setEdit(true);
	}

	const offCanEdit = (): void => {
		setEdit(false);
	}

	const onKeyDown = (event: React.KeyboardEvent<HTMLInputElement>): void => {
		if (event.key === 'Enter') {
			offCanEdit();
		}
	}

	const onChangeData = (tabId: string, formId: string, formData: Fields): void => {
		setDataChanged(dataChanges + 1)
		refTabs.current[tabId].formGroup = refTabs.current[tabId].formGroup.map(form => (
			form.formId === formId ? { ...form, ...formData } : form
		));
	}

	const onSaveChanges = (): void => {
		if (required) {
			setCaptionError(false)
			let isFieldsValidated = validateFields(required)
			if (isFieldsValidated !== true) {
				setTabs(isFieldsValidated)
				if (validateCaption && !currentValue || currentValue.length < 4) {
					setCaptionError(true)
				}
			} else if (validateCaption && !currentValue || currentValue.length < 4) {
				setCaptionError(true)
			} else {
				saveChanges({ tabs: Object.values(refTabs.current) }, caption ? { caption: currentValue } : null);
				toggleModal();
			}
		} else if (validateCaption && !currentValue || validateCaption && currentValue.length < 4) {
			setCaptionError(true)
		} else {
			saveChanges({ tabs: Object.values(refTabs.current) }, caption ? { caption: currentValue } : null);
			toggleModal();
		}
	}
	const validateFields = (required: any) => {
		let filedsCount: number = 0;
		let FieldsAreFullfilled: number = 0;
		let valid = true;
		let tabs = Object.values(refTabs.current)

		tabs.forEach((tab: any) => {
			tab.formGroup.forEach((fields: any) => {
				fields.fields.forEach((field: any, index: number) => {
					filedsCount++
					if (required.includes(field.name)) {
						if (field.type === InputTypes.PASSWORD && field.value.length > 0) {
							if (field.name === 'confirm password') {
								if (fields.fields[index - 1] && fields.fields[index - 1].value && fields.fields[index - 1].value === field.value) {
									field.error = null
									FieldsAreFullfilled++;
								} else {
									field.error = { label: formatMessage({ id: "passwords not match" }) };
								}
							}
						}
						else if (field.value && (field.value.length > 0 || field.value.value)) {
							field.error = null
							FieldsAreFullfilled++;
						} else {
							field.error = { label: formatMessage({ id: 'this input is required' }) };

						}
					}
					if (field.validations) {
						let isValid = field.validations.map((v: any) => {
							if (v && v(field.value, 6)) return true
							return false
						})
						if (!isValid.includes(false)) {
							field.error = null
							valid = true;
						} else {
							valid = false
							field.error = { label: formatMessage({ id: "password not strong enough" }) };
						}
					}
				})
			})
		})
		return FieldsAreFullfilled === required.length && valid ? true : tabs
	}


	if (shown) {
		return (
			<Drawer dataChanges={dataChanges} modalBeforeClose={modalBeforeClose} shown={shown} toggleModal={toggleModal}>
				<SidePanel onClose={toggleModal}>
					<div className='edit-drawer__body overflow-visible'>
						<h3 className={classnames("edit-drawer__title", {
						})} >
							<div onClick={titleEditable ? onCanEdit : () => null} className={classnames("px-05 min-width-15-rem", {
								"border-warning": captionError,
								"border-radius-05rem": captionError,
							})}>
								{isCaptionText ? <input
									className='no-outline no-border'
									ref={inputRef}
									onChange={onChange}
									onBlur={offCanEdit}
									onKeyDown={onKeyDown}
									// placeholder={caption}
									value={currentValue}
									disabled={!isEdit}
									style={{ width: ((currentValue.length + 1) * 11) + 'px', minWidth: '8rem' }}
								/> :
									<input
										className='no-outline no-border'
										ref={inputRef}
										onChange={onChange}
										onBlur={offCanEdit}
										onKeyDown={onKeyDown}
										placeholder={caption}
										disabled={!isEdit}
										style={{ width: ((caption.length + 1) * 11) + 'px', minWidth: '8rem' }}
									/>}

								{titleEditable === false ? null :
									<img className="cursor-pointer" src={config.iconsPath + "general/edit-icon.svg"} alt='' />}
								{captionError ? <div className='ml-05 text-bold-600 font-small-2 text-danger'>{formatMessage({ id: validateCaption })}</div> : null}
							</div>
						</h3>
						<div className='edit-drawer__tabs'>

							{isTabsHidden ? null : <Tabs value={activeTab} onChangTab={setActiveTab}>
								{tabs.map(({ title, tabId }) => (
									<Tab key={tabId} caption={title} name={tabId} />
								))}
							</Tabs>}

						</div>
						<TabContent className='edit-drawer__tab-content' activeTab={activeTab}>
							{tabs.map(({ tabId, formGroup }) => (
								<TabPane key={tabId} tabId={tabId}>
									{formGroup.map(({ title, formId, fields, additionalFields, extraFields }) => (
										<FormGroup selectedSearch={selectedSearch} setSelectedSearch={setSelectedSearch} key={formId} {...{ title, fields, additionalFields, extraFields, id: tabId, formId, onChangeData, tempData, setTempData }} />
									))}
								</TabPane>
							))}
						</TabContent>
						<div className='edit-drawer__footer'>
							<Button className='round mr-1' disabled={saveDisabled} onClick={onSaveChanges} color='primary'>{saveButtonText ? formatMessage({ id: saveButtonText }) : formatMessage({ id: 'save' })}</Button>
							{hideZeroFilters ? null : <Button className='round ' color='primary' outline onClick={toggleModal}>{formatMessage({ id: 'cancel' })}</Button>}
						</div>
					</div>
				</SidePanel>
			</Drawer >
		)
	}

	return null;
}
