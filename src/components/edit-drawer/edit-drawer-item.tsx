import * as React from 'react';

interface Props {
	caption: string;
	name: string;
	value: string | number | boolean;
	onChange: (data: {name: string; value: string}) => void;
}

export const EditDrawerItem: React.FC<Props> = ({caption, name, value: val, onChange}) => {
	const [value, setValue] = React.useState(String(val));

	const updateValue = (event: React.FormEvent<HTMLInputElement>) => {
		setValue(event.currentTarget.value);
		onChange({name, value: event.currentTarget.value});
	}

	return (
		<li className='edit-drawer__form-group'>
			<label className='edit-drawer__label'>
				<span>{caption}</span>
				<input onChange={updateValue} type="text" name={name} value={value}/>
			</label>
		</li>
	)
}
