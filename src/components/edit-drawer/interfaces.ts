import { FormGroupType } from '@src/components/form-group';

export interface DrawerTab {
	title: string;
	tabId: string;
	formGroup: FormGroupType[];
}

export interface GenericDrawerData {
	itemName?:any
	tabs: DrawerTab[];
}
