import {createPortal} from "react-dom";
import * as React from 'react';

const portalHolder = document.getElementById('portal-holder');

export const Portal: React.FC = ({children}) => {
	if (portalHolder) {
		return createPortal(
			children,
			portalHolder
		);
	}

	return null;
}
