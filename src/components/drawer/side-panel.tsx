import * as React from 'react';
import classnames from "classnames";

import './side-panel.scss';

interface Props {
	shown?: boolean;
	onClose: () => void;
	timout?: number;
}

export const SidePanel: React.FC<Props> = ({children, shown, timout, onClose}) => {
	const classes = classnames('side-panel', {
		open: shown,
	});

	return (
		<div className={classes} style={{transition: `left ${timout}ms linear`}}>
			<div className='side-panel__body'>
				{children}
			</div>
		</div>
	);
}
