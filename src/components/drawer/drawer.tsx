import * as React from 'react';
import classnames from "classnames";
import { Portal } from "@src/components/portal";
import { useIntl, FormattedMessage } from "react-intl"
import { Modal, ModalBody, ModalFooter, Button } from 'reactstrap'
import { ModalHeader } from '@src/components/modal';

import './drawer.scss';

interface Props {
	shown: boolean;
	toggleModal: () => void;
	timout?: number;
	modalBeforeClose?: boolean
	dataChanges?: number
}

const TIMOUT = 150;

export const Drawer: React.FC<Props> = ({ shown, toggleModal, timout = TIMOUT, children, modalBeforeClose, dataChanges }) => {
	const { formatMessage } = useIntl();

	const [fade, setFade] = React.useState(false);
	const [isAreYouSureModalOpen, setAreYouSureModal] = React.useState(false)

	React.useEffect(() => {
		setFade(shown);
	}, [shown]);

	const onClose = () => {
		if (modalBeforeClose && dataChanges && dataChanges > 0) {
			setAreYouSureModal(true)
		} else {
			setFade(false);
			setTimeout(toggleModal, timout)
		}
	}

	const mapChildren = (child: React.ReactNode, key: number): React.ReactNode => {
		if (React.isValidElement(child)) {
			return React.cloneElement(child, {
				key,
				timout,
				shown: fade,
			});
		}

		return child;
	};

	const classes = classnames('drawer', {
		show: shown,
	});

	const classesBackdrop = classnames('drawer__backdrop', {
		'drawer__backdrop_fade': fade
	});


	const areYouSureModal = [{
		headerClasses: 'margin-auto',
		isOpen: isAreYouSureModalOpen, toggle: () => setAreYouSureModal(false), header: (<span className="font-weight-bold">{formatMessage({ id: 'warning' })}</span>),
		body: (<><p> {formatMessage({ id: 'closeCreateNewUserWarning' })}  </p> <p>{formatMessage({ id: 'areYouSure' })}</p></>),
		buttons: [
			{
				color: 'primary', onClick: () => {
					setFade(false);
					setTimeout(toggleModal, timout)
				}, label: 'yesIamSure', outline: true
			},
			{ color: 'primary', onClick: () => setAreYouSureModal(false), label: 'noBack' }
		]
	}]
	const returnModal = (modal: any) => {
		return (<Modal zIndex={999999999} isOpen={modal.isOpen} toggle={modal.toggle} className='modal-dialog-centered modal-md text-center width-10-per min-width-25-rem'>
			<ModalHeader onClose={modal.toggle} classNames={modal.headerClasses}>
				<h5 className="modal-title">{modal.header}</h5>
			</ModalHeader>
			<ModalBody>
				{modal.body}
			</ModalBody>
			<ModalFooter>
				<div className="m-auto">
					{modal.buttons.map((ele: any, index: number) => <Button outline={ele.outline ? true : false} className='round ml-05 mr-05' key={index} color={ele.color} onClick={ele.onClick} disabled={ele.disabled}>{formatMessage({ id: ele.label })}</Button>)}
				</div>
			</ModalFooter>
		</Modal>
		)
	}

	if (shown) {
		return (
			<Portal>
				<div className={classes}>
					{React.Children.map(children, mapChildren)}
					<div onClick={onClose} style={{ transition: `all ${timout}ms linear` }} className={classesBackdrop} />
				</div>

				{isAreYouSureModalOpen && returnModal(areYouSureModal[0])}
			</Portal>
		);
	}

	return null;
}
