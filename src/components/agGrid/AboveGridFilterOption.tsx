import * as React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { ArrowDown, ArrowUp } from 'react-feather';
import { BlankPopUp } from '@components';
import * as moment from 'moment';
import { config } from '../../config'

interface Props {
	item: any;
	removeColumnFromFilter: (field: any) => void
	removeSingleFilterFromMulti: (item: any, i: any) => void
}

export const AboveGridFilterOption: React.FC<Props> = (props: Props) => {
	const { item, removeSingleFilterFromMulti, removeColumnFromFilter } = props
	const [displayAboveGridFilterMultiSelect, setDisplayAboveGridFilterMultiSelect] = React.useState<any>()

	const dispatch = useDispatch();
	return (
		<div key={item.field} className='d-flex justify-content-center align-items-center mr-1 text-bold-700 position-relative cursor-pointer'>
			{!item.cannotRemove && <div onClick={() => removeColumnFromFilter(item.field)}
				style={{ left: '0.6rem', top: '0.6rem' }} className='border-radius-2rem float-right text-black'>
				<img src={`../../${config.iconsPath}/aggrid/closeIcon.svg`} />
			</div>}

			{item.data && item.data.length > 1 ?
				<div className='d-flex justify-content-center align-items-center' onClick={() => setDisplayAboveGridFilterMultiSelect(!displayAboveGridFilterMultiSelect)}>
					<span>{item.preText}</span>
					<ArrowDown size={14} />
					{displayAboveGridFilterMultiSelect &&
						<div className='position-absolute position-left-0'>
							<BlankPopUp noXbutton
								onOutsideClick={setDisplayAboveGridFilterMultiSelect}
								style={{ width: '10rem', height: `${5 + item.data.length * 1.75}rem`, top: '1.5rem' }}>
								<div className='d-flex-column width-80-per m-auto-t-1-5 text-black font-bold-400'>
									{item.data.map((i: any) => {
										return (
											<div className='d-flex justify-content-start'>
												<div onClick={() => removeSingleFilterFromMulti(item, i)}
													style={{ left: '0.6rem', top: '0.6rem' }} className='border-radius-2rem cursor-pointer float-right text-black'>
													<img src={`../../${config.iconsPath}/aggrid/closeIcon.svg`} />
												</div>
												<span className='mr-05 elipsis'>{item.type === 'Date' ? moment(new Date(i.label)).format('DD.MM.YY') : i.label}</span>
											</div>
										)
									})}
								</div>
							</BlankPopUp>
						</div>}

				</div>
				:
				<span className='mr-05'>{item.preText} {item.type && item.type === 'Date' ? moment(new Date(item.data[0] && item.data[0].label)).format('DD.MM.YY') : item.data[0] && item.data[0].label}</span>
			}
		</div>
	);
};

export default AboveGridFilterOption;
