import * as React from 'react';
import { Button } from 'reactstrap';

interface Props {
    renderButton: RenderButtonType[];
    data: any;
}

export interface RenderButtonType {
    label: string,
    disabled?: string,
    outline?: string,
    onClick: (data: any) => void;
    byField?: string;
    value?: any;
    classNames?: string;
    color?: string;
}

export default (props: Props) => {
    let { renderButton, data, } = props;

    return (
        <div className="d-flex align-items-center justify-content-center width-100-per ">
            {renderButton.map((ele: RenderButtonType) => {
                return (ele.byField && data[ele.byField] === ele.value) || !ele.byField ?
                    <Button className={`round ${ele.classNames ? ele.classNames : ''} ${ele.outline ? 'grid-btn-outline' : ''}`} outline={ele.outline ? true : false} color={ele.color ? ele.color : 'primary'} disabled={ele.disabled ? true : false} onClick={() => ele.onClick(data)}>{ele.label}</Button>
                    : null;

            })}

        </div>
    );
}