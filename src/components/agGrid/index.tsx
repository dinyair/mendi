
import * as React from 'react';
import * as moment from 'moment';
import '@ag-grid-community/all-modules/dist/styles/ag-grid.css';
import '@ag-grid-community/all-modules/dist/styles/ag-theme-alpine.css';
import classnames from "classnames"
import ExportExcel from '../ExportExcel/ExportExcel';
import CustomHeader from './customHeader.jsx';
import IconRender from './IconRender.jsx';
import TooltipRender from './TooltipRender.jsx';
import ReactToPrint from 'react-to-print';
import AboveGridFilterOption from './AboveGridFilterOption'
import ButtonRender from './ButtonRender';
import CustomLoadingOverlay from './customLoadingOverlay.jsx';
import CustomNoRowsOverlay from './customNoRowsOverlay.jsx';

import { AgGridReact, AgGridColumn } from '@ag-grid-community/react';
import { AllCommunityModules, CellEditingStoppedEvent, EditableCallbackParams, GridApi } from '@ag-grid-community/all-modules';
import CustomDateComponent from './CustomDateComponent.jsx';
import { ClientSideRowModelModule } from '@ag-grid-community/client-side-row-model';
import { RowGroupingModule } from '@ag-grid-enterprise/row-grouping';
import { Sorting } from '../../helpers/helpers'
import { FiltersToolPanelModule } from '@ag-grid-enterprise/filter-tool-panel';
import { MenuModule } from '@ag-grid-enterprise/menu';
import { ClipboardModule } from '@ag-grid-enterprise/clipboard';
import { SetFilterModule } from '@ag-grid-enterprise/set-filter';
import { MultiFilterModule } from '@ag-grid-enterprise/multi-filter';
import { Icon, Popup } from '@components';
import { config } from '../../config'
import { ColumnsToolPanelModule } from '@ag-grid-enterprise/column-tool-panel';
import { Button, Badge, Spinner } from "reactstrap"
import ComboBox, { CheckBoxTree } from "@src/components/combo-box";
import { FormattedMessage, useIntl } from "react-intl"
import { ColumnsPopup } from './ColumnsPopup'
import { ChevronLeft, ChevronRight, ChevronsLeft, ChevronsRight } from 'react-feather';
import InputText from '@src/components/input-text'
import '@components/combo-box/combo-box.scss';

import './index.scss';
import { CheckBoxStatus } from '../CheckBoxGroupsAccordion/interfaces';
import { RichSelectModule } from '@ag-grid-enterprise/all-modules';

interface Props {
	readonly rows: any
	readonly id?: string,
	readonly translateHeader?: boolean
	readonly resizable?: boolean
	readonly checkbox?: boolean
	readonly checkboxFirstColumn?: boolean
	readonly checkboxOptions?: boolean
	readonly displayExcel?: boolean
	readonly displayColumnDisplayer?: boolean
	readonly displayPrint?: boolean
	readonly aggFunction?: any
	readonly fields: any
	readonly groups: any[]
	readonly totalRows: any[],
	readonly cellBgRedMatriza?: boolean,
	readonly floatFilter?: boolean,
	readonly cellGreenFields?: any
	readonly cellLightBlueFields?: any
	readonly success?: string
	readonly successDark?: string
	readonly successLight?: string
	readonly danger?: string
	readonly dangerUnderZero?: string
	readonly cellUnSetFontSize?: any
	readonly cellRedFields?: any
	readonly leftBorderField?: string
	readonly hideWhenGroup?: string
	readonly changeRowDataOnSelectionChange?: any
	readonly gridHeight: string
	readonly saveOption?: boolean
	readonly onClickSave?: (rows: any[]) => void;
	readonly defaultSortFieldNum?: number
	readonly deletedFlag?: any
	readonly descSort?: boolean
	readonly yellowField?: any
	readonly yellowFlag?: any
	readonly blueField?: any
	readonly blueFlag?: any
	readonly pagination?: boolean
	readonly rowBufferAmount?: number
	readonly freeSearch?: freeSearchInterface
	// readonly freeSearchFields?: any[]
	// readonly freeSearchTitle?: any
	// readonly freeSearchOriginalRows?: any
	readonly customSortField?: any
	readonly customSortType?: any
	readonly showOther?: any
	readonly checkboxFilterBox?: checkboxFilterInterface
	readonly colorColumnFlag?: string
	readonly autoSelectedAboveGridFilter?: any
	readonly afterAboveFilterRemovedOrAdded?: (data: any, type: string, filter?: any) => void
	readonly dontFilterAbove?: boolean
	readonly buttonOnClick?: buttonOnClickInterface
	displayLoadingScreen?: boolean
	readonly onCellEditingStopped?: (data: any, field: any, newValue: any, oldValue: any) => void
	readonly editFields?: any
	readonly onRowDoubleClick?: (data: any) => void
	readonly noSortFields?: boolean
	readonly customFieldsDirection?: string;
	readonly noSort?: boolean
	readonly customRowHeight?: number;
	readonly headerHeight?: number
	readonly autoHeight?: boolean;
}




interface freeSearchInterface {
	readonly title: string,
	readonly rows: any[],
	readonly fieldsToSearch: any[],
}
interface buttonOnClickInterface {
	readonly disabled: boolean
	readonly title: string,
	readonly function: () => void,
}
interface checkboxFilterInterface {
	readonly title: string,
	readonly rows: any[],
	readonly options: any[],
	readonly flag: string,
}

function isFirstColumn(params: any) {
	var displayedColumns = params.columnApi.getAllDisplayedColumns();
	var thisIsFirstColumn = displayedColumns[0] === params.column;
	return thisIsFirstColumn;
}

function strcmp(a, b) {
	return a < b ? -1 : a > b ? 1 : 0;
}

function dateComparator(date1: any, date2: any) {
	var date1Number = monthToComparableNumber(date1);
	var date2Number = monthToComparableNumber(date2);
	if (date1Number === null && date2Number === null) {
		return 0;
	}
	if (date1Number === null) {
		return -1;
	}
	if (date2Number === null) {
		return 1;
	}
	return date1Number - date2Number;
}
function monthToComparableNumber(date: any) {
	if (date === undefined || date === null || date.length !== 10) {
		return null;
	}
	var yearNumber = date.substring(6, 10);
	var monthNumber = date.substring(3, 5);
	var dayNumber = date.substring(0, 2);
	var result = yearNumber * 10000 + monthNumber * 100 + dayNumber;
	return result;
}

export const agGrid = (props: Props): React.ReactElement<HTMLButtonElement> => {
	let checkboxCheckedPath = `../../${config.iconsPath}/aggrid/checkboxChecked.png`
	let checkboxNotCheckedPath = `../../${config.iconsPath}/aggrid/checkboxNotChecked.png`

	const { formatMessage } = useIntl();

	const
		{ id, rows, fields, groups, totalRows, resizable,
			aggFunction, checkbox, checkboxOptions,
			translateHeader, checkboxFirstColumn, displayExcel,
			displayColumnDisplayer, displayPrint, defaultSortFieldNum,
			cellBgRedMatriza, floatFilter, cellGreenFields, success, successDark, cellLightBlueFields, successLight,
			saveOption, onClickSave, freeSearch, deletedFlag,
			cellRedFields, danger, dangerUnderZero, leftBorderField, hideWhenGroup,
			customSortField, customSortType, cellUnSetFontSize,
			changeRowDataOnSelectionChange, gridHeight, descSort,
			yellowField, yellowFlag, blueField, blueFlag, pagination, rowBufferAmount,
			showOther, checkboxFilterBox, colorColumnFlag, autoSelectedAboveGridFilter,
			afterAboveFilterRemovedOrAdded, buttonOnClick, dontFilterAbove,
			displayLoadingScreen, onCellEditingStopped, editFields, onRowDoubleClick, noSortFields,
			customFieldsDirection, noSort, customRowHeight, headerHeight, autoHeight
		} = props

	const TableRef = React.useRef<HTMLElement>(null);
	const [gridApi, setGridApi] = React.useState<any>(null);
	const [gridColumnApi, setGridColumnApi] = React.useState<any>(null);
	const [showColumnPoup, setShowColumnPoup] = React.useState(false);
	const [hiddenColumns, setHiddenColumns] = React.useState<any[]>(fields ? fields.map((f: any) => {
		if (!f.hide) return null
		else return f.text
	}) : []);
	// Sorting(rows, 'boolean', customSortField) : 
	const [render, setRender] = React.useState<boolean>(false);
	const [isGroupsPopupOpen, setIsGroupsPopupOpen] = React.useState<any>(false);
	const [rowData, setRowData] = React.useState(rows && rows.length && fields && fields.length && !noSort ? Sorting(rows, { field: customSortField ? customSortField : fields[defaultSortFieldNum ? defaultSortFieldNum : 0].text, type: customSortType ? customSortType : typeof (rows[0][fields[defaultSortFieldNum ? defaultSortFieldNum : 0].text]), asc: descSort ? false : true, times: 0 }) : rows && rows.length && fields && fields.length ? rows : []);
	// const [rowData, setRowData] = React.useState(rows && rows.length ? rows : []);
	const [firstRowData, setFirstRowData] = React.useState<any[]>([]);
	const [mergedGroups, setMergedGroups] = React.useState<any>([]);
	const [selectedState, setSelectedState] = React.useState<any>('all');
	const [columnResultsPicker, setColumnResultsPicker] = React.useState<any>([]);
	let initialSelectedRows: any[] = props.rows ? props.rows.map((row: any) => { if (row.isSelected) { return { data: row } } else return undefined }).filter((a: any) => a) : []
	const [selected, setSelected] = React.useState<any>(initialSelectedRows);
	// const customizer = useSelector((state: RootStore) => state.customizer.customizer)
	const customizer = { direction: document.getElementsByTagName("html")[0].dir }
	const [selectedButton, setSelectedButton] = React.useState<boolean>(true)
	const [notSelectedButton, setNotSelectedButton] = React.useState<boolean>(true)
	const [refreshDataIconBg, setRefreshDataIconBg] = React.useState<string>('bg-gray')
	const [saveIconBg, setSaveIconBg] = React.useState<string>('bg-gray')
	const freeSearchInputRef = React.useRef<any>();
	const [paginationPageSize, setPaginationPageSize] = React.useState<number>(20)
	const [className20, setClassName20] = React.useState<any>('')
	const [className50, setClassName50] = React.useState<any>('')
	const [className100, setClassName100] = React.useState<any>('')
	const [className200, setClassName200] = React.useState<any>('')
	const [paginationRowCount, setPaginationRowCount] = React.useState<any>()
	const [checkboxFilterChcked, setCheckboxFilterChecked] = React.useState<any[]>(checkboxFilterBox ? checkboxFilterBox.options : [])
	const [aboveGridFilterList, setAboveGridFilterList] = React.useState<any>([])


	const agGridColumn = (key: number, field: any) => {
		return <AgGridColumn
			pinned={field.pinned}
			key={key}
			// lockPosition={field.draggable || !mergedGroups.includes(field.text) ? false : true}
			// editable={field.editable}
			aggFunc={field.aggFunc ? field.aggFunc : ''}
			columnsMenuParams={{
				suppressColumnFilter: true,
				suppressColumnSelectAll: true,
				suppressColumnExpandAll: true,
			}}
			field={field.text}
			headerName={translateHeader ? String(formatMessage({ id: field.text })) : field.text}
			width={field.width ? field.width : 150}
			minWidth={field.minWidth ? field.minWidth : 150}
			suppressSizeToFit={false}
			tooltipField={field.type != 'Date' && field.type != 'DateRange' && field.type != 'DateTime' && field.type != 'CheckBoxCell' ? field.text : ''}
			headerTooltip={translateHeader ? String(formatMessage({ id: field.text })) : field.text}
			//  tooltipComponent="customTooltip"
			headerCheckboxSelection={checkboxFirstColumn ? isFirstColumn : false}
			headerCheckboxSelectionFilteredOnly={true}
			checkboxSelection={checkbox && key === 0 && !mergedGroups.length ? true : checkbox && field.text === 'choose' ? true : false}
			hide={mergedGroups.includes(field.text) || field.hide || hiddenColumns.includes(field.text)}//
			cellRenderer={field.cellRenderer ? field.cellRenderer : (params) => {
				if (params.value === formatMessage({ id: 'other' }) && !showOther) {
					if (!params.node.allLeafChildren) {
						return ''

					}
				}
				if (typeof params.node.id !== 'number' && params.node.level > 0) { // update params of child of group but not last child!
					if (params.type === 'percentage') {
						let field: string = params.field
						let sumTotal: number
						let amount: number
						let percentAmount: any
						if (field === 'Percent_TotalAmount') {
							sumTotal = Number(params.node.parent.aggData['TotalAmount'])
							amount = Number(params.node.aggData['TotalAmount'])
							percentAmount = ((amount / sumTotal) * 100).toFixed(2) + '%'
							return percentAmount
						} else if (field === 'Percent_TotalPrice') {
							sumTotal = Number(params.node.parent.aggData['TotalPrice'])
							amount = Number(params.node.aggData['TotalPrice'])
							percentAmount = ((amount / sumTotal) * 100).toFixed(2) + '%'
							return percentAmount
						} else if (field === 'Percent_Avg') {
							let fatherTotalPrice = Number(params.node.parent.aggData['TotalPrice'])
							let fatherTotalAmount = Number(params.node.parent.aggData['TotalAmount'])
							let fatherCalculated = fatherTotalPrice + fatherTotalAmount / 2
							let sonTotalPrice = Number(params.node.aggData['TotalPrice'])
							let sonTotalAmount = Number(params.node.aggData['TotalAmount'])
							let sonCalculated = sonTotalPrice + sonTotalAmount / 2
							percentAmount = ((sonCalculated / fatherCalculated) * 100).toFixed(2) + '%'
							return percentAmount
						} else if (field === 'Percent_MarketAmount') {
							let totalRowsMarket = params.node.parent.allLeafChildren.reduce((acc: number, row: any) => acc + Number(row.data['TotalMarket']), 0)
							let totalMarket = params.node.allLeafChildren.reduce((acc: number, row: any) => acc + Number(row.data['TotalMarket']), 0)
							let percentAmount: any = totalMarket && totalRowsMarket ? Number(((totalMarket / totalRowsMarket) * 100)).toFixed(2) + '%' : 0 + '%'
							return percentAmount
						}
					}
				}
				// if (params.node.parent.id === 'ROOT_NODE_ID' && params.node.allChildrenCount) { // update father params of group
				// 	let pinnedRowData = params.agGridReact.gridOptions.pinnedBottomRowData[0]
				// 	let totalAmount = pinnedRowData.TotalAmount
				// 	let totalPrice = pinnedRowData.TotalPrice

				// 	console.log(params.node.selectionController.selectedNodes)
				// 	console.log(params.node.allLeafChildren)

				// 	totalAmount = totalAmount.replace(/\D/g, '');
				// 	totalPrice = totalPrice.replace(/\D/g, '');
				// 	let amount: number
				// 	let percentAmount: any

				// 	if (field.text === 'Percent_TotalAmount' || field.text === 'Percent_TotalPrice' || field.text === 'Percent_Avg' || field.text === 'Percent_MarketAmount') {
				// 		if (field.text === 'Percent_TotalAmount') {
				// 			amount = Number(params.node.aggData.TotalAmount)
				// 			percentAmount = ((amount / totalAmount) * 100).toFixed(2) + '%'
				// 			return percentAmount
				// 		} else if (field.text === 'Percent_TotalPrice') {
				// 			amount = Number(params.node.aggData.TotalPrice)
				// 			percentAmount = ((amount / totalPrice) * 100).toFixed(2) + '%'
				// 			return percentAmount
				// 		} else if (field.text === 'Percent_Avg') {
				// 			let fatherCalculated = Number(Number(totalPrice) + Number(totalAmount) / 2)
				// 			let sonTotalPrice = Number(params.node.aggData.TotalPrice)
				// 			let sonTotalAmount = Number(params.node.aggData.TotalAmount)
				// 			let sonCalculated = Number(sonTotalPrice + sonTotalAmount / 2)
				// 			percentAmount = ((sonCalculated / fatherCalculated) * 100).toFixed(2) + '%'
				// 			return percentAmount
				// 		} else if (field.text === 'Percent_MarketAmount') {
				// 			let totalRowsMarket = rowData.reduce((acc: number, row: any) => acc + Number(row['TotalMarket']), 0)
				// 			let totalMarket = params.node.allLeafChildren.reduce((acc: number, row: any) => acc + Number(row.data['TotalMarket']), 0)
				// 			let percentAmount: any = totalMarket && totalRowsMarket ? Number(((totalMarket / totalRowsMarket) * 100)).toFixed(2) + '%' : 0 + '%'
				// 			return percentAmount
				// 		}
				// 	}
				// }
				if (params.node.parent.id !== 'ROOT_NODE_ID' && params.type === 'percentage') { // update params of last child -- //regular or grouping// 
					let field = params.field
					let sumTotal
					let amount
					let percentAmount
					if (field === 'Percent_TotalAmount' && params.value > 0) {
						sumTotal = Number(params.node.parent.aggData['TotalAmount'])
						amount = Number(params.data['TotalAmount'])
						percentAmount = ((amount / sumTotal) * 100).toFixed(2) + '%'
						return percentAmount
					} else if (field === 'Percent_TotalPrice' && params.value > 0) {
						sumTotal = Number(params.node.parent.aggData['TotalPrice'])
						amount = Number(params.data['TotalPrice'])
						percentAmount = ((amount / sumTotal) * 100).toFixed(2) + '%'
						return percentAmount
					} else if (field === 'Percent_Avg' && params.value > 0) {
						let fatherTotalPrice = Number(params.node.parent.aggData['TotalPrice'])
						let fatherTotalAmount = Number(params.node.parent.aggData['TotalAmount'])
						let fatherCalculated = fatherTotalPrice + fatherTotalAmount / 2
						let sonTotalPrice = Number(params.data['TotalPrice'])
						let sonTotalAmount = Number(params.data['TotalAmount'])
						let sonCalculated = sonTotalPrice + sonTotalAmount / 2
						percentAmount = ((sonCalculated / fatherCalculated) * 100).toFixed(2) + '%'
						return percentAmount
					} else if (field === 'Percent_MarketAmount' && params.value > 0) {
						let totalRowsMarket = params.node.parent.allLeafChildren.reduce((acc: number, row: any) => acc + Number(row.data['TotalMarket']), 0)
						let totalMarket = params.data['TotalMarket']
						let percentAmount: any = totalMarket && totalRowsMarket ? Number(((totalMarket / totalRowsMarket) * 100)).toFixed(2) + '%' : 0 + '%'
						return percentAmount
					}
				}

				if (field.type === 'percentage') {
					if (hideWhenGroup && params.field === hideWhenGroup && params.node.allChildrenCount) return ''
					else return params.value + '%'
				}
				else if (field.type === 'fullNumber') {
					if (!params.value) return ''
					if (params.value) return Number(params.value).toLocaleString()
					else return Number(params.value).toLocaleString()
				}
				else if (field.type === 'number') {
					if (!params.value) return ''
					return Number(params.value)
				} else if (field.type === 'numberDisplayZero') {
					if (!params.value) return 0
					return Number(params.value)
				}
				else if (field.type === 'matrizaNumber') {
					if (!params.value || params.value === 0) return ''
					return Number(params.value)
				}
				else if (field.type === 'fixedNumber') {
					if (!params.value) return null
					return Number(Number(params.value).toFixed(2))
				} else if (field.type === 'ceilNumber') {
					if (!params.value) return null
					return Math.ceil(params.value)
				} else if (field.type === 'fixedAndLocaleNumber') {
					if (!params.value) return null
					return params.value.toLocaleString(undefined, {
						minimumFractionDigits: 2,
						maximumFractionDigits: 2
					})
				}
				else if (field.type === 'Date') return moment(new Date(params.value)).format('DD.MM.YY')
				else if (field.type === 'FullDate') return moment(new Date(params.value)).format('DD.MM.YY')
				else if (field.type === 'DateTime') {
					if (params.value && params.value.length) {
						return moment(new Date(params.value)).format('DD.MM.YY | HH:mm')
					} else return ''
				}
				else if (field.type === 'color') return ''
				else if (params.value === 'null') return formatMessage({ id: 'other' })
				else return params.value
			}
			}
			cellRendererParams={{
				'field': field.text, 'type': field.type, 'customToolTip': field.customToolTip ?
					field.customToolTip : '', 'customTooltipTitle': field.customTooltipTitle ?
						field.customTooltipTitle : null, "renderIcons": field.renderIcons ? field.renderIcons : {},
				'clickableCells': field.clickableCells ? field.clickableCells : {},
				'checkBoxCell': field.checkBoxCell ? field.checkBoxCell : {},
				'centerHeader': field.centerHeader ? { centerHeader: true } : {},
				"redHeader": field.redHeader ? true : false,
				"coloredColumn": field.coloredColumn ? field.coloredColumn : {},
				"renderButton": field.renderButton ? field.renderButton : {},
				"renderMultiLine": field.renderMultiLine ? field.renderMultiLine : {}
			}}
			// valueFormatter={(params) => {
			// 	if (field.filter === 'agDateColumnFilter') return 2500
			// 	else return params.value
			// }}
			comparator={
				['number', 'percentage'].includes(field.type) ?
					(valueA, valueB, nodeA, nodeB, isInverted) => Number(valueA) - Number(valueB) :
					(valueA, valueB, nodeA, nodeB, isInverted) => {
						if (valueA == valueB) return 0;
						return (valueA > valueB) ? 1 : -1;
					}
			}
			filter={field.filter ? field.filter : field.noFilter ? '' : ['number', 'percentage', 'fullNumber', 'fixedAndLocaleNumber', 'fixedNumber', 'matrizaNumber'].includes(field.type) ? 'agNumberColumnFilter' : 'agTextColumnFilter'}
			filterParams={field.filter === 'agMultiColumnFilter' ? field.filterParams : field.filter === 'agDateColumnFilter' ? DateFilter : {}}
			floatingFilterComponent={field.filter === 'booleanFilter' ? 'customFloatingFilter' : ''}

			// filter="agSetColumnFilter"
			// filterParams={{ suppressMiniFilter: true }}
			// suppressMiniFilter={true}
			// floatingFilterComponentParams={{

			//  }}
			rowGroup={mergedGroups.includes(field.text)}
			enableRowGroup={groups.includes(field.text)}
			enablePivot={false}
			type={customizer.direction === 'ltr' ? 'rightAligned' : 'rightAligned'}
			pinnedRowCellRenderer="customPinnedRowRenderer"
			pinnedRowCellRendererParams={{ style: { fontWeight: 'bold', } }}// backgroundColor: '#ececec' 
			cellClassRules={ragCellClassRules}
			cellEditor={field.cellEditor ? field.cellEditor : null}
			cellEditorParams={field.cellEditorParams ? field.cellEditorParams : null}
		/>
	}
	var DateFilter = {
		comparator: (filterLocalDateAtMidnight: any, cellValue: any) => {
			let dateAsString = cellValue;

			if (dateAsString == null) {
				return 0;
			}
			dateAsString = moment(cellValue).format('YYYY-MM-DD')
			const dateParts = dateAsString.split('-');

			let fday = String(dateParts[2]);
			const month = Number(dateParts[1]) - 1;
			const year = Number(dateParts[0]);

			let finalDay: any = fday
			if (fday.includes(':')) {
				let aday = fday.split(':')
				let sday = aday[0].split(' ')
				finalDay = Number(sday[0])
			}
			const cellDate = new Date(year, month, Number(finalDay));

			if (cellDate < filterLocalDateAtMidnight) {
				return -1;
			} else if (cellDate > filterLocalDateAtMidnight) {
				return 1;
			}
			return 0;
		}
	}
	const ragCellClassRules = {
		'success': function (params: any) {
			if (success) {
				if (params.data[success] || success === 'all') {
					if (cellGreenFields.length) {
						let foundMatch = false
						for (let i = 0; i < cellGreenFields.length || foundMatch; i++) {
							if (cellGreenFields[i] == params.colDef.field) {
								foundMatch = true
								return params.value
							}
						}
					}
				}
			}
		},
		'success-dark': function (params: any) {
			if (successDark) {
				if (params.data[successDark] || successDark === 'all') {
					if (cellGreenFields.length) {
						let foundMatch = false
						for (let i = 0; i < cellGreenFields.length || foundMatch; i++) {
							if (cellGreenFields[i] == params.colDef.field) {
								foundMatch = true
								return params.value
							}
						}
					}
				}
			}
		},
		'success-light': function (params: any) {
			if (successLight) {
				if (params.data[successLight] || successLight === 'all') {
					if (cellLightBlueFields.length) {
						let foundMatch = false
						for (let i = 0; i < cellLightBlueFields.length || foundMatch; i++) {
							if (cellLightBlueFields[i] == params.colDef.field) {
								foundMatch = true
								return params.value
							}
						}
					}
				}
			}
		},
		'danger': function (params: any) {
			if (danger) {
				if (params.data[danger]) {
					if (params.colDef.field == cellRedFields[0]) return params.value
				}
			}
			if (dangerUnderZero) {
				if (params.data[dangerUnderZero] < 0) {
					return params.value
				}
			}
		},
		'blue-text': function (params: any) {
			if (blueField) {
				if (params.data[blueFlag] && params.data[blueFlag] === 1) {
					if (params.colDef.field == blueField) return params.value
				}
			}
		},
		'yellow-text': function (params: any) {
			if (yellowField) {
				if (params.data[yellowFlag] && params.data[yellowFlag] === 1) {
					if (params.colDef.field == yellowField) return params.value
				}
			}
		},
		'warning-bg': function (params: any) {
			if (cellBgRedMatriza) {
				if (params.node.rowPinned != "bottom" && (params.value === undefined || params.value === 0)) {
					return { backgrondColor: '' }
				}
			}
		},
		'left-border': function (params: any) {
			if (leftBorderField) {
				if (params.colDef.field === leftBorderField) {
					return params.value
				}
			}
		},
		'bg-gray': function (params: any) {
			if (params.node && params.node.rowPinned) {
				return params.value
			}
		},
		'deleted': function (params: any) {
			if (deletedFlag) {
				if (params.node.data && params.node.data[deletedFlag]) {
					return params.value
				}
			}
		},
		'd-ltr': function (params: any) {
			if (customizer.direction === 'ltr' && !customFieldsDirection) {
				return params.value
			} else if (customFieldsDirection === 'ltr') {
				return params.value
			}
		},
		'd-rtl': function (params: any) {
			if (customizer.direction === 'rtl' && !customFieldsDirection) {
				return params.value
			} else if (customFieldsDirection === 'rtl') {
				return params.value
			}
		},
		'bg-light-blue': function (params: any) {
			if (colorColumnFlag) {
				if (params.data[colorColumnFlag] && params.data[colorColumnFlag] === params.colDef.field) {
					return params.value
				}
			}
		},
	}
	const getAllRows = () => {
		let rowsData: any[] = [];
		let selectedRows = selected.map((s: any) => s.data ? id ? s.data[id] : s.data['id'] : undefined)
		if (gridApi) {
			gridApi.forEachNodeAfterFilterAndSort((node: any) => {
				if (node.data) {
					node.data.isSelected = selectedRows.includes(node.data[id ? id : 'id'])
					if(node.data.isSelected && node.data.faces==0) node.data.faces=1
					rowsData.push(node.data)
				}
			});
		}
		else rowsData = rowData
		return rowsData;
	}

	const setSelectedRows = (selected: any[]) => {
		// if(gridApi && rows && rows.length ) gridApi.showLoadingOverlay()

		setTimeout(() => {
			let selectedRows = selected.map((s: any) => s.data ? id ? s.data[id] : s.data['id'] : undefined)

			if (gridApi) gridApi.forEachNodeAfterFilterAndSort((node: any) => {
				if (node.data) {
					node.setSelected(selectedRows.includes(node.data[id ? id : 'id']))
				};
			})
			if (gridApi && rows && rows.length) gridApi.hideOverlay()

		}, 150);

	}

	const saveGrid = () => {
		changeRowDataSelected(gridApi, rowData, selected)
		setRefreshDataIconBg('bg-gray')
		setSaveIconBg('bg-gray')
		if (onClickSave) { setTimeout(() => { onClickSave(getAllRows()) }, 50); }
	}

	const Print = (<div className="d-flex justify-content-center align-items-center ml-05 cursor-pointer bg-gray font-medium-1 text-black text-bold-600 p-05">
		<ReactToPrint
			trigger={() => <div> <Icon src={'../' + config.iconsPath + "table/print.svg"} style={{ height: '0.5rem', width: '0.5rem' }} /></div>}
			content={() => TableRef.current}
		/>
	</div>)
	const Excel = (<div className="d-flex justify-content-center align-items-center ml-05 cursor-pointer font-medium-1 text-black text-bold-600 bg-gray">
		<ExportExcel fields={fields} data={checkbox ? getAllRows() : rowData} direction={customizer.direction} translateHeader={translateHeader}
			icon={<Icon src={'../' + config.iconsPath + "table/excel-download.svg"} />} />
	</div>)
	const columnDisplayer = (<div onClick={() => setShowColumnPoup(true)}
		className="ml-05 d-flex justify-content-center align-items-center cursor-pointer bg-gray font-medium-1 text-black text-bold-600 p-05">
		<Icon src={'../' + config.iconsPath + "table/column-view.svg"} style={{ height: '0.5rem', width: '0.5rem' }} />
	</div>)
	const refreshRowDataIcon = (
		<div onClick={() => changeRowDataSelected(gridApi, rowData, selected)}
			className={`ml-05 d-flex justify-content-center align-items-center cursor-pointer font-medium-1 text-black text-bold-600 p-05 ${refreshDataIconBg}`}>
			<img src={'../' + config.iconsPath + "aggrid/refresh.png"} style={{ height: '1.5rem', width: '1.5rem' }} />
		</div>
	)
	const saveIcon = (
		<div onClick={() => { saveGrid() }}
			className={`ml-05 d-flex justify-content-center align-items-center cursor-pointer font-medium-1 text-black text-bold-600 p-05 ${saveIconBg}`}>
			<img src={'../' + config.iconsPath + "aggrid/save.png"} style={{ height: '1.5rem', width: '1.5rem' }} />
		</div>
	)



	React.useEffect(() => {
		if (pagination) {
			if (gridApi) setPaginationRowCount(gridApi.paginationGetRowCount())
			if (gridApi) gridApi.paginationSetPageSize(paginationPageSize)
			setClassName20(paginationPageSize === 20 ? 'paginationPageSizeSelected paginationPageSize rounded' : 'paginationPageSize')
			setClassName50(paginationPageSize === 50 ? 'paginationPageSizeSelected ml-1 paginationPageSize rounded' : 'ml-1 paginationPageSize')
			setClassName100(paginationPageSize === 100 ? 'paginationPageSizeSelected ml-1 paginationPageSize rounded' : 'ml-1 paginationPageSize')
			setClassName200(paginationPageSize === 200 ? 'paginationPageSizeSelected ml-1 paginationPageSize rounded' : 'ml-1 paginationPageSize')
		}
	}, [paginationPageSize])


	const paginationChoseAmount = (
		<div className={classnames("d-flex align-items-center paginationPageSizeContainer ml-1", {
			"justify-content-end": customizer.direction == 'ltr',
			"position-left-0": customizer.direction == 'ltr',
		})}>
			{//Todo: use map and with if statement reverse the array 
			customizer.direction == 'ltr' ?
				<div className='d-flex align-items-center'>
					<span className={classnames("ml-2 pagination__rowCount", {
					})} >{formatMessage({ id: 'rowNumber' })}: {paginationRowCount}</span>
					<div className={className200} onClick={() => setPaginationPageSize(200)}>200</div>
					<div className={className100} onClick={() => setPaginationPageSize(100)}>100</div>
					<div className={className50} onClick={() => setPaginationPageSize(50)}>50</div>
					<div className={className20} onClick={() => setPaginationPageSize(20)}>20</div>
				</div> :
				<div className='d-flex align-items-center'>
					<div className={className20} onClick={() => setPaginationPageSize(20)}>20</div>
					<div className={className50} onClick={() => setPaginationPageSize(50)}>50</div>
					<div className={className100} onClick={() => setPaginationPageSize(100)}>100</div>
					<div className={className200} onClick={() => setPaginationPageSize(200)}>200</div>
					<span className={classnames("ml-2 pagination__rowCount", {
					})} >{formatMessage({ id: 'rowNumber' })}: {paginationRowCount}</span>
				</div>}
		</div>
	)
	const [totalPageCount, setTotalPageCount] = React.useState<any>(null)
	const [currentPage, setCurrentPage] = React.useState<any>(null)
	const onBtFirst = () => {
		if (gridApi) {
			gridApi.paginationGoToFirstPage();
			setTotalPageCount(gridApi.paginationGetTotalPages())
			setCurrentPage(gridApi.paginationGetCurrentPage() + 1)
		}
	};

	const onBtLast = () => {
		if (gridApi) {
			gridApi.paginationGoToLastPage();
			setTotalPageCount(gridApi.paginationGetTotalPages())
			setCurrentPage(gridApi.paginationGetCurrentPage() + 1)
		}
	};

	const onBtNext = () => {
		if (gridApi) {
			gridApi.paginationGoToNextPage();
			setTotalPageCount(gridApi.paginationGetTotalPages())
			setCurrentPage(gridApi.paginationGetCurrentPage() + 1)
		}
	};

	const onBtPrevious = () => {
		if (gridApi) {
			gridApi.paginationGoToPreviousPage();
			setTotalPageCount(gridApi.paginationGetTotalPages())
			setCurrentPage(gridApi.paginationGetCurrentPage() + 1)
		}
	};

	const paginationNextPage = (
		<div>
			{
				customizer.direction == 'ltr' ?
					<div className='d-flex justify-content-center align-items-center font-small-4'>

						<button disabled={gridApi && gridApi.paginationGetCurrentPage() + 1 == gridApi.paginationGetTotalPages() ? true : false} className='no-outline no-border cursor-pointer' onClick={onBtLast} >
							<ChevronsRight color={gridApi && gridApi.paginationGetCurrentPage() == 0 ? 'gray' : 'black'} size={16} />
						</button>
						<button disabled={gridApi && gridApi.paginationGetCurrentPage() + 1 == gridApi.paginationGetTotalPages() ? true : false} onClick={onBtNext}
							className={classnames("no-outline no-border d-flex justify-content-center align-items-center", {
								"cursor-default": gridApi && gridApi.paginationGetCurrentPage() + 1 == gridApi.paginationGetTotalPages(),
							})}>
							<ChevronRight color={gridApi && gridApi.paginationGetCurrentPage() + 1 == gridApi.paginationGetTotalPages() ? 'gray' : 'black'} size={15} /></button>

						<div className='mr-05'>{formatMessage({ id: 'outof' })} <span className='text-bold-700'>{totalPageCount ? totalPageCount : gridApi ? gridApi.paginationGetTotalPages() : 0}</span></div>
						<div className='mr-05'>{formatMessage({ id: 'page' })} <span className='text-bold-700'>{currentPage ? currentPage : gridApi ? gridApi.paginationGetCurrentPage() + 1 : 0}</span></div>
						<button disabled={gridApi && gridApi.paginationGetCurrentPage() == 0 ? true : false} onClick={onBtPrevious}
							className={classnames("no-outline no-border d-flex justify-content-center align-items-center", {
								"cursor-default": gridApi && gridApi.paginationGetCurrentPage() == 0,
							})}>
							<ChevronLeft color={gridApi && gridApi.paginationGetCurrentPage() == 0 ? 'gray' : 'black'} size={15} /></button>
						<button disabled={gridApi && gridApi.paginationGetCurrentPage() == 0 ? true : false} className='no-outline no-border cursor-pointer' onClick={onBtFirst}>
							<ChevronsLeft color={gridApi && gridApi.paginationGetCurrentPage() + 1 == gridApi.paginationGetTotalPages() ? 'gray' : 'black'} size={16} />
						</button>
					</div >
					:
					<div className='d-flex justify-content-center align-items-center font-small-4'>
						<button disabled={gridApi && gridApi.paginationGetCurrentPage() == 0 ? true : false} className='no-outline no-border cursor-pointer' onClick={onBtFirst}>
							<ChevronsRight color={gridApi && gridApi.paginationGetCurrentPage() == 0 ? 'gray' : 'black'} size={16} />
						</button>

						<button disabled={gridApi && gridApi.paginationGetCurrentPage() == 0 ? true : false} onClick={onBtPrevious} className={classnames("no-outline no-border d-flex justify-content-center align-items-center", {
							"cursor-default": gridApi && gridApi.paginationGetCurrentPage() == 0,
						})}>
							<ChevronRight color={gridApi && gridApi.paginationGetCurrentPage() == 0 ? 'gray' : 'black'} size={15} /></button>
						<div className='mr-05'>{formatMessage({ id: 'page' })} <span className='text-bold-700'>{currentPage ? currentPage : gridApi ? gridApi.paginationGetCurrentPage() + 1 : 0}</span></div>
						<div className='mr-05'>{formatMessage({ id: 'outof' })} <span className='text-bold-700'>{totalPageCount ? totalPageCount : gridApi ? gridApi.paginationGetTotalPages() : 0}</span></div>
						<button disabled={gridApi && gridApi.paginationGetCurrentPage() + 1 == gridApi.paginationGetTotalPages() ? true : false} onClick={onBtNext}
							className={classnames("no-outline no-border d-flex justify-content-center align-items-center", {
								"cursor-default": gridApi && gridApi.paginationGetCurrentPage() + 1 == gridApi.paginationGetTotalPages(),
							})}>
							<ChevronLeft color={gridApi && gridApi.paginationGetCurrentPage() + 1 == gridApi.paginationGetTotalPages() ? 'gray' : 'black'} size={15} /></button>
						<button disabled={gridApi && gridApi.paginationGetCurrentPage() + 1 == gridApi.paginationGetTotalPages() ? true : false} className='no-outline no-border cursor-pointer' onClick={onBtLast} >
							<ChevronsLeft color={gridApi && gridApi.paginationGetCurrentPage() + 1 == gridApi.paginationGetTotalPages() ? 'gray' : 'black'} size={16} />
						</button>
					</div >
			}
		</div>

	)




	const Options = (
		<div className="d-flex justify-content-evenly">
			{saveOption && (saveIcon)}
			{changeRowDataOnSelectionChange && (refreshRowDataIcon)}
			{displayExcel && (Excel)}
			{displayColumnDisplayer && (columnDisplayer)}
			{/* {displayPrint && (Print)} */}
		</div>
	)
	const freeSearchDiv = freeSearch ? (
		<div className='mr-1 freeSearchInput'>
			<InputText searchIcon onChange={onFilterTextBoxChanged} disabled={!freeSearch || !rows || !rows.length} placeholder={freeSearch.title ? freeSearch.title : 'Send title'} inputRef={freeSearchInputRef} />
		</div>
	) : null

	const customButtonDiv = buttonOnClick ? (
		<Button
			disabled={buttonOnClick.disabled}
			onClick={buttonOnClick.function}
			color="primary"
			className="width-10-rem ml-auto height-2-5-rem round text-bold-400 d-flex justify-content-center align-items-center cursor-pointer btn-primary"
		>
			+ &nbsp;<FormattedMessage id={buttonOnClick.title} />
		</Button>
	) : null

	function onFilterTextBoxChanged() {
		if (freeSearch && rows && rows.length) {
			let filteredRowData = freeSearch.rows.filter((row: any) => {
				let isFound: boolean = false
				if (freeSearch.fieldsToSearch && freeSearch.fieldsToSearch[0]) {
					freeSearch.fieldsToSearch.forEach((field: any) => {
						let searchField = String(row[field])
						if (searchField && searchField.includes(String(freeSearchInputRef.current.value))) isFound = true
					})
				}
				return isFound
			})
			gridApi.setRowData(filteredRowData)
			if (pagination) {
				setPaginationRowCount(gridApi.paginationGetRowCount())
			}
		}
	}


	const updateRowsCheckboxFilter = (options: any) => {
		setCheckboxFilterChecked(options)
		let newRows = checkboxFilterBox ? checkboxFilterBox.rows.filter((row: any) => options.includes(row[checkboxFilterBox.flag])) : rows
		gridApi.setRowData(newRows)
	}

	React.useEffect(() => {

		if (checkboxFilterBox) setCheckboxFilterChecked(checkboxFilterBox.options)
	}, [checkboxFilterBox])


	const checkboxFilterDiv = (
		checkboxFilterBox && checkboxFilterBox.options && checkboxFilterBox.options.length > 0 &&
			<ComboBox caption={formatMessage({ id: 'choose user type' })} mix='edit-order__filter'>
				{(onHide, childRef) => (
					<CheckBoxTree
						childRef={childRef}
						onChange={updateRowsCheckboxFilter}
						onHide={onHide}
						options={checkboxFilterBox ? checkboxFilterBox.options : []}
						checkedIds={checkboxFilterChcked}
					/>
				)}
			</ComboBox>
			)




	const checkIfIsInMultiAboveGridFilter = (props: any, gridClick: boolean, aboveFilterParams?: any) => {

		let fieldName = props && props.colDef && props.colDef.field ? props.colDef.field : null
		let newAboveFilter = aboveGridFilterList.map((item: any) => {
			if (item.field === fieldName) {
				return null
			} else return item
		}).filter((g: any) => g)
		setTimeout(() => {
			aboveGridFilterFunc(props, false, newAboveFilter, aboveFilterParams)
		}, 5);
	}

	const addAutoAboveFilter = (arr: any, resetFilter?: boolean) => {
		if (arr && arr.length) {
			let data: any = []
			arr.forEach((a: any) => {
				if (a.length) {
					data = data ? [...data, { type: a[0].type, dontFilter: a[0].dontFilter, cannotRemove: a[0].cannotRemove, field: a[0].name, key: a[0].key, preText: formatMessage({ id: a[0].name }), data: a.map((a: any) => { return { value: a.value, label: a.label } }) }] :
						[{ field: a[0].name, key: a[0].key, preText: formatMessage({ id: a[0].name }), data: a.map((a: any) => { return { value: a.value, label: a.label } }) }]
				}
			})
			aboveGridFilterFunc(data, resetFilter)
		} else if (arr.api) {
			aboveGridFilterFunc(arr)
		}
	}
	const aboveGridFilterFunc = (props: any, resetFilter?: boolean, customAboveFilter?: any, aboveFilterParams?: any) => {
		if (!props.length && props.colDef && props.colDef.cellRendererParams && props.colDef.cellRendererParams.clickableCells && props.colDef.cellRendererParams.clickableCells.customAboveGridFilter) {
			props.colDef.cellRendererParams.clickableCells.customAboveGridFilter(props)
		} else {
			let filterArr = props
			if (props.api) {
				filterArr = [{ cannotRemove: resetFilter, type: props.colDef.cellRendererParams.type, field: props.colDef.field, key: props.colDef.field, preText: formatMessage({ id: props.colDef.headerName }), data: [{ label: props.value, value: props.data[`${props.colDef.field}Id`] ? props.data[`${props.colDef.field}Id`] : props.value }] }]
			}
			let newAboveFilter: any = customAboveFilter ? [...customAboveFilter, ...filterArr] : resetFilter ? [...filterArr] : [...aboveGridFilterList, ...filterArr]


			if (aboveFilterParams && aboveFilterParams.length && Array.isArray(aboveFilterParams)) {
				let fieldN = filterArr[0].field
				let valueN = filterArr[0].data[0].value
				let selectedData = rows.find((r: any) => r[fieldN] == valueN)
				aboveFilterParams.forEach((item: any) => {
					let id = selectedData[item.id]
					let isFieldAlreadyFiltered: boolean = false
					customAboveFilter.forEach((i: any) => { if (i.field == item.field) isFieldAlreadyFiltered = true })
					!isFieldAlreadyFiltered && filterArr.push({ cannotRemove: false, type: 'text', field: item.field, key: item.id, preText: formatMessage({ id: item.field }), data: [{ label: selectedData[item.field], value: id }] })
				})
				newAboveFilter = customAboveFilter ? [...customAboveFilter, ...filterArr] : resetFilter ? [...filterArr] : [...aboveGridFilterList, ...filterArr]
			}

			setAboveGridFilterList(newAboveFilter)
			setHiddenColumns([...newAboveFilter.map((p: any) => {
				if (p.data && p.data.length > 1) return null
				else return p.field
			})])

			let newRows = rows ? rows.filter((row: any) => {
				let trueAm = 0;
				newAboveFilter.forEach((item: any) => {
					if (!item.dontFilter) {
						item.data.forEach((i: any) => {
							if (row[item.key] == i.value || row[item.key] == i.label) trueAm++
						})
					} else trueAm++
				})
				return trueAm === newAboveFilter.length ? true : false
			}) : []
			setRowData(newRows && newRows.length ? newRows : [])
			if (pagination) {
				setPaginationRowCount(newRows.length)
			}

			if (afterAboveFilterRemovedOrAdded) afterAboveFilterRemovedOrAdded(newAboveFilter, 'addedFilter')

		}
	}
	const removeColumnFromFilter = (column: string) => {
		let newFilter = aboveGridFilterList.filter((o: any) => o.field != column)
		setHiddenColumns([...newFilter.map((p: any) => {
			if (p.data && p.data.length > 1) return null
			else return p.field
		})])
		setAboveGridFilterList(newFilter)
		let newRows = rows.filter((row: any) => {
			let trueAm = 0;
			newFilter.forEach((item: any) => {
				if (!item.dontFilter) {
					item.data.forEach((i: any) => {
						if (row[item.key] == i.value || row[item.key] == i.label) trueAm++
					})
				} else trueAm++
			})
			return trueAm === newFilter.length ? true : false
		})

		setRowData(newFilter && newFilter.length ? newRows : rows)
		if (pagination) {
			setPaginationRowCount(newFilter && newFilter.length ? newRows.length : rows.length)
		}
		if (afterAboveFilterRemovedOrAdded) afterAboveFilterRemovedOrAdded(newFilter, 'removedColumn')

	}
	const removeSingleFilterFromMulti = (filter: any, item: any) => {
		let newHiddenColumns: any = []
		let filterList = aboveGridFilterList.map((i: any) => {
			let subFilter = i.data
			if (i.field === filter.field) {
				subFilter = i.data.filter((iData: any) => iData.label != item.label)
				if (subFilter.length === 1) {
					newHiddenColumns.push(i.field)
				}
			}
			if (!subFilter) return ''
			else return { ...i, data: subFilter }
		}).filter((g: any) => g)


		setHiddenColumns([...hiddenColumns, ...newHiddenColumns])

		setAboveGridFilterList(filterList)
		let newRows = rows.filter((row: any) => {
			let trueAm = 0;
			filterList.forEach((item: any) => {
				if (!item.dontFilter) {
					item.data.forEach((i: any) => {
						if (row[item.key] == i.value || row[item.key] == i.label) trueAm++
					})
				} else trueAm++
			})
			return trueAm === filterList.length ? true : false
		})
		setRowData(filterList && filterList.length ? newRows : rows)
		if (pagination) {
			setPaginationRowCount(filterList && filterList.length ? newRows.length : rows.length)
		}

		if (afterAboveFilterRemovedOrAdded) afterAboveFilterRemovedOrAdded(filterList, 'removedSingle')
	}

	const Groups = (<>
		<div className="d-flex">
			{groups!.map((group: string, index: number) => {
				let columnResults = rows.map((row: any) => { return row[group] }).filter((a: any) => a)
				columnResults = [formatMessage({ id: 'select_all' }), ...new Set(columnResults)]
				let groupColumnResultsPicker = columnResultsPicker.map((cr: any) => { if (cr.group === group) { return cr.option } else return })
				let removed = groupColumnResultsPicker.filter((gcrp: any) => columnResults.includes(gcrp))
				return (// !hiddenColumns.includes(group)
					<div className="position-relative" key={index}>
						<div
							onClick={!mergedGroups.includes(group) ? (e) => { mergeByGroups(mergedGroups, group) } : (e) => { mergeByGroups(mergedGroups, group, true) }}
							className={classnames("d-flex align-items-center bg-gray position-relative font-small-3 cursor-pointer py-05 border-2 px-1 text-black text-bold-700  mr-05 border-radius-2rem", {
								"border-turquoise": mergedGroups.includes(group),
								//"bg-gray": !mergedGroups.includes(grfoup)
							})}>
							{<FormattedMessage id={group} />}
							{/* <div onClick={(e) => { e.stopPropagation(); setIsGroupsPopupOpen(group) }}
								className="pl-1">
								<Icon className={classnames("", { "svg-black": removed.length })} src={'../' + config.iconsPath + "table/filter.svg"} style={{ height: '0.5rem', width: '0.5rem' }} />
							</div> */}
							<Badge pill color='primary' className='badge-up bg-turquoise border-white border-1 text-bold-600 text-white width-1-rem position-left-0 position-top-02'>
								{mergedGroups.findIndex((mg: any) => mg === group) > -1 ? mergedGroups.findIndex((mg: any) => mg === group) + 1 : null}
							</Badge>
						</div>
						<Popup
							varient={'checkbox'}
							className="width-15-rem"
							multiple
							search
							selected={groupColumnResultsPicker}
							isOpen={isGroupsPopupOpen === group}
							onClick={(): void => {
								return;
							}}
							options={columnResults
								.map((option: any) => ({
									text: option,
									action: (): void => {
										option !== formatMessage({ id: 'select_all' })
										let newColumnResultsPicker = columnResultsPicker.filter((o: any) => o.group === group && o.option !== option)
										setColumnResultsPicker(newColumnResultsPicker)
										filterRowsResult(newColumnResultsPicker)
									},
									removeSelect: (): void => {
										option !== formatMessage({ id: 'select_all' })
										let newColumnResultsPicker = [...columnResultsPicker, { group, option }].filter(o => o)
										setColumnResultsPicker(newColumnResultsPicker)
										filterRowsResult(newColumnResultsPicker)
										// setIsGroupsPopupOpen(false);
									},
									selectAll: (): void => {
										let newColumnResultsPicker = columnResults.map((option: any) => { return { group, option } }).filter((o: any) => o)
										setColumnResultsPicker(newColumnResultsPicker)
										filterRowsResult(newColumnResultsPicker)
									},
									deSelectAll: (): void => {
										let newColumnResultsPicker = columnResultsPicker.filter((cr: any) => cr.group !== group).filter((o: any) => o)
										setColumnResultsPicker(newColumnResultsPicker)
										filterRowsResult(newColumnResultsPicker)
									},
								}))}
							onOutsideClick={(): void => setIsGroupsPopupOpen(false)}
						/>
					</div>
				)
			}
			)
			}
		</div>

	</>
	)
	const TotalRow = (fields: any) => {
		if (rows && rows.length) {
			var result: any = {};
			let selectedRows = selected.map((s: any) => s.data ? id ? s.data[id] : s.data['id'] : undefined)
			let allRows: any = rows
			let newRows: any = []
			gridApi && gridApi.forEachNodeAfterFilterAndSort((node: any) => {
				if (node.data) newRows.push(node.data)
			})
			gridApi ? allRows = newRows : null
			if (id) {
				allRows = rows.filter((row: any) => ['all', 'selected'].includes(selectedState) ? selectedRows.includes(row[id ? id : 'id']) : !selectedRows.includes(row[id ? id : 'id']))
			}
			if (allRows && allRows.length) {
				fields.forEach((field: any, index: number) => {
					if (field.fields) {
						field.fields.forEach((field: any, index: number) => {
							if (totalRows.includes(field.text)) {
								let reduce: number = Math.round(allRows.reduce((acc: number, row: any) => { return acc + Number(row[field.text]) }, 0))
								result[field.text] = index === 0 ? `${formatMessage({ id: "rows number" })} ${allRows.length}` : `${field.type === 'percentage' ? '100' : reduce.toLocaleString()}${field.type === 'percentage' ? '%' : ''}`
							}

						})
					} else {
						if (totalRows.includes(field.text)) {
							let allRowsWithValue: number = 0
							let reduce: any = field.totalRow && field.totalRow === 'average' ? ((allRows.reduce((acc: number, row: any) => {
								if (Number(row[field.text]) > 0) allRowsWithValue += 1
								return Number(row[field.text]) > 0 ? acc + Number(row[field.text]) : acc
							}, 0)) / allRowsWithValue).toFixed(2)
								: field.totalRow === 'last' ? Number(allRows[allRows.length - 1][field.text])
									:
									Math.round(allRows.reduce((acc: number, row: any) => { return row[field.text] ? acc + Number(row[field.text]) : acc }, 0))

							result[field.text] = index === 0 ? `${formatMessage({ id: "rows number" })} ${allRows.length}` : `${field.type === 'percentage' ? '100' : reduce === 0 ? reduce = '' : reduce.toLocaleString()}${field.type === 'percentage' ? '%' : ''}`

						}

					}
				})

				return [result]
			} else {
				{ return [0] }
			}
		} else { return [0] }
	}



	const onGridReady = (params: any) => {
		if (displayLoadingScreen) {
			params.api.showLoadingOverlay();
			if (rows && rows.length) setTimeout(() => { if (params.api && params.api.gridOptionsWrapper && params.api.gridOptionsWrapper.gridOptions && params.api.gridOptionsWrapper.gridOptions.api) params.api.gridOptionsWrapper.gridOptions.api.hideOverlay(); }, 100);
		}
		setRowData(rows && rows.length && fields && fields.length && !noSort ? Sorting(rows, { field: customSortField ? customSortField : fields[defaultSortFieldNum ? defaultSortFieldNum : 0].text, type: customSortType ? customSortType : typeof (rows[0][fields[defaultSortFieldNum ? defaultSortFieldNum : 0].text]), asc: descSort ? false : true, times: 0 }) : rows && rows.length && fields && fields.length ? rows : [])
		setGridApi(params.api);
		setGridColumnApi(params.columnApi);
		if (pagination) {
			setPaginationRowCount(rows ? rows.length : 0)
			params.api.paginationSetPageSize(paginationPageSize)
			setClassName20(paginationPageSize === 20 ? 'paginationPageSizeSelected paginationPageSize rounded' : 'paginationPageSize')
			setClassName50(paginationPageSize === 50 ? 'paginationPageSizeSelected ml-1 paginationPageSize rounded' : 'ml-1 paginationPageSize')
			setClassName100(paginationPageSize === 100 ? 'paginationPageSizeSelected ml-1 paginationPageSize rounded' : 'ml-1 paginationPageSize')
			setClassName200(paginationPageSize === 200 ? 'paginationPageSizeSelected ml-1 paginationPageSize rounded' : 'ml-1 paginationPageSize')
		}
		setRender(true)
		setFirstRowData(rowData)
		setTimeout(() => {
			setSaveIconBg('bg-gray')
			setRefreshDataIconBg('bg-gray')
		}, 500);
	}
	React.useEffect(() => {
		if (gridApi) {
			setRowData(rows && rows.length && fields && fields.length && !noSort ? Sorting(rows, { field: customSortField ? customSortField : fields[defaultSortFieldNum ? defaultSortFieldNum : 0].text, type: customSortType ? customSortType : typeof (rows[0][fields[defaultSortFieldNum ? defaultSortFieldNum : 0].text]), asc: descSort ? false : true, times: 0 }) : rows && rows.length && fields && fields.length ? rows : [])
			setSelectedRows(selected)
		}
	}, [gridApi])


	React.useEffect(() => {

		if (gridApi && (rows && !rows.length)) {
			if (displayLoadingScreen) {
				gridApi.showLoadingOverlay();
			} else {
				setHiddenColumns([])
				setRowData([])
				gridApi.setRowData([])
				gridApi.hideOverlay()
				gridApi.showNoRowsOverlay()
			}

			if (pagination) {
				setPaginationRowCount(rows ? rows.length : 0)
				if (gridApi) gridApi.paginationSetPageSize(paginationPageSize)
				setClassName20(paginationPageSize === 20 ? 'paginationPageSizeSelected paginationPageSize rounded' : 'paginationPageSize')
				setClassName50(paginationPageSize === 50 ? 'paginationPageSizeSelected ml-1 paginationPageSize rounded' : 'ml-1 paginationPageSize')
				setClassName100(paginationPageSize === 100 ? 'paginationPageSizeSelected ml-1 paginationPageSize rounded' : 'ml-1 paginationPageSize')
				setClassName200(paginationPageSize === 200 ? 'paginationPageSizeSelected ml-1 paginationPageSize rounded' : 'ml-1 paginationPageSize')
			}
			// setTimeout(() => {
			// 	gridApi.showNoRowsOverlay()
			// }, 1);
		} else {
			if (displayLoadingScreen != undefined && gridApi) {
				if (displayLoadingScreen) {
					gridApi.showLoadingOverlay();
				} else if (!displayLoadingScreen && (rows && !rows.length)) {
					gridApi.showNoRowsOverlay();
				} else {
					if (rows && rows.length) gridApi.hideOverlay()
					else gridApi.showNoRowsOverlay()
					gridApi.setRowData(rows && rows.length && fields && fields.length && !noSort ? Sorting(rows, { field: customSortField ? customSortField : fields[defaultSortFieldNum ? defaultSortFieldNum : 0].text, type: customSortType ? customSortType : typeof (rows[0][fields[defaultSortFieldNum ? defaultSortFieldNum : 0].text]), asc: descSort ? false : true, times: 0 }) : rows && rows.length && fields && fields.length ? rows : [])

					setRowData(rows && rows.length && fields && fields.length && !noSort ? Sorting(rows, { field: customSortField ? customSortField : fields[defaultSortFieldNum ? defaultSortFieldNum : 0].text, type: customSortType ? customSortType : typeof (rows[0][fields[defaultSortFieldNum ? defaultSortFieldNum : 0].text]), asc: descSort ? false : true, times: 0 }) : rows && rows.length && fields && fields.length ? rows : [])
					setSelected(rows ? rows.map((row: any) => { if (row.isSelected) { return { data: row } } else return undefined }).filter((a: any) => a) : [])
					let newSelected = rows ? rows.map((row: any) => { if (row.isSelected) { return { data: row } } else return undefined }).filter((a: any) => a) : []
					if (newSelected) {
						setSelectedRows(newSelected)
						setTimeout(() => {
							setSaveIconBg('bg-gray')
							setRefreshDataIconBg('bg-gray')
						}, 500);
					}
					if (pagination) {
						setPaginationRowCount(rows ? rows.length : 0)
						if (gridApi) gridApi.paginationSetPageSize(paginationPageSize)
						setClassName20(paginationPageSize === 20 ? 'paginationPageSizeSelected paginationPageSize rounded' : 'paginationPageSize')
						setClassName50(paginationPageSize === 50 ? 'paginationPageSizeSelected ml-1 paginationPageSize rounded' : 'ml-1 paginationPageSize')
						setClassName100(paginationPageSize === 100 ? 'paginationPageSizeSelected ml-1 paginationPageSize rounded' : 'ml-1 paginationPageSize')
						setClassName200(paginationPageSize === 200 ? 'paginationPageSizeSelected ml-1 paginationPageSize rounded' : 'ml-1 paginationPageSize')
					}
				}
			}
		}
	}, [displayLoadingScreen])

	React.useEffect(() => {
		if (gridApi && (rows && !rows.length)) {
			if (displayLoadingScreen) {
				gridApi.showLoadingOverlay();
			} else {
				setHiddenColumns([])
				setRowData([])
				gridApi.setRowData([])
				gridApi.hideOverlay()
				gridApi.showNoRowsOverlay()
			}
			if (pagination) {
				setPaginationRowCount(rows ? rows.length : 0)
				if (gridApi) gridApi.paginationSetPageSize(paginationPageSize)
				setClassName20(paginationPageSize === 20 ? 'paginationPageSizeSelected paginationPageSize rounded' : 'paginationPageSize')
				setClassName50(paginationPageSize === 50 ? 'paginationPageSizeSelected ml-1 paginationPageSize rounded' : 'ml-1 paginationPageSize')
				setClassName100(paginationPageSize === 100 ? 'paginationPageSizeSelected ml-1 paginationPageSize rounded' : 'ml-1 paginationPageSize')
				setClassName200(paginationPageSize === 200 ? 'paginationPageSizeSelected ml-1 paginationPageSize rounded' : 'ml-1 paginationPageSize')
			}
		} else {
			if (displayLoadingScreen && gridApi && rows && rows.length) {
				gridApi.showLoadingOverlay();
			} else {
				setRowData(rows && rows.length && fields && fields.length && !noSort ? Sorting(rows, { field: customSortField ? customSortField : fields[defaultSortFieldNum ? defaultSortFieldNum : 0].text, type: customSortType ? customSortType : typeof (rows[0][fields[defaultSortFieldNum ? defaultSortFieldNum : 0].text]), asc: descSort ? false : true, times: 0 }) : rows && rows.length && fields && fields.length ? rows : [])
				if (gridApi) {
					gridApi.setRowData(rows && rows.length && fields && fields.length && !noSort ? Sorting(rows, { field: customSortField ? customSortField : fields[defaultSortFieldNum ? defaultSortFieldNum : 0].text, type: customSortType ? customSortType : typeof (rows[0][fields[defaultSortFieldNum ? defaultSortFieldNum : 0].text]), asc: descSort ? false : true, times: 0 }) : rows && rows.length && fields && fields.length ? rows : [])

					let newSelected = rows ? rows.map((row: any) => { if (row.isSelected) { return { data: row } } else return undefined }).filter((a: any) => a) : []
					setSelectedRows(newSelected)
					setTimeout(() => {
						setSaveIconBg('bg-gray')
						setRefreshDataIconBg('bg-gray')
					}, 500);
					if (rows && rows.length) gridApi.hideOverlay()
					else gridApi.showNoRowsOverlay()
					if (pagination) {
						setPaginationRowCount(rows ? rows.length : 0)
						if (gridApi) gridApi.paginationSetPageSize(paginationPageSize)
						setClassName20(paginationPageSize === 20 ? 'paginationPageSizeSelected paginationPageSize rounded' : 'paginationPageSize')
						setClassName50(paginationPageSize === 50 ? 'paginationPageSizeSelected ml-1 paginationPageSize rounded' : 'ml-1 paginationPageSize')
						setClassName100(paginationPageSize === 100 ? 'paginationPageSizeSelected ml-1 paginationPageSize rounded' : 'ml-1 paginationPageSize')
						setClassName200(paginationPageSize === 200 ? 'paginationPageSizeSelected ml-1 paginationPageSize rounded' : 'ml-1 paginationPageSize')
					}
				}
			}
		}
	}, [rows])



	React.useEffect(() => {
		if (autoSelectedAboveGridFilter) {
			setHiddenColumns([])
			setAboveGridFilterList([])
			setTimeout(() => {
				addAutoAboveFilter(autoSelectedAboveGridFilter, true)
			}, 1);
		}
	}, [autoSelectedAboveGridFilter])



	const mergeByGroups = (mergedGroups: any[], field?: string, reverse?: boolean) => {
		if (field) {
			let newMergedGroups = [...mergedGroups, field]
			if (reverse) {
				newMergedGroups = mergedGroups.filter((f: string) => f !== field)
				setMergedGroups(newMergedGroups)
			} else setMergedGroups([...mergedGroups, field])

			// setTimeout(() => {
			// 	gridColumnApi.refreshView()
			// }, 500);  
		}
	};



	const filterRowsResult = (columnResultsPicker: any[]) => {
		let newRowData: any[] = rows
		// let allDisplayedRows = getAllRows()
		columnResultsPicker.forEach((crf: any) => {
			let { group, option } = crf
			newRowData = newRowData.map((row: any) => { if (option === row[group]) { return } else return row }).filter(a => a)
		})
		setRowData(rows && rows.length && fields && fields.length && !noSort ? Sorting(rows, { field: customSortField ? customSortField : fields[defaultSortFieldNum ? defaultSortFieldNum : 0].text, type: customSortType ? customSortType : typeof (rows[0][fields[defaultSortFieldNum ? defaultSortFieldNum : 0].text]), asc: descSort ? false : true, times: 0 }) : rows && rows.length && fields && fields.length ? rows : [])
		setTimeout(() => {
			setSelectedRows(selected)
			if (gridApi) gridApi.setPinnedBottomRowData(TotalRow(fields))
		}, 50)
	}

	function getData(count: number) {
		var rowData = [];
		for (var i = 0; i < count; i++) {
			rowData.push(rows[i]);
		}
		return rowData;
	}


	const setRowDataController = (type: string) => {
		const setSelectedR = () => { setTimeout(() => setSelectedRows(selected), 50) }
		let selectedRows = selected.map((s: any) => s.data ? id ? s.data[id] : s.data['id'] : undefined)

		let selectedB: any
		let notSelectedB: any

		switch (type) {
			case 'selected':
				if (!selectedButton && notSelectedButton) {
					selectedB = true
					notSelectedB = true
				} else if (selectedButton && notSelectedButton) {
					selectedB = !selectedButton
					notSelectedB = true
				} else {
					selectedB = !selectedButton
					notSelectedB = false
				}
				break;
			case 'unselected':
				if (selectedButton && !notSelectedButton) {
					selectedB = true
					notSelectedB = true
				} else if (selectedButton && notSelectedButton) {
					selectedB = true
					notSelectedB = !notSelectedButton
				} else {
					notSelectedB = !notSelectedButton
					selectedB = false
				}
				break;
			default: return
		}

		if (selectedB && notSelectedB || !notSelectedB && !selectedB) {
			setRowData(rows && rows.length && fields && fields.length && !noSort ? Sorting(rows, { field: customSortField ? customSortField : fields[defaultSortFieldNum ? defaultSortFieldNum : 0].text, type: customSortType ? customSortType : typeof (rows[0][fields[defaultSortFieldNum ? defaultSortFieldNum : 0].text]), asc: descSort ? false : true, times: 0 }) : rows && rows.length && fields && fields.length ? rows : [])
		} else if (selectedB && !notSelectedB) {
			setRowData(rows.filter((row: any) => selectedRows.includes(row[id ? id : 'id']))); setSelectedR(); setSelectedState(type)
		} else if (notSelectedB && !selectedB) {
			let unSelectedRows = rows.filter((row: any) => !selectedRows.includes(row[id ? id : 'id']))
			setRowData(unSelectedRows); setSelectedR(); setSelectedState(type);
		}
	};
	const changeRowDataSelected = (api: any, rowData: any[], selected: any[]) => {
		if (changeRowDataOnSelectionChange) {

			api.showLoadingOverlay();
			setTimeout(() => {
				rowData.forEach((row: any) => {
					let rowNode = api.getRowNode(row[id ? id : 'id'])
					if (rowNode) {
						rowNode.setDataValue('Percent_TotalPrice', 0);
						rowNode.setDataValue('Percent_TotalAmount', 0);
						rowNode.setDataValue('Percent_Avg', 0);
						rowNode.setDataValue('Percent_MarketAmount', 0);
					}
				})

				let totalRowsPrice = selected.reduce((acc: number, row: any) => acc + row.data['TotalPrice'], 0)
				let totalRowsAmount = selected.reduce((acc: number, row: any) => acc + row.data['TotalAmount'], 0)
				let totalRowsMarket = selected.reduce((acc: number, row: any) => acc + row.data['TotalMarket'], 0)

				selected.forEach((row: any) => {
					let Percent_TotalPrice: number = row.data['TotalPrice'] > 0 && totalRowsPrice > 0 ? ((row.data['TotalPrice'] / totalRowsPrice) * 100) : 0
					let Percent_TotalAmount: number = row.data['TotalAmount'] && totalRowsAmount ? ((row.data['TotalAmount'] / totalRowsAmount) * 100) : 0
					let Percent_Avg = Percent_TotalPrice && Percent_TotalAmount ? ((Percent_TotalPrice + Percent_TotalAmount) / 2) : 0
					let Percent_MarketAmount: number = row.data['TotalMarket'] && totalRowsMarket ? ((row.data['TotalMarket'] / totalRowsMarket) * 100) : 0

					let rowNode = api.getRowNode(row.data[id ? id : 'id'])
					if (rowNode) {
						rowNode.setDataValue('Percent_TotalPrice', Percent_TotalPrice.toFixed(2));
						rowNode.setDataValue('Percent_TotalAmount', Percent_TotalAmount.toFixed(2));
						rowNode.setDataValue('Percent_Avg', Percent_Avg.toFixed(2));
						rowNode.setDataValue('Percent_MarketAmount', Percent_MarketAmount.toFixed(2));
					}
				})
				api.refreshCells()
				api.hideOverlay()
				setRefreshDataIconBg('bg-gray')
			}, 100);

		}
	}
	const agGridCellClicked = () => {
		console.log('function in ag grid clicked')
	}

	const agGridCheckBoxOnChange = () => {
		console.log('checkbox in ag grid clicked')
	}
	return (
		<>

			<div style={{ width: '100%', height: '100%' }}>
				{true ? //!render
					<>

						{!saveOption && !changeRowDataOnSelectionChange && !displayExcel && !displayColumnDisplayer && !displayPrint && groups.length < 1 ? null
							:
							<div className="width-100-per d-flex justify-content-between align-items-center">
								{Groups}
								{displayExcel || displayColumnDisplayer || displayPrint || changeRowDataOnSelectionChange || saveOption ? (Options) : null}
							</div>
						}
						<div className='d-flex'>
							{freeSearch && (freeSearchDiv)}
							{checkboxFilterBox && (checkboxFilterDiv)}
							{buttonOnClick && (customButtonDiv)}
						</div>


						{checkboxOptions ?
							<div className={'selectedFilter'} >
								<div className={selectedButton ? 'selectedSortDiv selected' : 'selectedSortDiv'}>
									<img src={checkboxCheckedPath} className={'selectionFilter'} onClick={() => { setSelectedButton(!selectedButton); setRowDataController('selected') }} />
								</div>
								<div className={notSelectedButton ? 'selectedSortDiv selected' : 'selectedSortDiv'} >
									<img src={checkboxNotCheckedPath} className={'selectionFilter'} onClick={() => { setNotSelectedButton(!notSelectedButton); setRowDataController('unselected') }} />
								</div>
							</div>
							: null
						}

						{aboveGridFilterList && aboveGridFilterList.length ?
							<div className='d-flex mb-1'>
								{aboveGridFilterList.map((item: any, key: number) => {
									return (
										<AboveGridFilterOption key={key} item={item} removeColumnFromFilter={removeColumnFromFilter} removeSingleFilterFromMulti={removeSingleFilterFromMulti} />
									)
								})}
							</div> : null}

						<div className='catalog height-100-per'>
							<div style={{ width: '100%' }} className='catalogGrid height-100-per'>
								<div
									id="myGrid"
									ref={TableRef as React.RefObject<HTMLDivElement>}
									style={{
										direction: "rtl",
										height: `${gridHeight}`,
										width: '100%',
										fontFamily: 'Assistant'
									}}
									className="ag-theme-alpine"
								>

									<AgGridReact
										headerHeight={headerHeight ? headerHeight : 44}
										scrollbarWidth={20}
										modules={[
											...AllCommunityModules,
											ClientSideRowModelModule,
											RowGroupingModule,
											MenuModule,
											ClipboardModule,
											SetFilterModule,
											MultiFilterModule,
											FiltersToolPanelModule,
											RichSelectModule,
											ColumnsToolPanelModule]}

										aggFuncs={
											{
												'calculation': (params: any) => {
													let selected = params.api.getSelectedRows()
													return aggFunction(selected, params, params.column.colId)
												},
												'mySum': (params: any) => {
													let sum = 0;
													let selectedRows = params.rowNode.allLeafChildren.map((row: any) => { if (row.selected) { return row } }).filter((r: any) => r)
													selectedRows.forEach((row: any) => {
														if (params.colDef.field === 'TotalAmount') sum += Number(row.data['TotalAmount'])
														else if (params.colDef.field === 'TotalPrice') sum += Number(row.data['TotalPrice'])
													})
													return sum
												},
												'mySumFixed': (params: any) => {
													let sum = 0;
													params.values.forEach((value: number) => sum += Number(value));
													let sumFixed = sum.toFixed(2)
													return sumFixed
												},
												'dontDisplayOnMain': () => {
													return ''

												}
											}}
										rowBuffer={rowBufferAmount ? rowBufferAmount : 35}
										pagination={pagination ? true : false}
										paginationPageSize={50}
										getRowNodeId={(data) => data[id ? id : 'ID']}
										suppressDragLeaveHidesColumns={true}
										suppressPaginationPanel={true} //suppress 
										onSelectionChanged={(event: any) => {
											const selected = event.api.getSelectedNodes();
											setSelected(selected)

											setRefreshDataIconBg('bg-turquoise')
											setSaveIconBg('bg-turquoise')
											// window.alert('selection changed, ' + selected.length + ' nodesrows selected');
										}}
										// sendToClipboard={(params: any) => { console.log('params.data', params.data), params.data.trim() }}
										suppressCopyRowsToClipboard={true}
										suppressClipboardPaste={true}
										copyHeadersToClipboard={false}
										autoGroupColumnDef={{ headerName: '', field: ' ', filter: true, minWidth: 200, editable: false, headerCheckboxSelection: checkbox ? true : false, headerCheckboxSelectionFilteredOnly: true, cellRenderer: 'agGroupCellRenderer', cellRendererParams: { checkbox: false } }}
										getRowStyle={(params: any) => {

											if (params.node.rowIndex % 2 === 0) {
												return { background: '#f3f3f5!important' };
											}
										}}
										onCellClicked={(props: any) => props.colDef.cellRendererParams.clickableCells && props.colDef.cellRendererParams.clickableCells.aboveGridFilter && checkIfIsInMultiAboveGridFilter(props, true, props.colDef.cellRendererParams.clickableCells.aboveGridFilter)}
										onRowDoubleClicked={onRowDoubleClick ? (props: any) => { onRowDoubleClick(props.data) } : () => { return null }}
										rowHeight={customRowHeight ? customRowHeight : window.innerHeight * 0.045}
										enableRangeSelection={true}
										animateRows={true}
										suppressMakeColumnVisibleAfterUnGroup={true}
										showOpenedGroup={true}
										suppressAggFuncInHeader={true}
										suppressRowClickSelection={true}
										groupSelectsChildren={true}
										groupSelectsFiltered={true}
										// rowGroupPanelShow={'always'}
										// pivotPanelShow={'always'}
										// pivotMode={!mergedGroups.length}
										// sideBar={'columns'}
										enableRtl={customizer.direction === 'ltr' ? false : true}
										// pinnedTopRowData={createData(1, 'Top')}
										onFilterChanged={() => { if (gridApi) gridApi.setPinnedBottomRowData(TotalRow(fields)) }}
										pinnedBottomRowData={totalRows && totalRows.length ? TotalRow(fields) : []}

										frameworkComponents={{
											agDateInput: CustomDateComponent,
											customLoadingOverlay: CustomLoadingOverlay,
											customNoRowsOverlay: CustomNoRowsOverlay,
											agColumnHeader: !noSortFields ? CustomHeader : (props: any) => {
												return <b className="customHeaderLabel">{props.displayName}</b>
											},
											customPinnedRowRenderer: (props: any) => { return <span style={props.style}>{props.value}</span> },
											"IconRender": IconRender,
											"ButtonRender": ButtonRender,
											"TranslateCell": (props: any) => {
												return <span>{formatMessage({ id: props.value })}</span>
											},
											"MultiLineRender": (props: any) => {
												const { data, renderMultiLine } = props
												// console.log(props.renderMultiLine)

												return (<div className="d-flex align-items-center flex-column">
													{renderMultiLine.map((ele: any) => <span className={`height-1-2-rem ${ele.classNames ? ele.classNames : ''}`}>{data[ele.field]}</span>)}
												</div>)

											},
											"TooltipRender": TooltipRender,
											"ClickableCell": (props: any) => {
												let onClick = props.clickableCells.onClick ? props.clickableCells.onClick : props.clickableCells.aboveGridFilter ? () => { } : agGridCellClicked
												if (props.clickableCells) {
													if (props.clickableCells.byStatment && props.data[props.clickableCells.if] === props.clickableCells.value) {
														return <span className={`cursor-pointer font-size-1-rem width-100-per`} onClick={() => onClick(props.data)} style={props.style}>{props.value}</span>
													} else if (!props.clickableCells.byStatment) {
														if (props.type === 'Date') {
															return <span className='cursor-pointer font-size-1-rem width-100-per' onClick={(e) => { e.preventDefault(); onClick(props) }} style={props.style}>{moment(new Date(props.value)).format('DD.MM.YY')}</span>
														} else {
															return <span className='cursor-pointer font-size-1-rem width-100-per' onClick={(e) => { e.preventDefault(); onClick(props) }} style={props.style}>{props.value}</span>
														}
													} else {
														return <span style={props.style}>{props.value}</span>
													}
												} else {
													return <span style={props.style}>{props.value}</span>
												}
											},
											"CheckBoxCell": (props: any) => {
												let onChange = props.checkBoxCell.onChange ? props.checkBoxCell.onChange : agGridCheckBoxOnChange
												let forceDisable = props.checkBoxCell.forceDisable ? true : false;
												let isDisabled = props.checkBoxCell.isDisabled && props.data[props.checkBoxCell.isDisabled];
												if (props.checkBoxCell) {
													return <div className="d-flex align-items-center mr-3 mb-2 combobox__drop-down_item">
														<div onClick={() => isDisabled || forceDisable ? null : onChange(props)} className={'combobox__drop-down_label'}>
															<span className={`combobox__drop-down_custom-checkbox ${isDisabled ? CheckBoxStatus.disabled : props.value && !forceDisable ? CheckBoxStatus.checked : props.value && forceDisable ? CheckBoxStatus.disabled_checked : CheckBoxStatus.unchecked}`} />
														</div>
													</div>
													// return <div className={"text-center"}>
													// 	<input type="checkbox" checked={props.value} onChange={() => onChange(props)} disabled={props.checkBoxCell.isDisabled && props.data[props.checkBoxCell.isDisabled] ? props.data[props.checkBoxCell.isDisabled] : false} />
													// </div>
												}

											},
											"ColorCell": (props: any) => {
												return (<div style={{ backgroundColor: `${props.value}` }} className='m-auto border-radius-02rem width-65-per height-1-2-rem ml-1'></div>)
											},
											"CenterCell": (props: any) => {
												return (
													<div className="d-flex align-items-center">
														<span className=" m-auto">{props.value} </span>
													</div>
												)
											},
										}}
										loadingOverlayComponent={'customLoadingOverlay'}
										loadingOverlayComponentParams={{
											loadingMessage: 'O',
										}}
										overlayNoRowsTemplate={formatMessage({ id: 'noResults' })}
										// noRowsOverlayComponent={'customNoRowsOverlay'}
										// noRowsOverlayComponentParams={{
										//   noRowsMessageFunc: () => 'Sorry - no rows! at: ' + new Date(),
										// }}

										components={{
											customFloatingFilter: '',

											// customNumberFilter: getNumberFilterComponent(),
										}}

										tooltipShowDelay={1}
										rowSelection={'multiple'}
										rowMultiSelectWithClick={true}
										singleClickEdit={true}
										allowContextMenuWithControlKey={true}

										getContextMenuItems={() => { return ['copy',] }}//'separator',
										defaultColDef={{
											lockPosition: true,
											floatingFilter: floatFilter,
											menuTabs: ['filterMenuTab', 'gibberishMenuTab'],//'filterMenuTab',
											columnsMenuParams: { suppressSyncLayoutWithGrid: true },
											editable: (params: EditableCallbackParams) => {
												if (editFields) {
													let isEditable: boolean = true;
													let fieldName = params.colDef.field;
													let field = editFields.find((ele: any) => ele.field === fieldName)

													if (!field) isEditable = false;
													if (field && field.flag && !params.data[field.flag]) isEditable = false;
													return isEditable
												} else {
													return false
												}
											},
											sortable: true,
											enableValue: true,
											flex: 1,
											filter: true,
											resizable: resizable,
											cellRendererParams: {
												isResizable: resizable,
												suppressCount: true,
												// suppressDoubleClickExpand: true,
												checkbox: true,
											},
											autoHeight: autoHeight
										}
										}

										stopEditingWhenCellsLoseFocus={true}
										onCellEditingStopped={(event: CellEditingStoppedEvent) => {
											setSaveIconBg('bg-turquoise')
											onCellEditingStopped ? onCellEditingStopped(event.data, event.colDef.field, event.newValue, event.oldValue,) : null
										}}
										// getRowHeight={(params: any) => {

										// 	return params.rowHeight 
										// 	console.log(params)
										// }}
										onGridReady={onGridReady}
										rowData={rowData}
									>
										{
											fields.map((field: any, key: number) =>
												field.fields ? (
													<AgGridColumn key={key} groupId={field.category} type={customizer.direction === 'ltr' ? 'leftAligned' : 'leftAligned'}
														headerName={translateHeader ? String(formatMessage({ id: field.category })) : field.category}>
														{field.fields.map((field: any, key: number) => agGridColumn(key, field)
														)}
													</AgGridColumn>
												)
													:
													agGridColumn(key, field)
											)
										}
									</AgGridReact>
									<div className='d-flex flex-direction-row justify-content-between align-items-center mt-05 mb-1'>
										{pagination && (paginationChoseAmount)}
										{pagination && (paginationNextPage)}
									</div>

								</div>
							</div>
						</div>
					</> : null}

			</div>



			{
				showColumnPoup && (
					<ColumnsPopup isOpen={showColumnPoup}
						fields={fields}
						hiddenColumns={hiddenColumns}
						setHiddenColumns={setHiddenColumns}
						toogle={setShowColumnPoup}
						mergedGroups={mergedGroups} />
				)
			}

		</>
	);
};

export default agGrid;

