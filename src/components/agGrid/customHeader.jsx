import React, { useEffect, useState } from 'react';
import { Icon } from '@components';
import { config } from '@src/config'
import classnames from "classnames"

export default (props) => {
  const [ascSort, setAscSort] = useState('inactive');
  const [descSort, setDescSort] = useState('inactive');
  const [noSort, setNoSort] = useState('inactive');
  const [sortDisplay, setSortDisplay] = useState(false)

  const onSortChanged = () => {
    setAscSort(props.column.isSortAscending() ? 'active' : 'inactive');
    setDescSort(props.column.isSortDescending() ? 'active' : 'inactive');
    setNoSort(
      !props.column.isSortAscending() && !props.column.isSortDescending()
        ? 'active'
        : 'inactive'
    );
  };

  const handleSortRequest = (order, event) => {
    onSortRequested(order, event)
  }

  const onSortRequested = (order, event) => {
    props.setSort(order, event.shiftKey);
    setSortDisplay(true);
    // props.api.redrawRows()
  };

  useEffect(() => {
    props.column.addEventListener('sortChanged', onSortChanged);
    onSortChanged();
  }, []);
  const displaySortIcon = () => {
    setSortDisplay(true);
  }
  const hideSortIcon = () => {
    setTimeout(() => {
      setSortDisplay(false)
    }, 3000);
  }
  const renderSortIcon = (noSort, ascSort) => {
    let sortStyle
    if (sortDisplay) sortStyle = 'opacity-1'
    else sortStyle = 'opacity-0'

    let redHeader = false
    if(props.column && props.column.userProvidedColDef&& props.column.userProvidedColDef.cellRendererParams && props.column.userProvidedColDef.cellRendererParams.redHeader){
       redHeader = true
    }

    const columnTitle = (<b className={classnames("customHeaderLabel", {
      'text-red' : redHeader,
      "text-black": !redHeader
    })} >{props.displayName}</b>)

    const sortDiv = (sortType, icon, classN) => {
      const classFinal = `${classN} ${sortStyle}`
      return (
        <div className='headerAndSort d-flex-reverse-row align-items-center' onMouseEnter={displaySortIcon} onClick={(event) => handleSortRequest(sortType, event)} onMouseEnter={displaySortIcon}>
          {columnTitle}
          <Icon className={classFinal} src={'../../../' + config.iconsPath + icon} style={{ height: '0.5rem', width: '0.5rem' }} />
        </div>
      )
    }
    switch (noSort) {
      case 'active':
        return (
          sortDiv('asc', "table/sort-disabled.svg")
        )
      case 'inactive':
        switch (ascSort) {
          case 'active':
            return (
              sortDiv('desc', "table/sort.svg", 'rotate-180')
            )
          case 'inactive':
            return (
              sortDiv('', "table/sort.svg")
            )
        }
    }
  }

  // let shouldColorCell = props.column.colDef.cellRendererParams.coloredColumn


  return (
    <div className={classnames("columnSortTitle d-flex", {
    })} onMouseLeave={hideSortIcon}>
      <div className={classnames("align-self-center headerTitle", {
        "m-auto": props && props.column.userProvidedColDef && props.column.userProvidedColDef.cellRendererParams && props.column.userProvidedColDef.cellRendererParams.centerHeader && props.column.userProvidedColDef.cellRendererParams.centerHeader.centerHeader,
      })}>
        {renderSortIcon(noSort, ascSort)}
      </div>
      {props.column.colId != 'actions' && props.column.colDef.cellRendererParams && !props.column.colDef.cellRendererParams.isResizable &&
        <div className='divider'></div>
      }
    </div>
  );
};