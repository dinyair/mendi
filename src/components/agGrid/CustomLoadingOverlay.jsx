import React from 'react';
import { Spinner } from "reactstrap"

export default (props) => {
    return (
        <div
            className="ag-custom-loading-cell"
            style={{ paddingLeft: '10px', lineHeight: '25px' }}
        >
            <Spinner />
        </div>
    );
};