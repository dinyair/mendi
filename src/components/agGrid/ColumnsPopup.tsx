
import * as React from 'react';
import { Icon } from '@components';
import { config } from '../../config'
import { useIntl, FormattedMessage } from "react-intl"
// import { Spinner } from 'reactstrap'

import {
	Button,
	Modal,
	ModalHeader,
	ModalBody,
	ModalFooter,
  } from "reactstrap"


  interface Props {
    readonly isOpen: boolean;
    readonly fields: any[];
    readonly mergedGroups: any[];
    readonly hiddenColumns: any[];
    readonly setHiddenColumns: (fields:any[]) => void;
    readonly toogle: (b:boolean) => void;
  }

 export const ColumnsPopup: React.FunctionComponent<Readonly<Props>> = (props: Readonly<Props>) => {
    const { formatMessage } = useIntl();
    const [hiddenColumns, setHiddenColumns] = React.useState<any[]>(props.hiddenColumns);	
    const { isOpen, toogle, fields, mergedGroups } = props
  return (
    <Modal
                  isOpen={isOpen}
                  toggle={()=>toogle(false)}
                  className="modal-dialog-centered modal-sm"
                >
                   {/*
                    <ModalHeader toggle={()=>toogle(false)}>
                    </ModalHeader> 
                  */}
                  <ModalBody>
                    <div onClick={()=>toogle(false)}
                        className="cursor-pointer float-right">
                     <Icon src={config.iconsPath+"general/x.svg"} style={{height: '0.5rem', width: '0.5rem'}}/>
                    </div>
                    <div className="mx-1">
                        <div className="pb-05 text-black font-medium-2 text-bold-600">
                          <FormattedMessage id="remove_columns_from_table"/>
                        </div>
                        <div className="text-black font-small-3 text-bold-600">
                          <FormattedMessage id="choose_the_columns_you_are_want_to_remove"/>
                        </div>
                    </div>
                    <div>
                      <div className="pt-1 width-100-per d-flex flex-wrap align-items-center">
                      {fields.map((field: any, fi: number) =>  !field.hide ? field.category ? 
                       (<div className="pb-1 width-100-per justify-content-center d-flex flex-wrap align-items-center">
                        <div className="width-100-per py-1 text-center text-bold-600 font-medium-1">{ field["category"] }</div>
                        {field.fields.map((field: any, ffi: number) => 
                        <div className="pb-1 width-30-per d-flex align-items-center">
                            <input type="checkbox" className="width-075-rem bg-turquoise" 
                            onClick={!hiddenColumns.includes(field.text) ? ()=>  setHiddenColumns([...hiddenColumns, field.text ]) : ()=> setHiddenColumns([...hiddenColumns.filter(hc=> hc!== field.text)])}
                            disabled={mergedGroups[field.text]}
                            checked={!hiddenColumns.includes(field.text)}
                            ></input>
                            <div className="text-bold-600 font-medium-1 ml-05"><FormattedMessage id={field.text}/></div>
                        </div>
                        )}
                        </div>)	
                        :
                         <div className="pb-1 width-30-per d-flex align-items-center">
                           <input type="checkbox" className="width-075-rem bg-turquoise"
                           onClick={!hiddenColumns.includes(field.text) ? ()=>  setHiddenColumns([...hiddenColumns, field.text ]) : ()=> setHiddenColumns([...hiddenColumns.filter(hc=> hc!== field.text)])}
                            disabled={mergedGroups[field.text]}
                            checked={!hiddenColumns.includes(field.text)}
                            ></input> 
                           <div className="text-bold-600 font-medium-1 ml-05"><FormattedMessage id={field.text}/></div>
                        </div>
                       :null )}
                       </div>

                    <div>
                    </div>  
                    </div>
                  </ModalBody>
                 <ModalFooter className="justify-content-end py-2">
                   <Button
                      color="primary"
                     disabled={props.hiddenColumns === hiddenColumns}
                     className="round text-bold-400 d-flex justify-content-center align-items-center width-7-rem cursor-pointer btn-primary"
                     onClick={()=> { props.setHiddenColumns(hiddenColumns); toogle(false)}}
                   >
                     <FormattedMessage id="confirm"/>
                  </Button>
                 </ModalFooter>
    </Modal>
    )
                }