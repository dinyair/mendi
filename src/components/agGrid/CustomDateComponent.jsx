import React, { forwardRef, useEffect, useImperativeHandle, useRef, useState } from 'react';
import moment from 'moment';

export default forwardRef((props, ref) => {
	const [date, setDate] = useState(null);
	const [picker, setPicker] = useState(null);
	const refFlatPickr = useRef();
	const refInput = useRef();

	//*********************************************************************************
	//          LINKING THE UI, THE STATE AND AG-GRID
	//*********************************************************************************

	const onDateChanged = selectedDates => {
		setDate(selectedDates[0]);
		props.onDateChanged();
	};

	useEffect(() => {
		setPicker(
			flatpickr(refFlatPickr.current, {
        className: 'ml-5',
				onChange: onDateChanged,
				dateFormat: 'd/m/Y',
				wrap: false
			})
		);
	}, []);

	useEffect(() => {
		if (picker && picker.calendarContainer) {
			picker.calendarContainer.classList.add('ag-custom-component-popup');
		}
	}, [picker]);

	useEffect(() => {
		//Callback after the state is set. This is where we tell ag-grid that the date has changed so
		//it will proceed with the filtering and we can then expect AG Grid to call us back to getDate
		if (picker) {
			picker.setDate(date);
		}
	}, [date, picker]);

	useImperativeHandle(ref, () => ({
		//*********************************************************************************
		//          METHODS REQUIRED BY AG-GRID
		//*********************************************************************************
		getDate() {
			//ag-grid will call us here when in need to check what the current date value is hold by this
			//component.
			return date;
		},

		setDate(date) {
			//ag-grid will call us here when it needs this component to update the date that it holds.
			setDate(date);
		}

		//*********************************************************************************
		//          AG-GRID OPTIONAL METHODS
		//*********************************************************************************

		// setInputPlaceholder(placeholder) {
		//   if (refInput.current) {
		//     refInput.current.setAttribute('placeholder', placeholder);
		//   }
		// },

		// setInputAriaLabel(label) {
		//   if (refInput.current) {
		//     refInput.current.setAttribute('aria-label', label);
		//   }
		// },
	}));

	return (
			<div
				className="ag-input-wrapper custom-date-filter"
				// role="presentation"
				ref={refFlatPickr}
			>
				<div className='ag-input-field-input ag-text-field-input d-flex justify-content-center border-ag border-radius-3px'>
					<input
						type="text"
						placeholder={date ? `${moment(date).format('DD.MM.YY')}` : ''}
						className="no-outline no-border"
						ref={refInput}
						data-input
						style={{ width: '100%' }}
					/>
				</div>
			</div>
	);
});
