# ag grid documentation 
* * *

> ag grid react documentation: [click here](https://www.ag-grid.com/react-grid)


To display AgGrid, we need to use AgGrid component.

Use **Example:**
```js
import { AgGrid } from "@components";

<AgGrid
    rowBufferAmount={300}
    defaultSortFieldNum={0}
    id={"id"}
    gridHeight={"70vh"}
    floatFilter={false}
    translateHeader
    fields={fields}
    groups={[]}
    totalRows={[]}
    rows={rows}
    checkboxFirstColumn={false}
    cellGreenFields={["SupplierName"]}
    success={"flag_detail"}
    pagination
    resizable
/>;
```
<br>
#####LICENSE KEY: 
Can be found under :
- `src\index.tsx`

```js
import {LicenseManager} from "@ag-grid-enterprise/core";
LicenseManager.setLicenseKey("For_Trialing_ag-Grid_Only-Not_For_Real_Development_Or_Production_Projects-Valid_Until-1_May_2021_[v2]_MTYxOTgyMzYwMDAwMA==a717ffda041154ad580159495341d1fd");

```
<br><br>
# Table of Contents
* * *
<details>
<summary>
<img src="https://www.ag-grid.com/static/icon-api-85972da3c96c14a531e165188e653633.svg" height="40"><br>
<h3  style="color:green;"> Properties</h3>

</summary>

- [Properties](#properties)
  - [rows](#rows)
  - [fields](#fields)
  - [groups](#groups)
  - [totalRows](#totalRows)
  - [gridHeight](#gridHeight)
</details>

<br><br>

<details>
<summary>
<img src="https://www.ag-grid.com/static/icon-information-88ad11a5d6144c17b2eac27e80435e9f.svg" height="40">
<h3  style="color:green;">Optional Properties</h3>
</summary>

- [Optional Properties](#optional-properties)
  - [id](#id)
  - [translateHeader](#translateHeader)
  - [resizable](#resizable)
  - [checkbox](#checkbox)
  - [checkboxFirstColumn](#checkboxFirstColumn)
  - [checkboxOptions](#checkboxOptions)
  - [displayExcel](#displayExcel)
  - [displayColumnDisplayer](#displayColumnDisplayer)
  - [displayPrint](#displayPrint)
  - [aggFunction](#aggFunction)
  - [cellBgRedMatriza](#cellBgRedMatriza)
  - [floatFilter](#floatFilter)
  - [cellGreenFields](#cellGreenFields)
  - [success](#success)
  - [successDark](#successDark)
  - [cellLightBlueFields](#cellLightBlueFields)
  - [successLight](#successLight)
  - [danger](#danger)
  - [cellRedFields](#cellRedFields)
  - [leftBorderField](#leftBorderField)
  - [hideWhenGroup](#hideWhenGroup)
  - [changeRowDataOnSelectionChange](#changeRowDataOnSelectionChange)
  - [saveOption](#saveOption)
  - [onClickSave](#onClickSave)
  - [defaultSortFieldNum](#defaultSortFieldNum)
  - [deletedFlag](#deletedFlag)
  - [descSort](#descSort)
  - [yellowField](#yellowField)
  - [yellowFlag](#yellowFlag)
  - [blueField](#blueField)
  - [blueFlag](#blueFlag)
  - [pagination](#pagination)
  - [rowBufferAmount](#rowBufferAmount)
  - [freeSearch](#freeSearch)
  - [customSortField](#customSortField)
  - [customSortType](#customSortType)
  - [showOther](#showOther)
  - [checkboxFilterBox](#checkboxFilterBox)
  - [colorColumnFlag](#colorColumnFlag)
  - [autoSelectedAboveGridFilter](#autoSelectedAboveGridFilter)
  - [afterAboveFilterRemovedOrAdded](#afterAboveFilterRemovedOrAdded)
  - [buttonOnClick](#buttonOnClick)
  - [displayLoadingScreen](#displayLoadingScreen)
  - [onCellEditingStopped](#onCellEditingStopped)
  - [editFields](#editFields)
  - [onRowDoubleClick](#onRowDoubleClick)
</details>

<br><br>

<details>
<summary>
<img src="https://www.ag-grid.com/static/icon-style-4250f093ef37e4a698cd05d826cb78ef.svg" height="50"><br>
<h3 style="color:green;">Layout & Styling</h3>
</summary>

  - [gridHeight](#gridHeight)
  - [fields.width](#width)
  - [fields.minWidth](#minWidth)
  - [CenterCell](#CenterCell)
  - [resizable](#resizable)
  - [cellBgRedMatriza](#cellBgRedMatriza)
  - [cellGreenFields](#cellGreenFields)
  - [success](#success)
  - [successDark](#successDark)
  - [cellLightBlueFields](#cellLightBlueFields)
  - [successLight](#successLight)
  - [danger](#danger)
  - [cellRedFields](#cellRedFields)
  - [yellowField](#yellowField)
  - [yellowFlag](#yellowFlag)
  - [blueField](#blueField)
  - [blueFlag](#blueFlag)
  - [colorColumnFlag](#colorColumnFlag)
</details>

<br><br>

<details>
<summary>
<img src="https://www.ag-grid.com/static/icon-selection-c719f872b88838ecfe9f8441b6e0924d.svg" height="40">
<h3 style="color:green;">Selection</h3>
</summary>

  - [checkbox](#checkbox)
  - [checkboxFirstColumn](#checkboxFirstColumn)
  - [checkboxOptions](#checkboxOptions)
  - [checkboxFilterBox](#checkboxFilterBox)
</details>

<br><br>


<details>
<summary>
<img src="https://www.ag-grid.com/static/icon-import-export-8a8fb7e055a7bd39e83b254ae88c6b25.svg" height="40">
<h3 style="color:green;">Export/Import</h3>
</summary>

  - [displayExcel](#displayExcel)
</details>

<br><br>


<details>
<summary>
<img src="https://www.ag-grid.com/static/icon-filter-b948e9791687f81f8c406a13a3a1c537.svg" height="40">
<h3  style="color:green;">Sorting & Filtering</h3>
</summary>

  - [floatFilter](#floatFilter)
  - [defaultSortFieldNum](#defaultSortFieldNum)
  - [descSort](#descSort)
  - [pagination](#pagination)
  - [freeSearch](#freeSearch)
  - [customSortField](#customSortField)
  - [customSortType](#customSortType)
  - [showOther](#showOther)
  - [autoSelectedAboveGridFilter](#autoSelectedAboveGridFilter)
  - [afterAboveFilterRemovedOrAdded](#afterAboveFilterRemovedOrAdded)
</details>

<br><br>

<details>
<summary>
<img src="https://www.ag-grid.com/static/icon-edit-cells-984a12db4ac9df88b8b25815d309fee4.svg" height="40">
<h3  style="color:green;">Editing</h3>
</summary>

  - [saveOption](#saveOption)
  - [onClickSave](#onClickSave)
  - [editFields](#editFields)
  - [onCellEditingStopped](#onCellEditingStopped)
</details>

<br><br>

# Properties
* * *
## rows
### Type: `any`
This is the rows property of the agGrid.
It’s an array of objects with data that integrates with the fields property.
Each row must have all the properties that the fields array has.
<br><br>


## fields
### Type: `any`
Should be an array of objects that represents the agGrid fields.
This property needs to be integrated with the rows property.

**Example:**
```js
[
  {
    text: "BranchName",
    type: "string",
    width: window.innerWidth * 0.03,
    minWidth: 50,
  },
  {
    text: "SupplierName",
    type: "string",
    width: window.innerWidth * 0.03,
    minWidth: 50,
    cellRenderer: "ClickableCell",
    clickableCells: {
      byStatment: true,
      if: "flag_detail",
      value: true,
      onClick: (data: any) => {},
    },
  },
  {
    text: "Wensell",
    type: "string",
    width: window.innerWidth * 0.03,
    minWidth: 50,
  },
  {
    text: "actions",
    type: "string",
    width: window.innerWidth * 0.001,
    minWidth: 1,
    noFilter: true,
    cellRenderer: "IconRender",
    renderIcons: [
      { actions: true },
      {
        type: "edit",
        title: "Edit",
        onClick: (data: any) => {},
        icon: "edit.svg",
      },
      {
        type: "delete",
        title: "Delete",
        disabled: "flag_detail",
        returnDataWithProps: true,
        onClick: (data: any) => {},
        icon: "trash.svg",
      },
    ],
  },
];
```
<br><br>

### <u>Each object on the `fields` property must contain these properties:</u>

### text `string`
The header of the field. If `translateHeader` property set to `true`, the title should be stored in the translation files.
The value under this key must be a property in each object under the rows 

### type `string`
#### Options:
Option | Description
------ | -------------
`percentage` | will add `%` mark after the `value`
`fullNumber` | will convert the number `toLocaleString()`
`number` | if no value return `''` else return `value`
`numberDisplayZero` | if no value return `0` else return `value`
`martizaNumber` | if no value return `0` else return `value`
`fixedNumber` | if no value return `null` else return `value`
`fixedAndLocaleNumber` | if no `value` return `null`, else return the `number` to `localeString()` with 2 fraction digits.
`Date` | return a `formatted date (DD.MM.YY)` using `moment` package
`DateTime` | return a `formatted date with hour (DD.MM.YY hh:mm)` using `moment` package
`color` | return `''`
`null` | return `'other'`


### width 
#### type: `number`
Set the width of the field column
<br><br>

### minWidth
#### type: `number`
Set the minimum width of the field column. 
This minimum width will affect when the user will resize the columns, (only if `resizable` was set to true).
<br><br>
### <u>Each object on the `fields` property can contain these properties (optional properties):</u>

### cellRenderer `string`   
Can be one of these options: 
Option | Renderer
------ | --------
**TooltipRender** | <a href="#customToolTip">customToolTip</a>
**IconRender** | <a href="#renderIcons">renderIcons</a>
**ClickableCell** | <a href="#clickableCells">clickableCells</a>
**CheckBoxCell** | <a href="#checkBoxCell">checkBoxCell</a>
**CenterCell** | <a href="#CenterCell">CenterCell</a>


### customToolTip 
A parameter under the field property, you must declare this parameter if cellRenderer is set to `TooltipRender`. If the condition inside has the value the `customToolTipTitle` will show up. 

**Example:**
```js
customToolTip:
[
 {
  if: 'flag_green',
  value: '1',
  customTooltipTitle: formatMessage({ id: 'inventoryCountDone' })
 }
]
```

Property Name | Description
------------- | -----------
if `string` | The name of the key in the rows, that has a value.
value `string|number` | The value that is supposed to be in the key that is declared in the if property.
customToolTipTitle `string` | The title that the tooltip should show if the the key has the correct value.
<br><br>

### renderIcons 
A parameter under the field property, you must declare this parameter if cellRenderer is set to `IconRender`. This parameter should be an array of objects. 
The first object is a declaration about the render type:
```js
    { actions: true }
```
OR:
```js
    { status: true }
```

If status is set to `true`, the other objects should contain: `type` parameter, `title` parameter and `icon` parameter.
```js
    { type: 'done', title: 'done', icon: 'view.svg' }
```

If actions is set to `true`, the other objects should contain a: `type` parameter, `title` parameter, `onClick` parameter and `icon` parameter.
```js
    { type: 'view', title: 'View', onClick: (data: any) => handleActionClick('view', data), icon: 'view.svg' }
```

**Example:**
```js
cellRenderer: 'IconRender', renderIcons: [
 { status: true },
 { type: 'done', title: 'done', icon: 'view.svg' },
 { type: 'notDone', title: 'not done', icon: 'print.svg' },
 { type: 'deleted', title: 'deleted', icon: 'trash.svg' },
 { type: 'draft', title: 'draft', icon: 'trash.svg' },
]
```

```js
cellRenderer: 'IconRender', renderIcons: [
 { actions: true },
 { type: 'view', title: 'View', onClick: (data: any) => handleActionClick('view', data), icon: 'view.svg' },
 { type: 'print', title: 'Print', onClick: (data: any) => handleActionClick('print', data), icon: 'print.svg' },
    { type: 'delete', title: 'Delete', onClick: (data: any) => handleActionClick('delete', data), icon: 'trash.svg' },
]
```
<br><br>

### clickableCells 
A parameter under the field property, you must declare this parameter if cellRenderer is set to `ClickableCell`.
This parameter is an object that should contain:

Property Name | Description
------------- | -----------
**byStatement** `boolean` | If set to `true`, the `clickableCell` will be clickable only if the if parameter and the value parameter are declared.
**if** `string` | The name of the key in the rows, that has the value `true` or `false`.
**value** `any` | The condition for the `clickableCell` to be clickable if `byStatement` was set to `true`.
**aboveGridFilter** | If set to `true`, the `clickableCell` will filter the grid by field value.
**customAboveGridFilter** | If `aboveGridFilter` set to `true`, and you need custom filter for this field, use `customAboveGridFilter` to get a callBack with data. 
**onClick** `function` | A callBack function that is called when the `clickableCell` has a click event.


<br><br>

### checkBoxCell
A parameter under the field property, you must declare this parameter if cellRenderer is set to `CheckBoxCell`.
This parameter is an object that should contain:

Property Name  | Description
-------------  | -----------
**value** `any`| The name of the key in the rows, that has the value `true` or `false`.
**idDisabled** `boolean` | The name of the key in the rows, that has the value `true` or `false`. If `isDisabled` is set to `true`, the CheckBox will be disabled.
**onChange** `function`   | a callBack function that is called when the checkBox has a change event.


**Example:** 
```js
{
 value: 'Sunday', isDisabled: 'SundayDisabled', 
    onChange: (data: any) =>handleCheckBoxChange(data)
}
```
<br><br>


### CenterCell 
A parameter under the field property, if the cellRenderer parameter is to `CenterCell`, the text in this field will be centered.
 

**Example:**
```js
cellRenderer: 'CenterCell', 
```

```js
			{ text: "Wensell", type: 'string', width: window.innerWidth * 0.03, minWidth: 50, cellRenderer: 'CenterCell'},
```


Used in: 
- `src\containers\branch_planning\supply_days\index.tsx`

<br><br>

### centerHeader 
#### type: `boolean`
If this parameter is set to `true`, it’ll add a margin auto class to the field header in the agGrid, to center the header text.
<br><br>

### <u>End of `fields` properties.</u>

<br><br>
## groups
#### Type: `any[]`

**Example:**
```js
<AgGrid 
    groups={['Supplier', 'Segment', 'Brand', 'Series', 'SubgroupName']}
/>
```       

Used in: 
- `src\planogram\pages\manage\planogram\main\DataReports\index.tsx`
<br><br>

## totalRows
#### Type: `any`
Send an array with field names that exists under `fields` property to add last row with total amount of each column.
**Example:**
```js
<AgGrid 
    totalRows={['Period1', 'Period2', 'Period3','Average']}
/>
```
<br><br>

## gridHeight
#### Type: `string`
The height of the agGrid table is set by this parameter.
**Example:**
```js
<AgGrid 
    gridHeight={"62vh"} 
/>
```
<br><br>


# Optional Properties
* * *
## id
#### Type: `string`
Pass this property to set the id of the agGrid.
<br><br>



## translateHeader
#### Type: `boolean`
If the property is declared or set to true, it’ll translate headers by field names.

**Example:**
```js
<AgGrid 
    translateHeader 
/>
```
<br><br>




## resizable
#### Type: `boolean`
If the property is declared or set to true, it’ll allow the user to rezise the columns width.
Each field object on the `fields` property must have `width` and `minWidth`.
To make sure that the text is aligned right/left, you need to pass this parameter.

**Example:**
```js
<AgGrid 
    resizable 
/>
```
<br><br>



## checkbox
#### Type: `boolean`
If the property is declared or set to true, it’ll add a checkbox in the first column for each row.

**Example:**
```js
<AgGrid 
    checkbox 
/>
```

Used in:
- `src\pages\manage\planogram\main\DataReports\index.tsx`
<br><br>



## checkboxFirstColumn
#### Type: `boolean`
If the property is declared or set to true, it’ll add a checkbox in the first header.

**Example:**
```js
<AgGrid 
    checkboxFirstColumn
/>
```

Used in:
- `src\pages\manage\planogram\main\DataReports\index.tsx`
<br><br>



## checkboxOptions
#### Type: `boolean`
If the property is declared or set to `true`, it’ll add two checkboxes to each row.

**Example:**
```js
<AgGrid 
    checkboxOptions 
/>
```

Used in:
- `src\pages\manage\planogram\main\DataReports\index.tsx`
<br><br>



## displayExcel
#### Type: `boolean`
If the property is declared or set to `true`, it’ll add an excel button on top of the agGrid. When clicked - it’ll download an excel file with the current rows data.

**Example:**
```js
<AgGrid 
    displayExcel 
/>
```
<br><br>



## displayColumnDisplayer
#### Type: `boolean`
If the property is declared or set to `true`, it’ll add a button on top of the agGrid. When clicked - it’ll show a modal to hide columns from the agGrid.

**Example:**
```js
<AgGrid 
    displayColumnDisplayer 
/>
```

Used in:
- `src\pages\manage\planogram\main\DataReports\index.tsx`
<br><br>



## displayPrint
#### Type: `boolean`
If the property is declared or set to `true`, it’ll add a print button on top of the agGrid. When clicked - it’ll print the content of the agGrid - still in development.

**Example:**
```js
<AgGrid 
    displayPrint
/>
```

```js
<AgGrid 
    displayPrint={true}
/>
```

Used in:
- `src\pages\manage\planogram\main\DataReports\index.tsx`
<br><br>



## aggFunction
#### Type: `any`
Pass this function to agGrid to do custom calculations using `aggFunction`.


**Example:**
```js
    aggFunction={(rows: any[], params: any, field: string) => {
			return fields.filter((f: any) => f.text === field)[0].aggFunction(rows, params)
	}}
```


Used in:
- `src\planogram\DataReports\index.tsx`
<br><br>




## cellBgRedMatriza
#### Type: `boolean`
If the property is declared or set to `true`, it’ll add a yellow background color to every row field that has the undefined value.

**Example:**
```js
<AgGrid 
    cellBgRedMatriza
/>
```

```js
<AgGrid 
    cellBgRedMatriza={true}
/>
```

Used in:
- `src\containers\reports\item-trends\index.tsx`
<br><br>



## floatFilter
#### Type: `boolean`
If the property is declared or set to `true`, it’ll add a row after the agGrid table header, that row includes a filter input to filter inside the column data.

**Example:**
```js
<AgGrid 
    floatFilter
/>
```
<br><br>



## cellGreenFields
#### Type: `any`
Should be an array of field names that you need to change color of the field text by a boolean flag that passed in the `success` or `successDark` properties.
If the flag is set to `true`, The text will be colored in green.

**Example:**
```js
<AgGrid
  cellGreenFields={["barcode", "branch", "supplier", "OrderDate", "OrderNum"]}
/>
```

Used in:
- `src\containers\branch_planning\supply_days\index.tsx`
- `src\containers\reports\item-trends\index.tsx`
- `src\containers\reports\order-analyza\Item.tsx`
- `src\containers\reports\order-analyza\Order.tsx`
<br><br>



## success
#### Type: `string`
Should pass the name of a boolean flag field to check whether to color the field text or not.
If passed `all`, the whole row text field should be colored.

This will add `.success` class to the text.
```css
.success {
  color: #6ec482 !important;
  font-weight: bold !important;
}
```
Integrates with `cellGreenFields` property.

**Example:**
```js
<AgGrid 
    success={"all"}
    cellGreenFields={["barcode", "branch", "supplier", "OrderDate", "OrderNum"]}
/>
```



## successDark
#### Type: `string`
Should pass the name of a boolean flag field to check whether to color the field text or not.
If passed `all`, the whole row text field should be colored.

This will add `.success-dark` class to the text.
```css
.success-dark {
  color: #027b7a !important;
  font-weight: bold !important;
}
```
Integrates with `cellGreenFields` property.

**Example:**
```js
<AgGrid 
    successDark={"all"}
    cellGreenFields={["barcode", "branch", "supplier", "OrderDate", "OrderNum"]}
/>
```

Used in:
- `src\containers\branch_planning\supply_days\index.tsx`
- `src\containers\reports\item-trends\index.tsx`
- `src\containers\reports\order-analyza\Item.tsx`
- `src\containers\reports\order-analyza\Order.tsx`


<br><br>

## cellLightBlueFields
#### Type: `any`
Should be an array of field names that you need to change color of the field text by a boolean flag that passed in the `successLight` property.
If the flag is set to `true`, The text will be colored in green.

**Example:**
```js
<AgGrid
	cellLightBlueFields={['WeeklyAverageSales']}
    successLight={'all'}
/>
```

Used in:
- `src\containers\invertory\balances\index.tsx`
<br><br>



## successLight
#### Type: `string`
Should pass the name of a boolean flag field to check whether to color the field text or not.
If passed `all`, the whole row text field should be colored.

This will add `.success-light` class to the text.
```css
.success-light {
  color:   #4a4ff9e0!important;
  font-weight: bold !important;
}
```
Integrates with `cellLightBlueFields` property.

**Example:**
```js
<AgGrid 
	cellLightBlueFields={['WeeklyAverageSales']}
    successLight={'all'}
/>
```

Used in:
- `src\containers\invertory\balances\index.tsx`




<br><br>


## danger
#### Type: `string`
Should pass the name of a boolean flag field to check whether to color the field text or not.
This will add `.danger` class to the text.
```css
.danger {
  color: #d03639 !important;
  font-weight: bold !important;
}
```
Integrates with `cellRedFields` property.

**Example:**
```js
<AgGrid 
    danger={"flag"} 
    cellRedFields={["transfer"]} 
/>
```

Used in:
- `src\containers\reports\item-trends\index.tsx`
<br><br>


## cellRedFields
#### Type: `any`
Should be an array of field names that you need to change color of the field text by a boolean flag that passed in the `success` property.
If the flag is set to `true`, The text will be colored in red.

**Example:**
```js
<AgGrid 
    cellRedFields={["transfer"]} 
/>
```

Used in:
- `src\containers\reports\item-trends\index.tsx`
<br><br>



## leftBorderField
#### Type: `string`
Pass the field name, add a custom field at the beginning of the agGrid.
Must be a field in the Fields property.

**Example:**
```js
<AgGrid 
    leftBorderField={"choose"} 
/>
```

Used in:
- `src\pages\manage\planogram\main\DataReports\index.tsx`
<br><br>



## hideWhenGroup
#### Type: `string`
Pass the field name to hide the percentage symbol in the selected field if the field type is `percentage`.

**Example:**
```js
<AgGrid 
    hideWhenGroup={"Percent_Profit"} 
/>
```

Used in:
- `src\pages\manage\planogram\main\DataReports\index.tsx`
<br><br>



## changeRowDataOnSelectionChange
#### Type: `any`
If the property is declared or set to `true`, it’ll add a refresh icon on top of the agGrid. When the user selects/unselects rows, the refresh icon will change color and after clicking on the button it will refresh the rows on agGrid.

**Example:**
```js
<AgGrid 
    changeRowDataOnSelectionChange 
/>
```

Used in:
- `src\pages\manage\planogram\main\DataReports\index.tsx`
<br><br>



## saveOption
#### Type: `boolean`
If the property is declared or set to `true`, it’ll add a save icon on top of the agGrid. When clicked - it’ll call `saveGrid()` function.

**Example:**
```js
<AgGrid 
    saveOption 
/>
```

Used in:
- `src\pages\manage\planogram\main\DataReports\index.tsx`
<br><br>




## onClickSave
#### Type: `(rows: any[]) => void`
If `saveOption` is declared and `onClickSave` is declared,
`onClickSave` will call an event in the father component, when `saveGrid()` function is called.

**Example:**
```js
<AgGrid
  onClickSave={async (rows: any[]) => {
    let newPlano: any = await getNewPlano({
      updatedb: 2,
      aisle_id: currentAisleIndex,
      newplano: rows,
    });
    if (newPlano) {
      newPlano.data = rows;
      setNewPlano(newPlano);
    }
  }}
/>
```

Used in:
- `src\pages\manage\planogram\main\DataReports\index.tsx`
<br><br>



## defaultSortFieldNum 
#### Type: `number`
If the property is declared with a number the agGrid will load and sort by default the selected field number (0 for first).

**Example:**
```js
<AgGrid
    defaultSortFieldNum={1}
/>
```
Used in:
- `src\containers\reports\item-trends\index.tsx`
<br><br>



## deletedFlag
#### Type: `any`
If the property is declared with a flag name in the row data and the flag value in the row is set to `true`, it will add the `deleted` class from css to the row.

**Example:**
```js
<AgGrid
    deletedFlag={'isDeleted'}
/>
```

Used in:
- `src\containers\settings\users\Users\Users.tsx`
- `src\containers\stock\orders\prev-orders.tsx`
<br><br>



## descSort
#### Type: `boolean`
If the property is declared or set to true, the agGrid will sort the data by desc order.

**Example:**
```js
<AgGrid 
    descSort 
/>
```

Used in:
- `src\containers\settings\users\Users\Users.tsx`
- `src\containers\stock\orders\prev-orders.tsx`
<br><br>



## yellowField
#### Type: `any`
Should be a field name that you need to change color of the field text by a `boolean` flag that passed in the `yellowFlag` property.
If the flag is set to `true`, The text will be colored in yellow.

**Example:**
```js
<AgGrid 
    yellowField={'sale'}
/>
```

Used in:
- `src\containers\reports\item-trends\index.tsx`
<br><br>



## yellowFlag
#### Type: `any`
Should pass the name of a `boolean` flag field to check whether to color the field text or not.
Integrates with `yellowField` property.

**Example:**
```js
<AgGrid 
    yellowFlag={'virt_sale'}
/>
```

Used in:
- `src\containers\reports\item-trends\index.tsx`
<br><br>



## blueField
#### Type: `any`
Should be a field name that you need to change color of the field text by a boolean flag that passed in the `blueFlag` property.
If the flag is set to `true`, The text will be colored in blue.

**Example:**
```js
<AgGrid 
    blueField={'sale'}
/>
```

Used in:
- `src\containers\reports\item-trends\index.tsx`
<br><br>



## blueFlag

#### Type: `any`
Should pass the name of a `boolean` flag field to check whether to color the field text or not.
Integrates with `blueField` property.

**Example:**
```js
<AgGrid 
    blueFlag={'flag2'}
/>
```

Used in:
- `src\containers\reports\item-trends\index.tsx`
<br><br>


## pagination
#### Type: `boolean`
If the property is declared or set to `true`, it’ll add a pagination bar under the agGrid, that will allow it to paginate the data.

**Example:**
```js
<AgGrid 
    pagination 
/>
```
<br><br>


## rowBufferAmount
#### Type: `number`
The number of rows rendered outside the viewable area the grid renders. Having a buffer means the grid will have rows ready to show as the user slowly scrolls vertically.

If the property is declared with a value, it’ll set the row buffer to the exact value. Otherwise, it’ll set the row buffer amount to 35 rows.


**Example:**
```js
<AgGrid 
    rowBufferAmount={100} 
/>
```
<br><br>


## freeSearch
#### Type: `freeSearchInterface`
If the property is declared correct, it’ll add free search input on top of the agGrid, to search data inside the agGrid rows.

**Example:**
```js
<AgGrid 
    freeSearch={{
        rows: subRows,
        fieldsToSearch: ['BarCode', 'BarCodeName'],
        title: formatMessage({ id: 'input_placeholder' })
    }}
/>
```

Used in:
- `src\containers\settings\users\Users\Users.tsx`
- `src\containers\stock\orders\prev-orders.tsxx`
<br><br>



## customSortField
#### Type: `any`
Should be a field name that you want to sort by this field as default on agGrid loading.

**Example:**
```js
<AgGrid 
    customSortField={'isSelected'} 
/>
```

Used in:
- `src\pages\manage\planogram\main\DataReports\index.tsx`
<br><br>



## customSortType
#### Type: `any`
Should be the field type that you want to sort by as default on agGrid loading.
Integrates with `customSortField` property.

**Example:**
```js
<AgGrid 
    customSortType={'boolean'} 
/>
```

Used in:
- `src\pages\manage\planogram\main\DataReports\index.tsx`
<br><br>



## showOther
#### Type: `any`
If the property is declared or set to true, it’ll allow you to add the `other` / `אחר` word to the agGrid fields.

**Example:**
```js
<AgGrid 
    showOther  
/>
```

Used in:
- `src\pages\manage\planogram\navbar\ColorReport\ColorReport.tsx`
<br><br>



## checkboxFilterBox
#### Type: `checkboxFilterInterface`
It’ll add a filter box with checkboxes on top of the agGrid to add the option to filter the rows by term or a value.

**Example:**
```js
<AgGrid 
    checkboxFilterBox={{
 rows: rows, 
    title: formatMessage({ id: 'userType' }),
 options: Array.from(new Set(rows.map((row: any) => row.role ? row.role : formatMessage({ id: 'other' })))),
 flag: 'role'
}}  
/>
```

Used in:
- `src\containers\settings\users\Users\Users.tsx`
<br><br>


## colorColumnFlag
#### Type: `string`
Should be the name of the flag that controls the background color of the field every row.
If that flag has a `true` value, the field background color will be set to blue.

**Example:**
```js
<AgGrid 
    colorColumnFlag={"coloredColumn"} 
/>
```

Used in:
- `src\components\import-excel-modal\import-excel-modal.tsx`
<br><br>


## autoSelectedAboveGridFilter
#### Type: any
Send this property to agGrid component to add filters above the grid when the agGrid loads first time on the page.
Used in:
- `src\containers\reports\order-analyza\Item.tsx`
- `src\containers\reports\order-analyza\Order.tsx`
<br><br>


## afterAboveFilterRemovedOrAdded
#### Type: any
????
Used in:
- `src\containers\reports\order-analyza\Item.tsx`
- `src\containers\reports\order-analyza\Order.tsx`
<br><br>


## buttonOnClick
#### Type: buttonOnClickInterface
Pass this object to add a button above the agGrid.
This object should contain 3 parameters:
title - `string`      | disabled - `boolean`            | function - `() => void`
--------------------- | ------------------------------- | --------------------
set the button text   | set the button disabled/enabled | set the callback function on onClick event


**Example:**
```js
<AgGrid 
    buttonOnClick={{
            title: 'user', 
            function: () => { setSelectedUser(null); setShowDrawer(true); }, 
            disabled: userBranches && userBranches.length && roles && roles.length ? false : true 
            }}
/>
```		

Used in:
- `src\containers\settings\users\Users\Users.tsx`
<br><br>

## displayLoadingScreen
#### Type: `boolean`
Send this parameter with boolean value to control when to show loading spinner in the center of the table.
**Example:**
```js
<AgGrid 
    displayLoadingScreen={isLoadingData ? true : false}
/>
```
<br><br>					


## onCellEditingStopped
#### Type: any
Pass a function that returns this data to the father component that loads the agGrid component.
This function will call when the cell had stop edit event.
Parameter | description
------ | -------------
data   | original row data
field  | the field name that has been edited
newValue | the new value that has been updated
oldValue | the old value of the field
<br><br>


## editFields
#### Type: any
Pass this paramter to `agGrid`, to allow edit column.
Edit column for each row will be available if the array includes an object with parameters:
field | flag (optional)
------ | -------------
set the field name to edit | set the flag for allow/disallow editing

This parameters should appear in each row under `rows` property.

**Example:**
```js
<AgGrid
	editFields={[{ field: 'ManualIronInvertory', flag: 'ManualIronInvertoryId' }]}
/>
```
<br><br>



## onRowDoubleClick
#### Type: any
Pass this function paramter to `agGrid`, to receive an `event` with column data when user double click on column.

**Example:**
```js
<AgGrid 
    onRowDoubleClick={(data:any)=> {
            console.log(data)
        }}
/>
```
<br><br>