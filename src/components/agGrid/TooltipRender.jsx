import * as React from 'react';
import { Tooltip } from '@material-ui/core';
import { useIntl } from "react-intl"
import classnames from "classnames"

export default props => {
    let { customTooltipTitle, customToolTip, data, field, type } = props
    const moment = require('moment');

    const getValue = (value) => {
        if (type === 'percentage') return value + '%'
        else if (type === 'fullNumber') {
            if (!value) return ''
            if (value) return Number(value).toLocaleString()
            else return Number(value).toLocaleString()
        }
        else if (type === 'number') {
            if (!value) return ''
            return Number(value).toLocaleString()
        }
        else if (type === 'fixedNumber') {
            if (!value) return ''
            return Number(value).toFixed(2)
        }
        else if (type === 'Date') return moment(new Date(value)).format('DD.MM.YY');
        else if (value === 'null') return formatMessage({ id: 'other' })
        else if (type === 'fixedAndLocaleNumber') {
            if (!value) return null
            else return value.toLocaleString(undefined, {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            })
        }
        else return value
    }
    if (customToolTip && customToolTip[0]) {
        customToolTip.forEach(row => {
            if (data[String(row.if)] === Number(row.value)) {
                customTooltipTitle = row.customTooltipTitle
            }
        })
    }

    return (
        <div className="d-flex justify-content-end width-100-per">
            <Tooltip placement="top" title={customTooltipTitle ? customTooltipTitle : ''}>
                <div className="toolTipCell">{getValue(data[field])}</div>
            </Tooltip>
        </div>
    );
}

