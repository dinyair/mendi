import * as React from 'react';
import { Tooltip } from '@material-ui/core';
import { useIntl } from "react-intl"
import classnames from "classnames"
import { config } from '@src/config'
import { Icon } from '@components';

export default props => {
    let { renderIcons, data } = props
    const { formatMessage } = useIntl();
    let actionIcons = []
    if (renderIcons[0] && renderIcons[0].actions) {
        actionIcons = renderIcons.map((icon) => {
            if( icon ) {
            if (icon.actions || (icon.disabled && !data[icon.disabled])) {
                return (<div className='mr-05 cursor-pointer min-width-2-rem max-width-2-rem'></div>)
            }
            else if (data.isDeleted && icon && icon.type === 'view' && !icon.isHidden) {
                let imagePath = `${config.iconsPath}/planogram/${icon.icon}`
                return (
                    <Tooltip placement="top-start" title={formatMessage({ id: icon.title })}>
                        <div onClick={icon.onClick ? () => icon.onClick(props.data) : null}> <Icon src={imagePath}
                            className="mr-05 cursor-pointer min-width-2-rem max-width-2-rem" style={{ height: '0.5rem', width: '0.2rem' }} /> </div>
                    </Tooltip>
                )
            }
            else {
                let orderDate = new Date(props.data.OrderDate).setHours(0, 0, 0, 0)
                let today = new Date().setHours(0, 0, 0, 0)
                if (icon.type === 'delete' && today > orderDate) {
                    return <div className='mr-05 cursor-pointer min-width-2-rem max-width-2-rem'></div>
                } else {
                    let imagePath = `${config.iconsPath}/planogram/${icon.icon}`
                    return (
                        <Tooltip placement="top-start" title={formatMessage({ id: icon.title })}>
                            <div className={'d-flex align-items-center justify-content-center'} onClick={icon.onClick && icon.returnDataWithProps ? () => icon.onClick(props) : icon.onClick ? () => icon.onClick(props.data) : null}>
                            {icon.value ? data[icon.value] : ''}
                            <Icon src={imagePath}
                                    className="mr-05 cursor-pointer min-width-2-rem max-width-2-rem" style={{ height: '0.5rem', width: '0.2rem' }} />
                            </div>

                        </Tooltip>
                    )
                }
            }
            }
        })
    }
    let status = {}
    if (renderIcons[0] && renderIcons[0].status) {
        if (renderIcons[0] && renderIcons[0].status) {
            if (data.status && data.status['draft']) status['draft'] = data.status['draft']
            if (data.status && data.status['done']) status['done'] = data.status['done']
            if (data.status && data.status['deleted']) status['deleted'] = data.status['deleted']
            if (data.status && data.status['notdone']) status['notdone'] = data.status['notdone']
        }
    }

    if (data) {
        if (data['Blocked']) {
            renderIcons = {};
            renderIcons['Blocked'] = '#31baab'
        } else if (data['NewBarCode']) {
            renderIcons = {};
            renderIcons['NewBarCode'] = '#ff705d'
        } else if (data['LowSale']) {
            renderIcons = {};
            renderIcons['LowSale'] = '#1a56b0'
        }
    }
    return (
        <div className="d-flex width-100-per">
            {renderIcons[0] && renderIcons[0].actions ?
                <div className='m-auto'>
                    <div className='mr-2 d-flex zindex-2 align-items-center'>{actionIcons}</div>
                </div>
                : null}

            {renderIcons && Object.keys(renderIcons).length ?
                Object.keys(renderIcons).map((key) => {
                    return (
                        <div  className={classnames('', { 'opacity-0 position-absolute': !data || !data[key] })}>
                            {/* <Tooltip className={classnames('', { 'opacity-0 position-absolute': !data || !data[key] })} placement="top" title={data && data[key] ? formatMessage({ id: key }) : ''}> */}
                                <div className="cursor-pointer oval rounded mr-2" style={{ backgroundColor: renderIcons[key] }} />
                            {/* </Tooltip> */}
                        </div>
                    )
                })
                : null
            }
            {renderIcons[0] && renderIcons[0].status && Object.keys(status).length ?
                Object.keys(status).map((key) => {
                    return (
                        <div className='m-auto'>
                            <div className={classnames('d-flex align-items-center', { 'opacity-0 position-absolute': !data })}>
                                <div className="cursor-pointer oval rounded mr-05" style={{ backgroundColor: status[key] }} />
                                <div className='font-medium-1 mr-05' >{key ? formatMessage({ id: key }) : 'err'}</div>
                            </div>
                        </div>
                    )
                })
                : null
            }
            {/* { renderIcons[0] && renderIcons[0].status && <div className='d-flex zindex-2 align-items-center'>{status}</div>} */}
        </div>
    );
}
