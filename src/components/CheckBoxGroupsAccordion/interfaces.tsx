
export interface CheckBoxGroupsOptions {
    Id: number;
    Name: string;
    SubOptions: CheckBoxSubOption[];
    openAccordion?:boolean;
    Status?: CheckBoxStatus;
}

export interface CheckBoxSubOption {
    Id: number;
    Name: string;
    Status: CheckBoxStatus;
}

export enum CheckBoxStatus {
    checked = 'checked',
    semi_checked = 'semi-checked',
    unchecked = 'unchecked',
    disabled = 'disabled',
    disabled_checked = 'disabled-checked',
    half_checked = 'half-checked'
}

