
import * as React from 'react';
import { Accordion } from "@src/components/accordion";

import '@components/combo-box/combo-box.scss';
import InputText from '../input-text';
import { CheckBoxGroupsOptions, CheckBoxStatus, CheckBoxSubOption } from './interfaces';

interface Props {
    options: CheckBoxGroupsOptions[];
    returnSelectedData: (selectedOptions: CheckBoxGroupsOptions[]) => void;
    titleLabel: string;
    searchBarPlaceHolder: string;
    allowSelectMulti?: boolean;
    allowSemiCheck?: boolean;
    handleCheckBoxEvent?: (subOption: CheckBoxSubOption, optionIndex: number, subOptionIndex: number) => void
    handleStateChangesFromFather?: boolean;
    backButton?: any;
}


export const CheckBoxesGroupsAccordion: React.FunctionComponent<Readonly<Props>> = (props: Readonly<Props>) => {
    const { options,
        returnSelectedData, titleLabel, searchBarPlaceHolder, allowSelectMulti, allowSemiCheck, handleCheckBoxEvent, handleStateChangesFromFather, backButton } = props
    const [filtered, setFiltered] = React.useState<any>([])
    const [filter, setFilter] = React.useState<string>('');
    const freeSearchInputRef = React.useRef<any>();
    const [selectedOptions, setSelectedOptions] = React.useState<CheckBoxGroupsOptions[]>(options);

    const searchContainer = (value: any, onChange: Function, placeholder: string, className?: string) => {
        return (
            <div className={`${className ? className : null}`}>
                <InputText
                    searchIcon
                    id={placeholder}
                    placeholder={placeholder}
                    onChange={() => onChange(freeSearchInputRef.current.value)}
                    inputRef={freeSearchInputRef}
                />
            </div>
        );
    };



    const handleSubOptionsClicks = (subOption: CheckBoxSubOption, subOptionIndex: number, optionIndex: number) => {
        if (!allowSemiCheck) {
            if (subOption.Status === CheckBoxStatus.unchecked) {
                if (!handleStateChangesFromFather) {
                    const newOptions = [...selectedOptions]
                    newOptions[optionIndex].SubOptions[subOptionIndex].Status = CheckBoxStatus.checked
                    setSelectedOptions([...newOptions])
                }
                handleCheckBoxEvent && handleCheckBoxEvent(subOption, optionIndex, subOptionIndex)
            } else {
                if (!handleStateChangesFromFather) {
                    const newOptions = [...selectedOptions]
                    newOptions[optionIndex].SubOptions[subOptionIndex].Status = CheckBoxStatus.unchecked
                    setSelectedOptions([...newOptions])
                }
                handleCheckBoxEvent && handleCheckBoxEvent(subOption, optionIndex, subOptionIndex)
            }
        } else {
            if (subOption.Status === CheckBoxStatus.unchecked) {

                if (!handleStateChangesFromFather) {
                    const newOptions = [...selectedOptions]
                    newOptions[optionIndex].SubOptions[subOptionIndex].Status = CheckBoxStatus.semi_checked
                    setSelectedOptions([...newOptions])
                }
                handleCheckBoxEvent && handleCheckBoxEvent(subOption, optionIndex, subOptionIndex)

            } else if (subOption.Status === CheckBoxStatus.semi_checked) {
                if (!handleStateChangesFromFather) {
                    const newOptions = [...selectedOptions]
                    newOptions[optionIndex].SubOptions[subOptionIndex].Status = CheckBoxStatus.checked
                    setSelectedOptions([...newOptions])
                }
                handleCheckBoxEvent && handleCheckBoxEvent(subOption, optionIndex, subOptionIndex)
            } else if (subOption.Status === CheckBoxStatus.checked) {
                if (!handleStateChangesFromFather) {
                    const newOptions = [...selectedOptions]
                    newOptions[optionIndex].SubOptions[subOptionIndex].Status = CheckBoxStatus.unchecked
                    setSelectedOptions([...newOptions])
                }
                handleCheckBoxEvent && handleCheckBoxEvent(subOption, optionIndex, subOptionIndex)
            }
        }

    }

    const handleOptionsClicks = (option: CheckBoxGroupsOptions, optionIndex: number, optionStatus: CheckBoxStatus) => {
        if (!allowSemiCheck) {
            if (optionStatus === CheckBoxStatus.unchecked) {
                const newOptions = [...selectedOptions]
                newOptions[optionIndex].SubOptions.forEach((subOption: CheckBoxSubOption, index: number) => {
                    newOptions[optionIndex].SubOptions[index].Status = CheckBoxStatus.checked
                })
                setSelectedOptions([...newOptions])
            } else {
                const newOptions = [...selectedOptions]
                newOptions[optionIndex].SubOptions.forEach((subOption: CheckBoxSubOption, index: number) => {
                    newOptions[optionIndex].SubOptions[index].Status = CheckBoxStatus.unchecked
                })
                setSelectedOptions([...newOptions])
            }
        } else {
            if (optionStatus === CheckBoxStatus.unchecked) {
                const newOptions = [...selectedOptions]
                newOptions[optionIndex].SubOptions.forEach((subOption: CheckBoxSubOption, index: number) => {
                    newOptions[optionIndex].SubOptions[index].Status = CheckBoxStatus.semi_checked
                })
                setSelectedOptions([...newOptions])
            } else if (optionStatus === CheckBoxStatus.semi_checked) {
                const newOptions = [...selectedOptions]
                newOptions[optionIndex].SubOptions.forEach((subOption: CheckBoxSubOption, index: number) => {
                    newOptions[optionIndex].SubOptions[index].Status = CheckBoxStatus.checked
                })
                setSelectedOptions([...newOptions])
            } else if ([CheckBoxStatus.checked, CheckBoxStatus.half_checked].includes(optionStatus)) {
                const newOptions = [...selectedOptions]
                newOptions[optionIndex].SubOptions.forEach((subOption: CheckBoxSubOption, index: number) => {
                    newOptions[optionIndex].SubOptions[index].Status = CheckBoxStatus.unchecked
                })
                setSelectedOptions([...newOptions])
            }
        }

    }

    React.useEffect(() => {
        returnSelectedData && returnSelectedData(selectedOptions)
    }, [selectedOptions])

    const container = searchContainer(filter, (filter: string) => {
        setFilter(filter);
        if (options) {
            let afterFilter = options.map((option: CheckBoxGroupsOptions) => {
                let fatherTrue: boolean = false;
                let childTrue: boolean = false;
                if (option.Name.includes(filter)) fatherTrue = true;
                if (option.SubOptions) {
                    option.SubOptions.forEach((subOption: CheckBoxSubOption) => {
                        if (subOption.Name.includes(filter)) childTrue = true;
                    })
                }
                if (fatherTrue || childTrue) return { ...option, openAccordion: true }
                else return null;
            }).filter((t: any) => t)
            setFiltered(afterFilter)
        }
    }, searchBarPlaceHolder)

    return (
        <>
            <div className="mx-1">
                <p className="text-black font-medium-4 text-bold-700 d-flex">{backButton}{titleLabel}</p>
                {container}
            </div>
            <div className="height-60-vh overflow-auto width-100-per justify-content-center d-flex-column flex-wrap align-items-center">
                {(filter && filter.length ? filtered : options).map((option: CheckBoxGroupsOptions, optionIndex: number) => {
                    const subOptionsLength = option.SubOptions.length;
                    let semiCheckCount: number = 0;
                    let uncheckedCount: number = 0;
                    let checkCount: number = 0;

                    allowSelectMulti && option.SubOptions.forEach((subOption: CheckBoxSubOption) => {
                        if (subOption.Status === CheckBoxStatus.semi_checked) semiCheckCount++;
                        if (subOption.Status === CheckBoxStatus.unchecked) uncheckedCount++;
                        if (subOption.Status === CheckBoxStatus.checked) checkCount++;
                    })

                    let optionStatus: CheckBoxStatus;
                    if (subOptionsLength === semiCheckCount) optionStatus = CheckBoxStatus.semi_checked;
                    else if (subOptionsLength === uncheckedCount) optionStatus = CheckBoxStatus.unchecked;
                    else if (subOptionsLength === checkCount) optionStatus = CheckBoxStatus.checked;
                    else optionStatus = CheckBoxStatus.half_checked;

                    return (
                        option.SubOptions && option.SubOptions.length > 0 ?
                            <Accordion buttonClassName={'d-flex align-items-center justify-content-center'} defaultOpen={option.openAccordion ? true : false}
                                checkBox={allowSelectMulti ? <div style={{ marginTop: 3 }} onClick={() => {
                                    console.log(optionStatus)
                                    handleOptionsClicks(option, optionIndex, optionStatus)
                                }} >
                                    <span className={`combobox__drop-down_custom-checkbox ${optionStatus}`} />
                                </div> : null}
                                caption={option.Name}
                                className={"pt-1 pb-1 "}
                                style={{ borderLeft: 0, borderRight: 0, borderTop: 0, borderBottom: '1px solid #f3f3f5' }}
                            >
                                {option.SubOptions ? option.SubOptions.map((subOption: CheckBoxSubOption, subOptionIndex: number) => {
                                    return (
                                        <div className="d-flex align-items-center mb-1 combobox__drop-down_item ml-3 ">
                                            {<div onClick={() => {
                                                handleSubOptionsClicks(subOption, subOptionIndex, optionIndex)
                                            }} className='combobox__drop-down_label'>
                                                <span className={`combobox__drop-down_custom-checkbox ${subOption.Status}`} />
                                                <span className='ml-075'>{subOption.Name}</span>
                                            </div>}
                                        </div>
                                    )
                                }) : []}
                            </Accordion> : null
                    )
                })}
                <div>
                </div>
            </div>
        </>
    );
}

