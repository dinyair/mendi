# CheckBoxGroupsAccordion Documentation
* * *

Use **Example:**
```js
import { CheckBoxesGroupsAccordion } from '@src/components/checkBoxes-groups-accordion';

<CheckBoxesGroupsAccordion
    options={selectedCheckBoxOptions ? selectedCheckBoxOptions : divisionsWithGroups}
    returnSelectedData={(data: CheckBoxGroupsOptions[]) => setSelectedCheckBoxOptions(data)}
    allowSelectMulti
    allowSemiCheck
    searchBarPlaceHolder={formatMessage({ id: 'searchDivisionsAndGroups' })}
    titleLabel={formatMessage({ id: 'assignDivisionsAndGroups' })}
    handleCheckEvent={(option: CheckBoxSubOption) => { console.log('check', option) }}
    handleSemiCheckEvent={(option: CheckBoxSubOption) => { console.log('semi-check', option) }}
    handleUnCheckEvent={(option: CheckBoxSubOption) => { console.log('un-check', option) }}
    handleStateChangesFromFather
/>
```


## Interfaces & enums
* * *

## <u>`CheckBoxGroupsOptions`</u>
```js
export interface CheckBoxGroupsOptions {
    Id: number; 
    Name: string;
    SubOptions: CheckBoxSubOption[];
    openAccordion?:boolean;
    Status?: CheckBoxStatus;
}
```

Property | Type | Description
-------- | ---- |-------------
Id       | `number` | Id of the option
Name     | `string` | Name of the option to be displayed
SubOptions | `CheckBoxSubOption[]` | Array of sub options
openAccordion | `boolean` (optional) | If set to true, the option accordion will be opened. This parameter is managed by the `CheckBoxesGroupsAccordion` component.
Status |  `CheckBoxStatus` (optional) | This parameter will have the status value of the option, and the checkbox will have this value. This parameter is managed by the `CheckBoxesGroupsAccordion` component.

***Example***
```js
[
    {
        "Id": 1,
        "Name": "מכולת",
        "SubOptions": [
            {
                "Id": 99017,
                "Name": "קפה",
                "Status": "semi-checked"
            },
            {
                "Id": 115,
                "Name": "דגנים",
                "Status": "unchecked"
            },
            {
                "Id": 180,
                "Name": "מאפים מלוחים",
                "Status": "unchecked"
            }
        ]
    },
    {
        "Id": 4,
        "Name": "קפואים",
        "openAccordion":true,
        "SubOptions": [
            {
                "Id": 235,
                "Name": "מוכנים קפוא",
                "Status": "unchecked"
            },
            {
                "Id": 225,
                "Name": "קפוא מן הטבע",
                "Status": "unchecked"
            },
            {
                "Id": 230,
                "Name": "ירקות ופירות קפוא",
                "Status": "unchecked"
            },
            {
                "Id": 250,
                "Name": "דגים קפוא",
                "Status": "unchecked"
            },
            {
                "Id": 220,
                "Name": "מוצרי בצק",
                "Status": "unchecked"
            }
        ]
    },
    {
        "Id": 5,
        "Name": "מעדניה",
        "openAccordion":true,
        "SubOptions": [
            {
                "Id": 275,
                "Name": "מעדניה בשרי",
                "Status": "unchecked"
            }
        ]
    },
    {
        "Id": 6,
        "Name": "משקאות",
        "SubOptions": [
            {
                "Id": 165,
                "Name": "תרכיזים",
                "Status": "unchecked"
            }
        ]
    },
    {
        "Id": 7,
        "Name": "תינוקות",
        "SubOptions": [
            {
                "Id": 340,
                "Name": "מזון תינוקות",
                "Status": "unchecked"
            }
        ]
    }  
]
```


## <u>`CheckBoxSubOption`</u>
```js
export interface CheckBoxSubOption {
    Id: number;
    Name: string;
    Status: CheckBoxStatus;
}
```

Property | Type | Description
-------- | ---- |-------------
Id       | `number` | Id of the sub option
Name     | `string` | Name of the sub option to be displayed
Status   |  `CheckBoxStatus` | This parameter will have the status value of the option, and the checkbox will have this value. 

***Example:***
```js
{
	"Id": 235,
	"Name": "מוכנים קפוא",
	"Status": "unchecked"
}
```



## <u>`CheckBoxStatus`</u>
```js

export enum CheckBoxStatus {
    checked = 'checked',
    semi_checked = 'semi-checked',
    unchecked = 'unchecked'
}
```
The status of each option or sub option. This status also affect the checkbox by adding a the status to the checkbox classnames.

<br><br>
## Table of Contents
* * *
<details>
<summary>
<img src="https://www.ag-grid.com/static/icon-api-85972da3c96c14a531e165188e653633.svg" height="40"><br>
<h3  style="color:green;"> Properties</h3>

</summary>

- [Properties](#properties)
  - [options](#options)
  - [returnSelectedData](#returnSelectedData)
  - [titleLabel](#titleLabel)
  - [searchBarPlaceHolder](#searchBarPlaceHolder)

</details>


<br><br>

<details>
<summary>
<img src="https://www.ag-grid.com/static/icon-information-88ad11a5d6144c17b2eac27e80435e9f.svg" height="40">
<h3  style="color:green;">Optional Properties</h3>
</summary>

- [Optional Properties](#optional-properties)
  - [allowSelectMulti](#allowSelectMulti)
  - [allowSemiCheck](#allowSemiCheck)
  - [handleCheckEvent](#handleCheckEvent)
  - [handleSemiCheckEvent](#handleSemiCheckEvent)
  - [handleUnCheckEvent](#handleUnCheckEvent)
  - [handleStateChangesFromFather](#handleStateChangesFromFather)
</details>

<br><br>



## Properties
* * *
## <u>options -</u>
### Type: `CheckBoxGroupsOptions[]`
This property is the options array. Each option must be of type `CheckBoxGroupsOptions`.
**Example:**
```js
<CheckBoxesGroupsAccordion
    options={selectedCheckBoxOptions ? selectedCheckBoxOptions : divisionsWithGroups}
/>
```
<br><br>


## <u>returnSelectedData -</u>
### Type: `(selectedOptions: CheckBoxGroupsOptions[]) => void`
After changes, the nested `CheckBoxesGroupsAccordion` component will send new array of type: `CheckBoxGroupsOptions[]` with the new options status.
```js
<CheckBoxesGroupsAccordion
    returnSelectedData={(data: CheckBoxGroupsOptions[]) => setSelectedCheckBoxOptions(data)
/>
```
<br><br>


## <u>titleLabel -</u>
#### Type: `string`
This property is the title that appears above the search bar.
```js
<CheckBoxesGroupsAccordion
    titleLabel={formatMessage({ id: 'assignDivisionsAndGroups' })}
/>
```
<br><br>


## <u>searchBarPlaceHolder -</u>
#### Type: `string`
This property is the placeholder that appears above the search bar.
**Example:**
```js
<CheckBoxesGroupsAccordion
    searchBarPlaceHolder={formatMessage({ id: 'searchDivisionsAndGroups' })}
/>
```
<br><br>

    
# Optional Properties
* * *

## <u>allowSelectMulti -</u>
#### Type: `boolean` (optional)
If the property is declared or set to `true`, it’ll add checkbox to every father option, to select / deselect all related sub options.
**Example:**
```js
<CheckBoxesGroupsAccordion
    allowSelectMulti
/>
```
<br><br>

## <u>allowSemiSelect -</u>
#### Type: `boolean` (optional)
If the property is declared or set to `true`, it’ll add a semi-select option to each checkbox. 
If the property `allowSelectMulti` also declared or set to true, it'll add a semi-select option to every father option also.
**Example:**
```js
<CheckBoxesGroupsAccordion
    allowSemiSelect
/>
```
<br><br>

## <u>handleCheckBoxEvent -</u>
#### Type: `(subOption: CheckBoxSubOption) => void` (optional)
If the property is declared, the nested `CheckBoxesGroupsAccordion` component will send the sub option with the old status to the father component.
The father component will handle the changes.
**Example:**
```js
<CheckBoxesGroupsAccordion
    handleCheckEvent={(subOption: CheckBoxSubOption, optionIndex: number, subOptionIndex: number) => { console.log(subOption, optionIndex, subOptionIndex) }}
/>
```
<br><br>

## <u>handleStateChangesFromFather -</u>
#### Type: `boolean` (optional)
If the property is declared or set to `true`, it’ll allow checkbox changes from the father component. (for adding custom handlers)
If this property set to true, you must use `handleCheckBoxEvent` from the father component to handle each checkbox event.
**Example:**
```js
<CheckBoxesGroupsAccordion
    handleStateChangesFromFather
/>
```
<br><br>


