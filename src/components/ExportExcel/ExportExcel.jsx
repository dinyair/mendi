import React from "react";
import ReactExport from "react-export-excel";
import { useIntl } from "react-intl"

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

// interface Props {
// 	readonly data?:any;
// 	readonly fields?:any;
// 	readonly icon?:any;
// 	readonly translateHeader?:any;
// 	readonly filename?:any;
// 	readonly direction?:any;
// 	readonly className?: string;
// }
const ExportExcel = (props) => {
    const { data, fields, icon, direction, translateHeader, filename } = props
    const { formatMessage } = useIntl();

    return data && data.length ? (
        <ExcelFile element={icon ? icon : null} filename={filename}>
            <ExcelSheet data={data} name="table">
                {fields.map((title) => (
                    <ExcelColumn key={title.text} label={translateHeader ? formatMessage({ id: title.text }) : title.text} value={title.text} />
                ))}
            </ExcelSheet>
        </ExcelFile>
    ) : null;
}
export default ExportExcel;