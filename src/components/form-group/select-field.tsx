import * as React from 'react';
import AsyncSelect from 'react-select/async';
import { useIntl, FormattedMessage } from 'react-intl';
import { Icon } from '@components';
import { components } from 'react-select';
import makeAnimated from 'react-select/animated';
import { config } from '@src/config';
import { useEffect } from 'react';

interface Props {
	error?: any;
	value: any | string | Date;
	name: string;
	type: string;
	placeholder?: any;
	options?: string[] | any;
	isMulti?: boolean;
	selectAllOption?:boolean
	separator?: boolean;
	firstSelected?: boolean;
	defaultValue?: any;
	updateValue: any;
	disabled?: boolean;
	selectedSearch:any
	setSelectedSearch:(string:string)=>void;
}


export const SelectOption: React.FC<Props> = props => {
	const {
		setSelectedSearch,
		selectedSearch,
		name,
		type,
		options,
		error,
		updateValue,
		firstSelected,
		value,
		isMulti,
		selectAllOption,
		placeholder,
		defaultValue,
		disabled
	} = props;
	const { formatMessage } = useIntl();
	const selectRef = React.useRef(null);

	const selectAllOptionValue = {
		value: "<SELECT_ALL>",
		label: formatMessage({ id: "select_all" })
	  };

	const preparedOptions: any =
		options && options[0] && !options[0].value
			? [selectAllOption? selectAllOptionValue: null,...options?.map((option: any) => ({ label: option, value: option }))].filter((g:any)=>g)
			: options && options.length ? [selectAllOption? selectAllOptionValue: null,...options].filter((g:any)=>g): []

	const onChange = (option: any,e?:any,actionMeta?:any): void => {
		if(setSelectedSearch) { setSelectedSearch(inputValue) }
		updateValue(name, option,{e,actionMeta});
	};

	React.useEffect(() => {
		if (firstSelected) onChange(preparedOptions[0]);
	}, []);




	const [inputValue, setInputValue] = React.useState<any>('');



	const loadOptions = (inputValue: string, callback: any) => {
		if (inputValue.length >= 3 || preparedOptions.length <= 700) {
			callback(filterOptions(inputValue));
		} else {
			callback();
		}
	};
	const filterOptions = (inputValue: string) => {
		const regex = /\d/;
		const doesContainNumber = regex.test(inputValue);

		let filtered = preparedOptions.filter((i: any) => {
			return i.label.includes(inputValue);
		});

		if (doesContainNumber) {
			filtered.sort((a: any, b: any) => a.value - b.value);
		} else {
			filtered.sort((a: any, b: any) => a.label.length - b.label.length);
		}
		return filtered.slice(0,220);
	};
	const handleInputChange = (query:string, { action }:any) => {
		if (action !== 'set-value') setInputValue(query);
		// return query;
	};
	const Option = (props: any) => {
		return (
			<div>
				<components.Option {...props}>
					<div className='d-flex no-wrap justify-content-start align-items-center'>
					<input type="checkbox" checked={props.isSelected} onChange={() => null} />{' '}
					<label className='elipsis'>{props.label}</label>
					</div>
				</components.Option>
			</div>
		);
	};

	const MultiValue = (props: any) => (
		<components.MultiValue {...props}>
			<span>{props.data.label}</span>
		</components.MultiValue>
	);

	return (
		
		<div className="d-flex flex-column width-100-per">
			<AsyncSelect
				// onMenuOpen={onMenuOpen}
				// menuPlacement={menuPlacement}
				// components={{ IndicatorsContainer: props => <div {...props} /> }}
				isMulti={isMulti}
				isClearable={true}
				ref={selectRef}
				closeMenuOnSelect={isMulti ? false : true}
				blurInputOnSelect={false}
				hideSelectedOptions={false}
				components={isMulti ? { Option, MultiValue } : { MultiValue }}
				// getOptionLabel={ x => x.label}
				// getOptionValue={ x => x.value}
				isDisabled={preparedOptions && preparedOptions.length ? (disabled ? true : false) : true}
				id={`form-group-${type}-${name}`}
				placeholder={placeholder ? placeholder : typeof value == 'string' ? value : ''}
				// defaultValue={defaultValue ? defaultValue : null }
				value={value && typeof value != 'string' ? value: defaultValue ? defaultValue : firstSelected ? preparedOptions[0] : null} //defaultOption
				// defaultOption={defaultValue ? defaultValue : []}
				// value={}
				className="form-group__field_select"
				loadOptions={loadOptions}
				defaultOptions={preparedOptions&&preparedOptions.length > 700 ? false : preparedOptions}
				noOptionsMessage={
					inputValue.length >= 3
						? () => formatMessage({ id: 'noResults' })
						: () => formatMessage({ id: 'trending_productMinimumSearch' })
				}
				
				onInputChange={handleInputChange}
				onChange={onChange}
				classNamePrefix="form-group__select"
				styles={{
					menu: provided => ({ ...provided, zIndex: 9999 }),
					control: provided => ({
						...provided,
						paddingRight: 5,
						border: error ? '1px solid red!important' : ''
					})
				}}
				theme={theme => ({
					...theme,
					borderRadius: 20,
					spacing: {
						baseUnit: 3.5,
						controlHeight: 30,
						menuGutter: 8
					},
					colors: {
						...theme.colors,
						primary: '#31baab'
					}
				})}
			/>
				<div className={`ml-05 text-bold-600 font-small-2 text-danger min-height-1-rem max-height-1-rem ${!error ? 'visibility-hidden':''}`}>
				{error && error.label ? error.label: 'bla'}
			</div>
		</div>
	);
};
