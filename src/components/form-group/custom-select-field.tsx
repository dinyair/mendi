import * as React from 'react';
import AsyncSelect from 'react-select/async';
import { useIntl, FormattedMessage } from 'react-intl';
import { Icon } from '@components';
import { components } from 'react-select';
import makeAnimated from 'react-select/animated';
import { config } from '@src/config';
import { useEffect } from 'react';
import { Option, SelectUnSelectEvents } from './interfaces';
import './form-group.scss';

interface Props {
	error?: any;
	value: any | string | Date;
	name: string;
	type: string;
	placeholder?: any;
	options?: string[] | any;
	isMulti?: boolean;
	selectAllOption?: boolean
	selectUnSelectOption?: boolean;
	onChangeMultiValues?: (name: string, newValues: any[]) => void;
	separator?: boolean;
	firstSelected?: boolean;
	defaultValue?: any;
	updateValue: any;
	disabled?: boolean;
	selectedSearch: any
	setSelectedSearch: (string: string) => void;
}



export const CustomSelect: React.FC<Props> = props => {
	const {
		setSelectedSearch,
		selectedSearch,
		name,
		type,
		options,
		error,
		updateValue,
		firstSelected,
		value,
		isMulti,
		selectAllOption,
		selectUnSelectOption,
		onChangeMultiValues,
		placeholder,
		defaultValue,
		disabled
	} = props;
	const { formatMessage } = useIntl();
	const selectRef = React.useRef(null);

	const selectAllOptionValue = {
		value: "<SELECT_ALL>",
		label: formatMessage({ id: "select_all" }),
		event: SelectUnSelectEvents.selectAll
	};

	const unSelectAllOption = {
		value: "<UN_SELECT_ALL>",
		label: formatMessage({ id: "un_select_all" }),
		event: SelectUnSelectEvents.unSelectAll
	};
	const selectAllFilteredOption = {
		value: "<SELECT_ALL_FILTER>",
		label: formatMessage({ id: "select_all" }) + ' - ',
		event: SelectUnSelectEvents.selectFiltered
	};

	const unSelectAllFilteredOption = {
		value: "<UN_SELECT_ALL_FILTER>",
		label: formatMessage({ id: "un_select_all" }) + ' - ',
		event: SelectUnSelectEvents.unSelectFiltered
	};
	const preparedOptions: any =
		options && options[0] && !options[0].value
			? [selectAllOption ? selectAllOptionValue : null, ...options?.map((option: any) => ({ label: option, value: option }))].filter((g: any) => g)
			: options && options.length ? [selectAllOption ? selectAllOptionValue : null, ...options].filter((g: any) => g) : []

	const onChange = (option: any, e?: any, actionMeta?: any): void => {
		if (setSelectedSearch) { setSelectedSearch(inputValue) }
		updateValue(name, option, { e, actionMeta });
	};

	React.useEffect(() => {
		if (firstSelected) onChange(preparedOptions[0]);
	}, []);




	const [inputValue, setInputValue] = React.useState<any>('');



	// console.log({ firstSelected })
	// const [menuPlacement, setMenuPlacement] = React.useState('auto');
	// const onMenuOpen = () => {
	// 	const { select } = selectRef.current || {};
	// 	const { controlRef } = select;
	// 	// const optionsCount = select.commonProps.options.length;
	// 	const form = controlRef.closest('.form-group');
	// 	const inputRect = controlRef.getBoundingClientRect();
	// 	const formRect = form.getBoundingClientRect();
	// 	// const bottomSpace = (formRect.top + formRect.height) - (inputRect.top + inputRect.height);
	// 	// const topSpace = inputRect.top - formRect.top;

	// 	// if (topSpace > bottomSpace && bottomSpace < optionsCount * 40) {
	// 	// 	setMenuPlacement('top');
	// 	// } else {
	// 	// 	setMenuPlacement('auto');
	// 	// }
	// }

	const loadOptions = (inputValue: string, callback: any) => {
		if (inputValue.length >= 3 || preparedOptions.length <= 700) {
			callback(filterOptions(inputValue));
		} else {
			callback();
		}
	};
	const filterOptions = (inputValue: string) => {
		const regex = /\d/;
		const doesContainNumber = regex.test(inputValue);

		let filtered = preparedOptions.filter((i: any) => {
			return i.label.includes(inputValue);
		});

		if (doesContainNumber) {
			filtered.sort((a: any, b: any) => a.value - b.value);
		} else {
			filtered.sort((a: any, b: any) => a.label.length - b.label.length);
		}
		return filtered.slice(0, 220);
	};
	const handleInputChange = (query: string, { action }: any) => {
		if (action !== 'set-value') setInputValue(query);
		// return query;
	};
	const Option = (props: any) => {
		return (
			<div>
				<components.Option {...props}>
					<input type="checkbox" checked={props.isSelected} onChange={() => null} />{' '}
					<label>{props.label}</label>
				</components.Option>
			</div>
		);
	};

	const MultiValue = (props: any) => (
		<components.MultiValue {...props}>
			<span>{props.data.label}</span>
		</components.MultiValue>
	);

	const [selectUnSelectHover, setSelectUnSelectHover] = React.useState<boolean>(false);
	const MenuList = (props: any) => {
		console.log({ value, defaultValue })
		const selectedOptions = defaultValue ? defaultValue : []
		let filteredOptions = preparedOptions.filter((i: any) => i.label.includes(inputValue));

		let filteredValues = filteredOptions && filteredOptions.length ? filteredOptions.map((ele: any) => ele.value).sort((a: number, b: number) => a - b) : []
		let selectedValues = selectedOptions && selectedOptions.length ? selectedOptions.filter((ele: any) => ele.value !== selectAllFilteredOption.value).map((ele: any) => ele.value).sort((a: number, b: number) => a - b) : []
		let isAllSelected = filteredValues.every((r: any) => selectedValues.includes(r));
		// console.log({ isAllSelected, filteredOptions,selectedOptions })
		// console.log({ filtered: filteredOptions })

		const newFiltered = [...filteredOptions]
		let label: string = '';
		let event: SelectUnSelectEvents = SelectUnSelectEvents.selectAll;
		if (filteredOptions.length > 1 && !isAllSelected && filteredOptions.length == options.length) { label = selectAllOptionValue.label; event = SelectUnSelectEvents.selectAll }

		else if (filteredOptions.length > 1 && !isAllSelected && filteredOptions.length !== options.length) { label = selectAllFilteredOption.label + inputValue; event = SelectUnSelectEvents.selectFiltered }

		else if (filteredOptions.length > 1 && isAllSelected && filteredOptions.length === options.length) { label = unSelectAllOption.label; event = SelectUnSelectEvents.unSelectAll }

		else if (filteredOptions.length > 1 && isAllSelected && filteredOptions.length !== options.length) { label = unSelectAllFilteredOption.label + inputValue; event = SelectUnSelectEvents.unSelectFiltered }

		const checked = [SelectUnSelectEvents.unSelectFiltered, SelectUnSelectEvents.unSelectAll].includes(event) ? true : false;
		const checkedClassNames = 'form-group__select__option form-group__select__option--is-focused form-group__select__option--is-selected css-19866sa-option'
		const unCheckedClassNames = 'form-group__select__option css-1aa89sw-option select-menu-option'

		let newValues: any = [];
		switch (event) {
			case SelectUnSelectEvents.selectAll:
				newValues = [...filteredOptions];
				break;
			case SelectUnSelectEvents.unSelectAll:
				newValues = [];
				break;
			case SelectUnSelectEvents.selectFiltered:
				const newSelected: any = [];
				filteredOptions.forEach((filtered: Option) => {
					if (!selectedOptions.find((selected: Option) => selected.value === filtered.value)) newSelected.push(filtered)
				});
				newValues = [...selectedOptions, ...newSelected];
				break;
			case SelectUnSelectEvents.unSelectFiltered:
				let newOptions: any = [...selectedOptions];
				console.log(newOptions, filteredOptions)
				filteredOptions.forEach((filtered: Option) => {
					newOptions = newOptions.filter((option: Option) => option.value !== filtered.value)
				});
				newValues = [...newOptions];
				break;
		}

		return (
			<components.MenuList {...props}>
				{/* onMouseDown={(e) =>setSelectUnSelectHover(false) } onMouseOver={(e) => setSelectUnSelectHover(true)} */}
				{label && <div className={checked ? checkedClassNames : unCheckedClassNames} onClick={() => { onChangeMultiValues && onChangeMultiValues(name, newValues) }}>
					<input id="custom-check" type="checkbox"
						checked={checked}
						onChange={() => null} />{' '}
					<label htmlFor="custom-check">{label}</label>
				</div>}
				{props.children}
			</components.MenuList>
		);
	};


	return (

		<div className="d-flex flex-column width-100-per">
			<AsyncSelect
				// onMenuOpen={onMenuOpen}
				// menuPlacement={menuPlacement}
				// components={{ IndicatorsContainer: props => <div {...props} /> }}
				isMulti={isMulti}
				isClearable={true}
				ref={selectRef}
				closeMenuOnSelect={false}
				blurInputOnSelect={false}
				hideSelectedOptions={false}
				components={selectUnSelectOption && isMulti ? { MenuList, Option, MultiValue } : isMulti ? { Option, MultiValue } : { MultiValue }}
				// getOptionLabel={ x => x.label}
				// getOptionValue={ x => x.value}
				isDisabled={preparedOptions && preparedOptions.length ? (disabled ? true : false) : true}
				id={`form-group-${type}-${name}`}
				placeholder={placeholder ? placeholder : typeof value == 'string' ? value : ''}
				// defaultValue={defaultValue ? defaultValue : null }
				// value={ value && typeof value != 'string' ? value: defaultValue ? defaultValue : firstSelected ? preparedOptions[0] : null} //defaultOption
				// defaultOption={defaultValue ? defaultValue : []}
				value={defaultValue ? defaultValue : null}
				defaultValue={defaultValue ? defaultValue : null}
				className="form-group__field_select"
				loadOptions={loadOptions}
				defaultOptions={preparedOptions && preparedOptions.length > 700 ? false : preparedOptions}
				noOptionsMessage={
					inputValue.length >= 3
						? () => formatMessage({ id: 'noResults' })
						: () => formatMessage({ id: 'trending_productMinimumSearch' })
				}
				// menuIsOpen={true}
				onInputChange={handleInputChange}
				onChange={onChange}
				classNamePrefix="form-group__select"
				styles={{
					menu: provided => ({ ...provided, zIndex: 9999 }),
					control: provided => ({
						...provided,
						paddingRight: 5,
						border: error ? '1px solid red!important' : ''
					})
				}}
				theme={theme => ({
					...theme,
					borderRadius: 20,
					spacing: {
						baseUnit: 3.5,
						controlHeight: 30,
						menuGutter: 8
					},
					colors: {
						...theme.colors,
						primary: '#31baab'
					}
				})}
			/>
			{error && error.label ?
				<div className="ml-05 text-bold-600 font-small-2 text-danger min-height-1-rem max-height-1-rem">
					{error.label}
				</div>
				: ''}
		</div>
	);
};
