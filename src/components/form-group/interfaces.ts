export const enum InputTypes {
	TEXT = 'text',
	NUMBER = 'number',
	PASSWORD = 'password',
	SELECT = 'select',
	SELECT_ALL = 'select_all',
	CHECKBOX = 'checkbox',
	DATE = 'date',
	SINGLE_DATE = "single_date"
}

export interface Field {
	error?: any;
	disabled?: boolean;
	alllowsCharacters?: any[];
	isMulti?: boolean;
	selectAllOption?: boolean
	optionsRequired?: boolean;
	label: string;
	labelClassNames?: string;
	fieldClassNames?: string;
	value: string | Date | any[] | boolean | number;
	name: string;
	selectName?:string;
	selectValue?:any
	type: InputTypes;
	options?: string[];
	separator?: boolean;
	placeholder?: any;
	columns?: number;
	firstSelected?: boolean
	defaultValue?: any,
	actionButton?: any,
	validations?: any[],
	onBlur?: (value:any)=> void;
	isSelected?: boolean
	classNames?: string
	onChangeFunc?: (newData: any, name: string, props?: any) => void
	dontChangeTemp?: boolean
	dateData?: any
	datePosition?: any
	calanderId?: string
	inputId?: string
	selectUnSelectOption ? :boolean
	onChangeMultiValues ?: (name: string, newValues: any[]) => void;
}

export interface AdditionalFields {
	title: string;
	fields: Field[];
}

export interface Fields {
	fields: Field[];
	additionalFields?: AdditionalFields;
	extraFields?: any;
}

export interface FormGroup {
	title?: string;
	formId: string;
	fields: Field[];
	additionalFields?: AdditionalFields;
	extraFields?: any;
}

export interface Option {
	label: string,
	value: any,
}

export enum SelectUnSelectEvents {
	selectAll  = 'selectAll',
	unSelectAll = 'unSelectAll',
	selectFiltered = 'selectFiltered',
	unSelectFiltered = 'unSelectFiltered'
}
