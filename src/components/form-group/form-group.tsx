import * as React from 'react';
import classnames from "classnames";
import { Button } from 'reactstrap';
import { Field } from './field';
import { Field as FieldType, AdditionalFields, Fields } from './interfaces';
import { FormattedMessage, useIntl } from "react-intl"
import { Divider } from '@material-ui/core';
import { InputTypes } from "./interfaces";

import './form-group.scss';

interface Props {
	title?: string;
	extraFields?: any;
	id: string;
	formId: string;
	setTempData?: (tempData: any[]) => void;
	setSaveDisabled: (boolean: boolean) => void;
	tempData?: any
	fields: FieldType[];
	description: string;
	additionalFields?: AdditionalFields;
	onChangeData: (tabId: string, formId: string, fields: Fields) => void;
	formGroupListClassName?:string
	extraFieldsClassName?:string
	forceDelete?:boolean
	selectedSearch:any
	setSelectedSearch:(string:string)=>void;
}

export const FormGroup: React.FC<Props> = props => {
	const { formatMessage } = useIntl();

	const selectAllOptionValue = {
		value: "<SELECT_ALL>",
		label: formatMessage({ id: "select_all" })
	  };

	const { title, extraFields, fields, description, additionalFields, id, formId, onChangeData, setTempData, setSaveDisabled, tempData,formGroupListClassName,extraFieldsClassName,forceDelete ,selectedSearch, setSelectedSearch} = props;
	const [shown, setStateAdditionalFields] = React.useState(false);
	const [additionalFieldsList, setAdditionalFields] = React.useState<FieldType[]>([]);
	const [extraFieldsList, setExtraFields] = React.useState<FieldType[]>(extraFields && extraFields.fields ? extraFields.fields : []);
	const refData = React.useRef<{
		fields: FieldType[]; additionalFields: AdditionalFields; extraFields: AdditionalFields
	}>({ fields, additionalFields, extraFields });


	const onChange = ({ name, value,dontChangeTemp }: { name: string; value: any,dontChangeTemp?:boolean }, p?: any): void => {
		refData.current.fields.forEach(field => {
			if (field.name === name) {
				if( field.type === InputTypes.SELECT) {
					field.defaultValue = value
					field.value = value;
					// field.defaultValue = p.defaultValue; 
				} else {
					field.defaultValue = value
					field.value = value;
				}
				if(field.name.includes('password') && p && p.disabled) field.disabled = true
			}else  if(field.selectName == name && p && p.splitedSelectField){
				field.selectValue = value;
			}
		});
		if (refData.current.additionalFields?.fields) {
			refData.current.additionalFields.fields.forEach(field => {
				if (field.name === name) {
					field.value = value
					field.defaultValue = value
				}
			});
		}
		if (refData.current.extraFields?.fields) {
			refData.current.extraFields.fields.forEach(field => {
				if (field.name === name) {
					field.defaultValue = value
					field.value = value
				
				}
			});
		}
		onChangeData(id, formId, refData.current)
		if (setTempData && !dontChangeTemp) setTimeout(() => {
			if (!value) {
				let newTempData = tempData
				newTempData[name] = {}
				setTempData([])
				setTempData(newTempData)
			} else {
				let isSelectAll = false
				if(Array.isArray(value)){
					value.forEach((g:any)=> {if(g.value == selectAllOptionValue.value)isSelectAll = true})
				}
				if(!isSelectAll){
				setTempData({ ...tempData, [name]: { name: name, preText: name, data: value && typeof value != 'string' ? Array.isArray(value) ? value.map((val: any) => { return { label: val.label, value: val.value, name } }) : [{ label: value.label, value: value.label, name }] : [{ label: value, value: value, name }] } })
				}
			}
		}, 100);
	}
	const onDelete = (name: string): void => {
		// TODO - REGULAR FIELDS DELETE 
		// refData.current.fields = refData.current.fields.filter(field => field.name != name )
		let columns: any = 0;
		let index;

		if (refData.current.additionalFields?.fields) {
			index = refData.current.additionalFields.fields.findIndex(ele => ele.name === name)
			columns = refData.current.additionalFields.fields[index].columns;
			const fields = [...refData.current.additionalFields.fields];
			if (refData.current.additionalFields?.fields.length > 2) {
				fields.splice(index - 1, columns)
				refData.current.additionalFields.fields = [...fields]
			}
			else {
				for (let index = 0; index < columns; index++) {
					fields[index].value = '';
					fields[index].defaultValue = '';
				}

				refData.current.extraFields.fields = [...fields]
				setExtraFields([])
				setTimeout(() => {
					setExtraFields(fields)
				}, 1);
			}
		}
		if (refData.current.extraFields?.fields) {
			index = refData.current.extraFields.fields.findIndex(ele => ele.name === name)
			columns = refData.current.extraFields.fields[index].columns;
			const fields: any = [...refData.current.extraFields.fields];

			if (refData.current.extraFields?.fields.length > 2 || forceDelete) {
				fields.splice(index - 1, columns)
				refData.current.extraFields.fields = [...fields]
				setExtraFields(fields)
			} else {
				for (let index = 0; index < columns; index++) {
					fields[index].value = '';
					fields[index].defaultValue = '';
				}

				refData.current.extraFields.fields = [...fields]
				setExtraFields([])
				setTimeout(() => {
					setExtraFields(fields)
				}, 1);
			}
		}
		onChangeData(id, formId, refData.current)
	}

	const toggleAdditionalFields = (): void => {
		setStateAdditionalFields(prevValue => !prevValue);
	}

	const onSaveAdditionalFields = (): void => {
		if (refData.current.additionalFields?.fields) {
			const newAdditionalFieldsList = refData.current.additionalFields?.fields.filter(({ value }) => value);
			setAdditionalFields(newAdditionalFieldsList);
		}
		toggleAdditionalFields();
	}

	const addExtraFieldsList = (): void => {
		if (refData.current.extraFields?.fields) {
			refData.current.extraFields.fields = [...refData.current.extraFields.fields, ...extraFields.base.map((ef: any, index: number) => { return { ...ef, name: ef.name + (extraFieldsList.length + index) } })]
			onChangeData(id, formId, refData.current)
			setExtraFields(refData.current.extraFields?.fields)
		}
	}



	const classes = classnames(`form-group__additional-group ${extraFieldsClassName ? extraFieldsClassName : ''}`, {
		'is-open': shown
	});

	const classesSelectedList = classnames('form-group__additional-group_selected', {
		'is-open-content': shown
	});

	return (<div className={classnames('',{"d-flex": description})}>
		<div className='form-group width-100-per'>
			{title ? <h3 className='form-group__title'>{title}</h3> : null}
			<ul className={`form-group__list ${formGroupListClassName?formGroupListClassName:''}`}>
				{fields.map((field, i) => (
					<li key={i} className='form-group__item' style={field.columns ? { gridColumn: `span ${field.columns}` } : {}}>
						<Field selectedSearch={selectedSearch} setSelectedSearch={setSelectedSearch} key={field.name} setSaveDisabled={setSaveDisabled} {...{ fieldData: field, onChange }} />
					</li>
				))}
			</ul>
			{additionalFields ? (<>
				<div className={classes}>
					<h3 className='form-group__additional-group_title'>
						<span className='form-group__additional-group_label'>{formatMessage({ id: 'kosher' })}</span>
						{!additionalFieldsList.length ? (
							<span className='form-group__additional-group_action-btn' onClick={toggleAdditionalFields}>
								{additionalFields.title}
							</span>) : null
						}
					</h3>
					<div className='form-group__additional-group_content'>
						<ul className='form-group__additional-group_list'>
							{additionalFields.fields.map(field => (
								<Field selectedSearch={selectedSearch} setSelectedSearch={setSelectedSearch} key={field.name} setSaveDisabled={setSaveDisabled} {...{ fieldData: field, onChange }} />
							))}
						</ul>
						<Button className='round mr-1 form-group__additional-group_apply-changes' color='primary' onClick={onSaveAdditionalFields}>Save</Button>
					</div>
					{additionalFieldsList.length ? (
						<>
							<ul className={classesSelectedList}>
								{additionalFieldsList.map(field => (
									<li className='form-group__additional-group_selected-item' key={field.name}>{field.label}</li>
								))}
							</ul>
							<span className='form-group__additional-group_action-btn' onClick={toggleAdditionalFields}>
								{additionalFields.title}
							</span>
						</>
					) : null}



				</div> </>) : null}

			{extraFields ? (
				<div className={classes}>

					{extraFieldsList.length ? (
						<>
							<ul className='form-group__list'>
								{extraFieldsList.map((field, i) => (
									<li key={i} className='form-group__item' style={field.columns ? { gridColumn: `span ${field.columns}` } : {}}>
										{/* && extraFieldsList.length > field.columns */}
										<Field selectedSearch={selectedSearch} setSelectedSearch={setSelectedSearch} setSaveDisabled={setSaveDisabled}  key={field.name} deleteAble={!extraFields.disableDeleteButton && field.columns && (i + 1) % field.columns == 0 ? true : false} {...{ fieldData: field, validations: field.validations, onDelete, onChange, id: `${field.name}${i}`, name: `${field.name}${i}` }} />
									</li>
								))}
							</ul>

						</>
					) : null}
					{!extraFields.disableAddButton && extraFields.max > (extraFieldsList.length / extraFields.base.length) ?
						<h3 className='form-group__additional-group_title'>
							<span className='form-group__additional-group_action-btn' onClick={addExtraFieldsList}>
								{<FormattedMessage id={extraFields.title} />}
							</span>
						</h3> : null}
				</div>
			) : null}
		</div>
	
		
		{description && ( <>
			<Divider/>
			<div dangerouslySetInnerHTML={{ __html: description }} />
		</>)}
	</div>);
}
