import * as React from 'react';
import classnames from 'classnames';
import { FieldByType } from './field-by-type';
import { Field as FieldType, InputTypes } from './interfaces';
import { config } from '@src/config';
import { Icon } from '@components';
import { Button, Spinner } from 'reactstrap';
import { FormattedMessage } from 'react-intl';
const RandExp = require('randexp');
interface Props {
	fieldData: FieldType;
	deleteAble?: boolean;
	setSaveDisabled: (bool: boolean) => void;
	onDelete?: (name: string) => void;
	onChange: (obj: { name: string; value: string | Date; dontChangeTemp?: boolean }, props?: any) => void;
	selectedSearch: any;
	setSelectedSearch: (string: string) => void;
}

export const Field: React.FC<Props> = ({
	fieldData,
	onChange,
	onDelete,
	deleteAble,
	setSelectedSearch,
	selectedSearch,
	setSaveDisabled
}) => {
	let {
		label,
		labelClassNames,
		value,
		name,
		selectValue,
		selectName,
		options,
		isMulti,
		selectAllOption,
		type,
		fieldClassNames,
		disabled,
		actionButton,
		validations,
		separator,
		firstSelected,
		error,
		placeholder,
		defaultValue,
		alllowsCharacters,
		classNames,
		onChangeFunc,
		dontChangeTemp,
		dateData,
		calanderId,
		inputId,
		optionsRequired,
		datePosition,
		selectUnSelectOption,
		onChangeMultiValues,
		onBlur
	} = fieldData;
	const [currVal, setValue] = React.useState(value);
	const [loading, setLoading] = React.useState<boolean>(false);




	const updateValue = (name: string, newValue: string | Date, props?: any): void => {
		// if (props && props.disabled) fieldData.disabled = true
		if (onChangeFunc) {
			onChangeFunc(newValue, name, props);
		}
		if (!selectAllOption) {
			// if(newValue != '')
			onChange({ name, value: newValue, dontChangeTemp }, props);
			setValue(newValue);
		}
	};

	const classes = classnames(`form-group__field ${fieldClassNames ?? ''}`, {
		'form-group__checkbox-field': type === InputTypes.CHECKBOX,
	});

	React.useEffect(() => {
		setValue(value);
	}, [value, fieldData]);

	return (
		<>
			<div className={classes}>
				<label
					className={`form-group__label ${labelClassNames ?? ''}`}
					htmlFor={`form-group-${type}-${name}`}
				>
					{label}
				</label>
				<div className={`d-flex align-items-center  ${type === InputTypes.CHECKBOX ? '' : 'min-width-8-rem'}`}>
					<FieldByType
						{...{
							selectedSearch,
							setSelectedSearch,
							calanderId,
							inputId,
							optionsRequired,
							alllowsCharacters,
							value: currVal,
							setSaveDisabled,
							disabled,
							onBlur,
							name,
							selectValue,
							selectName,
							validations,
							type,
							options,
							actionButton,
							isMulti,
							selectAllOption,
							error,
							separator,
							updateValue,
							firstSelected,
							placeholder,
							defaultValue,
							classNames,
							datePosition,
							dateData,
							selectUnSelectOption,
							onChangeMultiValues
						}}
					/>
					{deleteAble && onDelete ? (
						<div onClick={() => onDelete(name)}>
							<Icon
								src={`${config.iconsPath}/planogram/trash.svg`}
								className="mr-05 cursor-pointer"
								style={{ height: '0.5rem', width: '0.2rem' }}
							/>
						</div>
					) : null}
				</div>
			</div>
			{actionButton && (
				<div
					className={classnames('d-flex align-items-start mt-05', {
						'text-lightGray cursor-default': loading || fieldData.disabled
					})}
				>
					<button
						disabled={loading || fieldData.disabled}
						className={classnames(
							'font-medium-1 text-bold-700 mr-1 d-flex align-items-center no-outline no-border no-bg text-turquoise-non',
							{
								'text-lightGray cursor-default': loading || fieldData.disabled
							}
						)}
						onClick={() => {
							// setLoading(true)
							let newPassword = '';
							// const passRegex = new RegExp(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8}$/);
							const passRegex = new RegExp(
								/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8}$/
							);

							newPassword = new RandExp(passRegex).gen();
							while (!passRegex.test(newPassword)) {
								newPassword = new RandExp(passRegex).gen();
							}
							updateValue(name, newPassword, { disabled: true });
							updateValue('password', newPassword, { disabled: true });
							setTimeout(() => {
								setLoading(false);
							}, 300);
							if (actionButton && actionButton.onClick) actionButton.onClick();
						}}
					>
						<div className="d-flex justify-content-center align-items-center">
							<Icon
								src={`${config.iconsPath}/general/paperPlane.svg`}
								className="mr-05 mt-015 cursor-pointer"
								style={{ height: '0.5rem', width: '0.2rem' }}
							/>
							<span>{<FormattedMessage id={actionButton.buttonText} />}</span>
						</div>
					</button>
					{loading && <Spinner />}
				</div>
			)}
		</>
	);
};
