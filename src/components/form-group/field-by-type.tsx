import * as React from "react";
import { CustomInput } from "reactstrap";
import { SeparateNumberField } from "./separate-number-field";
import { CustomSelect } from "./custom-select-field";
import { InputTypes, SelectUnSelectEvents } from "./interfaces";
import { CalendarDoubleInput, CalendarSingleInput } from '@components';
import classnames from "classnames"
import { SelectOption } from "./select-field";
import { useIntl } from "react-intl"


interface Props {
	isMulti?: boolean;
	selectAllOption: boolean
	error?: any;
	disabled?: boolean;
	value: string | Date | any[] | number;
	name: string;
	selectValue: any;
	selectName: string;
	type: string;
	options?: any[];
	optionsRequired: boolean;
	separator?: boolean;
	firstSelected?: boolean;
	alllowsCharacters?: any[];
	placeholder?: any
	defaultValue?: any
	dateData?: any
	datePosition?: any
	validations?: any
	onBlur: (value:any)=> boolean;
	setSaveDisabled: (boolean:boolean) => void;
	classNames?: any
	updateValue: (name: string, val: string | Date | number | any[], props?: any) => void;
	calanderId?: string
	inputId?: string
	selectedSearch: any
	setSelectedSearch: (string: string) => void;
	selectUnSelectOption?: boolean
	onChangeMultiValues?: (name: string, newValues: any[]) => void;
}

export const FieldByType: React.FC<Props> = props => {
    const { formatMessage } = useIntl();

	const { selectedSearch, setSelectedSearch, value, name, selectValue, selectName, type,  disabled, options, onBlur,setSaveDisabled, optionsRequired, alllowsCharacters, classNames, isMulti, selectAllOption, separator, updateValue, firstSelected, placeholder, defaultValue, datePosition, dateData, calanderId, inputId, onChangeMultiValues, selectUnSelectOption } = props;
    const [error, setError] = React.useState<any | null | undefined>(props.error);
	
	React.useEffect(() => {
		setError(props.error)
	}, [props.error]);

	switch (type) {
		case InputTypes.TEXT: {
			const onChange = (event: React.FormEvent<HTMLInputElement>): void => {
				if (alllowsCharacters && alllowsCharacters.length) {
					let isValid = alllowsCharacters.map((v: any) => {
						if ((event.currentTarget.value.toString().length) || v && v(event.currentTarget.value)) return true
						return false
					})
					if (!isValid.includes(false)) {
						updateValue(name, event.currentTarget.value);
					}
				} else updateValue(name, event.currentTarget.value);
			}

			if (typeof value === 'string') {
				return (<div className="d-flex flex-column width-100-per">
					<input
						style={
							error
								?
								{ border: '1px solid red' } : {}}
						placeholder={placeholder}
						autoComplete={'off'}
						disabled={disabled}
						id={`form-group-${type}-${name}`}
						onChange={onChange}
						className={classnames("form-group__field_text", {
							"bg-gray": disabled,
						})}
						name={name}
						type='text'
						value={value}
					/>
					{error && error.label ? <div className='ml-05 text-bold-600 font-small-2 text-danger min-height-1-rem max-height-1-rem'>{error.label}</div> : ''}
				</div>);
			}

			return null;
		}
		case InputTypes.PASSWORD: {
			const onChange = (event: React.FormEvent<HTMLInputElement>): void => {
				if (alllowsCharacters && alllowsCharacters.length) {
					let isValid = alllowsCharacters.map((v: any) => {
						if ((event.currentTarget.value.toString().length) || v && v(event.currentTarget.value)) return true
						return false
					})
					if (!isValid.includes(false)) {
						updateValue(name, event.currentTarget.value);
					}
				} else updateValue(name, event.currentTarget.value);
			}

			if (typeof value === 'string') {
				return (<div className="d-flex flex-column">
					<input
						style={
							error
								?
								{ border: '1px solid red' } : {}}
						disabled={disabled}
						id={`form-group-${type}-${name}`}
						autoComplete={'new-password'}
						onChange={onChange}
						className='form-group__field_text'
						name={name}
						type='password'
						value={value}
					/>
					{error && error.label ? <div className='ml-05 text-bold-600 font-small-2 text-danger min-height-1-rem max-height-1-rem'>{error.label}</div> : ''}
				</div>);
			}

			return null;
		}
		case InputTypes.NUMBER: {
			const onChange = (event: React.FormEvent<HTMLInputElement>): void => {
				if (alllowsCharacters && alllowsCharacters.length) {
					let isValid = alllowsCharacters.map((v: any) => {
						if ((event.currentTarget.value.toString().length) || v && v(event.currentTarget.value)) return true
						return false
					})
					if (!isValid.includes(false)) {
						updateValue(name, event.currentTarget.value);
					}
				} else updateValue(name, event.currentTarget.value);
				
			}

			if (separator) {
				return <SeparateNumberField {...{ value, name, selectValue, selectName, error, type, options, updateValue, defaultValue }} />;
			}
			if (typeof value === 'string' || typeof value === 'number') {
				return (<div className="d-flex flex-column width-100-per">
					<input
						style={
							error
								?
								{ border: '1px solid red' } : {}}
						disabled={disabled}
						placeholder={placeholder}
						autoComplete={'off'}
						id={`form-group-${type}-${name}`}
						onChange={onChange}
						onBlur={(event:any)=> {
							if( onBlur ){ 
								let exist:boolean = onBlur(event.currentTarget.value)
								if( exist ) {
									setError({label: formatMessage({ id: 'this barcode allready exists'}) })
									setSaveDisabled(true)
								} else {
									setError(null)
									setSaveDisabled(false)
								}
							}
						}}
						className={classnames("form-group__field_number", {
							"bg-gray": disabled,
						})}
						name={name}
						type='number'
						value={value ? Number(value) : ''}
					/>
					{error && error.label ? <div className='ml-05 text-bold-600 font-small-2 text-danger min-height-1-rem max-height-1-rem'>{error.label}</div> : ''}
				</div>);
			}

			return null;
		}
		case InputTypes.CHECKBOX: {
			const onChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
				const newValue = e.target.checked ? 'true' : '';
				updateValue(name, newValue);
			}

			if (typeof value === 'string' || typeof value === 'boolean') {
				return (<div className='d-flex flex-column width-2-vw'>
					<CustomInput
						onChange={onChange}
						className={`form-group__checkbox-field_checkbox ${classNames}`}
						id={`form-group-${type}-${name}`}
						type='checkbox'
						name={name}
						checked={Boolean(value)}
						value={value}
					/>
					{error && error.label ? <div className='ml-05 text-bold-600 font-small-2 text-danger min-height-1-rem max-height-1-rem'>{error.label}</div> : ''}
				</div>)
			}


			return null;
		}
		case InputTypes.DATE: {
			const onChange = (date: any): void => {
				updateValue(name, date);
			}
			return (
				<div className="d-flex flex-column width-100-per position-relative">
					<CalendarDoubleInput
						onChange={onChange}
						name={name}
						setDate1={value.setDate1}
						setDate2={value.setDate2}
						date2={value.date2}
						date1={value.date1}
						// dontClose={true}
						top={'-4rem'}
						left={'0rem'}
						calanderId={calanderId ? calanderId : null}
						inputId={inputId ? inputId : null}
					/>
					{error && error.label ? <div className='ml-05 text-bold-600 font-small-2 text-danger min-height-1-rem max-height-1-rem'>{error.label}</div> : ''}
				</div>
			)
		}
		case InputTypes.SINGLE_DATE: {
			const onChange = (date: Date): void => {
				updateValue(name, date)
			}
			return (
				<div className="d-flex flex-column width-100-per position-relative">

					<CalendarSingleInput
						date={value ? new Date(String(value)) : null}
						onChange={dateData ? dateData.setDate1 : onChange}
						// onChange={onChange}
						top={datePosition && datePosition === 'below' ? '-5rem' : '-30rem'}
						left={'0rem'}
						calanderId={calanderId ? calanderId : null}
						inputId={inputId ? inputId : null}
					/>
					{error && error.label ? <div className='ml-05 text-bold-600 font-small-2 text-danger min-height-1-rem max-height-1-rem'>{error.label}</div> : ''}
				</div>
			)
		}
		case InputTypes.SELECT: {
			return <SelectOption {...{ setSelectedSearch, selectedSearch, value, name, type, options, isMulti, selectAllOption, separator, error, updateValue, firstSelected, placeholder, defaultValue, disabled,  onChangeMultiValues, selectUnSelectOption }} />
		}
		case InputTypes.SELECT_ALL: {
			return <CustomSelect {...{ setSelectedSearch, selectedSearch, value, name, type, options, isMulti, selectAllOption, separator, error, updateValue, firstSelected, placeholder, defaultValue, disabled,  onChangeMultiValues, selectUnSelectOption }} />
		}
		default:
			const onChange = (event: React.FormEvent<HTMLInputElement>): void => {
				if (alllowsCharacters && alllowsCharacters.length) {
					let isValid = alllowsCharacters.map((v: any) => {
						if ((event.currentTarget.value.toString().length) || v && v(event.currentTarget.value)) return true
						return false
					})
					if (!isValid.includes(false)) {
						updateValue(name, event.currentTarget.value);
					}
				} else updateValue(name, event.currentTarget.value);
			}

			if (typeof value === 'string') {
				return (<div className="d-flex flex-column">
					<input
						disabled={disabled}
						id={`form-group-${type}-${name}`}
						onChange={onChange}
						autoComplete={'off'}
						className='form-group__field_text'
						name={name}
						type='text'
						value={value}
					/>
					{error && error.label ? <div className='ml-05 text-bold-600 font-small-2 text-danger min-height-1-rem max-height-1-rem'>{error.label}</div> : ''}
				</div>);
			}

			return null;
	}
}
