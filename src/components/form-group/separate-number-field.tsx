import * as React from 'react';
import Select from 'react-select';

interface Props {
	error?: any;
	disabled?: boolean;
	value: string | Date | any[] | number;
	name: string;
	selectValue: any;
	selectName: string;
	type: string;
	options: any;
	defaultValue?: any;
	updateValue: (name: string, val: string | Date | number | any[], props?: any) => void;
}

export const SeparateNumberField: React.FC<Props> = props => {
	const { value, name, type, options, updateValue, error, defaultValue, selectName, selectValue } = props;
	// const [valueOption, setOption] = React.useState(selectValue[0]);

	// const roundValue = (val: string | number) => {
	// 	const regex = new RegExp(options.join('|'), 'g');
	// 	if (typeof val === 'string') return val.replace(regex, '')
	// 	else return Math.round(val)
	// }

	const onChange = (event: React.FormEvent<HTMLInputElement>) => {
		// const rounded = roundValue(event.currentTarget.value)
		updateValue(name, event.currentTarget.value);
	};
	let preparedOptions = options.map((option: any) => {
		return { label: option.label, value: option.value };
	});

	const onChangeOption = (newOption: any) => {
		// setOption(newOption.value);
		// const rounded = roundValue(value)
		updateValue(selectName, [newOption], { splitedSelectField: true });
	};

	return (
		<div className="form-group__field_separate">
			<input
				id={`form-group-${type}-${name}`}
				onChange={onChange}
				className="form-group__field_number-separate"
				name={name}
				type="number"
				value={defaultValue ? Number(defaultValue) : value ? Number(value) : ''}
			/>
			<Select
				id={`form-group-${type}-${name}`}
				defaultValue={selectValue && selectValue.length ? selectValue : [{ value: '', label: '' }]}
				onChange={onChangeOption}
				components={{ IndicatorsContainer: props => <div {...props} /> }}
				className="form-group__field_select-separate"
				options={preparedOptions}
				styles={{
					control: provided => ({
						...provided,
						paddingRight: 5,
						border: error ? '1px solid red!important' : ''
					})
				}}
				theme={theme => ({
					...theme,
					borderRadius: '20px 0 0 20px',
					spacing: {
						baseUnit: 3.5,
						controlHeight: 30,
						menuGutter: 8
					},
					colors: {
						...theme.colors,
						primary: '#31baab'
					}
				})}
			/>
			<div
				className={`ml-05 text-bold-600 font-small-2 text-danger min-height-1-rem max-height-1-rem ${
					!error ? 'visibility-hidden' : ''
				}`}
			>
				{error && error.label ? error.label : 'bla'}
			</div>
		</div>
	);
};
