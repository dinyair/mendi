import * as React from 'react';

interface Props {
    readonly seconds : number;
    readonly onTimerEnd : () => void;
}

export const CountDownTimer: React.FunctionComponent<Props> = (props:Props) => {
    const { onTimerEnd,seconds } = props;
    const [countdownTimer, setCountdownTimer] = React.useState<number>(seconds);

    React.useEffect(() => {
		if (!countdownTimer) return;
		const timer = setInterval(() => {
			setCountdownTimer(countdownTimer - 1);
		}, 1000);
		return () => clearInterval(timer);
	}, [countdownTimer]);

	React.useEffect(() => {
		if (countdownTimer === 0) setTimeout(() => {
            onTimerEnd()	
        }, 100);		
	}, [countdownTimer])

    React.useEffect(() => {
        setCountdownTimer(seconds)
    }, [seconds])
    
    return (
        <>
            {countdownTimer}
        </>
    )
};

export default CountDownTimer;




 
/* Dont delete the code below!!! */

// const IconOption = (props: any) => (
//     <Option {...props}>
//         {props.data.label}

//         <img
//             className="ml-1"
//             src={`http://localhost:8080/assets/images/customers/nyocha.png`}
//             style={{ width: 36 }}
//             alt={props.data.label}
//         />
//     </Option>
// );

// const CustomSelectValue = (props:any) => (
//     <div className="d-flex justify-content-end">
//         {props.data.label}

//         <img
//             className="ml-1"
//             src={`http://localhost:8080/assets/images/customers/nyocha.png`}
//             style={{ width: 36 }}
//             alt={props.data.label}
//         />
//     </div>
// )