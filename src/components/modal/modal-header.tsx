import * as React from 'react';
import { X } from 'react-feather';

import './modal-header.scss';

interface Props {
	children: React.ReactNode;
	onClose: () => void;
	isClicked?:boolean;
	classNames?:string;
}

export const CustomModalHeader: React.FC<Props> = props => {
	const { children, onClose, isClicked, classNames } = props;

	return (
		<div className={`modal-header d-flex justify-content-between ${classNames}`}>
			<div className='modal-header__content'>
				{children}
			</div>
			{!isClicked &&
				<button onClick={onClose} className='modal-header__close-btn'>
					<X size={16} color='#1e1e20' />
				</button>}

		</div>
	);
}
