import * as React from 'react';
import { Button, Modal, ModalBody, ModalFooter, } from "reactstrap"
import { ModalHeader, } from "@src/components/modal";
import { Icon } from '../icon';
import { config } from '@src/config';
import { ModalInterface } from './interfaces';
import classnames from 'classnames';

interface Props {
	readonly modal: ModalInterface | undefined;
	readonly onKeyPress?: any;
}

export const GenericModals: React.FC<Props> = (props) => {
	const { modal , onKeyPress} = props

	const customizer = { direction: document.getElementsByTagName("html")[0].dir }
	const returnModal = (modal: ModalInterface) =>
	(<div onKeyPress={onKeyPress}>
		<Modal zIndex={500000} isOpen={modal.isOpen} toggle={modal.toggle} className={`modal-dialog-centered   ${modal.classNames ? modal.classNames : ''}`}>
		{modal && modal.hideHeader && modal.hideHeader === true ? null :
			<ModalHeader onClose={modal.toggle} classNames={modal.headerClassNames ? modal.headerClassNames : ''}>
				<div className={modal.classNames}>
					{modal.back &&
						<span className={classnames("mr-1", {
							"rotate-180": customizer.direction == 'ltr',
						})} onClick={modal.back}>
							<Icon src={'../' + config.iconsPath + "table/arrow-right.svg"} />
						</span>}
					{modal.header}
				</div>
			</ModalHeader>}
		<ModalBody className={`px-2 ${modal.bodyClassNames ? modal.bodyClassNames : ''}`}>
			{modal.body}
		</ModalBody>
		<ModalFooter className={modal.footerClassNames ? modal.footerClassNames : ''}>
			{modal.buttons && modal.buttons.length > 0 && modal.buttons.map((ele: any, index: number) => <Button key={index} className={`round ${ele.className ? ele.className : ''}`} color={ele.color} outline={ele.outline ? true : false} onClick={ele.onClick} disabled={ele.disabled}>{ele.label}</Button>)}
			{modal.footerChilds && modal.footerChilds.length > 0 && modal.footerChilds.map((ele: any) => ele)}
		</ModalFooter>
	</Modal>
	</div>)

	return modal ? returnModal(modal) : null;
}
