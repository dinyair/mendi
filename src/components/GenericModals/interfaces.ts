
export interface ModalInterface {
	isOpen: boolean;
	toggle:any;
	header: any;
	body: any;
	buttons?:any;
	footerChilds?:any;
	classNames?:string;
	back?:any;
	footerClassNames?:string;
	bodyClassNames?:string;
	headerClassNames?:string
	hideHeader ?:boolean;
}

export enum ModalTypes {
    none = 'none',
    add = 'add',
    delete = 'delete',
    edit = 'edit',
    export  = 'export',
    import = 'import',
	error = 'error',
	info = 'info',
}

export interface ModalsInterface {
	none:ModalInterface[] | [],
	add: ModalInterface[] |[],
	delete: ModalInterface[] | [],
	edit: ModalInterface[] | [],
	export : ModalInterface[] | [],
	import:ModalInterface[] | [],
	error:ModalInterface[] | [],
	info:ModalInterface[] | []
}