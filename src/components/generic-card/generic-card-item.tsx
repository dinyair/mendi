import * as React from 'react';
import { Option } from '@src/containers/catalog/categories/services/interfaces';
import { ActionButtons } from './action-buttons';
import './generic-card.scss';
import { useIntl } from "react-intl";

interface Props {
	option: any;
	type: string;
	onSendToDone?: (optionId: string, type: string) => void;
	onSendDraft?: (optionId: string, type: string) => void;
	backToStart?: (optionId: string, type: string) => void;
	editOption?: (option: any) => void;
	editOrder?: (option: any) => void;
	displayId?:boolean;
	optionId?: any
	optionName?: any
	
	buttons:any;

	chosenSupplierId?: any
	chosenBranch?: any

	setDisplayOrdersBySupplier?: (val: any) => void;
	searchOrders?: () => void;
	onOptionDelete: (optionId: any) => void;
	handlePrintDoneOrder?: (option: any) => void;

}

export const GenericCardItem: React.FC<Props> = props => {
	const {  option, type, displayId,onSendToDone, onSendDraft, backToStart, buttons,   editOrder, chosenSupplierId, chosenBranch, setDisplayOrdersBySupplier, searchOrders, handlePrintDoneOrder , onOptionDelete, } = props;
	const [isOpenModal, setStateModal] = React.useState(false)
	const [isDeleteModalOpen, setDeleteModalOpen] = React.useState(false)
	const [isEditModalOpen, setEditModalOpen] = React.useState(false)
	const { formatMessage } = useIntl();

	// const toggleModal = () => {
	// 	setStateModal(prevValue => !prevValue);
	// }
	// console.log('option', option)
	const toggleModalDelete = () => {
		setDeleteModalOpen(prevValue => !prevValue);
	}

	const toggleModalEdit = () => {
		setEditModalOpen(prevValue => !prevValue);
	}

	// const sendToDone = () => {
	// 	toggleModal();
	// 	if (onSendToDone) {
	// 		onSendToDone(option.id, type);
	// 	}
	// }

	// const sendToDraft = () => {
	// 	toggleModal();
	// 	if (onSendDraft) {
	// 		onSendDraft(option.id, type);
	// 	}
	// }

	const deleteOption = (id:any) => {
		console.log('iddddddddd', id)
		onOptionDelete(option.Id)
		toggleModalDelete();
	}

	const editOption = (id:any) => {
		console.log('iddddddddd', id)
		// onOptionDelete(option.Id)
		toggleModalEdit();
	}

	const onEdit = () => {
		if (editOrder) {
			editOrder(option);
		}
	}

	return (
		<li className='generic-card__item'>
			<div className='generic-card__text elipsis'>
				{displayId && <span className='generic-card__value mr-2 min-width-3-rem'>{option.id }</span>}
				
				<span className='generic-card__value elipsis'>{option.value}</span>
				{option.ordersAmount && <span className='stock-card__price elipsis'>({option.ordersAmount})</span>}
				{option.OrderNum && <span className='stock-card__price elipsis'>{formatMessage({ id: 'orderNum.' })} {option.OrderNum}</span>}
			</div>
			<div className='generic-card__action-buttons'>
				<ActionButtons {...{
					buttons ,
					option,
					setDisplayOrdersBySupplier,
					type,
					isOpenModal,
					toggleModal:()=>{},
					deleteOption,
					editOption,
					toggleModalDelete,
					toggleModalEdit,
					isDeleteModalOpen,
					isEditModalOpen,
					handlePrintDoneOrder,
					isDeleteDraftModelOpen: isDeleteModalOpen,
					sendToDone:()=>{},
					sendToDraft:()=>{},
				}} />
			</div>
		</li>
	)
}
