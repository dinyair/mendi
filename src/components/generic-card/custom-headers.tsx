import * as React from 'react';
import { Icon } from '@components';
import { config } from '@src/config'
import { Sorting } from '@src/helpers/helpers';



interface Props {
    options: any;
    onSortChange: (options: any) => void
    title: string;
    field: string;
    fieldType: string;
}

export const CustomHeader: React.FC<Props> = props => {
    const [ascSort, setAscSort] = React.useState(false);
    const [noSort, setNoSort] = React.useState(true);
    const [sortDisplay, setSortDisplay] = React.useState(false)


    const { onSortChange, title, options, field, fieldType } = props;

    const [sort, setSort] = React.useState({ field, type: fieldType, asc: ascSort, times: 0 })

    const onSortChanged = () => {
        let _asc: boolean = false;
        let _noSort: boolean = false;

        if (noSort) {
            // console.log('no mode')
            _asc = true;
            _noSort = false;
        } else if (ascSort) {
            // console.log('asc mode')
            _asc = false;
            _noSort = false;
        } else {
            // console.log('desc mode')
            _asc = false;
            _noSort = true;
        }

        setAscSort(_asc)
        setNoSort(_noSort)
        // console.log('sort changed')

        setTimeout(() => {
            const newSort = { field, type: fieldType, asc: _asc, times: sort.times === 0 ? 1 : sort.times === 1 ? 2 : 0 }
            // console.log(newSort)
            setSort(newSort)
            if (newSort.times === 0) {
                onSortChange(undefined)
            } else {
                let data = Sorting(options, newSort)
                onSortChange(data)
            }
        }, 1);

    };


    const displaySortIcon = () => {
        setSortDisplay(true);
    }
    const hideSortIcon = () => {
        setTimeout(() => {
            setSortDisplay(false)
        }, 3000);
    }
    const renderSortIcon = (noSort: any, ascSort: any) => {
        let sortStyle: any;
        if (sortDisplay) sortStyle = 'opacity-1'
        else sortStyle = 'opacity-0'

        const columnTitle = (<b className="customHeaderLabel">{title}</b>)

        const sortDiv = (sortType: any, icon: any, classN: any) => {
            const classFinal = `${classN} ${sortStyle}`
            return (
                <div className='headerAndSort d-flex align-items-center'  onClick={onSortChanged}>
                    {columnTitle}
                    <Icon className={classFinal} src={'../../../' + config.iconsPath + icon} style={{ height: '0.5rem', width: '0.5rem' }} />
                </div>
            )
        }
        switch (noSort) {
            case true:
                return (
                    sortDiv('', "table/sort-disabled.svg", '')
                )
            case false:
                return (
                    ascSort ? sortDiv('asc', "table/sort.svg", 'rotate-180') : sortDiv('desc', "table/sort.svg", '')
                )
        }
    }



    return (
        <div className='columnSortTitle d-flex' onMouseEnter={displaySortIcon} onMouseLeave={hideSortIcon}>
            <div className='align-self-center headerTitle'>
                {renderSortIcon(noSort, ascSort)}
            </div>
        </div>
    );
};
