import * as React from 'react';
import { Option } from '@src/containers/catalog/categories/services/interfaces';
import { GenericCardItem } from "./generic-card-item";
import './generic-card.scss';
import InputText from '../input-text';
import { useIntl } from 'react-intl';
import { CustomHeader } from './custom-headers';
// import { Sorting } from '@src/helpers/helpers'

interface Props {
	title: string;
	options: any;
	optionsLength? : number;
	type: string;
	onChangeValue?: (value: string) => void;
	handler?: () => void;
	sendToDraft?: (optionId: string, type: string) => void;
	sendToDone?: (optionId: string, type: string) => void;
	backToStart?: (optionId: string, type: string) => void;
	editOrder?: (options: any) => void;
	chosenSupplierId?: any
	chosenBranch?: any
	setDisplayOrdersBySupplier?: (val: any) => void;
	searchOrders?: () => void;

	buttons: any;

	displaySearchBar?: boolean;
	searchProp?: any;

	allowSorting?: boolean;
	displayId?: boolean;
	headers?: any;
}

export const GenericCard: React.FC<Props> = props => {
	const { title,  displaySearchBar, allowSorting, headers, buttons, searchProp, displayId, options, optionsLength, type, sendToDone, sendToDraft, backToStart, editOrder, chosenSupplierId, chosenBranch, setDisplayOrdersBySupplier, searchOrders } = props;

	const [searchInput, setSearchInput] = React.useState<string | null>(null)
	const [rows, setRows] = React.useState<any>([])
	const [originalOrderRows, setoriginalOrderRows] = React.useState<any>([])
	const [limitedRows, setLimitedRows] = React.useState<number>(10)
	const freeSearchInputRef = React.useRef<any>();

	const { formatMessage } = useIntl();

	React.useEffect(() => {
		setRows(options)
		setoriginalOrderRows(options)
	}, [options])

	const onOptionDelete = (optionId: any) => {
		// console.log(optionId)
		const newRows = returnFilteredState(optionId, options)
		setRows(newRows)
	}

	const returnFilteredState = (id: number, state: any) => [...state].filter(ele => ele.id !== id);

	function onFilterTextBoxChanged() {
		setSearchInput(freeSearchInputRef.current.value)
	}

	const handleScroll = (e: any) => {
		const element = e.target
		// console.log(element.scrollHeight)
		// console.log(element.scrollTop)
		// console.log(element.clientHeight)
		if (element.scrollHeight - Math.ceil(element.scrollTop) === element.clientHeight || element.scrollHeight - Math.floor(element.scrollTop) === element.clientHeight) {
			setLimitedRows(limitedRows + 10)
		}
	}


	const onSortChange = (data: any) => {
		setRows([])
		setTimeout(() => {
			data ? setRows(data) : setRows(originalOrderRows)
		}, 1);
	}

	return (
		<>
			{limitedRows && <div className={`mb-2 w-100 p-2 generic-card generic-card_${type} min-width-25-rem ml-1 mr-1`}>
				<div className={`d-flex justify-content-between height-2-2-rem ${!allowSorting && 'mb-1'}`}>
					<span className='generic-card__title'>{`${title} (${optionsLength? optionsLength : options.length})`}</span>
					{displaySearchBar &&
						<div className="max-width-10-rem">
							<div><InputText searchIcon id={title} onChange={onFilterTextBoxChanged} placeholder={formatMessage({ id: 'search' })} inputRef={freeSearchInputRef} /></div>
						</div>}

				</div>

				{allowSorting &&
					<li className="generic-card__item mb-1 mt-1" >
						<div className='generic-card__text'>
							{headers.map((ele: any) =>
								<span className={`generic-card__value ${ele.classNames}`}>
									<CustomHeader title={ele.title} onSortChange={onSortChange} options={[...rows]} field={ele.field} fieldType={ele.fieldType === 'number' ? ele.fieldType : 'custom_header'} />
								</span>
							)}
						</div>
					</li>}


				<div className='generic-card__list max-height-50vh overflow-y-scroll overflow-x-hidden' onScroll={handleScroll} >
					{
						(freeSearchInputRef.current && freeSearchInputRef.current.value && searchProp ? rows.filter((ele: any) => ele[searchProp].includes(freeSearchInputRef.current.value)) : rows).map((option: any, index: number) => (
							limitedRows >= index ?
								<GenericCardItem key={`${option.id},${option.value}`} {...{
									chosenSupplierId,
									chosenBranch,
									setDisplayOrdersBySupplier,
									searchOrders,
									option,
									type,
									onOptionDelete,
									displayId,
									onSendDraft: sendToDraft,
									onSendToDone: sendToDone,
									backToStart,
									editOrder,
									buttons,
								}} />
								: null
						))}
				</div>
			</div>}

		</>
	)
}




















