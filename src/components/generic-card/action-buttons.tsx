import * as React from 'react';
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap'
import { Icon } from '@components';
import { config } from '@src/config';
import './generic-card.scss';
import { useIntl } from "react-intl";
import * as stockTypes from "../../containers/stock/orders/services/reducer"
import { getSuggestedOrder } from '@src/containers/order/initial-state'
import * as moment from 'moment';

import { sendOrderToDraft, addOrder, getAspkaDate } from '@src/containers/order/initial-state'
import { deleteOrderByOrderId, deleteOrderDateBranchSupply } from '@src/containers/stock/orders/initial-state'
import { getLogo } from '@src/containers/stock/orders/initial-state'
import { useReactToPrint } from 'react-to-print';
import { Spinner } from 'reactstrap'
import { CATALOG_TYPES } from '@src/containers/catalog/categories/services/enums';
import { useDispatch, useSelector } from 'react-redux';
import { RootStore } from '@src/store';


interface Props {
	isOpenModal: boolean;
	type: string;
	deleteOption: (optionId: any) => void;
	editOption: (optionId: any) => void;
	option?: any
	toggleModalDelete?: () => void;
	toggleModalEdit?: () => void;
	isDeleteModalOpen: boolean
	isEditModalOpen: boolean
	buttons: any;


	toggleModal?: () => void;
	sendToDone?: () => void;
	sendToDraft?: () => void;
	onEdit?: () => void;
	chosenSupplierId?: any
	chosenBranch?: any
	setDisplayOrdersBySupplier?: (val: any) => void;
	searchOrders?: () => void;
	isDeleteDraftModelOpen: boolean
}

export const ActionButtons: React.FC<Props> = props => {
	const { formatMessage } = useIntl();
	let userBranches:any = useSelector((state: RootStore) => state._userBranches);
	
	const { option, type,
		isDeleteModalOpen, isEditModalOpen, toggleModalDelete, toggleModalEdit, deleteOption, editOption,
		toggleModal, isOpenModal, onEdit, buttons, chosenSupplierId,
		chosenBranch, setDisplayOrdersBySupplier, searchOrders,
		isDeleteDraftModelOpen, }
		= props;

	const [isClicked, setIsClicked] = React.useState<any>(false)
	const [printData, setPrintData] = React.useState<any[]>([])



	const handleDelete = async () => {
		if (deleteOption && option) {
			deleteOption(option.id)
		}
	}

	const handleEdit = async () => {
		if (editOption && option) {
			editOption(option.id)
		}
	}


	const sendOrderToDone = async () => {
		if (searchOrders && setDisplayOrdersBySupplier && chosenBranch && option) {
			setIsClicked(true)
			let rec = {
				branchId: chosenBranch,
				sapakId: option.SapakId,
				date1: moment(new Date()).format('YYYY-MM-DD')
			}
			const suggestedOrderJson = await getSuggestedOrder(rec)
			const aspkaDate = await getAspkaDate(rec)

			let itemsToOrder = suggestedOrderJson.map((i: any) => {
				let AmountMaraz = i.amount_yit_0
				let BranchId = chosenBranch
				let AmountOrderOrg = i.AmountOrder
				let AmountOrder = i.AmountOrder
				let averageDaySum = i.sumofday
				let Ariza = i.ariza
				let BarCode = i.BarCode
				let lastyitra = i.lastBalance
				let madaf = i.shelfData
				let newyitra = i.lastBalance
				let sales = i.averageDay
				let SapakId = chosenSupplierId
				let GroupId = i.GroupId
				let OrderDate = moment(new Date()).format('YYYY-MM-DD')
				let AspakaDate = aspkaDate
				return {
					AmountMaraz, BranchId, AmountOrderOrg, AmountOrder, averageDaySum, Ariza, BarCode, lastyitra, madaf, newyitra, sales
					, SapakId, GroupId, OrderDate, AspakaDate
				}
			})
			await sendOrderToDraft(itemsToOrder)
			let rec1 = {
				SapakId: chosenSupplierId,
				OrderDate: moment(new Date()).format('YYYY-MM-DD'),
				BranchId: userBranches.length === 1 ? userBranches[0].BranchId : chosenBranch,
				AspakaDate: aspkaDate
			}
			await addOrder(rec1)
			setIsClicked(false)
			setDisplayOrdersBySupplier(false)
			searchOrders()
		}
	}
	
	const deleteOrder = async () => {
		if (searchOrders && option) {
			setIsClicked(true)
			await deleteOrderByOrderId(option.OrderNum)
			setIsClicked(false)
			searchOrders()
		}
	}
	const deleteDraftOrder = async () => {
		if (searchOrders && option) {
			setIsClicked(true)
			let rec = {
				SapakId: option.SapakId,
				OrderDate: moment(new Date()).format('YYYY-MM-DD'),
				BranchId: chosenBranch,
			}
			await deleteOrderDateBranchSupply(rec)
			setIsClicked(false)
			searchOrders()
		}
	}
	const componentRef = React.useRef(null);
	const onBeforeGetContentResolve = React.useRef<(() => void) | null>(null);
	const [loading, setLoading] = React.useState(false);

	const handleAfterPrint = React.useCallback(() => {
	}, []);

	const handleBeforePrint = React.useCallback(() => {
	}, []);

	const handleOnBeforeGetContent = React.useCallback(() => {
		setLoading(true);
		return new Promise<void>((resolve) => {
			onBeforeGetContentResolve.current = resolve;
			setLoading(false);
			resolve();
		});
	}, [setLoading]);

	const reactToPrintContent = React.useCallback(() => {
		return componentRef.current;
	}, [componentRef.current]);

	const handlePrint = useReactToPrint({
		content: reactToPrintContent,
		documentTitle: formatMessage({ id: 'sapak-orders' }),
		onBeforeGetContent: handleOnBeforeGetContent,
		onBeforePrint: handleBeforePrint,
		onAfterPrint: handleAfterPrint,
		removeAfterPrint: true,
	});
	const printDoneOrder = async () => {
		let logo = await getLogo()
		let marazSum: number = 0
		let singleSum: number = 0
		option.subOrder.forEach((order: any) => {
			if(order.AmountOrder>0){
				marazSum += order.AmountMaraz>0 ?  order.AmountMaraz : 0
				singleSum += order.Ariza>0&&order.AmountMaraz>0 ? (order.Ariza*order.AmountMaraz) : order.AmountOrder
				}

			// if (order.AmountMaraz && order.AmountMaraz % 1 == 0) marazSum += order.AmountMaraz
			// if (order.AmountMaraz && order.AmountMaraz % 1 == 0 && order.Ariza) singleSum += order.AmountMaraz * order.Ariza
			// else if (order.AmountMaraz && order.AmountMaraz % 1 != 0) singleSum += order.AmountMaraz
		})
		option['marazSum'] = marazSum.toFixed(0)
		option['singleSum'] = singleSum.toFixed(0)
		option['logo'] = `https://grid.algoretail.co.il/assets/images/${logo}`

		setPrintData(option)
		setTimeout(() => { if (handlePrint) handlePrint() }, 20);
	}


	const deleteEditButtons = (
		<>
			{buttons.map((ele: any, index: number) => ele && (
				<button key={`${option.id},${option.value},${index}`} onClick={() => ele.action(option, type)} className={`generic-${ele.class}`}>
					<Icon src={`${config.iconsPath}general/${ele.icon}.svg`} />
				</button>
			))}
			
		</>
	)

	switch (type) {
		case CATALOG_TYPES.DIVISIONS:
			return deleteEditButtons;
		case CATALOG_TYPES.GROUPS:
			return deleteEditButtons;
		case CATALOG_TYPES.SUBGROUPS:
			return deleteEditButtons;
		case CATALOG_TYPES.LAYOUTS:
			return deleteEditButtons;
		case CATALOG_TYPES.MODELS:
			return deleteEditButtons;
		case CATALOG_TYPES.SEGMENTS:
			return deleteEditButtons;
		case CATALOG_TYPES.SERIES:
			return deleteEditButtons;

		case stockTypes.TYPES.NEW: {
			return (
				<>
					<button onClick={onEdit} className='stock-card__edit'>
						<Icon src={config.iconsPath + "general/edit-icon.svg"} />
					</button>
					<button onClick={toggleModal} className='stock-card__send'>
						<Icon src={config.iconsPath + "general/send-icon.svg"} />
					</button>
					<Modal isOpen={isOpenModal} toggle={toggleModal} className='modal-dialog-centered'>
						<ModalHeader isClicked={isClicked}>{formatMessage({ id: 'warning' })}</ModalHeader>
						<ModalBody>
							{formatMessage({ id: 'noValuePromoMessage' })}
						</ModalBody>
						<ModalFooter>
							<Button className='round' color='primary' disabled={isClicked} onClick={sendOrderToDone}>{formatMessage({ id: 'sendAnyWay' })}</Button>
							<Button className='round' color='primary' disabled={isClicked} outline onClick={onEdit}>{formatMessage({ id: 'goBackToFix' })}</Button>
							{isClicked ? <Spinner /> : null}
						</ModalFooter>
					</Modal>
				</>
			)
		}
		case stockTypes.TYPES.PENDING: {
			return (
				<>
					<button onClick={onEdit} className='stock-card__edit'>
						<Icon src={config.iconsPath + "general/edit-icon.svg"} />
					</button>
					<button onClick={toggleModal} className='stock-card__send'>
						<Icon src={config.iconsPath + "general/send-icon.svg"} />
					</button>
					<button onClick={toggleModalDelete} className='stock-card__trash'>
						<Icon src={config.iconsPath + "general/delete-icon.svg"} />
					</button>
					<Modal isOpen={isOpenModal} toggle={toggleModal} className='modal-dialog-centered'>
						<ModalHeader isClicked={isClicked}>{formatMessage({ id: 'warning' })}</ModalHeader>
						<ModalBody>
							{formatMessage({ id: 'noValuePromoMessage' })}
						</ModalBody>
						<ModalFooter>
							<Button className='round' color='primary' disabled={isClicked} onClick={sendOrderToDone}>{formatMessage({ id: 'sendAnyWay' })}</Button>
							<Button className='round' color='primary' disabled={isClicked} outline onClick={onEdit}>{formatMessage({ id: 'goBackToFix' })}</Button>
							{isClicked ? <Spinner /> : null}
						</ModalFooter>
					</Modal>
					<Modal isOpen={isDeleteDraftModelOpen} toggle={toggleModalDelete} className='modal-dialog-centered'>
						<ModalHeader isClicked={isClicked}>{formatMessage({ id: 'warning' })}</ModalHeader>
						<ModalBody>
							{formatMessage({ id: 'deleteDraftMsg' })}
						</ModalBody>
						<ModalFooter>
							<Button className='round' color='primary' disabled={isClicked} onClick={deleteDraftOrder}>{formatMessage({ id: 'delete' })}</Button>
							<Button className='round' color='primary' disabled={isClicked} outline onClick={toggleModalDelete}>{formatMessage({ id: 'cancel' })}</Button>
							{isClicked ? <Spinner /> : null}
						</ModalFooter>
					</Modal>
				</>
			)
		}
		case stockTypes.TYPES.DELETE: {
			return (
				<>
					<button className='stock-card__print' onClick={printDoneOrder}>
						<Icon src={config.iconsPath + "general/print-icon.svg"} />
					</button>
					<button onClick={toggleModal} className='stock-card__trash'>
						<Icon src={config.iconsPath + "general/delete-icon.svg"} />
					</button>
					<Modal isOpen={isOpenModal} toggle={toggleModal} className='modal-dialog-centered'>
						<ModalHeader isClicked={isClicked} >{formatMessage({ id: 'warning' })}</ModalHeader>
						<ModalBody>
							{formatMessage({ id: 'deleteOrderMsg' })}
						</ModalBody>
						<ModalFooter>
							<Button className='round' color='primary' disabled={isClicked} onClick={deleteOrder}>{formatMessage({ id: 'delete' })}</Button>
							<Button className='round' color='primary' disabled={isClicked} outline onClick={toggleModal}>{formatMessage({ id: 'cancel' })}</Button>
							{isClicked ? <Spinner /> : null}
						</ModalFooter>
					</Modal>
				</>
			)


		}
		default: return null;
	}

}


