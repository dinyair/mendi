import { config } from '@src/config';
import * as React from 'react';
import { Icon } from '..';


interface Props {
	childs: Array<object>;
}

export const HortzionalScroller: React.FC<Props> = props => {
	const ref = React.useRef() as React.MutableRefObject<HTMLDivElement>;

	const [displayRightIcon, setDisplayRightIcon] = React.useState(false)
	const [displayLeftIcon, setDisplayLeftIcon] = React.useState(true)
	const [direction, setDirection] = React.useState(document.dir);
	const [isScrolling, setIsScrolling] = React.useState<boolean>(false);

	React.useEffect(() => {
		setDirection(document.dir)
		console.log(document.dir)
	}, [document.dir])

	const scroll = async (scrollOffset: number, scrollRight: boolean, speed:number) => {
		// console.log(scrollOffset)
		// console.log(scrollRight)
		// console.log(ref.current.scrollLeft)

		for (let index = 0; index < Math.abs(scrollOffset); index++) {
			await new Promise(resolve => setTimeout(resolve, 0));
			scrollRight ? ref.current.scrollLeft += speed : ref.current.scrollLeft -= speed;
		}
	};


	const handleScroll = (e: React.ChangeEvent<any>) => {
		const element = e.target
		const { scrollWidth, scrollLeft, offsetWidth } = element
		const calculate = Math.abs(scrollLeft - offsetWidth)
		/* Calculations: */
		// console.log({ scrollWidth })
		// console.log({ scrollLeft })
		// console.log({ offsetWidth })
		// console.log({ calculate })
		// console.log({ firstCheck: Math.abs(scrollLeft) })
		// console.log({ left: Math.abs(calculate - scrollWidth) })
		// console.log({ right: Math.abs(calculate - offsetWidth) })

		if (Math.abs(scrollLeft) >= 0 && direction === 'rtl') {
			if (Math.abs(calculate - scrollWidth) < 3) {
				// console.log('the left end')
				setDisplayLeftIcon(false)
				setDisplayRightIcon(true)
			} else if (Math.abs(calculate - offsetWidth) < 3) {
				// console.log('the right end')
				setDisplayRightIcon(false)
				setDisplayLeftIcon(true)
			} else {
				setDisplayLeftIcon(true)
				setDisplayRightIcon(true)
			}
		} else if (direction === 'ltr'){
			if (Math.abs(scrollWidth - scrollLeft - offsetWidth) < 3) {
				// console.log('the right end')
				setDisplayLeftIcon(false)
				setDisplayRightIcon(true)
			} else if (scrollLeft < 3) {
				// console.log('the left end')
				setDisplayRightIcon(false)
				setDisplayLeftIcon(true)
			} else {
				setDisplayLeftIcon(true)
				setDisplayRightIcon(true)
			}
		}
	}


	return (
		<div className="d-flex align-items-center">
			{displayRightIcon &&
				<div onClick={async () => {
					if (!isScrolling) {
						if (!displayLeftIcon) {
							setIsScrolling(true)
							await scroll(direction === 'rtl' ? -400 : 400, direction === 'rtl' ? true : false, 8)
							setIsScrolling(false)
						} else {
							setIsScrolling(true)
							await scroll(direction === 'rtl' ? -94.5 : 94.5, direction === 'rtl' ? true : false,4)
							setIsScrolling(false)
						}
					}
				}} >
					<Icon className={`${direction === 'rtl' ? 'rotate-180' : ''} cursor-pointer`} src={`${config.iconsPath}general/arrow-left.svg`} />
				</div>
			}

			<div onScroll={handleScroll} ref={ref} className="mb-1 flex-nowrap d-flex overflow-hidden">
				{props.childs.map((ele: any) => ele)}
			</div>

			{displayLeftIcon &&
				<div onClick={async () => {
					if (!isScrolling) {
						setIsScrolling(true)
						await scroll(direction === 'rtl' ? 94.5 : -94.5, direction === 'rtl' ? false : true,4)
						setIsScrolling(false)
					}
				}}>
					<Icon className={`${direction === 'ltr' ? 'rotate-180' : ''} cursor-pointer`} src={`${config.iconsPath}general/arrow-left.svg`} />
				</div>
			}
		</div>
	)
}