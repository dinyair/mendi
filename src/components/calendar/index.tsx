import * as React from 'react';
import classnames from 'classnames';
import { Calendar } from 'react-date-range';
import Flatpickr from "react-flatpickr";
import { FormattedMessage, useIntl } from 'react-intl';
import { config } from '@src/config'
import { useOutsideClick } from '@src/utilities';
import * as moment from 'moment';
import { Icon } from '@components';
import './index.scss';
import { min } from 'moment';
import Placeholder from 'react-select/src/components/Placeholder';

interface Props {
	onChange: (item: Date) => void;
	date: any;
	placeholder?: any;
	className?: string;
	onOutsideClick?: () => void;
	minDate?: any;
	readonly flatpickr?: boolean
	readonly range?: boolean
	readonly maxDate?: any
}

export const CalendarComponent: React.FC<Props> = (props: Props) => {

	let { onChange, onOutsideClick, date, flatpickr, range, placeholder, className, minDate, maxDate } = props;
	const classes = [];
	const { formatMessage } = useIntl();
	const ref: React.MutableRefObject<any> = React.useRef(null);
	const fpRef: any = React.useRef(null);
	useOutsideClick(ref, onOutsideClick!);

	if (className) {
		classes.push(className);
	}
	if (!minDate) minDate = new Date('1/1/2020')
	if (!maxDate) maxDate = new Date('1/1/2030')

	return (
		<div ref={ref} >
			{flatpickr ? <>
				{date && !date.length && placeholder && (`${formatMessage({ id: 'select' })} ${formatMessage({ id: 'date' })}`)}
				<Flatpickr
					ref={fpRef}
					id="flatPicker"
					placeholder={placeholder ? placeholder : ''}
					className={classnames(className ? className : '', {
						'position-absolute': !date.length,
						'position-left-0': !date.length
					})}
					value={date}
					options={range ? {
						prevArrow: `<img src=${config.iconsPath + '/calander/arrow-prev.png'}/>`,
						nextArrow: `<img class='rotate-180' src=${config.iconsPath + '/calander/arrow-prev.png'}/>`,
						showMonths: 1,
						locale: 'he',
						mode: "range",
						maxDate: "today",
						dateFormat: "d-m-Y",
						inline: true
					} : {

						locale: 'he',

					}}
					onChange={(date: any): void => { onChange(date) }}
				/>
			</>
				:
				<Calendar
					minDate={minDate}
					// locale={'Arabic'}
					maxDate={maxDate}
					onChange={(date: Date): void => { onChange(date) }}
					date={date}
					className={classnames(className ? className : '')}
				/>
			}
		</div>
	);
};

interface CalendarDoubleProps {
	setDate1: any;
	setDate2: any;
	date1: any;
	date2: any;
	left?: string;
	top: string;
	onlyPopUp?: boolean
	displayDatePickerOutside?: any
	setDisplayDatePickerOutside?: any
	dontClose?: boolean
	calanderId?: any
	inputId?: any
	onChange?:any
	name?:string
}
export const CalendarDoubleInput: React.FC<CalendarDoubleProps> = ({ calanderId,onChange,name, inputId, setDate1, setDate2, date1, date2, left, top, onlyPopUp, displayDatePickerOutside, setDisplayDatePickerOutside, dontClose }) => {
	const { formatMessage } = useIntl();

	const [displayDatePicker, setDisplayDatePicker] = React.useState<any>(onlyPopUp ? true : false)
	const [niceDate, setNiceDate] = React.useState<any>()

	const whatDayLetter = (date: any) => {

		const dateString = new Date(date)
		let day: any

		switch (dateString.getDay()) {
			case 0:
				day = formatMessage({ id: 'Sunday' })
				break;
			case 1:
				day = formatMessage({ id: 'Monday' })
				break;
			case 2:
				day = formatMessage({ id: 'Tuesday' })
				break;
			case 3:
				day = formatMessage({ id: 'Wednesday' })
				break;
			case 4:
				day = formatMessage({ id: 'Thursday' })
				break;
			case 5:
				day = formatMessage({ id: 'Friday' })
				break;
			case 6:
				day = formatMessage({ id: 'Saturday' })
				break;
		}
		return day
	}
	const whatMonth = (date: any) => {

		const dateString = new Date(date)
		let month: any

		switch (dateString.getMonth()) {
			case 0:
				month = formatMessage({ id: 'January' })
				break;
			case 1:
				month = formatMessage({ id: 'February' })
				break;
			case 2:
				month = formatMessage({ id: 'March' })
				break;
			case 3:
				month = formatMessage({ id: 'April' })
				break;
			case 4:
				month = formatMessage({ id: 'May' })
				break;
			case 5:
				month = formatMessage({ id: 'June' })
				break;
			case 6:
				month = formatMessage({ id: 'July' })
				break;
			case 7:
				month = formatMessage({ id: 'August' })
				break;
			case 8:
				month = formatMessage({ id: 'September' })
				break;
			case 9:
				month = formatMessage({ id: 'October' })
				break;
			case 10:
				month = formatMessage({ id: 'Nobember' })
				break;
			case 11:
				month = formatMessage({ id: 'December' })
				break;
		}
		return month
	}
	const displayNiceDate = (setDate: any, date1: any, date2: any) => {

		if (date1 && date2) {
			const dayDate1 = whatDayLetter(date1)
			const monthDate1 = whatMonth(date1)
			const dateDate1 = date1.getDate()

			const dayDate2 = whatDayLetter(date2)
			const monthDate2 = whatMonth(date2)
			const dateDate2 = date2.getDate()
			let niceDate1 = `${formatMessage({ id: 'singleDay' })} ${dayDate1}׳, ${dateDate1} ${formatMessage({ id: 'in' })}${monthDate1}`
			let niceDate2 = `${formatMessage({ id: 'singleDay' })} ${dayDate2}׳, ${dateDate2} ${formatMessage({ id: 'in' })}${monthDate2}`

			if (document.getElementsByTagName("html")[0].dir === 'ltr') {
				niceDate1 = `${dayDate1}, ${dateDate1} ${monthDate1}`
				niceDate2 = `${dayDate2}, ${dateDate2} ${monthDate2}`
			}
			let fullNiceDate = `${niceDate1} - ${niceDate2}`

			setDate(fullNiceDate)
		}
	}

	React.useEffect(() => {
		displayNiceDate(setNiceDate, date1 ? date1 : null, date2 ? date2 : null)
	}, [date1])
	React.useEffect(() => {
		displayNiceDate(setNiceDate, date1 ? date1 : null, date2 ? date2 : null)
	}, [date2])

	const openDatePicker = () => {
		setDisplayDatePicker(true)
	}
	document.addEventListener('click', function (event: any) {
		const specifiedElement = document.getElementById(calanderId ? calanderId : 'calendarId');
		const inputElement = document.getElementById(inputId ? inputId : 'inputId');
		if (specifiedElement && inputElement && !dontClose) {
			const isClickInside = specifiedElement.contains(event.target);
			const isClickInsideInput = inputElement.contains(event.target)
			if (!isClickInside && !isClickInsideInput) {
				setDisplayDatePicker(false)
				if (displayDatePickerOutside && setDisplayDatePickerOutside) setDisplayDatePickerOutside(false)
			}
		}
	});
	const customizer = { direction: document.getElementsByTagName("html")[0].dir }
	return (
		<div>
			{onlyPopUp ? null :
				<div id={inputId ? inputId : 'inputId'} onClick={openDatePicker} className='date__inputBar d-flex justify-content-evenly align-items-center cursor-pointer round px-1 bg-white font-medium-1 text-bold-600'>
					<div className='m-auto'>
						{date1 && date2 ?
							<div>
								{moment(date1).format('DD.MM.YY')} - {moment(date2).format('DD.MM.YY')}
							</div>
							:
							<>
								{date1 ? <div>{moment(date1).format('DD.MM.YY')} - {moment(new Date()).format('DD.MM.YY')}</div> :
									<>
										{date2
											?
											<div>
												{moment(new Date(date2)).subtract(1, 'year').format('DD.MM.YY')} - {moment(date2).format('DD.MM.YY')}
											</div>
											:
											<div>{formatMessage({ id: 'date_choose' })}</div>
										}
									</>
								}
							</>
						}
					</div>
					<Icon className='align-self-center date__icon' src={config.iconsPath + "table/date.svg"} />
				</div>
			}
			{
				displayDatePicker || displayDatePickerOutside ?
					<div id={calanderId ? calanderId : "calendarId"} className={classnames("date__container d-flex justify-content-evenly", {
						'date__onlyPopUp': onlyPopUp
					})} style={customizer.direction === 'rtl' ? { top: top, left: left }: { top: top, right: left }}>
						<div>
							<p className='date__title date__title--right'>{formatMessage({ id: 'date_start' })}</p>
							<CalendarComponent
								className="c-calendar c"
								onChange={(date: any): void => {
									
									// if (date2) {
									// 	setDisplayDatePicker(false)
									// 	if (displayDatePickerOutside && setDisplayDatePickerOutside) setDisplayDatePickerOutside(false)
									// }
									if (date > date2) {
										setDate1(date2)
										setDate2(date)
										if(onChange)onChange({setDate1,setDate2,date2:date,date1:date2})

									}else{
										setDate1(date)
										if(onChange)onChange({setDate1,setDate2,date2:date2,date1:date})

									}
								}}
								maxDate={new Date()}
								date={date1 ? date1 : new Date()}
							/>
						</div>
						<div>
							<p className='date__title date__title--left'>{formatMessage({ id: 'date_end' })}</p>
							<CalendarComponent
								className="c-calendar2 c"
								onChange={(date: any): void => {
									
							
									if (date1 > date) {
										setDate2(date1)
										setDate1(date)
										if(onChange)onChange({setDate1,setDate2,date2:date1,date1:date})
									}else{
									setDate2(date)
										if(onChange)onChange({setDate1,setDate2,date2:date,date1:date1})
									}
									if (date1) {
										setDisplayDatePicker(false)
										if (displayDatePickerOutside && setDisplayDatePickerOutside) setDisplayDatePickerOutside(false)

									}
								}}
								maxDate={new Date()}
								date={date2 ? date2 : new Date()}
							/>
						</div>
						<div className='date__displayFullDate mb-1'>
							{date1 && date2 ?

								<>
									{niceDate}
								</>
								:
								null
							}
						</div>
					</div>
					: null}
		</div >

	)

}


interface CalendarSingleProps {
	date?: any
	left?: string;
	top: string;
	onlyPopUp?: boolean
	displayDatePickerOutside?: any
	setDisplayDatePickerOutside?: any
	dontClose?: boolean
	onChange: (date: any) => void
	calanderId: any
	inputId: any,
	classNames? :string
}
export const CalendarSingleInput: React.FC<CalendarSingleProps> = ({ calanderId, inputId, onChange, date, left, top, onlyPopUp, displayDatePickerOutside, setDisplayDatePickerOutside, dontClose,classNames }) => {
	const [displayDatePicker, setDisplayDatePicker] = React.useState<any>(onlyPopUp ? true : false)

	const openDatePicker = () => {
		setDisplayDatePicker(true)
	}
	document.addEventListener('click', function (event: any) {
		const specifiedElement = document.getElementById(calanderId);
		const inputElement = document.getElementById(inputId);
		if (specifiedElement && inputElement && !dontClose) {
			const isClickInside = specifiedElement.contains(event.target);
			const isClickInsideInput = inputElement.contains(event.target)
			if (!isClickInside && !isClickInsideInput) {
				setDisplayDatePicker(false)
				if (displayDatePickerOutside && setDisplayDatePickerOutside) setDisplayDatePickerOutside(false)
			}
		}
	});
	const customizer = { direction: document.getElementsByTagName("html")[0].dir }

	return (
		<div>
			{onlyPopUp ? null :
				<div id={inputId} onClick={openDatePicker}  className={`${classNames ? classNames : ''} date__inputBar d-flex justify-content-evenly align-items-center cursor-pointer round px-1 bg-white font-medium-1 text-bold-600`} >
					<div className='m-auto'>
						{date ? moment(date).format('DD.MM.YY') : ''}
					</div>
					<Icon className='align-self-center date__icon' src={config.iconsPath + "table/date.svg"} />
				</div>
			}
			{
				displayDatePicker || displayDatePickerOutside ?
					<div id={calanderId} className={classnames("date__container_single d-flex justify-content-evenly", {
						'date__onlyPopUp': onlyPopUp,
					})} style={customizer.direction === 'rtl' ? { top: top, left: left }: { top: top, right: left }}>
						<div>
							<CalendarComponent
								className="c-calendar c"
								onChange={(date: any): void => {
									onChange(date)
									setDisplayDatePicker(false)
								}}
								// maxDate={new Date()}
								minDate={new Date('01 Jan 2000 00:00:00 GMT')}
								date={date ? date : new Date()}
							/>
						</div>
					</div>
					: null}
		</div >

	)

}

export default CalendarComponent;
