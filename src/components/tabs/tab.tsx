import * as React from 'react';
import { NavItem, NavLink } from 'reactstrap'

interface Props {
	id?: string;
	name: string;
	caption: string;
	selected?: boolean;
	className?: string;
	navClassName?: string;
	onChangeTab?: (name: string) => void;
}

export const Tab: React.FC<Props> = ({id, name, caption, selected, className, onChangeTab,navClassName}) => {
	const handler = () => {
		if (onChangeTab) {
			onChangeTab(name);
		}
	}

	return (
		<NavItem>
			<NavLink id={id} className={navClassName} active={selected} onClick={handler}>
				<div className={className}>{caption}</div>
			</NavLink>
		</NavItem>
	)
}
