import * as React from 'react';
import { Nav } from 'reactstrap';
import './tabs.scss';

interface Props {
	children: React.ReactNode;
	value: string;
	onChangTab?: (name: string) => void;
	disableChangeTab?: boolean;
}

export const Tabs: React.FC<Props> = ({ children, value, onChangTab, disableChangeTab }) => {
	const [activeTab, setActiveTab] = React.useState(value)

	const changeTab = (tab: string) => {
		if (activeTab !== tab && !disableChangeTab) {
			setActiveTab(tab);

			if (onChangTab) {
				onChangTab(tab);
			}
		}
	}

	const mapChildren = (child: React.ReactNode, key: number): React.ReactNode => {
		if (React.isValidElement(child)) {
			const { name } = child && child.props || {};

			return React.cloneElement(child, {
				key,
				onChangeTab: changeTab,
				selected: activeTab === name,
			});
		}

		return child;
	};

	return (
		<Nav tabs className='tabs'>
			{React.Children.map(children, mapChildren)}
		</Nav>
	)
}
