import * as React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { RootStore } from '@src/store';
import { Options } from '@containers/options'
import { sortRows } from '@utilities'
// import { UsersActionTypes } from '@containers/tasks/enums';
import { Popup, Table, TableType } from '@components';
// import { Event } from '@containers/planning/interfaces';
import { ArrowDown, ArrowUp } from 'react-feather';
import { FormattedMessage, useIntl } from "react-intl"

interface Props {
	readonly id: string;
	readonly options?: boolean;
	readonly titles?: any;
	readonly type: TableType
	readonly index: number;
	readonly list: any;
	readonly rows: any;
	readonly category: any;
	readonly provided?: any;
	readonly disableParentDroppable?: string;
	readonly innerRef?: any;
	readonly fields: any;
	readonly isDone?: boolean;
	readonly setFields: (value: any[]) => void;
	readonly addColumnDispatch: (key: string, value: any, category_id: number) => void;
	readonly deleteColumnDispatch: (key: string, category_id: number) => void;
	readonly updateColumnTitleDispatch: (key: string, value: string, category_id: any) => void;
	readonly addNewCategory?: () => void;
	readonly deleteCategory?: (listId:number) => void;
	readonly handleSidebarTitleChange?: (event: React.ChangeEvent<HTMLInputElement>, user_id: number, project_id: number, color: string, id: number) => void;

}
	export const ListView: React.FC<Props> = (props: Props) => {

	const { formatMessage } = useIntl();
	const dispatch = useDispatch();
	const { id, list,titles, isDone,type, options, rows, index,fields, category, setFields,addColumnDispatch, deleteColumnDispatch, updateColumnTitleDispatch, addNewCategory, deleteCategory, handleSidebarTitleChange, provided,  innerRef , disableParentDroppable} = props;
	const [isPopupOpen, setIsPopupOpen] = React.useState(false);
	const [sidebarTitle, setSidebarTitle] = React.useState(category);
	const event = useSelector((state: RootStore) => state.events.filter((event: Event) => {return event.preview})[0])

	const totalExpenses =
		type === TableType.EXPENSES || type === TableType.UNPLANNED
			? rows.reduce((acc: number, row: any) => {
					if (row.price === undefined) {
						return acc + 0;
					}

					return Number(acc) + (Number(row.price) * Number(row.quantity));
			  }, 0)
			: 
			0;

			// const maxIndexColumn = (fields: any, type: string) => {
			// 	let columnItems:any = fields.map((field:any, index:number)=> {
			// 		if(type == 'number' && field.includes('number')) {
			// 			return Number(field.split('number')[1])
			// 		}
			// 		if(type == 'text' &&  field.includes('text') ) {
			// 			return Number(field.split('text')[1])        
			// 		}
			// 		else return
			// 	})
			// 	return columnItems.filter(c=>c).length ?  Math.max(...columnItems.filter(c=>c)) : 0
					
			// }

			// React.useEffect(() => {
			// 		setSidebarTitle();
			// }, [list]);


	return (
		<div className="p-2">
			<div className="h4 d-flex align-items-center position-relative">
				<div className="text-light-purple d-flex">
					{handleSidebarTitleChange ? 
					<input
						onChange={(event: React.ChangeEvent<HTMLInputElement>): void => {
							handleSidebarTitleChange(event,list.user_id, list.project_id, list.color, list.id);
							setSidebarTitle(event.target.value);

						}}
						type="text"
						className="bg-transparent border-0 width-10-rem text-light-purple"
						id={list.id}
						placeholder={formatMessage({id: 'enter_title'})}
						value={sidebarTitle}
					/>
					: sidebarTitle }
					{addNewCategory && (
					<div className="ml-1"
						onClick={(): void => {
							setIsPopupOpen(!isPopupOpen);
						}}>
					{isPopupOpen ? <ArrowUp className="text-light-purple" size={16}/> : <ArrowDown className="text-light-purple" size={16}/>}
					</div>
				)}
				</div>
				
				<Popup
					isOpen={isPopupOpen}
					onClick={(): void => {
						return;
					}}
					onOutsideClick={(): void => setIsPopupOpen(false)}
					options={[
						{
							text: formatMessage({id: 'new'}),
							disabled: !addNewCategory,
							action: (): void => {
								setIsPopupOpen(!isPopupOpen);
								if( addNewCategory) addNewCategory()
							}
						},
						{
							text: formatMessage({id: 'delete'}),
							disabled: Boolean(rows.length > 0),
							action: (): void => {
								setIsPopupOpen(!isPopupOpen);
								if( deleteCategory ) deleteCategory(list.id)
							}
						}
					]}
					className="c-popup--tasks-list-view"
				/>

			{type === TableType.EXPENSES || type === TableType.UNPLANNED && (
				<div className="ml-2 text-light-purple">
					<span className="c-table__sidebar-data-text"><FormattedMessage id='cost'/>: </span>${totalExpenses}
				</div>
			)}
			</div>

		{options && (
			<Options 
						   fields={fields}
						   category_id={list.id}
						   addColumnDispatch={addColumnDispatch}
						   setFields={setFields}
						   sortBy={(field: string)=>{
							sortRows(list.tasks.default, field)
							.then(rows=>{
								// dispatch({
							})
							
						}}/>
		  
		)}
		
			<Table
				id={id}
				type={type}
				rows={rows}
				titles={titles ? titles : undefined}
				setFields={setFields}
				disableParentDroppable={disableParentDroppable}
				deleteColumnDispatch={deleteColumnDispatch}
				updateColumnTitleDispatch={updateColumnTitleDispatch}
				fields={fields}
				provided={provided}
				innerRef={innerRef}
				index={index}
				key={index}
				isDone={isDone}
				listId={list.id}
			/>
		</div>
	);
};

export default ListView;
