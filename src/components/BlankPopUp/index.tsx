import * as React from "react";
import { useOutsideClick } from '@src/utilities';
import './index.scss'
interface Props {
    children?: any
    style?: any
    className?: string
    onOutsideClick: any
    noXbutton?: boolean
    center?:boolean;
    bgBlueDark ? :boolean;
    bigX?:boolean
    // onOutsideClick: () => void;
}

export const BlankPopUp: React.FC<Props> = props => {
    const { children, style, className, onOutsideClick, noXbutton,bgBlueDark ,bigX} = props;
    const [closeButtonClicked, setCloseButtonClicked] = React.useState<boolean>(false)
    const ref: React.MutableRefObject<any> = React.useRef(null);
    useOutsideClick(ref, onOutsideClick!);
    let classes = `${className ? className : ''} ${bgBlueDark ? 'bg-blue-dark' : 'bg-white'} position-absolute zindex-10003 box-shadow-1 round-edge-xs`


    return (
        <div style={style} className={classes} ref={!closeButtonClicked ? ref : null}>
            {!noXbutton &&
                <div onClick={() => { setCloseButtonClicked(true); setTimeout(() => { onOutsideClick(false) }, 2) }}
                    style={{  top: '0.8rem' }} className='blackPopUp right-08-rem border-radius-2rem cursor-pointer float-right mt-05 mr-05 text-black position-absolute'>
                    <svg xmlns="http://www.w3.org/2000/svg" width={bigX?'32':'20'} height={bigX?'32':'20'} viewBox="0 0 20 20">
                        <g fill="none" fillRule="evenodd" opacity=".35">
                            <g fill="#1E1E20" fillRule="nonzero">
                                <g>
                                    <path d="M11 3v6h6v2h-6v6H9v-6H3V9h6V3h2z" transform="translate(-785 -304) translate(785 304) rotate(45 10 10)" />
                                </g>
                            </g>
                        </g>
                    </svg>
                </div>}
            {children ? children : null}
        </div>
    )
}
export default BlankPopUp