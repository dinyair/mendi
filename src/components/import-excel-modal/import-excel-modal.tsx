import * as React from 'react';

import { useIntl } from "react-intl"
import AsyncSelect from "react-select/async";
import { GenericModals } from '@src/components/GenericModals/index';
import { ModalInterface, ModalTypes } from '@src/components/GenericModals/interfaces';
import { AgGrid } from '@src/components';
import { DropZoneField } from './dropZone';
import { Spinner } from 'reactstrap';


interface Props {
    toggleModal: (type: ModalTypes) => void;
    modalHandlers: any;
    wantedFields: string[];
    indexField: string;
    exampleRows: any;
    exampleFields: any;
    description: any;
}

export const ImportExcelModal: React.FC<Props> = props => {

    const {
        toggleModal,
        modalHandlers,
        wantedFields,
        indexField,
        exampleRows,
        exampleFields,
        description
    } = props;

    const { formatMessage } = useIntl();


    const [currentLevel, setCurrentLevel] = React.useState<number>(0);

    const [uploadFile, setUploadFile] = React.useState<any>(null)
    const [fields, setFields] = React.useState<any>()
    const [originalFields, setOriginalFields] = React.useState<any>()


    const [rows, setRows] = React.useState<any[]>([])
    const [selectedColumn, setSelectedColumn] = React.useState<string>('')
    const [isLoading, setIsLoading] = React.useState<boolean>(false);
    const [relatedColumns, setRelatedColumns] = React.useState<any>({})




    const productStyles = {
        container: (base: any) => ({
            ...base,
        }),
        option: (provided: any, state: any) => ({
            ...provided,
            ...provided,

            "&:hover": {
                backgroundColor: "#31baab"
            },
            color: state.isSelected ? '#ffffff' : '#1e1e20',
            backgroundColor: state.isSelected ? '#027b7a' : '#ffffff',
            borderBottom: '1px dotted lightgray',
            padding: 10,
        }),
    }



    const handleNextLevel = () => {
        setCurrentLevel(currentLevel + 1);
    }

    const handleBackLevel = (removeFile: boolean) => {
        removeFile && setUploadFile(null)
        setCurrentLevel(currentLevel - 1);
    }




    const setFieldsDefault = (coloredColumn?: any) => {
        const newFields = uploadFile.cols.map((ele: any) => {
            return {
                text: ele, coloredColumn: coloredColumn ? coloredColumn : '', type: 'string', width: window.innerWidth * 0.0001, minWidth: 50, cellRenderer: 'ClickableCell', clickableCells: {
                    onClick: (data: any) => {
                        updateRows(data.field)
                    }
                },
            }
        })
        setFields(newFields)
        setOriginalFields(uploadFile.cols.map((ele: any) => { return { text: ele, type: 'string', width: window.innerWidth * 0.0001, minWidth: 50, } }))
    }

    const updateRows = (field: string) => {
        let newRows = uploadFile.rows.map((row: any) => {
            let coloredColumn = field
            return { ...row, coloredColumn }
        })
        // console.log(newRows)
        setFieldsDefault(field)
        setSelectedColumn(field)
        // console.log(indexField)
        setRelatedColumns({ [field]: indexField })
        // console.log({ [field]: indexField })
        setRows([])
        setTimeout(() => {
            setRows(newRows)
        }, 1);
    }

    const mergeRowsWithColumns = () => {
        setIsLoading(true);
        // console.log(relatedColumns)
        // console.log(uploadFile.rows)
        const newRows = uploadFile.rows.map((row: any) => {
            const keys = Object.keys(relatedColumns)
            const values = Object.values(relatedColumns)
            // console.log(keys)
            // console.log(values)
            // console.log(row)
            let obj: any = {}
            values.forEach((value: any, index: number) => {
                obj[value] = row[keys[index]]
            })
            return obj
        })

        modalHandlers.import(newRows)
    }

    const importModals: ModalInterface[] = [{
        classNames: 'width-35-per min-width-30-rem max-width-30-rem',
        isOpen: true, toggle: () => toggleModal(ModalTypes.none), 
        header: '',
        headerClassNames:'pt-2',
        footerClassNames: 'd-none',
        body: (
            <>
                <div className="mb-2">
                    <h2>{formatMessage({ id: 'importExcelFile' })} {formatMessage({ id: 'Excel/CSV' })}</h2>

                    <AgGrid
                        id={'exampleRows'}
                        gridHeight={'9.5rem'}
                        floatFilter={false}
                        translateHeader={false}
                        fields={exampleFields}
                        groups={[]}
                        totalRows={[]}
                        rows={exampleRows}
                        noSortFields
                    />
                    
                    <p className="mt-1">
                        <span className='text-bold-600 '>{formatMessage({id:'description'})}: </span>
                        <br/> {description}</p>
                    
                    {/* <h3 className="text-danger">{formatMessage({ id: 'MakeSureTheUploadFileContainsItemsFromSupplier' })}  {supplierData && supplierData.SupplierName} {formatMessage({ id: 'only' })}</h3> */}
                </div>

                <div className="d-flex justify-content-center mb-1">
                    <DropZoneField handleUpload={(data) => {
                        setUploadFile(data), handleNextLevel()
                        setRows(data.rows)
                    }} classNames="border-dashed p-3 width-70-per import-excel-border" />
                </div>

            </>
        ),
    }, {
        classNames: 'width-35-per min-width-30-rem max-width-30-rem',
        isOpen: true, toggle: () => toggleModal(ModalTypes.none),
        header: '',
        headerClassNames:'pt-2',
        body: (
            <>
                <div className="text-center">
                    <h2>{formatMessage({ id: 'Cool, The file was uploaded!' })}</h2>
                </div>
                <div className="d-flex justify-content-center">
                    <div className="border-dashed p-2 width-70-per import-excel-border text-center width-70-per">
                        <h4>{uploadFile && uploadFile.fileName}</h4>
                        <p className="m-0" onClick={() => handleBackLevel(true)}>{formatMessage({ id: "You can replace the file from here" })}</p>
                    </div>
                </div>
            </>
        ),
        footerClassNames: 'd-flex justify-content-center',
        buttons: [{
            color: 'primary', onClick: () => {
                setFieldsDefault()
                handleNextLevel()
            }, label: formatMessage({ id: 'chooseTargetForCulomns' }), disabled: false
        },]
    },
    {
        isOpen: true,
        toggle: () => toggleModal(ModalTypes.none),
        classNames: 'min-width-50-rem',
        header: (
            <div className="mt-2">
                <h2>{formatMessage({ id: 'whatIsYourBarcodeCulomn' })}</h2>
                <span className="font-weight-400"> {formatMessage({ id: 'InOrderToUploadFileWeNeedToKnowTheCulomnOrder' })}</span>
            </div>
        ),
        body: (
            <>
                <div className='height-40-vh'>
                    {uploadFile && rows.length ?
                        <AgGrid
                            rowBufferAmount={35}
                            defaultSortFieldNum={0}
                            id={'id'}
                            gridHeight={'40vh'}
                            translateHeader={false}
                            fields={fields}
                            groups={[]}
                            totalRows={[]}
                            rows={rows}
                            resizable
                            colorColumnFlag={"coloredColumn"}
                        />
                        : null}
                </div>

            </>
        ),
        buttons: [{
            color: 'primary', onClick: () => {
                handleBackLevel(false)
            }, label: formatMessage({ id: 'back' }), disabled: false
        }, {
            color: 'primary', onClick: () => {
                handleNextLevel()
                // modalHandlers.import(uploadFile.rows)
            }, label: formatMessage({ id: 'chooseTargetForCulomns' }), disabled: !selectedColumn
        },
        ]
    }, {
        classNames: 'min-width-50-rem',
        isOpen: true, toggle: () => toggleModal(ModalTypes.none), header: (
            <div className="mt-2">
                <h2>{formatMessage({ id: "WhatAboutTheOtherColumns?" })}</h2>
                <span className="font-weight-400"> {formatMessage({ id: 'SelectTheColumnHeaderForAllColumnsFromExcel' })}</span>
            </div>
        ),
        body: (
            <>
                <div className="d-flex ">
                    {uploadFile && uploadFile.cols &&
                        uploadFile.cols.map((ele: any) => {
                            return {
                                isDisabled: ele === selectedColumn,
                                name: ele,
                                isMulti: false,
                                allowSelectAll: false,
                                loadOptions: () => { },
                                text: ele === selectedColumn ? formatMessage({ id: indexField }) : formatMessage({ id: 'SelectCulomn' }),
                                defaultOptions: wantedFields.map((option: any, index: number) => {
                                    return { index, value: option, label: formatMessage({ id: option }) }
                                }),
                                onChange: (e: any, actionMeta: any) => {
                                    // console.log(e, actionMeta)
                                    const related = { ...relatedColumns };
                                    related[actionMeta.name] = e.value;
                                    // console.log(related)
                                    setRelatedColumns(related)
                                },
                                defaultValue: [],

                                styles: productStyles
                            }
                        }).map((item: any, index: number) => {
                            return (
                                <div key={index} className="mr-05">
                                    <AsyncSelect
                                        name={item.name}
                                        isDisabled={item.isDisabled}
                                        styles={item.styles}
                                        loadOptions={item.loadOptions}
                                        defaultOptions={item.defaultOptions}
                                        // onInputChange={handleInputChange}
                                        defaultValue={item.defaultValue}
                                        onChange={item.onChange}
                                        classNamePrefix='max-width-9-rem select'
                                        noOptionsMessage={() => formatMessage({ id: 'trending_productMinimumSearch' })}
                                        placeholder={item.text}
                                    />
                                </div>
                            )
                        })}
                </div>


                <div className='height-40-vh mt-1'>
                    {uploadFile ?

                        <AgGrid
                            rowBufferAmount={35}
                            defaultSortFieldNum={0}
                            id={'sec'}
                            gridHeight={'40vh'}
                            floatFilter={false}
                            translateHeader={false}
                            fields={originalFields}
                            groups={[]}
                            totalRows={[]}
                            rows={uploadFile.rows}
                            checkboxFirstColumn={false}
                            resizable
                        />

                        : null}
                </div>

            </>
        ),
        buttons: [{
            color: 'primary', onClick: () => {
                setRelatedColumns({ [selectedColumn]: indexField })
                handleBackLevel(false)
            }, label: formatMessage({ id: 'back' }), disabled: isLoading
        }, {
            color: 'primary', onClick: () => {
                mergeRowsWithColumns()
            }, label: !isLoading ? formatMessage({ id: 'uploadDataToTheServer' }) : <Spinner />, disabled: Object.keys(relatedColumns).length - 1 < wantedFields.length && Object.values(relatedColumns).length - 1 < wantedFields.length || isLoading
        },
        ]
    }
    ]






    return (
        <GenericModals key={`importExcelModal${currentLevel}`} modal={importModals[currentLevel]} />
    );
};



export default ImportExcelModal;
