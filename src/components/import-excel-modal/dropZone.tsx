import { config } from '@src/config';
import * as React from 'react';
// import "./styles.css";

import Dropzone from 'react-dropzone';
import { ExcelRenderer } from 'react-excel-renderer';
import { useIntl } from 'react-intl';
import { Icon } from '../icon';


interface Props {
    maxFiles?: number;
    acceptedFiles?: string;
    classNames?: string;
    handleUpload: (data: any) => void
}



export const DropZoneField: React.FC<Props> = (props) => {
    const { maxFiles, acceptedFiles, handleUpload, classNames } = props;

    const { formatMessage } = useIntl();

    const fileHandler = (files: any) => {
        ExcelRenderer(files[0], (err: any, resp: any) => {
            if (err) {
                console.log(err);
            }
            else {
                const cols = resp.rows[0];
                resp.rows.shift();


                const rows = resp.rows.map((row: any[]) => {
                    const obj: any = {};

                    row.forEach((col, index) => {
                        obj[cols[index]] = col;
                    })

                    return obj
                })


                handleUpload({
                    fileName: files[0].name,
                    cols,
                    rows: rows.filter((obj: any) => Object.keys(obj).length !== 0)
                });
            }
        });

    }

    return (
        <>
            <Dropzone
                onDrop={fileHandler}
                accept={".xls, .xlsx"}
                minSize={1024}
                maxSize={3072000}
                maxFiles={1}
            >
                {({ getRootProps, getInputProps }) => (
                    <div {...getRootProps({ className: ` d-flex flex-column align-items-center justify-content-center dropzone text-center ${classNames}` })}>

                        <Icon src={`${config.iconsPath}general/file.svg`} className={'mb-2'} />

                        <input {...getInputProps()} />
                        <h4 className={'text-bold-700'}>{formatMessage({ id: "You can drag the file here" })}</h4>
                        <h5 className={'text-bold-600'}>{formatMessage({ id: 'Or' })}</h5>
                        <button className='btn btn-primary round height-2-5-rem d-flex justify-content-center align-items-center width-9-5-rem mr-2 ml-2'>
                            {formatMessage({ id: 'Search in files' })}
                        </button>
                    </div>
                )}
            </Dropzone>

        </>
    )
}
