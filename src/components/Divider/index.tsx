import * as React from "react";


interface Props {
    style?: any
    className?: any
}

export const Divider: React.FC<Props> = props => {
    const { style, className } = props;
    let classes = `${className} bg-lightgray height-2 width-80-per m-auto`
    return (
        <div style={style} className={classes}></div>
    )
}
export default Divider