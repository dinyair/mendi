import * as React from 'react';
import { groupBy, Sorting, filterBy } from '../../helpers/helpers'
import { v4 as uuidv4 } from 'uuid';
import classnames from "classnames"
import { Icon } from '@components';
import { config } from '../../config'
import { Badge } from "reactstrap"
import { ColumnsPopup } from './ColumnsPopup'
import { useDispatch, useSelector } from 'react-redux';
import ExportExcel from './ExportExcel';
import ReactToPrint from 'react-to-print';
import { FormattedMessage, useIntl } from "react-intl"
import * as ExpensesActions from "../../redux/actions/budget/expenses" // you can use this structure to make an organized dispatch calls 
// import {
// 	Draggable,
// 	Droppable,
// 	DropResult,
// 	DragDropContext,
// 	DroppableProvided,
// 	DraggableStateSnapshot,
// 	DraggableProvided
// } from 'react-beautiful-dnd';
import { RootStore } from '@src/store';
import { capitalize } from '@utilities';
import { Popup, NewRow, Rows, SearchBox} from '@components';
import './index.scss';
import { setWeekYearWithOptions } from 'date-fns/fp';
import { Collapse } from '@material-ui/core';
import { id } from 'date-fns/esm/locale';

export enum TableType {

}

interface Props {
	readonly id: string;
	readonly type: TableType;
	readonly rows: any;
	readonly index: number;
	readonly fields: any
	readonly hiddenColumns?: any[]
	readonly withoutHeader?: boolean;
	readonly groups?: any;
	readonly listId?: any;
	// readonly provided?: DroppableProvided;
	// readonly innerRef?: any;
	// readonly disableParentDroppable?: string;
	// readonly deleteColumnDispatch?: (key: string, category_id: number) => void;
	// readonly updateColumnTitleDispatch?: (key: string, value: string, category_id?: any) => void;
	// readonly addNewCategory?: () => void;	
	readonly setFields: (value: any[]) => void;

}
export const Table: React.FC<Props> = (props: Props) => {
	const { type, index,groups,  withoutHeader, listId, fields, setFields } = props;
	const { formatMessage } = useIntl();
	const dispatch = useDispatch();	
	const TableRef = React.useRef<HTMLElement>(null);
	const [isTitlePopupOpen, setisTitlePopupOpen] = React.useState(false);	
	const [mergedGroups, setMergedGroups] = React.useState<any>([]);	
	const [hiddenColumns, setHiddenColumns] = React.useState<any[]>(props.hiddenColumns ? props.hiddenColumns : []);
	const [sort, setSort] = React.useState({ field: '', asc: true, times: 0 })
	const [rows, setRows] = React.useState<any[]>(props.rows);	
	const [updatedRows, setUpdatedRows] = React.useState<any[]>(rows);	
	const [selected, setSelected] = React.useState<any[]>([]);	
	const [isIndeterminate, setIsIndeterminate] = React.useState<boolean>(false);	
	const [showColumnPoup, setShowColumnPoup] = React.useState(false);	
	const hasRows = rows.length !== 0 ? true : false;

	const handleSort = (field: string, type: string) => {
		if (sort.times === 2)  { 
			let newSort:any = { field: '', type: '', asc: false, times: 0 }
			setSort(newSort) 
			let newRows:any[] = Sorting(rows, newSort)
			setRows(newRows)
		}
		else {
			let newSort:any = { field: field, type: type, asc: sort.field === field ? !sort.asc : false, times: sort.times+1 }
			setSort(newSort)
			let newRows:any[] = Sorting(rows, newSort)
			setRows(newRows)
			let groups:any =  groupBy(newRows,Object.keys(mergedGroups).length ?  Object.keys(mergedGroups) : [])
			setUpdatedRows(groups)
		}
	  }
	
	const onKeyPress = (id: string): void => {}		

	const changeValueDB = async (value: any, id: string | number, field: string) => {
		let index = rows.findIndex((item: any, i:number) => item.id === id);
		var task = rows.find((item: any) => item.id === id);
		var item = {...task, [field]: value}
		//await 
	} 	
	const changeValue = async (value: any, id: string | number, field: string) => {
		var task = rows.find((item: any) => item.id === id);
		var item = {...task, [field]: value}
		//	dispatch
	};
	const onAddRow = async () => {
		let row = {
				pos: rows.length + 1,
				id: uuidv4(),
		} as any

		//dispatch();
	};
	
	const deleteRow = async (id: number) => {
		var task = rows.find((item: any) => item.id === id);
		const newRows = rows.filter((row: any) => row.id !== id);
		if( typeof(id) == 'number' ) {}// await deleteRow(id)
		//dispatch(ExpensesActions.setExpenses(newRows, index, title))
	};

	  
	const mergeByGroups = (field?: string, reverse?:boolean) => {
		if (field) if( reverse ) {let newMergedGroups:any = mergedGroups; delete newMergedGroups[field] ;setMergedGroups(newMergedGroups)}  else mergedGroups[field] = []
		// Object.keys(groups).map((k, i) => { return {rows: groups[k], field: 'din'}})
		let groups:any =  groupBy(rows,Object.keys(mergedGroups).length ?  Object.keys(mergedGroups) : [])//).map(c=>{return{...c, group:true}}
		setUpdatedRows(groups)

	}
	const Groups = (
		<div className="d-flex py-3">
			{ groups && groups.length && (
				groups.map((group:string, index:number) => true ? (// !hiddenColumns.includes(group)
				<div
					key={index}
					onClick={ !mergedGroups[group] ? ()=> {mergeByGroups(group)}: ()=> {mergeByGroups(group, true)}}
					 className={classnames("bg-gray position-relative font-small-3 cursor-pointer py-05 border-2 px-2 text-black text-bold-700  mr-05 border-radius-2rem",{
					   "border-turquoise": mergedGroups[group],
					//    "bg-gray": !mergedGroups.includes(group)
					 })}>
						{<FormattedMessage id={group}/>}
						<Badge pill color='primary' className='badge-up bg-turquoise border-white border-1 text-bold-600 text-white width-1-rem position-left-0 position-top-02'>
						{Object.entries(mergedGroups).findIndex(mg => mg[0] === group) > -1 ? Object.entries(mergedGroups).findIndex(mg => mg[0] === group)+1 : null}
						</Badge>					
				</div>
				
			):null))}
		</div>
	)

	const Options = (
	<div className="d-flex">
		<div onClick={()=>setShowColumnPoup(true)}
			className="d-flex justify-content-center align-items-center cursor-pointer bg-gray font-medium-1 text-black text-bold-600 p-05">
			<Icon src={config.iconsPath+"table/column-view.svg"}  style={{height:'0.5rem', width: '0.5rem'}}/>
		</div>
		<div className="d-flex justify-content-center align-items-center ml-05 cursor-pointer bg-gray font-medium-1 text-black text-bold-600 p-05">
			<ExportExcel data={updatedRows} 
			icon={<Icon src={config.iconsPath+"table/excel.svg"}  style={{height:'0.5rem', width: '0.5rem'}}/>}/>

			
		</div>
		<div className="d-flex justify-content-center align-items-center ml-05 cursor-pointer bg-gray font-medium-1 text-black text-bold-600 p-05">
		<ReactToPrint
          trigger={() => <div> <Icon src={config.iconsPath+"table/print.svg"}  style={{height:'0.5rem', width: '0.5rem'}}/></div>}
          content={() => TableRef.current}
        />
		
		</div>
	</div>
	)

	
	const Title = (key: number, title: any, type: string) => {return (
		<>
		
		<div  
	key={key}
	onClick={()=>{handleSort(title, type)}}
	className={classnames(`btn height-3-rem cursor-pointer font-small-3 text-bold-600  text-black  custom-control-info py-05 px-1 d-flex align-items-center  position-relative
	  ${isTitlePopupOpen == title ? 'popup-open' : ''}`,{
		'width-7-rem': true,
		'width-12-rem': type == 'large_string',
		"bg-turquoise": false,
		"bg-light-turquoise-1": false && fields.length >= 3  && fields.length/3 > key,
		"bg-light-turquoise-2": false && fields.length >= 4  && fields.length/4 > key,
		// "bg-light-turquoise-3": fields.length >= 5  && fields.length/1 > key
	  })}>
		 <FormattedMessage id={title} />
	  
	
	{ sort.field === title ?
	 <Icon src={config.iconsPath+"table/sort.svg"} 
			className={classnames("display-hidden ml-1",{
				'rotate-180': sort.asc
			})}
			style={{height:'0.5rem', width: '0.5rem'}}/>
	: 
		<Icon src={'../../../'+config.iconsPath+"table/sort-disabled.svg"} className="display-hidden ml-1" style={{height:'0.5rem', width: '0.5rem'}}/>
	}
	<span onClick={(): void => {
			setisTitlePopupOpen(isTitlePopupOpen ? false : title);
	}}/>	
	
	{isTitlePopupOpen == title && (
		<Popup
			isOpen={isTitlePopupOpen}
			onClick={(): void => {
				return;
			}}
			onOutsideClick={(): void => setisTitlePopupOpen(false)}
			options={[
				{
					disabled: false,
					text: formatMessage({id: 'delete'}),
					action: (): void => {
						if( false )// deleteColumnDispatch(title, listId)
						setisTitlePopupOpen(false)
					}
				}
					]}
			className="c-popup--tasks-list-view"
		/>
	)}
	</div> 
		</>)}

	const Header = (!withoutHeader ?
		<div className="c-table__header full-width"> 
		<div className="position-absolute position-left-minus-1-rem mt-3 d-flex align-items-center px-1">
			<input 
			onClick={ selected.length === rows.length ? ()=> setSelected([]) : ()=> setSelected(rows.map((r:any, ri) => r.id))}
			type="checkbox" id="allRowsCheckBox" className="width-075-rem bg-turquoise"
			ref={elem => elem && (elem.indeterminate = isIndeterminate)}></input>
			{// onClick={()=> { if( document.getElementById('HeaderCheckbox') ) document.getElementById('HeaderCheckbox').indeterminate = true}}
			}
		</div>
		{Object.keys(mergedGroups).length ? (
			<div className="d-flex flex-column">
					<div className='d-flex flex-column width-8-rem height-3-rem'></div>
			</div>
		):null}
		{fields.map((title: any, key: number) => !mergedGroups[title.text] && !hiddenColumns.includes(title.text)? 
			(
			<div className="d-flex flex-column">
			<div className={classnames('height-3-rem cursor-pointer font-small-3 text-bold-600  text-black  custom-control-info py-05 align-items-center justify-content-center px-1 d-flex position-relative',{
				'opacity-0': !title.category ,
				'display-hidden':  title.category && (!title.fields || !title.fields.length)
			})}
			style={{backgroundColor: title.bgColor ? title.bgColor : '#f3f3f5'}}
			>
				{title.category && (title.category)}
			</div>
			<div className="d-flex">
				{title.category ? 
				title.fields.map((title: any, key: number) =>  !mergedGroups[title.text] && !hiddenColumns.includes(title.text) ?  (
					Title(key, title.text , title.type) 
				) : null)
				:	Title(key, title.text, title.type) 
				}
			</div>
				</div> )
				:null
			)} 
	</div>
	:null)

	const renderDefualt = (): React.ReactElement => (<>
		<div className="width-100-per d-flex justify-content-between align-items-center">
			{Groups}
			{Options}
		</div>
		<div className={`c-table px-1 width-min-content c-table--${type}`} ref={TableRef as React.RefObject<HTMLDivElement>}>
			{Header}
			<SearchBox/>
			{/* <div ref={innerRef ? innerRef : null} className="c-table__body" {...provided.droppableProps}> */}
			<div className="c-table__body" >
			{/* Object.keys(groups).map((k, i) => { return {rows: groups[k], field: 'din'}}) */}
			{Object.entries(updatedRows).map((data: any, key: number) =>{
				data = !data[fields[0]] && data[1] ?  data[1] : updatedRows[key]
					return (
					// <Draggable index={key} key={key} draggableId={`${data.id}${key}`}>
					// 	{(provided: DraggableProvided): React.ReactElement => (
								<Rows
										first
										key={key}
										index={key}
										selected={selected}
										selectRow={(id:number)=> {
										// var allRowsCheckBox:any = document.getElementById("allRowsCheckBox");
										if( id > 0 )  {
											if( selected.includes(id) ) {
												let newSelected = selected.filter(s=> s != id)
												if(!newSelected.length) setIsIndeterminate(false)
												setSelected(newSelected)

											}
											else {
													setSelected([...selected, id])
													setIsIndeterminate(true)
												}
										}}}
										hiddenColumns={hiddenColumns}
										mergedGroups={mergedGroups}
										data={data}
										fields={fields}
										onDelete={deleteRow}
										onKeyPress={onKeyPress}
										onChange={changeValue}
										onBlur={changeValueDB}
									/>
				)})}		
						
						
						
			

				{/* {provided.placeholder} */}
				
				{/* TODO {paginating} */}

				<div className="d-flex">
					<button className="" onClick={() => onAddRow()}>
						<FormattedMessage id='add_row'/>
					</button>
				</div>
			</div>
		
		</div>
	</>);


	switch (type) {
		default:
			return (<>
				{showColumnPoup &&(
					 <ColumnsPopup isOpen={showColumnPoup} 
					 			   fields={fields}
								   hiddenColumns={hiddenColumns}
								   setHiddenColumns={setHiddenColumns}
								   toogle={setShowColumnPoup} 
					 			   mergedGroups={mergedGroups} />
					)}
					 {renderDefualt()}
				 </>)
	}
};

export default (props: Props): React.ReactElement => {
	/* eslint-disable @typescript-eslint/no-non-null-assertion */

		return (
			// <DragDropContext onDragEnd={onDragEnd}>
			// 	<Droppable droppableId={String(props.id)}>
			// 		{(provided: DroppableProvided): React.ReactElement => (
						<Table
							{...props}
							// provided={provided}
							// innerRef={provided.innerRef}
						></Table>
			// 		)}
			// 	</Droppable>
			// </DragDropContext>
		);
	
};
