import React from "react";
import ReactExport from "react-export-excel";

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

const ExportExcel = ({ data, icon }) => {

    return (
        <ExcelFile  element={icon ? icon : null}>
            <ExcelSheet data={data} name="table">
                {data[0] ? Object.keys(data[0]).map((title) => (
                    <ExcelColumn label={title} value={title} />
                )):null}
            </ExcelSheet>
        </ExcelFile>
    );
}
export default ExportExcel;