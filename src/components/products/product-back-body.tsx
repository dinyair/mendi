import * as React from 'react';
import { Icon } from '@components';
import { config } from '@src/config'
import { useIntl } from "react-intl";
import { PRODUCT_TYPES } from './product';

import './product.scss';

interface Props {
	name: string;
	lastBalance: number;
	stockOnTheWay: number;
	shelfData: number;
	packingFactor: number;
	type: any;
	salePredication: any;
	preTextData:string

}

export const ProductBackBody: React.FC<Props> = props => {
	const { stockOnTheWay, shelfData, packingFactor, type, salePredication,preTextData } = props;
	const { formatMessage } = useIntl();

	return (
		<ul className='product__body product__back-body'>

			{type === PRODUCT_TYPES.SPECIALS &&
				<li>
					<span className='product__icon'>
						<Icon src={`${config.iconsPath}order/package.svg`} />
					</span>
					<span>{formatMessage({ id: 'expected' })} {salePredication} {formatMessage({ id: preTextData })}</span>
				</li>
			}

			<li>
				<span className='product__icon'>
					<Icon src={`${config.iconsPath}order/car-package.svg`} />
				</span>
				<span>{stockOnTheWay} {formatMessage({ id: 'numOfItemsOnTheWay' })}</span>
			</li>
			<li>
				<span className='product__icon'>
					<Icon src={`${config.iconsPath}order/shelf.svg`} />
				</span>
				<span>{formatMessage({ id: 'shelfStockContent' })}: {shelfData} {formatMessage({ id: preTextData })}</span>
			</li>
			<li>
				<span className='product__icon'>
					<Icon src={`${config.iconsPath}order/shop-package.svg`} />
				</span>
				<span>{formatMessage({ id: 'packingFactor' })}: {packingFactor && packingFactor>0 ? <span>{packingFactor} {formatMessage({ id: preTextData })} {formatMessage({ id: 'inCrate' })}</span>
				: <span>{formatMessage({ id: 'none' })}</span> } </span>
			</li>
		</ul>
	);
};
