import * as React from 'react';
import { Icon } from '@components';
import { config } from '@src/config';
import { PRODUCT_TYPES } from './product';
import { useIntl } from "react-intl";
import * as moment from 'moment';
import classnames from 'classnames'

import './product.scss';

interface Props {
	type: string;
	name: string;
	lastBalance: number;
	salePredication?: number;
	isPromo?: string;
	promoData1?: any;
	promoData2?: any;
	promoToDate?: any;
	promoFromDate?: any;
	barcode: any;
	preTextData:string
}

export const ProductFrontBody: React.FC<Props> = props => {
	let { name, lastBalance, salePredication, type, promoData1, promoData2, promoToDate, promoFromDate, barcode,preTextData } = props;
	const customizer = { direction: document.getElementsByTagName("html")[0].dir }

	if (promoToDate && promoFromDate) {
		var promo_fdateArr = promoFromDate.split("/");
		promoFromDate = promo_fdateArr[1] + '/' + promo_fdateArr[0] + '/' + promo_fdateArr[2];

		var promo_tdateArr = promoToDate.split("/");
		promoToDate = promo_tdateArr[1] + '/' + promo_tdateArr[0] + '/' + promo_tdateArr[2];
	}
	const { formatMessage } = useIntl();

	const noImg = () => {
		let imgRef: any = document.getElementById(String(barcode))
		if (imgRef) imgRef.src = `${config.imagesPath}noImg/noimg.png`
	}
	return (
		<div className='product__body product__front-body'>
			<div className='product__picture d-flex justify-content-center align-items-center'>
				<img id={String(barcode)} onError={noImg} src={`${config.productsImages}${barcode}.jpg`} alt={name} />
			</div>
			<div className='product__bottom'>
				<span className='product__count-in-stock'>
					<span className='product__icon'>
						<Icon src={`${config.iconsPath}order/shop-package.svg`} />
					</span>
					<span><span className={classnames("d-inline-block", {
						'd-rtl': customizer.direction == 'rtl'
					})}>{lastBalance}</span> {formatMessage({ id: preTextData })} {formatMessage({ id: 'inStock' })}</span>
				</span>
				{type === PRODUCT_TYPES.PRODUCTS ? (
					<span className='product__sale'>
						<span className='product__icon'>
							<Icon src={`${config.iconsPath}order/package.svg`} />
						</span>
						<span>{formatMessage({ id: 'expected' })} {salePredication} {formatMessage({ id: preTextData })}</span>
					</span>
				) : (
					<div>
						{promoData1 ? <span className='product__offer'>
							<span className='product__offer-title'>
								{promoData1 ? <span>{promoData1} {formatMessage({ id: 'for' })}{promoData2}</span> : null}
							</span>
							{promoToDate ? <span>{moment(promoFromDate).format('DD.M')}-{moment(promoToDate).format('DD.M')}</span> : null}
						</span> : null
						}
					</div>

				)}
			</div >
		</div >
	);
};
