import * as React from 'react';
import { Icon } from '@components';
import { config } from '@src/config';
import classnames from 'classnames';
import { ProductFrontBody } from './product-front-body';
import { ProductBackBody } from './product-back-body';
import { useIntl } from 'react-intl';
import { RootStore } from '@src/store';
import { useDispatch, useSelector } from 'react-redux';
import { setOrder } from '@src/redux/actions/order/orderActions';
import { setStockOrder } from '@src/containers/order/services/actions';
import * as moment from 'moment';
import {sendOrderToDraft} from '@src/containers/order/initial-state'
import {setStateOrders} from '@src/containers/stock/orders/services/actions'

import './product.scss';

export const PRODUCT_TYPES = {
	PRODUCTS: 'Products',
	SPECIALS: 'Specials'
};

interface Props {
	order: any;
	// order: OrderType;
	type: string;
}

export const Product: React.FC<Props> = props => {
	let {
		type,
		order: {
			name,
			barcode,
			lastBalance,
			isPromo,
			salePredication,
			stockOnTheWay,
			shelfData,
			packingFactor,
			amountOrder1,
			promoData1,
			promoData2,
			promoToDate,
			promoFromDate,
			defaultUnderZero,
			isNewProduct,
			isShakele,
			isOutPromo
		}
	} = props;

	const { formatMessage } = useIntl();
	const dispatch = useDispatch();
	const orderProducts = useSelector((state: RootStore) => state.stockOrdersOrder);
	const itemsToOrder = useSelector((state: RootStore) => state._orderToPlace);
	const [stateInfo, setStateInfo] = React.useState(false);
	const [countOrder, setCountOrder] = React.useState(amountOrder1);
	const stockOrders = useSelector(({ stockOrders }: RootStore) => stockOrders);

	const [displayInput, setDisplayInput] = React.useState<any>(false);

	const [visibleCountOrder, setVisibleCountOrder] = React.useState(false);
	const [hasChanged, setHasChanged] = React.useState(0);

	const footerTimerRef = React.useRef<any>(null);
	const productTimerRef = React.useRef<any>(null);
	const footerRef = React.useRef<any>(null);
	const inputRef = React.useRef<any>();

	const openInfo = () => {
		setStateInfo(prevState => !prevState);
	};

	React.useEffect(() => {
		if (displayInput) {
			inputRef.current.focus();
			setTimeout(() => {
				inputRef.current.select();
			}, 5);
		}
	}, [displayInput]);

	const showOrderCounts = () => {
		setVisibleCountOrder(true);
	};

	const hideOrderCounts = () => {
		setVisibleCountOrder(false);
	};
	const updateOrderAmountStore = async (order: string) => {
		setHasChanged(hasChanged+1)
		let updatedOrderNum: number = countOrder;

		if (order === 'inc') ++updatedOrderNum;
		else --updatedOrderNum;

		let updatedOrderAmount = itemsToOrder.map((order: any) => {
			if (order.BarCode === barcode) {
				return {
					...order,
					AmountOrder: packingFactor && packingFactor > 0 ? updatedOrderNum * packingFactor : updatedOrderNum,
					AmountMaraz: updatedOrderNum
				};
			} else {
				return order;
			}
		});

		const newRegProducts = orderProducts[0].map((order: any) => {
			if (order.barcode === barcode) {
				return {
					...order,
					AmountOrder: updatedOrderNum,
					amountOrder1: updatedOrderNum
				};
			} else {
				return order;
			}
		});
		const newPromoProducts = orderProducts[1].map((order: any) => {
			if (order.barcode === barcode) {
				return {
					...order,
					AmountOrder: updatedOrderNum,
					amountOrder1: updatedOrderNum
				};
			} else {
				return order;
			}
		});
		const orderSeries = orderProducts[2].map((series: any) => {
			let promoSeries = series.itemsToOrder.promoSeries.map((order: any) => {
				if (order.barcode === barcode) {
					return {
						...order,
						AmountOrder: updatedOrderNum,
						amountOrder1: updatedOrderNum
					};
				} else {
					return order;
				}
			});
			let productSeries = series.itemsToOrder.regularSeries.map((order: any) => {
				if (order.barcode === barcode) {
					return {
						...order,
						AmountOrder: updatedOrderNum,
						amountOrder1: updatedOrderNum
					};
				} else {
					return order;
				}
			});
			return { ...series, itemsToOrder: { promoSeries: promoSeries, regularSeries: productSeries } };
		});

		
		// let editedBarCode = updatedOrderAmount.find((i:any)=>i.BarCode==barcode)
		// await sendOrderToDraft([editedBarCode])
		dispatch(setStockOrder(newRegProducts, newPromoProducts, orderSeries));
		dispatch(setOrder(updatedOrderAmount));
	};
	const increment = () => {
		setCountOrder(prevValue => ++prevValue);
		updateOrderAmountStore('inc');
	};

	const decrement = () => {
		setCountOrder(prevValue => {
			if (prevValue > 0) {
				return --prevValue;
			}
			return prevValue;
		});
		updateOrderAmountStore('dec');
	};

	const updateDraftScreen = () =>{
		let newDraft:any = []
		let newStockOrders:any = {}
		for (const key of Object.keys(stockOrders)) {
			if(!newStockOrders[key]) newStockOrders[key] = []
			let type = stockOrders[key]
			type.forEach((i:any)=>{
				if(itemsToOrder && itemsToOrder.length &&i.SapakId==itemsToOrder[0].SapakId ){
					newDraft.push({...i,subOrders: itemsToOrder,ordersAmount:itemsToOrder.filter((g:any)=>g.AmountOrder>0).length})
				}else{
					newStockOrders[key].push(i)
				}
			})
		}
		newStockOrders['pending'].push(...newDraft)
		setOrders(newStockOrders['new'],newStockOrders['pending'],newStockOrders['delete'])
	}
	const setOrders = (notDoneOrders: any[], draftOrders: any[], ordersDone: any[]) => {
		dispatch(setStateOrders(notDoneOrders, draftOrders, ordersDone));
	};
	const classes = classnames('product', {
		['product__active']: stateInfo,
		'bg-danger-product': defaultUnderZero,
		'bg-product': !defaultUnderZero
	});

	const updateCountOrder = () => {
		setCountOrder(Number(inputRef.current.value));
		updateStoreFromInput();
	};
	const sendItemToDraft = async() =>{
		let itemToSave = itemsToOrder.find((i:any)=>i.BarCode==barcode)
		await sendOrderToDraft([itemToSave])

		setTimeout(() => {
			updateDraftScreen()
		}, 5);
	}
	React.useEffect(() => {
		if(hasChanged && hasChanged>0){
			sendItemToDraft()
		}
	}, [hasChanged]);

	const updateStoreFromInput = () => {
		setHasChanged(hasChanged+1)
		let updatedOrderNum = Number(inputRef.current.value);
		let updatedOrderAmount = itemsToOrder.map((order: any) => {
			if (order.BarCode === barcode) {
				return {
					...order,
					AmountOrder: packingFactor && packingFactor > 0 ? updatedOrderNum * packingFactor : updatedOrderNum,
					AmountMaraz: updatedOrderNum
				};
			} else {
				return order;
			}
		});
		const newRegProducts = orderProducts[0].map((order: any) => {
			if (order.barcode === barcode) {
				return { ...order, AmountOrder: updatedOrderNum, amountOrder1: updatedOrderNum };
			} else {
				return order;
			}
		});
		const newPromoProducts = orderProducts[1].map((order: any) => {
			if (order.barcode === barcode) {
				return { ...order, AmountOrder: updatedOrderNum, amountOrder1: updatedOrderNum };
			} else {
				return order;
			}
		});
		const orderSeries = orderProducts[2].map((series: any) => {
			let promoSeries = series.itemsToOrder.promoSeries.map((order: any) => {
				if (order.barcode === barcode) {
					return {
						...order,
						AmountOrder:
							packingFactor && packingFactor > 0 ? updatedOrderNum * packingFactor : updatedOrderNum,
						amountOrder1: updatedOrderNum
					};
				} else {
					return order;
				}
			});
			let productSeries = series.itemsToOrder.regularSeries.map((order: any) => {
				if (order.barcode === barcode) {
					return {
						...order,
						AmountOrder:
							packingFactor && packingFactor > 0 ? updatedOrderNum * packingFactor : updatedOrderNum,
						amountOrder1: updatedOrderNum
					};
				} else {
					return order;
				}
			});
			return { ...series, itemsToOrder: { promoSeries: promoSeries, regularSeries: productSeries } };
		});
		dispatch(setStockOrder(newRegProducts, newPromoProducts, orderSeries));
		dispatch(setOrder(updatedOrderAmount));
	};

	let preTextOrderAmount = '';
	let preTextData = '';
	let promoEnding = null;

	const customizer = { direction: document.getElementsByTagName('html')[0].dir };

	const daysRemaining = (untilDate: any) => {
		if(untilDate){
		var promo_tdateArr = untilDate.split('/');
		let untilDate1 = promo_tdateArr[1] + '-' + promo_tdateArr[0] + '-' + promo_tdateArr[2];

		let eventDate: any = moment(untilDate1);
		let todaysDate = moment(new Date());
		return eventDate.diff(todaysDate, 'days') + 1;
		} return 0
	};

	if (isPromo && promoToDate && promoFromDate && daysRemaining(promoToDate) <= 7) {
		promoEnding = daysRemaining(promoToDate);
	}

	if (isShakele == undefined) isShakele = 0;
	if (packingFactor == undefined) packingFactor = 0;

	if (isShakele && isShakele > 0 && packingFactor > 0) {
		preTextOrderAmount = 'boxs';
		preTextData = 'kg';
	} else if (isShakele == 1 && packingFactor == 0) {
		preTextOrderAmount = 'kg';
		preTextData = 'kg';
	} else if (isShakele == 0 && packingFactor > 0) {
		preTextOrderAmount = 'boxs';
		preTextData = 'units';
	} else {
		preTextOrderAmount = 'units';
		preTextData = 'units';
	}

	return (
		<li
			className="products__item"
			onMouseEnter={() => {
				if (productTimerRef.current) {
					clearTimeout(productTimerRef.current);
				}
			}}
			onMouseLeave={() => {
				productTimerRef.current = setTimeout(() => {
					hideOrderCounts();
				}, 3000);
			}}
		>
			<div className={classes}>
				<div className="product__content">
					<header className="product__header">
						<div>
							<h4 className="product__title">
								<span className="d-flex">
									<span>{name}</span>
								</span>

								<div className="d-flex-reverse-row justify-content-end product__barcodeHolder">
									{isNewProduct == 1 ? (
										<div
											className={classnames(
												'ml-1 product__newProduct d-flex justify-content-center',
												{}
											)}
										>
											<span className="product__newProductText align-self-center">
												{formatMessage({ id: 'new' })}
											</span>
										</div>
									) : null}

									<small
										className={classnames('text-secondary product__barcode', {
											'text-black': defaultUnderZero
										})}
									>
										{barcode}
									</small>
								</div>
								<div className="d-flex">
									{promoEnding ? (
										<div
											className={classnames(
												'mt-02 product__promoEnding d-flex justify-content-center',
												{}
											)}
										>
											<span className="product__promoEndingText d-flex justify-content-center align-items-center align-self-center">
												{customizer.direction == 'ltr' ? (
													<span>
														{promoEnding} {formatMessage({ id: 'daysUntilEndPromo' })}
													</span>
												) : (
													<span>
														{formatMessage({ id: 'remaining' })} {promoEnding}{' '}
														{formatMessage({ id: 'daysUntilEndPromo' })}
													</span>
												)}
											</span>
										</div>
									) : null}
									{!isPromo && isOutPromo>=0? (
										<div
											className={classnames(
												'mt-02 product__promoEnding d-flex justify-content-center',
												{}
											)}
										>
											<span className="product__promoEndingText d-flex justify-content-center align-items-center align-self-center">
												{customizer.direction == 'ltr' ? (
													<span>
														{promoEnding} {formatMessage({ id: 'daysUntilEndPromo' })}
													</span>
												) : (
													<div>
														{isOutPromo>7 && isOutPromo<14 ?  <span>{formatMessage({ id: 'hasBeekWeekAnd' })}-{isOutPromo} {formatMessage({ id: 'sincePromoEnded' })}</span> : isOutPromo<7 ? 
														<span>{formatMessage({ id: 'hasBeen' })} {isOutPromo} {formatMessage({ id: 'daysSincePromoEnded' })}</span>: isOutPromo==7? <span>{formatMessage({ id: 'beenWeekSincePromoEnded' })}</span> : <span>{formatMessage({ id: 'hasBeenTwoWeeksSincePromoEnded' })}</span>}
													</div>
												)}
											</span>
										</div>
									) : null}
								</div>
							</h4>
						</div>
						<button
							onClick={() => {
								openInfo();
							}}
							className="product__info"
						>
							<Icon src={config.iconsPath + 'general/info-icon.svg'} />
						</button>
					</header>
					{stateInfo ? (
						<ProductBackBody
							{...{
								name,
								lastBalance,
								stockOnTheWay,
								shelfData,
								packingFactor,
								type,
								salePredication,
								preTextData
							}}
						/>
					) : (
						<ProductFrontBody
							{...{
								barcode,
								type,
								promoData1,
								promoData2,
								name,
								lastBalance,
								promoToDate,
								promoFromDate,
								salePredication,
								preTextData
							}}
						/>
					)}
				</div>
				<footer
					ref={footerRef}
					onClick={() => {
						footerRef.current.focus();
						showOrderCounts();
					}}
					onMouseEnter={() => {
						if (footerTimerRef.current) {
							clearTimeout(footerTimerRef.current);
						}
					}}
					className="product__footer"
					onMouseLeave={() => {
						footerTimerRef.current = setTimeout(() => {
							hideOrderCounts();
							setDisplayInput(false);
						}, 3000);
					}}
					onBlur={() => {
						console.log('blur blur blur');
						footerTimerRef.current = setTimeout(() => {
							hideOrderCounts();
							setDisplayInput(false);
						}, 3000);
					}}
				>
					{!visibleCountOrder ? (
						<>
							{type === 'Specials' && countOrder === 0 ? (
								<span className="product__order-count">{formatMessage({ id: 'order' })}</span>
							) : (
								<span className="product__order-count">
									{countOrder} {formatMessage({ id: preTextOrderAmount })}{' '}
									{formatMessage({ id: 'orderAmount' })}
								</span>
							)}
						</>
					) : (
						<>
							<div className="product__order-buttons justify-content-center">
								<button onClick={decrement} className="product__btn-count product__btn-count_minus">
									<Icon src={config.iconsPath + 'general/minus.svg'} />
								</button>

								{displayInput ? (
									<input
										min="0"
										// max="1000"
										id={'inputId'}
										className="product__orderInput product__order-count m-2"
										ref={inputRef}
										defaultValue={countOrder > 0 ? countOrder : null}
										onInput={updateCountOrder}
										type="number"
										onBlur={()=>{sendItemToDraft()}}
									/>
								) : (
									<span onClick={() => setDisplayInput(true)} className="product__order-count m-2">
										{countOrder}
									</span>
								)}
								<button onClick={increment} className="product__btn-count product__btn-count_plus">
									<Icon src={config.iconsPath + 'general/plus.svg'} />
								</button>
							</div>
						</>
					)}
				</footer>
			</div>
		</li>
	);
};
