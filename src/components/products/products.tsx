import * as React from 'react';
import { Product } from './product';
import { OrderType } from '@src/containers/order';

import './products.scss';

interface Props {
	orders: any;
	// orders: OrderType[];
	type: string;
	displayAll?: boolean;
}

export const Products: React.FC<Props> = ({ orders, type, displayAll }) => {
	let amountDisplayed =
		!displayAll && orders ? orders.filter((i: any) => i.amountOrder1 > 0).length : orders ? orders.length : 0;
	// if()
	return (
		<div className="products">
			<ul
				className={`products__grid ${
					window.innerWidth > 1200 && amountDisplayed < 6 ? 'products__grid__small' : ''
				}`}
			>
				{orders && orders.length
					? orders.map((order: any) => {
							if (type === 'Specials') return <Product key={order.barcode} {...{ order, type }} />;
							else if (displayAll) return <Product key={order.barcode} {...{ order, type }} />;
							else if (!displayAll && order.amountOrder1 > 0)
								return <Product key={order.barcode} {...{ order, type }} />;
							else return null;
					  })
					: []}
			</ul>
		</div>
	);
};
