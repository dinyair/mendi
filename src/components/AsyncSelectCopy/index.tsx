import * as React from 'react';
import AsyncSelect from "react-select/async";

interface Props {
    name?: string,
    isDisabled?: boolean,
    isClearable?: boolean,
    styles: any,
    value: any,
    loadOptions: any,
    defaultOptions: any;
    onChange: (e: any, actionMeta: any) => void,
    noOptionsMessage?: string;
    placeholder: string;
    closeMenuOnSelect?: boolean;
    hideSelectedOptions?: boolean;
}





export const AsyncSelectCopy: React.FC<Props> = (props: Props) => {
    let { name,
        isDisabled,
        isClearable,
        styles, value, loadOptions, defaultOptions, onChange, noOptionsMessage, placeholder, closeMenuOnSelect,
        hideSelectedOptions } = props;

    const [_value, _setValue] = React.useState<any>();
    const [inputElement, setInputElement] = React.useState<any>(null);

    const showPlaceHolder = () => {
        if (inputElement === null) {
            const searchbar: any = document.getElementById('test-select');
            let input = searchbar.getElementsByTagName('input')[0]
            input.placeholder = placeholder
            setInputElement(input)
        } else {
            inputElement.placeholder = placeholder
        }
    }

    const setInputValue = (value:string | null) => {
        // const val =  
      //צ
    }

    const hidePlaceHolder = () => {
        inputElement.placeholder = ''

        // const searchbar: any = document.getElementById('test-select')
        // let input = searchbar.getElementsByTagName('input')
        // input[0].placeholder = ''
    }

    React.useEffect(() => {
        showPlaceHolder()
    }, [])


    return (
        <div id="test-select" >
            <AsyncSelect
                name={name}
                isDisabled={isDisabled ? isDisabled : false}
                isClearable={isClearable ? isClearable : true}
                styles={styles}
                value={value}
                loadOptions={loadOptions}
                defaultOptions={defaultOptions}
                onChange={(e: any, actionMeta: any) => {
                    onChange(e, actionMeta);

                    if (e) {_setValue(e); setInputValue(e.value)}
                    else {_setValue(null); showPlaceHolder()}
                }}
                closeMenuOnSelect={true}
                hideSelectedOptions={false}
                classNamePrefix='select'
                // formatMessage({ id: 'trending_productMinimumSearch' })
                noOptionsMessage={() => 'test'}
                onMenuOpen={() => {
                    if (_value) hidePlaceHolder()
                }}
                onMenuClose={() => {
                    if (!_value) showPlaceHolder()
                }}
                onInputChange={(newValue:any) => {
                    console.log(newValue)
                    if (!newValue && !_value) {showPlaceHolder() 
                        setInputValue(null)
                    }
                }}
                

            // placeholder={placeholder}
            />
        </div>

    );
};

export default AsyncSelectCopy;
