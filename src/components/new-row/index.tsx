import * as React from 'react';
import classnames from 'classnames'
import { DraggableProvided } from 'react-beautiful-dnd';
import { useSelector, useDispatch } from 'react-redux';
import { RootStore } from '@src/store';
import Popup from '../popup';
import { FormattedMessage, useIntl } from 'react-intl';
import { NewCell, Attachments } from '@components';
import { InputGroup, InputGroupText, InputGroupAddon, Button } from 'reactstrap';
import { XCircle, ArrowDownCircle} from "react-feather"
import { iconButtons } from '../reactstrap/buttons/ButtonsSourceCode';
import { fi } from 'date-fns/locale';
import { Select } from '@material-ui/core';

interface Props {
	readonly index: number;
	readonly data: any;
	readonly color: string;
	readonly selected: any[];								
	readonly mergedGroups: any[];								
	readonly hiddenColumns: any[];								
	readonly totalRow?: boolean								
	readonly showCheckBox: boolean								
	readonly showOptions: boolean								
	readonly selectRow: (id: number) => void;
	readonly onChange: (value: any, id: string | number, field: string) => void;
	readonly onBlur: (value: any, id: string | number, field: string) => void;
	readonly onDelete: (id: any) => void;
	readonly onKeyPress: (id: string) => void;
	readonly provided: DraggableProvided;
	readonly innerRef: any;
	readonly fields: any;
}

export const NewRow: React.FunctionComponent<Props> = (props: Props) => {
	const inputRef = React.useRef(null);
	const { formatMessage } = useIntl();
	const dispatch = useDispatch();
	const {  totalRow, mergedGroups,hiddenColumns, selected, selectRow, showOptions,showCheckBox, index, data, fields, onChange, onBlur, onDelete,  onKeyPress } = props
	const [open, setOpen] = React.useState(false);
	const [isEditable, setIsEditable] = React.useState<boolean>(false);
	const isReadonly = (field: string): boolean => [''].includes(field);
	const isOptions = (field: string): boolean => ['type', 'supplier_job_title'].includes(field);

	const Cell = (fi:number, field:string, type: string) => {
		return (
			<InputGroup key={fi} className={classnames('',{
				'width-7-rem': true,
				'width-12-rem': type == 'large_string',
				'display-hidden' : mergedGroups[field] || hiddenColumns.includes(field)
			})}>
			<NewCell
				isEditable={isEditable}
				// id={data.id}
				key={fi}
				bgColor={totalRow ? '#ececec' 
				: (index%2 == 0)? '#f3f3f5' :  'transparent'}
				type={type ? type : 'string'}
				field={field}
				value={(data as any)[field]}
				prefix={type  === 'percentage' ? '%' : ''}
				// suffix={type  === 'percentage' ? '%' : ''}
				onChange={!isOptions(field) ?
					(value:any)=> onChange(value, data.id,field)
				:
				(value:any)=>{
					onChange(value, data.id, field);
					if( field == 'supplier_job_title' ) setOpen(true)
					// else if ( field == 'type' ) setIsTypePopupOpen(true)
				}}
				onBlur={!isOptions(field) || !isReadonly(field) ? (value:any)=> onBlur(value, data.id, field) : ()=>{}}
				classnames={
				 totalRow  ? !data[field] && data[field] != 0 ? ['opacity-0'] 
				: ['font-small-3', 'text-center','text-black'] 
				: []}
				inputRef={index === 0 ? inputRef : null}
				isReadOnly={isReadonly(field)}
				// placeholder={}
			>
				
			
			</NewCell>
			</InputGroup>
		)
	}
	return (
		<div
			// ref={props.innerRef}
			className={classnames("n-row position-relative d-flex align-items-center",{
				'cursor-pointer': !isEditable
			})}
			onClick={!isEditable ? ()=> selectRow(data.id) : ()=>{}	}
			onKeyPress={event => {
				if( isEditable ) {
					if (event.key === 'Enter') {
						if(onKeyPress) onKeyPress(data.id)
					}
				}
			}}
			// {...props.provided.draggableProps}
			// {...props.provided.dragHandleProps}
		>
			{/* <span className="n-row__color" style={{ backgroundColor: props.color }} /> */}
			{showCheckBox && (
			<div className="position-absolute position-left-minus-1-rem  zindex-1 d-flex align-items-center">
				<input
					checked={selected.includes(data.id)}
					onClick={()=> selectRow(data.id)}
					type="checkbox" className="cursor-pointer width-075-rem bg-turquoise" ></input>
			</div>
			)}
				{Object.keys(mergedGroups).length ? (
					<div className="d-flex flex-column">
							<div className='d-flex flex-column width-8-rem height-3-rem'></div>
					</div>
				):null}
				{fields.map((field: any, fi: number) => field.category ? (
				<>
				{field.fields && field.fields.length ? (
					field.fields.map((field: any, ffi: number) =>
					Cell(fi+ffi, field.text, field.type)
				)):null}
				</>
				)	
				:	Cell(fi, field.text, field.type) 
				)}
			

			
			{showOptions && (
			<div className="c-table__cell-actions">
				<div className="cursor-pointer" onClick={()=> setIsEditable(!isEditable)}>
						{isEditable ? 
							<FormattedMessage id="save"/>
								:
							<FormattedMessage id="edit"/>
						}
				
				</div>
				<div className="cursor-pointer pl-1" onClick={(): void => props.onDelete(data.id)}><FormattedMessage id="delete"/></div>
			</div>
			)}
		</div>
	);
};

export default NewRow;
