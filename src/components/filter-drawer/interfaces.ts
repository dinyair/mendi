import { FormGroupType } from '@src/components/form-group';

export interface DrawerLayout {
	title: string;
	layoutId: string;
	formGroup: FormGroupType[];
	defaultOpen?: boolean;
	description? : string,
}

export interface FilterDrawerData {
	layouts: DrawerLayout[];
}
