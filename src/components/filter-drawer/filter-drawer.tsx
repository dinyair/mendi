import * as React from 'react';
import { Button } from 'reactstrap';

import { Drawer, SidePanel } from '@src/components/drawer';
import { FormGroup, Fields } from '@src/components/form-group';
import { Accordion } from "@src/components/accordion";
import { FilterDrawerData, DrawerLayout } from './interfaces';
import { useIntl } from "react-intl"
import { InputTypes } from "@src/components/form-group";
import { Spinner } from 'reactstrap'
import './filter-drawer.scss';


interface Props {
	required?: any;
	saveDisabled?: boolean;
	loadingBySave?: boolean
	caption: string;
	shown: boolean;
	onCancelButton?: () => void
	toggleModal: () => void;
	data: FilterDrawerData;
	saveChanges: (data: FilterDrawerData) => void;
	hideZeroFilters?: boolean;
	saveButtonText?: string
	cancelDisabled?: boolean
	modalBeforeClose?: boolean
	tempData?: any
	setTempData?: (tempData: any[]) => void;
	didDateChange?: (bool: boolean) => void
}

export const FilterDrawer: React.FC<Props> = ({ tempData, setTempData, didDateChange, caption, data, saveDisabled, cancelDisabled, shown, required, hideZeroFilters, toggleModal, saveChanges, onCancelButton, saveButtonText, loadingBySave, modalBeforeClose }) => {
	const { formatMessage } = useIntl();

	const [layouts, setLayouts] = React.useState<any>(data.layouts)
	const [dataChanges, setDataChanged] = React.useState<number>(0)
	const [openAcc, setOpenAcc] = React.useState<any>(false)

	const copyLayouts: DrawerLayout[] = React.useMemo(() => (
		JSON.parse(JSON.stringify(layouts))
	), [layouts]);

	React.useEffect(() => {
		setLayouts(data.layouts)
	}, [data])

	React.useEffect(() => {
		if (didDateChange) didDateChange(false)
	}, [])

	const refLayouts = React.useRef<{ [key: string]: DrawerLayout }>(copyLayouts.reduce((acc, curr) => ({
		...acc,
		[curr.layoutId]: curr
	}), {}));

	const onChangeData = (layoutId: string, formId: string, formData: Fields): void => {
		if (didDateChange) didDateChange(true)
		setDataChanged(dataChanges + 1)
		refLayouts.current[layoutId].formGroup = refLayouts.current[layoutId].formGroup.map(form => (
			form.formId === formId ? { ...form, ...formData } : form
		));

		let newLayout: any = []
		for (const key of Object.keys(refLayouts.current)) {
			newLayout.push(refLayouts.current[key])
		}
		setLayouts(newLayout)

	};

	const onSaveChanges = (): void => {
		if (required) {
			let IsAllFieldsAreFullfilled = IfAllFieldsAreFullfilled(required)
			if (IsAllFieldsAreFullfilled !== true) {
				setOpenAcc(false)
				setTimeout(() => {
					setOpenAcc(true)
					setLayouts(IsAllFieldsAreFullfilled)
				}, 5);

			} else {
				saveChanges({ layouts: Object.values(refLayouts.current) });
				toggleModal();
			}
		} else {

			saveChanges({ layouts: Object.values(refLayouts.current) });
			toggleModal();
		}
	};



	const IfAllFieldsAreFullfilled = (required: any[]) => {
		let filedsCount: number = 0;
		let FieldsAreFullfilled: number = 0;
		let valid = true;
		let layouts1 = Object.values(layouts)//refLayouts.current
		layouts1.forEach((layout: any, indexLayout: number) => {
			layout.formGroup.forEach((fields: any, formIndex: number) => {
				fields.fields.forEach((field: any, index: number) => {
					filedsCount++
					if (required.includes(field.name)) {
						if (field.type === InputTypes.PASSWORD && field.value.length > 0) {
							if (field.name === 'confirm password') {
								if (fields.fields[index - 1] && fields.fields[index - 1].value && fields.fields[index - 1].value === field.value) {
									layout.defaultOpen = false
									field.error = null
									FieldsAreFullfilled++;
								} else {
									layout.defaultOpen = true
									field.error = { label: formatMessage({ id: "passwords not match" }) };
								}
							}
						}
						else if (field.value && (field.value.length > 0 || field.value.value)) {
							layout.defaultOpen = false
							field.error = null
							FieldsAreFullfilled++;
						} else {
							layout.defaultOpen = true
							field.error = { label: formatMessage({ id: 'this input is required' }) };
						}
					}
					let fieldValidations = data.layouts[indexLayout].formGroup[formIndex].fields[index].validations
					if (fieldValidations) {
						let isValid = fieldValidations.map((v: any) => {
							if (v && v(field.value, 6)) return true
							return false
						})
						if (!isValid.includes(false)) {
							FieldsAreFullfilled++;
							layout.defaultOpen = false
							field.error = null
							valid = true;
						} else {
							valid = false
							layout.defaultOpen = true
							field.error = { label: formatMessage({ id: "password not strong enough" }) };
						}
					}
				})
			})
		})
		return FieldsAreFullfilled === required.length && valid ? true : layouts
	}

	if (shown) {
		return (
			<Drawer dataChanges={dataChanges} modalBeforeClose={modalBeforeClose} shown={shown} toggleModal={toggleModal}>
				<SidePanel onClose={toggleModal}>
					<div className='filter-drawer__body'>
						<h3 className='filter-drawer__title'>{caption}</h3>
						<div className='filter-drawer__accordion'>
							{layouts.map(({ layoutId, formGroup, title }) => (
								<Accordion openAcc={openAcc} key={layoutId} className='filter-drawer__form-group' caption={title} endArrow={true}>
									{formGroup.map(({ title, formId, fields, additionalFields }) => (
										<FormGroup key={formId} {...{ title, fields, additionalFields, id: layoutId, formId, onChangeData, tempData, setTempData }} />
									))}
								</Accordion>
							))}
						</div>
						<div className='edit-drawer__footer'>
							{/* {!hideZeroFilters && (
								
							)}onCancelButton */}
							{!hideZeroFilters ? onCancelButton ? <Button disabled={cancelDisabled} className='round mr-1' color='primary' outline onClick={() => {
								onCancelButton(); setTimeout(() => {
									toggleModal()
								}, 2);
							}}>{formatMessage({ id: 'zeroFilters' })}</Button> : <Button disabled={cancelDisabled} className='round mr-1' color='primary' outline onClick={toggleModal}>{formatMessage({ id: 'zeroFilters' })}</Button> : null}
							<Button disabled={saveDisabled ? true : false}
								className='round' onClick={onSaveChanges} color='primary'>{saveButtonText ? formatMessage({ id: saveButtonText }) : formatMessage({ id: 'save' })}</Button>
							{loadingBySave ? <div className='ml-1 mt-05'> <Spinner /> </div> : null}
						</div>
					</div>
				</SidePanel>
			</Drawer>
		)
	}

	return null;
}
