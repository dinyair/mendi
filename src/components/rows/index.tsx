    import * as React from 'react';
    import { Rows as ReduceRows,   NewRow } from '@components';
    import { Collapse } from '@material-ui/core';
    import { Icon } from '@components';
    import { config } from '../../config'
    
    interface Props {
        readonly index:number
        readonly key:number
        readonly first?:boolean
        readonly isRowsCollapsedProps?:any
        readonly data: any
        readonly selected: any[]
        readonly fields: any[]
        readonly mergedGroups: any[]
        readonly hiddenColumns: any[]
        readonly selectRow: (id: number) => void;
        readonly onChange: (value: any, id: string | number, field: string) => void;
        readonly onBlur: (value: any, id: string | number, field: string) => void;
        readonly onDelete: (id: any) => void;
        readonly onKeyPress: (id: string) => void;
    }
    
    export const Rows = (props: Props): React.ReactElement<HTMLButtonElement> => {
        const {  index, key, first, data, fields, mergedGroups,hiddenColumns,selected,selectRow, isRowsCollapsedProps, onChange, onBlur, onDelete, onKeyPress } = props
    
        const getHeader = (data:any, header:string) => {
            return  data.rows ? data.rows[0].rows ? getHeader(data.rows[0],header) : data.rows[0][header] : null
        }

   
        const HeaderRow = (data: any, header:string, index:number) => {
        // let row = !!data && data.constructor == Array ?  data[0] : !data[fields[0]] ? Object.entries(data)[1] : data
        
        const reduceFields = (rows:any[], field: string) => {
            return rows.reduce((acc: number, row: any) => {
                if (row[field] === undefined)  return acc + 0;
                return acc + row[field]
          }, 0)
        }


        return(
            <div className="d-flex">
            <div
                            key={'header-'+index}
                            onClick={true ?
                             ()=>{}
                            : ()=> {}}
                            className="width-12-rem position-absolute d-flex mb-05  align-items-center border-bottom border-top"
                            style={{padding: '0.5rem ' + (Number(index)*1.1)+'rem'}}>
                                <div className="zindex-2 cursor-pointer font-small-3 text-bold-600  text-black custom-control-info  d-flex align-items-center position-relative">
                                    <Icon src={config.iconsPath+"table/arrow-left.svg"} 
                                    className="mr-05" style={{height:'0.5rem', width: '0.5rem'}}/>
                                    {getHeader(data, header)}
                                </div>
            </div>
                     <NewRow
                                  totalRow
                                  key={index}
                                  selected={selected}
                                  selectRow={selectRow}
                                  hiddenColumns={hiddenColumns}
                                  mergedGroups={mergedGroups}
                                  index={index}
                                  data={{
                                      
                                    "TotalPrice": reduceFields(data.rows, "TotalPrice"),
                                    "Percent_TotalPrice": reduceFields(data.rows, "Percent_TotalPrice"),
                                    "TotalAmount": reduceFields(data.rows, "TotalAmount"),
                                    "Percent_TotalAmount": reduceFields(data.rows, "Percent_TotalAmount"),
                                    "Percent_Avg" : reduceFields(data.rows, "Percent_Avg"),
                                    "Percent_Profit" : reduceFields(data.rows, "Percent_Profit"),
                                    "Percent_MarketAmount" : reduceFields(data.rows, "Percent_MarketAmount")
                                    }}
                                  fields={fields}
                         />	
            </div>
          
        )}
    
        // let  Data = !!data && data.constructor === Array ? Object.entries(data) : data
    
        return (<>
    
        {!!data && data.constructor == Array && data[fields[0]] > 1 ? 
                <>
                 {true && (
                  data.map((data: any, key: number) => { 
                    
                    if(!!data && data.constructor == Array ) {
                        return (
                            <>
                           <Collapse key={key} in={data.collapsed} >
                                {data.map((data: any, index: number) => 
                                (
                                        <NewRow
                                                showOptions
                                                showCheckBox
                                                key={index}
                                                index={index}
                                                data={data}
                                                selected={selected}
                                                selectRow={selectRow}
                                                hiddenColumns={hiddenColumns}
                                                mergedGroups={mergedGroups}
                                                // color={data.color}
                                                fields={fields}
                                                onDelete={onDelete}
                                                onKeyPress={onKeyPress}
                                                onChange={onChange}
                                                onBlur={onBlur}
                                        />
                                
                                    ))}
                             </Collapse>
                             </>
                            )}
                           else return (
                               <ReduceRows
                                    key={key}
                                    index={key}
                                    mergedGroups={mergedGroups}
                                    data={data[mergedGroups[index]]}
                                    fields={fields}
                                    onDelete={onDelete}
                                    onKeyPress={onKeyPress}
                                    onChange={onChange}
                                    onBlur={onBlur}
                               />
                            ) 
                    }
                    ))}
                </>
           :
           <>
       {(Object.keys(mergedGroups).map((header:string, key:number) => ( HeaderRow(data, header, key) )))}
            <NewRow
                showOptions
                showCheckBox
                key={index}
                index={index}
                data={data}
                // color={data.color}
                selected={selected}
                selectRow={selectRow}
                fields={fields}
                hiddenColumns={hiddenColumns}
                mergedGroups={mergedGroups}
                onDelete={onDelete}
                onKeyPress={onKeyPress}
                onChange={onChange}
                onBlur={onBlur}
            />
            </> }
                           
        </>);
    };
    
    export default Rows;
    