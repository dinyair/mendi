import * as React from "react";
import { CustomInput } from "reactstrap"
import { Option } from "./combo-box";
import { useIntl } from "react-intl"

import './combo-box.scss';


interface Props {
	option: Option;
	onChangeOption: (option: Option) => void;
}

export const ComboBoxItem: React.FC<Props> = ({ option, onChangeOption }) => {
	const { formatMessage } = useIntl();

	const onChange = () => {
		onChangeOption({ name: option.name, checked: !option.checked });
	}

	return (
		<li className='combobox__drop-down_item'>
			<label className='combobox__drop-down_label' htmlFor={option.name}>
				<CustomInput id={option.name} onChange={onChange} type='checkbox' value={option.name} checked={option.checked} />
				{option.name ? option.name : formatMessage({ id: 'other' })}
			</label>
		</li>
	)
}
