import * as React from "react";
import classnames from "classnames";
import { X, Check } from "react-feather";
import { ComboBoxItem } from "./item";
import { useIntl } from "react-intl";

import './combo-box.scss';

export interface Option {
	name: string;
	checked: boolean;
}

interface Props {
	onChange: (e: string[]) => void;
	onHide: () => void;
	options: string[];
	childRef: { current: HTMLUListElement | null };
	checkedIds: string[];
	oneOption?: boolean;
	setItemsOrderCheckBoxCaption?: (caption: any) => void;
}

const checkBoxState = {
	CHECKED: 'checked',
	UNCHECKED: 'unchecked',
	HALF_CHECKED: 'half-checked'
}

export const CheckBoxTree: React.FC<Props> = ({ childRef, onHide, onChange, options, checkedIds, oneOption, setItemsOrderCheckBoxCaption }) => {
	const { formatMessage } = useIntl();
	const [optionsTree, updateTree] = React.useState<Option[]>(() => options.map(option => ({

		name: option,
		checked: checkedIds.some(id => id === option)
	})));

	
	const [checkedAll, selectAll] = React.useState(() => {
		if (options.length === checkedIds.length) {
			return checkBoxState.CHECKED;
		} else if (checkedIds.length) {
			return checkBoxState.HALF_CHECKED;
		}
		return checkBoxState.UNCHECKED;
	});

	const onChangeOption = (option: Option) => {
		const mapChecked = optionsTree.map(opt => (
			opt.name === option.name ? option : opt
		));

		updateTree(mapChecked);

		if (mapChecked.every(opt => opt.checked)) {
			selectAll(checkBoxState.CHECKED);
		} else if (mapChecked.every(opt => !opt.checked)) {
			selectAll(checkBoxState.UNCHECKED);
		} else {
			selectAll(checkBoxState.HALF_CHECKED);
		}
	}

	const toggleAll = () => {
		selectAll(prevValue => {
			if (prevValue === checkBoxState.CHECKED) {
				const updatedTree = optionsTree.map(option => ({
					...option,
					checked: false
				}));

				updateTree(updatedTree);
				return checkBoxState.UNCHECKED
			} else {
				const updatedTree = optionsTree.map(option => ({
					...option,
					checked: true
				}))
				updateTree(updatedTree);
				return checkBoxState.CHECKED
			}
		});
	}

	const applyChanges = () => {

		const checked = optionsTree.filter(option => option.checked).map(option => option.name)

		if (oneOption) {
			if (checked.length > 0 && setItemsOrderCheckBoxCaption) {
				setItemsOrderCheckBoxCaption(formatMessage({ id: 'allItems' }))
			} else if (checked.length === 0 && setItemsOrderCheckBoxCaption) {
				setItemsOrderCheckBoxCaption(formatMessage({ id: 'itemsToOrder' }))
			}
		}
		onChange(checked);
		updateTree(optionsTree);
		onHide();
	}

	const customCheckbox = classnames('combobox__drop-down_custom-checkbox', {
		[checkBoxState.CHECKED]: checkedAll === checkBoxState.CHECKED,
		[checkBoxState.UNCHECKED]: checkedAll === checkBoxState.UNCHECKED,
		[checkBoxState.HALF_CHECKED]: checkedAll === checkBoxState.HALF_CHECKED,
	});

	return (
		<ul ref={childRef} className='combobox__drop-down'>
			<li className='combobox__drop-down_item'>
				{!oneOption &&
					<div onClick={toggleAll} className='combobox__drop-down_label'>
						<span className={customCheckbox} />
						{formatMessage({ id: 'selectAll' })}
					</div>}
			</li>
			{optionsTree.map(option => (
				<ComboBoxItem key={option.name} option={option} onChangeOption={onChangeOption} />
			))}
			<li className='combobox__drop-down_item action-buttons'>
				<button onClick={applyChanges} className='action-button d-flex justify-content-center align-items-center'>
					<Check size='16px' color='#fff' />
				</button>
				<button onClick={onHide} className='action-button check-btn d-flex justify-content-center align-items-center'>
					<X size='16px' color='#31baab' />
				</button>
			</li>
		</ul>
	)
}
