import * as React from "react";
import classnames from "classnames";
import {ChevronDown} from "react-feather";

import './combo-box.scss';

export interface Option {
	name: string;
	checked: boolean;
}

interface Props {
	caption: string;
	mix?: string;
	children: (
		onHide: () => void,
		childRef: {current: HTMLUListElement | null}
	) => React.ReactChild;
}

export const ComboBox: React.FC<Props> = props => {
	const {children, caption, mix = ''} = props;
	const [focus, setFocus] = React.useState(false);
	const childRef = React.useRef<HTMLUListElement>(null);
	const currentRef = React.useRef<HTMLDivElement>(null);

	const onHide = () => {
		setFocus(false);
		// eslint-disable-next-line @typescript-eslint/no-use-before-define
		document.removeEventListener('click', closeChild);
	}

	const closeChild = (e: Event) => {
		const {target} = e;
		if (target instanceof HTMLElement) {
			if (!childRef.current?.contains(target) && !currentRef.current?.contains(target)) {
				onHide();
			}
		}
	}

	const onFocus = (e: React.MouseEvent<HTMLButtonElement>) => {
		const {target} = e;
		setFocus(true);
		if (target instanceof HTMLElement && childRef.current && currentRef.current?.contains(target)) {
			onHide();
		} else {
			document.addEventListener('click', closeChild);
		}
	}

	const classes = classnames('combobox__select-btn', {
		focus
	});

	const classesNames = classnames('combobox', 'mb-2', 'w-100', 'mr-1', {
		[mix]: mix
	});

	return (
		<div ref={currentRef} className={classesNames}>
			<button onClick={onFocus} className={classes}>
				<span className="elipsis max-width-10-rem max-height-2-rem">{caption}</span>
				<ChevronDown size={16} />
			</button>
			{focus && children(onHide, childRef)}
		</div>
	)
}
