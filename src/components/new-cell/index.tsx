import * as React from 'react';
import { Input } from 'reactstrap';
import { useIntl } from 'react-intl';

interface Props {
	readonly id: any;
	readonly isEditable?: boolean;
	readonly type: 'string' | 'large_string' |  'number' | 'percentage' | 'dropdown' | 'action' ;
	readonly field: string;
	readonly value?: string | number;
	readonly prefix?: string;
	readonly bgColor?: string;
	readonly suffix?: string;
	readonly onChange: (value: any) => void;
	readonly onClick?: () => void;
	readonly onBlur: ( (value: any) => void ) |  null ;
	readonly onKeyPress?: (id: string) => void;
	readonly inputRef: any;
	readonly children?: any;
	readonly isPricePrimCellVisible?: boolean;
	readonly isReadOnly?: boolean;
	readonly placeholder?: any;
	readonly disabled?: boolean;
	readonly classnames? :any[]
	readonly invalid?: undefined | Boolean
}

export const NewCell: React.FunctionComponent<Props> = (props: Props) => {
	const {
		id,
		type,
		field,
		value,
		bgColor,
		prefix,
		suffix,
		onChange,
		onClick,
		onBlur,
		onKeyPress,
		inputRef,
		children,
		placeholder,
		isEditable,
		isPricePrimCellVisible,
		isReadOnly,
		classnames,
		invalid,
	} = props;
    const { formatMessage } = useIntl();
	return  (
		<div className={`p-1 height-3-rem d-flex align-items-center font-small-2 text-bold-700 ${classnames ? classnames.toString().replace(',',' ')	: null}`}
		style={{backgroundColor: bgColor}}
		onClick={onClick ? ()=>onClick() : ()=>{}}>
			{prefix}&nbsp;
			{type && ['string','large_string', 'number', 'percentage'].includes(type) ? (
				<Input
					ref={inputRef}
					className={`p-0 height-3-rem bg-transparent no-border font-small-3 text-bold-700 sqaure form-control border-left pr-0 `} 
					style={{borderRadius:0}}
					// invalid={invalid}
					type={['string','large_string'].includes(type) ? 'text' : 'number'}
					value={value}
					onKeyPress={event => {
						if (event.key === 'Enter') {
							if( onKeyPress ) onKeyPress(id)
						}
					}}
					onBlur={({ target }: React.ChangeEvent<HTMLInputElement>): void =>{
						if( onBlur ) onBlur(target.value)
					}}
					onChange={({ target }: React.ChangeEvent<HTMLInputElement>): void => {
						onChange(target.value)
					}}
					placeholder={String(formatMessage({id: field}))}
					readOnly={isReadOnly}
					disabled={!isEditable}
				/>
			) : null}
			{children}
			{suffix}
		</div>
	);
};

NewCell.defaultProps = {
	placeholder: '',
	disabled: false
} as Partial<Props>;

export default NewCell;
