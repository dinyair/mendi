import * as React from 'react';

import './index.scss';
import { useOutsideClick } from '@src/utilities';
import { useIntl } from 'react-intl';
import { Check, PlusCircle } from 'react-feather';
import { NavLink  } from 'react-router-dom';
import { Icon } from '@components';
import { config } from '@src/config'
import { Divider } from '@material-ui/core'
import classnames from "classnames"

interface Option {
	readonly text: string;
	readonly disabled?: boolean;
	readonly className?: string | null;
	readonly link?: any;
	readonly action: () => void;
	readonly removeSelect?: () => void;
	readonly selectAll?: () => void;
	readonly deSelectAll?: () => void;
}

interface Props {
	readonly isOpen: boolean;
	readonly options?: Option[];
	readonly varient?: 'checkbox' | 'navLinks';
	readonly multiple?: boolean;
	readonly search?: boolean;
	readonly divider?: boolean;
	readonly add?: any;
	readonly selected?: any[];
	readonly onClick: (e: Event) => void;
	readonly onOutsideClick: () => void;
	readonly className?: string;
	readonly style?: any;
	readonly children?: any;
	readonly onAddField?: (fieldValue: string) => void;
}

export const Popup: React.FC<Props> = (props: Props) => {
	const customizer = { direction: document.getElementsByTagName("html")[0].dir }
	const { formatMessage } = useIntl();
	const ref: React.MutableRefObject<any> = React.useRef(null);
	const {search, options, varient, add, multiple,selected, isOpen, onOutsideClick,divider, className,style, children, onAddField } = props;
	const [newFieldValue, setNewFieldValue] = React.useState('');
	const [filter, setFilter] = React.useState<string>('');
	/* eslint-disable @typescript-eslint/no-non-null-assertion */
	useOutsideClick(ref, onOutsideClick!);
	return isOpen && options && options.length ? (
		<div className={`select__control c-popup ${className === undefined ? '' : className}`} style={style ? style : {}} ref={ref}>
			<ul className={classnames("c-popup__items max-height-40vh overflow-auto width-100-per", {
						"d-rtl": customizer.direction == 'rtl',
					})}>
			
				{children
					? children																																																															
					: <>
					{search && (
							<>
						<div className="width-90-per mx-auto d-flex align-items-center pl-1 mt-05 mb-05 height-2-rem py-1 border-radius-2rem" style={{border: '1px solid black'}}>
						<Icon  src={'../../'+config.iconsPath+"planogram/sidebar/search.svg"} 
							style={{height: '0.5rem', width: '0.5rem'}}/>

							<input type="text"  placeholder={formatMessage({id: 'search'})} className="width-100-per no-border placeholder-text-bold-700 font-small-3 placeholder-text-black text-bold-700 ml-05" 
										onChange={(e:any)=> setFilter(e.target.value.toLowerCase())}></input>
						</div>
					
							</>
					)}
					{add && (
							<>
						<div className="width-90-per mx-auto d-flex align-items-center pl-1 mt-05 mb-05 height-2-rem py-1" 
						onClick={()=>add.onClick()}>
						<Icon  src={'../../'+config.iconsPath+"planogram/add.svg"} 
							style={{height: '0.5rem', width: '0.5rem'}}/>
							<div className="ml-05 text-turquoise font-small-3">{add.text}</div>
						</div>
					
							</>
					)}
					{options!.map((option: Option, key: number) => {
							const isNew = option.text === formatMessage({id: 'add_new'});
							const isSelectAll = option.text === formatMessage({id: 'select_all'})
							const isSelected = selected ? varient === 'checkbox' ? !selected.includes(option.text)  : selected.includes(option.text) : false
							if(option.disabled || (filter.length && !option.text.toLowerCase().includes(filter) && (option.text !== formatMessage({id: 'select_all'}) || option.text == formatMessage({id: 'select_all'})))  ) {return} 
							return (<>
								<li
									key={key}
									onClick={isSelectAll ?
										selected && selected.length === options.length ?  option.deSelectAll : option.selectAll :
										isNew ? (): void => void 0 : isSelected ? option.removeSelect :  option.action}
									className={`c-popup__item cursor-pointer font-medium-1 d-flex align-items-center text-left ${isNew ? 'c-popup__item--with-input' : ''} ${option.className ? option.className : ''}`}	//c-popup__item	
						>

								{multiple ?
								 varient && varient === 'checkbox' ?
									<input type="checkbox" className="width-1-rem height-1-rem mr-05 bg-turquoise" 
									onClick={isSelectAll ?
										selected && selected.length === options.length ?  option.deSelectAll : option.selectAll :
										isNew ? (): void => void 0 : isSelected ? option.removeSelect :  option.action}
									disabled={false}
									checked={isSelected}
									></input>
								:
								isSelected ? <Check /> : null
								: null}
									{varient === 'navLinks' ? <NavLink to={option.link}><div className={`width-100-per ${option.className ? option.className :'' }`}>{option.text}</div></NavLink>
									:
									isNew ? 
									<div className={`width-100-per ${option.className ? option.className :'' }`}>{option.text}</div>
									:option.text}
									{isNew && typeof onAddField === 'function' && (
										<>
											<input
												onChange={(event: React.ChangeEvent<HTMLInputElement>): void => {
													setNewFieldValue(event.target.value);
												}}
												type="text"
												value={newFieldValue}
												placeholder={formatMessage({id: 'add_new'})+'...'}
											/>
											<button
												onClick={(): void => {
													onAddField(newFieldValue!), setNewFieldValue('');
												}}
											></button>
										</>
									)}
								</li>
							{divider? 	<Divider className="width-90-per mt-0 mb-0 mx-auto bg-gray"/> : null}
							</>);
					  })}
					  
					  </>
					  }
			</ul>
		</div>
	) : null;
};

export default Popup;
