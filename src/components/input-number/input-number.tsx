import * as React from "react";
import classnames from "classnames"
import { useIntl } from "react-intl";

import './input-number.scss';

interface Props {
	onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
	placeholder?: any;
	inputRef?: any;
	inputClassNames?: string;
	classNames?: string;
	id?: any;
	value?: string | number;
	onKeyDown?: (e: React.ChangeEvent<HTMLInputElement>) => void
	type?: string;
	disabled?: boolean;
	hideFocus?: boolean;
	inputHeight?: string;
	error?: boolean;
	errorLabel?: string;
	oneDigitInput?: boolean;
	onFocusEvent?: (e: any) => void;
	showFocus?: boolean;
	validateNumbersOnly?: boolean;
};

export const InputNumber: React.FC<Props> = ({ onChange, onKeyDown, onFocusEvent, showFocus, placeholder, inputRef, id, value, inputClassNames, disabled, hideFocus, classNames, inputHeight, error, errorLabel, oneDigitInput, validateNumbersOnly }) => {
	const { formatMessage } = useIntl();
	const [focus, setFocus] = React.useState(false);

	React.useEffect(() => {
		showFocus ? setFocus(true) : null
	}, [showFocus])

	React.useEffect(() => {
		hideFocus ? setFocus(false) : null
	}, [hideFocus])


	const onFocus = () => {
		!hideFocus && setFocus(true);
		onFocusEvent ? onFocusEvent(true) : null
	}

	const onBlur = () => {
		setFocus(false);
		onFocusEvent ? onFocusEvent(false) : null

	}

	const classes = classnames(oneDigitInput ? 'input-digit' : 'input-number', classNames ? classNames : null, error ? 'red-border ' : '', {
		focus
	});

	const validateNumbers = (e: any) => {
		let key;

		key = e.keyCode || e.which;
		key = String.fromCharCode(key);
		if (!/[0-9]|\./.test(key) || key === '.') {
			e.returnValue = false;
			if (e.preventDefault) e.preventDefault();
		}
	}

	return (
		<div className="d-flex flex-column justify-content-center width-100-per">
			<div className={classes}>
				<input type='number' onWheel={(e) => e.currentTarget.blur()} min={0} disabled={disabled ? disabled : false}
					max={oneDigitInput ? 9 : undefined}
					className={`${inputClassNames ? inputClassNames : ''} ${disabled ? 'disabled-background disabled-color' : ''}`}
					style={{ height: inputHeight ? inputHeight : '38px' }}
					id={id} value={value != null ? value : undefined}
					onChange={(e: any) => { onChange(e); if (onKeyDown) onKeyDown(e.currentTarget.value) }}
					onKeyPress={(e: any) => {
						if (validateNumbersOnly) validateNumbers(e)
					}}
					onPaste={(e: any) => {
						if (validateNumbersOnly) validateNumbers(e)
					}}
					onFocus={onFocus} onBlur={onBlur} ref={inputRef ? inputRef : null}
					placeholder={placeholder ? placeholder : ''} />
			</div>
			{errorLabel ? <div className="ml-05 text-bold-600 font-small-2 text-danger min-height-1-rem max-height-1-rem">{errorLabel}</div> : null}
		</div>
	)
}
