import * as React from 'react';
import classnames from 'classnames';
import { FormGroup, Fields } from '@src/components/form-group';
import { TabContent, TabPane, Button, Spinner } from 'reactstrap';
import { useIntl } from 'react-intl';
import { drawerLayout, DrawerTab, drawerData } from './interfaces';
import { InputTypes } from '@src/components/form-group';
import { Tabs, Tab } from '@src/components/tabs';
import { config } from '@src/config';
import { Drawer, SidePanel } from '@src/components/drawer';
import { Accordion } from '@src/components/accordion';
import './generic-drawer.scss';

interface Props {
	didDateChange?: (bool: boolean) => void;
	saveChanges: (data: drawerData, caption?: any) => void;
	toggleModal: () => void;
	onCancelButton?: () => void;
	setTempData?: (tempData: any[]) => void;
	tempData?: any;
	shown: boolean;
	drawerType: 'ACCORDION' | 'TAB';
	data: any;
	required?: any;
	caption?: string;
	isCaptionText?: boolean;
	validateCaption?: string;
	modalBeforeClose?: boolean;
	hideZeroFilters?: boolean;
	cancelDisabled?: boolean;
	saveDisabled?: boolean;
	loadingBySave?: boolean;
	saveButtonText?: string;
	titleEditable?: boolean;
	isTabsHidden?: boolean;
	alignButtonsLeft?: boolean;
	edit?: boolean;
	customFieldsValdtion?:(data:any)=> void
}

const dType = {
	ACCORDION: 'ACCORDION',
	TAB: 'TAB'
};
export const GenericDrawer: React.FC<Props> = (props: Props) => {
	const {
		saveChanges,
		toggleModal,
		didDateChange,
		onCancelButton,
		setTempData,
		tempData,
		drawerType,
		data,
		required,
		caption,
		isCaptionText,
		validateCaption,
		shown,
		modalBeforeClose,
		hideZeroFilters,
		cancelDisabled,
		loadingBySave,
		saveButtonText,
		titleEditable,
		isTabsHidden,
		alignButtonsLeft,
		customFieldsValdtion,
		edit
	} = props;

	const { formatMessage } = useIntl();
	const [saveDisabled, setSaveDisabled] = React.useState<boolean>(props.saveDisabled || false);
	const [activeDrawer, setActiveDrawer] = React.useState<any>(drawerType === dType.TAB ? dType.TAB : dType.ACCORDION);
	const [layouts, setLayouts] = React.useState<any>(drawerType === dType.ACCORDION && data.layouts);
	const [editedLayouts, setEditedLayouts] = React.useState<any>(
		activeDrawer === dType.ACCORDION ? data.layouts : data.tabs
	);
	const [tabs, setTabs] = React.useState<any>(drawerType === dType.TAB && data.tabs);
	const [editedTabs, setEditedTabs] = React.useState<any>(drawerType === dType.TAB && data.tabs);
	const [activeTab, setActiveTab] = React.useState(drawerType === dType.TAB ? tabs[0].tabId : 0);

	const [dataChanges, setDataChanged] = React.useState<number>(0);
	const [openAcc, setOpenAcc] = React.useState<any>(false);
	const [currentValue, setValue] = React.useState(isCaptionText ? (caption ? caption : '') : '');
	const [captionError, setCaptionError] = React.useState<boolean>(false);
	const [isEdit, setEdit] = React.useState(false);
	const [selectedSearch, setSelectedSearch] = React.useState<any>('');
	const inputRef = React.useRef<HTMLInputElement>(null);

	React.useEffect(() => {
		if (didDateChange) didDateChange(false);
	}, []);

	React.useEffect(() => {
		if (isEdit && inputRef.current) {
			inputRef.current.focus();
		}
	}, [isEdit]);


	const copyDrawerData: drawerLayout[] =
		activeDrawer === dType.ACCORDION
			? React.useMemo(() => layouts, [layouts])
			: React.useMemo(() => editedTabs, [editedTabs]);

	let drawerRef =
		activeDrawer === dType.ACCORDION
			? React.useRef<{ [key: string]: drawerLayout }>(
					copyDrawerData.reduce(
						(acc, curr) => ({
							...acc,
							[curr.layoutId]: curr
						}),
						{}
					)
			  )
			: React.useRef<{ [key: string]: DrawerTab }>(
					copyDrawerData.reduce(
						(acc, curr) => ({
							...acc,
							[curr.tabId]: curr
						}),
						{}
					)
			  );

	React.useEffect(() => {
		if (activeDrawer === dType.ACCORDION) {
			setLayouts(data.layouts);
			let layoutWithNewOptions = data.layouts;
			let newEditLayouts =
				layoutWithNewOptions && layoutWithNewOptions.length && editedLayouts && editedLayouts.length
					? editedLayouts.map((layout: any, mainIndex: number) => {
							return {
								...layout,
								formGroup: layout.formGroup.map((formGroup: any, formIndex: number) => {
									return {
										...formGroup,
										fields: formGroup.fields.map((field: any, fieldIndex: number) => {
											// let newOptions =
											// 	layoutWithNewOptions[mainIndex].formGroup[formIndex].fields[fieldIndex]
											// 		.options;
											// if (newOptions && field.options.length != newOptions.length) {
											// 	return { ...field, options: newOptions };
											// } else return field;
											return {
												...layoutWithNewOptions[mainIndex].formGroup[formIndex].fields[
													fieldIndex
												],
												value: field.value,
												error: field.error,
												defaultValue: field.defaultValue
											};
										})
									};
									// return formGroup
								})
							};
							// return layout
					  })
					: editedLayouts;
			setEditedLayouts(newEditLayouts);
		} else {
			let tabsWithNewOptions = data.tabs;
			let newEditTabs =
				tabsWithNewOptions && tabsWithNewOptions.length && editedTabs && editedTabs.length
					? editedTabs.map((tab: any, mainIndex: number) => {
							return {
								...tab,
								formGroup: tab.formGroup.map((formGroup: any, formIndex: number) => {
									return {
										...formGroup,
										fields: formGroup.fields.map((field: any, fieldIndex: number) => {
											// let newOptions =
											// tabsWithNewOptions[mainIndex].formGroup[formIndex].fields[fieldIndex]
											// 		.options;

											// if (newOptions && field.options.length != newOptions.length) {

											return {
												...tabsWithNewOptions[mainIndex].formGroup[formIndex].fields[
													fieldIndex
												],
												value: field.value,
												error: field.error,
												defaultValue: field.defaultValue

											};
											// } else return field;
										})
									};
									// return formGroup
								})
							};
							// return layout
					  })
					: editedTabs;

			setTabs(newEditTabs);
			setEditedTabs(newEditTabs);
		}
	}, [data]);

	const onChangeData = (id: string, formId: string, formData: Fields): void => {
		setDataChanged(dataChanges + 1);
		drawerRef.current[id].formGroup = drawerRef.current[id].formGroup.map(form =>
			form.formId === formId ? { ...form, ...formData } : form
		);

		if (activeDrawer === dType.ACCORDION) {
			if (didDateChange) didDateChange(true);
			let newLayout: any = [];
			for (const key of Object.keys(drawerRef.current)) {
				newLayout.push(drawerRef.current[key]);
			}
			setLayouts(newLayout);
			setEditedLayouts(newLayout);
		} else {
			let newTabs = [];
			for (const key of Object.keys(drawerRef.current)) {
				newTabs.push(drawerRef.current[key]);
			}
			setTabs(newTabs);
			setEditedTabs(newTabs);
		}
	};

	const onSaveChanges = (): void => {
		if (required) {
			setCaptionError(false);
			let IsAllFieldsAreFullfilled:any = true;
			if(customFieldsValdtion) {
				IsAllFieldsAreFullfilled = customFieldsValdtion(Object.values(activeDrawer === dType.ACCORDION ? editedLayouts:editedTabs))
			}else{
			IsAllFieldsAreFullfilled =
				activeDrawer === dType.ACCORDION ? validateDataAccordion(required) : validateDataTabs(required);
			}
			if (IsAllFieldsAreFullfilled != true) {
				setOpenAcc(false);
				setTimeout(() => {
					setOpenAcc(true);
					if (activeDrawer === dType.ACCORDION) {
						setLayouts(IsAllFieldsAreFullfilled)
						setEditedLayouts(IsAllFieldsAreFullfilled);
					} else {
						setEditedTabs(IsAllFieldsAreFullfilled);
						setTabs(IsAllFieldsAreFullfilled);
					}
				}, 5);
				if ((validateCaption && !currentValue) || (currentValue && currentValue.length < 4)) {
					setCaptionError(true);
				}
			} else if ((validateCaption && !currentValue) || (currentValue && currentValue.length < 4)) {
				setCaptionError(true);
			} else {
				activeDrawer === dType.ACCORDION
					? saveChanges({ layouts: Object.values(drawerRef.current) })
					: saveChanges(
							{ tabs: Object.values(drawerRef.current) },
							caption ? { caption: currentValue } : null
					  );

				toggleModal();
			}
		} else if ((validateCaption && !currentValue) || (validateCaption && currentValue && currentValue.length < 4)) {
			setCaptionError(true);
		} else {
			activeDrawer === dType.ACCORDION
				? saveChanges({ layouts: Object.values(drawerRef.current) })
				: saveChanges({ tabs: Object.values(drawerRef.current) }, caption ? { caption: currentValue } : null);
			toggleModal();
		}
	};

	const onChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
		setDataChanged(dataChanges + 1);
		setValue(e.target.value);
	};

	const onCanEdit = (): void => {
		setEdit(true);
	};

	const offCanEdit = (): void => {
		setEdit(false);
	};

	const onKeyDown = (event: React.KeyboardEvent<HTMLInputElement>): void => {
		if (event.key === 'Enter') {
			offCanEdit();
		}
	};

	const validateDataAccordion = (required: any[]) => {
		let additionalValidaiton = 0;
		let filedsCount: number = 0;
		let FieldsAreFullfilled: number = 0;
		let valid = true;
		editedLayouts.forEach((layout: any, indexLayout: number) => {
			layout.formGroup.forEach((fields: any, formIndex: number) => {
				fields.fields.forEach((field: any, index: number) => {
					filedsCount++;
					if (required.includes(field.name) || (edit && field.name === 'confirm password')) {
						if (field.type === InputTypes.PASSWORD && field.value.length > 0) {
							if (field.name === 'confirm password') {
								if (
									fields.fields[index - 1] &&
									fields.fields[index - 1].value &&
									fields.fields[index - 1].value === field.value
								) {
									layout.defaultOpen = false;
									field.error = null;
									FieldsAreFullfilled++;
								} else {
									layout.defaultOpen = true;
									field.error = { label: formatMessage({ id: 'passwords not match' }) };
								}
							}
						} else if (field.value && (field.value.length > 0 || field.value.value)) {
							layout.defaultOpen = false;
							field.error = null;
							FieldsAreFullfilled++;
						} else {
							layout.defaultOpen = true;
							field.error = { label: formatMessage({ id: 'this input is required' }) };
						}
					}
					let fieldValidations = data.layouts[indexLayout].formGroup[formIndex].fields[index].validations;

					if (
						(fieldValidations && !edit) ||
						(edit && field.value.length > 0 && fieldValidations && field.type === InputTypes.PASSWORD)
					) {
						let isValid = fieldValidations.map((v: any) => {
							if (v && v(field.value, 6)) return true;
							return false;
						});

						if (!isValid.includes(false)) {
							if (!required.includes('password')) {
								additionalValidaiton += 2;
							}
							FieldsAreFullfilled++;
							layout.defaultOpen = false;
							field.error = null;
							valid = true;
						} else {
							valid = false;
							layout.defaultOpen = true;
							field.error = { label: formatMessage({ id: 'password not strong enough' }) };
						}
					}
				});
			});
		});
		return FieldsAreFullfilled === required.length + additionalValidaiton && valid ? true : editedLayouts;
	};
	const validateDataTabs = (required: any[]) => {
		let FieldsAreFullfilled: number = 0;
		let optionsRequiredLength: number = 0;
		let valid = true;
		let navigateToTab: any = null;
		let tabs1 = Object.values(editedTabs);
		tabs1.forEach((tab: any, indexLayout: number) => {
			tab.formGroup.forEach((fields: any, formIndex: number) => {
				fields.fields.forEach((field: any, index: number) => {
					let optionsRequired = field.optionsRequired;
					// drawerRef.current[indexLayout + 1].formGroup[formIndex].fields[index].optionsRequired;
					if (required.includes(field.name) || optionsRequired) {
						if (field.type === InputTypes.PASSWORD && field.value.length > 0) {
							if (field.name === 'confirm password') {
								if (
									fields.fields[index - 1] &&
									fields.fields[index - 1].value &&
									fields.fields[index - 1].value === field.value
								) {
									tab.defaultOpen = false;
									if (!navigateToTab) navigateToTab = null;
									field.error = null;
									FieldsAreFullfilled++;
								} else {
									tab.defaultOpen = true;
									if (!navigateToTab) navigateToTab = tab.tabId;
									field.error = { label: formatMessage({ id: 'passwords not match' }) };
								}
							}
						} else if (optionsRequired) {
							optionsRequiredLength++;
							let value: any = field.selectValue;

							if (
								(field.value == 0 || !field.value) &&
								value &&
								(!value[0] || (value[0] && (!value[0].value || value[0].value < 1)))
							) {
								tab.defaultOpen = false;
								if (!navigateToTab) navigateToTab = null;
								field.error = null;
								FieldsAreFullfilled++;
							} else if (field.value && field.value > 0 && value && value[0] && value[0].value > 0) {
								tab.defaultOpen = false;
								if (!navigateToTab) navigateToTab = null;
								field.error = null;
								FieldsAreFullfilled++;
							} else {
								tab.defaultOpen = true;
								if (!navigateToTab) navigateToTab = tab.tabId;
								field.error = { label: formatMessage({ id: 'this input is required' }) };
							}
						} else if (field.value && (field.value.length > 0 || field.value.value)) {
							tab.defaultOpen = false;
							if (!navigateToTab) navigateToTab = null;
							field.error = null;
							FieldsAreFullfilled++;
						} else {
							tab.defaultOpen = true;
							if (!navigateToTab) navigateToTab = tab.tabId;
							field.error = { label: formatMessage({ id: 'this input is required' }) };
						}
					}
					if (field.validations) {
						let isValid = field.validations.map((v: any) => {
							if (v && v(field.value, 6)) return true;
							return false;
						});
						if (!isValid.includes(false)) {
							tab.defaultOpen = false;
							if (!navigateToTab) navigateToTab = null;
							field.error = null;
							valid = true;
						} else {
							tab.defaultOpen = true;
							if (!navigateToTab) navigateToTab = tab.tabId;
							valid = false;
							field.error = { label: formatMessage({ id: 'password not strong enough' }) };
						}
					}
				});
			});
		});
		let d: any = document.getElementById(navigateToTab);
		if (navigateToTab && d) d.click();
		//setActiveTab(navigateToTab)

		return FieldsAreFullfilled === required.length + optionsRequiredLength && valid ? true : editedTabs;
	};
	if (shown) {
		return (
			<Drawer
				dataChanges={dataChanges}
				modalBeforeClose={modalBeforeClose}
				shown={shown}
				toggleModal={toggleModal}
			>
				<SidePanel onClose={toggleModal}>
					{activeDrawer === dType.ACCORDION ? (
						<div className="accordion-drawer__body">
							<h3 className="accordion-drawer__title">{caption}</h3>
							<div className="accordion-drawer__accordion">
								{editedLayouts.map(({ layoutId, formGroup, title, description }) => (
									<Accordion
										openAcc={openAcc}
										key={layoutId}
										className="accordion-drawer__form-group"
										caption={title}
										endArrow={true}
									>
										{formGroup.map(({ title, formId, fields, additionalFields }) => (
											<FormGroup
												selectedSearch={selectedSearch}
												setSelectedSearch={setSelectedSearch}
												setSaveDisabled={setSaveDisabled}
												key={formId}
												{...{
													title,
													fields,
													additionalFields,
													description,
													id: layoutId,
													formId,
													onChangeData,
													tempData,
													setTempData
												}}
											/>
										))}
									</Accordion>
								))}
							</div>
							<div
								className={classnames('tab-drawer__footer', {
									'justify-content-end': alignButtonsLeft
								})}
							>
								{!hideZeroFilters ? (
									onCancelButton ? (
										<Button
											disabled={cancelDisabled}
											className="round mr-1"
											color="primary"
											outline
											onClick={() => {
												onCancelButton();
												setTimeout(() => {
													toggleModal();
												}, 2);
											}}
										>
											{formatMessage({ id: 'zeroFilters' })}
										</Button>
									) : (
										<Button
											disabled={cancelDisabled}
											className="round mr-1"
											color="primary"
											outline
											onClick={toggleModal}
										>
											{formatMessage({ id: 'zeroFilters' })}
										</Button>
									)
								) : null}
								<Button
									disabled={saveDisabled ? true : false}
									className="round"
									onClick={onSaveChanges}
									color="primary"
								>
									{saveButtonText
										? formatMessage({ id: saveButtonText })
										: formatMessage({ id: 'save' })}
								</Button>
								{loadingBySave ? (
									<div className="ml-1 mt-05">
										{' '}
										<Spinner />{' '}
									</div>
								) : null}
							</div>
						</div>
					) : (
						<div className="tab-drawer__body overflow-y-auto">
							<h3 className={classnames('tab-drawer__title', {})}>
								<div
									onClick={titleEditable ? onCanEdit : () => null}
									className={classnames('px-05 min-width-15-rem', {
										'border-warning': captionError,
										'border-radius-05rem': captionError
									})}
								>
									{isCaptionText ? (
										<input
											className="no-outline no-border"
											ref={inputRef}
											onChange={onChange}
											onBlur={offCanEdit}
											onKeyDown={onKeyDown}
											// placeholder={caption}
											value={currentValue}
											disabled={!isEdit}
											style={{ width: (currentValue.length + 1) * 11 + 'px', minWidth: '8rem' }}
										/>
									) : (
										<input
											className="no-outline no-border"
											ref={inputRef}
											onChange={onChange}
											onBlur={offCanEdit}
											onKeyDown={onKeyDown}
											placeholder={caption}
											disabled={!isEdit}
											style={{
												width: (caption ? caption.length + 1 : 5) * 11 + 'px',
												minWidth: '8rem'
											}}
										/>
									)}

									{titleEditable === false ? null : (
										<img
											className="cursor-pointer"
											src={config.iconsPath + 'general/edit-icon.svg'}
											alt=""
										/>
									)}
									{captionError ? (
										<div className="ml-05 text-bold-600 font-small-2 text-danger">
											{formatMessage({ id: validateCaption })}
										</div>
									) : null}
								</div>
							</h3>
							<div className="tab-drawer__tabs">
								{isTabsHidden ? null : (
									<Tabs value={activeTab} onChangTab={setActiveTab}>
										{editedTabs.map(({ title, tabId }) => (
											<Tab id={tabId} key={tabId} caption={title} name={tabId} />
										))}
									</Tabs>
								)}
							</div>
							<TabContent className="tab-drawer__tab-content" activeTab={activeTab}>
								{editedTabs.map(({ tabId, formGroup }) => (
									<TabPane key={tabId} tabId={tabId}>
										{formGroup.map(
											({ title, formId, fields, additionalFields, extraFields, description }) => (
												<FormGroup
													selectedSearch={selectedSearch}
													setSelectedSearch={setSelectedSearch}
													setSaveDisabled={setSaveDisabled}
													key={formId}
													{...{
														title,
														fields,
														additionalFields,
														description,
														extraFields,
														id: tabId,
														formId,
														onChangeData,
														tempData,
														setTempData
													}}
												/>
											)
										)}
									</TabPane>
								))}
							</TabContent>
							<div
								className={classnames('tab-drawer__footer', {
									'justify-content-end': alignButtonsLeft
								})}
							>
								<Button
									className="round mr-1"
									disabled={saveDisabled}
									onClick={onSaveChanges}
									color="primary"
								>
									{saveButtonText
										? formatMessage({ id: saveButtonText })
										: formatMessage({ id: 'save' })}
								</Button>
								{hideZeroFilters ? null : (
									<Button className="round " color="primary" outline onClick={toggleModal}>
										{formatMessage({ id: 'cancel' })}
									</Button>
								)}
							</div>
						</div>
					)}
				</SidePanel>
			</Drawer>
		);
	}

	return null;
};

export default GenericDrawer;
