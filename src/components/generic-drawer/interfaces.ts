import { FormGroupType } from '@src/components/form-group';

export interface DrawerLayout {
	title: string;
	layoutId: string;
	formGroup: FormGroupType[];
	defaultOpen?: boolean;
}
export interface drawerLayout {
	title: string;
	layoutId?: any
	tabId?: any
	formGroup: FormGroupType[];
	defaultOpen?: boolean;
}
export interface DrawerTab {
	title: string;
	tabId: string;
	formGroup: FormGroupType[];
}
export interface drawerData {
	layouts?: DrawerLayout[];
	itemName?:any
	tabs?: DrawerTab[];
}
