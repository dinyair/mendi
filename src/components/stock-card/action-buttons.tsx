import * as React from 'react';
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap'
import { Icon } from '@components';
import { config } from '@src/config';
import { TYPES } from '@src/containers/stock/orders/services/reducer';
import './stock-card.scss';
import { Spinner } from 'reactstrap'
import * as moment from 'moment';
import { sendOrderToDraft, addOrder, getAspkaDate } from '@src/containers/order/initial-state'
import { deleteOrderByOrderId, deleteOrderDateBranchSupply } from '@src/containers/stock/orders/initial-state'
import { RootStore } from '@src/store';
import { useDispatch, useSelector } from 'react-redux';
import { useIntl } from "react-intl";
import { getLogo } from '@src/containers/stock/orders/initial-state'
import { getSuggestedOrder } from '@src/containers/order/initial-state'

interface Props {
	isOpenModal: boolean;
	type: string;
	toggleModal?: () => void;
	sendToDone?: () => void;
	sendToDraft?: () => void;
	deleteOption?: () => void;
	onEdit?: () => void;
	chosenSupplierId?: any
	chosenBranch?: any
	setDisplayOrdersBySupplier?: (val: any) => void;
	searchOrders?: () => void;
	option?: any
	toggleModalDelete?: () => void,
	isDeleteDraftModelOpen: boolean
	setPrintData?: (data:any)=>void
}

export const ActionButtons: React.FC<Props> = props => {
	const { formatMessage } = useIntl();
	let userBranches: any = useSelector((state: RootStore) => state._userBranches);

	let { type, toggleModal, isOpenModal, onEdit, chosenSupplierId,
		chosenBranch, setDisplayOrdersBySupplier, searchOrders, option,
		isDeleteDraftModelOpen, toggleModalDelete,setPrintData }
		= props;
	const [isClicked, setIsClicked] = React.useState<any>(false)

	const sendOrderToDone = async (draft?:boolean) => {
		if (searchOrders && setDisplayOrdersBySupplier && chosenBranch && option) {
			setIsClicked(true)
			let rec = {
				isDraft: draft?true:false,
				branchId: chosenBranch,
				sapakId: option.SapakId,
				date1: moment(new Date()).format('YYYY-MM-DD')
			}
			const suggestedOrderJson = await getSuggestedOrder(rec)
			const aspkaDate = await getAspkaDate(rec)


			let itemsToOrder = suggestedOrderJson[0].map((i: any) => {
				let AmountMaraz = i.amountOrder1
				let BranchId = chosenBranch
				let AmountOrderOrg = i.AmountOrderOrg
				let AmountOrder = i.amountOrder
				let averageDaySum = i.sumofday
				let Ariza = i.ariza
				let BarCode = i.BarCode
				let lastyitra = i.lastBalance
				let madaf = i.shelfData
				let newyitra = i.lastBalance
				let sales = i.averageDay
				let SapakId = option.SapakId
				let GroupId = i.GroupId
				let OrderDate = moment(new Date()).format('YYYY-MM-DD')
				let AspakaDate = aspkaDate
				return {
					AmountMaraz, BranchId, AmountOrderOrg, AmountOrder, averageDaySum, Ariza, BarCode, lastyitra, madaf, newyitra, sales
					, SapakId, GroupId, OrderDate, AspakaDate
				}
			})
			await sendOrderToDraft(itemsToOrder)
			let rec1 = {
				SapakId: option.SapakId,
				OrderDate: moment(new Date()).format('YYYY-MM-DD'),
				BranchId: userBranches.length === 1 ? userBranches[0].BranchId : chosenBranch,
				AspakaDate: aspkaDate
			}
			await addOrder(rec1)
			setIsClicked(false)
			setDisplayOrdersBySupplier(false)
			searchOrders()
		}
	}

	const deleteOrder = async () => {
		if (searchOrders && option) {
			setIsClicked(true)
			await deleteOrderByOrderId(option.OrderNum)
			setIsClicked(false)
			searchOrders()
		}
	}
	const deleteDraftOrder = async () => {
		if (searchOrders && option) {
			setIsClicked(true)
			let rec = {
				SapakId: option.SapakId,
				OrderDate: moment(new Date()).format('YYYY-MM-DD'),
				BranchId: userBranches.length === 1 ? userBranches[0].BranchId : chosenBranch,
			}
			await deleteOrderDateBranchSupply(rec)
			setIsClicked(false)
			searchOrders()
		}
	}


	// const printDoneOrder = async () => {
	// 	let logo = await getLogo()
	// 	let marazSum: number = 0
	// 	let singleSum: number = 0
	// 	option.subOrder.forEach((order: any) => {
	// 		marazSum += order.AmountMaraz
	// 		singleSum += order.Ariza ? (order.Ariza*order.AmountMaraz) : order.AmountOrder
	// 	})
	// 	option['marazSum'] = marazSum.toFixed(0)
	// 	option['singleSum'] = singleSum.toFixed(0)
	// 	option['logo'] = `https://grid.algoretail.co.il/assets/images/${logo}`

	// 	let finalData = option
	// 	finalData.subOrder = finalData.subOrder ? finalData.subOrder.filter((g: any) => g.AmountOrder && g.AmountOrder > 0) : []

	// 	if(setPrintData) setPrintData(finalData)  
	// }

	const printDoneOrder = async () => {
		let logo = await getLogo();
		let marazSum: number = 0;
		let singleSum: number = 0;
		let finalData = option;

		finalData['logo'] = `https://grid.algoretail.co.il/assets/images/${logo}`;
		finalData.sapakName = finalData.SapakName;

		console.log({finalData})
		
		finalData.subOrder = finalData.subOrder?finalData.subOrder.map((row: any) => {
			let AmountMaraz = row.AmountMaraz;
			let AmountOrder = row.AmountOrder;
			let AmountOrderSub = row.AmountMaraz;

			if (row.userId == null && row.AmountOrder != (row.AmountMaraz * row.Ariza)) {
				AmountMaraz = row.AmountOrder;
				AmountOrderSub = row.AmountOrder;
				AmountOrder = row.AmountOrder * row.Ariza;
			}
			if(AmountOrder<1) return null
			let singleAmount = AmountOrder;
			return { ...row, singleAmount, AmountOrderSub, AmountOrder, AmountMaraz };
		}).filter((g:any)=>g):[];

		finalData.subOrder.forEach((order: any) => {
				marazSum += order.AmountMaraz > 0 ? order.AmountMaraz : 0;
				singleSum += order.AmountOrder
			
		});
		
		finalData['marazSum'] = marazSum.toFixed(0);
		finalData['singleSum'] = singleSum.toFixed(0);

		if(setPrintData) setPrintData(finalData)  

	};


	switch (type) {
		case TYPES.NEW: {
			return (
				<>
					<button onClick={onEdit} className='stock-card__edit'>
						<Icon src={config.iconsPath + "general/edit-icon.svg"} />
					</button>
					<button onClick={toggleModal} className='stock-card__send'>
						<Icon src={config.iconsPath + "general/send-icon.svg"} />
					</button>
					<Modal isOpen={isOpenModal} toggle={toggleModal} className='modal-dialog-centered'>
						<ModalHeader isClicked={isClicked}>{formatMessage({ id: 'warning' })}</ModalHeader>
						<ModalBody>
							{formatMessage({ id: 'noValuePromoMessage' })}
						</ModalBody>
						<ModalFooter>
							<Button className='round' color='primary' disabled={isClicked} onClick={()=>{sendOrderToDone(false)}}>{formatMessage({ id: 'sendAnyWay' })}</Button>
							<Button className='round' color='primary' disabled={isClicked} outline onClick={onEdit}>{formatMessage({ id: 'goBackToFix' })}</Button>
							{isClicked ? <Spinner /> : null}
						</ModalFooter>
					</Modal>
				</>
			)
		}
		case TYPES.PENDING: {
			return (
				<>
					<button onClick={onEdit} className='stock-card__edit'>
						<Icon src={config.iconsPath + "general/edit-icon.svg"} />
					</button>
					<button onClick={toggleModal} className='stock-card__send'>
						<Icon src={config.iconsPath + "general/send-icon.svg"} />
					</button>
					<button onClick={toggleModalDelete} className='stock-card__trash'>
						<Icon src={config.iconsPath + "general/delete-icon.svg"} />
					</button>
					<Modal isOpen={isOpenModal} toggle={toggleModal} className='modal-dialog-centered'>
						<ModalHeader isClicked={isClicked}>{formatMessage({ id: 'warning' })}</ModalHeader>
						<ModalBody>
							{formatMessage({ id: 'noValuePromoMessage' })}
						</ModalBody>
						<ModalFooter>
							<Button className='round' color='primary' disabled={isClicked} onClick={()=>{sendOrderToDone(true)}}>{formatMessage({ id: 'sendAnyWay' })}</Button>
							<Button className='round' color='primary' disabled={isClicked} outline onClick={onEdit}>{formatMessage({ id: 'goBackToFix' })}</Button>
							{isClicked ? <Spinner /> : null}
						</ModalFooter>
					</Modal>
					<Modal isOpen={isDeleteDraftModelOpen} toggle={toggleModalDelete} className='modal-dialog-centered'>
						<ModalHeader isClicked={isClicked}>{formatMessage({ id: 'warning' })}</ModalHeader>
						<ModalBody>
							{formatMessage({ id: 'deleteDraftMsg' })}
						</ModalBody>
						<ModalFooter>
							<Button className='round' color='primary' disabled={isClicked} onClick={deleteDraftOrder}>{formatMessage({ id: 'delete' })}</Button>
							<Button className='round' color='primary' disabled={isClicked} outline onClick={toggleModalDelete}>{formatMessage({ id: 'cancel' })}</Button>
							{isClicked ? <Spinner /> : null}
						</ModalFooter>
					</Modal>
				</>
			)
		}
		case TYPES.DELETE: {
			return (
				<>
					<button className='stock-card__print' onClick={printDoneOrder}>
					{/* <div className='print__card__container'>
						<PrintOrder ref={componentRef} data={printData} />
					</div> */}
					{/* {printDiv ? <PrintOrderW data={printData}/> : null} */}
						<Icon src={config.iconsPath + "general/print-icon.svg"} />
					</button>
					<button onClick={toggleModal} className='stock-card__trash'>
						<Icon src={config.iconsPath + "general/delete-icon.svg"} />
					</button>
					<Modal isOpen={isOpenModal} toggle={toggleModal} className='modal-dialog-centered'>
						<ModalHeader isClicked={isClicked} >{formatMessage({ id: 'warning' })}</ModalHeader>
						<ModalBody>
							{formatMessage({ id: 'deleteOrderMsg' })}
						</ModalBody>
						<ModalFooter>
							<Button className='round' color='primary' disabled={isClicked} onClick={deleteOrder}>{formatMessage({ id: 'delete' })}</Button>
							<Button className='round' color='primary' disabled={isClicked} outline onClick={toggleModal}>{formatMessage({ id: 'cancel' })}</Button>
							{isClicked ? <Spinner /> : null}
						</ModalFooter>
					</Modal>
				</>
			)
		}
		default: return null;
	}
}


