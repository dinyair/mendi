import * as React from 'react';
import { Option } from '@src/containers/stock/orders/services/interfaces';
import { ActionButtons } from './action-buttons';
import './stock-card.scss';
import { useIntl } from "react-intl";
import classnames from 'classnames';

interface Props {
	option: any;
	type: string;
	onSendToDone?: (optionId: string, type: string) => void;
	onSendDraft?: (optionId: string, type: string) => void;
	backToStart?: (optionId: string, type: string) => void;
	editOrder?: (option: any) => void;
	chosenSupplierId?: any
	chosenBranch?: any
	setDisplayOrdersBySupplier?: (val: any) => void;
	searchOrders?: () => void;
	handlePrintDoneOrder?: (option: any) => void;
	setPrintData?:(data:any)=>void;

}

export const StockCardItem: React.FC<Props> = props => {
	const { option, type, onSendToDone, onSendDraft, backToStart, editOrder, chosenSupplierId, chosenBranch, setDisplayOrdersBySupplier,setPrintData, searchOrders, handlePrintDoneOrder } = props;
	const { formatMessage } = useIntl();
	const [isOpenModal, setStateModal] = React.useState(false)
	const [isDeleteDraftModelOpen, setDeleteDraftModal] = React.useState(false)
	const toggleModal = () => {
		setStateModal(prevValue => !prevValue);
	}
	// console.log('option', option)
	const toggleModalDelete = () => {
		setDeleteDraftModal(prevValue => !prevValue);
	}

	const sendToDone = () => {
		toggleModal();
	}

	const sendToDraft = () => {
		toggleModal();
		if (onSendDraft) {
			onSendDraft(option.id, type);
		}
	}

	const deleteOption = () => {
		toggleModal();
		if (backToStart) {
			backToStart(option.id, type);
		}
	}

	const onEdit = () => {
		if (editOrder) {
			editOrder(option);
		}
	}
	return (
		<li	className={classnames(
			'stock-card__item',
			{}
		)}>
			<div 
			className={classnames(
				'stock-card__text',
				{
					'flex-direction-column': window.innerWidth<1601
				}
			)}
			>
				<span className='stock-card__value elipsis'>{option.value} {option.ordersAmount ? <span className='stock-card__price'>({option.ordersAmount})</span>: null}</span>
				{/* {option.ordersAmount && <span className='stock-card__price '>({option.ordersAmount})</span>} */}
				{option.OrderNum && <span 
				className={classnames(
					'stock-card__price width-100-per ',
					{
						'ml-05': window.innerWidth>1600
					}
				)}>{formatMessage({ id: 'orderNum.' })} {option.OrderNum}</span>}
			</div>
			<div className='stock-card__action-buttons'>
				<ActionButtons {...{
					option,
					chosenSupplierId,
					chosenBranch,
					setDisplayOrdersBySupplier,
					searchOrders,
					type,
					isOpenModal,
					toggleModal,
					sendToDone,
					sendToDraft,
					deleteOption,
					onEdit,
					toggleModalDelete,
					isDeleteDraftModelOpen,
					handlePrintDoneOrder,
					setPrintData
				}} />
			</div>
		</li>
	)
}
