import * as React from 'react';
import { Option } from '@src/containers/stock/orders/services/interfaces';
import { StockCardItem } from "./stock-card-item";
import './stock-card.scss';

interface Props {
	title: string;
	options: Option[];
	type: string;
	onChangeValue?: (value: string) => void;
	handler?: () => void;
	sendToDraft?: (optionId: string, type: string) => void;
	sendToDone?: (optionId: string, type: string) => void;
	backToStart?: (optionId: string, type: string) => void;
	editOrder?: (options: any) => void;
	chosenSupplierId?: any
	chosenBranch?: any
	setDisplayOrdersBySupplier?: (val: any) => void;
	searchOrders?: () => void;
	setPrintData?:(data:any)=>void
}

export const StockCard: React.FC<Props> = props => {
	const { title, options, type, sendToDone, sendToDraft, backToStart, editOrder, chosenSupplierId, chosenBranch, setDisplayOrdersBySupplier, setPrintData,searchOrders } = props;

	return (
		<div className={`mb-2 w-100 p-2 stock-card stock-card_${type}  `}>
			<h4 className='mb-1 stock-card__title'>{`${title} (${options.length})`}</h4>
			<div className='max-height-45vh overflow-y-scroll overflow-x-hidden'>
				<ul className='stock-card__list'>
					{options.map(option => (
						<StockCardItem key={option.id} {...{
							chosenSupplierId,
							chosenBranch,
							setDisplayOrdersBySupplier,
							searchOrders,
							option,
							type,
							onSendDraft: sendToDraft,
							onSendToDone: sendToDone,
							backToStart,
							editOrder,
							setPrintData
						}} />
					))}
				</ul>
			</div>
		</div>
	)
}
