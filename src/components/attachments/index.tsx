import * as React from 'react';
import { SwipeableDrawer } from '@material-ui/core';
import './index.scss';
import { useOutsideClick } from '@src/utilities';
import { Attachment } from './interfaces'



interface Option {
	readonly text: string;
	readonly disabled: boolean;
	readonly action: () => void;
}

interface Props {
	readonly isOpen: boolean;
	readonly attachments: Attachment[];
	readonly onChange: (f: File) => void;
	readonly setIsAttachmentsOpen: (bool: boolean) => void;
	readonly onOutsideClick: () => void;
}

export const Attachments: React.FC<Props> = (props: Props) => {
	const { attachments, setIsAttachmentsOpen } = props
	const ref: React.MutableRefObject<any> = React.useRef(null);
	const { isOpen, onOutsideClick, onChange } = props;
	const [newFieldValue, setNewFieldValue] = React.useState<string>('');
	const [file, setFile] =  React.useState<any>(undefined);

	/* eslint-disable @typescript-eslint/no-non-null-assertion */
	useOutsideClick(ref, onOutsideClick!);

	return isOpen ? (
		<div>
		<SwipeableDrawer
			anchor={'right'}
			open={isOpen}
			onOpen={()=>setIsAttachmentsOpen(true)}
			onClose={onOutsideClick}
		>
										<>
										{attachments.map((a: any, i: number)=>(
											<h2 key={i}>{a.file_name}</h2>
										))}
											<input
												onChange={(event: React.ChangeEvent<HTMLInputElement>): void => {
													setNewFieldValue(event.target.value);
													if(event.target.files){setFile(event.target.files[0])}													
												}}
												type="file"
												value={newFieldValue}
												placeholder="Add new..."
											/>
											<button onClick={()=>{onChange(file); setNewFieldValue('')}}>upload</button>
										</>
			</SwipeableDrawer>
			</div>	
	) : null
};

export default Attachments;
