import * as React from "react";
import classnames from "classnames"
import * as FeatherIcons from "react-feather";
import { useIntl } from "react-intl";

import './input-text.scss';

interface Props {
	onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
	placeholder?: any;
	inputRef?: any;
	inputClassNames?: string;
	id?: any;
	value?: string | number | null;
	onKeyDown?: (e: React.ChangeEvent<HTMLInputElement>) => void
	noMarginBottom?: boolean;
	noMarginRight?: boolean
	textarea?: boolean;
	classNames?: string;
	searchIcon?: boolean;
	customIcon?: JSX.Element;
	inputType?: string;
	isFocused?: (isFocused: boolean) => void;
	error?: boolean;
	errorLabel?: string;
	disabled?:boolean
};



export const InputText: React.FC<Props> = ({ onChange, onKeyDown, isFocused, placeholder, inputRef, id, value, disabled,inputClassNames, noMarginBottom, noMarginRight, textarea, classNames, searchIcon, customIcon, inputType, error, errorLabel }) => {
	const { formatMessage } = useIntl();
	const [focus, setFocus] = React.useState(false);

	const onFocus = () => {
		setFocus(true);
		isFocused && isFocused(true)
	}

	const onBlur = () => {
		setFocus(false);
		isFocused && isFocused(false)
	}

	const focusIcon = () => {
		document.getElementById(id)?.focus()
		setFocus(true)
	}

	const classes = classnames('input-text', classNames ? classNames : '', error ? 'red-border ' : '', {
		focus,
		'mb-2': !noMarginBottom,
		'mr-1': !noMarginRight,
		'width-100-per': noMarginRight
	});

	return (
		<div className="d-flex flex-column justify-content-center">
			<div className={classes}>
				{customIcon || searchIcon ? <span className='input-text__icon' onClick={() => focusIcon()}>
					{searchIcon && <FeatherIcons.Search size={15} />}
					{customIcon ? customIcon : null}
				</span> : null}


				{!textarea ?
					<input disabled={disabled?true:false} type={inputType || 'text'} onWheel={(e) => e.currentTarget.blur()} className={inputClassNames || ''} id={id} value={value || undefined} onChange={(e: any) => { onChange(e); if (onKeyDown) onKeyDown(e.currentTarget.value) }} onFocus={onFocus} onBlur={onBlur} ref={inputRef || null} placeholder={placeholder || formatMessage({ id: 'searchPlaceHolder' })} /> :
					<textarea className={inputClassNames || ''} id={id} value={value || undefined} onChange={(e: any) => { onChange(e); if (onKeyDown) onKeyDown(e.currentTarget.value) }} onFocus={onFocus} onBlur={onBlur} ref={inputRef || null} placeholder={placeholder || formatMessage({ id: 'searchPlaceHolder' })} />
				}
			</div>
			{error ? <div className="ml-05 text-bold-600 font-small-2 text-danger min-height-1-rem max-height-1-rem">{errorLabel}</div> : null}
		</div>
	)
}
