import * as React from 'react';
import ReactSignatureCanvas from 'react-signature-canvas';
import { useIntl } from "react-intl"

interface Props {
	onChange: (image: string | null) => void;
	height?: string;
	width?: string;
	label: string;
};

export const SignatureBox = (props: Props) => {
	const [imageURL, setImageUrl] = React.useState(null);
	let signatureRef: any = React.useRef<any>();
	const { onChange, height, width, label } = props
	const { formatMessage } = useIntl();

	const clear = () => {
		signatureRef.current.clear();
		onChange && onChange(null);
		setImageUrl(null);
	}
	const trim = () => {
		setImageUrl(signatureRef.current.getCanvas().toDataURL('image/png'))
		onChange && onChange(signatureRef.current.getCanvas().toDataURL('image/png'))
	};

	return (
		<>
			<div className="d-flex flex-column width-100-per ">
				<div className="d-flex justify-content-between">
					<label className="font-medium-1">{label}</label>
					{imageURL ? <label className={`font-medium-1 text-bold-500`} onClick={() => clear()}>
						{formatMessage({ id: 'clear' })}
					</label> : null} 
				</div>

				<ReactSignatureCanvas
					canvasProps={{
						width: width ? width : '250px',
						height: height ? height : '250px',
						style: { border: imageURL ? '1px solid black' : '1px solid #269286' }
					}}
					backgroundColor={'white'}
					onEnd={() => trim()}
					ref={signatureRef}
				/>
			</div>
		</>
	);
};
