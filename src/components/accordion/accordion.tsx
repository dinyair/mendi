import * as React from 'react';
import classnames from "classnames";
import { ChevronLeft } from 'react-feather';

import './accordion.scss';

interface Props {
	caption: any;
	children: React.ReactNode;
	style?: object;
	className?: string;
	childClassName?: string;
	captionClassName?: string;
	endArrow?: boolean;
	hideArrow?: boolean;
	dontAllowClick?: boolean;
	data?: any
	buttonClassName?: any
	noMargin?: boolean
	defaultOpen?: boolean
	openAcc?: number
	checkBox?: any;
	dataClass?:string
	keepHeaderOnScroll?:boolean
	borderBottomOnOpen?:boolean
	childNum?:any
}

export const Accordion: React.FC<Props> = props => {
	const customizer = { direction: document.getElementsByTagName("html")[0].dir }
	const observerRef = React.useRef<ResizeObserver>();
	const childRef = React.useRef<HTMLDivElement>(null);
	const parentRef = React.useRef<HTMLDivElement>(null);

	const { caption, children, style,dataClass,keepHeaderOnScroll,borderBottomOnOpen,childNum, className = '', childClassName = '', captionClassName = '', endArrow = false, data, buttonClassName, noMargin, defaultOpen, openAcc, checkBox } = props;
	const [isOpen, setStateContent] = React.useState(false);
	React.useEffect(() => setStateContent(defaultOpen ? true : false), [defaultOpen])

	React.useEffect(() => {
		if (openAcc) setStateContent(true)
	}, [openAcc])

	const toggleAccordion = () => {
		setStateContent(!isOpen);
	}

	const adaptResize = React.useCallback(entries => {
		if (parentRef.current) {
			for (const entry of entries) {
				parentRef.current.style.maxHeight = `${entry.contentRect.height}px`
			}
		}
	}, []);

	React.useEffect(() => {
		observerRef.current = new ResizeObserver(adaptResize);
		return () => {
			if (observerRef.current) {
				observerRef.current.disconnect();
			}
		}

	}, []);

	React.useEffect(() => {
		if (observerRef.current && childRef.current) {
			if (isOpen) {
				observerRef.current.observe(childRef.current)
			} else {
				observerRef.current.unobserve(childRef.current)
			}
		}
	}, [observerRef.current, childRef.current, isOpen, defaultOpen]);

	const classesButton = classnames('accordion__button', {
		'active-ltr': isOpen && customizer.direction == 'ltr',
		'active-rtl': isOpen && customizer.direction == 'rtl',
		"position-sticky position-top-0": isOpen && keepHeaderOnScroll,
		"border-bottom-lightgray" : isOpen && borderBottomOnOpen ,
		'accordion__button_end-arrow': endArrow,
		[buttonClassName]: buttonClassName
	});

	const classes = classnames('accordion__content', {
		'overflow-visible': isOpen,
		'mt-1': isOpen && !noMargin,
	});
	const { offsetHeight } = childRef.current || { offsetHeight: 0 };
	const rootClasses = classnames('accordion', {
		[className]: className
	});

	return (
		<div className={rootClasses} style={style}>
			<button className={classesButton} style={keepHeaderOnScroll ? {top:`${childNum>0?(childNum*2.5):'0'}rem`,zIndex:100000-childNum}: {}}>
				<div className={`d-flex justify-content-center align-items-center flex-direction-row ${dataClass}`}>
				{checkBox ? <div className='align-self-center'>{checkBox}</div> : null}
				<span className={`accordion__button_text ${captionClassName}`}  onClick={toggleAccordion}>{caption}</span>
				 </div>
				<div className='align-self-center'  onClick={toggleAccordion}><span className={classnames("accordion__button_icon", {
					"rotate-180": customizer.direction == 'ltr',
					"d-flex": checkBox === null,
				})}><ChevronLeft size={16} /></span>
				</div>
			</button>
			<div ref={parentRef} className={classes} style={{ height: isOpen ? 'auto' : 0 }}>
				<div className={childClassName} ref={childRef}>
					{children}
				</div>
			</div>
		</div>
	)
}
