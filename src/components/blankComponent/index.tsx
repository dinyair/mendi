import * as React from 'react';
import { useIntl } from "react-intl"

interface Props {
}

export const blankComponent: React.FunctionComponent = (props:Props) => {
	const { formatMessage } = useIntl();
	document.title = '';

	return (
		<>
		
		</>
	)
};

export default blankComponent;

