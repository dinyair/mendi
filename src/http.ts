import axios, {
    AxiosRequestConfig, AxiosInstance, AxiosResponse
} from "axios";
import { useHistory } from "react-router";
import {config} from "./config";
// import { logout } from "./auth/auth.service";
// import { setAppState } from "./store/system/system.actions";
import { getToken } from "./containers/login/initial-state";
import { logOut } from "./containers/user/initial-state";
import { Routes } from "./utilities/enums";

export const httpClient: AxiosInstance = axios.create({
    baseURL: config.ipServer,
    timeout: 300000,
    headers: {}
});
export const httpClientEmpty: AxiosInstance = axios.create({
    // timeout: 30000,  // 30 seconds 
    timeout: 10800000,  // 3 hours
    headers: {}
});
httpClient.defaults.withCredentials = true;
httpClient.interceptors.request.use((req: AxiosRequestConfig) => {
    const token = getToken()
    if (token != null)
        req.headers["Authorization"] = "Bearer " + token;
    return req;
});

httpClient.interceptors.response.use(resp => resp, error => {
    if(error && error.response && error.response.status && error.response.status === 401){
        logOut()
    }
    return Promise.reject(error);
});

export async function postRequest<T>(url: string, data: any, config?: AxiosRequestConfig): Promise<T> {
    const response = await httpClient.post<T, AxiosResponse<T>>(url, data, config);
    if (!response || response.status !== 200)
        throw new Error("HttpClient:ERROR - " + response.status + ":" + response.statusText);
    return response.data;
}
export async function putRequest<T>(url: string, data: any, config?: AxiosRequestConfig): Promise<T> {
    const response = await httpClient.put<T, AxiosResponse<T>>(url, data, config);
    if (!response || response.status !== 200)
        throw new Error("HttpClient:ERROR - " + response.status + ":" + response.statusText);
    return response.data;
}
export async function deleteRequest<T>(url: string, config?: AxiosRequestConfig): Promise<T> {
    const response = await httpClient.delete(url, config);
    if (!response || response.status !== 200)
        throw new Error("HttpClient:ERROR - " + response.status + ":" + response.statusText);
    return response.data;
}
export async function getRequest<T>(url: string, config?: AxiosRequestConfig): Promise<T> {
    let response = await httpClient.get(url, config);
    if (!response || response.status !== 200)
        throw new Error("HttpClient:ERROR - " + response.status + ":" + response.statusText);
    return response.data;
}

export class HttpClientError extends Error { }