import * as React from 'react';
import * as Loadable from 'react-loadable';

import Spinner from "@vuexy/spinner/Loading-spinner"


export const Login: any = Loadable({
	loader: () => import('@containers/login').then((comp: any) => comp),
	loading: () => <Spinner />
});

export const ItemTrends: any = Loadable({
	loader: () => import('@containers/reports/item-trends').then((comp: any) => comp),
	loading: () => <Spinner />
});
export const OrderAnalyze: any = Loadable({
	loader: () => import('@containers/reports/order-analyza').then((comp: any) => comp),
	loading: () => <Spinner />
});
export const SettingsSuppliers: any = Loadable({
	loader: () => import('@containers/settings/suppliers').then((comp: any) => comp),
	loading: () => <Spinner />
});


export const SupplyDays: any = Loadable({
	loader: () => import('@containers/branch_planning/supply_days').then((comp: any) => comp),
	loading: () => <Spinner />
});

export const Home: any = Loadable({
	loader: () => import('@containers/home').then((comp: any) => comp),
	loading: () => <Spinner />
});

export const StockOrders: any = Loadable({
	loader: () => import('@containers/stock/orders').then((comp: any) => comp),
	loading: () => <Spinner />
});
export const StockSimulator: any = Loadable({
	loader: () => import('@containers/stock/simulator').then((comp: any) => comp),
	loading: () => <Spinner />
});

export const EditOrder: any = Loadable({
	loader: () => import('@containers/order').then((comp: any) => comp),
	loading: () => <Spinner />
});


export const CatalogItems: any = Loadable({
	loader: () => import('@containers/catalog/items').then((comp: any) => comp),
	loading: () => <Spinner />
});
export const CatalogCategories: any = Loadable({
	loader: () => import('@containers/catalog/categories').then((comp: any) => comp),
	loading: () => <Spinner />
});


export const Planogram: any = Loadable({
	loader: () => import('./PlanoApp').then((comp: any) => comp),
	loading: () => <Spinner />
});


export const SettingsUsers: any = Loadable({
	loader: () => import('@containers/settings/users').then((comp: any) => comp),
	loading: () => <Spinner />
});

export const InvertoryBalance: any = Loadable({
	loader: () => import('@containers/invertory/balances').then((comp: any) => comp),
	loading: () => <Spinner />
});

export const InvertoryCount: any = Loadable({
	loader: () => import('@containers/invertory/count').then((comp: any) => comp),
	loading: () => <Spinner />
});

export const RecieveStock: any = Loadable({
	loader: () => import('@containers/stock/receive').then((comp: any) => comp),
	loading: () => <Spinner />
});

export const StockTransfers: any = Loadable({
	loader: () => import('@containers/stock/transfer').then((comp: any) => comp),
	loading: () => <Spinner />
});


export const StockReturns: any = Loadable({
	loader: () => import('@containers/stock/return').then((comp: any) => comp),
	loading: () => <Spinner />
});


export const StockDestruction: any = Loadable({
	loader: () => import('@containers/stock/destruction').then((comp: any) => comp),
	loading: () => <Spinner />
});

export const ClientSettings: any = Loadable({
	loader: () => import('@containers/settings/clients').then((comp: any) => comp),
	loading: () => <Spinner />
});


// export const ClientSettings: any = Loadable({
// 	loader: () => import('@containers/test').then((comp: any) => comp),
// 	loading: () => <Spinner />
// });
