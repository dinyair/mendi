import { routerMiddleware } from 'connected-react-router';
import { composeWithDevTools } from 'redux-devtools-extension';
import { History, createBrowserHistory } from 'history';
import createSagaMiddleware, { Saga, SagaMiddleware } from 'redux-saga';
import { Store, Middleware, createStore, applyMiddleware, compose } from 'redux';

import sagas from './sagas';
import rootReducer from './reducers';

// import { SupplierGroup } from '@containers/suppliers/interfaces';
// import { suppliersInitialState } from '@containers/suppliers/initial-state';
import { userInitialState } from '@containers/user/initial-state';
// import { IncomeGroup, ExpenseGroup } from '@containers/budget/interfaces';
// import { incomeInitialState, expensesInitialState } from '@containers/budget/initial-state';
import { UserInterface } from '@containers/user/interfaces';
// import { Script } from '@containers/scripts/interfaces';

// import { usersInitialState } from '@containers/tasks/initial-state';
// import { Event, PlanningState } from '@containers/planning/interfaces';
// import { planningInitialState, eventsInitialState } from '@containers/planning/initial-state';
// import { overviewInitialState } from '@containers/overview/initial-state';
// import { scriptsInitialState } from './containers/scripts/initial-state';
// import { ShootingDay } from './containers/shooting_days/interfaces';
// import { shootingDaysInitialState } from './containers/shooting_days/initial-state';

import { navbarInitialState } from "./redux/reducers/navbar/initial-state"

import thunk from "redux-thunk"
import createDebounce from "redux-debounced"
import {State as StockOrdersState} from '@src/containers/stock/orders/services/reducer';


export const history: History = createBrowserHistory();
export const sagaMiddleware: SagaMiddleware = createSagaMiddleware();

export interface RootStore {
	readonly user: UserInterface;
	readonly _branches: any[];
	readonly _userBranches:any[]
	readonly _catalog: any;
	readonly _catalogUpdated:any[]
	readonly _catalogFullData: any[]
	readonly _catalogFullDataNoArchive: any[]
	readonly _groups: any;
	readonly _subGroups: any[];
	readonly _suppliers: any[];
	readonly _subSuppliers: any[];
	readonly _series:any[]
	readonly _layouts:any[]
	readonly _models:any[]
	readonly _segments:any[]
	readonly _divisions:any[]
	readonly _orderToPlace:any[];
	readonly _logo:any;
	readonly calendar: any;
	readonly emailApp: any;
	readonly todoApp: any;
	readonly chatApp: any;
	// readonly planogram: any;
	readonly customizer: any;
	readonly navbar: any;
	readonly dataList: any;
	readonly system: any;
	readonly stockOrders: StockOrdersState;
	readonly stockOrdersOrder: any;
}

export function configureStore(): Store<RootStore> {
	const historyMiddleware: Middleware = routerMiddleware(history);
	const middleware = [
		// sagaMiddleware,
		historyMiddleware,
		createDebounce(),
		thunk,
	];

	const store: Store<RootStore> = createStore(
		rootReducer(history),
		{
			user: userInitialState,
			_branches: [],
			_catalog: {},
			_catalogUpdated: [],
			_catalogFullData:[],
			_catalogFullDataNoArchive:[],
			stockOrdersOrder:[],
			_divisions:[],
			_layouts:[],
			_models:[],
			_series:[],
			_segments:[],
			_userBranches:[],
			_groups: [],
			_subGroups: [],
			_suppliers: [],
			_subSuppliers: [],
			_orderToPlace: [],
			_logo: {},
			calendar: {},
			emailApp: {},
			todoApp: {},
			chatApp:{},
			// planogram:{},
			customizer:{},
			navbar: navbarInitialState,
			dataList: {},
		},
		// composeWithDevTools(applyMiddleware(...middlewares,sagaMiddleware, historyMiddleware))
		// composeWithDevTools(
		// 	applyMiddleware(...middleware),
		// 	// other store enhancers if any
		//   )
		composeWithDevTools(applyMiddleware(thunk,createDebounce(),historyMiddleware))
	);

	if (module.hot) {
		module.hot.accept();

		store.replaceReducer(require('./reducers').default(history));
	}

	sagas.forEach((saga: Saga) => {
		sagaMiddleware.run(saga);
	});

	return store;
}
