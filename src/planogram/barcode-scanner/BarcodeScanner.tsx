
import * as React from "react"
//@ts-ignore
import * as Quagga from "quagga";
import "./barcode-scanner.scss"

export function getUserMedia(constraints: any) {
    if (navigator.mediaDevices
        && typeof navigator.mediaDevices.getUserMedia === 'function') {
        return navigator.mediaDevices
            .getUserMedia(constraints);
    }
    return Promise.reject(new Error('getUserMedia is not defined'));
}

type BarcodeScannerProps = {
    onDetected: (result: string | number) => void
    onReady: (startScan: Function, stopScan?: Function) => void,
    onError: () => void
};

const VIDEO_CONSTRAINTS = {
    // width: 640,
    // height: 480,
    facingMode: "environment"
};



export default class BarcodeScanner extends React.Component<BarcodeScannerProps> {
    elementRef =  React.createRef<HTMLDivElement>();
    state: { barcode: number | null } = {
        barcode: null
    }
    componentDidMount() {
        getUserMedia(VIDEO_CONSTRAINTS).then(() => {
            Quagga.init({
                inputStream: {
                    type: "LiveStream",
                    constraints: VIDEO_CONSTRAINTS,
                    target: document.getElementById("BarcodeScanner") || "#BarcodeScanner"
                },
                locator: {
                    patchSize: "medium",
                    halfSample: true
                },
                numOfWorkers: 4,
                decoder: {
                    readers: ["code_128_reader", "ean_reader"]
                },
                locate: true
            }, (err: any) => {
                if (err)
                    throw err;
                if (this.props.onReady)
                    this.props.onReady(Quagga.start, Quagga.stop);
            });
            Quagga.onDetected(this._onDetected);

        }).catch(err => {
            this.props.onError();
        });
    }

    componentWillUnmount() {
        if (Quagga.offDetected)
            Quagga.offDetected(this._onDetected);
    }

    _onDetected = (result: any) => {
        if (result && result.codeResult && result.codeResult.code) {
            const barcode = result.codeResult.code;
            if (this.state.barcode == barcode)
                return setTimeout(() => {
                    if (this.state.barcode === barcode)
                        this.setState({
                            barcode: null
                        });
                }, 1000);
            this.setState({
                barcode
            })
            this.props.onDetected(barcode);
        }
    }
    render() {
        return (
            <div ref={this.elementRef} id="BarcodeScanner" className="scanner-viewport" />
        );
    };
}