
import * as React from 'react';
import { useIntl } from "react-intl"
import { Accordion } from "@src/components/accordion";
import { postRequest } from "@src/http";
import { useSelector } from 'react-redux';
import { AppState } from '@src/planogram/shared/store/app.reducer';
import CustomCheckBox from './generic/customCheckBox';
import { ModalInterface, ModalTypes } from '@src/components/GenericModals/interfaces';
import { GenericModals } from '@src/components/GenericModals';
import DeleteModalData from './modals/deleteModalData';
import BranchAssigmentModalsData from './modals/branchAssigmentModalsData';

import '@components/combo-box/combo-box.scss';
import { CheckBoxesGroupsAccordion } from '@src/components/CheckBoxGroupsAccordion';
import { CheckBoxGroupsOptions, CheckBoxStatus, CheckBoxSubOption } from '@src/components/CheckBoxGroupsAccordion/interfaces';

interface Props {
  modalType: ModalTypes;
  toggleModal: (type: ModalTypes) => void;
  readonly oldStores: any[];
  readonly setBranches: (branches: any) => void;
  readonly searchContainer: (value: any, onChange: any, placeholder: string) => any;
  readonly options: any[];
}


export const BranchAssignment: React.FunctionComponent<Readonly<Props>> = (props: Readonly<Props>) => {
  const { formatMessage } = useIntl();
  const url = window.location.href;
  const urlElements = url.split('/');
  let currentAisleIndex = parseInt(urlElements[7]);

  const { modalType, oldStores, toggleModal, searchContainer, setBranches, options } = props

  const branches = useSelector((state: AppState) => state.system.data.branches)
  const stores = useSelector((state: AppState) => state.system.data.stores)

  const [branchesWithStores, setBranchesWithStore] = React.useState<CheckBoxGroupsOptions[]>([])
  const [newStores, setNewStores] = React.useState<any[]>(oldStores);
  const [aisles, setAisles] = React.useState<any[]>(options);
  const [optionIndex, setOptionIndex] = React.useState<any>(null)
  const [subOptionIndex, setsubOptionIndex] = React.useState<any>(null)
  const [selectedStore, setSelectedStore] = React.useState<any>(null)

  const layout2branch = async (send_email: boolean, new_stores: number[], del_stores: number[]) => {
    let branches = await postRequest<any>('layout2branch', {
      send_email,
      aisle_id: currentAisleIndex,
      new_stores,
      del_stores
    });

    setBranches(branches)

    return branches
  }


  React.useEffect(() => {
    if (stores && branches) {
      let branchesWstores: CheckBoxGroupsOptions[] = []
      branches.forEach((b: any) => {
        let branchStores: CheckBoxSubOption[] = []
        stores.forEach((s: any) => {
          if (s.branch_id === b.BranchId) {
            const option = aisles.find((ele) => ele.id === s.id)
            const Status = !newStores.includes(s.id) ? CheckBoxStatus.unchecked : option && option.aisle_version > 0 ? CheckBoxStatus.semi_checked : CheckBoxStatus.checked
            branchStores.push({ Id: s.id, Name: s.name, Status })
          }
        })
        branchesWstores.push({ ...b, SubOptions: branchStores })
      })
      setBranchesWithStore(branchesWstores)
    }
  }, [stores])

  const [modalsType, setModalsType] = React.useState<ModalTypes>(ModalTypes.none)

  const toggleModals = (newType: ModalTypes) => {
    setModalsType(newType)
  }



  const handleClicks = (option: CheckBoxSubOption, optionIndex: number, subOptionIndex: number) => {
    const isCheckedOnServer = oldStores.includes(option.Id)
    setSelectedStore(option)
    setOptionIndex(optionIndex)
    setsubOptionIndex(subOptionIndex)
    let update = [...branchesWithStores]

    if (modalsType == ModalTypes.none && option.Status === CheckBoxStatus.checked && isCheckedOnServer) {
      toggleModals(ModalTypes.delete)
    }
    else if (modalsType == ModalTypes.none && option.Status === CheckBoxStatus.semi_checked && isCheckedOnServer) {
      toggleModals(ModalTypes.add)
    }
    else if (modalsType == ModalTypes.none && option.Status === CheckBoxStatus.unchecked) {
      update[optionIndex].SubOptions[subOptionIndex].Status = CheckBoxStatus.checked
      setBranchesWithStore(update)
      setNewStores([...newStores, option.Id])
    }
    else if (modalsType == ModalTypes.none && option.Status === CheckBoxStatus.checked && !isCheckedOnServer) {
      update[optionIndex].SubOptions[subOptionIndex].Status = CheckBoxStatus.unchecked
      setBranchesWithStore(update)
      setNewStores([...newStores.filter((s: any) => s !== option.Id)])
    }
  }

  const modalHandlers = {
    delete: () => {
      let update = [...branchesWithStores]
      update[optionIndex].SubOptions[subOptionIndex].Status = CheckBoxStatus.unchecked
      setNewStores([...newStores.filter((s: any) => s !== selectedStore.Id)])
      layout2branch(false, [], [selectedStore.Id]);
      setBranchesWithStore(update)
      toggleModals(ModalTypes.none)
      setSelectedStore(null)
    },
    add: () => {
      let update = [...branchesWithStores]
      update[optionIndex].SubOptions[subOptionIndex].Status = CheckBoxStatus.checked
      const aisleIndex = aisles.findIndex((ele) => ele.id === selectedStore.Id)
      const aislesCopy = [...aisles]
      aislesCopy[aisleIndex].original_version = aislesCopy[aisleIndex].aisle_version
      aislesCopy[aisleIndex].aisle_version = 0;
      setAisles(aislesCopy)
      layout2branch(false, [selectedStore.Id], [selectedStore.Id]);
      setBranchesWithStore(update)
      setSelectedStore(null)
      toggleModals(ModalTypes.none)
    }
  }

  const branchAssignmentModal: ModalInterface = {
    classNames: 'modal-dialog-centered modal-sm',
    isOpen: modalType === ModalTypes.add,
    toggle: () => toggleModal(ModalTypes.none),
    header: "",
    headerClassNames: 'pb-2',
    body: (
      <>
        <CheckBoxesGroupsAccordion
          options={branchesWithStores}
          returnSelectedData={(data: CheckBoxGroupsOptions[]) => { }}
          searchBarPlaceHolder={`${formatMessage({ id: 'search' })} ${formatMessage({ id: 'branch' })}`}
          titleLabel={formatMessage({ id: 'toWhomToAssociateTheLayout' })}
          allowSemiCheck
          handleStateChangesFromFather
          handleCheckBoxEvent={handleClicks}
        />
      </>
    ),
    buttons: [{
      color: 'primary', onClick: async () => {
        const new_stores = [...newStores.filter((os: any) => !oldStores.includes(os))]
        layout2branch(false, new_stores, []);
        toggleModal(ModalTypes.none)
      },
      className: "mr-2 round height-3-rem text-bold-600 d-flex justify-content-center align-items-center font-small-4 cursor-pointer btn-primary",
      label: formatMessage({ id: 'associateToBranch' }),
      disabled: false
    }],
  }


  return (
    <>
      {modalsType != ModalTypes.none ? <BranchAssigmentModalsData
        modalHandlers={modalHandlers}
        modalType={modalsType}
        selectedStore={selectedStore}
        toggleModal={(type: ModalTypes) => toggleModals(type)} /> : null}
      {modalType != ModalTypes.none ? <GenericModals key={modalType} modal={branchAssignmentModal} /> : null}

    </>
  );
}

