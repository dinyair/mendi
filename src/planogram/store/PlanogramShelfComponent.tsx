import * as React from "react"
import { PlanogramDragDropTypes } from "../generic/DragAndDropType";
import PlanogramShelfItemComponent from "./PlanogramShelfItemComponent";
import { DropTarget, DragElementWrapper, DragSource } from 'react-dnd';
import { DragDropResultBase } from "../generic/DragDropWrapper";
import { PlanogramShelf, DimensionObject, PlacementObject, PlanogramElementId, PlanogramItem, PlanogramSection, PlanogramAisle } from "@src/planogram/shared/store/planogram/planogram.types";
import { Action, AnyAction } from "redux";
import { connect } from "react-redux";
import { switchItemsAction, deleteItemAction, editShelfItemAction, addProductAction } from "@src/planogram/shared/store/planogram/store/item/item.actions";
import { heightDensity, widthDensity, shelfItemDimensions, shelfAvailableSpace } from "../provider/calculation.service";
import { ItemPlacementModal } from "../modals/ItemPlacementModal";
import { editShelfDimensionsAction } from "@src/planogram/shared/store/planogram/store/shelf/shelf.actions";
import { dimensionText, catalogProductDimensionObject, validateDimensions } from "@src/planogram/provider/planogram.service";
import { AppState } from "@src/planogram/shared/store/app.reducer";
import { CatalogBarcode } from "@src/planogram/shared/interfaces/models/CatalogProduct";
import { setMultiSelectId, setCurrentSelectedId, hideProductDetailer, setAisleSaveArray, setAisleSaveIndex } from "@src/planogram/shared/store/planogram/planogram.actions";
import { Menu, contextMenu, MenuProvider } from "@src/planogram/generic/ContextMenu";
import { ProductDefaultDimensions,  blankPlanogramAisle, blankItem } from "@src/planogram/shared/store/planogram/planogram.defaults";
import { setModal, toggleModal } from "@src/planogram/shared/components/Modal";
import { DimensionModal } from "../modals/DimensionModal";
import * as planogramApi from '@src/planogram/shared/api/planogram.provider';
import { ThunkDispatch } from "redux-thunk";
import { editProductDimensions } from "@src/planogram/shared/store/catalog/catalog.action";
import { setAisleAction } from "@src/planogram/shared/store/planogram/store/aisle/aisle.actions";
import * as planogramProvider from "@src/planogram/shared/api/planogram.provider";

interface DropProps {
    connectDragSource?: DragElementWrapper<any>,
    isDragging?: boolean,
}
interface TargetProps {
    connectDropTarget?: DragElementWrapper<any>,
    canDrop?: boolean,
}
interface ShelfComponentProps {
    aisleId?: number,
    printArray?:any
    shelf: PlanogramShelf,
    shelfIndex: number;
    realDropTarget?: string,
    onDrop?: (item: DragDropResultBase) => any,
    onDragEnd?: (item: DragDropResultBase, targetItem: DragDropResultBase) => void,
    move?: (item: DragDropResultBase, targetItem: DragDropResultBase) => void,
    // move?: (currentIndex: number, targetIndex: number) => void,
    verifyDrop?: (droppedItem: DragDropResultBase) => boolean,
}
const DraggableSource = DragSource<ShelfComponentProps, DropProps>(
    PlanogramDragDropTypes.SHELF, {
    beginDrag(props, monitor) {
        const { shelf } = props;
        return ({
            type: PlanogramDragDropTypes.SHELF,
            payload: shelf,
        });
    },
    endDrag(props, monitor) {
        if (!monitor.didDrop()) {
            return;
        }
        if (props.onDragEnd)
            props.onDragEnd(monitor.getItem(), monitor.getDropResult());
    },
}, (connect, monitor, props) => ({
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging(),
})
);
const dropTypes = [
    PlanogramDragDropTypes.PRODUCT_SIDEBAR,
    PlanogramDragDropTypes.SERIES_SIDEBAR,
    PlanogramDragDropTypes.SHELF_ITEM,
    PlanogramDragDropTypes.SHELF
];
const DroppableTarget = DropTarget<ShelfComponentProps, TargetProps>(dropTypes, {
    drop(props, monitor) {
        console.log('DroppableTarget drop shelf', props, monitor);
        const { shelf } = props;
        let distFromStart = 0;
        let distFromTop = 0;
        if (props.onDrop) {
            /* Barak 29.9.20 - Add code to figure out the distance from the start of the shelf of dropped item */
            // based on 'How do I get the coordinates of my dropped item' - https://github.com/react-dnd/react-dnd/issues/151
            let item = monitor.getItem();
            let my_div = document.getElementById("sh_id_" + shelf.id);
            let dropTargetPosition = { left: 0, top: 0 };
            let box: any;
            const initialPosition: any = monitor.getInitialSourceClientOffset();
            const finalPosition: any = monitor.getSourceClientOffset();
            const { y: finalY, x: finalX } = finalPosition;
            const { y: initialY, x: initialX } = initialPosition;
            console.log('initialPosition', initialPosition)

            try {
                if (my_div) {
                    dropTargetPosition = my_div.getBoundingClientRect();
                    box = my_div.getBoundingClientRect();
                }
            } catch (e) { }
            let newXposition =
                finalX > initialX
                    ? initialX + (finalX - initialX) - dropTargetPosition.left
                    : initialX - (initialX - finalX) - dropTargetPosition.left;
            let newYposition =
                finalY > initialY
                    ? initialY + (finalY - initialY) - dropTargetPosition.top
                    : initialY - (initialY - finalY) - dropTargetPosition.top;

            console.log('dropTargetPosition', dropTargetPosition, 'initialPosition', initialPosition,
                'finalPosition', finalPosition, 'newYposition', newYposition, 'newXposition', newXposition);

             
            // let newXposition = finalX > dropTargetPosition.left ? finalX - dropTargetPosition.left : 0;
           
            // X-axis moves from left to right ->
            if (newXposition < 0) newXposition = 0;
            // if (newXposition > box.width) newXposition = box.width;
            // Y-axis moves from top to bottom 
            if (newYposition < 0) newYposition = 0;
            // if (newYposition > box.height) newYposition = box.height;


            // const offset = monitor.getSourceClientOffset();
            // if (offset && finalX < initialX) {
            //         newXposition =  box.left - offset.x
            //         newYposition = offset.y - box.top 
            // }

            let pixelDensityW = box.width / shelf.dimensions.width; // how many mm to 1px - width
            let pixelDensityH = box.height / shelf.dimensions.height; // how many mm to 1px - height
            distFromStart = Math.round(newXposition / pixelDensityW); // this is the distance from begining of shelf in cm
            // if (finalX < initialX ) distFromStart =  Math.abs(Math.abs(distFromStart) - shelf.dimensions.width)
                    // alert(distFromStart)
           
            distFromTop = Math.round(newYposition / pixelDensityH); // this is the distance from bottom of shelf in cm
            if (distFromStart > shelf.dimensions.width) {
                if (item.payload.placement)
                    distFromStart = shelf.dimensions.width - (item.payload.placement.width * item.payload.placement.faces);
                else
                    distFromStart = shelf.dimensions.width;
            }
            if (distFromTop > shelf.dimensions.height) {
                if (item.payload.placement)
                    distFromTop = shelf.dimensions.height - (item.payload.placement.height * item.payload.placement.stack);
                else
                    distFromTop = shelf.dimensions.height;
            }

            console.log('DroppableTarget drop shelf inside', box, initialX, finalX, shelf.dimensions, newXposition,
                'pixelDensityW', pixelDensityW, 'pixelDensityH', pixelDensityH,
                'distFromStart', distFromStart, 'distFromTop', distFromTop);
            // this.props.setDistFromStart(distFromStart);
            localStorage.removeItem('distFromStart');
            localStorage.removeItem('distFromTop');
            localStorage.setItem('distFromStart', JSON.stringify(distFromStart));
            localStorage.setItem('distFromTop', JSON.stringify(distFromTop));
            /************************************************************************************************/
            props.onDrop(monitor.getItem());
        }
        return ({
            type: monitor.getItemType(),
            payload: shelf,
            distFromStart: distFromStart,
            distFromTop: distFromTop
        });
    },
    canDrop(props, monitor) {
        if (monitor.didDrop() || !monitor.isOver({ shallow: true }))
            return false;
        return props.verifyDrop ? props.verifyDrop(monitor.getItem()) : true;
    },
}, (connect, monitor, props) => ({
    connectDropTarget: connect.dropTarget(),
    canDrop: monitor.canDrop(),
    position: monitor.getClientOffset(),
    isOver: monitor.isOver(),
    isOverCurrent: monitor.isOver({ shallow: true }),
    itemType: monitor.getItemType(),
}));
type ShelfProps = ShelfComponentProps & DropProps & TargetProps;
const mapDispatchToProps = (dispatch: ThunkDispatch<AppState, {}, AnyAction>) => {
    return {
        setCurrentSelectedId: (currentSelectedId: string) => dispatch(setCurrentSelectedId(currentSelectedId)),
        switchItems: (base: PlanogramElementId, remote: PlanogramElementId) => {
            dispatch(switchItemsAction(base, remote));
        },
        addProduct: (shelf: PlanogramElementId, product: CatalogBarcode, placement?: PlacementObject) => {
            dispatch(addProductAction(shelf, product, placement));
        },
        deleteItem: (shelf: PlanogramElementId, item: PlanogramElementId) => {
            dispatch(deleteItemAction(shelf, item))
        },
        editShelfDimensions: (shelf: PlanogramElementId, dimensions: DimensionObject) => {
            dispatch(editShelfDimensionsAction(shelf, dimensions))
        },
        editShelfItemPlacement: (item: PlanogramElementId, placement: PlacementObject) => {
            dispatch(editShelfItemAction(item, placement));
        },
        setMultiSelectId: (multiSelectId: string[]) => dispatch(setMultiSelectId(multiSelectId)),
        hideProductDisplayerBarcode: () => {
            dispatch(hideProductDetailer());
        },
        updateProductDimensions: (barcode: number, dimensions: DimensionObject, afterAction: Action) => {
            dispatch(editProductDimensions(barcode, dimensions));
            dispatch(afterAction);
        },
        setStoreAisle: (aisle: PlanogramAisle, aislePid?: PlanogramElementId) => {
            dispatch(setAisleAction(aisle, aislePid));
        },
        setAisleSaveArray: (aisleSaveArray: PlanogramAisle[]) => {
            dispatch(setAisleSaveArray(aisleSaveArray));
        },
        setAisleSaveIndex: (aisleSaveIndex: number) => {
            dispatch(setAisleSaveIndex(aisleSaveIndex));
        },

    }
}
function mapStateToProps(state: AppState, ownProps: ShelfProps) {
    return {
        ...state.newPlano,
        ...ownProps,
        shelvesDetails: state.planogram.virtualStore.shelfDetails,
        aisleShelves: state.planogram.virtualStore.shelfMap,
        multiSelectId: state.planogram.display.multiSelectId,
        
        store: state.planogram.store,
        aisle: state.planogram.store != null && state.planogram.display.aisleIndex != null ? state.planogram.store.aisles[state.planogram.display.aisleIndex] : null,
        alignToLeft: state.planogram.display.alignToLeft,
        virtualStore: state.planogram.virtualStore,
        newPlano: state.newPlano,
        aisleSaveArray: state.planogram.display.aisleSaveArray,
        aisleSaveIndex: state.planogram.display.aisleSaveIndex,
    }
}
type PlanogramShelfComponentProps = ReturnType<typeof mapDispatchToProps> & ReturnType<typeof mapStateToProps>;
class PlanogramShelfComponentContainer extends React.Component<PlanogramShelfComponentProps> {
    state = {
        multiSelectId: [],
        dragImg: new Image(),
    }
    componentDidMount() {
        const img = new Image();
        img.src = "https://grid.algoretail.co.il/media/images/products/7290008909860.jpg";
        img.onload = () => this.setState({ dragImg: img });
    }
    alignThisShelfToLeft = async (shelf: PlanogramShelf, shelfPaddingFromStart?:number) => {
        const { store, aisleShelves, shelvesDetails } = this.props;

        // check if we need to align multiple shelves
        let all_shelves_to_align: PlanogramShelf[] = [];
        if (this.props.multiSelectId.length > 1) {
            // sort multiSelectId by shelf and section
            this.props.multiSelectId.sort(function (a: any, b: any) {
                let currSE_A: number = parseInt(a.substring(a.indexOf('SE') + 2, a.indexOf('SH')));
                let currSH_A: number = parseInt(a.substring(a.indexOf('SH') + 2, a.indexOf('I')));
                let currSE_B: number = parseInt(b.substring(b.indexOf('SE') + 2, b.indexOf('SH')));
                let currSH_B: number = parseInt(b.substring(b.indexOf('SH') + 2, b.indexOf('I')));
                // if it's the same shelf order by section    
                if (currSH_A === currSH_B)
                    return (currSE_A > currSE_B) ? 1 : (currSE_A < currSE_B) ? -1 : 0;
                else return (currSH_A > currSH_B) ? 1 : (currSH_A < currSH_B) ? -1 : 0;
            });
            // find each shelf represented in multiSelectId and add them to all_shelves_to_align
            for (let a = 0; a < this.props.multiSelectId.length; a++) {
                let target_A = this.props.multiSelectId[a].substring(0, this.props.multiSelectId[a].indexOf('SE'));
                let target_SE = parseInt(this.props.multiSelectId[a].substring(this.props.multiSelectId[a].indexOf('SE') + 2, this.props.multiSelectId[a].indexOf('SH')));
                let target_SH = parseInt(this.props.multiSelectId[a].substring(this.props.multiSelectId[a].indexOf('SH') + 2));
                if (store) {
                    for (let i = 0; i < store.aisles.length; i++) {
                        if (store.aisles[i].id === target_A) {
                            let thisShelf = store.aisles[i].sections[target_SE].shelves[target_SH];
                            let index = all_shelves_to_align.findIndex(shelf => shelf.id === thisShelf.id);
                            // freezer = 0: regular shelf, freezer = 1: deep freezer, freezer = 2: regular shelf set as temp freezer to allow item over item
                            if (index < 0 && thisShelf.freezer != 1 && thisShelf.freezer != 2) all_shelves_to_align.push(thisShelf);
                            // now that we've added the shelf the item is actually on, we need to check if the item covers more than one shelf.
                            // since we know this is the shelf the item starts in we can get the item's details from it.
                            let item = thisShelf.items.filter(line => line.id === this.props.multiSelectId[a]);
                            let itemTotalWidth = (item[0].placement.distFromStart ? item[0].placement.distFromStart : 0) + ((item[0].placement.pWidth ? item[0].placement.pWidth : ProductDefaultDimensions.width) * item[0].placement.faces);
                            let shelfWidth = thisShelf.dimensions.width;
                            if (itemTotalWidth > shelfWidth) {
                                while (itemTotalWidth > shelfWidth) {
                                    target_SE++;
                                    let nextShelf = store.aisles[i].sections[target_SE].shelves[target_SH];
                                    let index = all_shelves_to_align.findIndex(shelf => shelf.id === nextShelf.id);
                                    if (index < 0 && nextShelf.freezer != 1 && nextShelf.freezer != 2) all_shelves_to_align.push(nextShelf);
                                    itemTotalWidth -= shelfWidth;
                                    shelfWidth = nextShelf.dimensions.width;
                                }
                            }
                        }
                    }
                }
            }
        } else if (shelf.freezer != 1 && shelf.freezer != 2) all_shelves_to_align.push(shelf);
        // now that we have all shelvs that need to be aligned we can align them one after the other
        for (let aa = 0; aa < all_shelves_to_align.length; aa++) {
            // find latest version of this shelf in the store aisle
            let thisShelf = all_shelves_to_align[aa];
            let target_A = all_shelves_to_align[aa].id.substring(0, all_shelves_to_align[aa].id.indexOf('SE'));
            let target_SE = parseInt(all_shelves_to_align[aa].id.substring(all_shelves_to_align[aa].id.indexOf('SE') + 2, all_shelves_to_align[aa].id.indexOf('SH')));
            let target_SH = parseInt(all_shelves_to_align[aa].id.substring(all_shelves_to_align[aa].id.indexOf('SH') + 2));
            if (store) {
                for (let i = 0; i < store.aisles.length; i++) {
                    if (store.aisles[i].id === target_A) {
                        thisShelf = store.aisles[i].sections[target_SE].shelves[target_SH];
                    }
                }
            }

            // get this shelf's combined shelves and only leave in the array shelves that exist in all_shelves_to_align
            let shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[all_shelves_to_align[aa].id]));
            while (shelfDetails.main_shelf) {
                let ids = JSON.parse(JSON.stringify(shelfDetails.main_shelf));
                shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[ids]));
                thisShelf = aisleShelves[ids];
            }
            let { combined } = shelfDetails;

            let combinedShelves = [thisShelf].concat(combined.map((id: any) => aisleShelves[id]));

            // remove from combinedShelves any shelves that do not appear in all_shelves_to_align
            let newCombinedShelves = [];
            for (let i = 0; i < combinedShelves.length; i++) {
                let index = all_shelves_to_align.findIndex(shelf => shelf.id === combinedShelves[i].id);
                if (index >= 0) newCombinedShelves.push(combinedShelves[i]);
            }
            combinedShelves = newCombinedShelves;

            let A_Id = combinedShelves[0].id.substring(0, combinedShelves[0].id.indexOf('SE'));
            let SE_Index = parseInt(combinedShelves[0].id.substring(combinedShelves[0].id.indexOf('SE') + 2, combinedShelves[0].id.indexOf('SH')));
            let SH_Index = parseInt(combinedShelves[0].id.substring(combinedShelves[0].id.indexOf('SH') + 2));

            // check if this aisle has a shelf in the same height, in previous section
            let firstDistFromStart = shelfPaddingFromStart ? shelfPaddingFromStart : 0;
            if (store) {
                let store_aisle: PlanogramAisle = store.aisles.filter(aisle => aisle.id === A_Id)[0];
                if (store_aisle && store_aisle.sections[SE_Index - 1] && store_aisle.sections[SE_Index - 1].shelves[SH_Index]
                    &&
                    ((store_aisle.sections[SE_Index - 1].shelves[SH_Index].dimensions.height === store_aisle.sections[SE_Index].shelves[SH_Index].dimensions.height)
                        || SH_Index === 0
                    )) {                    // get all items in the previous shelf
                    let prev_shelf_items = JSON.parse(JSON.stringify(store_aisle.sections[SE_Index - 1].shelves[SH_Index].items));
                    // sort the items by distFromStart in case they are not already sorted
                    prev_shelf_items.sort(function (a: any, b: any) {
                        let aNumber = a.placement.distFromStart ? a.placement.distFromStart : 0;
                        let bNumber = b.placement.distFromStart ? b.placement.distFromStart : 0;
                        let aId = a.Id;
                        let bId = b.Id;
                        return (aNumber > bNumber) ? 1 : (aNumber < bNumber) ? -1 : ((aId > bId) ? 1 : (aId < bId) ? -1 : 0);
                    });

                    // if so check if the last item (based on distFromStart) on the previous shelf intrudes into this shelf
                    let lastItem = prev_shelf_items[prev_shelf_items.length - 1];
                    if (lastItem) {
                        let endPoint = (lastItem.placement.distFromStart ? lastItem.placement.distFromStart : 0) + ((lastItem.placement.pWidth != undefined ? lastItem.placement.pWidth : ProductDefaultDimensions.width) * lastItem.placement.faces);
                        let leftInShelf = store_aisle.sections[SE_Index].shelves[SH_Index].dimensions.width - endPoint;
                        if (leftInShelf < 0) firstDistFromStart = (-1 * leftInShelf);
                    }
                }
            }

            // save initial state of shelvs with items ordered by distFromStart (distance from start of shelf)
            for (let i = 0; i < combinedShelves.length; i++) {
                // sort shelf items by distFromStart
                combinedShelves[i].items.sort(function (a: any, b: any) {
                    let aNumber = a.placement.distFromStart ? a.placement.distFromStart : 0;
                    let bNumber = b.placement.distFromStart ? b.placement.distFromStart : 0;
                    let aId = a.Id;
                    let bId = b.Id;
                    return (aNumber > bNumber) ? 1 : (aNumber < bNumber) ? -1 : ((aId > bId) ? 1 : (aId < bId) ? -1 : 0);
                });
            }
            let viewCombinedShelves = JSON.parse(JSON.stringify(combinedShelves));
            // empty all items from current shelvs
            for (let i = 0; i < combinedShelves.length; i++) {
                combinedShelves[i].items = [];
            }

            let distFromStart = firstDistFromStart;
            let distInShelf = firstDistFromStart;
            let index = 0;
            let startOfShelfId = '';
            let spaceInShelf = 0;
            // go over the initial state and populate each shelf in combinedShelves with the correct items in the correct order
            let shelfId = 0;
            for (let i = 0; i < viewCombinedShelves.length; i++) {
                if (i === 0) {
                    startOfShelfId = viewCombinedShelves[i].id;
                    spaceInShelf = viewCombinedShelves[shelfId].dimensions.width;
                }
                for (let n = 0; n < viewCombinedShelves[i].items.length; n++) {
                    let item = JSON.parse(JSON.stringify(viewCombinedShelves[i].items[n]));
                    /* return items above main line (item index >= 100) back into the shelf without altering anything else */
                    let itemInd = parseInt(item.id.substring(item.id.indexOf('I') + 1));
                    if (itemInd >= 100) {
                        if (item.shelfLevel === 1 || item.shelfLevel === undefined) combinedShelves[shelfId].items.push(item); // for items in freezer === 2
                        /*  verify items with shelfLevel 2 position */
                        else { // for items with shelfLevel > 1
                            // we need to return the item to it's original shelf
                            let destinationSec = parseInt(item.id.substring(item.id.indexOf('SE') + 2, item.id.indexOf('SH')));
                            let destinationSh = parseInt(item.id.substring(item.id.indexOf('SH') + 2, item.id.indexOf('I')));
                            let aisle: PlanogramAisle = JSON.parse(JSON.stringify(blankPlanogramAisle));
                            let aisles = this.props.store ? this.props.store.aisles : undefined;
                            let aisleId = shelf.id.substring(0, shelf.id.indexOf('SE'));
                            if (aisles) {
                                for (let i = 0; i < aisles.length; i++) {
                                    if (aisles[i].id === aisleId) {
                                        aisle = aisles[i];
                                        break;
                                    }
                                }
                            }
                            aisle.sections[destinationSec].shelves[destinationSh].items.push(item);
                        }
                    }
                    else {
                        if (spaceInShelf <= 0) {
                            shelfId++; // the shelf id in the viewCombinedShelves array
                            if (viewCombinedShelves[shelfId]) {
                                // there is a next shelf so we update the shelfNumber, startOfShelfId, spaceInShelf and index
                                // let shelfNumber = parseInt(JSON.parse(JSON.stringify(startOfShelfId.substring(startOfShelfId.indexOf('SE') + 2, startOfShelfId.indexOf('SH'))))) + 1;
                                let old_sec_num = parseInt(JSON.parse(JSON.stringify(startOfShelfId.substring(startOfShelfId.indexOf('SE') + 2, startOfShelfId.indexOf('SH')))));
                                let new_sec_num = parseInt(JSON.parse(JSON.stringify(viewCombinedShelves[shelfId].id.substring(viewCombinedShelves[shelfId].id.indexOf('SE') + 2, viewCombinedShelves[shelfId].id.indexOf('SH')))))
                                startOfShelfId = JSON.parse(JSON.stringify(startOfShelfId.substring(0, startOfShelfId.indexOf('SE') + 2) + new_sec_num + startOfShelfId.substring(startOfShelfId.indexOf('SH'))));
                                if (new_sec_num - old_sec_num === 1) {
                                    distInShelf = (spaceInShelf * -1);
                                    distFromStart = (spaceInShelf * -1);
                                    spaceInShelf = viewCombinedShelves[shelfId].dimensions.width - distInShelf;
                                } else {
                                    distInShelf = 0;
                                    distFromStart = 0;
                                    spaceInShelf = 0;
                                }
                                index = 0;
                            } else
                                // this shelf dosn't exist in the arry so we stay in this shelf
                                shelfId--;
                        }
                        item.id = startOfShelfId + 'I' + index;
                        index++;
                        item.placement.distFromStart = distFromStart;
                        let itemWidth = viewCombinedShelves[i].items[n].placement.pWidth;
                        distFromStart += (itemWidth != undefined ? itemWidth : ProductDefaultDimensions.width) * item.placement.faces;
                        distInShelf += (itemWidth != undefined ? itemWidth : ProductDefaultDimensions.width) * item.placement.faces;
                        if (viewCombinedShelves[shelfId]) {
                            spaceInShelf = viewCombinedShelves[shelfId].dimensions.width - distInShelf;
                            combinedShelves[shelfId].items.push(item);
                        }
                    }
                }
            }
        }
        await this.saveAisleShelf(shelf);
    }
    addShelfItem = async (shelf: PlanogramShelf, newItem?: PlanogramItem | null, shlfStart?: number | null, newItems?: PlanogramItem[]) => {
        // make sure shelf has no undefined items
        let initItems = JSON.parse(JSON.stringify(shelf.items))
        shelf.items = [];
        for (let i = 0; i < initItems.length; i++) {
            if (initItems[i] && typeof initItems[i] != 'undefined')
                shelf.items.push(initItems[i]);
        }
        if (newItems) {
            let initItems = JSON.parse(JSON.stringify(newItems))
            newItems = [];
            for (let i = 0; i < initItems.length; i++) {
                if (initItems[i] && typeof initItems[i] != 'undefined') {
                    newItems.push(initItems[i]);
                }
            }
                }
                let itemsShelfWidth:any = shelf.items[0] && shelf.items[0].placement ? shelf.items[0].placement.distFromStart : 0
                    shelf.items.forEach((_item:PlanogramItem) => {
                           let itemWidth = _item.placement.pWidth ? _item.placement.pWidth * _item.placement.faces : 0
                           itemsShelfWidth += itemWidth
                    });
                   
                                let { virtualStore } = this.props;
                                let shelvesDetails = virtualStore.shelfDetails;
                                let aisleShelves = virtualStore.shelfMap;
                    let fullShelf = JSON.parse(JSON.stringify(shelf));
                    let shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[shelf.id]));
                    // get the full combined shelves
                    while (shelfDetails.main_shelf) {
                        let ids = JSON.parse(JSON.stringify(shelfDetails.main_shelf));
                        shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[ids]));
                    }
                    let { combined } = shelfDetails;
                    let combinedShelves = [fullShelf].concat(combined.map((id: any) => aisleShelves[id]));
                    let fullSelfItemsWidth = 0
                    let fullSelfWidth = 0
                    combinedShelves.forEach((shelf:PlanogramShelf) => {
                        fullSelfWidth += (shelf.dimensions.width?shelf.dimensions.width:0)
                        shelf.items.forEach((_item:PlanogramItem) => {
                            let itemWidth = _item.placement.pWidth ? _item.placement.pWidth * _item.placement.faces : 0
                            fullSelfItemsWidth += itemWidth
                        });
                    });
                    let shelfIndex = 0
                    for(var w = fullSelfItemsWidth; w > shelf.dimensions.width;){
                        shelfIndex = shelfIndex+1
                        w =  w-shelf.dimensions.width
                    }



    
                //if there are multiple items to add to the begining of the shelf add them
              if (newItems && newItems.length > 0) {
                    // push all items on the shelf by 1 to allow to push new items at the begining of the shelf
                    for (let i = 0; i < shelf.items.length; i++) {
                        let itemDist = 0;
                        if (shelf.items[i].placement.distFromStart != undefined)
                            itemDist = parseInt(JSON.stringify(shelf.items[i].placement.distFromStart));
                    }
                    // add the new items at the begining of the shelf items array
                    if( !newItem ) {
                        for (let i = 0 - 1; i < newItems.length ; i++) {
                            if (newItems[i]) shelf.items.push(newItems[i]);
                        }
                    } else {
                        for (let i = newItems.length-1 ; i >= 0 ; i--) {
                            if (newItems[i]) shelf.items.push(newItems[i]);
                        }
                    }1
             
                    // go over all the items on the shelf and re-do their distance from start of shelf
                    for (let i = 0; i < shelf.items.length; i++) {
                        if (shelf.items[i]) {
                            let distToNextItem = 0;
                            let itemEndPoint = parseInt(JSON.stringify(shelf.items[i].placement.distFromStart)) + (shelf.items[i].placement.faces * parseInt(JSON.stringify(shelf.items[i].placement.pWidth)));
                            if (shelf.items[i + 1] && (i + 1 <= newItems.length || i > newItems.length)) distToNextItem = parseInt(JSON.stringify(shelf.items[i + 1].placement.distFromStart)) - itemEndPoint;
                            if (i === 0 && shlfStart != undefined) {
                                // shelf.items[0].placement.distFromStart =   itemEndPoint;
                                itemEndPoint += shlfStart;
                            }
                            if (shelf.items[i + 1]) shelf.items[i + 1].placement.distFromStart = itemEndPoint + (distToNextItem > 0 ? distToNextItem : 0);
                        }
                    }
                    await this.saveSnapShot(shelf);
            }


        // update shelf.distFromStart if shlfStart was sent
        if (shlfStart && shlfStart >= 0) {
            shelf.distFromStart = shlfStart;
            /***************************/
            let aisles_1 = this.props.store ? this.props.store.aisles : undefined;
            let aisleId_1 = shelf.id.substring(0, shelf.id.indexOf('SE'));
            let aisle_1: PlanogramAisle = JSON.parse(JSON.stringify(blankPlanogramAisle));
            if (aisles_1) {
                for (let i = 0; i < aisles_1.length; i++) {
                    if (aisles_1[i].id === aisleId_1) {
                        aisle_1 = aisles_1[i];
                        let seI = parseInt(shelf.id.substring(shelf.id.indexOf('SE') + 2, shelf.id.indexOf('SH')));
                        let shI = parseInt(shelf.id.substring(shelf.id.indexOf('SH') + 2));
                        if (aisle_1.sections[seI] && aisle_1.sections[seI].shelves[shI])
                            aisle_1.sections[seI].shelves[shI] = shelf;
                        break;
                    }
                }
            }
            if (aisle_1.id === aisleId_1) {
                await this.props.setStoreAisle(aisle_1);
            }
            /***************************/
        }
        // make sure, in case no shlfStart was sent that the shelf.distFromStart fits with the first Item (ordered by distFromStart) on that shelf
        else {
            if (shelf.distFromStart > 0) {
                let num: number = shelf.items[0] && shelf.items[0].placement.distFromStart ? shelf.items[0].placement.distFromStart : 0;
                if (shelf.items.length >= 1 && num >= 0) shelf.distFromStart = num;
            }
        }
        // if( newItem && newItem.product === 0 ) {
        //     alert(newItem.product +' '+newItem.placement.distFromStart)
        //     newItem.placement.distFromStart = ((itemsShelfWidth + this.props.aisleShelves[shelf.id].distFromStart) - this.props.aisleShelves[shelf.id].dimensions.width) 
        //     alert(newItem.placement.distFromStart)
        // }
        // // check if this shelf even has room to add a new item;
        if (itemsShelfWidth >= shelf.dimensions.width && newItem && newItem.product > 0 && shelf.freezer != 1 && shelf.freezer != 2 && (newItem.shelfLevel === 1 || newItem.shelfLevel === undefined)) {
            // if there is a next shelf activate function with the rest of the items 
            let fullShelf = JSON.parse(JSON.stringify(shelf));
            // this is the data for the actual shelf
            let shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[shelf.id]));

            // get the full combined shelves
            while (shelfDetails.main_shelf) {
                let ids = JSON.parse(JSON.stringify(shelfDetails.main_shelf));
                shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[ids]));
                fullShelf = aisleShelves[ids];
            }
            let { combined } = shelfDetails;
            if (combined == null) combined = [];
            let combinedShelves = [fullShelf].concat(combined.map((id: any) => aisleShelves[id]));
            // find the index of the current shelf 
            let currI = combinedShelves.findIndex(object => object.id === shelf.id);
            if (currI >= 0 && combinedShelves[currI + 1]) {
                // move item/s to the next shelf
                newItem.placement.distFromStart =  ( this.props.aisleShelves[shelf.id].dimensions.width - itemsShelfWidth )* -1
               
                await this.addShelfItem(JSON.parse(JSON.stringify(combinedShelves[currI + 1])), newItem);
                
            }
            return;
        }
        // if there is a new item add it at the end of the shelf
        if (newItem && newItem.product > 0) {
            if (shelf.freezer === 2
            ) {
                if (newItem.id === '') {
                    console.log('newItem.id === ""');
                    if (shelf.items.length > 1) {
                        let lastI = parseInt(shelf.items[shelf.items.length - 1].id.substring(shelf.items[shelf.items.length - 1].id.indexOf('I') + 1));
                        let newI = 0;
                        if (lastI < 100) newI = lastI + 100;
                        else newI = lastI + 1;
                        newItem.id = shelf.id + "I" + newI;
                    } else {
                        newItem.id = shelf.id + "I" + 100;
                    }
                }
            }
            if (newItem.shelfLevel > 1) {
                if (newItem.id === '') {
                    if (shelf.items.length > 1) {
                        let lastI = parseInt(shelf.items[shelf.items.length - 1].id.substring(shelf.items[shelf.items.length - 1].id.indexOf('I') + 1));
                        let newI = 0;
                        if (lastI < 100) newI = lastI + 100;
                        else newI = lastI + 1;
                        newItem.id = shelf.id + "I" + newI;
                    } else {
                        newItem.id = shelf.id + "I" + 100;
                    }
                }
                // now that we have an id to the new item we can save it in the linked bottom level item as linkedItemUp
                let sItem = shelf.items.filter(line => line.id === this.props.multiSelectId[0]);
                if (sItem && sItem[0]) sItem[0].linkedItemUp = newItem.id;
            }
            /************************************************************************/
            shelf.items.push(newItem);
        }


        if (shelf.freezer === 1 || shelf.freezer === 2) {
            // /* Barak 22.11.20 */ await this.saveSnapShot(shelf);
            /* Barak 22.11.20 */await this.saveAisleShelf(shelf);
            return;
        }

        // save initial state of shelf with items ordered by distFromStart (distance from start of shelf)
        shelf.items.sort(function (a: any, b: any) {
            let aNumber = a.placement.distFromStart ? a.placement.distFromStart : 0;
            let bNumber = b.placement.distFromStart ? b.placement.distFromStart : 0;
            let aId = a.Id;
            let bId = b.Id;
            return (aNumber > bNumber) ? 1 : (aNumber < bNumber) ? -1 : ((aId > bId) ? 1 : (aId < bId) ? -1 : 0);
        });

        // go over shelf and if there are 2 items occupying the same space and they are the same product 
        // - increase faces to the first and remove the second
        for (let i = 0; i < shelf.items.length; i++) {
            if (shelf.items[i]) {
                if (parseInt(JSON.stringify(shelf.items[i].placement.distFromStart)) < shelf.distFromStart)
                    shelf.items[i].placement.distFromStart = shelf.distFromStart;
                if (shelf.items[i + 1] && (shelf.items[i + 1].shelfLevel === 1 || shelf.items[i + 1].shelfLevel === undefined)) {
                    console.log('check item a', shelf.items[i], 'vs item a+1', shelf.items[i + 1]);
                    let first_dist: number = parseInt(JSON.stringify(shelf.items[i].placement.distFromStart));
                    let first_end = first_dist + (parseInt(JSON.stringify(shelf.items[i].placement.pWidth)) * shelf.items[i].placement.faces);
                    let second_dist: number = parseInt(JSON.stringify(shelf.items[i + 1].placement.distFromStart));
                    let second_end = second_dist + (parseInt(JSON.stringify(shelf.items[i + 1].placement.pWidth)) * shelf.items[i + 1].placement.faces);
                    if (((second_dist <= first_dist && second_end <= first_end) || (second_dist >= first_dist && second_end <= first_end) || (second_dist >= first_dist && second_end > first_end))
                        && shelf.items[i].product === shelf.items[i + 1].product) {
                        // same item - increase faces
                        shelf.items[i].placement.faces += shelf.items[i + 1].placement.faces;
                        // remove second item 
                        shelf.items.splice(i + 1, 1);
                    }
                }
            }
        }

        let viewShelf = JSON.parse(JSON.stringify(shelf));
        // empty all items from current shelf
        shelf.items = [];

        let distFromStart = viewShelf.items.length > 0 && viewShelf.items[0] ? viewShelf.items[0].placement.distFromStart : 0;
        let distInShelf = viewShelf.items.length > 0 && viewShelf.items[0] ? viewShelf.items[0].placement.distFromStart : 0;
        let index = 0;
        let startOfShelfId = viewShelf.id;
        let spaceInShelf = 0;
        // go over the initial state and populate each shelf in combinedShelves with the correct items in the correct order
        for (let n = 0; n < viewShelf.items.length; n++) {
            let item = JSON.parse(JSON.stringify(viewShelf.items[n]));
            /* Barak 26.11.20 - return items above main line (item index >= 100) back into the shelf without altering anything else */
            let itemInd = parseInt(item.id.substring(item.id.indexOf('I') + 1));
            if (itemInd >= 100) shelf.items.push(item);
            else {
                /************************************************************************************************************************/
                let nextItem = null;
                if (viewShelf.items[n + 1]) nextItem = viewShelf.items[n + 1];
                // console.log('item', item, 'itemExists', itemExists, 'nextItem', nextItem);
                if (spaceInShelf < 0) {
                    // if there is a next shelf activate function with the rest of the items 
                    let fullShelf = JSON.parse(JSON.stringify(shelf));
                    // this is the data for the actual shelf
                    let shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[shelf.id]));

                    // get the full combined shelves
                    while (shelfDetails.main_shelf) {
                        let ids = JSON.parse(JSON.stringify(shelfDetails.main_shelf));
                        shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[ids]));
                        fullShelf = aisleShelves[ids];
                    }
                    let { combined } = shelfDetails;
                    if (combined == null) combined = [];
                    let combinedShelves = [fullShelf].concat(combined.map((id: any) => aisleShelves[id]));
                    // find the index of the current shelf 
                    let currI = combinedShelves.findIndex(object => object.id === viewShelf.id);
                    if (currI >= 0 && combinedShelves[currI + 1]) {
                        while (combinedShelves[currI + 1] && spaceInShelf * -1 > combinedShelves[currI + 1].dimensions.width) {
                            spaceInShelf += combinedShelves[currI + 1].dimensions.width;
                            console.log('call addShelfItem for combinedShelves', JSON.parse(JSON.stringify(combinedShelves)), 'shelf', JSON.parse(JSON.stringify(combinedShelves[currI + 1])), 'changing start to', combinedShelves[currI + 1].dimensions.width);
                            await this.addShelfItem(JSON.parse(JSON.stringify(combinedShelves[currI + 1])), blankItem, combinedShelves[currI + 1].dimensions.width, []);
                            currI++;
                        }
                        let newItemsToSend: PlanogramItem[] = [];
                        if (n < viewShelf.items.length) {
                            // get all items left over into a new array
                            for (let i = n; i < viewShelf.items.length; i++) {
                                viewShelf.items[i].placement.distFromStart =  shlfStart//viewShelf.items[i-1] ? viewShelf.items[i-1].distFromStart (spaceInShelf * -1)
                                newItemsToSend.push(viewShelf.items[i]);
                            }
                            // // remove the items we're passing to the next shelf from the array of this shelf
                            // viewShelf.items.splice(n, viewShelf.items.length - n);
                        }//(spaceInShelf * -1)
                        await this.addShelfItem(JSON.parse(JSON.stringify(combinedShelves[currI + 1])), blankItem, (spaceInShelf * -1), newItemsToSend);
                        spaceInShelf = 0;
                        // leave the loop since there are no more items in this shelf to take care of  
                        //  - all the left over items are moved to the next shelf
                        break;
                    }
                }
                if (item.placement.distFromStart > distFromStart && (startOfShelfId === item.id.substring(0, item.id.indexOf('I')) || item.id === ''))
                    distFromStart = item.placement.distFromStart;
                item.id = startOfShelfId + 'I' + index;
                index++;
                item.placement.distFromStart = distFromStart;
                let itemWidth = viewShelf.items[n].placement.pWidth;
                let itemSpace = (itemWidth != undefined ? itemWidth : ProductDefaultDimensions.width) * item.placement.faces;
                distFromStart += itemSpace;
                distInShelf += itemSpace;
                spaceInShelf = viewShelf.dimensions.width - distInShelf;
                // add item back into the shelf
                shelf.items.push(item);


                // check if there is a next shelf 
                let fullShelf = JSON.parse(JSON.stringify(shelf));
                // this is the data for the actual shelf
                let shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[shelf.id]));
                // get the full combined shelves
                while (shelfDetails.main_shelf) {
                    let ids = JSON.parse(JSON.stringify(shelfDetails.main_shelf));
                    shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[ids]));
                    fullShelf = aisleShelves[ids];
                }
                let { combined } = shelfDetails;
                if (combined == null) combined = [];
                let combinedShelves = [fullShelf].concat(combined.map((id: any) => aisleShelves[id]));
                // find the index of the current shelf 
                let currI = combinedShelves.findIndex(object => object.id === viewShelf.id);


                if (distFromStart > viewShelf.dimensions.width && currI >= 0 && combinedShelves[currI + 1]) {
                    // only of there is a next shelf can we reduce the viewShelf.dimensions.width from the next distFromStart 
                    spaceInShelf = (distFromStart - viewShelf.dimensions.width) * -1;
                    distFromStart = distFromStart - viewShelf.dimensions.width;
                }
                console.log('--- item', item, 'distInShelf', distInShelf, 'distFromStart next', distFromStart, 'spaceInShelf', spaceInShelf, "shelf", JSON.parse(JSON.stringify(shelf)), "combinedShelves", JSON.parse(JSON.stringify(combinedShelves)));
            }
        }
        // in case the last item on the shelf is the one that extends into the next shelf, 
        // after we're done going over all items we check if the spaceInShelf has extended byond the current shelf
        if (spaceInShelf < 0) {
            console.log('last item on shelf extends beyond this shelf. spaceInShelf', spaceInShelf);
            // if there is a next shelf activate function to update the next shelf's distFromStart 
            let fullShelf = JSON.parse(JSON.stringify(shelf));
            // this is the data for the actual shelf
            let shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[shelf.id]));

            // get the full combined shelves
            while (shelfDetails.main_shelf) {
                let ids = JSON.parse(JSON.stringify(shelfDetails.main_shelf));
                shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[ids]));
                fullShelf = aisleShelves[ids];
            }
            let { combined } = shelfDetails;
            if (combined == null) combined = [];
            let combinedShelves = [fullShelf].concat(combined.map((id: any) => aisleShelves[id]));
            // find the index of the current shelf 
            let currI = combinedShelves.findIndex(object => object.id === viewShelf.id);
            if (currI >= 0 && combinedShelves[currI + 1]) {
                while (combinedShelves[currI + 1] && spaceInShelf * -1 > combinedShelves[currI + 1].dimensions.width) {
                    spaceInShelf += combinedShelves[currI + 1].dimensions.width;
                    console.log('call addShelfItem for shelf', JSON.parse(JSON.stringify(combinedShelves[currI + 1])), 'changing start to', combinedShelves[currI + 1].dimensions.width);
                    await this.addShelfItem(JSON.parse(JSON.stringify(combinedShelves[currI + 1])), blankItem, combinedShelves[currI + 1].dimensions.width, []);
                    currI++;
                }
                let newItemsToSend: PlanogramItem[] = [];
                console.log('call addShelfItem for shelf', JSON.parse(JSON.stringify(combinedShelves[currI + 1])), 'changing start to', spaceInShelf * -1, 'new items', newItemsToSend);
                await this.addShelfItem(JSON.parse(JSON.stringify(combinedShelves[currI + 1])), blankItem, (spaceInShelf * -1), newItemsToSend);
                console.log('after call addShelfItem');
            }
        }

        /* Barak 21.12.20 - verify items with shelfLevel 2 position */
        for (let b = 0; b < shelf.items.length; b++) {
            if (shelf.items[b].linkedItemUp) {
                let upItem = shelf.items.filter(line => line.id === shelf.items[b].linkedItemUp);
                if (upItem && upItem[0]) {
                    upItem[0].placement.distFromStart = shelf.items[b].placement.distFromStart;
                    upItem[0].linkedItemDown = shelf.items[b].id;
                    upItem[0].id = shelf.items[b].id.substring(0, shelf.items[b].id.indexOf('I')) + 'I' + upItem[0].id.substring(upItem[0].id.indexOf('I') + 1);
                    shelf.items[b].linkedItemUp = upItem[0].id;
                }
                console.log('linkedItemUp', shelf.items[b], upItem[0], shelf);
            }
        }
        await this.saveAisleShelf(JSON.parse(JSON.stringify(shelf)));
    }
  
    createFreezerBoxArr = (shelf: PlanogramShelf) => {
        // create a 2d array corresponding to the freezer
        let boxArr: any[] = [];
        for (let i = 0; i <= shelf.dimensions.width; i++) {
            boxArr[i] = [];
            for (let a = 0; a <= shelf.dimensions.height; a++) {
                boxArr[i][a] = { value: 0, id: '' };
            }
        }

        // at the loaction of each item place 1's in the corresponding coordinates
        for (let i = 0; i < shelf.items.length; i++) {
            let item = shelf.items[i];
            if (item) {
                let startX = item.placement.distFromStart ? item.placement.distFromStart : 0;
                let itemTotalWidth = startX + ((item.placement.pWidth ? item.placement.pWidth : ProductDefaultDimensions.width) * item.placement.faces);
                let startY = item.placement.distFromTop ? item.placement.distFromTop : 0;
                let itemTotalHeight = startY + ((item.placement.pHeight ? item.placement.pHeight : ProductDefaultDimensions.height) * item.placement.stack);
                for (let x = startX; x <= itemTotalWidth; x++) {
                    for (let y = startY; y <= itemTotalHeight; y++) {
                        if (boxArr[x] && boxArr[x][y])
                            boxArr[x][y] = { value: 1, id: item.id };
                    }
                }
            }
        }

        return boxArr;
    }
    saveSnapShot = async (shelf: PlanogramShelf) => {
        let tArray = this.props.aisleSaveArray;
        let tIndex = this.props.aisleSaveIndex;
        if (tArray.length === 0 || (tArray.length > 0 && tArray[0].id != shelf.id.substring(0, shelf.id.indexOf('SE')))) {
            let newAisle = await planogramProvider.getStoreAisle(this.props.store ? this.props.store.store_id : 0, this.props.aisleId != undefined ? this.props.aisleId : 0);
            tArray = [JSON.parse(JSON.stringify(newAisle))];
            tIndex = 0;
            await this.props.setAisleSaveArray(tArray);
            await this.props.setAisleSaveIndex(tIndex);
        }
        let aisle: PlanogramAisle = JSON.parse(JSON.stringify(blankPlanogramAisle));
        let aisles = this.props.store ? this.props.store.aisles : undefined;
        let aisleId = shelf.id.substring(0, shelf.id.indexOf('SE'));
        if (aisles) {
            for (let i = 0; i < aisles.length; i++) {
                if (aisles[i].id === aisleId) {
                    aisle = aisles[i];
                    break;
                }
            }
        }
        if (aisle.id === aisleId) {
            tIndex++;
            // insert into array tArray item aisle at location tIndex, deleting 0 items first
            tArray.splice(tIndex, 0, JSON.parse(JSON.stringify(aisle)));
            if (tArray.length > tIndex + 1) {
                tArray.length = tIndex + 1;
            }
            await this.props.setAisleSaveArray(tArray);
            await this.props.setAisleSaveIndex(tIndex);
            await this.props.setStoreAisle(aisle);
        }
    }
    saveAisleShelf = async (shelf: PlanogramShelf) => {
        let aisle: PlanogramAisle = JSON.parse(JSON.stringify(blankPlanogramAisle));
        let aisles = this.props.store ? this.props.store.aisles : undefined;
        let aisleId = shelf.id.substring(0, shelf.id.indexOf('SE'));
        if (aisles) {
            for (let i = 0; i < aisles.length; i++) {
                if (aisles[i].id === aisleId) {
                    aisle = aisles[i];
                    let seI = parseInt(shelf.id.substring(shelf.id.indexOf('SE') + 2, shelf.id.indexOf('SH')));
                    let shI = parseInt(shelf.id.substring(shelf.id.indexOf('SH') + 2));
                    if (aisle.sections[seI] && aisle.sections[seI].shelves[shI]) {
                        aisle.sections[seI].shelves[shI] = JSON.parse(JSON.stringify(shelf));
                    }
                    break;
                }
            }
        }
        if (aisle.id === aisleId) {
            await this.props.setStoreAisle(aisle);
            await this.checkLevel2(aisle);
        }
    }
    checkLevel2 = async (aisle: PlanogramAisle) => {
        let combinedItems: any[] = [];
        for (let a = 0; a < aisle.sections.length; a++) {
            for (let b = 0; b < aisle.sections[a].shelves.length; b++) {
                combinedItems = combinedItems.concat(aisle.sections[a].shelves[b].items);
            }
        }
        for (let c = 0; c < combinedItems.length; c++) {
            if (combinedItems[c].linkedItemUp) {
                let upItem = combinedItems.filter((line: any) => line.id === combinedItems[c].linkedItemUp);
                if (upItem && upItem[0]) {
                    let itemToMove = JSON.parse(JSON.stringify(upItem[0]));
                    // find location of item to move
                    let moveSec = parseInt(itemToMove.id.substring(itemToMove.id.indexOf('SE') + 2, itemToMove.id.indexOf('SH')));
                    let moveSh = parseInt(itemToMove.id.substring(itemToMove.id.indexOf('SH') + 2, itemToMove.id.indexOf('I')));
                    let index = aisle.sections[moveSec].shelves[moveSh].items.findIndex(line => line.id === itemToMove.id);
                    // remove this item from original shelf
                    aisle.sections[moveSec].shelves[moveSh].items.splice(index, 1);
                    // update itemToMove properties
                    itemToMove.placement.distFromStart = combinedItems[c].placement.distFromStart;
                    itemToMove.linkedItemDown = combinedItems[c].id;
                    // itemToMove.id = combinedItems[c].id.substring(0, combinedItems[c].id.indexOf('I')) + 'I' + itemToMove.id.substring(upItem[0].id.indexOf('I') + 1);
                    itemToMove.id = ''
                    // find new location of item and create new item id
                    let destinationSec = parseInt(combinedItems[c].id.substring(combinedItems[c].id.indexOf('SE') + 2, combinedItems[c].id.indexOf('SH')));
                    let destinationSh = parseInt(combinedItems[c].id.substring(combinedItems[c].id.indexOf('SH') + 2, combinedItems[c].id.indexOf('I')));
                    if (aisle.sections[destinationSec].shelves[destinationSh].items.length > 1) {
                        let lastI = parseInt(aisle.sections[destinationSec].shelves[destinationSh].items[aisle.sections[destinationSec].shelves[destinationSh].items.length - 1].id.substring(aisle.sections[destinationSec].shelves[destinationSh].items[aisle.sections[destinationSec].shelves[destinationSh].items.length - 1].id.indexOf('I') + 1));
                        let newI = 0;
                        if (lastI < 100) newI = lastI + 100;
                        else newI = lastI + 1;
                        itemToMove.id = aisle.sections[destinationSec].shelves[destinationSh].id + "I" + newI;
                    } else {
                        itemToMove.id = aisle.sections[destinationSec].shelves[destinationSh].id + "I" + 100;
                    }
                    // add itemToMove to its new location
                    aisle.sections[destinationSec].shelves[destinationSh].items.push(itemToMove);
                    // update the linkedItemUp with the new item id 
                    combinedItems[c].linkedItemUp = itemToMove.id;
                }
                else {
                    combinedItems[c].linkedItemUp = null;
                }
            }
        }
        await this.props.setStoreAisle(aisle);
    }
    orderFreezerShelfWidth = (shelf: PlanogramShelf, item: PlanogramItem, placement: PlacementObject, originalPlacement: PlacementObject) => {
        // create the item array we will return 
        let itemArray: string[] = [];
        // get all items that are in the path of this item (directly or indirectly)
        let tempItem = JSON.parse(JSON.stringify(item));
        // tempItem.placement = placement;
        let itemsInPath = this.getItemsInTheWayRight(shelf, tempItem, itemArray);
        itemsInPath = itemsInPath.filter((v, i, a) => a.indexOf(v) === i);
        // now that we have all the items in the way we canget them and order them by distFromStart
        let itemsToMove: any[] = [];
        for (let i = 0; i < itemsInPath.length; i++) {
            let ind = parseInt(itemsInPath[i].substring(itemsInPath[i].indexOf('I') + 1));
            itemsToMove.push(shelf.items[ind]);
        }
        itemsToMove.sort(function (a: any, b: any) {
            let aNumber = a.placement.distFromStart ? a.placement.distFromStart : 0;
            let bNumber = b.placement.distFromStart ? b.placement.distFromStart : 0;
            let aId = a.Id;
            let bId = b.Id;
            return (aNumber > bNumber) ? 1 : (aNumber < bNumber) ? -1 : ((aId > bId) ? 1 : (aId < bId) ? -1 : 0);
        });
        // if this array has items that have the same distFromStart we'll count only the widest of them
        let widthArray: any[] = [];
        for (let i = 0; i < itemsToMove.length; i++) {
            let ind = widthArray.findIndex(line => line.placement.distFromStart === itemsToMove[i].placement.distFromStart);
            if (ind < 0) widthArray.push(itemsToMove[i]);
            else {
                let newWidth = parseInt(JSON.stringify(itemsToMove[i] && itemsToMove[i].placement && itemsToMove[i].placement.pWidth ? itemsToMove[i].placement.pWidth : ProductDefaultDimensions.width));
                let newEnd = newWidth * itemsToMove[i].placement.faces;
                let currWidth = parseInt(JSON.stringify(widthArray[ind] && widthArray[ind].placement && widthArray[ind].placement.pWidth ? widthArray[ind].placement.pWidth : ProductDefaultDimensions.width));
                let currEnd = currWidth * widthArray[ind].placement.faces;
                if (newEnd > currEnd) widthArray[ind] = itemsToMove[i];
            }
        }
        // now that all items in path are arranged by distFromStart, we can check if there is room for this item to change faces
        let initEndPoint = (placement.distFromStart ? placement.distFromStart : 0) + (parseInt(JSON.stringify(placement && placement.pWidth ? placement.pWidth : ProductDefaultDimensions.width)) * placement.faces);
        let widthAvail = shelf.dimensions.width - initEndPoint;
        for (let i = 0; i < widthArray.length; i++) {
            let itemWidth: number = parseInt(JSON.stringify(widthArray[i] && widthArray[i].placement && widthArray[i].placement.pWidth ? widthArray[i].placement.pWidth : ProductDefaultDimensions.width));
            widthAvail -= (itemWidth * widthArray[i].placement.faces);
        }
        // if widthAvail < 0 there is no room to add the new faces
        if (widthAvail < 0) {
            alert("no room to change this item's position");
            return true; // there is a problem
        }
        // if there is room, we may need to move the other items based on itemsToMove
        let initEndOfItem = (placement.distFromStart ? placement.distFromStart : 0) + ((placement.pWidth ? placement.pWidth : ProductDefaultDimensions.width) * placement.faces);
        let moveArray: any[] = [];
        for (let i = 0; i < itemsToMove.length; i++) {
            let ind = moveArray.findIndex(line => line.distFromStart === itemsToMove[i].placement.distFromStart);
            if (ind < 0) moveArray.push({ distFromStart: itemsToMove[i].placement.distFromStart, items: [itemsToMove[i]] });
            else moveArray[ind].items.push(itemsToMove[i]);
        }
        for (let i = 0; i < moveArray.length; i++) {
            if (moveArray[i].distFromStart < initEndOfItem) {
                let newInitEndOfItem = initEndOfItem;
                for (let x = 0; x < moveArray[i].items.length; x++) {
                    moveArray[i].items[x].placement.distFromStart = initEndOfItem;
                    let newEndPoint = (moveArray[i].items[x].placement.distFromStart ? moveArray[i].items[x].placement.distFromStart : 0) + ((moveArray[i].items[x].placement.pWidth ? moveArray[i].items[x].placement.pWidth : ProductDefaultDimensions.width) * moveArray[i].items[x].placement.faces);
                    if (newEndPoint > newInitEndOfItem) newInitEndOfItem = newEndPoint;
                }
                initEndOfItem = newInitEndOfItem;
            }
        }
        return false; // there is no problem
    }
    orderFreezerShelfHeight = (shelf: PlanogramShelf, item: PlanogramItem, placement: PlacementObject, originalPlacement: PlacementObject) => {
        // create the item array we will return 
        let itemArray: string[] = [];
        // get all items that are in the path of this item (directly or indirectly)
        let tempItem = JSON.parse(JSON.stringify(item));
        // tempItem.placement = placement;
        let itemsInPath = this.getItemsInTheWayUp(shelf, tempItem, itemArray);
        itemsInPath = itemsInPath.filter((v, i, a) => a.indexOf(v) === i);
        // now that we have all the items in the way we canget them and order them by distFromTop
        let itemsToMove: any[] = [];
        for (let i = 0; i < itemsInPath.length; i++) {
            let ind = parseInt(itemsInPath[i].substring(itemsInPath[i].indexOf('I') + 1));
            itemsToMove.push(shelf.items[ind]);
        }
        itemsToMove.sort(function (a: any, b: any) {
            let aNumber = a.placement.distFromStart ? a.placement.distFromTop : 0;
            let bNumber = b.placement.distFromStart ? b.placement.distFromTop : 0;
            let aId = a.Id;
            let bId = b.Id;
            return (aNumber < bNumber) ? 1 : (aNumber > bNumber) ? -1 : ((aId > bId) ? 1 : (aId < bId) ? -1 : 0);
        });
        // if this array has items that have the same distFromTop we'll count only the tallest of them
        let heightArray: any[] = [];
        for (let i = 0; i < itemsToMove.length; i++) {
            let ind: any = null;
            if (itemsToMove[i].placement.distFromTop != null && itemsToMove[i].placement.distFromTop != 0) {
                ind = heightArray.findIndex(line => line.placement.distFromTop === itemsToMove[i].placement.distFromTop);
            } else {
                ind = heightArray.findIndex(line => (line.placement.distFromTop === null || line.placement.distFromTop === 0));
            }
            if (ind < 0) heightArray.push(itemsToMove[i]);
            else {
                let newHeight = itemsToMove[i] && itemsToMove[i].placement && itemsToMove[i].placement.pHeight ? itemsToMove[i].placement.pHeight : ProductDefaultDimensions.height;
                let newToal = newHeight * itemsToMove[i].placement.stack;
                let currHeight = heightArray[ind] && heightArray[ind].placement && heightArray[ind].placement.pHeight ? heightArray[ind].placement.pHeight : ProductDefaultDimensions.height;
                let currTotal = currHeight * heightArray[ind].placement.stack;
                if (newToal > currTotal) heightArray[ind] = itemsToMove[i];
            }
        }
        // now that all items in path are arranged by distFromTop, we can check if there is room for this item to change stack

        if (shelf.freezer === 2 && placement.distFromTop === null) placement.distFromTop = shelf.dimensions.height - (parseInt(JSON.stringify(placement && placement.pHeight ? placement.pHeight : ProductDefaultDimensions.height)) * placement.stack);

        let initTopPoint = (placement.distFromTop ? placement.distFromTop : 0);
        //     'height', parseInt(JSON.stringify(placement && placement.pHeight ? placement.pHeight : ProductDefaultDimensions.height)));
        if (placement.stack - originalPlacement.stack > 0) {
            initTopPoint = (placement.distFromTop ? placement.distFromTop : 0) - ((placement && placement.pHeight ? placement.pHeight : ProductDefaultDimensions.height) * (placement.stack - originalPlacement.stack));
        } else {
            initTopPoint = (placement.distFromTop ? placement.distFromTop : 0) + ((placement && placement.pHeight ? placement.pHeight : ProductDefaultDimensions.height) * (originalPlacement.stack - placement.stack))
        }
        let heightAvail = initTopPoint;
        for (let i = 0; i < heightArray.length; i++) {
            let itemHeight: number = parseInt(JSON.stringify(heightArray[i] && heightArray[i].placement && heightArray[i].placement.pHeight ? heightArray[i].placement.pHeight : ProductDefaultDimensions.height));
            heightAvail -= (itemHeight * heightArray[i].placement.stack);
        }
        // if widthAvail < 0 there is no room to add the new faces
        if (heightAvail < 0) {
            alert("no room to change this item's position");
            return true; // there is a problem
        }
        // if there is room, we may need to move the other items based on itemsToMove
        let moveArray: any[] = [];
        for (let i = 0; i < itemsToMove.length; i++) {
            let itemBottom = (itemsToMove[i].placement.distFromTop ? itemsToMove[i].placement.distFromTop : 0) +
                ((itemsToMove[i] && itemsToMove[i].placement && itemsToMove[i].placement.pHeight ? itemsToMove[i].placement.pHeight : ProductDefaultDimensions.height) * itemsToMove[i].placement.stack);
            let ind = moveArray.findIndex(line => line.itemBottom === itemBottom);
            if (ind < 0) moveArray.push({ itemBottom: itemBottom, items: [itemsToMove[i]] });
            else moveArray[ind].items.push(itemsToMove[i]);
        }
        let initBottom = initTopPoint;
        for (let i = 0; i < moveArray.length; i++) {
            if (moveArray[i].itemBottom > initBottom) {
                let newInitBottom = initBottom;
                for (let x = 0; x < moveArray[i].items.length; x++) {
                    moveArray[i].items[x].placement.distFromTop = initBottom - ((moveArray[i].items[x] && moveArray[i].items[x].placement && moveArray[i].items[x].placement.pHeight ? moveArray[i].items[x].placement.pHeight : ProductDefaultDimensions.height) * moveArray[i].items[x].placement.stack);
                    let newBottom = (moveArray[i].items[x].placement.distFromTop ? moveArray[i].items[x].placement.distFromTop : 0);
                    if (newBottom < newInitBottom) newInitBottom = newBottom;
                }
                initBottom = newInitBottom;
            }
        }
        return false; // there is no problem
    }
    getItemsInTheWayRight = (shelf: PlanogramShelf, item: PlanogramItem, itemArray: string[]) => {
        // create a boxArr to represent the freezer 
        let boxArr = this.createFreezerBoxArr(shelf);

        let startX = item.placement.distFromStart ? item.placement.distFromStart - 1 : 0;
        let itemTotalWidth = startX + ((item.placement.pWidth ? item.placement.pWidth : ProductDefaultDimensions.width) * item.placement.faces) + 2;
        let startY = item.placement.distFromTop ? item.placement.distFromTop : 0;
        let itemTotalHeight = startY + ((item.placement.pHeight ? item.placement.pHeight : ProductDefaultDimensions.height) * item.placement.stack);

        for (let x = itemTotalWidth; x < shelf.dimensions.width; x++) {
            for (let y = startY; y < itemTotalHeight; y++) {
                // if the check bumps into another item return this location
                if (boxArr[x] && boxArr[x][y] && boxArr[x][y].value === 1) {
                    // encountered new item
                    let index = itemArray.findIndex(line => line === boxArr[x][y].id);
                    if (index < 0) {
                        itemArray.push(boxArr[x][y].id);
                        let itemInd = shelf.items.findIndex(line => line.id === boxArr[x][y].id);
                        let newItem = shelf.items[itemInd];
                        itemArray = itemArray.concat(this.getItemsInTheWayRight(shelf, newItem, itemArray));
                    }
                }
            }
        }
        itemArray = itemArray.filter((v, i, a) => a.indexOf(v) === i);
        return itemArray;
    }
    getItemsInTheWayUp = (shelf: PlanogramShelf, item: PlanogramItem, itemArray: string[]) => {
        // create a boxArr to represent the freezer 
        let boxArr = this.createFreezerBoxArr(shelf);

        // check backwards from itemTotalHeight to 0 and for a width of startX to itemTotalWidth
        let startX = item.placement.distFromStart ? item.placement.distFromStart : 0;
        let itemTotalWidth = startX + ((item.placement.pWidth ? item.placement.pWidth : ProductDefaultDimensions.width) * item.placement.faces) - 1;
        let startY = item.placement.distFromTop ? item.placement.distFromTop - 2 : 0;

        for (let x = itemTotalWidth; x >= startX; x--) {
            for (let y = startY; y >= 0; y--) {
                // if the check bumps into another item return this location
                if (boxArr[x] && boxArr[x][y] && boxArr[x][y].value === 1) {
                    // encountered new item
                    let index = itemArray.findIndex(line => line === boxArr[x][y].id);
                    if (index < 0) {
                        itemArray.push(boxArr[x][y].id);
                        let itemInd = shelf.items.findIndex(line => line.id === boxArr[x][y].id);
                        let newItem = shelf.items[itemInd];
                        itemArray = itemArray.concat(this.getItemsInTheWayUp(shelf, newItem, itemArray));
                    }
                }
            }
        }
        itemArray = itemArray.filter((v, i, a) => a.indexOf(v) === i);
        return itemArray;
    }
    updateMultiSelectId = (multi: string[]) => {
        this.setState({ multiSelectId: multi });
    }
    render() {
        const { canDrop, isDragging, connectDropTarget, connectDragSource,
            deleteItem,
            productsMap,
            aisleId,
            store,
            editShelfItemPlacement,
        } = this.props;
        let { shelf } = this.props;
        const { dimensions } = shelf;
        const url = window.location.href;
        const urlElements = url.split('/');
        let PageType = urlElements[5];
        let combinedShelves: PlanogramShelf[] = [shelf];
        const shelfItemsInit = combinedShelves.map(sh => sh.items).reduce((p, c) => p.concat(c));
        let shelfItems = [];
        for (let i = 0; i < shelfItemsInit.length; i++) {
            // make sure there are only real items on the shelf
            if (shelfItemsInit[i]) shelfItems.push(shelfItemsInit[i]);
        }
        const itemDimensions = shelfItems.map((item) => shelfItemDimensions(item.placement, {
            width: item.placement.pWidth ? item.placement.pWidth : ProductDefaultDimensions.width,
            height: item.placement.pHeight ? item.placement.pHeight : ProductDefaultDimensions.height,
            depth: item.placement.pDepth ? item.placement.pDepth : ProductDefaultDimensions.depth
        }));
        const itemsWidth: number = shelfItems.length > 0 ? itemDimensions.map(d => d.width).reduce((p, c) => p + c) : 0;
        let collectedShelfWidth = 0;
        let collectedItemsWidth = 0;
        let collectedMargins = 0;

        let nextMargin = 0;
        const element = (
            <div
                id={"sh_id_" + shelf.id}
                onClick={e => {
                    e.stopPropagation();

                    if (!e.ctrlKey) {
                        e.stopPropagation();
                        this.props.setCurrentSelectedId('');
                        this.props.setMultiSelectId([]);
                        this.props.hideProductDisplayerBarcode();
                    }
                }}
                onDoubleClick={e => {
                    e.stopPropagation();
                    contextMenu.show({
                        id: "pde_" + shelf.id,
                        event: e
                    });
                }}
                style={{
                    height: dimensions.height ? 
                    (shelf.freezer != 1 ?
                        localStorage.printStatus === 'true'  ?  
                         heightDensity(dimensions.height + (dimensions.height * 0.25)) * 1.15 + "px" :
                         heightDensity(dimensions.height + (dimensions.height * 0.25)) + "px"
                          : 
                          localStorage.printStatus === 'true' ?  
                          heightDensity(dimensions.height) * 1.15 + "px"
                          :
                         heightDensity(dimensions.height) + "px") : "initial",
                    width: widthDensity(dimensions.width) + "px",
                    outline: shelf.freezer === 1 ? 'solid 20px #2b998a' : '',
                    backgroundColor: shelf.freezer === 2 ? 'lightblue' : '',
                    marginTop: shelf.freezer === 1 ? '100px' : ''
                }}
                className={(shelf.freezer === 1 ? "planogram-shelf-freezer" : "planogram-shelf") + (canDrop ? " droppable" : "") + (isDragging ? " dragged" : "")}>
                {true ? <div style={{
                    display: true ? "inherit" : "none",
                }}>
                    {combinedShelves.map((shelf, i) => shelf.items.map((item) => {
                        if (item) {
                            const product = productsMap ? productsMap[item.product] : null;
                            const productDimensions = catalogProductDimensionObject(product);
                            const originalPlacement = JSON.parse(JSON.stringify(item.placement));
                            // const itemDimensions = shelfItemDimensions(item.placement, productDimensions);
                            // ITEM PLACMENT MODAL
                            return <Menu id={"ed_" + item.id} key={"ed_" + item.id} className="planogram-context-window" >
                                <ItemPlacementModal
                                    init={item.placement}
                                    title={product ? product.Name : ''}
                                    subtitle={product ? String(product.BarCode) : ''}
                                    barcode={product ? product.BarCode : 0}
                                    dimensions={productDimensions}
                                    maxDimensions={{
                                        ...shelf.dimensions,
                                        //  commenting checking if ther's enough room on the shelf
                                        //width: realWidth - itemsWidth + itemDimensions.width,
                                    }}
                                    onSubmit={async (placement) => {
                                        let problem: any = false;
                                        if (shelf.freezer === 1 || shelf.freezer === 2) {
                                            if (placement.stack != originalPlacement.stack || placement.position != originalPlacement.position) {
                                                problem = await this.orderFreezerShelfHeight(shelf, item, placement, originalPlacement);
                                            }
                                            if (placement.faces != originalPlacement.faces || placement.position != originalPlacement.position) {
                                                problem = await this.orderFreezerShelfWidth(shelf, item, placement, originalPlacement);
                                            }
                                            // check if the total height of the item extends beyond the freezer
                                            if ((placement.pHeight ? placement.pHeight : ProductDefaultDimensions.height) * placement.stack > shelf.dimensions.height)
                                                placement.stack = Math.floor(shelf.dimensions.height / (placement.pHeight ? placement.pHeight : ProductDefaultDimensions.height));
                                            // check if the total width of the item extends beyond the freezer
                                            if ((placement.pWidth ? placement.pWidth : ProductDefaultDimensions.width) * placement.faces > shelf.dimensions.width)
                                                placement.faces = Math.floor(shelf.dimensions.width / (placement.pWidth ? placement.pWidth : ProductDefaultDimensions.width));
                                            //make sure no item extends beyond its wall
                                            let itemEndPoint = (placement.distFromStart ? placement.distFromStart : 0) + ((placement.pWidth ? placement.pWidth : ProductDefaultDimensions.width) * placement.faces);
                                            if (itemEndPoint > shelf.dimensions.width) {
                                                // update the item's distFromStart so it's still in the freezer
                                                placement.distFromStart = shelf.dimensions.width - ((placement.pWidth ? placement.pWidth : ProductDefaultDimensions.width) * placement.faces);
                                            }
                                        }
                                        // in case we changed stack we need to recalculate distFromTop 
                                        if (placement.distFromTop && placement.distFromTop > 0) {
                                            placement.distFromTop = placement.distFromTop - ((placement.pHeight ? placement.pHeight : ProductDefaultDimensions.height) * (placement.stack - originalPlacement.stack));
                                            if (placement.distFromTop < 0) placement.distFromTop = 0;
                                        }
                                        /* Barak 11.1.21 - create turned image im server */
                                        if(placement.position != originalPlacement.position && placement.position != 0){
                                            // console.log('editShelfItemPlacement',product && product.client_image ? product.client_image : item.product);
                                            let clientImage:string = JSON.stringify(item.product);
                                            if(product && product.client_image) clientImage = JSON.stringify(product.client_image);
                                            let res = planogramApi.makeImageVersion(clientImage,placement.position);
                                        }
                                        if (!problem) await this.props.editShelfItemPlacement(item.id, placement);
                                        // get this item's shelfId
                                        let shelfId = item.id.substring(0, item.id.indexOf('I'));
                                        if (this.props.aisleShelves[shelfId] && this.props.aisleShelves[shelfId] != undefined
                                            && this.props.aisleShelves[shelfId].freezer != 1 && this.props.aisleShelves[shelfId].freezer != 2) {
                                            let itemsShelfWidth:any = this.props.aisleShelves[shelfId].items[0] && this.props.aisleShelves[shelfId].items[0].placement ? this.props.aisleShelves[shelfId].items[0].placement.distFromStart : 0
                                            this.props.aisleShelves[shelfId].items.forEach((_item:PlanogramItem) => {
                                                if( item.id == _item.id ) {
                                                    let itemWidth = placement.pWidth ? placement.pWidth * placement.faces : 0
                                                    itemsShelfWidth += itemWidth
                                                } else {
                                                    let itemWidth = _item.placement.pWidth ? _item.placement.pWidth * _item.placement.faces : 0
                                                    itemsShelfWidth += itemWidth
                                                }
                                            });
                                            let distFromStart:any =  ((itemsShelfWidth + this.props.aisleShelves[shelfId].distFromStart) - this.props.aisleShelves[shelfId].dimensions.width) * -1
                                            if( itemsShelfWidth + this.props.aisleShelves[shelfId].distFromStart <  this.props.aisleShelves[shelfId].dimensions.width ) distFromStart = undefined
                                            await this.addShelfItem(this.props.aisleShelves[shelfId], blankItem, distFromStart);
                                     
                                        }
                                        // else this.saveSnapShot(this.props.aisleShelves[shelfId]);
                                        if (this.props.alignToLeft && this.props.aisleShelves[shelfId].freezer != 1 && this.props.aisleShelves[shelfId].freezer != 2) {
                                            await this.alignThisShelfToLeft(this.props.aisleShelves[shelfId]);
                                        }
                                        /********************************************************************************************************/
                                        /* Barak 22.10.20 */ this.saveSnapShot(this.props.aisleShelves[shelfId]);
                                        contextMenu.hideAll();
                                    }} />
                            </Menu>
                        }
                    })
                    )}
                    {combinedShelves.map((shelf, shelf_index) => {
                        const _itemDimensions = shelf.items.map((item) => shelfItemDimensions(item ? item.placement : {
                            faces: 1,
                            stack: 1,
                            row: 1,
                            manual_row_only: 0,
                            position: 0,
                            pWidth: ProductDefaultDimensions.width,
                            pHeight: ProductDefaultDimensions.height,
                            pDepth: ProductDefaultDimensions.depth,
                            distFromStart: 0,
                            /* Barak 26.11.20 */ distFromTop: null
                        }, {
                            width: item && item.placement.pWidth ? item.placement.pWidth : ProductDefaultDimensions.width,
                            height: item && item.placement.pHeight ? item.placement.pHeight : ProductDefaultDimensions.height,
                            depth: item && item.placement.pDepth ? item.placement.pDepth : ProductDefaultDimensions.depth
                        }));
                        /*****************/
                        const itemsWidth: number = _itemDimensions.length > 0 ? _itemDimensions.map(d => d.width).reduce((p, c) => p + c) : 0;
                        const shelfWidth = shelf.dimensions.width;

                        const currentMargin = nextMargin;

                        collectedShelfWidth += shelfWidth;
                        collectedItemsWidth += itemsWidth;

                        if (collectedShelfWidth - collectedItemsWidth - collectedMargins > 0) {
                            nextMargin = collectedShelfWidth - collectedItemsWidth - collectedMargins;
                            collectedMargins += nextMargin;
                        }
                        else
                            nextMargin = 0;
                        return <div
                            className="shelf-container"
                            key={"sh_ed_" + shelf.id}
                            style={{
                                marginLeft: (widthDensity(currentMargin)) + "px",
                            }}>
                                
                        {  this.props.printArray && this.props.printArray.item && this.props.printArray.times &&  localStorage.printStatus == 'true' ? 
                        
                            Array.from(Array(this.props.printArray.times).keys()).map(n=> (
                                <PlanogramShelfItemComponent 
                                            aisleId={aisleId ? aisleId : 0}
                                            netw={''}
                                            userPhone={''}
                                            printItem
                                            printItemObj={this.props.printArray}
                                            item={this.props.printArray.item}
                                            shelf={shelf}
                                            dragImg={this.state.dragImg}
                                            shelfHeight={dimensions.height}
                                            onEndDrag={async (source, target) => {
                                              
                                            }}
                                            verifyDrop={() => { return true }}
                                            onDrop={async (dropItem) => {}}
                                        />
                            )) :null}
                                
                        {/* Barak 29.9.20 * - for testing -  "sh_id_" + shelf.id*/}
                            {shelf.items.sort(function (a: any, b: any) {
                                let aId = a.id ? parseInt(a.id.substring(a.id.indexOf('I') + 1)) : 0;
                                let bId = b.id ? parseInt(b.id.substring(b.id.indexOf('I') + 1)) : 0;
                                return (aId > bId) ? 1 : (aId < bId) ? -1 : 0;
                            }).map((item, i) => {
                             
                                if (item ) {// shelf_index == this.props.printArray.item.shelfLevel-1
                                    const originalPlacement = JSON.parse(JSON.stringify(item.placement));
                                    if( localStorage.printStatus == 'true' &&  this.props.printArray && this.props.printArray.item && item.id === this.props.printArray.item.id ) {return null};
                                    let availableSpace = shelfAvailableSpace(shelf, productsMap);
                                    let shelfHeight = shelf.dimensions.height;
                                    let shelfDepth = shelf.dimensions.depth;
                          
                                    return <MenuProvider
                                        id={"ed_" + item.id}
                                        style={{
                                            zIndex: 15 + 100 * parseInt(shelf.id.substring(shelf.id.indexOf('SH') + 2))
                                        }}
                                        key={item.id}>
                                        <PlanogramShelfItemComponent
                                            aisleId={aisleId ? aisleId : 0}
                                            netw={''}
                                            userPhone={''}
                                            item={item}
                                            shelf={shelf}dragImg={this.state.dragImg}
                                            shelfHeight={dimensions.height}
                                            onEndDrag={async (source:any, target) => {
                                                if (target.type === PlanogramDragDropTypes.TRASH) {
                                                    /* if item.linkedItemUp is not null - DO NOT ALLOW IT TO BE DELETED */
                                                    if (item.linkedItemUp != null) {
                                                        let errorMsg = 'Cannot delete an item if there is another item above it';
                                                        alert(errorMsg);
                                                        return;
                                                    }
                                                    if (item.linkedItemDown != null) {
                                                        // we're deleting the second story item so we need to change the item.linkedItemUp of the original item to null
                                                        let checkSecD = parseInt(item.linkedItemDown.substring(item.linkedItemDown.indexOf('SE') + 2, item.linkedItemDown.indexOf('SH')));
                                                        let checkShD = parseInt(item.linkedItemDown.substring(item.linkedItemDown.indexOf('SH') + 2, item.linkedItemDown.indexOf('I')));
                                                        let aisle: PlanogramAisle = JSON.parse(JSON.stringify(blankPlanogramAisle));
                                                        let aisles = this.props.store ? this.props.store.aisles : undefined;
                                                        let aisleId = item.id.substring(0, item.id.indexOf('SE'));
                                                        if (aisles) {
                                                            for (let i = 0; i < aisles.length; i++) {
                                                                if (aisles[i].id === aisleId) {
                                                                    aisle = aisles[i];
                                                                    break;
                                                                }
                                                            }
                                                        }
                                                        if (aisle) {
                                                            let checkIndD = aisle.sections[checkSecD].shelves[checkShD].items.findIndex(line => line.id === item.linkedItemDown);
                                                            let itemToUpdate = aisle.sections[checkSecD].shelves[checkShD].items[checkIndD];
                                                            if (itemToUpdate) itemToUpdate.linkedItemUp = null;
                                                        }
                                                    }
                                                    if( this.props.multiSelectId && this.props.multiSelectId.length ) {
                                                        this.props.multiSelectId.forEach(async (aisle_id:string)=> {
                                                            await deleteItem(shelf.id, aisle_id);
                                                        })
                                                        this.props.setMultiSelectId([])

                                                    } else await deleteItem(shelf.id, item.id);
                                                    /*  if alignToLeft is true we need to immediately align the entire new shelf to the left */
                                                    if (this.props.alignToLeft)
                                                        await this.alignThisShelfToLeft(this.props.aisleShelves[shelf.id]);
                                                    this.saveSnapShot(shelf);
                                                }
                                            }}
                                            verifyDrop={() => { return true }}
                                            onDrop={ PageType === 'editor' ? 
                                                async (dropItem:any) => {
                                                if( this.props.newPlano && this.props.newPlano.productsMap ) {
                                                dropItem.payload = JSON.parse(JSON.stringify(dropItem.payload));
                                                if (dropItem.type === PlanogramDragDropTypes.SHELF_ITEM
                                                    && (shelf.freezer != 1 && shelf.freezer != 2)
                                                    && parseInt(item.id.substring(item.id.indexOf('I') + 1)) < 100) {
                                                    // sort shelf items by distFromStart so that the last item we see will actually be the last item 
                                                    shelf.items.sort(function (a: any, b: any) {
                                                        let aNumber = a.placement.distFromStart ? a.placement.distFromStart : 0;
                                                        let bNumber = b.placement.distFromStart ? b.placement.distFromStart : 0;
                                                        let aId = a.id ? parseInt(a.id.substring(a.id.indexOf('I') + 1)) : 0;
                                                        let bId = b.id ? parseInt(b.id.substring(b.id.indexOf('I') + 1)) : 0;
                                                        return (aNumber > bNumber) ? 1 : (aNumber < bNumber) ? -1 : ((aId > bId) ? 1 : (aId < bId) ? -1 : 0);
                                                    });
                                                    let lastItem = shelf.items[shelf.items.length - 1];
                                                    let currSHLocal = item.id.substring(0, item.id.indexOf('I'));
                                                    let currSHNew = dropItem.payload.id.substring(0, dropItem.payload.id.indexOf('I'));
                                                    // checking if we both items are on the same shelf
                                                    if (currSHLocal === currSHNew) {
                                                        if (item.id === dropItem.payload.id)
                                                            return;
                                                        /*  switchItems(item.id, dropItem.payload.id); */
                                                        // check if previous item is the same as new item
                                                        let prevItemId = parseInt(item.id.substring(item.id.indexOf('I') + 1)) /*- 1*/;
                                                        if (prevItemId < 0) prevItemId = 0;
                                                        let dropItemId = parseInt(dropItem.payload.id.substring(dropItem.payload.id.indexOf('I') + 1));
                                                        if (dropItemId === prevItemId) {
                                                        }
                                                        else {
                                                            if (this.props.aisleShelves[shelf.id].items[prevItemId].product === dropItem.payload.product) {
                                                                // same item - need to update faces 
                                                                if (dropItem.payload.placement && dropItem.payload.placement.faces)
                                                                    this.props.aisleShelves[shelf.id].items[prevItemId].placement.faces += dropItem.payload.placement.faces;
                                                                else this.props.aisleShelves[shelf.id].items[prevItemId].placement.faces += 1;
                                                                // since we increased faces, we now need to remove the original dropped item from the shelf
                                                                await deleteItem(shelf.id, dropItem.payload.id);
                                                            } else {
                                                             
                                                                let distFromStartN = 0;
                                                                if (dropItem.distFromStart > 0)
                                                                    distFromStartN = dropItem.distFromStart;
                                                                // else distFromStartN = item.placement && item.placement.distFromStart ? item.placement.distFromStart - 1 : 0;
                                                                else distFromStartN = item.placement && item.placement.distFromStart
                                                                    // place new item right to the right of this item
                                                                    ? (item.placement.distFromStart + ((item.placement.pWidth ? item.placement.pWidth : ProductDefaultDimensions.width) * item.placement.faces) - 1)
                                                                    : 0;
                                                                if (distFromStartN < 0) distFromStartN = 0;
                                                                let shelfId = shelf.id;
                                                                let continueLoop = true;
                                                                while (distFromStartN > this.props.aisleShelves[shelfId].dimensions.width && continueLoop) {
                                                                    let secIndex: number = parseInt(shelf.id.substring(shelf.id.indexOf('SE') + 2)) + 1;
                                                                    shelfId = shelf.id.substring(0, shelf.id.indexOf('SE') + 2) + secIndex + 'SH' + shelf.id.substring(shelf.id.indexOf('SH') + 2);
                                                                    if (this.props.aisleShelves[shelfId]) {
                                                                        distFromStartN = distFromStartN - this.props.aisleShelves[shelfId].dimensions.width;
                                                                    } else {
                                                                        shelfId = shelf.id;
                                                                        continueLoop = false;
                                                                    }
                                                                }
                                                                await deleteItem(shelf.id, dropItem.payload.id);
                                                                let movedItem = JSON.parse(JSON.stringify(dropItem.payload));
                                                                movedItem.placement.distFromStart = distFromStartN 

                                                                if (this.props.aisleShelves[shelfId] && this.props.aisleShelves[shelfId] != undefined)
                                                                    await this.addShelfItem(this.props.aisleShelves[shelfId], movedItem);
                                                                  }
                                                            if (this.props.alignToLeft) await this.alignThisShelfToLeft(this.props.aisleShelves[shelf.id]);
                                                            this.saveSnapShot(this.props.aisleShelves[shelf.id]);
                                                        }
                                                    }
                                                    else {
                                                        if (item.product === dropItem.payload.product) {
                                                            if (dropItem.payload.placement)
                                                                item.placement.faces += dropItem.payload.placement.faces;
                                                            else
                                                                item.placement.faces += 1;
                                                            await deleteItem(currSHNew, dropItem.payload.id);
                                                            editShelfItemPlacement(item.id, item.placement);
                                                            
                                                            if (this.props.aisleShelves[shelf.id] && this.props.aisleShelves[shelf.id] != undefined)
                                                                await this.addShelfItem(this.props.aisleShelves[shelf.id], blankItem);
                                                            this.saveSnapShot(this.props.aisleShelves[shelf.id]);
                                                        }
                                                        else {
                                                            // before adding it to the shelf in a place BEFORE this item, check if the previous item is the same product 
                                                            // if it is - we just need to update the faces
                                                            let prevItemId = parseInt(item.id.substring(item.id.indexOf('I') + 1)) - 1;
                                                            if (prevItemId < 0) prevItemId = 0;
                                                            if (this.props.aisleShelves[shelf.id].items[prevItemId].product === dropItem.payload.product) {
                                                                // same item - need to update faces 
                                                                if (dropItem.payload.placement && dropItem.payload.placement.faces)
                                                                    this.props.aisleShelves[shelf.id].items[prevItemId].placement.faces += dropItem.payload.placement.faces;
                                                                else this.props.aisleShelves[shelf.id].items[prevItemId].placement.faces += 1;
                                                            } else {
                                                                if (dropItem.distFromStart > 0)
                                                                    dropItem.payload.placement.distFromStart = dropItem.distFromStart;
                                                                // else dropItem.payload.placement.distFromStart = item.placement && item.placement.distFromStart ? item.placement.distFromStart - 1 : 0;
                                                                else dropItem.payload.placement.distFromStart = item.placement && item.placement.distFromStart
                                                                    // place new item right to the right of this item
                                                                    ? (item.placement.distFromStart + ((item.placement.pWidth ? item.placement.pWidth : ProductDefaultDimensions.width) * item.placement.faces) - 1)
                                                                    : 0;
                                                                if (dropItem.payload.placement.distFromStart > this.props.aisleShelves[currSHNew].dimensions.width) {
                                                                    let secIndex: number = parseInt(currSHNew.substring(currSHNew.indexOf('SE') + 2)) + 1;
                                                                    currSHNew = currSHNew.substring(0, currSHNew.indexOf('SE') + 2) + secIndex + 'SH' + currSHNew.substring(currSHNew.indexOf('SH') + 2);
                                                                    if (this.props.aisleShelves[currSHNew]) {
                                                                        dropItem.payload.placement.distFromStart = dropItem.payload.placement.distFromStart - this.props.aisleShelves[currSHNew].dimensions.width;
                                                                    } else currSHNew = dropItem.payload.id.substring(0, dropItem.payload.id.indexOf('I'));;
                                                                }
                                                                if (dropItem.payload.placement.distFromStart < 0) dropItem.payload.placement.distFromStart = 0;

                                                                await deleteItem(currSHNew, dropItem.payload.id);
                                                                // await addProduct(shelf.id, JSON.parse(JSON.stringify(dropItem.payload.product)), JSON.parse(JSON.stringify(dropItem.payload.placement)));
                                                                const shelfDepth = shelf.dimensions.depth;
                                                                const catalogProductDepth = this.props.newPlano.productsMap[dropItem.payload.product] && this.props.newPlano.productsMap[dropItem.payload.product].length
                                                                    ? this.props.newPlano.productsMap[dropItem.payload.product].length : null
                                                                let newItem: PlanogramItem = {
                                                                    id: '',
                                                                    placement: {
                                                                        faces: dropItem.payload.placement && dropItem.payload.placement.faces ? dropItem.payload.placement.faces : 1,
                                                                        stack: dropItem.payload.placement && dropItem.payload.placement.stack ? dropItem.payload.placement.stack : 1,
                                                                        row: dropItem.payload.placement && dropItem.payload.placement.row ? dropItem.payload.placement.row : Math.floor(shelfDepth / (catalogProductDepth ? catalogProductDepth : 1)),
                                                                        manual_row_only: dropItem.payload.placement && dropItem.payload.placement.manual_row_only ? dropItem.payload.placement.manual_row_only : 0,
                                                                        position: dropItem.payload.placement && dropItem.payload.placement.position ? dropItem.payload.placement.position : 0,
                                                                        pWidth: this.props.newPlano.productsMap[dropItem.payload.product].width != undefined ? this.props.newPlano.productsMap[dropItem.payload.product].width : ProductDefaultDimensions.width,
                                                                        pHeight: this.props.newPlano.productsMap[dropItem.payload.product].height != undefined ? this.props.newPlano.productsMap[dropItem.payload.product].height : ProductDefaultDimensions.height,
                                                                        pDepth: this.props.newPlano.productsMap[dropItem.payload.product].length != undefined ? this.props.newPlano.productsMap[dropItem.payload.product].length : ProductDefaultDimensions.depth,
                                                                        // distFromStart: dropItem.payload.placement && dropItem.payload.placement.distFromStart ? dropItem.payload.placement.distFromStart : 0,
                                                                        distFromStart: item.placement && item.placement.distFromStart
                                                                            // place new item right to the right of this item
                                                                            ? (item.placement.distFromStart + ((item.placement.pWidth ? item.placement.pWidth : ProductDefaultDimensions.width) * item.placement.faces) - 1)
                                                                            : (dropItem.payload.placement && dropItem.payload.placement.distFromStart ? dropItem.payload.placement.distFromStart : 0),
                                                                        distFromTop: null
                                                                    },
                                                                    product: dropItem.payload.product,
                                                                    lowSales: dropItem.payload.lowSales ? dropItem.payload.lowSales : false,
                                                                    missing: dropItem.payload.missing ? dropItem.payload.missing : 0,
                                                                    branch_id: dropItem.payload.branch_id ? dropItem.payload.branch_id : 0,
                                                                    /* Barak 21.12.20 - Add linking item option */
                                                                    linkedItemUp: dropItem.payload.linkedItemUp,
                                                                    linkedItemDown: dropItem.payload.linkedItemDown,
                                                                    shelfLevel: dropItem.payload.shelfLevel ? dropItem.payload.shelfLevel : 1,
                                                                };
                                                                if (this.props.aisleShelves[shelf.id] && this.props.aisleShelves[shelf.id] != undefined)
                                                                    await this.addShelfItem(this.props.aisleShelves[shelf.id], newItem);
                                                             
                                                            }
                                                        }
                                                        // if alignToLeft is true we need to immediately align the entire new shelf to the left
                                                        if (this.props.alignToLeft) await this.alignThisShelfToLeft(this.props.aisleShelves[shelf.id]);
                                                        this.saveSnapShot(this.props.aisleShelves[shelf.id]);
                                                    }
                                                }
                                                if (dropItem.type === PlanogramDragDropTypes.SERIES_SIDEBAR) {
                                                            const catalogProductDepth = this.props.newPlano.productsMap[dropItem.payload.product] && this.props.newPlano.productsMap[dropItem.payload.product].length
                                                            let series = dropItem.payload
                                                            let newItems =  series.map((p:any, i:number)=> {
                                                                if( p.faces > 0) {
                                                                const product:any = productsMap[p.BarCode];
                                                                return {...
                                                                blankItem, product: product.BarCode ,placement: 
                                                                  {...blankItem.placement,
                                                                    faces: p.faces,
                                                                    stack: product.placement && product.placement.stack ? product.placement.stack : 1,
                                                                    row: product.placement && product.placement.row ? product.placement.row : Math.floor(shelfDepth / (catalogProductDepth ? catalogProductDepth : 1)),
                                                                    manual_row_only: product.placement && product.placement.manual_row_only ? product.placement.manual_row_only : 0,
                                                                    position: product.placement && product.placement.position ? product.placement.position : 0,
                                                                    pWidth: this.props.newPlano.productsMap[product.BarCode].width != undefined ? this.props.newPlano.productsMap[product.BarCode].width : ProductDefaultDimensions.width,
                                                                    pHeight: this.props.newPlano.productsMap[product.BarCode].height != undefined ? this.props.newPlano.productsMap[product.BarCode].height : ProductDefaultDimensions.height,
                                                                    pDepth: this.props.newPlano.productsMap[product.BarCode].length != undefined ? this.props.newPlano.productsMap[product.BarCode].length : ProductDefaultDimensions.depth,
                                                                    distFromStart: item.placement && item.placement.distFromStart
                                                                      ? series[i-1] && series[i-1].placement && series[i-1].placement.distFromStart ? ((item.placement.distFromStart + ((series[i-1].placement.pWidth ? series[i-1].placement.pWidth : ProductDefaultDimensions.width) * series[i-1].placement.faces) - 1)
                                                                           + ((item.placement.pWidth ? item.placement.pWidth : ProductDefaultDimensions.width) * item.placement.faces) - 1)
                                                                      : (item.placement.distFromStart + ((item.placement.pWidth ? item.placement.pWidth : ProductDefaultDimensions.width) * item.placement.faces) - 1)
                                                                      : (product.placement && product.placement.distFromStart ? product.placement.distFromStart : 0),
                                                                  distFromTop: null
                                                                }}} })
                                                            newItems.forEach(async (item:any)=> {
                                                                await this.addShelfItem(this.props.aisleShelves[shelf.id], item, 0);
                                                            })
                                                            // await this.addShelfItem(this.props.aisleShelves[shelf.id], item, 0, newItems);
                                                            await this.saveSnapShot(shelf);
                                                }
                                                if (dropItem.type === PlanogramDragDropTypes.PRODUCT_SIDEBAR
                                                    && (shelf.freezer != 1 && shelf.freezer != 2)
                                                    && parseInt(item.id.substring(item.id.indexOf('I') + 1)) < 100) {
                                                    let productBarcode: number = dropItem.payload;
                                                    const product = productsMap[productBarcode];
                                                    const productDimensions: DimensionObject = catalogProductDimensionObject(product);

                                                    if (!product.width || !product.height || !product.length || !validateDimensions(productDimensions)) {
                                                        const shelfDepth = shelf.dimensions.depth;
                                                        const shelfHeight = shelf.dimensions.height;
                                                        
                                                        let distFromStartN = 0;
                                                        if (dropItem.distFromStart > 0)
                                                            distFromStartN = dropItem.distFromStart;
                                                        let distFromTopN = 0;
                                                        if (dropItem.distFromTop > 0)
                                                            distFromTopN = dropItem.distFromTop;
                                                        // else distFromStartN = item.placement && item.placement.distFromStart ? item.placement.distFromStart - 1 : 0;
                                                        else distFromStartN = item.placement && item.placement.distFromStart
                                                            // place new item right to the right of this item
                                                            ? (item.placement.distFromStart + ((item.placement.pWidth ? item.placement.pWidth : ProductDefaultDimensions.width) * item.placement.faces) - 1)
                                                            : 0;
                                                        if (distFromStartN < 0) distFromStartN = 0;
                                                        let shelfId = shelf.id;
                                                        let continueLoop = true;
                                                        while (distFromStartN > this.props.aisleShelves[shelfId].dimensions.width && continueLoop) {
                                                            let secIndex: number = parseInt(shelf.id.substring(shelf.id.indexOf('SE') + 2)) + 1;
                                                            shelfId = shelf.id.substring(0, shelf.id.indexOf('SE') + 2) + secIndex + 'SH' + shelf.id.substring(shelf.id.indexOf('SH') + 2);
                                                            if (this.props.aisleShelves[shelfId]) {
                                                                distFromStartN = distFromStartN - this.props.aisleShelves[shelfId].dimensions.width;
                                                            } else {
                                                                shelfId = shelf.id;
                                                                continueLoop = false;
                                                            }
                                                        }
                                                        setModal(() => <DimensionModal
                                                            init={ProductDefaultDimensions}
                                                            title={"Section Dimensions"}
                                                            onSubmit={async (dimensions) => {
                                                                await planogramApi.updateProductDimensions(productBarcode, dimensions).then(async (res) => {
                                                                    await this.props.updateProductDimensions(
                                                                        productBarcode,
                                                                        dimensions,
                                                                        await addProductAction(shelf.id, productBarcode,
                                                                            {
                                                                                faces: dropItem.faces,
                                                                                stack: Math.floor(shelfHeight / dimensions.height),
                                                                                row: Math.floor(shelfDepth / dimensions.depth),
                                                                                manual_row_only: 0,
                                                                                position: 0,
                                                                                pWidth: dimensions.width,
                                                                                pHeight: dimensions.height,
                                                                                pDepth: dimensions.depth,
                                                                                distFromStart: distFromStartN,
                                                                                distFromTop: distFromTopN,
                                                                            }
                                                                        )
                                                                    );
                                                                    await this.saveSnapShot(shelf);
                                                                    toggleModal();
                                                                    if (this.props.alignToLeft) await this.alignThisShelfToLeft(this.props.aisleShelves[shelf.id]);
                                                                }).catch((err) => {
                                                                    console.error(err);
                                                                })
                                                            }}
                                                        />)
                                                    }
                                                    else {
                                                        if (item.product === dropItem.payload) {
                                                            editShelfItemPlacement(item.id, item.placement);
                                                            if (this.props.aisleShelves[shelf.id] && this.props.aisleShelves[shelf.id] != undefined)
                                                                await this.addShelfItem(this.props.aisleShelves[shelf.id], blankItem);
                                                            this.saveSnapShot(this.props.aisleShelves[shelf.id]);
                                                        }
                                                        let distFromStartN = 0;
                                                        if (dropItem.distFromStart > 0)
                                                            distFromStartN = dropItem.distFromStart;
                                                        // else distFromStartN = item.placement && item.placement.distFromStart ? item.placement.distFromStart - 1 : 0;
                                                        else distFromStartN = item.placement && item.placement.distFromStart
                                                            // place new item right to the right of this item
                                                            ? (item.placement.distFromStart + ((item.placement.pWidth ? item.placement.pWidth : ProductDefaultDimensions.width) * item.placement.faces) - 1)
                                                            : 0;
                                                        if (distFromStartN < 0) distFromStartN = 0;
                                                        let distFromTopN = null;
                                                        let shelfId = shelf.id;
                                                        let continueLoop = true;
                                                        while (distFromStartN > this.props.aisleShelves[shelfId].dimensions.width && continueLoop) {
                                                            let secIndex: number = parseInt(shelf.id.substring(shelf.id.indexOf('SE') + 2)) + 1;
                                                            shelfId = shelf.id.substring(0, shelf.id.indexOf('SE') + 2) + secIndex + 'SH' + shelf.id.substring(shelf.id.indexOf('SH') + 2);
                                                            if (this.props.aisleShelves[shelfId]) {
                                                                distFromStartN = distFromStartN - this.props.aisleShelves[shelfId].dimensions.width;
                                                            } else {
                                                                shelfId = shelf.id;
                                                                continueLoop = false;
                                                            }
                                                        }
                                                        const shelfDepth = shelf.dimensions.depth;
                                                        const catalogProductDepth = this.props.newPlano.productsMap[productBarcode] && this.props.newPlano.productsMap[productBarcode].length
                                                            ? this.props.newPlano.productsMap[productBarcode].length : null
                                                        let newItem: PlanogramItem = {
                                                            id: '',
                                                            placement: {
                                                                faces: dropItem.faces,
                                                                stack: 1,
                                                                row: Math.floor(shelfDepth / (catalogProductDepth ? catalogProductDepth : 1)),
                                                                manual_row_only: 0,
                                                                position: 0,
                                                                pWidth: this.props.newPlano.productsMap[productBarcode].width != undefined ? this.props.newPlano.productsMap[productBarcode].width : ProductDefaultDimensions.width,
                                                                pHeight: this.props.newPlano.productsMap[productBarcode].height != undefined ? this.props.newPlano.productsMap[productBarcode].height : ProductDefaultDimensions.height,
                                                                pDepth: this.props.newPlano.productsMap[productBarcode].length != undefined ? this.props.newPlano.productsMap[productBarcode].length : ProductDefaultDimensions.depth,
                                                                distFromStart: distFromStartN ? distFromStartN : 0,
                                                                distFromTop: distFromTopN ? distFromTopN : null,
                                                            },
                                                            product: productBarcode,
                                                            lowSales: false,
                                                            missing: 0,
                                                            branch_id: 0,
                                                            linkedItemUp: null,
                                                            linkedItemDown: null,
                                                            shelfLevel: 1,
                                                        };
                                                        if (this.props.aisleShelves[shelfId] && this.props.aisleShelves[shelfId] != undefined)
                                                            await this.addShelfItem(this.props.aisleShelves[shelfId], newItem);
                                                        if (this.props.alignToLeft) await this.alignThisShelfToLeft(this.props.aisleShelves[shelfId]);
                                                            this.saveSnapShot(this.props.aisleShelves[shelf.id]);
                                                    }
                                                }
                                            }

                                            }
                                        :()=>{}}
                                        />
                                    </MenuProvider>
                                }
                            }
                            )}</div>
                    })}
                </div> : null}
                {dimensions ? <div className="shelf-dimensions" style={{
                    zIndex: 50 + 100 * parseInt(shelf.id.substring(shelf.id.indexOf('SH') + 2)),
                }}>
                    {dimensionText(dimensions)}
                </div> : ""}
                <div className="shelf-structure" style={{
                    zIndex: 10 + 100 * parseInt(shelf.id.substring(shelf.id.indexOf('SH') + 2)),
                }}></div>
            </div>
        )

        return connectDragSource && connectDropTarget ? connectDragSource(connectDropTarget(element)) : element;
    }
}
export const PlanogramShelfComponent = connect(mapStateToProps, mapDispatchToProps)(PlanogramShelfComponentContainer);
export const PlanogramShelfDnd = DraggableSource(DroppableTarget(PlanogramShelfComponent));
