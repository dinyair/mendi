import * as React from "react";
import { DropTarget, DragElementWrapper, DragSource } from 'react-dnd';
import { Action, AnyAction } from "redux";
import { connect } from "react-redux";
import { ThunkDispatch } from "redux-thunk";
import { formatMessage } from 'devextreme/localization';
import { FormattedMessage } from "react-intl"
import { PlanogramDragDropTypes } from "../generic/DragAndDropType";
import { PlanogramShelfDnd } from "./PlanogramShelfComponent";
import { DragDropResultBase } from "../generic/DragDropWrapper";
import { DimensionObject, PlanogramSection, PlanogramShelf, PlanogramItem, PlanogramElementId, PlacementObject, PlanogramAisle, PlanogramStore, PlanogramStore } from "@src/planogram/shared/store/planogram/planogram.types";
import { AppState } from "@src/planogram/shared/store/app.reducer";
import { deleteShelfAction, switchShelvesAction, editShelfDimensionsAction } from "@src/planogram/shared/store/planogram/store/shelf/shelf.actions";
import { toggleModal, setModal } from "@src/planogram/shared/components/Modal";
import { addProductAction,  editShelfItemAction, deleteItemAction } from "@src/planogram/shared/store/planogram/store/item/item.actions";
import { widthDensity, heightDensity, shelfItemDimensions, shelfAvailableSpace, calculateShelvesHeight } from "../provider/calculation.service";
import { dimensionText, validateDimensions, catalogProductDimensionObject } from "@src/planogram/provider/planogram.service";
import { DimensionModal } from "../modals/DimensionModal";
import { ProductDefaultDimensions,  blankPlanogramAisle, blankItem } from "@src/planogram/shared/store/planogram/planogram.defaults";
import * as planogramApi from '@src/planogram/shared/api/planogram.provider';
import { editProductDimensions } from "@src/planogram/shared/store/catalog/catalog.action";
import { setMultiSelectId, setCurrentSelectedId, hideProductDetailer, setAisleSaveArray, setAisleSaveIndex } from "@src/planogram/shared/store/planogram/planogram.actions";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAlignJustify, faAlignLeft, faAlignRight, faArrowDown, faArrowLeft, faArrowRight, faArrowUp, faEllipsisH } from "@fortawesome/free-solid-svg-icons";
import { setAisleAction } from "@src/planogram/shared/store/planogram/store/aisle/aisle.actions";
import * as planogramProvider from "@src/planogram/shared/api/planogram.provider";
import { Menu, contextMenu, MenuProvider } from "@src/planogram/generic/ContextMenu";
import { alertReducer } from "../shared/store/newPlano/newPlano.reducer";

interface SourceCollectProps {
    connectDragSource: DragElementWrapper<any>,
    isDragging: boolean,
}

interface TargetCollectProps {
    connectDropTarget: DragElementWrapper<any>,
    canDrop: boolean,
    isOver: boolean,
    isOverCurrent: boolean
}

export interface PlanogramSectionComponentProps {
    sections:any;
    storeId: number,
    aisleId: number,
    section: PlanogramSection,
    index: number,
    sectionBorders: string,
    onDrop?: (item: DragDropResultBase) => void,
    onDragEnd?: (item: DragDropResultBase, targetItem: DragDropResultBase) => void,
    verifyDrop: (droppedItem: DragDropResultBase) => boolean,
}

const DraggableSource = DragSource<PlanogramSectionComponentProps, SourceCollectProps>(
    PlanogramDragDropTypes.SECTION, {
    beginDrag(props) {
        const { section } = props;
        return ({
            type: PlanogramDragDropTypes.SECTION,
            payload: section,
        });
    },
    endDrag(props, monitor) {
        if (!monitor.didDrop())
            return;
        if (props.onDragEnd)
            props.onDragEnd(monitor.getItem(), monitor.getDropResult());
    },
    // canDrag(props, monitor) {
    //     return props.section.shelves == null || props.section.shelves.length === 0
    // }
}, (connect, monitor) => ({
    canDrag: monitor.canDrag(),
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging(),
}));

const dropTaretTypes = [
    PlanogramDragDropTypes.SECTION,
    PlanogramDragDropTypes.SECTION_SIDEBAR,
    PlanogramDragDropTypes.SHELF_SIDEBAR,
    PlanogramDragDropTypes.SHELF,
    PlanogramDragDropTypes.SHELF_ITEM,
    PlanogramDragDropTypes.PRODUCT_SIDEBAR,
    PlanogramDragDropTypes.SERIES_SIDEBAR,
    PlanogramDragDropTypes.PRODUCT,
    PlanogramDragDropTypes.SHELF_SIDEBAR_FREEZER,
];

const DroppableTarget = DropTarget<PlanogramSectionComponentProps, TargetCollectProps>(
    dropTaretTypes, {
    drop(props, monitor) {
        const { section } = props;
        if (props.onDrop)
            props.onDrop(monitor.getItem());
        return ({
            type: dropTaretTypes,
            payload: section,
            distFromStart: 'section'
        });
    },
    canDrop(props, monitor) {
        if (monitor.didDrop() || !monitor.isOver({ shallow: true }))
            return false;
        return props.verifyDrop(monitor.getItem());
    }
}, (connect, monitor) => ({
    connectDropTarget: connect.dropTarget(),
    canDrop: monitor.canDrop(),
    isOver: monitor.isOver(),
    isOverCurrent: monitor.isOver({ shallow: true }),
}));

type SectionProps = PlanogramSectionComponentProps & TargetCollectProps & SourceCollectProps;

const mapDispatchToProps = (dispatch: ThunkDispatch<AppState, {}, AnyAction>) => {
    return {
        setCurrentSelectedId: (currentSelectedId: string) => dispatch(setCurrentSelectedId(currentSelectedId)),

        deleteShelf: (section: PlanogramSection, shelf: PlanogramElementId) => {
            dispatch(deleteShelfAction(section, shelf));
        },
        switchShelves: (base: PlanogramElementId, remote: PlanogramElementId) => {
            dispatch(switchShelvesAction(base, remote))
        },
        editShelfDimensions: (shelf: PlanogramElementId, dimensions: DimensionObject) => {
            dispatch(editShelfDimensionsAction(shelf, dimensions));
        },
        updateProductDimensions: (barcode: number, dimensions: DimensionObject, afterAction: Action) => {
            dispatch(editProductDimensions(barcode, dimensions));
            dispatch(afterAction);
        },
        setMultiSelectId: (multiSelectId: string[]) => dispatch(setMultiSelectId(multiSelectId)),
        hideProductDisplayerBarcode: () => {
            dispatch(hideProductDetailer());
        },
        editShelfItemPlacement: (item: PlanogramElementId, placement: PlacementObject) => {
            dispatch(editShelfItemAction(item, placement));
        },
        setStoreAisle: (aisle: PlanogramAisle, aislePid?: PlanogramElementId) => {
            dispatch(setAisleAction(aisle, aislePid));
        },
        deleteItem: (shelf: PlanogramElementId, item: PlanogramElementId) => {
            dispatch(deleteItemAction(shelf, item))
        },
        setAisleSaveArray: (aisleSaveArray: PlanogramAisle[]) => {
            dispatch(setAisleSaveArray(aisleSaveArray));
        },
        setAisleSaveIndex: (aisleSaveIndex: number) => {
            dispatch(setAisleSaveIndex(aisleSaveIndex));
        },
    }
}
const mapStateToProps = (state: AppState, ownProps: SectionProps) => {
    return {
        ...state.newPlano,
        ...ownProps,
        virtualStore: state.planogram.virtualStore,
        multiSelectId: state.planogram.display.multiSelectId,
        store: state.planogram.store,
        shelvesDetails: state.planogram.virtualStore.shelfDetails,
        aisleShelves: state.planogram.virtualStore.shelfMap,
        alignToLeft: state.planogram.display.alignToLeft,
        newPlano: state.newPlano,
        aisleSaveArray: state.planogram.display.aisleSaveArray,
        aisleSaveIndex: state.planogram.display.aisleSaveIndex,
        layouts: state.system.data.layouts,
    }
}
type SectionStoreProps = ReturnType<typeof mapDispatchToProps> & ReturnType<typeof mapStateToProps>;
const StoreConnector = connect(mapStateToProps, mapDispatchToProps);



function getItemsShelfBefore (sections:any, index:number, shelf_index:number):any {
    if( sections[index-1] ) {
        if(sections[index-1].shelves[shelf_index] && sections[index-1].shelves[shelf_index].items && sections[index-1].shelves[shelf_index].items.length ){
            return sections[index-1].shelves[shelf_index].items 
        } else return getItemsShelfBefore(sections, index-1, shelf_index)
    } else return null
}   


class PlanogramSectionComponentTarget extends React.Component<SectionStoreProps> {
    alignThisShelfToLeft = async (shelf: PlanogramShelf) => {
        const { store, aisleShelves, shelvesDetails } = this.props;

        // check if we need to align multiple shelves
        let all_shelves_to_align: PlanogramShelf[] = [];
        if (this.props.multiSelectId.length > 1) {
            // sort multiSelectId by shelf and section
            this.props.multiSelectId.sort(function (a: any, b: any) {
                let currSE_A: number = parseInt(a.substring(a.indexOf('SE') + 2, a.indexOf('SH')));
                let currSH_A: number = parseInt(a.substring(a.indexOf('SH') + 2, a.indexOf('I')));
                let currSE_B: number = parseInt(b.substring(b.indexOf('SE') + 2, b.indexOf('SH')));
                let currSH_B: number = parseInt(b.substring(b.indexOf('SH') + 2, b.indexOf('I')));
                // if it's the same shelf order by section    
                if (currSH_A === currSH_B)
                    return (currSE_A > currSE_B) ? 1 : (currSE_A < currSE_B) ? -1 : 0;
                else return (currSH_A > currSH_B) ? 1 : (currSH_A < currSH_B) ? -1 : 0;
            });
            // find each shelf represented in multiSelectId and add them to all_shelves_to_align
            for (let a = 0; a < this.props.multiSelectId.length; a++) {
                let target_A = this.props.multiSelectId[a].substring(0, this.props.multiSelectId[a].indexOf('SE'));
                let target_SE = parseInt(this.props.multiSelectId[a].substring(this.props.multiSelectId[a].indexOf('SE') + 2, this.props.multiSelectId[a].indexOf('SH')));
                let target_SH = parseInt(this.props.multiSelectId[a].substring(this.props.multiSelectId[a].indexOf('SH') + 2));
                if (store) {
                    for (let i = 0; i < store.aisles.length; i++) {
                        if (store.aisles[i].id === target_A) {
                            let thisShelf = store.aisles[i].sections[target_SE].shelves[target_SH];
                            let index = all_shelves_to_align.findIndex(shelf => shelf.id === thisShelf.id);
                            if (index < 0 && thisShelf.freezer != 1 && thisShelf.freezer != 2) all_shelves_to_align.push(thisShelf);
                            // now that we've added the shelf the item is actually on, we need to check if the item covers more than one shelf.
                            // since we know this is the shelf the item starts in we can get the item's details from it.
                            let item = thisShelf.items.filter(line => line.id === this.props.multiSelectId[a]);
                            let itemTotalWidth = (item[0].placement.distFromStart ? item[0].placement.distFromStart : 0) + ((item[0].placement.pWidth ? item[0].placement.pWidth : ProductDefaultDimensions.width) * item[0].placement.faces);
                            let shelfWidth = thisShelf.dimensions.width;
                            if (itemTotalWidth > shelfWidth) {
                                while (itemTotalWidth > shelfWidth) {
                                    target_SE++;
                                    let nextShelf = store.aisles[i].sections[target_SE].shelves[target_SH];
                                    let index = all_shelves_to_align.findIndex(shelf => shelf.id === nextShelf.id);
                                    if (index < 0 && nextShelf.freezer != 1 && nextShelf.freezer != 2) all_shelves_to_align.push(nextShelf);
                                    itemTotalWidth -= shelfWidth;
                                    shelfWidth = nextShelf.dimensions.width;
                                }
                            }
                        }
                    }
                }
            }
        } else if (shelf.freezer != 1 && shelf.freezer != 2) all_shelves_to_align.push(shelf);
        console.log('all_shelves_to_align', all_shelves_to_align);

        // now that we have all shelvs that need to be aligned we can align them one after the other
        for (let aa = 0; aa < all_shelves_to_align.length; aa++) {
            // find latest version of this shelf in the store aisle
            let thisShelf = all_shelves_to_align[aa];
            let target_A = all_shelves_to_align[aa].id.substring(0, all_shelves_to_align[aa].id.indexOf('SE'));
            let target_SE = parseInt(all_shelves_to_align[aa].id.substring(all_shelves_to_align[aa].id.indexOf('SE') + 2, all_shelves_to_align[aa].id.indexOf('SH')));
            let target_SH = parseInt(all_shelves_to_align[aa].id.substring(all_shelves_to_align[aa].id.indexOf('SH') + 2));
            if (store) {
                for (let i = 0; i < store.aisles.length; i++) {
                    if (store.aisles[i].id === target_A) {
                        thisShelf = store.aisles[i].sections[target_SE].shelves[target_SH];
                    }
                }
            }
            console.log('thisShelf', thisShelf);

            // get this shelf's combined shelves and only leave in the array shelves that exist in all_shelves_to_align
            let shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[all_shelves_to_align[aa].id]));
            while (shelfDetails.main_shelf) {
                let ids = JSON.parse(JSON.stringify(shelfDetails.main_shelf));
                shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[ids]));
                thisShelf = aisleShelves[ids];
            }
            let { combined } = shelfDetails;

            let combinedShelves = [thisShelf].concat(combined.map((id: any) => aisleShelves[id]));
            console.log('initial combinedShelves', combinedShelves);

            // remove from combinedShelves any shelves that do not appear in all_shelves_to_align
            let newCombinedShelves = [];
            for (let i = 0; i < combinedShelves.length; i++) {
                let index = all_shelves_to_align.findIndex(shelf => shelf.id === combinedShelves[i].id);
                if (index >= 0) newCombinedShelves.push(combinedShelves[i]);
            }
            combinedShelves = newCombinedShelves;
            console.log('newCombinedShelves', newCombinedShelves);

            let A_Id = combinedShelves[0].id.substring(0, combinedShelves[0].id.indexOf('SE'));
            let SE_Index = parseInt(combinedShelves[0].id.substring(combinedShelves[0].id.indexOf('SE') + 2, combinedShelves[0].id.indexOf('SH')));
            let SH_Index = parseInt(combinedShelves[0].id.substring(combinedShelves[0].id.indexOf('SH') + 2));

            // check if this aisle has a shelf in the same height, in previous section
            let firstDistFromStart = 0;
            if (store) {
                let store_aisle: PlanogramAisle = store.aisles.filter(aisle => aisle.id === A_Id)[0];
                console.log('store_aisle', store_aisle);
                if (store_aisle && store_aisle.sections[SE_Index - 1] && store_aisle.sections[SE_Index - 1].shelves[SH_Index]
                    &&
                    ((store_aisle.sections[SE_Index - 1].shelves[SH_Index].dimensions.height === store_aisle.sections[SE_Index].shelves[SH_Index].dimensions.height)
                        || SH_Index === 0
                    )) {                    // get all items in the previous shelf
                    let prev_shelf_items = JSON.parse(JSON.stringify(store_aisle.sections[SE_Index - 1].shelves[SH_Index].items));
                    // sort the items by distFromStart in case they are not already sorted
                    prev_shelf_items.sort(function (a: any, b: any) {
                        let aNumber = a.placement.distFromStart ? a.placement.distFromStart : 0;
                        let bNumber = b.placement.distFromStart ? b.placement.distFromStart : 0;
                        let aId = a.Id;
                        let bId = b.Id;
                        return (aNumber > bNumber) ? 1 : (aNumber < bNumber) ? -1 : ((aId > bId) ? 1 : (aId < bId) ? -1 : 0);
                    });

                    // if so check if the last item (based on distFromStart) on the previous shelf intrudes into this shelf
                    let lastItem = prev_shelf_items[prev_shelf_items.length - 1];
                    console.log('lastItem', lastItem);
                    if (lastItem) {
                        let endPoint = (lastItem.placement.distFromStart ? lastItem.placement.distFromStart : 0) + ((lastItem.placement.pWidth != undefined ? lastItem.placement.pWidth : ProductDefaultDimensions.width) * lastItem.placement.faces);
                        let leftInShelf = store_aisle.sections[SE_Index].shelves[SH_Index].dimensions.width - endPoint;
                        if (leftInShelf < 0) firstDistFromStart = (-1 * leftInShelf);
                        console.log('leftInShelf', leftInShelf, 'firstDistFromStart', firstDistFromStart);
                    }
                }
            }
            console.log('firstDistFromStart', firstDistFromStart);

            // thisShelf.items.sort(function (a: any, b: any) {
            //     let aNumber = a.placement.distFromStart ? a.placement.distFromStart : 0;
            //     let bNumber = b.placement.distFromStart ? b.placement.distFromStart : 0;
            //     let aId = a.Id;
            //     let bId = b.Id;
            //     return (aNumber > bNumber) ? 1 : (aNumber < bNumber) ? -1 : ((aId > bId) ? 1 : (aId < bId) ? -1 : 0);
            // });
            // let viewShelf = JSON.parse(JSON.stringify(thisShelf));
            // console.log('thisShelf initial', viewShelf);
            // // empty all items from current shelf
            // thisShelf.items = [];

            // let distFromStart = firstDistFromStart;
            // let distInShelf = 0;
            // let index = 0;
            // let startOfShelfId = viewShelf.id;
            // let spaceInShelf = viewShelf.dimensions.width;
            // // go over the initial state and populate each shelf with the correct items in the correct order
            // let shelfId = 0;
            // for (let n = 0; n < viewShelf.items.length; n++) {
            //     let item = JSON.parse(JSON.stringify(viewShelf.items[n]));
            //     item.id = startOfShelfId + 'I' + index;
            //     index++;
            //     item.placement.distFromStart = distFromStart;
            //     let itemWidth = viewShelf.items[n].placement.pWidth;
            //     distFromStart += (itemWidth != undefined ? itemWidth : 0) * item.placement.faces;
            //     distInShelf += (itemWidth != undefined ? itemWidth : 0) * item.placement.faces;
            //     spaceInShelf = thisShelf.dimensions.width - distInShelf;
            //     thisShelf.items.push(item);
            //     console.log('--- item', item, 'distInShelf', distInShelf, 'spaceInShelf', spaceInShelf);
            // }

            // save initial state of shelvs with items ordered by distFromStart (distance from start of shelf)
            for (let i = 0; i < combinedShelves.length; i++) {
                // sort shelf items by distFromStart
                combinedShelves[i].items.sort(function (a: any, b: any) {
                    let aNumber = a.placement.distFromStart ? a.placement.distFromStart : 0;
                    let bNumber = b.placement.distFromStart ? b.placement.distFromStart : 0;
                    let aId = a.Id;
                    let bId = b.Id;
                    return (aNumber > bNumber) ? 1 : (aNumber < bNumber) ? -1 : ((aId > bId) ? 1 : (aId < bId) ? -1 : 0);
                });
            }
            let viewCombinedShelves = JSON.parse(JSON.stringify(combinedShelves));
            console.log('combinedShelves initial', viewCombinedShelves);
            // empty all items from current shelvs
            for (let i = 0; i < combinedShelves.length; i++) {
                combinedShelves[i].items = [];
            }

            let distFromStart = firstDistFromStart;
            let distInShelf = firstDistFromStart;
            let index = 0;
            let startOfShelfId = '';
            let spaceInShelf = 0;
            // go over the initial state and populate each shelf in combinedShelves with the correct items in the correct order
            let shelfId = 0;
            for (let i = 0; i < viewCombinedShelves.length; i++) {
                if (i === 0) {
                    startOfShelfId = viewCombinedShelves[i].id;
                    spaceInShelf = viewCombinedShelves[shelfId].dimensions.width;
                }
                for (let n = 0; n < viewCombinedShelves[i].items.length; n++) {
                    let item = JSON.parse(JSON.stringify(viewCombinedShelves[i].items[n]));
                    /* Barak 26.11.20 - return items above main line (item index >= 100) back into the shelf without altering anything else */
                    let itemInd = parseInt(item.id.substring(item.id.indexOf('I') + 1));
                    if (itemInd >= 100) {
                        if (item.shelfLevel === 1 || item.shelfLevel === undefined) combinedShelves[shelfId].items.push(item); // for items in freezer === 2
                        /* Barak 21.12.20 - verify items with shelfLevel 2 position */
                        else { // for items with shelfLevel > 1
                            // we need to return the item to it's original shelf
                            let destinationSec = parseInt(item.id.substring(item.id.indexOf('SE') + 2, item.id.indexOf('SH')));
                            let destinationSh = parseInt(item.id.substring(item.id.indexOf('SH') + 2, item.id.indexOf('I')));
                            let aisle: PlanogramAisle = JSON.parse(JSON.stringify(blankPlanogramAisle));
                            let aisles = this.props.store ? this.props.store.aisles : undefined;
                            let aisleId = shelf.id.substring(0, shelf.id.indexOf('SE'));
                            if (aisles) {
                                for (let i = 0; i < aisles.length; i++) {
                                    if (aisles[i].id === aisleId) {
                                        aisle = aisles[i];
                                        break;
                                    }
                                }
                            }
                            aisle.sections[destinationSec].shelves[destinationSh].items.push(item);
                        }
                        /*************************************************************/
                    }
                    else {
                        /************************************************************************************************************************/
                        if (spaceInShelf <= 0) {
                            shelfId++; // the shelf id in the viewCombinedShelves array
                            if (viewCombinedShelves[shelfId]) {
                                // there is a next shelf so we update the shelfNumber, startOfShelfId, spaceInShelf and index
                                // let shelfNumber = parseInt(JSON.parse(JSON.stringify(startOfShelfId.substring(startOfShelfId.indexOf('SE') + 2, startOfShelfId.indexOf('SH'))))) + 1;
                                let old_sec_num = parseInt(JSON.parse(JSON.stringify(startOfShelfId.substring(startOfShelfId.indexOf('SE') + 2, startOfShelfId.indexOf('SH')))));
                                let new_sec_num = parseInt(JSON.parse(JSON.stringify(viewCombinedShelves[shelfId].id.substring(viewCombinedShelves[shelfId].id.indexOf('SE') + 2, viewCombinedShelves[shelfId].id.indexOf('SH')))))
                                startOfShelfId = JSON.parse(JSON.stringify(startOfShelfId.substring(0, startOfShelfId.indexOf('SE') + 2) + new_sec_num + startOfShelfId.substring(startOfShelfId.indexOf('SH'))));
                                if (new_sec_num - old_sec_num === 1) {
                                    distInShelf = (spaceInShelf * -1);
                                    distFromStart = (spaceInShelf * -1);
                                    spaceInShelf = viewCombinedShelves[shelfId].dimensions.width - distInShelf;
                                } else {
                                    distInShelf = 0;
                                    distFromStart = 0;
                                    spaceInShelf = 0;
                                }
                                index = 0;
                            } else
                                // this shelf dosn't exist in the arry so we stay in this shelf
                                shelfId--;
                            console.log('startOfShelfId', startOfShelfId, 'shelfId', shelfId);
                        }
                        item.id = startOfShelfId + 'I' + index;
                        index++;
                        item.placement.distFromStart = distFromStart;
                        let itemWidth = viewCombinedShelves[i].items[n].placement.pWidth;
                        distFromStart += (itemWidth != undefined ? itemWidth : ProductDefaultDimensions.width) * item.placement.faces;
                        distInShelf += (itemWidth != undefined ? itemWidth : ProductDefaultDimensions.width) * item.placement.faces;
                        if (viewCombinedShelves[shelfId]) {
                            spaceInShelf = viewCombinedShelves[shelfId].dimensions.width - distInShelf;
                            console.log('shelfId', shelfId, 'spaceInShelf', spaceInShelf);
                            combinedShelves[shelfId].items.push(item);
                        }
                        console.log('--- item', // viewCombinedShelves[i].items[n], 'new item',
                            item, 'distInShelf', distInShelf, 'spaceInShelf', spaceInShelf, 'distFromStart', distFromStart);
                    }
                }
            }
        }

        /* Barak 22.11.20 - Add saving array for aisle, allowing to do 
            Ctrl+Z to move back and Ctrl+Y to move forward *
            await this.saveSnapShot(shelf);
        *************************************************************/
        // console.log('checkLevel2 saveAisleShelf in alignThisShelfToLeft',JSON.parse(JSON.stringify(shelf)));
        /* Barak 22.11.20 */ await this.saveAisleShelf(shelf);
    }

    /* add function to align shelf to the right */
    alignThisShelfToRight = async (shelf: PlanogramShelf) => {
        const { store, aisleShelves, shelvesDetails } = this.props;

        // check if we need to align multiple shelves
        let all_shelves_to_align: PlanogramShelf[] = [];
        if (this.props.multiSelectId.length > 1) {
            // sort multiSelectId by shelf and section
            this.props.multiSelectId.sort(function (a: any, b: any) {
                let currSE_A: number = parseInt(a.substring(a.indexOf('SE') + 2, a.indexOf('SH')));
                let currSH_A: number = parseInt(a.substring(a.indexOf('SH') + 2, a.indexOf('I')));
                let currSE_B: number = parseInt(b.substring(b.indexOf('SE') + 2, b.indexOf('SH')));
                let currSH_B: number = parseInt(b.substring(b.indexOf('SH') + 2, b.indexOf('I')));
                // if it's the same shelf order by section    
                if (currSH_A === currSH_B)
                    return (currSE_A > currSE_B) ? 1 : (currSE_A < currSE_B) ? -1 : 0;
                else return (currSH_A > currSH_B) ? 1 : (currSH_A < currSH_B) ? -1 : 0;
            });
            // find each shelf represented in multiSelectId and add them to all_shelves_to_align
            for (let a = 0; a < this.props.multiSelectId.length; a++) {
                let target_A = this.props.multiSelectId[a].substring(0, this.props.multiSelectId[a].indexOf('SE'));
                let target_SE = parseInt(this.props.multiSelectId[a].substring(this.props.multiSelectId[a].indexOf('SE') + 2, this.props.multiSelectId[a].indexOf('SH')));
                let target_SH = parseInt(this.props.multiSelectId[a].substring(this.props.multiSelectId[a].indexOf('SH') + 2));
                if (store) {
                    for (let i = 0; i < store.aisles.length; i++) {
                        if (store.aisles[i].id === target_A) {
                            let thisShelf = store.aisles[i].sections[target_SE].shelves[target_SH];
                            let index = all_shelves_to_align.findIndex(shelf => shelf.id === thisShelf.id);
                            if (index < 0 && thisShelf.freezer != 1 && thisShelf.freezer != 2) all_shelves_to_align.push(thisShelf);
                            // now that we've added the shelf the item is actually on, we need to check if the item covers more than one shelf.
                            // since we know this is the shelf the item starts in we can get the item's details from it.
                            let item = thisShelf.items.filter((line:any) => line.id === this.props.multiSelectId[a]);
                            let itemTotalWidth = (item[0].placement.distFromStart ? item[0].placement.distFromStart : 0) + ((item[0].placement.pWidth ? item[0].placement.pWidth : ProductDefaultDimensions.width) * item[0].placement.faces);
                            let shelfWidth = thisShelf.dimensions.width;
                            if (itemTotalWidth > shelfWidth) {
                                while (itemTotalWidth > shelfWidth) {
                                    target_SE++;
                                    let nextShelf = store.aisles[i].sections[target_SE].shelves[target_SH];
                                    let index = all_shelves_to_align.findIndex(shelf => shelf.id === nextShelf.id);
                                    if (index < 0 && nextShelf.freezer != 1 && nextShelf.freezer != 2) all_shelves_to_align.push(nextShelf);
                                    itemTotalWidth -= shelfWidth;
                                    shelfWidth = nextShelf.dimensions.width;
                                }
                            }
                        }
                    }
                }
            }
        } else if (shelf.freezer != 1 && shelf.freezer != 2) all_shelves_to_align.push(shelf);
        console.log('all_shelves_to_align', all_shelves_to_align);

        // now that we have all shelvs that need to be aligned we can align them one after the other in reverse order (align right)
        for (let aa = all_shelves_to_align.length - 1; aa >= 0; aa--) {
            // find latest version of this shelf in the store aisle
            let thisShelf = all_shelves_to_align[aa];
            let target_A = all_shelves_to_align[aa].id.substring(0, all_shelves_to_align[aa].id.indexOf('SE'));
            let target_SE = parseInt(all_shelves_to_align[aa].id.substring(all_shelves_to_align[aa].id.indexOf('SE') + 2, all_shelves_to_align[aa].id.indexOf('SH')));
            let target_SH = parseInt(all_shelves_to_align[aa].id.substring(all_shelves_to_align[aa].id.indexOf('SH') + 2));
            if (store) {
                for (let i = 0; i < store.aisles.length; i++) {
                    if (store.aisles[i].id === target_A) {
                        thisShelf = store.aisles[i].sections[target_SE].shelves[target_SH];
                    }
                }
            }
            console.log('thisShelf', thisShelf);

            // get this shelf's combined shelves and only leave in the array of shelves that exist in all_shelves_to_align
            let shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[all_shelves_to_align[aa].id]));
            while (shelfDetails.main_shelf) {
                let ids = JSON.parse(JSON.stringify(shelfDetails.main_shelf));
                shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[ids]));
                thisShelf = aisleShelves[ids];
            }
            let { combined } = shelfDetails;

            let combinedShelves = [thisShelf].concat(combined.map((id: any) => aisleShelves[id]));
            console.log('initial combinedShelves', combinedShelves);

            // remove from combinedShelves any shelves that do not appear in all_shelves_to_align
            let newCombinedShelves = [];
            for (let i = 0; i < combinedShelves.length; i++) {
                let index = all_shelves_to_align.findIndex(shelf => shelf.id === combinedShelves[i].id);
                if (index >= 0) newCombinedShelves.push(combinedShelves[i]);
            }
            combinedShelves = newCombinedShelves;
            console.log('newCombinedShelves', newCombinedShelves);

            // save initial state of shelvs with items ordered by distFromStart (distance from start of shelf)
            let combinedShelvesLength = 0;
            let combinedItems: PlanogramItem[] = [];
            for (let i = 0; i < combinedShelves.length; i++) {
                // sort shelf items by distFromStart
                combinedShelves[i].items.sort(function (a: any, b: any) {
                    let aNumber = a.placement.distFromStart ? a.placement.distFromStart : 0;
                    let bNumber = b.placement.distFromStart ? b.placement.distFromStart : 0;
                    let aId = a.Id;
                    let bId = b.Id;
                    return (aNumber > bNumber) ? 1 : (aNumber < bNumber) ? -1 : ((aId > bId) ? 1 : (aId < bId) ? -1 : 0);
                });
                combinedItems = combinedItems.concat(combinedShelves[i].items);
                // get the combined length of the entire shelf
                combinedShelvesLength += combinedShelves[i].dimensions.width;
            }
            let viewCombinedShelves = JSON.parse(JSON.stringify(combinedShelves));
            console.log('combinedShelves initial', viewCombinedShelves);

            // get the combined length of all items on the shelf
            let combinedItemsLength = 0;
            for (let i = 0; i < combinedItems.length; i++) {
                let itemWidth = combinedItems[i].placement.pWidth;
                let itemInd = parseInt(combinedItems[i].id.substring(combinedItems[i].id.indexOf('I') + 1));
                if (itemInd >= 100) {
                    console.log('item', combinedItems[i].product, combinedItems[i].id, 'item width', (itemWidth != undefined ? itemWidth : ProductDefaultDimensions.width) * combinedItems[i].placement.faces);
                    combinedItemsLength += (itemWidth != undefined ? itemWidth : ProductDefaultDimensions.width) * combinedItems[i].placement.faces;
                }
            }

            // if the total length of the items is longer than the total width of the shelf DO NOT ALLOW TO ALIGN RIGHT
            if (combinedItemsLength > combinedShelvesLength) {
                // let errorMsg = 'לא ניתן לבצע הצמדה לימין במדף ' + viewCombinedShelves[0].id + ' - יש בו מוצרים גולשים';
                console.log('combinedItemsLength', combinedItemsLength, 'combinedShelvesLength', combinedShelvesLength);
                let errorMsg = 'Cannot align shelf - ' + viewCombinedShelves[0].id + ' to the right - it has overflowing items';
                alert(errorMsg);
                continue;
            }

            // empty all items from current shelvs
            for (let i = 0; i < combinedShelves.length; i++) {
                combinedShelves[i].items = [];
            }

            let distFromStart = viewCombinedShelves[viewCombinedShelves.length - 1].dimensions.width;
            let distInShelf = viewCombinedShelves[viewCombinedShelves.length - 1].dimensions.width;
            let index = viewCombinedShelves[viewCombinedShelves.length - 1].items.length - 1;
            let startOfShelfId = '';
            let spaceInShelf = viewCombinedShelves[viewCombinedShelves.length - 1].dimensions.width;
            // go over the initial state and populate each shelf in combinedShelves with the correct items in the correct order
            let shelfId = viewCombinedShelves.length - 1;
            for (let i = viewCombinedShelves.length - 1; i >= 0; i--) {
                if (i === viewCombinedShelves.length - 1) {
                    startOfShelfId = viewCombinedShelves[i].id;
                    spaceInShelf = viewCombinedShelves[shelfId].dimensions.width;
                }
                for (let n = viewCombinedShelves[i].items.length - 1; n >= 0; n--) {
                    let item = JSON.parse(JSON.stringify(viewCombinedShelves[i].items[n]));
                    /* Barak 26.11.20 - return items above main line (item index >= 100) back into the shelf without altering anything else */
                    let itemInd = parseInt(item.id.substring(item.id.indexOf('I') + 1));
                    if (itemInd >= 100) {
                        if (item.shelfLevel === 1 || item.shelfLevel === undefined) combinedShelves[shelfId].items.push(item); // for items in freezer === 2
                        /* Barak 21.12.20 - verify items with shelfLevel 2 position */
                        else { // for items with shelfLevel > 1
                            // we need to return the item to it's original shelf
                            let destinationSec = parseInt(item.id.substring(item.id.indexOf('SE') + 2, item.id.indexOf('SH')));
                            let destinationSh = parseInt(item.id.substring(item.id.indexOf('SH') + 2, item.id.indexOf('I')));
                            let aisle: PlanogramAisle = JSON.parse(JSON.stringify(blankPlanogramAisle));
                            let aisles = this.props.store ? this.props.store.aisles : undefined;
                            let aisleId = shelf.id.substring(0, shelf.id.indexOf('SE'));
                            if (aisles) {
                                for (let i = 0; i < aisles.length; i++) {
                                    if (aisles[i].id === aisleId) {
                                        aisle = aisles[i];
                                        break;
                                    }
                                }
                            }
                            aisle.sections[destinationSec].shelves[destinationSh].items.push(item);
                        }
                        /*************************************************************/
                    }
                    else {
                        /************************************************************************************************************************/
                        if (spaceInShelf <= 0) {
                            shelfId--; // the shelf id in the viewCombinedShelves array
                            if (viewCombinedShelves[shelfId]) {
                                let old_sec_num = parseInt(JSON.parse(JSON.stringify(startOfShelfId.substring(startOfShelfId.indexOf('SE') + 2, startOfShelfId.indexOf('SH')))));
                                let new_sec_num = parseInt(JSON.parse(JSON.stringify(viewCombinedShelves[shelfId].id.substring(viewCombinedShelves[shelfId].id.indexOf('SE') + 2, viewCombinedShelves[shelfId].id.indexOf('SH')))))
                                startOfShelfId = JSON.parse(JSON.stringify(startOfShelfId.substring(0, startOfShelfId.indexOf('SE') + 2) + new_sec_num + startOfShelfId.substring(startOfShelfId.indexOf('SH'))));
                                // startOfShelfId = JSON.parse(JSON.stringify(startOfShelfId.substring(0, startOfShelfId.indexOf('SE') + 2) + shelfNumber + startOfShelfId.substring(startOfShelfId.indexOf('SH'))));
                                if (new_sec_num - old_sec_num === -1) {
                                    distInShelf = viewCombinedShelves[viewCombinedShelves.length - 1].dimensions.width - (spaceInShelf * -1);
                                    distFromStart = viewCombinedShelves[viewCombinedShelves.length - 1].dimensions.width - (spaceInShelf * -1);
                                    spaceInShelf = distInShelf; // viewCombinedShelves[shelfId].dimensions.width - distInShelf;
                                } else {
                                    distInShelf = viewCombinedShelves[viewCombinedShelves.length - 1].dimensions.width;
                                    distFromStart = viewCombinedShelves[viewCombinedShelves.length - 1].dimensions.width;
                                    spaceInShelf = distInShelf;;
                                }
                                index = viewCombinedShelves[shelfId].items.length - 1;
                            } else
                                // this shelf dosn't exist in the arry so we stay in this shelf
                                shelfId++;
                        }
                        item.id = startOfShelfId + 'I' + index;
                        index--;
                        let itemWidth = viewCombinedShelves[i].items[n].placement.pWidth;
                        distFromStart -= (itemWidth != undefined ? itemWidth : ProductDefaultDimensions.width) * item.placement.faces;
                        distInShelf -= (itemWidth != undefined ? itemWidth : ProductDefaultDimensions.width) * item.placement.faces;
                        item.placement.distFromStart = distFromStart;
                        if (viewCombinedShelves[shelfId]) {
                            spaceInShelf = distInShelf; // viewCombinedShelves[shelfId].dimensions.width - distInShelf;
                            combinedShelves[shelfId].items.push(item);
                        }
                    }
                }
            }
        }

        await this.saveAisleShelf(shelf);
    }
    alignThisShelfToJustify = async (shelf: PlanogramShelf) => {
        const { store, aisleShelves, shelvesDetails } = this.props;

        // check if we need to align multiple shelves
        let all_shelves_to_align: PlanogramShelf[] = [];
        if (this.props.multiSelectId.length > 1) {
            // sort multiSelectId by shelf and section
            this.props.multiSelectId.sort(function (a: any, b: any) {
                let currSE_A: number = parseInt(a.substring(a.indexOf('SE') + 2, a.indexOf('SH')));
                let currSH_A: number = parseInt(a.substring(a.indexOf('SH') + 2, a.indexOf('I')));
                let currSE_B: number = parseInt(b.substring(b.indexOf('SE') + 2, b.indexOf('SH')));
                let currSH_B: number = parseInt(b.substring(b.indexOf('SH') + 2, b.indexOf('I')));
                // if it's the same shelf order by section    
                if (currSH_A === currSH_B)
                    return (currSE_A > currSE_B) ? 1 : (currSE_A < currSE_B) ? -1 : 0;
                else return (currSH_A > currSH_B) ? 1 : (currSH_A < currSH_B) ? -1 : 0;
            });
            // find each shelf represented in multiSelectId and add them to all_shelves_to_align
            for (let a = 0; a < this.props.multiSelectId.length; a++) {
                let target_A = this.props.multiSelectId[a].substring(0, this.props.multiSelectId[a].indexOf('SE'));
                let target_SE = parseInt(this.props.multiSelectId[a].substring(this.props.multiSelectId[a].indexOf('SE') + 2, this.props.multiSelectId[a].indexOf('SH')));
                let target_SH = parseInt(this.props.multiSelectId[a].substring(this.props.multiSelectId[a].indexOf('SH') + 2));
                if (store) {
                    for (let i = 0; i < store.aisles.length; i++) {
                        if (store.aisles[i].id === target_A) {
                            let thisShelf = store.aisles[i].sections[target_SE].shelves[target_SH];
                            let index = all_shelves_to_align.findIndex(shelf => shelf.id === thisShelf.id);
                            if (index < 0 && thisShelf.freezer != 1 && thisShelf.freezer != 2) all_shelves_to_align.push(thisShelf);
                            // now that we've added the shelf the item is actually on, we need to check if the item covers more than one shelf.
                            // since we know this is the shelf the item starts in we can get the item's details from it.
                            let item = thisShelf.items.filter((line:any) => line.id === this.props.multiSelectId[a]);
                            let itemTotalWidth = (item[0].placement.distFromStart ? item[0].placement.distFromStart : 0) + ((item[0].placement.pWidth ? item[0].placement.pWidth : ProductDefaultDimensions.width) * item[0].placement.faces);
                            let shelfWidth = thisShelf.dimensions.width;
                            if (itemTotalWidth > shelfWidth) {
                                while (itemTotalWidth > shelfWidth) {
                                    target_SE++;
                                    let nextShelf = store.aisles[i].sections[target_SE].shelves[target_SH];
                                    let index = all_shelves_to_align.findIndex(shelf => shelf.id === nextShelf.id);
                                    if (index < 0 && nextShelf.freezer != 1 && nextShelf.freezer != 2) all_shelves_to_align.push(nextShelf);
                                    itemTotalWidth -= shelfWidth;
                                    shelfWidth = nextShelf.dimensions.width;
                                }
                            }
                        }
                    }
                }
            }
        } else if (shelf.freezer != 1 && shelf.freezer != 2) all_shelves_to_align.push(shelf);
        console.log('all_shelves_to_align', all_shelves_to_align);

        // now that we have all shelvs that need to be aligned we can align them one after the other
        for (let aa = 0; aa < all_shelves_to_align.length; aa++) {
            // find latest version of this shelf in the store aisle
            let thisShelf = all_shelves_to_align[aa];
            let target_A = all_shelves_to_align[aa].id.substring(0, all_shelves_to_align[aa].id.indexOf('SE'));
            let target_SE = parseInt(all_shelves_to_align[aa].id.substring(all_shelves_to_align[aa].id.indexOf('SE') + 2, all_shelves_to_align[aa].id.indexOf('SH')));
            let target_SH = parseInt(all_shelves_to_align[aa].id.substring(all_shelves_to_align[aa].id.indexOf('SH') + 2));
            if (store) {
                for (let i = 0; i < store.aisles.length; i++) {
                    if (store.aisles[i].id === target_A) {
                        thisShelf = store.aisles[i].sections[target_SE].shelves[target_SH];
                    }
                }
            }
            console.log('thisShelf', thisShelf);

            // get this shelf's combined shelves and only leave in the array shelves that exist in all_shelves_to_align
            let shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[all_shelves_to_align[aa].id]));
            while (shelfDetails.main_shelf) {
                let ids = JSON.parse(JSON.stringify(shelfDetails.main_shelf));
                shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[ids]));
                thisShelf = aisleShelves[ids];
            }
            let { combined } = shelfDetails;

            let combinedShelves = [thisShelf].concat(combined.map((id: any) => aisleShelves[id]));
            console.log('initial combinedShelves', combinedShelves);

            // remove from combinedShelves any shelves that do not appear in all_shelves_to_align
            let newCombinedShelves = [];
            for (let i = 0; i < combinedShelves.length; i++) {
                let index = all_shelves_to_align.findIndex(shelf => shelf.id === combinedShelves[i].id);
                if (index >= 0) newCombinedShelves.push(combinedShelves[i]);
            }
            combinedShelves = newCombinedShelves;
            console.log('newCombinedShelves', newCombinedShelves);

            let A_Id = combinedShelves[0].id.substring(0, combinedShelves[0].id.indexOf('SE'));
            let SE_Index = parseInt(combinedShelves[0].id.substring(combinedShelves[0].id.indexOf('SE') + 2, combinedShelves[0].id.indexOf('SH')));
            let SH_Index = parseInt(combinedShelves[0].id.substring(combinedShelves[0].id.indexOf('SH') + 2));

            // check if this aisle has a shelf in the same height, in previous section
            let firstDistFromStart = 0;
            if (store) {
                let store_aisle: PlanogramAisle = store.aisles.filter((aisle: PlanogramAisle) => aisle.id === A_Id)[0];
                console.log('store_aisle', store_aisle, SE_Index, SH_Index);
                if (store_aisle && store_aisle.sections[SE_Index - 1] && store_aisle.sections[SE_Index - 1].shelves[SH_Index]
                    &&
                    ((store_aisle.sections[SE_Index - 1].shelves[SH_Index].dimensions.height === store_aisle.sections[SE_Index].shelves[SH_Index].dimensions.height)
                        || SH_Index === 0
                    )) {
                    // get all items in the previous shelf
                    let prev_shelf_items = JSON.parse(JSON.stringify(store_aisle.sections[SE_Index - 1].shelves[SH_Index].items));
                    // sort the items by distFromStart in case they are not already sorted
                    prev_shelf_items.sort(function (a: any, b: any) {
                        let aNumber = a.placement.distFromStart ? a.placement.distFromStart : 0;
                        let bNumber = b.placement.distFromStart ? b.placement.distFromStart : 0;
                        let aId = a.Id;
                        let bId = b.Id;
                        return (aNumber > bNumber) ? 1 : (aNumber < bNumber) ? -1 : ((aId > bId) ? 1 : (aId < bId) ? -1 : 0);
                    });

                    // if so check if the last item (based on distFromStart) on the previous shelf intrudes into this shelf
                    let lastItem = prev_shelf_items[prev_shelf_items.length - 1];
                    console.log('lastItem', lastItem);
                    if (lastItem) {
                        let endPoint = (lastItem.placement.distFromStart ? lastItem.placement.distFromStart : 0) + ((lastItem.placement.pWidth != undefined ? lastItem.placement.pWidth : ProductDefaultDimensions.width) * lastItem.placement.faces);
                        let leftInShelf = store_aisle.sections[SE_Index].shelves[SH_Index].dimensions.width - endPoint;
                        if (leftInShelf < 0) firstDistFromStart = (-1 * leftInShelf);
                        console.log('leftInShelf', leftInShelf, 'firstDistFromStart', firstDistFromStart);
                    }
                }
            }
            console.log('firstDistFromStart', firstDistFromStart);

            // save initial state of shelvs with items ordered by distFromStart (distance from start of shelf)
            let combinedShelvesLength = 0;
            let combinedItems: PlanogramItem[] = [];
            for (let i = 0; i < combinedShelves.length; i++) {
                // sort shelf items by distFromStart
                combinedShelves[i].items.sort(function (a: any, b: any) {
                    let aNumber = a.placement.distFromStart ? a.placement.distFromStart : 0;
                    let bNumber = b.placement.distFromStart ? b.placement.distFromStart : 0;
                    let aId = a.Id;
                    let bId = b.Id;
                    return (aNumber > bNumber) ? 1 : (aNumber < bNumber) ? -1 : ((aId > bId) ? 1 : (aId < bId) ? -1 : 0);
                });
                combinedItems = combinedItems.concat(combinedShelves[i].items);
                // get the combined length of the entire shelf
                combinedShelvesLength += combinedShelves[i].dimensions.width;
            }
            /* Barak 26.11.20 - remove from combinedItems items with Index of over 100 (items above other items) *
            let tempCDombinedItems = [];
            for (let n = 0; n < combinedItems.length; n++) {
                let itemInd = parseInt(combinedItems[n].id.substring(combinedItems[n].id.indexOf('I') + 1));
                if (itemInd < 100) tempCDombinedItems.push(combinedItems[n]);
            }
            combinedItems = tempCDombinedItems;
            *****************************************************************************************************/
            let viewCombinedShelves = JSON.parse(JSON.stringify(combinedShelves));
            console.log('combinedShelves initial', viewCombinedShelves);
            // empty all items from current shelvs
            for (let i = 0; i < combinedShelves.length; i++) {
                combinedShelves[i].items = [];
            }

            // get the combined length of all items on the shelf
            let combinedItemsLength = 0;
            for (let i = 0; i < combinedItems.length; i++) {
                let itemWidth = combinedItems[i].placement.pWidth;
                /* Barak 26.11.20 - count items directly on the shelf only (item index < 100) */
                let itemInd = parseInt(combinedItems[i].id.substring(combinedItems[i].id.indexOf('I') + 1));
                if (itemInd < 100)
                    /**************************************************************************/
                    combinedItemsLength += (itemWidth != undefined ? itemWidth : ProductDefaultDimensions.width) * combinedItems[i].placement.faces;
            }

            // get the space we need to add between each item on this shelf to get an even spread of items
            let evenSpaceAdded = (combinedShelvesLength - combinedItemsLength - firstDistFromStart) / combinedItems.length;

            console.log('alignThisShelfToJustify combinedShelvesLength', combinedShelvesLength, 'combinedItemsLength', combinedItemsLength, 'evenSpaceAdded', evenSpaceAdded);

            let distFromStart = firstDistFromStart;
            let distInShelf = firstDistFromStart;
            let index = 0;
            let startOfShelfId = '';
            let spaceInShelf = 0;
            // go over the initial state and populate each shelf in combinedShelves with the correct items in the correct order
            let shelfId = 0;
            let combinedItemsIndex = 0;
            for (let i = 0; i < viewCombinedShelves.length; i++) {
                if (i === 0) {
                    startOfShelfId = viewCombinedShelves[i].id;
                    spaceInShelf = viewCombinedShelves[shelfId].dimensions.width;
                }
                for (let n = 0; n < viewCombinedShelves[i].items.length; n++) {
                    let item = JSON.parse(JSON.stringify(viewCombinedShelves[i].items[n]));
                    /* Barak 26.11.20 - return items above main line (item index >= 100) back into the shelf without altering anything else */
                    let itemInd = parseInt(item.id.substring(item.id.indexOf('I') + 1));
                    if (itemInd >= 100) {
                        if (item.shelfLevel === 1 || item.shelfLevel === undefined) combinedShelves[shelfId].items.push(item); // for items in freezer === 2
                        /* Barak 21.12.20 - verify items with shelfLevel 2 position */
                        else { // for items with shelfLevel > 1
                            // we need to return the item to it's original shelf
                            let destinationSec = parseInt(item.id.substring(item.id.indexOf('SE') + 2, item.id.indexOf('SH')));
                            let destinationSh = parseInt(item.id.substring(item.id.indexOf('SH') + 2, item.id.indexOf('I')));
                            let aisle: PlanogramAisle = JSON.parse(JSON.stringify(blankPlanogramAisle));
                            let aisles = this.props.store ? this.props.store.aisles : undefined;
                            let aisleId = shelf.id.substring(0, shelf.id.indexOf('SE'));
                            if (aisles) {
                                for (let i = 0; i < aisles.length; i++) {
                                    if (aisles[i].id === aisleId) {
                                        aisle = aisles[i];
                                        break;
                                    }
                                }
                            }
                            aisle.sections[destinationSec].shelves[destinationSh].items.push(item);
                        }
                        /*************************************************************/
                    }
                    else {
                        /************************************************************************************************************************/
                        if (evenSpaceAdded > 0) {
                            // if there is just one item on the shelf
                            if (combinedItems.length === 1) {
                                distFromStart += evenSpaceAdded / 2;
                                distInShelf += evenSpaceAdded / 2;
                                spaceInShelf -= evenSpaceAdded / 2;
                            }
                            // if there are two item on the shelf and this is the second item 
                            if (combinedItems.length === 2 && combinedItemsIndex === 1) {
                                distFromStart += evenSpaceAdded * 2;
                                distInShelf += evenSpaceAdded * 2;
                                spaceInShelf -= evenSpaceAdded * 2;
                            }
                            // if there are more than two item on the shelf and this is NOT the first one or the last one
                            if (combinedItems.length > 2 && combinedItemsIndex > 0 && combinedItemsIndex < combinedItems.length) {
                                distFromStart += evenSpaceAdded * (combinedItems.length / (combinedItems.length - 1));
                                distInShelf += evenSpaceAdded * (combinedItems.length / (combinedItems.length - 1));
                                spaceInShelf -= evenSpaceAdded * (combinedItems.length / (combinedItems.length - 1));
                            }
                        }
                        if (spaceInShelf <= 0) {
                            shelfId++; // the shelf id in the viewCombinedShelves array
                            if (viewCombinedShelves[shelfId]) {
                                // there is a next shelf so we update the shelfNumber, startOfShelfId, spaceInShelf and index
                                // let shelfNumber = parseInt(JSON.parse(JSON.stringify(startOfShelfId.substring(startOfShelfId.indexOf('SE') + 2, startOfShelfId.indexOf('SH'))))) + 1;
                                let old_sec_num = parseInt(JSON.parse(JSON.stringify(startOfShelfId.substring(startOfShelfId.indexOf('SE') + 2, startOfShelfId.indexOf('SH')))));
                                let new_sec_num = parseInt(JSON.parse(JSON.stringify(viewCombinedShelves[shelfId].id.substring(viewCombinedShelves[shelfId].id.indexOf('SE') + 2, viewCombinedShelves[shelfId].id.indexOf('SH')))))
                                startOfShelfId = JSON.parse(JSON.stringify(startOfShelfId.substring(0, startOfShelfId.indexOf('SE') + 2) + new_sec_num + startOfShelfId.substring(startOfShelfId.indexOf('SH'))));
                                if (new_sec_num - old_sec_num === 1) {
                                    distInShelf = (spaceInShelf * -1);
                                    distFromStart = (spaceInShelf * -1);
                                    spaceInShelf = viewCombinedShelves[shelfId].dimensions.width - distInShelf;
                                } else {
                                    distInShelf = 0;
                                    distFromStart = 0;
                                    spaceInShelf = 0;
                                }
                                index = 0;
                            } else
                                // this shelf dosn't exist in the arry so we stay in this shelf
                                shelfId--;
                            console.log('startOfShelfId', startOfShelfId, 'shelfId', shelfId);
                        }
                        item.id = startOfShelfId + 'I' + index;
                        index++;
                        item.placement.distFromStart = distFromStart;
                        let itemWidth = viewCombinedShelves[i].items[n].placement.pWidth;
                        distFromStart += ((itemWidth != undefined ? itemWidth : ProductDefaultDimensions.width) * item.placement.faces);
                        distInShelf += ((itemWidth != undefined ? itemWidth : ProductDefaultDimensions.width) * item.placement.faces);
                        combinedItemsIndex++;
                        if (viewCombinedShelves[shelfId]) {
                            spaceInShelf = viewCombinedShelves[shelfId].dimensions.width - distInShelf;
                            combinedShelves[shelfId].items.push(item);
                        }
                        console.log('--- item', // viewCombinedShelves[i].items[n], 'new item',
                            item, 'distInShelf', distInShelf, 'spaceInShelf', spaceInShelf);
                    }
                }
            }
        }
        await this.saveAisleShelf(shelf);
    }
    // addShelfItem = async (shelf: PlanogramShelf, newItem: PlanogramItem, shlfStart?: number, newItems?: PlanogramItem[]) => {
    //     console.log('addShelfItem section', JSON.parse(JSON.stringify(shelf)), newItem);
    //     // make sure shelf has no undefined items
    //     let initItems = JSON.parse(JSON.stringify(shelf.items));
    //     shelf.items = [];
    //     for (let i = 0; i < initItems.length; i++) {
    //         if (initItems[i] && typeof initItems[i] != 'undefined')
    //             shelf.items.push(initItems[i]);
    //     }
    //     if (newItems) {
    //         initItems = JSON.parse(JSON.stringify(newItems));
    //         newItems = [];
    //         for (let i = 0; i < initItems.length; i++) {
    //             if (initItems[i] && typeof initItems[i] != 'undefined') {
    //                 initItems[i].placement.distFromStart = i;
    //                 newItems.push(initItems[i]);
    //             }
    //         }
    //     }
    //     console.log('addShelfItem section. shelf', JSON.parse(JSON.stringify(shelf)), 'new item', newItem, 'shlfStart', shlfStart, 'new items', newItems);
    //     const { virtualStore } = this.props;
    //     const shelvesDetails = virtualStore.shelfDetails;
    //     const aisleShelves = virtualStore.shelfMap;

    //     // update shelf.distFromStart if shlfStart was sent
    //     if (shlfStart && shlfStart >= 0) {
    //         shelf.distFromStart = shlfStart;
    //         /***************************/
    //         let aisles_1 = this.props.store ? this.props.store.aisles : undefined;
    //         let aisleId_1 = shelf.id.substring(0, shelf.id.indexOf('SE'));
    //         let aisle_1: PlanogramAisle = JSON.parse(JSON.stringify(blankPlanogramAisle));
    //         if (aisles_1) {
    //             for (let i = 0; i < aisles_1.length; i++) {
    //                 if (aisles_1[i].id === aisleId_1) {
    //                     aisle_1 = aisles_1[i];
    //                     let seI = parseInt(shelf.id.substring(shelf.id.indexOf('SE') + 2, shelf.id.indexOf('SH')));
    //                     let shI = parseInt(shelf.id.substring(shelf.id.indexOf('SH') + 2));
    //                     if (aisle_1.sections[seI] && aisle_1.sections[seI].shelves[shI])
    //                         aisle_1.sections[seI].shelves[shI] = shelf;
    //                     break;
    //                 }
    //             }
    //         }
    //         if (aisle_1.id === aisleId_1) {
    //             await this.props.setStoreAisle(aisle_1);
    //             console.log('shelf after saving dist from start', JSON.parse(JSON.stringify(shelf)), 'aisle after saving new shelf distFromStart', JSON.parse(JSON.stringify(aisle_1)));
    //         }
    //         /***************************/
    //     }
    //     // make sure, in case no shlfStart was sent that the shelf.distFromStart fits with the first Item (ordered by distFromStart) on that shelf
    //     else {
    //         if (shelf.distFromStart > 0) {
    //             let num: number = shelf.items[0] && shelf.items[0].placement.distFromStart ? shelf.items[0].placement.distFromStart : 0;
    //             if (shelf.items.length >= 1 && num >= 0) shelf.distFromStart = num;
    //         }
    //     }

    //     // check if this shelf even has room to add a new item;
    //     if (shelf.distFromStart === shelf.dimensions.width && newItem && newItem.product > 0 && shelf.freezer != 1 && shelf.freezer != 2 && (newItem.shelfLevel === 1 || newItem.shelfLevel === undefined)) {
    //         // if there is a next shelf activate function with the rest of the items 
    //         let fullShelf = JSON.parse(JSON.stringify(shelf));
    //         // this is the data for the actual shelf
    //         let shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[shelf.id]));

    //         // get the full combined shelves
    //         while (shelfDetails.main_shelf) {
    //             let ids = JSON.parse(JSON.stringify(shelfDetails.main_shelf));
    //             shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[ids]));
    //             fullShelf = aisleShelves[ids];
    //         }
    //         let { combined } = shelfDetails;
    //         if (combined == null) combined = [];
    //         let combinedShelves = [fullShelf].concat(combined.map((id: any) => aisleShelves[id]));
    //         // find the index of the current shelf 
    //         let currI = combinedShelves.findIndex(object => object.id === shelf.id);
    //         if (currI >= 0 && combinedShelves[currI + 1]) {
    //             // move item/s to the next shelf
    //             console.log('call addShelfItem for shelf', JSON.parse(JSON.stringify(combinedShelves[currI + 1])), 'add new item', newItem);
    //             await this.addShelfItem(JSON.parse(JSON.stringify(combinedShelves[currI + 1])), newItem);
    //         }
    //         return;
    //     }
    //     // if there is a new item add it at the end of the shelf
    //     if (newItem && newItem.product > 0) {
    //         /* Barak 26.11.20 - add special numbering for items in shelf.freezer === 2 */
    //         if (shelf.freezer === 2
    //             /* && newItem.placement.distFromTop != shelf.dimensions.height - ((newItem.placement.pHeight ? newItem.placement.pHeight : ProductDefaultDimensions.height) * newItem.placement.stack) */
    //         ) {
    //             if (newItem.id === '') {
    //                 console.log('newItem.id === ""');
    //                 if (shelf.items.length > 1) {
    //                     let lastI = parseInt(shelf.items[shelf.items.length - 1].id.substring(shelf.items[shelf.items.length - 1].id.indexOf('I') + 1));
    //                     let newI = 0;
    //                     if (lastI < 100) newI = lastI + 100;
    //                     else newI = lastI + 1;
    //                     newItem.id = shelf.id + "I" + newI;
    //                     console.log('1. newItem.id = ', newItem.id);
    //                 } else {
    //                     newItem.id = shelf.id + "I" + 100;
    //                     console.log('2. newItem.id = ', newItem.id);
    //                 }
    //             }
    //             console.log('addShelfItem freezer === 2 newItem', newItem);
    //         }
    //         /***************************************************************************/
    //         /* Barak 21.12.20 - add special numbering for items in shelfLevel > 1 */
    //         if (newItem.shelfLevel > 1) {
    //             if (newItem.id === '') {
    //                 console.log('newItem.id === ""');
    //                 if (shelf.items.length > 1) {
    //                     let lastI = parseInt(shelf.items[shelf.items.length - 1].id.substring(shelf.items[shelf.items.length - 1].id.indexOf('I') + 1));
    //                     let newI = 0;
    //                     if (lastI < 100) newI = lastI + 100;
    //                     else newI = lastI + 1;
    //                     newItem.id = shelf.id + "I" + newI;
    //                     console.log('1. newItem.id = ', newItem.id);
    //                 } else {
    //                     newItem.id = shelf.id + "I" + 100;
    //                     console.log('2. newItem.id = ', newItem.id);
    //                 }
    //             }
    //             console.log('addShelfItem shelfLevel > 1 newItem', newItem);
    //             // now that we have an id to the new item we can save it in the linked bottom level item as linkedItemUp
    //             let sItem = shelf.items.filter(line => line.id === this.props.multiSelectId[0]);
    //             if (sItem && sItem[0]) sItem[0].linkedItemUp = newItem.id;
    //         }
    //         /************************************************************************/
    //         shelf.items.push(newItem);
    //     }
    //     // if there are multiple items to add to the begining of the shelf add them
    //     if (newItems && newItems.length > 0) {
    //         // push all items on the shelf by 1 to allow to push new items at the begining of the shelf
    //         for (let i = 0; i < shelf.items.length; i++) {
    //             let itemDist = 0;
    //             if (shelf.items[i].placement.distFromStart != undefined)
    //                 itemDist = parseInt(JSON.stringify(shelf.items[i].placement.distFromStart));
    //             // if (shlfStart) itemDist += shlfStart;
    //             // shelf.items[i].placement.distFromStart = itemDist + newItems.length;
    //             shelf.items[i].placement.distFromStart = itemDist + 1;
    //         }
    //         console.log('1. newItems', JSON.parse(JSON.stringify(shelf)));
    //         // add the new items at the begining of the shelf items array
    //         for (let i = newItems.length - 1; i >= 0; i--) {
    //             if (newItems[i]) shelf.items.unshift(newItems[i]);
    //         }
    //         console.log('2. newItems', JSON.parse(JSON.stringify(shelf)));
    //         // go over all the items on the shelf and re-do their distance from start of shelf
    //         for (let i = 0; i < shelf.items.length; i++) {
    //             if (shelf.items[i]) {
    //                 let distToNextItem = 0;
    //                 let itemEndPoint = parseInt(JSON.stringify(shelf.items[i].placement.distFromStart)) + (shelf.items[i].placement.faces * parseInt(JSON.stringify(shelf.items[i].placement.pWidth)));
    //                 if (shelf.items[i + 1] && (i + 1 <= newItems.length || i > newItems.length)) distToNextItem = parseInt(JSON.stringify(shelf.items[i + 1].placement.distFromStart)) - itemEndPoint;
    //                 if (i === 0 && shlfStart != undefined) {
    //                     shelf.items[0].placement.distFromStart = shlfStart;
    //                     itemEndPoint += shlfStart;
    //                 }
    //                 if (shelf.items[i + 1]) shelf.items[i + 1].placement.distFromStart = itemEndPoint + (distToNextItem > 0 ? distToNextItem : 0);
    //             }
    //         }
    //         console.log('3. newItems', JSON.parse(JSON.stringify(shelf)));
    //     }
    //     console.log('original shelf after adding new items', JSON.parse(JSON.stringify(shelf)));

    //     /* Barak 26.11.20 - item distFromTop on a freezer shelf */
    //     if (shelf.freezer === 1 || shelf.freezer === 2) {
    //         // /* Barak 22.11.20 */ await this.saveSnapShot(shelf);
    //         /* Barak 22.11.20 */await this.saveAisleShelf(shelf);
    //         return;
    //     }
    //     // console.log('if shelf.freezer === 1 || shelf.freezer === 2 -> after return');
    //     /********************************************************/

    //     // save initial state of shelf with items ordered by distFromStart (distance from start of shelf)
    //     shelf.items.sort(function (a: any, b: any) {
    //         let aNumber = a.placement.distFromStart ? a.placement.distFromStart : 0;
    //         let bNumber = b.placement.distFromStart ? b.placement.distFromStart : 0;
    //         let aId = a.Id;
    //         let bId = b.Id;
    //         return (aNumber > bNumber) ? 1 : (aNumber < bNumber) ? -1 : ((aId > bId) ? 1 : (aId < bId) ? -1 : 0);
    //     });
    //     console.log('original shelf after sorting by distFromStart', JSON.parse(JSON.stringify(shelf)));

    //     // go over shelf and if there are 2 items occupying the same space and they are the same product 
    //     // - increase faces to the first and remove the second
    //     for (let i = 0; i < shelf.items.length; i++) {
    //         if (shelf.items[i]) {
    //             if (parseInt(JSON.stringify(shelf.items[i].placement.distFromStart)) < shelf.distFromStart)
    //                 shelf.items[i].placement.distFromStart = shelf.distFromStart;
    //             if (shelf.items[i + 1] && (shelf.items[i + 1].shelfLevel === 1 || shelf.items[i + 1].shelfLevel === undefined)) {
    //                 console.log('check item a', shelf.items[i], 'vs item a+1', shelf.items[i + 1]);
    //                 let first_dist: number = parseInt(JSON.stringify(shelf.items[i].placement.distFromStart));
    //                 let first_end = first_dist + (parseInt(JSON.stringify(shelf.items[i].placement.pWidth)) * shelf.items[i].placement.faces);
    //                 let second_dist: number = parseInt(JSON.stringify(shelf.items[i + 1].placement.distFromStart));
    //                 let second_end = second_dist + (parseInt(JSON.stringify(shelf.items[i + 1].placement.pWidth)) * shelf.items[i + 1].placement.faces);
    //                 if (((second_dist <= first_dist && second_end <= first_end) || (second_dist >= first_dist && second_end <= first_end) || (second_dist >= first_dist && second_end > first_end))
    //                     && shelf.items[i].product === shelf.items[i + 1].product) {
    //                     // same item - increase faces
    //                     shelf.items[i].placement.faces += shelf.items[i + 1].placement.faces;
    //                     // remove second item 
    //                     shelf.items.splice(i + 1, 1);
    //                 }
    //             }
    //         }
    //     }
    //     console.log('c', JSON.parse(JSON.stringify(shelf)));

    //     let viewShelf = JSON.parse(JSON.stringify(shelf));
    //     // empty all items from current shelf
    //     shelf.items = [];
    //     console.log('viewShelf initial', viewShelf, 'shelf without items', JSON.parse(JSON.stringify(shelf)));

    //     let distFromStart = viewShelf.items.length > 0 && viewShelf.items[0] ? viewShelf.items[0].placement.distFromStart : 0;
    //     let distInShelf = viewShelf.items.length > 0 && viewShelf.items[0] ? viewShelf.items[0].placement.distFromStart : 0;
    //     let index = 0;
    //     let startOfShelfId = viewShelf.id;
    //     let spaceInShelf = 0;
    //     // go over the initial state and populate each shelf in combinedShelves with the correct items in the correct order
    //     for (let n = 0; n < viewShelf.items.length; n++) {
    //         let item = JSON.parse(JSON.stringify(viewShelf.items[n]));
    //         /* Barak 26.11.20 - return items above main line (item index >= 100) back into the shelf without altering anything else */
    //         let itemInd = parseInt(item.id.substring(item.id.indexOf('I') + 1));
    //         if (itemInd >= 100) {
    //             console.log('itemInd >= 100', item, 'shelf', shelf);
    //             shelf.items.push(item);
    //         }
    //         else {
    //             /************************************************************************************************************************/
    //             let nextItem = null;
    //             if (viewShelf.items[n + 1]) nextItem = viewShelf.items[n + 1];
    //             // console.log('item', item, 'itemExists', itemExists, 'nextItem', nextItem);
    //             if (spaceInShelf < 0) {
    //                 // if there is a next shelf activate function with the rest of the items 
    //                 let fullShelf = JSON.parse(JSON.stringify(shelf));
    //                 // this is the data for the actual shelf
    //                 let shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[shelf.id]));

    //                 // get the full combined shelves
    //                 while (shelfDetails.main_shelf) {
    //                     let ids = JSON.parse(JSON.stringify(shelfDetails.main_shelf));
    //                     shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[ids]));
    //                     fullShelf = aisleShelves[ids];
    //                 }
    //                 let { combined } = shelfDetails;
    //                 if (combined == null) combined = [];
    //                 let combinedShelves = [fullShelf].concat(combined.map((id: any) => aisleShelves[id]));
    //                 // find the index of the current shelf 
    //                 let currI = combinedShelves.findIndex(object => object.id === viewShelf.id);
    //                 if (currI >= 0 && combinedShelves[currI + 1]) {
    //                     while (combinedShelves[currI + 1] && spaceInShelf * -1 > combinedShelves[currI + 1].dimensions.width) {
    //                         spaceInShelf += combinedShelves[currI + 1].dimensions.width;
    //                         console.log('call addShelfItem for shelf', JSON.parse(JSON.stringify(combinedShelves[currI + 1])), 'changing start to', combinedShelves[currI + 1].dimensions.width);
    //                         await this.addShelfItem(JSON.parse(JSON.stringify(combinedShelves[currI + 1])), blankItem, combinedShelves[currI + 1].dimensions.width, []);
    //                         currI++;
    //                     }
    //                     let newItemsToSend: PlanogramItem[] = [];
    //                     if (n < viewShelf.items.length) {
    //                         // get all items left over into a new array
    //                         for (let i = n; i < viewShelf.items.length; i++) {
    //                             newItemsToSend.push(viewShelf.items[i]);
    //                         }
    //                         // // remove the items we're passing to the next shelf from the array of this shelf
    //                         // viewShelf.items.splice(n, viewShelf.items.length - n);
    //                     }
    //                     console.log('call addShelfItem for shelf', JSON.parse(JSON.stringify(combinedShelves[currI + 1])), 'changing start to', spaceInShelf * -1, 'new items', newItemsToSend);
    //                     await this.addShelfItem(JSON.parse(JSON.stringify(combinedShelves[currI + 1])), blankItem, (spaceInShelf * -1), newItemsToSend);
    //                     console.log('after call addShelfItem');
    //                     spaceInShelf = 0;
    //                     // leave the loop since there are no more items in this shelf to take care of  
    //                     //  - all the left over items are moved to the next shelf
    //                     break;
    //                 }
    //             }
    //             if (item.placement.distFromStart > distFromStart && (startOfShelfId === item.id.substring(0, item.id.indexOf('I')) || item.id === ''))
    //                 distFromStart = item.placement.distFromStart;
    //             item.id = startOfShelfId + 'I' + index;
    //             index++;
    //             item.placement.distFromStart = distFromStart;
    //             let itemWidth = viewShelf.items[n].placement.pWidth;
    //             let itemSpace = (itemWidth != undefined ? itemWidth : ProductDefaultDimensions.width) * item.placement.faces;
    //             distFromStart += itemSpace;
    //             distInShelf += itemSpace;
    //             spaceInShelf = viewShelf.dimensions.width - distInShelf;
    //             // add item back into the shelf
    //             shelf.items.push(item);


    //             // check if there is a next shelf 
    //             let fullShelf = JSON.parse(JSON.stringify(shelf));
    //             // this is the data for the actual shelf
    //             let shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[shelf.id]));
    //             // get the full combined shelves
    //             while (shelfDetails.main_shelf) {
    //                 let ids = JSON.parse(JSON.stringify(shelfDetails.main_shelf));
    //                 shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[ids]));
    //                 fullShelf = aisleShelves[ids];
    //             }
    //             let { combined } = shelfDetails;
    //             if (combined == null) combined = [];
    //             let combinedShelves = [fullShelf].concat(combined.map((id: any) => aisleShelves[id]));
    //             // find the index of the current shelf 
    //             let currI = combinedShelves.findIndex(object => object.id === viewShelf.id);


    //             if (distFromStart > viewShelf.dimensions.width && currI >= 0 && combinedShelves[currI + 1]) {
    //                 // only of there is a next shelf can we reduce the viewShelf.dimensions.width from the next distFromStart 
    //                 spaceInShelf = (distFromStart - viewShelf.dimensions.width) * -1;
    //                 distFromStart = distFromStart - viewShelf.dimensions.width;
    //             }
    //             console.log('--- item', item, 'distInShelf', distInShelf, 'distFromStart next', distFromStart, 'spaceInShelf', spaceInShelf, "shelf", JSON.parse(JSON.stringify(shelf)));
    //         }
    //     }
    //     // in case the last item on the shelf is the one that extends into the next shelf, 
    //     // after we're done going over all items we check if the spaceInShelf has extended byond the current shelf
    //     if (spaceInShelf < 0) {
    //         console.log('last item on shelf extends beyond this shelf. spaceInShelf', spaceInShelf);
    //         // if there is a next shelf activate function to update the next shelf's distFromStart 
    //         let fullShelf = JSON.parse(JSON.stringify(shelf));
    //         // this is the data for the actual shelf
    //         let shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[shelf.id]));

    //         // get the full combined shelves
    //         while (shelfDetails.main_shelf) {
    //             let ids = JSON.parse(JSON.stringify(shelfDetails.main_shelf));
    //             shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[ids]));
    //             fullShelf = aisleShelves[ids];
    //         }
    //         let { combined } = shelfDetails;
    //         if (combined == null) combined = [];
    //         let combinedShelves = [fullShelf].concat(combined.map((id: any) => aisleShelves[id]));
    //         // find the index of the current shelf 
    //         let currI = combinedShelves.findIndex(object => object.id === viewShelf.id);
    //         if (currI >= 0 && combinedShelves[currI + 1]) {
    //             while (combinedShelves[currI + 1] && spaceInShelf * -1 > combinedShelves[currI + 1].dimensions.width) {
    //                 spaceInShelf += combinedShelves[currI + 1].dimensions.width;
    //                 console.log('call addShelfItem for shelf', JSON.parse(JSON.stringify(combinedShelves[currI + 1])), 'changing start to', combinedShelves[currI + 1].dimensions.width);
    //                 await this.addShelfItem(JSON.parse(JSON.stringify(combinedShelves[currI + 1])), blankItem, combinedShelves[currI + 1].dimensions.width, []);
    //                 currI++;
    //             }
    //             let newItemsToSend: PlanogramItem[] = [];
    //             console.log('call addShelfItem for shelf', JSON.parse(JSON.stringify(combinedShelves[currI + 1])), 'changing start to', spaceInShelf * -1, 'new items', newItemsToSend);
    //             await this.addShelfItem(JSON.parse(JSON.stringify(combinedShelves[currI + 1])), blankItem, (spaceInShelf * -1), newItemsToSend);
    //             console.log('after call addShelfItem');
    //         }
    //     }

    //     /* Barak 22.11.20 - Add saving array for aisle, allowing to do 
    //         Ctrl+Z to move back and Ctrl+Y to move forward *
    //         await this.saveSnapShot(shelf);
    //     *************************************************************/
    //     console.log('addShelfItem end: before saveAisleShelf', JSON.parse(JSON.stringify(shelf)));
    //     /* Barak 22.11.20 */ await this.saveAisleShelf(JSON.parse(JSON.stringify(shelf)));
    // }
   addShelfItem = async (shelf: PlanogramShelf, newItem: PlanogramItem, shlfStart?: number, newItems?: PlanogramItem[]) => {
    shelf.distFromStart = 0;  
    let distFromStartInit = localStorage.getItem('distFromStart');
        var din = 0;
        if (distFromStartInit) din = parseInt(distFromStartInit);
        // alert(din)
        console.log('addShelfItem section', JSON.parse(JSON.stringify(shelf)), newItem);
        // make sure shelf has no undefined items
        let initItems = JSON.parse(JSON.stringify(shelf.items));
        shelf.items = [];
        for (let i = 0; i < initItems.length; i++) {
            if (initItems[i] && typeof initItems[i] != 'undefined')
                shelf.items.push(initItems[i]);
        }
        if (newItems) {
            initItems = JSON.parse(JSON.stringify(newItems));
            newItems = [];
            for (let i = 0; i < initItems.length; i++) {
                if (initItems[i] && typeof initItems[i] != 'undefined') {
                    initItems[i].placement.distFromStart = i;
                    newItems.push(initItems[i]);
                }
            }
        }
        console.log('addShelfItem section. shelf', JSON.parse(JSON.stringify(shelf)), 'new item', newItem, 'shlfStart', shlfStart, 'new items', newItems);
        const { virtualStore } = this.props;
        const shelvesDetails = virtualStore.shelfDetails;
        const aisleShelves = virtualStore.shelfMap;

        // update shelf.distFromStart if shlfStart was sent
        if (shlfStart && shlfStart >= 0) {
            shelf.distFromStart = shlfStart;
            /***************************/
            let aisles_1 = this.props.store ? this.props.store.aisles : undefined;
            let aisleId_1 = shelf.id.substring(0, shelf.id.indexOf('SE'));
            let aisle_1: PlanogramAisle = JSON.parse(JSON.stringify(blankPlanogramAisle));
            if (aisles_1) {
                for (let i = 0; i < aisles_1.length; i++) {
                    if (aisles_1[i].id === aisleId_1) {
                        aisle_1 = aisles_1[i];
                        let seI = parseInt(shelf.id.substring(shelf.id.indexOf('SE') + 2, shelf.id.indexOf('SH')));
                        let shI = parseInt(shelf.id.substring(shelf.id.indexOf('SH') + 2));
                        if (aisle_1.sections[seI] && aisle_1.sections[seI].shelves[shI])
                            aisle_1.sections[seI].shelves[shI] = shelf;
                        break;
                    }
                }
            }
            if (aisle_1.id === aisleId_1) {
                await this.props.setStoreAisle(aisle_1);
                console.log('shelf after saving dist from start', JSON.parse(JSON.stringify(shelf)), 'aisle after saving new shelf distFromStart', JSON.parse(JSON.stringify(aisle_1)));
            }
            /***************************/
        }
        // make sure, in case no shlfStart was sent that the shelf.distFromStart fits with the first Item (ordered by distFromStart) on that shelf
        else {
            if (shelf.distFromStart > 0) {
                let num: number = shelf.items[0] && shelf.items[0].placement.distFromStart ? shelf.items[0].placement.distFromStart : 0;
                if (shelf.items.length >= 1 && num >= 0) shelf.distFromStart = num;
            }
        }

        // check if this shelf even has room to add a new item;
        if (shelf.distFromStart === shelf.dimensions.width && newItem && newItem.product > 0 && shelf.freezer != 1 && shelf.freezer != 2 && (newItem.shelfLevel === 1 || newItem.shelfLevel === undefined)) {
            // if there is a next shelf activate function with the rest of the items 
            let fullShelf = JSON.parse(JSON.stringify(shelf));
            // this is the data for the actual shelf
            let shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[shelf.id]));

            // get the full combined shelves
            while (shelfDetails.main_shelf) {
                let ids = JSON.parse(JSON.stringify(shelfDetails.main_shelf));
                shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[ids]));
                fullShelf = aisleShelves[ids];
            }
            let { combined } = shelfDetails;
            if (combined == null) combined = [];
            let combinedShelves = [fullShelf].concat(combined.map((id: any) => aisleShelves[id]));
            // find the index of the current shelf 
            let currI = combinedShelves.findIndex(object => object.id === shelf.id);
            if (currI >= 0 && combinedShelves[currI + 1]) {
                // move item/s to the next shelf
                console.log('call addShelfItem for shelf', JSON.parse(JSON.stringify(combinedShelves[currI + 1])), 'add new item', newItem);
                await this.addShelfItem(JSON.parse(JSON.stringify(combinedShelves[currI + 1])), newItem);
            }
            return;
        }
        // if there is a new item add it at the end of the shelf
        if (newItem && newItem.product > 0) {
            /* Barak 26.11.20 - add special numbering for items in shelf.freezer === 2 */
            if (shelf.freezer === 2
                /* && newItem.placement.distFromTop != shelf.dimensions.height - ((newItem.placement.pHeight ? newItem.placement.pHeight : ProductDefaultDimensions.height) * newItem.placement.stack) */
            ) {
                if (newItem.id === '') {
                    console.log('newItem.id === ""');
                    if (shelf.items.length > 1) {
                        let lastI = parseInt(shelf.items[shelf.items.length - 1].id.substring(shelf.items[shelf.items.length - 1].id.indexOf('I') + 1));
                        let newI = 0;
                        if (lastI < 100) newI = lastI + 100;
                        else newI = lastI + 1;
                        newItem.id = shelf.id + "I" + newI;
                        console.log('1. newItem.id = ', newItem.id);
                    } else {
                        newItem.id = shelf.id + "I" + 100;
                        console.log('2. newItem.id = ', newItem.id);
                    }
                }
                console.log('addShelfItem freezer === 2 newItem', newItem);
            }
            /***************************************************************************/
            /* Barak 21.12.20 - add special numbering for items in shelfLevel > 1 */
            if (newItem.shelfLevel > 1) {
                if (newItem.id === '') {
                    console.log('newItem.id === ""');
                    if (shelf.items.length > 1) {
                        let lastI = parseInt(shelf.items[shelf.items.length - 1].id.substring(shelf.items[shelf.items.length - 1].id.indexOf('I') + 1));
                        let newI = 0;
                        if (lastI < 100) newI = lastI + 100;
                        else newI = lastI + 1;
                        newItem.id = shelf.id + "I" + newI;
                        console.log('1. newItem.id = ', newItem.id);
                    } else {
                        newItem.id = shelf.id + "I" + 100;
                        console.log('2. newItem.id = ', newItem.id);
                    }
                }
                console.log('addShelfItem shelfLevel > 1 newItem', newItem);
                // now that we have an id to the new item we can save it in the linked bottom level item as linkedItemUp
                let sItem = shelf.items.filter(line => line.id === this.props.multiSelectId[0]);
                if (sItem && sItem[0]) sItem[0].linkedItemUp = newItem.id;
            }
            /************************************************************************/
            shelf.items.push(newItem);
        }
        // if there are multiple items to add to the begining of the shelf add them
        if (newItems && newItems.length > 0) {
            // push all items on the shelf by 1 to allow to push new items at the begining of the shelf
            for (let i = 0; i < shelf.items.length; i++) {
                let itemDist = 0;
                if (shelf.items[i].placement.distFromStart != undefined)
                    itemDist = parseInt(JSON.stringify(shelf.items[i].placement.distFromStart));
                // if (shlfStart) itemDist += shlfStart;
                // shelf.items[i].placement.distFromStart = itemDist + newItems.length;
                shelf.items[i].placement.distFromStart = itemDist + 1;
            }
            console.log('1. newItems', JSON.parse(JSON.stringify(shelf)));
            // add the new items at the begining of the shelf items array
            for (let i = newItems.length - 1; i >= 0; i--) {
                if (newItems[i]) shelf.items.unshift(newItems[i]);
            }
            console.log('2. newItems', JSON.parse(JSON.stringify(shelf)));
            // go over all the items on the shelf and re-do their distance from start of shelf
            for (let i = 0; i < shelf.items.length; i++) {
                if (shelf.items[i]) {
                    let distToNextItem = 0;
                    let itemEndPoint = parseInt(JSON.stringify(shelf.items[i].placement.distFromStart)) + (shelf.items[i].placement.faces * parseInt(JSON.stringify(shelf.items[i].placement.pWidth)));
                    if (shelf.items[i + 1] && (i + 1 <= newItems.length || i > newItems.length)) distToNextItem = parseInt(JSON.stringify(shelf.items[i + 1].placement.distFromStart)) - itemEndPoint;
                    if (i === 0 && shlfStart != undefined) {
                        shelf.items[0].placement.distFromStart = shlfStart;
                        itemEndPoint += shlfStart;
                    }
                    if (shelf.items[i + 1]) shelf.items[i + 1].placement.distFromStart = itemEndPoint + (distToNextItem > 0 ? distToNextItem : 0);
                }
            }
            console.log('3. newItems', JSON.parse(JSON.stringify(shelf)));
        }
        console.log('original shelf after adding new items', JSON.parse(JSON.stringify(shelf)));

        /* Barak 26.11.20 - item distFromTop on a freezer shelf */
        if (shelf.freezer === 1 || shelf.freezer === 2) {
            // /* Barak 22.11.20 */ await this.saveSnapShot(shelf);
            /* Barak 22.11.20 */await this.saveAisleShelf(shelf);
            return;
        }
        // console.log('if shelf.freezer === 1 || shelf.freezer === 2 -> after return');
        /********************************************************/

        // save initial state of shelf with items ordered by distFromStart (distance from start of shelf)
        shelf.items.sort(function (a: any, b: any) {
            let aNumber = a.placement.distFromStart ? a.placement.distFromStart : 0;
            let bNumber = b.placement.distFromStart ? b.placement.distFromStart : 0;
            let aId = a.Id;
            let bId = b.Id;
            return (aNumber > bNumber) ? 1 : (aNumber < bNumber) ? -1 : ((aId > bId) ? 1 : (aId < bId) ? -1 : 0);
        });
        console.log('original shelf after sorting by distFromStart', JSON.parse(JSON.stringify(shelf)));

        // go over shelf and if there are 2 items occupying the same space and they are the same product 
        // - increase faces to the first and remove the second
        for (let i = 0; i < shelf.items.length; i++) {
            if (shelf.items[i]) {
                if (parseInt(JSON.stringify(shelf.items[i].placement.distFromStart)) < shelf.distFromStart)
                    shelf.items[i].placement.distFromStart = shelf.distFromStart;
                if (shelf.items[i + 1] && (shelf.items[i + 1].shelfLevel === 1 || shelf.items[i + 1].shelfLevel === undefined)) {
                    console.log('check item a', shelf.items[i], 'vs item a+1', shelf.items[i + 1]);
                    let first_dist: number = parseInt(JSON.stringify(shelf.items[i].placement.distFromStart));
                    let first_end = first_dist + (parseInt(JSON.stringify(shelf.items[i].placement.pWidth)) * shelf.items[i].placement.faces);
                    let second_dist: number = parseInt(JSON.stringify(shelf.items[i + 1].placement.distFromStart));
                    let second_end = second_dist + (parseInt(JSON.stringify(shelf.items[i + 1].placement.pWidth)) * shelf.items[i + 1].placement.faces);
                    if (((second_dist <= first_dist && second_end <= first_end) || (second_dist >= first_dist && second_end <= first_end) || (second_dist >= first_dist && second_end > first_end))
                        && shelf.items[i].product === shelf.items[i + 1].product) {
                        // same item - increase faces
                        shelf.items[i].placement.faces += shelf.items[i + 1].placement.faces;
                        // remove second item 
                        shelf.items.splice(i + 1, 1);
                    }
                }
            }
        }
        console.log('c', JSON.parse(JSON.stringify(shelf)));

        let viewShelf = JSON.parse(JSON.stringify(shelf));
        // empty all items from current shelf
        shelf.items = [];
        console.log('viewShelf initial', viewShelf, 'shelf without items', JSON.parse(JSON.stringify(shelf)));

        let distFromStart = viewShelf.items.length > 0 && viewShelf.items[0] ? viewShelf.items[0].placement.distFromStart : 0;
        let distInShelf = viewShelf.items.length > 0 && viewShelf.items[0] ? viewShelf.items[0].placement.distFromStart : 0;
        let index = 0;
        let startOfShelfId = viewShelf.id;
        let spaceInShelf = 0;
        // go over the initial state and populate each shelf in combinedShelves with the correct items in the correct order
        for (let n = 0; n < viewShelf.items.length; n++) {
            let item = JSON.parse(JSON.stringify(viewShelf.items[n]));
            /* Barak 26.11.20 - return items above main line (item index >= 100) back into the shelf without altering anything else */
            let itemInd = parseInt(item.id.substring(item.id.indexOf('I') + 1));
            if (itemInd >= 100) {
                console.log('itemInd >= 100', item, 'shelf', shelf);
                shelf.items.push(item);
            }
            else {
                /************************************************************************************************************************/
                let nextItem = null;
                if (viewShelf.items[n + 1]) nextItem = viewShelf.items[n + 1];
                // console.log('item', item, 'itemExists', itemExists, 'nextItem', nextItem);
                if (spaceInShelf < 0) {
                    // if there is a next shelf activate function with the rest of the items 
                    let fullShelf = JSON.parse(JSON.stringify(shelf));
                    // this is the data for the actual shelf
                    let shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[shelf.id]));

                    // get the full combined shelves
                    while (shelfDetails.main_shelf) {
                        let ids = JSON.parse(JSON.stringify(shelfDetails.main_shelf));
                        shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[ids]));
                        fullShelf = aisleShelves[ids];
                    }
                    let { combined } = shelfDetails;
                    if (combined == null) combined = [];
                    let combinedShelves = [fullShelf].concat(combined.map((id: any) => aisleShelves[id]));
                    // find the index of the current shelf 
                    let currI = combinedShelves.findIndex(object => object.id === viewShelf.id);
                    if (currI >= 0 && combinedShelves[currI + 1]) {
                        while (combinedShelves[currI + 1] && spaceInShelf * -1 > combinedShelves[currI + 1].dimensions.width) {
                            spaceInShelf += combinedShelves[currI + 1].dimensions.width;
                            console.log('call addShelfItem for shelf', JSON.parse(JSON.stringify(combinedShelves[currI + 1])), 'changing start to', combinedShelves[currI + 1].dimensions.width);
                            await this.addShelfItem(JSON.parse(JSON.stringify(combinedShelves[currI + 1])), blankItem, combinedShelves[currI + 1].dimensions.width, []);
                            currI++;
                        }
                        let newItemsToSend: PlanogramItem[] = [];
                        if (n < viewShelf.items.length) {
                            // get all items left over into a new array
                            for (let i = n; i < viewShelf.items.length; i++) {
                                newItemsToSend.push(viewShelf.items[i]);
                            }
                            // // remove the items we're passing to the next shelf from the array of this shelf
                            // viewShelf.items.splice(n, viewShelf.items.length - n);
                        }
                        console.log('call addShelfItem for shelf', JSON.parse(JSON.stringify(combinedShelves[currI + 1])), 'changing start to', spaceInShelf * -1, 'new items', newItemsToSend);
                        await this.addShelfItem(JSON.parse(JSON.stringify(combinedShelves[currI + 1])), blankItem, (spaceInShelf * -1), newItemsToSend);
                        console.log('after call addShelfItem');
                        spaceInShelf = 0;
                        // leave the loop since there are no more items in this shelf to take care of  
                        //  - all the left over items are moved to the next shelf
                        break;
                    }
                }
                if (item.placement.distFromStart > distFromStart && (startOfShelfId === item.id.substring(0, item.id.indexOf('I')) || item.id === ''))
                    distFromStart = item.placement.distFromStart;
                item.id = startOfShelfId + 'I' + index;
                index++;
                item.placement.distFromStart = distFromStart;
                let itemWidth = viewShelf.items[n].placement.pWidth;
                let itemSpace = (itemWidth != undefined ? itemWidth : ProductDefaultDimensions.width) * item.placement.faces;
                distFromStart += itemSpace;
                distInShelf += itemSpace;
                spaceInShelf = viewShelf.dimensions.width - distInShelf;
                // add item back into the shelf
                shelf.items.push(item);


                // check if there is a next shelf 
                let fullShelf = JSON.parse(JSON.stringify(shelf));
                // this is the data for the actual shelf
                let shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[shelf.id]));
                // get the full combined shelves
                while (shelfDetails.main_shelf) {
                    let ids = JSON.parse(JSON.stringify(shelfDetails.main_shelf));
                    shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[ids]));
                    fullShelf = aisleShelves[ids];
                }
                let { combined } = shelfDetails;
                if (combined == null) combined = [];
                let combinedShelves = [fullShelf].concat(combined.map((id: any) => aisleShelves[id]));
                // find the index of the current shelf 
                let currI = combinedShelves.findIndex(object => object.id === viewShelf.id);


                if (distFromStart > viewShelf.dimensions.width && currI >= 0 && combinedShelves[currI + 1]) {
                    // only of there is a next shelf can we reduce the viewShelf.dimensions.width from the next distFromStart 
                    spaceInShelf = (distFromStart - viewShelf.dimensions.width) * -1;
                    distFromStart = distFromStart - viewShelf.dimensions.width;
                }
                console.log('--- item', item, 'distInShelf', distInShelf, 'distFromStart next', distFromStart, 'spaceInShelf', spaceInShelf, "shelf", JSON.parse(JSON.stringify(shelf)));
            }
        }
        // in case the last item on the shelf is the one that extends into the next shelf, 
        // after we're done going over all items we check if the spaceInShelf has extended byond the current shelf
        if (spaceInShelf < 0) {
            console.log('last item on shelf extends beyond this shelf. spaceInShelf', spaceInShelf);
            // if there is a next shelf activate function to update the next shelf's distFromStart 
            let fullShelf = JSON.parse(JSON.stringify(shelf));
            // this is the data for the actual shelf
            let shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[shelf.id]));

            // get the full combined shelves
            while (shelfDetails.main_shelf) {
                let ids = JSON.parse(JSON.stringify(shelfDetails.main_shelf));
                shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[ids]));
                fullShelf = aisleShelves[ids];
            }
            let { combined } = shelfDetails;
            if (combined == null) combined = [];
            let combinedShelves = [fullShelf].concat(combined.map((id: any) => aisleShelves[id]));
            // find the index of the current shelf 
            let currI = combinedShelves.findIndex(object => object.id === viewShelf.id);
            if (currI >= 0 && combinedShelves[currI + 1]) {
                while (combinedShelves[currI + 1] && spaceInShelf * -1 > combinedShelves[currI + 1].dimensions.width) {
                    spaceInShelf += combinedShelves[currI + 1].dimensions.width;
                    console.log('call addShelfItem for shelf', JSON.parse(JSON.stringify(combinedShelves[currI + 1])), 'changing start to', combinedShelves[currI + 1].dimensions.width);
                    await this.addShelfItem(JSON.parse(JSON.stringify(combinedShelves[currI + 1])), blankItem, combinedShelves[currI + 1].dimensions.width, []);
                    currI++;
                }
                let newItemsToSend: PlanogramItem[] = [];
                console.log('call addShelfItem for shelf', JSON.parse(JSON.stringify(combinedShelves[currI + 1])), 'changing start to', spaceInShelf * -1, 'new items', newItemsToSend);
                await this.addShelfItem(JSON.parse(JSON.stringify(combinedShelves[currI + 1])), blankItem, (spaceInShelf * -1), newItemsToSend);
                console.log('after call addShelfItem');
            }
        }

        /* Barak 22.11.20 - Add saving array for aisle, allowing to do 
            Ctrl+Z to move back and Ctrl+Y to move forward *
            await this.saveSnapShot(shelf);
        *************************************************************/
        console.log('addShelfItem end: before saveAisleShelf', JSON.parse(JSON.stringify(shelf)));
        /* Barak 22.11.20 */ await this.saveAisleShelf(JSON.parse(JSON.stringify(shelf)));
    }
 
    createFreezerBoxArr = (shelf: PlanogramShelf) => {
        // create a 2d array corresponding to the freezer
        let boxArr: any[] = [];
        for (let i = 0; i <= shelf.dimensions.width; i++) {
            boxArr[i] = [];
            for (let a = 0; a <= shelf.dimensions.height; a++) {
                boxArr[i][a] = { value: 0, id: '' };
            }
        }
        // console.log('initial boxArr', boxArr);

        // at the loaction of each item place 1's in the corresponding coordinates
        for (let i = 0; i < shelf.items.length; i++) {
            let item = shelf.items[i];
            if (item) {
                let startX = item.placement.distFromStart ? item.placement.distFromStart : 0;
                let itemTotalWidth = startX + ((item.placement.pWidth ? item.placement.pWidth : ProductDefaultDimensions.width) * item.placement.faces);
                let startY = item.placement.distFromTop ? item.placement.distFromTop : 0;
                let itemTotalHeight = startY + ((item.placement.pHeight ? item.placement.pHeight : ProductDefaultDimensions.height) * item.placement.stack);
                for (let x = startX; x <= itemTotalWidth; x++) {
                    for (let y = startY; y <= itemTotalHeight; y++) {
                        if (boxArr[x] && boxArr[x][y])
                            boxArr[x][y] = { value: 1, id: item.id };
                    }
                }
            }
        }

        return boxArr;
    }
    alignFreezerToLeft = async (shelf: PlanogramShelf) => {
        let items_to_align = [];
        // get all items to align from this.props.multiSelectId
        if (this.props.multiSelectId.length > 0) {
            for (let i = 0; i < this.props.multiSelectId.length; i++) {
                // if the item is in this freezer - set it's dist from start to 0
                let itemLocation = this.props.multiSelectId[i].substring(0, this.props.multiSelectId[i].indexOf('I'));
                if (itemLocation === shelf.id) {
                    let index = shelf.items.findIndex(line => line.id === this.props.multiSelectId[i]);
                    if (index >= 0) {
                        items_to_align.push(shelf.items[index]);
                    }
                }
            }
        } else items_to_align = shelf.items;

        // sort items_to_align by distFromStart
        items_to_align.sort(function (a: any, b: any) {
            let aNumber = a.placement.distFromStart ? a.placement.distFromStart : 0;
            let bNumber = b.placement.distFromStart ? b.placement.distFromStart : 0;
            return (aNumber > bNumber) ? 1 : (aNumber < bNumber) ? -1 : 0;
        });

        // go over items_to_align and align to left
        for (let i = 0; i < items_to_align.length; i++) {
            // create a 2d array corresponding to the freezer
            let boxArr = this.createFreezerBoxArr(shelf);
            items_to_align[i].placement.distFromStart = this.checkFreezerBoxLeft(boxArr, items_to_align[i]);
        }

        //this.saveSnapShot(shelf);
        this.saveAisleShelf(shelf);
    }
    saveSnapShot = async (shelf: PlanogramShelf) => {
        let tArray = this.props.aisleSaveArray;
        let tIndex = this.props.aisleSaveIndex;
        if (tArray.length === 0 || (tArray.length > 0 && tArray[0].id != shelf.id.substring(0, shelf.id.indexOf('SE')))) {
            let newAisle = await planogramProvider.getStoreAisle(this.props.store ? this.props.store.store_id : 0, this.props.aisleId != undefined ? this.props.aisleId : 0);
            tArray = [JSON.parse(JSON.stringify(newAisle))];
            tIndex = 0;
            await this.props.setAisleSaveArray(tArray);
            await this.props.setAisleSaveIndex(tIndex);
        }
        let aisle: PlanogramAisle = JSON.parse(JSON.stringify(blankPlanogramAisle));
        let aisles = this.props.store ? this.props.store.aisles : undefined;
        let aisleId = shelf.id.substring(0, shelf.id.indexOf('SE'));
        if (aisles) {
            for (let i = 0; i < aisles.length; i++) {
                if (aisles[i].id === aisleId) {
                    aisle = aisles[i];
                    break;
                }
            }
        }
        if (aisle.id === aisleId) {
            tIndex++;
            // insert into array tArray item aisle at location tIndex, deleting 0 items first
            tArray.splice(tIndex, 0, JSON.parse(JSON.stringify(aisle)));
            if (tArray.length > tIndex + 1) {
                tArray.length = tIndex + 1;
            }
            await this.props.setAisleSaveArray(tArray);
            await this.props.setAisleSaveIndex(tIndex);
            let sectionInd = parseInt(shelf.id.substring(shelf.id.indexOf('SE') + 2, shelf.id.indexOf('SH')));
            let shelfInd = parseInt(shelf.id.substring(shelf.id.indexOf('SH') + 2));
            aisle.sections[sectionInd].shelves[shelfInd] = shelf;
            await this.props.setStoreAisle(aisle);
        }
    }
    saveAisleShelf = async (shelf: PlanogramShelf) => {
        let aisle: PlanogramAisle = JSON.parse(JSON.stringify(blankPlanogramAisle));
        let aisles = this.props.store ? this.props.store.aisles : undefined;
        let aisleId = shelf.id.substring(0, shelf.id.indexOf('SE'));
        if (aisles) {
            for (let i = 0; i < aisles.length; i++) {
                if (aisles[i].id === aisleId) {
                    aisle = aisles[i];
                    let seI = parseInt(shelf.id.substring(shelf.id.indexOf('SE') + 2, shelf.id.indexOf('SH')));
                    let shI = parseInt(shelf.id.substring(shelf.id.indexOf('SH') + 2));
                    if (aisle.sections[seI] && aisle.sections[seI].shelves[shI]) {
                        aisle.sections[seI].shelves[shI] = shelf;
                    }
                    break;
                }
            }
        }
        if (aisle.id === aisleId) {
            await this.props.setStoreAisle(aisle);
            await this.checkLevel2(aisle);
        }
    }
    checkLevel2 = async (aisle: PlanogramAisle) => {
        let combinedItems: any[] = [];
        for (let a = 0; a < aisle.sections.length; a++) {
            for (let b = 0; b < aisle.sections[a].shelves.length; b++) {
                combinedItems = combinedItems.concat(aisle.sections[a].shelves[b].items);
            }
        }
        for (let c = 0; c < combinedItems.length; c++) {
            if (combinedItems[c].linkedItemUp) {
                let upItem = combinedItems.filter((line: any) => line.id === combinedItems[c].linkedItemUp);
                if (upItem && upItem[0]) {
                    let itemToMove = JSON.parse(JSON.stringify(upItem[0]));
                    // find location of item to move
                    let moveSec = parseInt(itemToMove.id.substring(itemToMove.id.indexOf('SE') + 2, itemToMove.id.indexOf('SH')));
                    let moveSh = parseInt(itemToMove.id.substring(itemToMove.id.indexOf('SH') + 2, itemToMove.id.indexOf('I')));
                    let index = aisle.sections[moveSec].shelves[moveSh].items.findIndex(line => line.id === itemToMove.id);
                    // remove this item from original shelf
                    aisle.sections[moveSec].shelves[moveSh].items.splice(index, 1);
                    // update itemToMove properties
                    itemToMove.placement.distFromStart = combinedItems[c].placement.distFromStart;
                    itemToMove.linkedItemDown = combinedItems[c].id;
                    itemToMove.id = ''
                    // find new location of item and create new item id
                    let destinationSec = parseInt(combinedItems[c].id.substring(combinedItems[c].id.indexOf('SE') + 2, combinedItems[c].id.indexOf('SH')));
                    let destinationSh = parseInt(combinedItems[c].id.substring(combinedItems[c].id.indexOf('SH') + 2, combinedItems[c].id.indexOf('I')));
                    if (aisle.sections[destinationSec].shelves[destinationSh].items.length > 1) {
                        let lastI = parseInt(aisle.sections[destinationSec].shelves[destinationSh].items[aisle.sections[destinationSec].shelves[destinationSh].items.length - 1].id.substring(aisle.sections[destinationSec].shelves[destinationSh].items[aisle.sections[destinationSec].shelves[destinationSh].items.length - 1].id.indexOf('I') + 1));
                        let newI = 0;
                        if (lastI < 100) newI = lastI + 100;
                        else newI = lastI + 1;
                        itemToMove.id = aisle.sections[destinationSec].shelves[destinationSh].id + "I" + newI;
                    } else {
                        itemToMove.id = aisle.sections[destinationSec].shelves[destinationSh].id + "I" + 100;
                    }
                    // add itemToMove to its new location
                    aisle.sections[destinationSec].shelves[destinationSh].items.push(itemToMove);
                    // update the linkedItemUp with the new item id 
                    combinedItems[c].linkedItemUp = itemToMove.id;
                }
                else {
                    combinedItems[c].linkedItemUp = null;
                }
            }
        }
        await this.props.setStoreAisle(aisle);
    }
    saveSnapShotDelete = async (shelf: PlanogramShelf) => {
        let tArray = this.props.aisleSaveArray;
        let tIndex = this.props.aisleSaveIndex;
        if (tArray.length === 0 || (tArray.length > 0 && tArray[0].id != shelf.id.substring(0, shelf.id.indexOf('SE')))) {
            let newAisle = await planogramProvider.getStoreAisle(this.props.store ? this.props.store.store_id : 0, this.props.aisleId != undefined ? this.props.aisleId : 0);
            tArray = [JSON.parse(JSON.stringify(newAisle))];
            tIndex = 0;
            await this.props.setAisleSaveArray(tArray);
            await this.props.setAisleSaveIndex(tIndex);
        }
        let aisle: PlanogramAisle = JSON.parse(JSON.stringify(blankPlanogramAisle));
        let aisles = this.props.store ? this.props.store.aisles : undefined;
        let aisleId = shelf.id.substring(0, shelf.id.indexOf('SE'));
        if (aisles) {
            for (let i = 0; i < aisles.length; i++) {
                if (aisles[i].id === aisleId) {
                    aisle = aisles[i];
                    break;
                }
            }
        }
        if (aisle.id === aisleId) {
            tIndex++;
            // insert into array tArray item aisle at location tIndex, deleting 0 items first
            tArray.splice(tIndex, 0, JSON.parse(JSON.stringify(aisle)));
            if (tArray.length > tIndex + 1) {
                tArray.length = tIndex + 1;
            }
            console.log('tArray in section', tArray, 'current index', tIndex);
            await this.props.setAisleSaveArray(tArray);
            await this.props.setAisleSaveIndex(tIndex);
            await this.props.setStoreAisle(aisle);
        }
    }
    checkFreezerBoxLeft = (boxArr: any[], item: PlanogramItem) => {
        // check backwards from item.placement.distFromStart to 0 and for a height of item.placement.distfromTop
        let startX = item.placement.distFromStart ? item.placement.distFromStart - 1 : 0;
        // let itemTotalWidth = startX + ((item.placement.pWidth ? item.placement.pWidth : ProductDefaultDimensions.width) * item.placement.faces);
        let startY = item.placement.distFromTop ? item.placement.distFromTop : 0;
        let itemTotalHeight = startY + ((item.placement.pHeight ? item.placement.pHeight : ProductDefaultDimensions.height) * item.placement.stack);
        let returnX = 0;
        for (let x = startX; x >= 0; x--) {
            for (let y = startY; y < itemTotalHeight; y++) {
                // if the check bumps into another item return this location
                if (boxArr[x] && boxArr[x][y] && boxArr[x][y].value === 1) {
                    if (x > returnX) returnX = x;
                }
            }
        }
        // check didn't encounter anything so we return 0 as the most left position available
        return returnX;
    }
    alignFreezerToDown = async (shelf: PlanogramShelf) => {
        let items_to_align = [];
        // get all items to align from this.props.multiSelectId
        if (this.props.multiSelectId.length > 0) {
            for (let i = 0; i < this.props.multiSelectId.length; i++) {
                // if the item is in this freezer - set it's dist from start to 0
                let itemLocation = this.props.multiSelectId[i].substring(0, this.props.multiSelectId[i].indexOf('I'));
                if (itemLocation === shelf.id) {
                    let index = shelf.items.findIndex(line => line.id === this.props.multiSelectId[i]);
                    if (index >= 0) {
                        items_to_align.push(shelf.items[index]);
                    }
                }
            }
        } else items_to_align = shelf.items;

        // sort items_to_align by distFromStart
        items_to_align.sort(function (a: any, b: any) {
            let aNumber = a.placement.distFromTop ? a.placement.distFromTop : 0;
            let bNumber = b.placement.distFromTop ? b.placement.distFromTop : 0;
            return (aNumber < bNumber) ? 1 : (aNumber > bNumber) ? -1 : 0;
        });
        console.log('items_to_align down sorted', items_to_align);

        // go over items_to_align and align to down
        for (let i = 0; i < items_to_align.length; i++) {
            // create a 2d array corresponding to the freezer
            let boxArr = this.createFreezerBoxArr(shelf);
            items_to_align[i].placement.distFromTop = this.checkFreezerBoxDown(boxArr, shelf, items_to_align[i]);
        }

        // this.saveSnapShot(shelf);
        this.saveAisleShelf(shelf);
    }
    checkFreezerBoxDown = (boxArr: any[], shelf: PlanogramShelf, item: PlanogramItem) => {
        // check backwards from itemTotalHeight to maxY and for a width of startX to itemTotalWidth
        let startX = item.placement.distFromStart ? item.placement.distFromStart : 0;
        let itemTotalWidth = startX + ((item.placement.pWidth ? item.placement.pWidth : ProductDefaultDimensions.width) * item.placement.faces) - 1;
        let startY = item.placement.distFromTop ? item.placement.distFromTop : 0;
        let itemTotalHeight = startY + ((item.placement.pHeight ? item.placement.pHeight : ProductDefaultDimensions.height) * item.placement.stack) + 1;
        let maxY = shelf.dimensions.height;
        let returnY = maxY;
        for (let x = itemTotalWidth; x > startX; x--) {
            for (let y = maxY; y > itemTotalHeight; y--) {
                // if the check bumps into another item return this location
                if (boxArr[x] && boxArr[x][y] && boxArr[x][y].value === 1) {
                    // console.log('down encounter full. y', y, 'return', y - 1)
                    if (y - 1 < returnY)
                        returnY = y - 1;
                    // if (y < returnY) returnY = y;
                }
            }
        }
        // check didn't encounter anything so we return 0 as the most left position available
        return returnY - ((item.placement.pHeight ? item.placement.pHeight : ProductDefaultDimensions.height) * item.placement.stack);
    }
    alignFreezerToUp = async (shelf: PlanogramShelf) => {
        let items_to_align = [];
        // get all items to align from this.props.multiSelectId
        if (this.props.multiSelectId.length > 0) {
            for (let i = 0; i < this.props.multiSelectId.length; i++) {
                // if the item is in this freezer - set it's dist from start to 0
                let itemLocation = this.props.multiSelectId[i].substring(0, this.props.multiSelectId[i].indexOf('I'));
                if (itemLocation === shelf.id) {
                    let index = shelf.items.findIndex(line => line.id === this.props.multiSelectId[i]);
                    if (index >= 0) {
                        items_to_align.push(shelf.items[index]);
                    }
                }
            }
        } else items_to_align = shelf.items;

        // in a temp freezer items may not have a distFromTop. Place all items by default on bottom.
        if (shelf.freezer === 2) {
            // console.log('shelf.freezer === 2');
            for (let i = 0; i < items_to_align.length; i++) {
                if (!items_to_align[i].placement.distFromTop) {
                    let newDistFromTop = shelf.dimensions.height - (parseInt(JSON.stringify(items_to_align[i].placement && items_to_align[i].placement.pHeight ? items_to_align[i].placement.pHeight : ProductDefaultDimensions.height)) * items_to_align[i].placement.stack);
                    items_to_align[i].placement.distFromTop = newDistFromTop;
                    // console.log(i, 'distFromTop updated to', JSON.parse(JSON.stringify(items_to_align[i])));
                }
            }
        }
        // sort items_to_align by distFromStart
        items_to_align.sort(function (a: any, b: any) {
            let aNumber = a.placement.distFromTop ? a.placement.distFromTop : 0;
            let bNumber = b.placement.distFromTop ? b.placement.distFromTop : 0;
            return (aNumber > bNumber) ? 1 : (aNumber < bNumber) ? -1 : 0;
        });

        // go over items_to_align and align to up
        for (let i = 0; i < items_to_align.length; i++) {
            // create a 2d array corresponding to the freezer
            let boxArr = this.createFreezerBoxArr(shelf);
            items_to_align[i].placement.distFromTop = this.checkFreezerBoxUp(boxArr, items_to_align[i]);
        }
        // this.saveSnapShot(shelf);
        this.saveAisleShelf(shelf);
    }
    checkFreezerBoxUp = (boxArr: any[], item: PlanogramItem) => {
        // check backwards from itemTotalHeight to 0 and for a width of startX to itemTotalWidth
        let startX = item.placement.distFromStart ? item.placement.distFromStart : 0;
        let itemTotalWidth = startX + ((item.placement.pWidth ? item.placement.pWidth : ProductDefaultDimensions.width) * item.placement.faces) - 1;
        let startY = item.placement.distFromTop ? item.placement.distFromTop - 2 : 0;
        // let itemTotalHeight = startY + ((item.placement.pHeight ? item.placement.pHeight : ProductDefaultDimensions.height) * item.placement.stack) + 1;
        let returnY = 0;
        for (let x = itemTotalWidth; x >= startX; x--) {
            for (let y = startY; y >= 0; y--) {
                // if the check bumps into another item return this location
                if (boxArr[x] && boxArr[x][y] && boxArr[x][y].value === 1) {
                    // console.log('down encounter full. y', y, 'return', y - 1)
                    if (y > returnY) returnY = y;
                }
            }
        }
        // check didn't encounter anything so we return 0 as the most left position available
        return returnY;
    }
    alignFreezerToRight = async (shelf: PlanogramShelf) => {
        let items_to_align = [];
        // get all items to align from this.props.multiSelectId
        if (this.props.multiSelectId.length > 0) {
            for (let i = 0; i < this.props.multiSelectId.length; i++) {
                // if the item is in this freezer - set it's dist from start to 0
                let itemLocation = this.props.multiSelectId[i].substring(0, this.props.multiSelectId[i].indexOf('I'));
                if (itemLocation === shelf.id) {
                    let index = shelf.items.findIndex(line => line.id === this.props.multiSelectId[i]);
                    if (index >= 0) {
                        items_to_align.push(shelf.items[index]);
                    }
                }
            }
        } else items_to_align = shelf.items;

        // sort items_to_align by distFromStart
        items_to_align.sort(function (a: any, b: any) {
            let aNumber = a.placement.distFromStart ? a.placement.distFromStart : 0;
            let bNumber = b.placement.distFromStart ? b.placement.distFromStart : 0;
            return (aNumber < bNumber) ? 1 : (aNumber > bNumber) ? -1 : 0;
        });

        // go over items_to_align and align to right
        for (let i = 0; i < items_to_align.length; i++) {
            // create a 2d array corresponding to the freezer
            let boxArr = this.createFreezerBoxArr(shelf);
            items_to_align[i].placement.distFromStart = this.checkFreezerBoxRight(boxArr, shelf, items_to_align[i]);
        }

        // this.saveSnapShot(shelf);
        this.saveAisleShelf(shelf);
    }
    checkFreezerBoxRight = (boxArr: any[], shelf: PlanogramShelf, item: PlanogramItem) => {
        // check backwards from item.placement.distFromStart to 0 and for a height of item.placement.distfromTop
        let startX = item.placement.distFromStart ? item.placement.distFromStart - 1 : 0;
        let itemTotalWidth = startX + ((item.placement.pWidth ? item.placement.pWidth : ProductDefaultDimensions.width) * item.placement.faces) + 2;
        let startY = item.placement.distFromTop ? item.placement.distFromTop : 0;
        let itemTotalHeight = startY + ((item.placement.pHeight ? item.placement.pHeight : ProductDefaultDimensions.height) * item.placement.stack);
        let returnX = shelf.dimensions.width;
        for (let x = itemTotalWidth; x < shelf.dimensions.width; x++) {
            for (let y = startY; y < itemTotalHeight; y++) {
                // if the check bumps into another item return this location
                if (boxArr[x] && boxArr[x][y] && boxArr[x][y].value === 1) {
                    // console.log('checkFreezerBoxLeft encoutered 1 at x', x, 'y', y);
                    if (x < returnX) returnX = x;
                    // if (x + 1 < returnX) returnX = x + 1;
                }
            }
        }
        return returnX - ((item.placement.pWidth ? item.placement.pWidth : ProductDefaultDimensions.width) * item.placement.faces);
    }
    render() {
        let {  multiSelectId, virtualStore, productsMap, deleteShelf, switchShelves, 
                editShelfDimensions, updateProductDimensions, deleteItem,
                aisleId, storeId, canDrop, isDragging, connectDropTarget, connectDragSource,
                editShelfItemPlacement, section } = this.props;
        const { height: sectionHeight, width: sectionWidth } = section.dimensions;
        const { shelfDetails, shelfMap: aisleShelves, sectionGroups } = virtualStore;
        const groupTag = sectionGroups.sectionToGroup[section.id] || section.id;
        const shelvesHeight = calculateShelvesHeight(section);

        let sectionName = '';
        if (section.section_layout > 0) {
            let layout:any = this.props.layouts ? this.props.layouts.filter((line:any) => line.Id === section.section_layout) : null;
            if (layout && layout[0] && layout[0].Name) sectionName = layout[0].Name;
        }

        return connectDropTarget(connectDragSource(
            <div key={"target_" + section.id} className={
                "planogram-section" + (canDrop ? " droppable" : "") + (isDragging ? " dragged" : "")}
                style={{
                    width: widthDensity(sectionWidth),
                    height: heightDensity(sectionHeight + (0.25 * sectionHeight)) + 20,
                    borderRight: this.props.sectionBorders === "both" || this.props.sectionBorders === "right" ? '#00A69A 0.2em solid' : '',
                    borderLeft: this.props.sectionBorders === "both" || this.props.sectionBorders === "left" ? '#00A69A 0.2em dashed' : '#cacaca 0.1em dashed'
                }}
                onClick={e => {
                    e.stopPropagation();
                    if (!e.ctrlKey) {
                        e.stopPropagation();
                        this.props.setCurrentSelectedId('');
                        this.props.setMultiSelectId([]);
                        this.props.hideProductDisplayerBarcode();
                    }
                }} >
                {section.shelves.map((shelf:PlanogramShelf, i:number) => {
                    let minDimensions;
                    // let shelvesHeight = section.shelves.map(sh => sh.dimensions.height).reduce((p, c) => p + c);
                    const maxDimensions = {
                        ...section.dimensions,
                        height: sectionHeight - shelvesHeight + shelf.dimensions.height
                    };
                    let initItems = JSON.parse(JSON.stringify(shelf.items));
                    shelf.items = [];
                    for (let i = 0; i < initItems.length; i++) {
                        if (initItems[i] && typeof initItems[i] != 'undefined')
                            shelf.items.push(initItems[i]);
                    }
                    if (shelf.items.length > 0) {
                        const itemDimensions = shelf.items.map((item:any) =>
                            shelfItemDimensions(item.placement, {
                                width: item.placement.pWidth ? item.placement.pWidth : ProductDefaultDimensions.width,
                                height: item.placement.pHeight ? item.placement.pHeight : ProductDefaultDimensions.height,
                                depth: item.placement.pDepth ? item.placement.pDepth : ProductDefaultDimensions.depth
                            }));
                        let itemsMinHeight = itemDimensions.map((sh:any) => sh.height).reduce((p:number, c:number) => Math.max(p, c));
                        let itemsMinWidth = itemDimensions.map((sh:any) => sh.width).reduce((p:number, c:number) => p + c);
                        let itemsMinDepth = itemDimensions.map((sh:any) => sh.depth).reduce((p:number, c:number) => Math.max(p, c));
                        if (maxDimensions.height >= itemsMinHeight && maxDimensions.width >= itemsMinWidth && maxDimensions.depth >= itemsMinDepth)
                            minDimensions = {
                                height: itemsMinHeight,
                                width: itemsMinWidth,
                                depth: itemsMinDepth
                            };
                    }
                    let originalShelfDimensions = JSON.parse(JSON.stringify(shelf.dimensions));

                   if (shelf.freezer === 0 || shelf.freezer === 2 || !shelf.freezer)
                        return <Menu id={"sed_" + shelf.id} key={"sed_" + shelf.id + "_" + i} className="planogram-context-window" >
                            <DimensionModal
                                init={shelf.dimensions}
                                title={<FormattedMessage id="shelf-dimensions"/>}
                                maxDimensions={maxDimensions}
                                minDimensions={minDimensions}
                                onSubmit={async (dimensions) => {
                                    await editShelfDimensions(shelf.id, dimensions);
                                    let ind = shelf.items.findIndex((line:any) => parseInt(line.id.substring(line.id.indexOf('I') + 1)) >= 100);
                                    if (shelf.freezer === 2 || ind >= 0) {
                                        // this shelf is a temp freezer - align it to the bottom
                                        let tempShelf = shelf;
                                        tempShelf.dimensions = dimensions;
                                        await this.alignFreezerToDown(tempShelf);
                                    }
                                    /*  re-calculate item rows if shelf.dimensions.depth changed */
                                    // note that we allow the shelf to change depth freely and this 
                                    // effects ONLY items on that shelf. We do not affect the section, other shelves or other items
                                    let tmpShelf = JSON.parse(JSON.stringify(shelf));
                                    tmpShelf.dimensions = dimensions;
                                    if (dimensions.depth != originalShelfDimensions.depth) {
                                        if (tmpShelf.items.length > 0) {
                                            // need to re calculate the row of all items based on the new shelf depth
                                            for (let i = 0; i < tmpShelf.items.length; i++) {
                                                // we force a re calculate of the row while IGNORING manual_row_only
                                                let item = tmpShelf.items[i];
                                                item.placement.row = Math.floor(dimensions.depth / (item.placement.pDepth ? item.placement.pDepth : ProductDefaultDimensions.depth));
                                                if (item.placement.row < 1) item.placement.row = 1;
                                            }
                                        }
                                    }
                                    await this.saveSnapShot(tmpShelf);
                                    contextMenu.hideAll();
                                }} />
                            {/* Add option to align all items left on the shelf */}
                            <div className="input-row">
                                <span style={{
                                    flex: "1", textAlign: "inherit", border: 'none', borderRadius: '0', background: '#ECF0F1',
                                    color: '#2b998a', padding: '0.5em 1em', fontSize: '1em'
                                }}>
                                    <input type="checkbox"
                                        style={{ marginLeft: "10px" }}
                                        checked={shelf.freezer === 2 ? true : false}
                                        onChange={e => {
                                            shelf.freezer = shelf.freezer === 0 ? 2 : 0;
                                            this.saveSnapShot(shelf);
                                        }} />
                                    <span><FormattedMessage id="allow-item-over-item" /></span>
                                </span>
                            </div>
                            {shelf.freezer === 0 || !shelf.freezer
                                ?
                                <div>
                                    <div className="input-row">
                                        <button onClick={e => {
                                            shelf.distFromStart = 0;
                                        }}>
                                            <FontAwesomeIcon icon={faEllipsisH} />
                                            <span><FormattedMessage id="reset-shelf-to-0" /></span>
                                        </button>
                                    </div>
                                    <div className="input-row">
                                        <button onClick={async (e) => {
                                            await this.alignThisShelfToLeft(shelf);
                                            this.saveSnapShot(shelf);
                                        }}>
                                            <FontAwesomeIcon icon={faAlignLeft} />
                                            <span><FormattedMessage id="align-to-left" /></span>
                                        </button>
                                    </div>
                                    <div className="input-row">
                                        <button onClick={async (e) => {
                                            await this.alignThisShelfToRight(shelf);
                                            this.saveSnapShot(shelf);
                                        }}>
                                            <FontAwesomeIcon icon={faAlignRight} />
                                            <span><FormattedMessage id="align-to-right" /></span>
                                        </button>
                                    </div>
                                    <div className="input-row">
                                        <button onClick={async (e) => {
                                            await this.alignThisShelfToJustify(shelf);
                                            this.saveSnapShot(shelf);
                                        }}>
                                            <FontAwesomeIcon icon={faAlignJustify} />
                                            <span><FormattedMessage id="align-justify" /></span>
                                        </button>
                                    </div>
                                </div>
                                :
                                <div>
                                    <div className="input-row">
                                        <button onClick={async (e) => {
                                            await this.alignFreezerToLeft(shelf);
                                            this.saveSnapShot(shelf);
                                        }}>
                                            <FontAwesomeIcon icon={faArrowLeft} />
                                            <span>Align Freezer Left</span>
                                        </button>
                                    </div>
                                    <div className="input-row">
                                        <button onClick={async (e) => {
                                            await this.alignFreezerToRight(shelf);
                                            this.saveSnapShot(shelf);
                                        }}>
                                            <FontAwesomeIcon icon={faArrowRight} />
                                            <span>Align Freezer Right</span>
                                        </button>
                                    </div>
                                    <div className="input-row">
                                        <button onClick={async (e) => {
                                            await this.alignFreezerToDown(shelf);
                                            this.saveSnapShot(shelf);
                                        }}>
                                            <FontAwesomeIcon icon={faArrowDown} />
                                            <span>Align Freezer Down</span>
                                        </button>
                                    </div>
                                    <div className="input-row">
                                        <button onClick={async (e) => {
                                            await this.alignFreezerToUp(shelf);
                                            this.saveSnapShot(shelf);
                                        }}>
                                            <FontAwesomeIcon icon={faArrowUp} />
                                            <span>Align Freezer Up</span>
                                        </button>
                                    </div>
                                </div>
                            }
                        </Menu>
                    if (shelf.freezer === 1)
                        return <Menu id={"sed_" + shelf.id} key={"sed_" + shelf.id + "_" + i} className="planogram-context-window" >
                            <DimensionModal
                                init={shelf.dimensions}
                                title={"Freezer Dimensions"}
                                maxDimensions={maxDimensions}
                                minDimensions={minDimensions}
                                onSubmit={(dimensions) => {
                                    editShelfDimensions(shelf.id, dimensions);
                                    contextMenu.hideAll();
                                }} />
                            <div className="input-row">
                                <button onClick={async (e) => {
                                    //if (window.confirm("Are you sure you want to align-justify all items on this shelf?"))
                                    await this.alignFreezerToLeft(shelf);
                                    /* Barak 22.11.20 */ this.saveSnapShot(shelf);
                                }}>
                                    <FontAwesomeIcon icon={faArrowLeft} />
                                    <span><FormattedMessage id="align-freezer-left" /></span>
                                </button>
                            </div>
                            <div className="input-row">
                                <button onClick={async (e) => {
                                    //if (window.confirm("Are you sure you want to align-justify all items on this shelf?"))
                                    await this.alignFreezerToRight(shelf);
                                    /* Barak 22.11.20 */ this.saveSnapShot(shelf);
                                }}>
                                    <FontAwesomeIcon icon={faArrowRight} />
                                    <span><FormattedMessage id="align-freezer-right" /></span>
                                </button>
                            </div>
                            <div className="input-row">
                                <button onClick={async (e) => {
                                    //if (window.confirm("Are you sure you want to align-justify all items on this shelf?"))
                                    await this.alignFreezerToDown(shelf);
                                    /* Barak 22.11.20 */ this.saveSnapShot(shelf);
                                }}>
                                    <FontAwesomeIcon icon={faArrowDown} />
                                    <span><FormattedMessage id="align-freezer-down" /></span>
                                </button>
                            </div>
                            <div className="input-row">
                                <button onClick={async (e) => {
                                    //if (window.confirm("Are you sure you want to align-justify all items on this shelf?"))
                                    await this.alignFreezerToUp(shelf);
                                    /* Barak 22.11.20 */ this.saveSnapShot(shelf);
                                }}>
                                    <FontAwesomeIcon icon={faArrowUp} />
                                    <span><FormattedMessage id="align-freezer-up" /></span>
                                </button>
                            </div>
                        </Menu>
                })}
                {section.shelves.map((shelf:any, i:number) => {
                    let PrintArray:any = {}
                    if(localStorage.printStatus === 'true') {
                        let itemsShelfBefore:any = getItemsShelfBefore(this.props.sections, this.props.index, i)
                        let lastItemShelfBefore:any = itemsShelfBefore &&  itemsShelfBefore[itemsShelfBefore.length-1] ? itemsShelfBefore[itemsShelfBefore.length-1] : null
                        if( !lastItemShelfBefore &&
                            shelf.items[shelf.items.length-1] &&  shelf.items[shelf.items.length-1].placement.pWidth &&
                            shelf.dimensions.width < ( shelf.items[shelf.items.length-1].placement.pWidth * shelf.items[shelf.items.length-1].placement.faces ) )
                            {
                                PrintArray.section_index = this.props.index
                                PrintArray.item =  shelf.items[shelf.items.length-1]
                                PrintArray.times =   Math.ceil(shelf.dimensions.width/shelf.items[shelf.items.length-1].placement.pWidth)
                             }
                        if( lastItemShelfBefore &&  
                            shelf.dimensions.width < ( lastItemShelfBefore.placement.distFromStart + ( lastItemShelfBefore.placement.pWidth * lastItemShelfBefore.placement.faces ) ) ) {
                                let BeforeShelfsWidth = this.props.sections.reduce((acc:number, section:any) =>{ if( section.shelves &&  section.shelves[i] && section.index < this.props.index ){return acc+section.shelves[i].dimensions.width} else return acc}, 0);
                                let extraWidth = Math.abs(( ( lastItemShelfBefore.placement.pWidth * lastItemShelfBefore.placement.faces ) - BeforeShelfsWidth) )//shelf.dimensions.width
                                PrintArray.item =  lastItemShelfBefore
                                PrintArray.times =  extraWidth < shelf.dimensions.width ? Math.ceil(extraWidth/lastItemShelfBefore.placement.pWidth) :  Math.ceil(shelf.dimensions.width/lastItemShelfBefore.placement.pWidth)
   
                        }               
                    }
                    let initItems = JSON.parse(JSON.stringify(shelf.items));
                    shelf.items = [];
                    for (let i = 0; i < initItems.length; i++) {
                        if (initItems[i] && typeof initItems[i] != 'undefined')
                            shelf.items.push(initItems[i]);
                    }
                    let availableSpace = shelfAvailableSpace(shelf, productsMap);
                    let shelfHeight = shelf.dimensions.height;
                    let shelfDepth = shelf.dimensions.depth;

                    let mainShelf: PlanogramShelf;

                    const shelfDetail = shelfDetails[shelf.id];
                    if (shelfDetail && shelfDetail.display && shelfDetail.combined.length > 0) {
                        const combinedShelves = [shelf, ...shelfDetail.combined.map((id) => aisleShelves[id])];
                        availableSpace = shelfAvailableSpace(combinedShelves, productsMap);
                        // console.log('1702 call shelfAvailableSpace',shelf.id, availableSpace);
                    }

                    if (shelfDetail && shelfDetail.main_shelf && aisleShelves[shelfDetail.main_shelf]) {
                        mainShelf = aisleShelves[shelfDetail.main_shelf];
                        let mainShelfDetails = shelfDetails[shelfDetail.main_shelf];
                        const combinedShelves = [mainShelf, ...mainShelfDetails.combined.map((id) => aisleShelves[id])];
                        availableSpace = shelfAvailableSpace(combinedShelves, productsMap);
                    }

                    return <MenuProvider id={"sed_" + shelf.id}
                        key={"sem_" + shelf.id}>
                        <PlanogramShelfDnd
                           aisleId={aisleId}
                            printArray={PrintArray}
                            shelf={shelf}
                            shelfIndex={i}
                            verifyDrop={(droppedItem) => {
                                return true;
                            }}
                            onDrop={async (source:any) => {
                                let distFromStartInit = localStorage.getItem('distFromStart');
                                let distFromTopInit = localStorage.getItem('distFromTop');
                                let distFromStart = 0;
                                let distFromTop: any = null;
                                if (distFromStartInit) distFromStart = parseInt(distFromStartInit);
                                if( this.props.newPlano && this.props.newPlano.productsMap ) {
                                    let itemsShelfWidth:any = shelf.items[0] && shelf.items[0].placement ? shelf.items[0].placement.distFromStart : 0
                                    shelf.items.forEach((_item:PlanogramItem) => {
                                        let itemWidth = _item.placement.pWidth ? _item.placement.pWidth * _item.placement.faces : 0
                                        itemsShelfWidth += itemWidth
                                    });
                                // find the Group this section belongs to and the first shelf in that section
                                if (storeId === 30) {
                                    let sectionGroups = virtualStore.sectionGroups;
                                    let allShelves = virtualStore.shelfMap;
                                    let selectedSection = '';
                                    for (let i = 0; i < sectionGroups.groupList.length; i++) {
                                        if (sectionGroups.groupMap[sectionGroups.groupList[i]].sections.findIndex((line:any) => line === section.id) >= 0)
                                            selectedSection = sectionGroups.groupMap[sectionGroups.groupList[i]].sections[0];
                                    }
                                    let shelfIndex = parseInt(shelf.id.substr(shelf.id.indexOf('SH') + 2, 1));
                                    // since the bottom most shelf never changes height it is exempt from sections - it only has
                                    // if (shelfIndex === 0) selectedSection = sectionGroups.groupMap[sectionGroups.groupList[0]].sections[0];
                                    let selectedShelf = selectedSection + 'SH' + shelfIndex;
                                    shelf = allShelves[selectedShelf];
                                }
                                if (source.type === PlanogramDragDropTypes.SERIES_SIDEBAR) {
                                    let series = source.payload
                                    const catalogProductDepth = this.props.newPlano.productsMap[source.payload.product] && this.props.newPlano.productsMap[source.payload.product].length
                                    let newItems = series.map((p:any,i: number)=> {
                                        if(p.faces > 0) {
                                        const product:any = productsMap[p.BarCode];
                                        return {
                                        ...blankItem, 
                                        product: product.BarCode,
                                        placement:  {...blankItem.placement,
                                            faces: p.faces,
                                            stack: product.placement && product.placement.stack ? product.placement.stack : 1,
                                            row: product.placement && product.placement.row ? product.placement.row : Math.floor(shelfDepth / (catalogProductDepth ? catalogProductDepth : 1)),
                                            manual_row_only: product.placement && product.placement.manual_row_only ? product.placement.manual_row_only : 0,
                                            position: product.placement && product.placement.position ? product.placement.position : 0,
                                            pWidth: this.props.newPlano.productsMap[product.BarCode].width != undefined ? this.props.newPlano.productsMap[product.BarCode].width : ProductDefaultDimensions.width,
                                            pHeight: this.props.newPlano.productsMap[product.BarCode].height != undefined ? this.props.newPlano.productsMap[product.BarCode].height : ProductDefaultDimensions.height,
                                            pDepth: this.props.newPlano.productsMap[product.BarCode].length != undefined ? this.props.newPlano.productsMap[product.BarCode].length : ProductDefaultDimensions.depth,
                                            distFromStart: distFromStart,//itemsShelfWidth,
                                            // distFromStart: product.placement && product.placement.distFromStart
                                            //   ? (product.placement.distFromStart + ((product.placement.pWidth ? product.placement.pWidth : ProductDefaultDimensions.width) * product.placement.faces) - 1)
                                            //   : (product.placement && product.placement.distFromStart ? product.placement.distFromStart : 0),
                                            distFromTop: null
                                        }}}});
                                           newItems.forEach(async (item:any, i:number)=> {
                                                        await this.addShelfItem(this.props.aisleShelves[shelf.id], item);
                                            })
                                    //  await this.addShelfItem(this.props.aisleShelves[shelf.id], null, 0, newItems);
                                    //  await this.alignThisShelfToLeft(this.props.aisleShelves[shelf.id], shelf.items[0].placement.distFromStart);
                                     await this.saveSnapShot(this.props.aisleShelves[shelf.id]);
                                 
                                }
                                if (source.type === PlanogramDragDropTypes.PRODUCT_SIDEBAR) {
                                    let productBarcode: number = source.payload;
                                    let distFromStartInit = localStorage.getItem('distFromStart');
                                    let distFromTopInit = localStorage.getItem('distFromTop');
                                    let distFromStart = 0;
                                    let distFromTop: any = null;
                                    if (distFromStartInit) distFromStart = parseInt(distFromStartInit);
                                    console.log('in section - item dropped from PRODUCT_SIDEBAR', productBarcode, localStorage.getItem('distFromStart'), localStorage.getItem('distFromTop'));

                                    const product = productsMap[productBarcode];
                                    /* Barak 10.9.20 */ const productDimensions: DimensionObject = catalogProductDimensionObject(product);
                                    /* Barak 10.9.20 *
                                    const productDimensions: DimensionObject = {
                                        width: source.payload.placement && source.payload.placement.pWidth > 0 ? source.payload.placement.pWidth : 0,
                                        height: source.payload.placement &&  source.payload.placement.pHeight > 0 ? source.payload.placement.pHeight : 0,
                                        depth: source.payload.placement &&  source.payload.placement.pDepth > 0 ? source.payload.placement.pDepth : 0
                                    };
                                    ****************/
                                    /* Barak 21.12.20 - check if this item should be a second level to an existing item */
                                    let itemOverItem = false;
                                    let bottomItemDistFromStart = 0;
                                    let origFromTop = shelf.dimensions.height;
                                    if (this.props.multiSelectId.length === 1 && shelf.id === this.props.multiSelectId[0].substring(0, this.props.multiSelectId[0].indexOf('I'))) {
                                        // get the level of the selected item
                                        let sItem = shelf.items.filter(line => line.id === this.props.multiSelectId[0]);
                                        if (sItem && sItem[0] && (sItem[0].shelfLevel === 1 || sItem[0].shelfLevel === undefined)
                                            && (sItem[0].linkedItemUp === null || sItem[0].linkedItemUp === undefined)
                                            && (sItem[0].linkedItemDown === null || sItem[0].linkedItemDown === undefined)) {
                                            // if there is only one item selected and it's on the same shelf as the target of the dropped item and the item is on level 1
                                            // and the item dosn't already have an item over it or under it 
                                            console.log('only one item selected and it is on the same shelf as the dropped item', this.props.multiSelectId[0], shelf.id);
                                            itemOverItem = true;
                                            bottomItemDistFromStart = sItem[0].placement && sItem[0].placement.distFromStart ? sItem[0].placement.distFromStart : 0;
                                            origFromTop = (sItem[0].placement.distFromTop
                                                ? sItem[0].placement.distFromTop
                                                : (shelf.dimensions.height - ((sItem[0].placement.pHeight ? sItem[0].placement.pHeight : ProductDefaultDimensions.height) * sItem[0].placement.stack)));
                                            // newItem.placement.distFromTop = origFromTop - ((newItem.placement.pHeight ? newItem.placement.pHeight : ProductDefaultDimensions.height) * newItem.placement.stack);
                                        }
                                    }
                                    /************************************************************************************/
                                    if (product === undefined || !product || !product.width || !product.height || !product.length || !validateDimensions(productDimensions)) {
                                        const shelfDepth = shelf.dimensions.depth;
                                        const shelfHeight = shelf.dimensions.height;
                                        /* Barak 29.9.20 */
                                        let distFromStartInit = localStorage.getItem('distFromStart');
                                        let distFromTopInit = localStorage.getItem('distFromTop');
                                        let distFromStart = 0;
                                        let distFromTop: any = null;
                                        if (distFromStartInit) distFromStart = parseInt(distFromStartInit);
                                        if (!this.props.aisleShelves[shelf.id]) console.log('!this.props.aisleShelves[shelf.id] 1', shelf.id, this.props.aisleShelves);
                                        if (distFromTopInit && this.props.aisleShelves[shelf.id] && (this.props.aisleShelves[shelf.id].freezer === 1 || this.props.aisleShelves[shelf.id].freezer === 2))
                                            // if (distFromTopInit)
                                            distFromTop = parseInt(distFromTopInit);
                                        console.log('distFromStart before addProductAction 1', distFromStart, distFromTop);
                                        /*****************/
                                        setModal(() => <DimensionModal
                                            init={ProductDefaultDimensions}
                                            title={"Section Dimensions"}
                                            onSubmit={async (dimensions) => {
                                                await planogramApi.updateProductDimensions(productBarcode, dimensions).then(async (res) => {
                                                    await updateProductDimensions(
                                                        productBarcode,
                                                        dimensions,
                                                        await addProductAction(shelf.id, productBarcode, {
                                                            faces: source.faces,
                                                            stack: Math.floor(shelfHeight / dimensions.height),
                                                            row: Math.floor(shelfDepth / dimensions.depth),
                                                            manual_row_only: 0,
                                                            /* Barak & Osher 10.9.20 - support rotating products*/
                                                            position: 0,
                                                            pWidth: dimensions.width,
                                                            pHeight: dimensions.height,
                                                            pDepth: dimensions.depth,
                                                            /****************************************************/
                                                            distFromStart: itemOverItem ? bottomItemDistFromStart : (distFromStart > 0 ? distFromStart : dimensions.width),
                                                            distFromTop: distFromStart ? distFromStart : itemOverItem ? origFromTop - (dimensions.height * Math.floor(shelfHeight / dimensions.height)) : (distFromTop >= 0 ? distFromTop : null)
                                                        },
                                                            /* Barak 21.12.20 */
                                                            itemOverItem ? undefined : undefined, // linkedItemUp
                                                            itemOverItem ? this.props.multiSelectId[0] : undefined, // linkedItemDown - new item is linked to the marked item
                                                            itemOverItem ? 2 : undefined // shelfLevel - it's a second level item
                                                            /******************/
                                                        )
                                                    );
                                                    toggleModal();
                                                }).catch((err) => {
                                                    console.error(err);
                                                })
                                                /* Barak 29.9.20 */
                                                // after adding item we need to re-order the shelf
                                                // this.reOrderThisShelfPosition(shelf);
                                                await this.addShelfItem(this.props.aisleShelves[shelf.id], blankItem);
                                                /* Barak 26.11.20 */ if (this.props.aisleShelves[shelf.id].freezer === 2) await this.alignFreezerToDown(this.props.aisleShelves[shelf.id]);
                                                /*****************/
                                                /* Barak 22.11.20 - Add saving array for aisle, allowing to do 
                                                   Ctrl+Z to move back and Ctrl+Y to move forward */
                                                await this.saveSnapShot(this.props.aisleShelves[shelf.id]);
                                                /*************************************************************/
                                            }}
                                        />)
                                    }
                                    else {
                                        console.log('before addShelfItem', JSON.parse(JSON.stringify(shelf)), productBarcode);
                                        /* Barak 29.9.20 */
                                      
                                        if (!this.props.aisleShelves[shelf.id]) console.log('!this.props.aisleShelves[shelf.id] 2', shelf.id, this.props.aisleShelves);
                                        if (distFromTopInit && this.props.aisleShelves[shelf.id] && (this.props.aisleShelves[shelf.id].freezer === 1 || this.props.aisleShelves[shelf.id].freezer === 2))                                            // if (distFromTopInit)
                                            distFromTop = parseInt(distFromTopInit);
                                        console.log('distFromStart before addProductAction 2', distFromStart, distFromTop);
                                        /*****************/
                                        const shelfDepth = shelf.dimensions.depth;
                                        const catalogProductDepth = this.props.newPlano.productsMap[source.payload.product] && this.props.newPlano.productsMap[source.payload.product].length
                                        let itemsShelfWidth:any = shelf.items[0] && shelf.items[0].placement ? shelf.items[0].placement.distFromStart : 0

                                        shelf.items.forEach((_item:PlanogramItem) => {
                                            let itemWidth = _item.placement.pWidth ? _item.placement.pWidth * _item.placement.faces : 0
                                            itemsShelfWidth += itemWidth
                                        });
                                        if( itemsShelfWidth  > shelf.dimensions.width ) {
                                            let target_A = shelf.items[0].id.substring(0, shelf.items[0].id.indexOf('SE'));
                                            let target_SE = parseInt(shelf.items[0].id.substring(shelf.items[0].id.indexOf('SE') + 2, shelf.items[0].id.indexOf('SH')));
                                            let target_SH = parseInt(shelf.items[0].id.substring(shelf.items[0].id.indexOf('SH') + 2));
                                            let itemTotalWidth = (shelf.items[0].placement.distFromStart ? shelf.items[0].placement.distFromStart : 0) + ((shelf.items[0].placement.pWidth ? shelf.items[0].placement.pWidth : ProductDefaultDimensions.width) * shelf.items[0].placement.faces);
                                                        let shelfWidth = shelf.dimensions.width;
                                                        if (itemTotalWidth > shelfWidth) {
                                                                var nextShelf =  this.props.store.aisles.filter((store:any) => store.id === target_A)[0].sections[target_SE].shelves[target_SH];
                                                                console.log('nextShelf', nextShelf)
                                                                var nexShelfDistFromStart = nextShelf.distFromStart 
                                                                var nexShelfLastItemWidth = nextShelf.items[nextShelf.items.length -1].placement.pWidth
                                                                var nexShelfLastItemFaces = nextShelf.items[nextShelf.items.length -1].placement.faces
                                                         }  
                                                  }
                                        let newHeight = this.props.newPlano.productsMap[productBarcode].height != undefined ? this.props.newPlano.productsMap[productBarcode].height : ProductDefaultDimensions.height;
                                        let newItem: PlanogramItem = {
                                            id: '',
                                            placement: {
                                                faces: source.faces,
                                                stack: 1,
                                                row: Math.floor(shelfDepth / (catalogProductDepth ? catalogProductDepth : 1)),
                                                manual_row_only: 0,
                                                position: 0,
                                                pWidth: this.props.newPlano.productsMap[productBarcode].width != undefined ? this.props.newPlano.productsMap[productBarcode].width : ProductDefaultDimensions.width,
                                                pHeight: this.props.newPlano.productsMap[productBarcode].height != undefined ? this.props.newPlano.productsMap[productBarcode].height : ProductDefaultDimensions.height,
                                                pDepth: this.props.newPlano.productsMap[productBarcode].length != undefined ? this.props.newPlano.productsMap[productBarcode].length : ProductDefaultDimensions.depth,
                                                distFromStart:  itemOverItem ? bottomItemDistFromStart : (distFromStart ? distFromStart : 0) ,// nexShelfDistFromStart  + (  nexShelfLastItemWidth * nexShelfLastItemFaces),//itemOverItem ? bottomItemDistFromStart : (distFromStart ? distFromStart : 0),
                                                distFromTop: itemOverItem ? origFromTop - (newHeight != undefined ? newHeight : 150) : (distFromTop ? distFromTop : null)
                                            },
                                            product: productBarcode,
                                            lowSales: false,
                                            missing: 0,
                                            branch_id: 0,
                                            /* Barak 21.12.20 - Add linking item option */
                                            linkedItemUp: itemOverItem ? null : null, // linkedItemUp
                                            linkedItemDown: itemOverItem ? this.props.multiSelectId[0] : null, // linkedItemDown - new item is linked to the marked item
                                            shelfLevel: itemOverItem ? 2 : 1, // shelfLevel - it's a second level item
                                            /******************/
                                        };
                                        console.log('2451 after alignFreezerToDown', JSON.parse(JSON.stringify(newItem)), JSON.parse(JSON.stringify(this.props.aisleShelves[shelf.id].items)));
                                        await this.addShelfItem(this.props.aisleShelves[shelf.id], newItem);
                                        console.log('2452 after alignFreezerToDown', JSON.parse(JSON.stringify(this.props.aisleShelves[shelf.id].items)));
                                        /* Barak 26.11.20 */ if (this.props.aisleShelves[shelf.id].freezer === 2) await this.alignFreezerToDown(this.props.aisleShelves[shelf.id]);
                                        /* Barak 26.11.20 */ if (this.props.aisleShelves[shelf.id].freezer === 2) console.log('2454 after alignFreezerToDown', JSON.parse(JSON.stringify(this.props.aisleShelves[shelf.id].items)));
                                        /* Barak 29.9.20 - if alignToLeft is true we need to immediately align the entire new shelf to the left */
                                        // need to get most recent shelf
                                        // const { aisleShelves } = this.props;
                                        // after adding item we need to re-order the shelf
                                        // this.reOrderThisShelfPosition(aisleShelves[shelf.id]);
                                        console.log('shelf after addShelfItem', this.props.aisleShelves[shelf.id]);
                                        if (this.props.alignToLeft && this.props.aisleShelves[shelf.id].freezer != 1) {
                                            // align the entire shelf (as long as it is of the same height) to the left
                                            await this.alignThisShelfToLeft(this.props.aisleShelves[shelf.id]);
                                            // alignThisShelfToLeft(this.props.store, aisleShelves[shelf.id], aisleShelves, this.props.shelvesDetails);
                                        }
                                        /********************************************************************************************************/
                                        /* Barak 22.11.20 - Add saving array for aisle, allowing to do 
                                            Ctrl+Z to move back and Ctrl+Y to move forward */
                                        this.saveSnapShot(this.props.aisleShelves[shelf.id]);
                                        /*************************************************************/
                                    }
                                }
                                else if (source.type === PlanogramDragDropTypes.SHELF_ITEM
                                    && ((shelf.freezer != 1 && shelf.freezer != 2) || source.payload.id.substring(0, source.payload.id.indexOf('I')) === shelf.id)) {
                                    /* Barak 21.12.20 - if item.shelfLevel is not 1 - DO NOT ALLOW IT TO BE MOVED */
                                    if (source.payload.shelfLevel != 1 && source.payload.shelfLevel != undefined) {
                                        let errorMsg = 'Cannot move a second story item';
                                        alert(errorMsg);
                                        return;
                                    }
                                    /******************************************************************************/
                                    // if (source.payload.product != null && shelf.items.findIndex(item => item.product === source.payload.product) === -1)
                                    // check if item was lifted and droppped from the same shelf to the same shelf
                                    if (source.payload.id.substring(0, source.payload.id.indexOf('I')) === shelf.id) {
                                        console.log("MOVE SHELF ITEM IN THE SAME SHELF", source.payload, shelf);
                                        // item is moved within the same shelf
                                        let placement = source.payload.placement;
                                        let distFromStartInit = localStorage.getItem('distFromStart');
                                        let distFromTopInit = localStorage.getItem('distFromTop');
                                        let distFromStart = 0;
                                        let distFromTop: any = null;
                                        if (distFromStartInit) distFromStart = parseInt(distFromStartInit);
                                        if (!this.props.aisleShelves[shelf.id]) console.log('!this.props.aisleShelves[shelf.id] 3', shelf.id, this.props.aisleShelves);
                                        if (distFromTopInit && this.props.aisleShelves[shelf.id] && (this.props.aisleShelves[shelf.id].freezer === 1 || this.props.aisleShelves[shelf.id].freezer === 2))
                                            // if (distFromTopInit)
                                            distFromTop = parseInt(distFromTopInit);
                                        placement.distFromStart = distFromStart;
                                        placement.distFromTop = distFromTop;
                                        editShelfItemPlacement(source.payload.id, placement);
                                    }
                                    console.log("DUPLICATE SHELF ITEM", source.payload, shelf);
                                    /* Barak 12.8.20 *
                                    createShelfItem(shelf.id, source.payload);
                                    *****************/
                                    /* Barak 12.8.20 - check if previous item on the same shelf has same barcode. if so, change faces rather than duplicate */
                                    /* Barak 29.9.20 - Add option to align all items left on the shelf */
                                    // // sort shelf items by distFromStart so that the last item we see will actually be the last item 
                                    // shelf.items.sort(function (a: any, b: any) {
                                    //     let aNumber = a.placement.distFromStart ? a.placement.distFromStart : 0;
                                    //     let bNumber = b.placement.distFromStart ? b.placement.distFromStart : 0;
                                    //     return (aNumber > bNumber) ? 1 : (aNumber < bNumber) ? -1 : 0;
                                    // });
                                    // /*******************************************************************/
                                    // let lastItem = shelf.items[shelf.items.length - 1];
                                    // if (lastItem && lastItem.product === source.payload.product) {
                                    //     console.log('same product - update faces for', lastItem.id);
                                    //     let placement = lastItem.placement;
                                    //     placement.faces += source.payload.placement.faces;
                                    //     editShelfItemPlacement(lastItem.id, placement);
                                    //     /* Barak 29.9.20 */
                                    //     // after changing placment (faces, position etc') we need to re-order the shelf
                                    //     this.reOrderThisShelfPosition(shelf);
                                    //     // find the shelf the item came from
                                    //     let currSHNew = source.payload.id.substring(0, source.payload.id.indexOf('I'));
                                    //     // delete the item on that shelf
                                    //     await deleteItem(currSHNew, source.payload.id);
                                    //     // if alignToLeft is true we need to immediately align the entire new shelf to the left 
                                    //     if (this.props.alignToLeft) {
                                    //         this.alignThisShelfToLeft(this.props.aisleShelves[currSHNew]);
                                    //         this.alignThisShelfToLeft(this.props.aisleShelves[shelf.id]);
                                    //     }
                                    //     /*****************/
                                    // } else {
                                    // console.log('not same product - duplicate item for', lastItem ? lastItem.id : 0);
                                    // check if multiSelectId length > 1
                                    let combined = 0;
                                    if (multiSelectId.length > 1) {
                                        let currentAisleId = parseInt(multiSelectId[i].substring(multiSelectId[i].indexOf('A') + 1, multiSelectId[i].indexOf('SE')));
                                        let currentAisle: any = null;
                                        if (this.props.store) {
                                            for (let i = 0; i < this.props.store.aisles.length; i++) {
                                                if (this.props.store.aisles[i].aisle_id === currentAisleId) {
                                                    currentAisle = this.props.store.aisles[i];
                                                    break;
                                                }
                                            }
                                        }
                                        multiSelectId.sort(function (a: any, b: any) {
                                            let currSE_A: number = parseInt(a.substring(a.indexOf('SE') + 2, a.indexOf('SH')));
                                            let currSH_A: number = parseInt(a.substring(a.indexOf('SH') + 2, a.indexOf('I')));
                                            let currI_A: number = parseInt(a.substring(a.indexOf('I') + 1));
                                            let currSE_B: number = parseInt(b.substring(b.indexOf('SE') + 2, b.indexOf('SH')));
                                            let currSH_B: number = parseInt(b.substring(b.indexOf('SH') + 2, b.indexOf('I')));
                                            let currI_B: number = parseInt(b.substring(b.indexOf('I') + 1));
                                            // if it's the same section and shelf, order by distFromStart and item index
                                            if (currSE_A === currSE_B && currSH_A === currSH_B) {
                                                let itemA = null;
                                                let itemB = null;
                                                if (currentAisle && currentAisle.sections[currSE_A]
                                                    && currentAisle.sections[currSE_A].shelves[currSH_A]
                                                    && currentAisle.sections[currSE_A].shelves[currSH_A].items[currI_A]) {
                                                    itemA = JSON.parse(JSON.stringify(currentAisle.sections[currSE_A].shelves[currSH_A].items[currI_A]));
                                                }
                                                if (currentAisle && currentAisle.sections[currSE_B]
                                                    && currentAisle.sections[currSE_B].shelves[currSH_B]
                                                    && currentAisle.sections[currSE_B].shelves[currSH_B].items[currI_B]) {
                                                    itemB = JSON.parse(JSON.stringify(currentAisle.sections[currSE_B].shelves[currSH_B].items[currI_B]));
                                                }
                                                if (itemA && itemB) {
                                                    let distFromStartA = itemA.placement.distFromStart ? itemA.placement.distFromStart : 0;
                                                    let distFromStartB = itemB.placement.distFromStart ? itemB.placement.distFromStart : 0;
                                                    return (distFromStartA < distFromStartB) ? 1 : (distFromStartA > distFromStartB) ? -1 : ((currI_A > currI_B) ? 1 : (currI_A < currI_B) ? -1 : 0);
                                                } else return (currI_A > currI_B) ? 1 : (currI_A < currI_B) ? -1 : 0;
                                            }
                                            // if it's the same section order by shelf    
                                            else if (currSE_A === currSE_B)
                                                return (currSH_A > currSH_B) ? 1 : (currSH_A < currSH_B) ? -1 : 0;
                                            else return (currSE_A > currSE_B) ? 1 : (currSE_A < currSE_B) ? -1 : 0;
                                        });
                                        console.log('sorted multiSelectId', multiSelectId);
                                        // go over each item in the multiSelectId, find the item in the store and duplicate it in the new shelf.id
                                        for (let i = multiSelectId.length - 1; i >= 0; i--) {
                                            let currSE: number = parseInt(multiSelectId[i].substring(multiSelectId[i].indexOf('SE') + 2, multiSelectId[i].indexOf('SH')));
                                            let currSH: number = parseInt(multiSelectId[i].substring(multiSelectId[i].indexOf('SH') + 2, multiSelectId[i].indexOf('I')));
                                            let currI: number = parseInt(multiSelectId[i].substring(multiSelectId[i].indexOf('I') + 1));
                                            let item: any = null;
                                            if (currentAisle && currentAisle.sections[currSE]
                                                && currentAisle.sections[currSE].shelves[currSH]
                                                && currentAisle.sections[currSE].shelves[currSH].items[currI]) {
                                                item = JSON.parse(JSON.stringify(currentAisle.sections[currSE].shelves[currSH].items[currI]));
                                            }
                                            /* Barak 29.9.20 */
                                            let distFromStartInit = localStorage.getItem('distFromStart');
                                            let distFromStart = 0;
                                            if (distFromStartInit) distFromStart = parseInt(distFromStartInit);
                                            if (i === 0 && item) {
                                                item.placement.distFromStart = distFromStart;
                                                combined += distFromStart + (item.placement.pWidth * item.placement.faces);
                                            }
                                            else {
                                                if (item) {
                                                    item.placement.distFromStart = combined;
                                                    combined += item.placement.pWidth * item.placement.faces;
                                                }
                                            }
                                            console.log('before createShelfItem 1', item, distFromStart);
                                            /*****************/
                                            // await createShelfItem(shelf.id, item);
                                            await this.addShelfItem(this.props.aisleShelves[shelf.id], item);
                                            /* Barak 26.11.20 */ if (this.props.aisleShelves[shelf.id].freezer === 2) await this.alignFreezerToDown(this.props.aisleShelves[shelf.id]);
                                            /* Barak 26.11.20 */ if (this.props.aisleShelves[shelf.id].freezer === 2) console.log('2595 after alignFreezerToDown', JSON.parse(JSON.stringify(this.props.aisleShelves[shelf.id].items)));
                                            /* Barak 22.11.20 - Add saving array for aisle, allowing to do 
                                              Ctrl+Z to move back and Ctrl+Y to move forward */
                                            await this.saveSnapShot(this.props.aisleShelves[shelf.id]);
                                            /*************************************************************/
                                        }
                                        /* Barak 29.9.20 */
                                        // after adding item we need to re-order the shelf
                                        // this.reOrderThisShelfPosition(shelf);
                                        await this.addShelfItem(this.props.aisleShelves[shelf.id], blankItem);
                                        /* Barak 26.11.20 */ if (this.props.aisleShelves[shelf.id].freezer === 2) console.log('2620 after alignFreezerToDown', JSON.parse(JSON.stringify(this.props.aisleShelves[shelf.id].items)));
                                        // sort the multiSelectId array again, this time by index only
                                        multiSelectId.sort(function (a: any, b: any) {
                                            let currSE_A: number = parseInt(a.substring(a.indexOf('SE') + 2, a.indexOf('SH')));
                                            let currSH_A: number = parseInt(a.substring(a.indexOf('SH') + 2, a.indexOf('I')));
                                            let currI_A: number = parseInt(a.substring(a.indexOf('I') + 1));
                                            let currSE_B: number = parseInt(b.substring(b.indexOf('SE') + 2, b.indexOf('SH')));
                                            let currSH_B: number = parseInt(b.substring(b.indexOf('SH') + 2, b.indexOf('I')));
                                            let currI_B: number = parseInt(b.substring(b.indexOf('I') + 1));
                                            // if it's the same section and shelf, order by item index
                                            if (currSE_A === currSE_B && currSH_A === currSH_B)
                                                return (currI_A > currI_B) ? 1 : (currI_A < currI_B) ? -1 : 0;
                                            // if it's the same section order by shelf    
                                            else if (currSE_A === currSE_B)
                                                return (currSH_A > currSH_B) ? 1 : (currSH_A < currSH_B) ? -1 : 0;
                                            else return (currSE_A > currSE_B) ? 1 : (currSE_A < currSE_B) ? -1 : 0;
                                        });
                                        for (let i = multiSelectId.length - 1; i >= 0; i--) {
                                            // find the shelf the item came from
                                            let currSHNew = multiSelectId[i].substring(0, multiSelectId[i].indexOf('I'));
                                            // delete the item on that shelf
                                            await deleteItem(currSHNew, multiSelectId[i]);
                                        }
                                        // once we deleted all the original items we can align to left if needed on all shelvs involved
                                        if (this.props.alignToLeft) {
                                            for (let i = 0; i < multiSelectId.length; i++) {
                                                // if alignToLeft is true we need to immediately align the entire new shelf to the left 
                                                let currSHNew = multiSelectId[i].substring(0, multiSelectId[i].indexOf('I'));
                                                await this.alignThisShelfToLeft(this.props.aisleShelves[currSHNew]);
                                            }
                                        }
                                        // now we align to left the current shelf
                                        if (this.props.alignToLeft) await this.alignThisShelfToLeft(this.props.aisleShelves[shelf.id]);
                                        // since we deleted all the originals we now empty the multiSelectId array
                                        this.props.setMultiSelectId([]);
                                        /*****************/
                                        /* Barak 22.11.20 - Add saving array for aisle, allowing to do 
                                            Ctrl+Z to move back and Ctrl+Y to move forward */
                                        this.saveSnapShot(this.props.aisleShelves[shelf.id]);
                                        /*************************************************************/
                                    }
                                    else {
                                        /* Barak 29.9.20 * createShelfItem(shelf.id, source.payload); */
                                        /* Barak 29.9.20 */
                                        let distFromStartInit = localStorage.getItem('distFromStart');
                                        let distFromStart = 0;
                                        if (distFromStartInit) distFromStart = parseInt(distFromStartInit);
                                        // alert('qa'+distFromStart)
                                        console.log('before createShelfItem 2', distFromStart, source.payload);
                                        let item = JSON.parse(JSON.stringify(source.payload));
                                        item.id = '';
                                        item.placement.distFromStart = distFromStart;
                                        /* Barak 17.11.20 *    
                                        // recalculate item row based on new shelf depth
                                        let totalItemDepth = (item.placement.pDepth ? item.placement.pDepth : ProductDefaultDimensions.depth) * item.placement.row;
                                        if(totalItemDepth>this.props.aisleShelves[shelf.id].dimensions.depth){
                                            item.placement.row = Math.floor(this.props.aisleShelves[shelf.id].dimensions.depth / (item.placement.pDepth ? item.placement.pDepth : ProductDefaultDimensions.depth));
                                        }
                                        ******************/
                                        /* Barak 17.11.20 * - at Bar's request - always recalculate the row value of the item based on the shelf you're dropping it into */
                                        item.placement.row = Math.floor(this.props.aisleShelves[shelf.id].dimensions.depth / (item.placement.pDepth ? item.placement.pDepth : ProductDefaultDimensions.depth));
                                        /*********************************************************************************************************************************/
                                        // find the shelf the item came from
                                        let currSHNew = source.payload.id.substring(0, source.payload.id.indexOf('I'));
                                        // delete the item on that shelf
                                        await deleteItem(currSHNew, source.payload.id);
                                        // await createShelfItem(shelf.id, item);
                                        await this.addShelfItem(this.props.aisleShelves[shelf.id], item);
                                        // after adding item we need to re-order the shelf
                                        // this.reOrderThisShelfPosition(shelf);
                                        // if alignToLeft is true we need to immediately align the entire new shelf to the left 
                                        if (this.props.alignToLeft) {
                                            await this.alignThisShelfToLeft(this.props.aisleShelves[currSHNew]);
                                            await this.alignThisShelfToLeft(this.props.aisleShelves[shelf.id]);
                                            //alignThisShelfToLeft(this.props.store, this.props.aisleShelves[shelf.id], this.props.aisleShelves, this.props.shelvesDetails);
                                        }
                                        /*****************/
                                        /* Barak 22.11.20 - Add saving array for aisle, allowing to do 
                                            Ctrl+Z to move back and Ctrl+Y to move forward */
                                        this.saveSnapShot(this.props.aisleShelves[shelf.id]);
                                        /*************************************************************/
                                    }
                                    // }
                                    /******************************************************************************************************/
                                }
                            }
                            }}
                            onDragEnd={async (source, dropResult) => {
                                if (dropResult.type === PlanogramDragDropTypes.TRASH) {
                                    await deleteShelf(section, shelf.id);
                                    let shelfIndex = section.shelves.findIndex((s:any) => s === shelf || s.id === shelf.id);
                                    if (shelfIndex >= 0) {
                                        section.shelves.splice(shelfIndex, 1);
                                    }
                                    await this.saveSnapShotDelete(shelf);
                                }
                                else if (dropResult.type === PlanogramDragDropTypes.SHELF) {
                                    if (shelf.id !== dropResult.payload.id) {
                                        await switchShelves(shelf.id, dropResult.payload.id);
                                        await this.saveSnapShot(shelf);
                                    }
                                }
                            }}
                        />
                    </MenuProvider>
                })}
                <div className="section-title">
                    {formatMessage('planogram-area-title', "AREA")}: {groupTag}
                </div>
                {section.section_layout > 0 ? <div style={{
                    position: "absolute", top: "-0.6em", height: "1.5em", background: "#273a48", color: "white",
                    display: "block", zIndex: 320, padding: "3px", width: "100%", fontWeight: 'bold',
                    textAlign: "center"
                }}>
                    {sectionName}
                </div> : ""}
                {section.dimensions ? <div className="section-dimensions">
                    {dimensionText(section.dimensions)}
                </div> : ""}
            </div>
        ));
    }
}


export default DraggableSource(DroppableTarget(StoreConnector(PlanogramSectionComponentTarget)));
