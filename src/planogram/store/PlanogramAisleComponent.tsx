import * as React from "react";
import { PlanogramDragDropTypes } from "../generic/DragAndDropType";
import PlanogramSectionComponent from "./PlanogramSectionComponent";
import { DragDropResultBase } from "../generic/DragDropWrapper";
import { DragElementWrapper, DragSource, DropTarget } from "react-dnd";
import { AppState } from "@src/planogram/shared/store/app.reducer";
import { Dispatch, Action } from "redux";
import { connect } from "react-redux";
import { setZoom } from "@src/planogram/shared/store/zoom/zoom.actions";
import { addShelfAction, duplicateShelfAction } from "@src/planogram/shared/store/planogram/store/shelf/shelf.actions";
import { deleteSectionAction, addSectionAction, duplicateSectionAction, editSectionDimensionAction, switchSectionsAction, removeSectionItemsAction } from "@src/planogram/shared/store/planogram/store/section/section.actions";
import { addShelfActionProps } from "@src/planogram/shared/store/planogram/store/shelf/shelf.types";
import { FormattedMessage, useIntl } from "react-intl"
import { NEWPLANO_ACTION } from "@src/planogram/shared/store/newPlano/newPlano.types";
import classnames from 'classnames';
import Draggable from "react-draggable";
import { addSectionActionProps } from "@src/planogram/shared/store/planogram/store/section/section.types";
import { DimensionModal } from "../modals/DimensionModal";
import { ShelfDefaultDimension, SectionMaxDimension, ProductDefaultDimensions, SectionDefaultDimension, blankPlanogramAisle } from "@src/planogram/shared/store/planogram/planogram.defaults";
import { calculateShelvesHeight } from "../provider/calculation.service";
import * as planogramApi from "@src/planogram/shared/api/planogram.provider";
import { editProductDimensions } from "@src/planogram/shared/store/catalog/catalog.action";
import { EditableText } from "@src/planogram/shared/components/Form";
import { editAisleName, removeAisleAction, setAisleAction } from "@src/planogram/shared/store/planogram/store/aisle/aisle.actions";
import { PlanogramAisle, PlanogramSection, PlanogramShelf, DimensionObject, PlanogramItem, PlanogramElementId, PlanogramStore } from "@src/planogram/shared/store/planogram/planogram.types";
import { RouteComponentProps, withRouter } from "react-router";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSave, faTrash, faUpload, faDownload, faEraser, faAlignLeft, faAlignRight, faAlignJustify, faCloudUploadAlt, faPrint } from "@fortawesome/free-solid-svg-icons";
import { FileLoadingButton, JsonDownloadButton, fileReadyCurrentTime } from "@src/planogram/generic/MenuButtons";
import { Menu, contextMenu, MenuProvider } from "@src/planogram/generic/ContextMenu";
import { TransformWrapper, TransformComponent } from "react-zoom-pan-pinch";
import { setMultiSelectId, hideProductDetailer, setAisleSaveArray, setCurrentSelectedId, setAisleSaveIndex } from "@src/planogram/shared/store/planogram/planogram.actions";
import * as planogramProvider from "@src/planogram/shared/api/planogram.provider";
import { SelectBox } from "devextreme-react";
import { setStore } from "@src/planogram/shared/store/planogram/store/store.actions";
import { SetSummaryObjects } from "@src/planogram/shared/store/planogram/display/display.actions";
import { SaveAisle, FlashBtn } from '@src/helpers/planogram';
import { Modal, ModalBody, ModalFooter, Button } from 'reactstrap'
import { ModalHeader } from '@src/components/modal';

interface SourceCollectProps {
    connectDragSource: DragElementWrapper<any>,
    isDragging: boolean,
}

interface TargetCollectProps {
    connectDropTarget: DragElementWrapper<any>,
    canDrop: boolean,
}

interface ComponentProps {
    aisle: PlanogramAisle,
    AisleRef: any,
    printStatus: boolean,
    printFromInput: number,
    printToInput: number,
    onDrop?: (item: DragDropResultBase) => void,
    onDragEnd?: (item: DragDropResultBase, targetItem: DragDropResultBase) => void,
}
const DraggableSource = DragSource<ComponentProps, SourceCollectProps>(PlanogramDragDropTypes.AISLE, {
    beginDrag(props, monitor) {
        const { aisle } = props;
        return ({
            type: monitor.getItemType(),
            payload: aisle,
        });
    },
    endDrag(props, monitor) {
        if (!monitor.didDrop())
            return;
        if (props.onDragEnd)
            props.onDragEnd(monitor.getItem(), monitor.getDropResult());
    },
    canDrag(props, monitor) {
        return false;
    }
}, (connect, monitor) => ({
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging(),
}));

const dropTypes = [
    PlanogramDragDropTypes.SECTION_SIDEBAR,
    PlanogramDragDropTypes.PRODUCT_SIDEBAR,
    PlanogramDragDropTypes.SERIES_SIDEBAR,
    PlanogramDragDropTypes.PRODUCT,
    PlanogramDragDropTypes.SHELF,
    PlanogramDragDropTypes.SHELF_SIDEBAR,
    PlanogramDragDropTypes.SECTION,
    PlanogramDragDropTypes.SHELF_SIDEBAR_FREEZER,
];

const DroppableTarget = DropTarget<ComponentProps, TargetCollectProps>(
    dropTypes, {
    canDrop: (props, monitor) => {
        if (monitor.didDrop() || !monitor.isOver({ shallow: true }))
            return false;

        return true;
    },
    drop(props, monitor) {
        const { aisle } = props;
        if (props.onDrop)
            props.onDrop(monitor.getItem());
        return ({
            type: monitor.getItemType(),
            payload: aisle,
            distFromStart: 'aisle'
        });
    }
}, (connect, monitor) => ({
    connectDropTarget: connect.dropTarget(),
    canDrop: monitor.canDrop(),
}));


type PlanogramAisleProps = ComponentProps & SourceCollectProps & TargetCollectProps;

function mapDispatchToProps(dispatch: Dispatch) {
    return {
        addSection: (aisle: PlanogramElementId, data: addSectionActionProps) => dispatch(addSectionAction(aisle, data)),
        deleteSection: (aisleId: PlanogramElementId, section: PlanogramSection) => {
            dispatch(deleteSectionAction(aisleId, section));
        },
        switchSections: (base: PlanogramSection, remote: PlanogramSection) => {
            dispatch(switchSectionsAction(base, remote));
        },
        removeSectionItemsAction: (aisle: PlanogramElementId, section: PlanogramElementId) => {
            dispatch(removeSectionItemsAction(aisle, section));
        },
        addShelf: (section: PlanogramSection, data?: addShelfActionProps) => {
            dispatch(addShelfAction(section, data));
        },
        duplicateShelf: (section: PlanogramSection, shelf: PlanogramShelf) => {
            dispatch(duplicateShelfAction(section, shelf));
        },
        editSectionDimensions: (section: PlanogramSection, dimension: DimensionObject) => {
            dispatch(editSectionDimensionAction(section, dimension));
        },
        updateProductDimensions: (barcode: number, dimensions: DimensionObject, afterAction: Action) => {
            dispatch(editProductDimensions(barcode, dimensions));
            dispatch(afterAction);
        },
        setCurrentSelectedId: (currentSelectedId: string) => dispatch(setCurrentSelectedId(currentSelectedId)),
        setMultiSelectId: (multiSelectId: string[]) => dispatch(setMultiSelectId(multiSelectId)),
        hideProductDisplayerBarcode: () => {
            dispatch(hideProductDetailer());
        },
        setStoreAisle: (aisle: PlanogramAisle, aislePid?: PlanogramElementId) => {
            dispatch(setAisleAction(aisle, aislePid));
        },
        setZoom: (zoom: number) => { dispatch(setZoom(zoom)); },
        setAisleSaveArray: (aisleSaveArray: PlanogramAisle[]) => {
            dispatch(setAisleSaveArray(aisleSaveArray));
        },
        setAisleSaveIndex: (aisleSaveIndex: number) => {
            dispatch(setAisleSaveIndex(aisleSaveIndex));
        },
        setStore: (store: PlanogramStore) => { dispatch(setStore(store)); },
    }
}
function mapStateToProps(state: AppState, ownProps: PlanogramAisleProps) {
    return {
        planogram: state.planogram,
        store: state.planogram.store,
        multiSelectId: state.planogram.display.multiSelectId,
        aisleSaveArray: state.planogram.display.aisleSaveArray,
        aisleSaveIndex: state.planogram.display.aisleSaveIndex,
        shelvesDetails: state.planogram.virtualStore.shelfDetails,
        aisleShelves: state.planogram.virtualStore.shelfMap,
        layouts: state.system.data.layouts,
        user: state.auth.user,
        ...state.newPlano,
        ...ownProps
    };
}

type SectionStoreProps = ReturnType<typeof mapDispatchToProps> & ReturnType<typeof mapStateToProps>;

let globalLastChangeSaved = true;
class PlanogramAisleComponent extends React.Component<SectionStoreProps> {
    state = { isLoading: false, duplicateModal:false,duplicateAmount:0,activeButton:null,duplicateData:{onClick:()=>{}} }
    alignThisSectionToLeft = async (section: PlanogramSection) => {
        const { store } = this.props;
        const { aisleShelves, shelvesDetails } = this.props;

        // check if we need to align multiple sections
        let all_sections_to_align: PlanogramSection[] = [];
        if (this.props.multiSelectId.length > 1) {
            // sort multiSelectId by shelf and section
            this.props.multiSelectId.sort(function (a: any, b: any) {
                let currSE_A: number = parseInt(a.substring(a.indexOf('SE') + 2, a.indexOf('SH')));
                let currSH_A: number = parseInt(a.substring(a.indexOf('SH') + 2, a.indexOf('I')));
                let currSE_B: number = parseInt(b.substring(b.indexOf('SE') + 2, b.indexOf('SH')));
                let currSH_B: number = parseInt(b.substring(b.indexOf('SH') + 2, b.indexOf('I')));
                // if it's the same shelf order by section    
                if (currSH_A === currSH_B)
                    return (currSE_A > currSE_B) ? 1 : (currSE_A < currSE_B) ? -1 : 0;
                else return (currSH_A > currSH_B) ? 1 : (currSH_A < currSH_B) ? -1 : 0;
            });
            // find each section represented in multiSelectId and add them to all_sections_to_align
            for (let a = 0; a < this.props.multiSelectId.length; a++) {
                let target_A = this.props.multiSelectId[a].substring(0, this.props.multiSelectId[a].indexOf('SE'));
                let target_SE = parseInt(this.props.multiSelectId[a].substring(this.props.multiSelectId[a].indexOf('SE') + 2, this.props.multiSelectId[a].indexOf('SH')));
                let target_SH = parseInt(this.props.multiSelectId[a].substring(this.props.multiSelectId[a].indexOf('SH') + 2));
                if (store) {
                    for (let i = 0; i < store.aisles.length; i++) {
                        if (store.aisles[i].id === target_A) {
                            let thisSection = store.aisles[i].sections[target_SE];
                            let thisShelf = store.aisles[i].sections[target_SE].shelves[target_SH];
                            let index = all_sections_to_align.findIndex(section => section.id === thisSection.id);
                            if (index < 0) all_sections_to_align.push(thisSection);
                            // now that we've added the section the item is actually on, we need to check if the item covers more than one section.
                            // since we know this is the shelf the item starts in we can get the item's details from it.
                            let item = thisShelf.items.filter(line => line.id === this.props.multiSelectId[a]);
                            let itemTotalWidth = (item[0].placement.distFromStart ? item[0].placement.distFromStart : 0) + ((item[0].placement.pWidth ? item[0].placement.pWidth : ProductDefaultDimensions.width) * item[0].placement.faces);
                            let shelfWidth = thisShelf.dimensions.width;
                            if (itemTotalWidth > shelfWidth) {
                                while (itemTotalWidth > shelfWidth) {
                                    target_SE++;
                                    let nextShelf = store.aisles[i].sections[target_SE].shelves[target_SH];
                                    let nextSection = store.aisles[i].sections[target_SE];
                                    let index = all_sections_to_align.findIndex(section => section.id === nextSection.id);
                                    if (index < 0) all_sections_to_align.push(nextSection);
                                    itemTotalWidth -= shelfWidth;
                                    shelfWidth = nextShelf.dimensions.width;
                                }
                            }
                        }
                    }
                }
            }

        } else all_sections_to_align.push(section);

        // now that we have all sections that need to be aligned we can align them one after the other
        for (let aa = 0; aa < all_sections_to_align.length; aa++) {
            // this function will only align items on shelves in this section
            // we'll go over each shelf and align it without looking at continuous shelves
            for (let i = 0; i < all_sections_to_align[aa].shelves.length; i++) {
                let thisShelf = all_sections_to_align[aa].shelves[i];
                if (thisShelf.freezer === 1) continue;

                // this is the data for the actual shelf
                let shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[thisShelf.id]));

                // no matter where the user clicked we need to go back and work on the main shelf
                while (shelfDetails.main_shelf) {
                    let ids = JSON.parse(JSON.stringify(shelfDetails.main_shelf));
                    shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[ids]));
                    thisShelf = aisleShelves[ids];
                }

                if (shelfDetails == null) {
                    return null;
                }

                let { margin_bottom, display, combined } = shelfDetails;
                if (margin_bottom == null) margin_bottom = 0;
                if (display == null) display = true;
                if (combined == null) combined = [];

                let realWidth = thisShelf.dimensions.width + (display && combined.length > 0 ? combined.map((sh: any) => {
                    const aShelf = aisleShelves[sh];
                    return aShelf.dimensions.width;
                }).reduce((a: any, b: any) => a + b) : 0);
                let combinedShelvesFirst = [thisShelf].concat(combined.map((id: any) => aisleShelves[id]));

                let combinedShelves: any[] = [];
                // remove from combinedShelves all shelves from sections that are not listed in all_sections_to_align
                for (let m = 0; m < combinedShelvesFirst.length; m++) {
                    let index = all_sections_to_align.findIndex(line => line.id === combinedShelvesFirst[m].id.substring(0, combinedShelvesFirst[m].id.indexOf('SH')));
                    if (index >= 0) combinedShelves.push(combinedShelvesFirst[m]);
                }

                // let shelfItems = combinedShelves.map(sh => sh.items).reduce((p, c) => p.concat(c));
                // save initial state of shelvs with items ordered by distFromStart (distance from start of shelf)
                for (let i = 0; i < combinedShelves.length; i++) {
                    // sort shelf items by distFromStart
                    combinedShelves[i].items.sort(function (a: any, b: any) {
                        let aNumber = a.placement.distFromStart ? a.placement.distFromStart : 0;
                        let bNumber = b.placement.distFromStart ? b.placement.distFromStart : 0;
                        let aId = a.Id;
                        let bId = b.Id;
                        return (aNumber > bNumber) ? 1 : (aNumber < bNumber) ? -1 : ((aId > bId) ? 1 : (aId < bId) ? -1 : 0);
                    });
                }
                let viewCombinedShelves = JSON.parse(JSON.stringify(combinedShelves));
                // empty all items from current shelvs
                for (let i = 0; i < combinedShelves.length; i++) {
                    if (combinedShelves[i].freezer != 1 && combinedShelves[i].freezer != 2) combinedShelves[i].items = [];
                }

                let distFromStart = 0;
                let distInShelf = 0;
                let index = 0;
                let startOfShelfId = '';
                let spaceInShelf = 0;
                // go over the initial state and populate each shelf in combinedShelves with the correct items in the correct order
                let shelfId = 0;
                for (let i = 0; i < viewCombinedShelves.length; i++) {
                    if (viewCombinedShelves[i].freezer === 1) {
                        continue;
                    };
                    if (i === 0) {
                        startOfShelfId = viewCombinedShelves[i].id;
                        spaceInShelf = viewCombinedShelves[shelfId].dimensions.width;
                    }
                    for (let n = 0; n < viewCombinedShelves[i].items.length; n++) {
                        let item = JSON.parse(JSON.stringify(viewCombinedShelves[i].items[n]));
                        /* Barak 26.11.20 - return items above main line (item index >= 100) back into the shelf without altering anything else */
                        let itemInd = parseInt(item.id.substring(item.id.indexOf('I') + 1));
                        if (itemInd >= 100) {
                            let shelfInd = combinedShelves.findIndex(line => line.id === item.id.substring(0, item.id.indexOf('I')));
                            if (shelfInd >= 0) combinedShelves[shelfInd].items.push(item);
                        }
                        else {
                            /************************************************************************************************************************/
                            if (spaceInShelf <= 0) {
                                shelfId++; // the shelf id in the viewCombinedShelves array
                                if (viewCombinedShelves[shelfId]) {
                                    // there is a next shelf so we update the shelfNumber, startOfShelfId, spaceInShelf and index
                                    // let shelfNumber = parseInt(JSON.parse(JSON.stringify(startOfShelfId.substring(startOfShelfId.indexOf('SE') + 2, startOfShelfId.indexOf('SH'))))) + 1;
                                    let old_sec_num = parseInt(JSON.parse(JSON.stringify(startOfShelfId.substring(startOfShelfId.indexOf('SE') + 2, startOfShelfId.indexOf('SH')))));
                                    let new_sec_num = parseInt(JSON.parse(JSON.stringify(viewCombinedShelves[shelfId].id.substring(viewCombinedShelves[shelfId].id.indexOf('SE') + 2, viewCombinedShelves[shelfId].id.indexOf('SH')))))
                                    startOfShelfId = JSON.parse(JSON.stringify(startOfShelfId.substring(0, startOfShelfId.indexOf('SE') + 2) + new_sec_num + startOfShelfId.substring(startOfShelfId.indexOf('SH'))));
                                    if (new_sec_num - old_sec_num === 1) {
                                        distInShelf = (spaceInShelf * -1);
                                        distFromStart = (spaceInShelf * -1);
                                        spaceInShelf = viewCombinedShelves[shelfId].dimensions.width - distInShelf;
                                    } else {
                                        distInShelf = 0;
                                        distFromStart = 0;
                                        spaceInShelf = 0;
                                    }
                                    index = 0;
                                } else
                                    // this shelf dosn't exist in the arry so we stay in this shelf
                                    shelfId--;
                            }
                            item.id = startOfShelfId + 'I' + index;
                            index++;
                            item.placement.distFromStart = distFromStart;
                            let itemWidth = viewCombinedShelves[i].items[n].placement.pWidth;
                            distFromStart += (itemWidth != undefined ? itemWidth : ProductDefaultDimensions.width) * item.placement.faces;
                            distInShelf += (itemWidth != undefined ? itemWidth : ProductDefaultDimensions.width) * item.placement.faces;
                            if (viewCombinedShelves[shelfId]) {
                                spaceInShelf = viewCombinedShelves[shelfId].dimensions.width - distInShelf;
                                combinedShelves[shelfId].items.push(item);
                            }
                        }
                    }
                }
            }
        }



        let aisle: PlanogramAisle = JSON.parse(JSON.stringify(blankPlanogramAisle));
        let aisles = this.props.store ? this.props.store.aisles : undefined;
        let aisleId = section.id.substring(0, section.id.indexOf('SE'));
        if (aisles) {
            for (let i = 0; i < aisles.length; i++) {
                if (aisles[i].id === aisleId) {
                    aisle = aisles[i];
                    break;
                }
            }
        }
        if (aisle.id === aisleId) {
            await this.props.setStoreAisle(aisle);
            /* Barak 21.12.20 */ await this.checkLevel2(aisle);
        }
    }

    /* Barak 29.9.20 - add function to align-justify section */
    alignThisSectionToJustify = async (section: PlanogramSection) => {
        const { store } = this.props;
        const { aisleShelves, shelvesDetails } = this.props;

        // check if we need to align multiple sections
        let all_sections_to_align: PlanogramSection[] = [];
        if (this.props.multiSelectId.length > 1) {
            // sort multiSelectId by shelf and section
            this.props.multiSelectId.sort(function (a: any, b: any) {
                let currSE_A: number = parseInt(a.substring(a.indexOf('SE') + 2, a.indexOf('SH')));
                let currSH_A: number = parseInt(a.substring(a.indexOf('SH') + 2, a.indexOf('I')));
                let currSE_B: number = parseInt(b.substring(b.indexOf('SE') + 2, b.indexOf('SH')));
                let currSH_B: number = parseInt(b.substring(b.indexOf('SH') + 2, b.indexOf('I')));
                // if it's the same shelf order by section    
                if (currSH_A === currSH_B)
                    return (currSE_A > currSE_B) ? 1 : (currSE_A < currSE_B) ? -1 : 0;
                else return (currSH_A > currSH_B) ? 1 : (currSH_A < currSH_B) ? -1 : 0;
            });
            // find each section represented in multiSelectId and add them to all_sections_to_align
            for (let a = 0; a < this.props.multiSelectId.length; a++) {
                let target_A = this.props.multiSelectId[a].substring(0, this.props.multiSelectId[a].indexOf('SE'));
                let target_SE = parseInt(this.props.multiSelectId[a].substring(this.props.multiSelectId[a].indexOf('SE') + 2, this.props.multiSelectId[a].indexOf('SH')));
                let target_SH = parseInt(this.props.multiSelectId[a].substring(this.props.multiSelectId[a].indexOf('SH') + 2));
                if (store) {
                    for (let i = 0; i < store.aisles.length; i++) {
                        if (store.aisles[i].id === target_A) {
                            let thisSection = store.aisles[i].sections[target_SE];
                            let thisShelf = store.aisles[i].sections[target_SE].shelves[target_SH];
                            let index = all_sections_to_align.findIndex(section => section.id === thisSection.id);
                            if (index < 0) all_sections_to_align.push(thisSection);
                            // now that we've added the section the item is actually on, we need to check if the item covers more than one section.
                            // since we know this is the shelf the item starts in we can get the item's details from it.
                            let item = thisShelf.items.filter((line:any) => line.id === this.props.multiSelectId[a]);
                            let itemTotalWidth = (item[0].placement.distFromStart ? item[0].placement.distFromStart : 0) + ((item[0].placement.pWidth ? item[0].placement.pWidth : ProductDefaultDimensions.width) * item[0].placement.faces);
                            let shelfWidth = thisShelf.dimensions.width;
                            if (itemTotalWidth > shelfWidth) {
                                while (itemTotalWidth > shelfWidth) {
                                    target_SE++;
                                    let nextShelf = store.aisles[i].sections[target_SE].shelves[target_SH];
                                    let nextSection = store.aisles[i].sections[target_SE];
                                    let index = all_sections_to_align.findIndex(section => section.id === nextSection.id);
                                    if (index < 0) all_sections_to_align.push(nextSection);
                                    itemTotalWidth -= shelfWidth;
                                    shelfWidth = nextShelf.dimensions.width;
                                }
                            }
                        }
                    }
                }
            }

        } else all_sections_to_align.push(section);

        // now that we have all sections that need to be aligned we can align them one after the other
        for (let aa = 0; aa < all_sections_to_align.length; aa++) {
            // this function will only align items on shelves in this section
            // we'll go over each shelf and align it without looking at continuous shelves
            for (let i = 0; i < all_sections_to_align[aa].shelves.length; i++) {
                let thisShelf = all_sections_to_align[aa].shelves[i];
                if (thisShelf.freezer === 1) continue;


                // this is the data for the actual shelf
                let shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[thisShelf.id]));

                // no matter where the user clicked we need to go back and work on the main shelf
                while (shelfDetails.main_shelf) {
                    let ids = JSON.parse(JSON.stringify(shelfDetails.main_shelf));
                    shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[ids]));
                    thisShelf = aisleShelves[ids];
                }

                if (shelfDetails == null) {
                    return null;
                }

                let { margin_bottom, display, combined } = shelfDetails;
                if (margin_bottom == null) margin_bottom = 0;
                if (display == null) display = true;
                if (combined == null) combined = [];

                let realWidth = thisShelf.dimensions.width + (display && combined.length > 0 ? combined.map((sh: any) => {
                    const aShelf = aisleShelves[sh];
                    return aShelf.dimensions.width;
                }).reduce((a: any, b: any) => a + b) : 0);
                let combinedShelvesFirst = [thisShelf].concat(combined.map((id: any) => aisleShelves[id]));

                let combinedShelves: any[] = [];
                // remove from combinedShelves all shelves from sections that are not listed in all_sections_to_align
                for (let m = 0; m < combinedShelvesFirst.length; m++) {
                    let index = all_sections_to_align.findIndex(line => line.id === combinedShelvesFirst[m].id.substring(0, combinedShelvesFirst[m].id.indexOf('SH')));
                    if (index >= 0) combinedShelves.push(combinedShelvesFirst[m]);
                }

                // save initial state of shelvs with items ordered by distFromStart (distance from start of shelf)
                let combinedShelvesLength = 0;
                let combinedItems: PlanogramItem[] = [];
                for (let i = 0; i < combinedShelves.length; i++) {
                    // sort shelf items by distFromStart
                    combinedShelves[i].items.sort(function (a: any, b: any) {
                        let aNumber = a.placement.distFromStart ? a.placement.distFromStart : 0;
                        let bNumber = b.placement.distFromStart ? b.placement.distFromStart : 0;
                        let aId = a.Id;
                        let bId = b.Id;
                        return (aNumber > bNumber) ? 1 : (aNumber < bNumber) ? -1 : ((aId > bId) ? 1 : (aId < bId) ? -1 : 0);
                    });
                    combinedItems = combinedItems.concat(combinedShelves[i].items);
                    // get the combined length of the entire shelf
                    combinedShelvesLength += combinedShelves[i].dimensions.width;
                }
                /* Barak 26.11.20 - remove from combinedItems items with Index of over 100 (items above other items) *
                let tempCDombinedItems = [];
                for (let n = 0; n < combinedItems.length; n++) {
                    let itemInd = parseInt(combinedItems[n].id.substring(combinedItems[n].id.indexOf('I') + 1));
                    if (itemInd < 100) tempCDombinedItems.push(combinedItems[n]);
                }
                combinedItems = tempCDombinedItems;
                *****************************************************************************************************/
                let viewCombinedShelves = JSON.parse(JSON.stringify(combinedShelves));
                // empty all items from current shelvs
                for (let i = 0; i < combinedShelves.length; i++) {
                    if (combinedShelves[i].freezer != 1 && combinedShelves[i].freezer != 2) combinedShelves[i].items = [];
                }

                // get the combined length of all items on the shelf
                let combinedItemsLength = 0;
                for (let i = 0; i < combinedItems.length; i++) {
                    let itemWidth = combinedItems[i].placement.pWidth;
                    /* Barak 26.11.20 - count items directly on the shelf only (item index < 100) */
                    let itemInd = parseInt(combinedItems[i].id.substring(combinedItems[i].id.indexOf('I') + 1));
                    if (itemInd < 100)
                        /**************************************************************************/
                        combinedItemsLength += (itemWidth != undefined ? itemWidth : ProductDefaultDimensions.width) * combinedItems[i].placement.faces;
                }

                // get the space we need to add between each item on this shelf to get an even spread of items
                let evenSpaceAdded = (combinedShelvesLength - combinedItemsLength) / combinedItems.length;

                let distFromStart = 0;
                let distInShelf = 0;
                let index = 0;
                let startOfShelfId = '';
                let spaceInShelf = 0;
                // go over the initial state and populate each shelf in combinedShelves with the correct items in the correct order
                let shelfId = 0;
                let combinedItemsIndex = 0;
                for (let i = 0; i < viewCombinedShelves.length; i++) {
                    if (viewCombinedShelves[i].freezer === 1) {
                        continue;
                    };
                    if (i === 0) {
                        startOfShelfId = viewCombinedShelves[i].id;
                        spaceInShelf = viewCombinedShelves[shelfId].dimensions.width;
                    }
                    for (let n = 0; n < viewCombinedShelves[i].items.length; n++) {
                        let item = JSON.parse(JSON.stringify(viewCombinedShelves[i].items[n]));
                        /* Barak 26.11.20 - return items above main line (item index >= 100) back into the shelf without altering anything else */
                        let itemInd = parseInt(item.id.substring(item.id.indexOf('I') + 1));
                        if (itemInd >= 100) {
                            let shelfInd = combinedShelves.findIndex(line => line.id === item.id.substring(0, item.id.indexOf('I')));
                            if (shelfInd >= 0) combinedShelves[shelfInd].items.push(item);
                        }
                        else {
                            if (evenSpaceAdded > 0) {
                                // if there is just one item on the shelf
                                if (combinedItems.length === 1) {
                                    distFromStart += evenSpaceAdded / 2;
                                    distInShelf += evenSpaceAdded / 2;
                                    spaceInShelf -= evenSpaceAdded / 2;
                                }
                                // if there are two item on the shelf and this is the second item 
                                if (combinedItems.length === 2 && combinedItemsIndex === 1) {
                                    distFromStart += evenSpaceAdded * 2;
                                    distInShelf += evenSpaceAdded * 2;
                                    spaceInShelf -= evenSpaceAdded * 2;
                                }
                                // if there are more than two item on the shelf and this is NOT the first one or the last one
                                if (combinedItems.length > 2 && combinedItemsIndex > 0 && combinedItemsIndex < combinedItems.length) {
                                    distFromStart += evenSpaceAdded * (combinedItems.length / (combinedItems.length - 1));
                                    distInShelf += evenSpaceAdded * (combinedItems.length / (combinedItems.length - 1));
                                    spaceInShelf -= evenSpaceAdded * (combinedItems.length / (combinedItems.length - 1));
                                }
                            }
                            if (spaceInShelf <= 0) {
                                shelfId++; // the shelf id in the viewCombinedShelves array
                                if (viewCombinedShelves[shelfId]) {
                                    // there is a next shelf so we update the shelfNumber, startOfShelfId, spaceInShelf and index
                                    // let shelfNumber = parseInt(JSON.parse(JSON.stringify(startOfShelfId.substring(startOfShelfId.indexOf('SE') + 2, startOfShelfId.indexOf('SH'))))) + 1;
                                    let old_sec_num = parseInt(JSON.parse(JSON.stringify(startOfShelfId.substring(startOfShelfId.indexOf('SE') + 2, startOfShelfId.indexOf('SH')))));
                                    let new_sec_num = parseInt(JSON.parse(JSON.stringify(viewCombinedShelves[shelfId].id.substring(viewCombinedShelves[shelfId].id.indexOf('SE') + 2, viewCombinedShelves[shelfId].id.indexOf('SH')))))
                                    startOfShelfId = JSON.parse(JSON.stringify(startOfShelfId.substring(0, startOfShelfId.indexOf('SE') + 2) + new_sec_num + startOfShelfId.substring(startOfShelfId.indexOf('SH'))));
                                    if (new_sec_num - old_sec_num === 1) {
                                        distInShelf = (spaceInShelf * -1);
                                        distFromStart = (spaceInShelf * -1);
                                        spaceInShelf = viewCombinedShelves[shelfId].dimensions.width - distInShelf;
                                    } else {
                                        distInShelf = 0;
                                        distFromStart = 0;
                                        spaceInShelf = 0;
                                    }
                                    index = 0;
                                } else
                                    // this shelf dosn't exist in the arry so we stay in this shelf
                                    shelfId--;
                            }
                            item.id = startOfShelfId + 'I' + index;
                            index++;
                            item.placement.distFromStart = distFromStart;
                            let itemWidth = viewCombinedShelves[i].items[n].placement.pWidth;
                            distFromStart += (itemWidth != undefined ? itemWidth : ProductDefaultDimensions.width) * item.placement.faces;
                            distInShelf += (itemWidth != undefined ? itemWidth : ProductDefaultDimensions.width) * item.placement.faces;
                            combinedItemsIndex++;
                            if (viewCombinedShelves[shelfId]) {
                                spaceInShelf = viewCombinedShelves[shelfId].dimensions.width - distInShelf;
                                combinedShelves[shelfId].items.push(item);
                            }
                        }
                    }
                }
            }
        }

        let aisle: PlanogramAisle = JSON.parse(JSON.stringify(blankPlanogramAisle));
        let aisles = this.props.store ? this.props.store.aisles : undefined;
        let aisleId = section.id.substring(0, section.id.indexOf('SE'));
        if (aisles) {
            for (let i = 0; i < aisles.length; i++) {
                if (aisles[i].id === aisleId) {
                    aisle = aisles[i];
                    break;
                }
            }
        }
        if (aisle.id === aisleId) {
            await this.props.setStoreAisle(aisle);
            await this.checkLevel2(aisle);
        }
    }

    /*  add function to align section to the right */
    alignThisSectionToRight = async (section: PlanogramSection) => {
        const { store } = this.props;
        const { aisleShelves, shelvesDetails } = this.props;

        // check if we need to align multiple sections
        let all_sections_to_align: PlanogramSection[] = [];
        if (this.props.multiSelectId.length > 1) {
            // sort multiSelectId by shelf and section
            this.props.multiSelectId.sort(function (a: any, b: any) {
                let currSE_A: number = parseInt(a.substring(a.indexOf('SE') + 2, a.indexOf('SH')));
                let currSH_A: number = parseInt(a.substring(a.indexOf('SH') + 2, a.indexOf('I')));
                let currSE_B: number = parseInt(b.substring(b.indexOf('SE') + 2, b.indexOf('SH')));
                let currSH_B: number = parseInt(b.substring(b.indexOf('SH') + 2, b.indexOf('I')));
                // if it's the same shelf order by section    
                if (currSH_A === currSH_B)
                    return (currSE_A > currSE_B) ? 1 : (currSE_A < currSE_B) ? -1 : 0;
                else return (currSH_A > currSH_B) ? 1 : (currSH_A < currSH_B) ? -1 : 0;
            });
            // find each section represented in multiSelectId and add them to all_sections_to_align
            for (let a = 0; a < this.props.multiSelectId.length; a++) {
                let target_A = this.props.multiSelectId[a].substring(0, this.props.multiSelectId[a].indexOf('SE'));
                let target_SE = parseInt(this.props.multiSelectId[a].substring(this.props.multiSelectId[a].indexOf('SE') + 2, this.props.multiSelectId[a].indexOf('SH')));
                let target_SH = parseInt(this.props.multiSelectId[a].substring(this.props.multiSelectId[a].indexOf('SH') + 2));
                if (store) {
                    for (let i = 0; i < store.aisles.length; i++) {
                        if (store.aisles[i].id === target_A) {
                            let thisSection = store.aisles[i].sections[target_SE];
                            let thisShelf = store.aisles[i].sections[target_SE].shelves[target_SH];
                            let index = all_sections_to_align.findIndex(section => section.id === thisSection.id);
                            if (index < 0) all_sections_to_align.push(thisSection);
                            // now that we've added the section the item is actually on, we need to check if the item covers more than one section.
                            // since we know this is the shelf the item starts in we can get the item's details from it.
                            let item = thisShelf.items.filter(line => line.id === this.props.multiSelectId[a]);
                            let itemTotalWidth = (item[0].placement.distFromStart ? item[0].placement.distFromStart : 0) + ((item[0].placement.pWidth ? item[0].placement.pWidth : ProductDefaultDimensions.width) * item[0].placement.faces);
                            let shelfWidth = thisShelf.dimensions.width;
                            if (itemTotalWidth > shelfWidth) {
                                while (itemTotalWidth > shelfWidth) {
                                    target_SE++;
                                    let nextShelf = store.aisles[i].sections[target_SE].shelves[target_SH];
                                    let nextSection = store.aisles[i].sections[target_SE];
                                    let index = all_sections_to_align.findIndex(section => section.id === nextSection.id);
                                    if (index < 0) all_sections_to_align.push(nextSection);
                                    itemTotalWidth -= shelfWidth;
                                    shelfWidth = nextShelf.dimensions.width;
                                }
                            }
                        }
                    }
                }
            }

        } else all_sections_to_align.push(section);

        // now that we have all sections that need to be aligned we can align them one after the other
        for (let aa = 0; aa < all_sections_to_align.length; aa++) {
            // this function will only align items on shelves in this section
            // we'll go over each shelf and align it without looking at continuous shelves
            for (let i = all_sections_to_align[aa].shelves.length - 1; i >= 0; i--) {
                let thisShelf = all_sections_to_align[aa].shelves[i];
                if (thisShelf.freezer === 1) continue;

                // this is the data for the actual shelf
                let shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[thisShelf.id]));

                // no matter where the user clicked we need to go back and work on the main shelf
                while (shelfDetails.main_shelf) {
                    let ids = JSON.parse(JSON.stringify(shelfDetails.main_shelf));
                    shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[ids]));
                    thisShelf = aisleShelves[ids];
                }

                if (shelfDetails == null) {
                    return null;
                }

                let { margin_bottom, display, combined } = shelfDetails;
                if (margin_bottom == null) margin_bottom = 0;
                if (display == null) display = true;
                if (combined == null) combined = [];

                let realWidth = thisShelf.dimensions.width + (display && combined.length > 0 ? combined.map((sh: any) => {
                    const aShelf = aisleShelves[sh];
                    return aShelf.dimensions.width;
                }).reduce((a: any, b: any) => a + b) : 0);
                let combinedShelvesFirst = [thisShelf].concat(combined.map((id: any) => aisleShelves[id]));

                let combinedShelves: any[] = [];
                // remove from combinedShelves all shelves from sections that are not listed in all_sections_to_align
                for (let m = 0; m < combinedShelvesFirst.length; m++) {
                    let index = all_sections_to_align.findIndex(line => line.id === combinedShelvesFirst[m].id.substring(0, combinedShelvesFirst[m].id.indexOf('SH')));
                    if (index >= 0) combinedShelves.push(combinedShelvesFirst[m]);
                }

                // save initial state of shelvs with items ordered by distFromStart (distance from start of shelf)
                let combinedItems: PlanogramItem[] = [];
                let combinedShelvesLength = 0;
                for (let i = 0; i < combinedShelves.length; i++) {
                    // sort shelf items by distFromStart
                    combinedShelves[i].items.sort(function (a: any, b: any) {
                        let aNumber = a.placement.distFromStart ? a.placement.distFromStart : 0;
                        let bNumber = b.placement.distFromStart ? b.placement.distFromStart : 0;
                        let aId = a.Id;
                        let bId = b.Id;
                        return (aNumber > bNumber) ? 1 : (aNumber < bNumber) ? -1 : ((aId > bId) ? 1 : (aId < bId) ? -1 : 0);
                    });
                    combinedItems = combinedItems.concat(combinedShelves[i].items);
                    // get the combined length of the entire shelf
                    combinedShelvesLength += combinedShelves[i].dimensions.width;
                }
                let viewCombinedShelves = JSON.parse(JSON.stringify(combinedShelves));

                // get the combined length of all items on the shelf
                let combinedItemsLength = 0;
                for (let i = 0; i < combinedItems.length; i++) {
                    let itemWidth = combinedItems[i].placement.pWidth;
                    combinedItemsLength += (itemWidth != undefined ? itemWidth : ProductDefaultDimensions.width) * combinedItems[i].placement.faces;
                }

                // if the total length of the items is longer than the total width of the shelf DO NOT ALLOW TO ALIGN RIGHT
                if (combinedItemsLength > thisShelf.dimensions.width) {
                    // let errorMsg = 'לא ניתן לבצע הצמדה לימין במדף ' + thisShelf.id + ' - יש בו מוצרים גולשים';
                    let errorMsg = 'Cannot align shelf - ' + thisShelf.id + ' to the right - it has overflowing items';
                    alert(errorMsg);
                    continue;
                }

                // empty all items from current shelvs
                for (let i = 0; i < combinedShelves.length; i++) {
                    if (combinedShelves[i].freezer != 1 && combinedShelves[i].freezer != 2) combinedShelves[i].items = [];
                }

                let distFromStart = viewCombinedShelves[viewCombinedShelves.length - 1].dimensions.width;
                let distInShelf = viewCombinedShelves[viewCombinedShelves.length - 1].dimensions.width;
                let index = viewCombinedShelves[viewCombinedShelves.length - 1].items.length - 1;
                let startOfShelfId = '';
                let spaceInShelf = viewCombinedShelves[viewCombinedShelves.length - 1].dimensions.width;
                // go over the initial state and populate each shelf in combinedShelves with the correct items in the correct order
                let shelfId = viewCombinedShelves.length - 1;
                for (let i = viewCombinedShelves.length - 1; i >= 0; i--) {
                    if (viewCombinedShelves[i].freezer === 1) {
                        continue;
                    };
                    if (i === viewCombinedShelves.length - 1) {
                        startOfShelfId = viewCombinedShelves[i].id;
                        spaceInShelf = viewCombinedShelves[shelfId].dimensions.width;
                    }
                    for (let n = viewCombinedShelves[i].items.length - 1; n >= 0; n--) {
                        let item = JSON.parse(JSON.stringify(viewCombinedShelves[i].items[n]));
                        /*  return items above main line (item index >= 100) back into the shelf without altering anything else */
                        let itemInd = parseInt(item.id.substring(item.id.indexOf('I') + 1));
                        if (itemInd >= 100) {
                            let shelfInd = combinedShelves.findIndex(line => line.id === item.id.substring(0, item.id.indexOf('I')));
                            if (shelfInd >= 0) combinedShelves[shelfInd].items.push(item);
                        }
                        else {
                            if (spaceInShelf <= 0) {
                                shelfId--; // the shelf id in the viewCombinedShelves array
                                if (viewCombinedShelves[shelfId]) {
                                    // there is a next shelf so we update the shelfNumber, startOfShelfId, spaceInShelf and index
                                    // let shelfNumber = parseInt(JSON.parse(JSON.stringify(startOfShelfId.substring(startOfShelfId.indexOf('SE') + 2, startOfShelfId.indexOf('SH'))))) - 1;
                                    let old_sec_num = parseInt(JSON.parse(JSON.stringify(startOfShelfId.substring(startOfShelfId.indexOf('SE') + 2, startOfShelfId.indexOf('SH')))));
                                    let new_sec_num = parseInt(JSON.parse(JSON.stringify(viewCombinedShelves[shelfId].id.substring(viewCombinedShelves[shelfId].id.indexOf('SE') + 2, viewCombinedShelves[shelfId].id.indexOf('SH')))))
                                    startOfShelfId = JSON.parse(JSON.stringify(startOfShelfId.substring(0, startOfShelfId.indexOf('SE') + 2) + new_sec_num + startOfShelfId.substring(startOfShelfId.indexOf('SH'))));
                                    // startOfShelfId = JSON.parse(JSON.stringify(startOfShelfId.substring(0, startOfShelfId.indexOf('SE') + 2) + shelfNumber + startOfShelfId.substring(startOfShelfId.indexOf('SH'))));
                                    if (new_sec_num - old_sec_num === -1) {
                                        distInShelf = viewCombinedShelves[viewCombinedShelves.length - 1].dimensions.width - (spaceInShelf * -1);
                                        distFromStart = viewCombinedShelves[viewCombinedShelves.length - 1].dimensions.width - (spaceInShelf * -1);
                                        spaceInShelf = distInShelf; // viewCombinedShelves[shelfId].dimensions.width - distInShelf;
                                    } else {
                                        distInShelf = viewCombinedShelves[viewCombinedShelves.length - 1].dimensions.width;
                                        distFromStart = viewCombinedShelves[viewCombinedShelves.length - 1].dimensions.width;
                                        spaceInShelf = distInShelf;;
                                    }
                                    index = viewCombinedShelves[shelfId].items.length - 1;
                                } else
                                    // this shelf dosn't exist in the arry so we stay in this shelf
                                    shelfId++;
                                console.log('startOfShelfId', startOfShelfId, 'shelfId', shelfId);
                            }
                            item.id = startOfShelfId + 'I' + index;
                            index--;
                            let itemWidth = viewCombinedShelves[i].items[n].placement.pWidth;
                            distFromStart -= (itemWidth != undefined ? itemWidth : ProductDefaultDimensions.width) * item.placement.faces;
                            distInShelf -= (itemWidth != undefined ? itemWidth : ProductDefaultDimensions.width) * item.placement.faces;
                            item.placement.distFromStart = distFromStart;
                            if (viewCombinedShelves[shelfId]) {
                                spaceInShelf = distInShelf; // viewCombinedShelves[shelfId].dimensions.width - distInShelf;
                                combinedShelves[shelfId].items.push(item);
                            }
                        }
                    }
                }
            }
        }
        let aisle: PlanogramAisle = JSON.parse(JSON.stringify(blankPlanogramAisle));
        let aisles = this.props.store ? this.props.store.aisles : undefined;
        let aisleId = section.id.substring(0, section.id.indexOf('SE'));
        if (aisles) {
            for (let i = 0; i < aisles.length; i++) {
                if (aisles[i].id === aisleId) {
                    aisle = aisles[i];
                    break;
                }
            }
        }
        if (aisle.id === aisleId) {
            await this.props.setStoreAisle(aisle);
            await this.checkLevel2(aisle);
        }
    }

    /*  verify items with shelfLevel 2 position */
    checkLevel2 = async (aisle: PlanogramAisle) => {
        let combinedItems: any[] = [];
        for (let a = 0; a < aisle.sections.length; a++) {
            for (let b = 0; b < aisle.sections[a].shelves.length; b++) {
                combinedItems = combinedItems.concat(aisle.sections[a].shelves[b].items);
            }
        }
        for (let c = 0; c < combinedItems.length; c++) {
            if (combinedItems[c].linkedItemUp) {
                let upItem = combinedItems.filter((line: any) => line.id === combinedItems[c].linkedItemUp);
                if (upItem && upItem[0]) {
                    let itemToMove = JSON.parse(JSON.stringify(upItem[0]));
                    // find location of item to move
                    let moveSec = parseInt(itemToMove.id.substring(itemToMove.id.indexOf('SE') + 2, itemToMove.id.indexOf('SH')));
                    let moveSh = parseInt(itemToMove.id.substring(itemToMove.id.indexOf('SH') + 2, itemToMove.id.indexOf('I')));
                    let index = aisle.sections[moveSec].shelves[moveSh].items.findIndex(line => line.id === itemToMove.id);
                    // remove this item from original shelf
                    aisle.sections[moveSec].shelves[moveSh].items.splice(index, 1);
                    // update itemToMove properties
                    itemToMove.placement.distFromStart = combinedItems[c].placement.distFromStart;
                    itemToMove.linkedItemDown = combinedItems[c].id;
                    // itemToMove.id = combinedItems[c].id.substring(0, combinedItems[c].id.indexOf('I')) + 'I' + itemToMove.id.substring(upItem[0].id.indexOf('I') + 1);
                    itemToMove.id = ''
                    // find new location of item and create new item id
                    let destinationSec = parseInt(combinedItems[c].id.substring(combinedItems[c].id.indexOf('SE') + 2, combinedItems[c].id.indexOf('SH')));
                    let destinationSh = parseInt(combinedItems[c].id.substring(combinedItems[c].id.indexOf('SH') + 2, combinedItems[c].id.indexOf('I')));
                    if (aisle.sections[destinationSec].shelves[destinationSh].items.length > 1) {
                        let lastI = parseInt(aisle.sections[destinationSec].shelves[destinationSh].items[aisle.sections[destinationSec].shelves[destinationSh].items.length - 1].id.substring(aisle.sections[destinationSec].shelves[destinationSh].items[aisle.sections[destinationSec].shelves[destinationSh].items.length - 1].id.indexOf('I') + 1));
                        let newI = 0;
                        if (lastI < 100) newI = lastI + 100;
                        else newI = lastI + 1;
                        itemToMove.id = aisle.sections[destinationSec].shelves[destinationSh].id + "I" + newI;
                    } else {
                        itemToMove.id = aisle.sections[destinationSec].shelves[destinationSh].id + "I" + 100;
                    }
                    // add itemToMove to its new location
                    // console.log('checkLevel2 push',itemToMove);
                    aisle.sections[destinationSec].shelves[destinationSh].items.push(itemToMove);
                    // update the linkedItemUp with the new item id 
                    combinedItems[c].linkedItemUp = itemToMove.id;
                }
                else {
                    combinedItems[c].linkedItemUp = null;
                }
            }
        }
        await this.props.setStoreAisle(aisle);
    }

    /*  Add saving array for aisle, allowing to do 
       Ctrl+Z to move back and Ctrl+Y to move forward   */
    saveSnapShotAisle = async (thisAisle: PlanogramAisle) => {
        // check if the array is empty or for a different aisle
        let tArray = this.props.aisleSaveArray;
        let tIndex = this.props.aisleSaveIndex;
        if (tArray.length === 0 || (tArray.length > 0 && tArray[0].id != thisAisle.id)) {
            let newAisle = await planogramProvider.getStoreAisle(this.props.store ? this.props.store.store_id : 0, thisAisle.aisle_id);
            tArray = [JSON.parse(JSON.stringify(newAisle))];
            tIndex = 0;
            await this.props.setAisleSaveArray(tArray);
            await this.props.setAisleSaveIndex(tIndex);
        }
        let aisle: PlanogramAisle = JSON.parse(JSON.stringify(blankPlanogramAisle));
        let aisles = this.props.store ? this.props.store.aisles : undefined;
        let aisleId = thisAisle.id;
        if (aisles) {
            for (let i = 0; i < aisles.length; i++) {
                if (aisles[i].id === aisleId) {
                    aisle = aisles[i];
                    break;
                }
            }
        }
        if (aisle.id === aisleId) {
            tIndex++;
            tArray.splice(tIndex, 0, JSON.parse(JSON.stringify(aisle)));
            if (tArray.length > tIndex + 1) {
                tArray.length = tIndex + 1;
            }
            await this.props.setAisleSaveArray(tArray);
            await this.props.setAisleSaveIndex(tIndex);
            await this.props.setStoreAisle(aisle);
        }
    }

    getEmptyAisle = async (aisleId: number) => {
        if (!this.state.isLoading) {
            this.setState({ isLoading: true })
            let newAisle = await planogramApi.fetchPlanogramAisle(this.props.store ? this.props.store.store_id : '', aisleId, this.props.user ? this.props.user.db : '');
            if (!newAisle || newAisle === undefined) {
                return;
            }

            if (this.props.store) {
                let store = this.props.store;
                for (let i = 0; i < store.aisles.length; i++) {
                    if (store.aisles[i] != undefined && newAisle != undefined && store.aisles[i].aisle_id === newAisle.aisle_id) {
                        store.aisles[i] = newAisle;
                        await this.props.setStore(store);
                        break;
                    }
                }
            }
        }
        this.setState({ isLoading: false })
    }

    toggleModal = () =>{
        this.setState({duplicateModal: !this.state.duplicateModal})
    }

    render() {
        const { store, } = this.props;

        if (!store) return null;

        const aisle = store.aisles.filter((line:any) => line.aisle_id === this.props.aisle.aisle_id)[0];
        if (!aisle || aisle.aisle_id === 0) return null

        const { sections } = aisle;
        const { canDrop, isDragging, connectDropTarget, connectDragSource } = this.props;
        const {
            addSection,
            addShelf,
            switchSections,
            removeSectionItemsAction,
            deleteSection,
            editSectionDimensions
        } = this.props;

        

        const returnModal = (modal: any) => {
            return (
            <Modal zIndex={999999999} isOpen={modal.isOpen} toggle={modal.toggle} className='modal-dialog-centered modal-md text-center width-10-per min-width-25-rem height-18-rem'>
                <ModalHeader onClose={modal.toggle} classNames={modal.headerClasses}>
                    <h5 className="modal-title">{modal.header}</h5>
                </ModalHeader>
                <ModalBody>
                    {modal.body}
                </ModalBody>
                <ModalFooter>
                    <div className="m-auto">
                        {modal.buttons.map((ele: any, index: number) => <Button outline={ele.outline ? true : false} className='round ml-05 mr-05' key={index} color={ele.color} onClick={ele.onClick} disabled={ele.disabled}>{ele.label}</Button>)}
                    </div>
                </ModalFooter>
            </Modal>
            )
        }
       

        const duplicateModalDiv = [{
            headerClasses: 'mr-auto',
            isOpen: this.state.duplicateModal, toggle: () => this.setState({duplicateModal:false}), header: (
            <div className='d-flex-column-w'>
                <span className="font-weight-bold mb-1 align-left"><FormattedMessage id="duplicate-object"/></span>
                <div className='d-flex-column-w'>
                    <span className='align-left'><FormattedMessage id="youChoseDuplicateShelf"/></span>
                    <span className='align-left'><FormattedMessage id="howMuchTimesDuplicate"/></span>
                </div>
                </div>
                ),
            body: (
            <div className='d-flex-reverse-row justify-content-center'>
                <div className={classnames('duplicateButton', { 'border-turquoise-2': this.state.activeButton == 0 })} onClick={()=>{this.setState({duplicateAmount:1,activeButton:0})}}>1</div>
                <div className={classnames('duplicateButton', { 'border-turquoise-2': this.state.activeButton == 1 })} onClick={()=>{this.setState({duplicateAmount:2,activeButton:1})}}>2</div>
                <div  className={classnames('duplicateButton', { 'border-turquoise-2': this.state.activeButton == 2 })} onClick={()=>{this.setState({duplicateAmount:5,activeButton:2})}}>5</div>
                <FormattedMessage id='other'>
                    {placeholder=>
                                    <input min="1" max="10" type='number' onClick={()=>{this.setState({activeButton:3})}} onChange={(e:any)=>{console.log(e.target.value);this.setState({duplicateAmount:Number(e.target.value)})}} 
                                    placeholder={`${placeholder?placeholder:''}`} className={classnames('duplicateButton', { 'border-turquoise-2': this.state.activeButton == 3 })}/>
                    }
                </FormattedMessage>

            </div>
                 ),
            buttons: [
                {
                    color: 'primary', onClick: () => {

                        this.state.duplicateData.onClick()
                        this.setState({duplicateAmount:0,activeButton:null})
                        this.setState({duplicateData:{onClick:()=>{}}})
                        setTimeout(this.toggleModal, 150)

                    }, label: <FormattedMessage id="duplicate"/>,
                },
                { color: 'primary', outline: true, onClick: () => this.setState({duplicateModal:false}), label: <FormattedMessage id="cancel"/> }
            ]
        }]
        

        return connectDropTarget(connectDragSource(
            <div
                className={"planogram-aisle" + (canDrop ? " droppable" : "") + (isDragging ? " dragged" : "")}
                ref={this.props.AisleRef as React.RefObject<HTMLDivElement>}
                onClick={e => {
                    e.stopPropagation();
                    if (!e.ctrlKey) {
                        e.stopPropagation();
                        this.props.setCurrentSelectedId('');
                        this.props.setMultiSelectId([]);
                        this.props.hideProductDisplayerBarcode();
                    }
                }}>
                {sections.map((section: PlanogramSection, i: number) => {
                    let sectionBorders = 'none';
                    if (i === 0) {
                        if (section.section_layout > 0 && ((sections[i + 1] && sections[i + 1].section_layout === 0) || (!sections[i + 1]))) sectionBorders = 'both';
                        if (section.section_layout > 0 && (sections[i + 1] && sections[i + 1].section_layout > 0) && section.section_layout != sections[i + 1].section_layout) sectionBorders = 'both';
                        if (section.section_layout > 0 && (sections[i + 1] && sections[i + 1].section_layout > 0) && section.section_layout === sections[i + 1].section_layout) sectionBorders = 'left';
                    }
                    if (i > 0 && i < sections.length - 1) {
                        if (section.section_layout > 0 && (sections[i - 1] && sections[i - 1].section_layout === 0) && (sections[i + 1] && sections[i + 1].section_layout === 0)) sectionBorders = 'both';
                        if (section.section_layout > 0 && (sections[i - 1] && sections[i - 1].section_layout > 0) && section.section_layout === sections[i - 1].section_layout && (sections[i + 1] && sections[i + 1].section_layout === 0)) sectionBorders = 'right';
                        if (section.section_layout > 0 && (sections[i - 1] && sections[i - 1].section_layout === 0) && (sections[i + 1] && sections[i + 1].section_layout > 0) && section.section_layout === sections[i + 1].section_layout) sectionBorders = 'left';
                        if (section.section_layout > 0 && (sections[i - 1] && sections[i - 1].section_layout > 0) && (sections[i + 1] && sections[i + 1].section_layout > 0)
                            && section.section_layout != sections[i - 1].section_layout && section.section_layout != sections[i + 1].section_layout) sectionBorders = 'both';
                        if (section.section_layout > 0 && (sections[i - 1] && sections[i - 1].section_layout === 0) && (sections[i + 1] && sections[i + 1].section_layout > 0)
                            && section.section_layout != sections[i + 1].section_layout) sectionBorders = 'both';
                        if (section.section_layout > 0 && (sections[i + 1] && sections[i + 1].section_layout === 0) && (sections[i - 1] && sections[i - 1].section_layout > 0)
                            && section.section_layout != sections[i - 1].section_layout) sectionBorders = 'both';
                    }
                    if (i > 0 && i === sections.length - 1) {
                        if (section.section_layout > 0 && (sections[i - 1] && sections[i - 1].section_layout > 0) && section.section_layout === sections[i - 1].section_layout) sectionBorders = 'right';
                        if (section.section_layout > 0 && (sections[i - 1] && sections[i - 1].section_layout === 0)) sectionBorders = 'both';
                        if (section.section_layout > 0 && (sections[i - 1] && sections[i - 1].section_layout > 0) && section.section_layout != sections[i - 1].section_layout) sectionBorders = 'both';
                    }
                    let minDimensions;
                    if (section.shelves.length > 0) {
                        let shelvesMinHeight = calculateShelvesHeight(section);
                        // let shelvesMinHeight = section.shelves.map(sh => sh.dimensions.height).reduce((p, c) => p + c);
                        let shelvesMinWidth = section.shelves.map(sh => sh.dimensions.width).reduce((p, c) => Math.max(p, c));
                        let shelvesMinDepth = section.shelves.map(sh => sh.dimensions.depth).reduce((p, c) => Math.max(p, c));
                        minDimensions = {
                            height: shelvesMinHeight,
                            width: shelvesMinWidth,
                            depth: shelvesMinDepth
                        };
                    }

                    return <React.Fragment key={i}>
                        <Menu id={"sec_ed_" + section.id} key={"sec_ed_" + section.id + "_" + i} className="planogram-context-window">
                            <DimensionModal
                                init={section.dimensions}
                                title={<FormattedMessage id="section-dimensions" />}
                                maxDimensions={SectionMaxDimension}
                                minDimensions={minDimensions}
                                onSubmit={async (dimensions) => {
                                  globalLastChangeSaved = false;
                                    await editSectionDimensions(section, dimensions);
                                    await this.saveSnapShotAisle(aisle);
                                    contextMenu.hideAll();
                                }}>
                                <div className="input-row">
                                    <span style={{
                                        flex: "1", textAlign: "inherit", border: 'none', borderRadius: '0', background: '#ECF0F1',
                                        color: '#2b998a', padding: '0.5em 1em', fontSize: '1em'
                                    }}>
                                        <div><FormattedMessage id="section-name" />: </div>
                                        <SelectBox
                                            rtlEnabled
                                            dataSource={{
                                                store: this.props.layouts,
                                                paginate: true,
                                                pageSize: 30,
                                            }}
                                            defaultValue={section.section_layout > 0 ? section.section_layout : null}
                                            searchEnabled={true}
                                            onValueChanged={(e) => {
                                                let value = parseInt(e.value);
                                                if (isNaN(value)) value = 0;
                                                section.section_layout = value;
                                                this.saveSnapShotAisle(aisle);
                                            }}
                                            valueExpr="Id"
                                            displayExpr="Name"
                                            placeholder="Select section name..."
                                        />
                                    </span>
                                </div>
                                <div className="input-row">
                                    <button onClick={e => {
                                        if (window.confirm("Are you sure you want to remove all items in this section?")) {
                                            globalLastChangeSaved = false;
                                            removeSectionItemsAction(aisle.id, section.id);
                                        }
                                    }}>
                                        <FontAwesomeIcon icon={faEraser} />
                                        <span><FormattedMessage id="clear-items" /></span>
                                    </button>
                                </div>
                                <div className="input-row">
                                    <button onClick={async (e) => {
                                        await this.alignThisSectionToLeft(section);
                                    }}>
                                        <FontAwesomeIcon icon={faAlignLeft} />
                                        <span><FormattedMessage id="align-to-left" /></span>
                                    </button>
                                </div>
                                <div className="input-row">
                                    <button onClick={async (e) => {
                                        await this.alignThisSectionToRight(section);
                                    }}>
                                        <FontAwesomeIcon icon={faAlignRight} />
                                        <span><FormattedMessage id="align-to-right" /></span>
                                    </button>
                                </div>
                                <div className="input-row">
                                    <button onClick={async (e) => {
                                        await this.alignThisSectionToJustify(section);
                                    }}>
                                        <FontAwesomeIcon icon={faAlignJustify} />
                                        <span><FormattedMessage id="align-justify" /></span>
                                    </button>
                                </div>
                                <div></div>
                            </DimensionModal>
                        </Menu>
                        <MenuProvider id={"sec_ed_" + section.id}
                            data={section}
                            key={section.id}
                            className={`planogram-section-wrapper`}
                            style={localStorage.printStatus === 'true' && (i + 1 < Number(localStorage.printFromInput) || i + 1 > Number(localStorage.printToInput)) ? //this.props.printStatus
                                {  display: 'none'  } : {}} >
                            <PlanogramSectionComponent
                                storeId={store.store_id}
                                aisleId={aisle.aisle_id}
                                section={section}
                                sections={sections}
                                index={i}
                                sectionBorders={sectionBorders}
                                verifyDrop={(droppedItem) => {
                                    const shelvesHeight = section.shelves.length > 0 ? calculateShelvesHeight(section) : 0;

                                    const itemType = droppedItem.type;
                                    const item = droppedItem.payload;
                                    if (itemType === PlanogramDragDropTypes.SHELF) {
                                        let shelfDimensions: DimensionObject = item.dimensions;
                                        return section.dimensions.height >= shelvesHeight + shelfDimensions.height;
                                    }
                                    return true;
                                }}
                                onDragEnd={async (source, dropResult) => {
                                    if (dropResult.type === PlanogramDragDropTypes.TRASH) {
                                        globalLastChangeSaved = false;
                                        await deleteSection(aisle.id, section);
                                        // else if (source.type === PlanogramDragDropTypes.SHELF && source.type === dropResult.type)
                                        //     switchShelf(p, dropResult.payload);
                                        // make sure the section is removed from the aisle before SnapShot is saved
                                        let sectionIndex = aisle.sections.findIndex((s:any) => s === section || s.id === section.id);
                                        if (sectionIndex >= 0) {
                                            aisle.sections.splice(sectionIndex, 1);
                                        }
                                        await this.saveSnapShotAisle(aisle);
                                    }
                                }}
                                onDrop={async (item: any) => {
                                    if (item.type === PlanogramDragDropTypes.SHELF_SIDEBAR) {
                                        // createNewShelf(section);
                                        // if (section.shelves.length >= 1) {
                                        // const shelvesHeight = section.shelves.map(sh => sh.dimensions.height).reduce((p, c) => p + c);
                                        const shelvesHeight = section.shelves.length > 0 ? calculateShelvesHeight(section) : 0;
                                        let netHeight = section.dimensions.height - shelvesHeight;
                                        /*  Add default sizes for section and shelf  */
                                        /*  when section is already set - take the width and depth from it - cpmpared to height */

                                        let storeShelfDimention = {
                                            width: section && section.dimensions ? section.dimensions.width : ShelfDefaultDimension.width,
                                            height: this.props.store && this.props.store.Shelf_height ? this.props.store.Shelf_height : ShelfDefaultDimension.height,
                                            depth: section && section.dimensions ? section.dimensions.depth : ShelfDefaultDimension.depth,
                                        }

                                        // adding new shelf will check if there's enough room - if you have 0 space - you will get an alert and wont get a new shelf.
                                        if (netHeight === 0) {
                                            alert("אין מקום למדפים נוספים");
                                        }
                                        else if (netHeight >= storeShelfDimention.height) {   //  shelf with default height
                                            globalLastChangeSaved = false;
                                            addShelf(section, {
                                                dimensions: storeShelfDimention
                                            });
                                            this.saveSnapShotAisle(aisle);
                                        }
                                        else {   //  shelf gets the height that is left from the total section height 
                                            storeShelfDimention.height = netHeight;
                                            globalLastChangeSaved = false;
                                            addShelf(section, {
                                                // dimensions
                                                dimensions: storeShelfDimention
                                            });
                                            this.saveSnapShotAisle(aisle);
                                        }
                                    }
                                    if (item.type === PlanogramDragDropTypes.SHELF_SIDEBAR_FREEZER) {
                                        const shelvesHeight = section.shelves.length > 0 ? calculateShelvesHeight(section) : 0;
                                        let netHeight = section.dimensions.height - shelvesHeight;
                                        /*  Add default sizes for section and shelf  */
                                        /*  when section is already set - take the width and depth from it - cpmpared to height */
                                        let storeShelfDimention = {
                                            width: section && section.dimensions ? section.dimensions.width : ShelfDefaultDimension.width,
                                            height: this.props.store && this.props.store.Shelf_height ? this.props.store.Shelf_height : ShelfDefaultDimension.height,
                                            depth: section && section.dimensions ? section.dimensions.depth : ShelfDefaultDimension.depth,
                                        }
                                        //  adding new shelf will check if there's enough room - if you have 0 space - you will get an alert and wont get a new shelf.
                                        if (netHeight === 0) {
                                            alert("אין מקום למדפים נוספים");
                                        }
                                        else if (netHeight >= storeShelfDimention.height) {   //  shelf with default height
                                            globalLastChangeSaved = false;
                                            addShelf(section, {
                                                dimensions: storeShelfDimention,
                                                freezer: 1
                                            });
                                            this.saveSnapShotAisle(aisle);
                                        }
                                        else {   //  shelf gets the height that is left from the total section height 
                                            storeShelfDimention.height = netHeight;
                                            globalLastChangeSaved = false;
                                            addShelf(section, {
                                                dimensions: storeShelfDimention,
                                                freezer: 1
                                            });
                                            this.saveSnapShotAisle(aisle);
                                        }
                                    }
                                    else if (item.type === PlanogramDragDropTypes.SHELF) {
                                        /* if item.shelfLevel is not 1 - DO NOT ALLOW IT TO BE MOVED */
                                        // if (item.payload.shelfLevel != 1 && item.payload.shelfLevel != undefined) {
                                        //     let errorMsg = 'Cannot move a second story item';
                                        //     alert(errorMsg);
                                        //     return;
                                        // }
                                        // const shelvesHeight = section.shelves.length > 0 ? calculateShelvesHeight(section) : 0;
                                        // let availableDimensions = {
                                        //     ...section.dimensions,
                                        //     height: 400//section.dimensions.height - shelvesHeight,
                                        // };
                                        // const shelfItem: PlanogramItem = item.payload;
                                        // globalLastChangeSaved = false;
                                        // addShelf(section, {
                                        //     dimensions: availableDimensions,
                                        //     // item: shelfItem
                                        // })
                                        // this.saveSnapShotAisle(aisle);
                                    }
                                    // else if (item.type === PlanogramDragDropTypes.SHELF_ITEM) {
                                    //     console.log("SHELF_ITEM");
                                    //     /* Barak 21.12.20 - if item.shelfLevel is not 1 - DO NOT ALLOW IT TO BE MOVED */
                                    //     if (item.payload.shelfLevel != 1 && item.payload.shelfLevel != undefined) {
                                    //         let errorMsg = 'Cannot move a second story item';
                                    //         alert(errorMsg);
                                    //         return;
                                    //     }

                                    //     const shelvesHeight = section.shelves.length > 0 ? calculateShelvesHeight(section) : 0;
                                    //     let availableDimensions = {
                                    //         ...section.dimensions,
                                    //         height: section.dimensions.height - shelvesHeight,
                                    //     };
                                    //     const shelfItem: PlanogramItem = item.payload;
                                    //     globalLastChangeSaved = false;
                                    //     addShelf(section, {
                                    //         dimensions: availableDimensions,
                                    //         item: shelfItem
                                    //     })
                                    //     this.saveSnapShotAisle(aisle);
                                    // }


                                    if (item.type === PlanogramDragDropTypes.SERIES_SIDEBAR) {
                                        let classname = "flash-bg-opacity"
                                        let shelf_btn: any = document.getElementById('shelf_btn')
                                        FlashBtn(classname, shelf_btn)
                                        //     let storeShelfDimention = {
                                        //         width: section && section.dimensions ? section.dimensions.width : ShelfDefaultDimension.width,
                                        //         height: this.props.store && this.props.store.Shelf_height ? this.props.store.Shelf_height : ShelfDefaultDimension.height,
                                        //         depth: section && section.dimensions ? section.dimensions.depth : ShelfDefaultDimension.depth,
                                        //     }

                                        //     let series = item.payload
                                        //      let newItems =  series.map((product:any)=> {return {...blankItem, product: product.BarCode ,placement: {...blankItem.placement, faces: product.faces}} })
                                        //     let shelf = await addShelf(section, {
                                        //         dimensions: storeShelfDimention
                                        //     });
                                        //     console.log('גשדגשדגשד', shelf)
                                        //      //  await this.addShelfItem(this.props.aisleShelves[shelf.id], blankItem, 0, newItems);
                                    }
                                    else if (item.type === PlanogramDragDropTypes.PRODUCT_SIDEBAR) {
                                        let classname = "flash-bg-opacity"
                                        let shelf_btn: any = document.getElementById('shelf_btn')
                                        FlashBtn(classname, shelf_btn)
                                        //     console.log(1)
                                        //     console.log("PRODUCT_SIDEBAR");
                                        //     const shelvesHeight = section.shelves.length > 0 ? calculateShelvesHeight(section) : 0;
                                        //     let avaialableHeight = section.dimensions.height - shelvesHeight;
                                        //     if (avaialableHeight > ShelfDefaultDimension.height)
                                        //         avaialableHeight = ShelfDefaultDimension.height;
                                        //     const newShelfDimensions = {
                                        //         ...section.dimensions,
                                        //         height: avaialableHeight,
                                        //     };
                                        //     let barcode: number = item.payload;
                                        //     const product: CatalogProduct = productsMap[barcode];
                                        //     /* Barak 10.9.20 * const productDimensions: DimensionObject = catalogProductDimensionObject(product); */
                                        //     /* Barak 10.9.20 */
                                        //     const productDimensions: DimensionObject = {
                                        //         width: item.payload.placement && item.payload.placement.pWidth ? item.payload.placement.pWidth : ProductDefaultDimensions.width,
                                        //         height: item.payload.placement && item.payload.placement.pHeight ? item.payload.placement.pHeight : ProductDefaultDimensions.height,
                                        //         depth: item.payload.placement && item.payload.placement.pDepth ? item.payload.placement.pDepth : ProductDefaultDimensions.depth
                                        //     }
                                        //     /****************/
                                        //     if (!validateDimensions(productDimensions)) {
                                        //         setModal(() => <DimensionModal
                                        //             init={ProductDefaultDimensions}
                                        //             title={"Section Dimensions"}
                                        //             onSubmit={async (dimensions) => {
                                        //             /* Barak 23.1.20 */ globalLastChangeSaved = false;
                                        //                 await planogramApi.updateProductDimensions(product.BarCode, dimensions).then(async (res) => {
                                        //                     await updateProductDimensions(
                                        //                         product.BarCode,
                                        //                         dimensions,
                                        //                         await addShelfAction(section, {
                                        //                             dimensions: newShelfDimensions,
                                        //                             product: product.BarCode,
                                        //                             item: {
                                        //                                 id: "",
                                        //                                 product: product.BarCode,
                                        //                                 placement: {
                                        //                                     ...ItemDefualtPlacement,
                                        //                                     faces:item.faces,
                                        //                                     row: Math.floor(newShelfDimensions.depth / dimensions.depth),
                                        //                                 },
                                        //                                 /* Barak 24.6.2020 - Add lowSales flag to the item */
                                        //                                 lowSales: false,
                                        //                                 /* Barak 23.7.2020 - Add missing to the item */
                                        //                                 missing: 0,
                                        //                                 /* Barak 28.6.2020 - Add branch_id */
                                        //                                 branch_id: store.branch_id,
                                        //                                 /* Barak 21.12.20 - Add linking item option */
                                        //                                 linkedItemUp: null,
                                        //                                 linkedItemDown: null,
                                        //                                 shelfLevel: 1,
                                        //                                 /********************************************/
                                        //                             }
                                        //                         }));
                                        //                     /* Barak 22.11.20 */ await this.saveSnapShotAisle(aisle);
                                        //                     toggleModal();
                                        //                 }).catch((err) => {
                                        //                     console.error(err);
                                        //                 })
                                        //             }} />)
                                        //     }
                                        //     else {
                                        //     /* Barak 23.1.20 */ globalLastChangeSaved = false;
                                        //         addShelf(section, {
                                        //             dimensions: newShelfDimensions,
                                        //             product: product.BarCode,
                                        //             item: {
                                        //                 id: "",
                                        //                 product: product.BarCode,
                                        //                 placement: {
                                        //                     ...ItemDefualtPlacement,
                                        //                     faces:item.faces,
                                        //                 },
                                        //                 /* Barak 24.6.2020 - Add lowSales flag to the item */
                                        //                 lowSales: false,
                                        //                 /* Barak 23.7.2020 - Add missing to the item */
                                        //                 missing: 0,
                                        //                 /* Barak 28.6.2020 - Add branch_id */
                                        //                 branch_id: store.branch_id,
                                        //                 /* Barak 21.12.20 - Add linking item option */
                                        //                 linkedItemUp: null,
                                        //                 linkedItemDown: null,
                                        //                 shelfLevel: 1,
                                        //                 /********************************************/
                                        //             }
                                        //         });
                                        //         /* Barak 22.11.20 */ this.saveSnapShotAisle(aisle);
                                        //     }

                                    }
                                    else if (item.type === PlanogramDragDropTypes.SHELF) {
                                        let classname = "flash-bg-opacity"
                                        let shelf_btn: any = document.getElementById('shelf_btn')
                                        FlashBtn(classname, shelf_btn)
                                            const shelf: PlanogramShelf = item.payload;
                                        /* Barak 23.1.20 */ 
                                            /* Barak 6.8.20 * duplicateShelf(section, shelf); */
                                            /* Barak 6.8.20 - Add option to duplicate shelf several times */

                                            this.setState({duplicateData:{onClick: async ()=>{
                                                globalLastChangeSaved = false
                                                let duplicateAm = Number(this.state.duplicateAmount)
                                                for (let i = 0; i < duplicateAm; i++) {
                                                    this.props.duplicateShelf(section, shelf);
                                                }
                                                setTimeout(async() => {
                                                await this.saveSnapShotAisle(aisle);
                                                }, 500);
                                            }}})
                                            this.setState({ duplicateModal:true})

                                        // globalLastChangeSaved = false;
                                            
                                            // let numberDuplicateString = prompt('כמה פעמים לשכפל?');
                                            
                                            // if the numberDuplicateString is null that means the user pressed 'cancel' so we do nothing
                                            // if (numberDuplicateString) {
                                            //     let numberDuplicate = 1;
                                            //     if (numberDuplicateString && !isNaN(parseInt(numberDuplicateString))) numberDuplicate = parseInt(numberDuplicateString);
                                            //     if (isNaN(parseInt(numberDuplicateString))) numberDuplicate = 0;
                                            //     if (numberDuplicate < 0) numberDuplicate = 0;
                                            //     for (let i = 0; i < numberDuplicate; i++) {
                                            //         this.props.duplicateShelf(section, shelf);
                                            //     }
                                            //     console.log('section shelf if',section)
                                            //     console.log('shelf shelf if',shelf)
                                            //     console.log('aisle shelf if',aisle)
                                            //     /* Barak 22.11.20 */ this.saveSnapShotAisle(aisle);
                                            // }
                                            /****************************************************************/
                                    }
                                    else if (item.type === PlanogramDragDropTypes.SECTION) {
                                        const remote: PlanogramSection = item.payload;
                                        globalLastChangeSaved = false;
                                        let arrIndexID = await this.props.aisle.sections.map(function (o: any) { return o.id; });
                                        let myIndex: number = -1;

                                        //  getting the index of the current section

                                        for (let i = 0; i < arrIndexID.length; i++) {
                                            if (section.id === arrIndexID[i]) { myIndex = i; break }
                                        }

                                        //  getting the index of the dragged section

                                        let myIndexRemote: number = -1;
                                        for (let i = 0; i < arrIndexID.length; i++) {
                                            if (remote.id === arrIndexID[i]) { myIndexRemote = i; break }
                                        }


                                        //  switching between the section so it will "push" the new section between the exsisting sections, 
                                        // checking if we dragged from the right or left by comparing which index is bigger and doing the switch accordignly 
                                        if (myIndex != -1 && myIndexRemote != -1) {

                                            if (myIndex < myIndexRemote) {
                                                for (let j = myIndexRemote; j > myIndex; j--) {
                                                    switchSections(this.props.aisle.sections[j - 1], this.props.aisle.sections[j]);
                                                }
                                            }
                                            else {
                                                for (let j = myIndexRemote; j < myIndex - 1; j++) {
                                                    switchSections(this.props.aisle.sections[j + 1], this.props.aisle.sections[j]);
                                                }

                                            }
                                        }

                                        // switchSections(section, remote);

                                    }
                                    /*  addind a new section to anywhere in aisle when dropping from sidebar*/
                                    else if (item.type === PlanogramDragDropTypes.SECTION_SIDEBAR) {
                                        /*  Add default sizes for section and shelf  */
                                        await addSection(aisle.id, {
                                            dimensions: {
                                                width: this.props.store && this.props.store.Section_width ? this.props.store.Section_width : SectionDefaultDimension.width,
                                                height: this.props.store && this.props.store.Section_height ? this.props.store.Section_height : SectionDefaultDimension.height,
                                                depth: this.props.store && this.props.store.Section_depth ? this.props.store.Section_depth : SectionDefaultDimension.depth,
                                            },

                                        });
                                        this.saveSnapShotAisle(aisle);
                                        

                                        //  getting the index of the current section
                                        let arrIndexID = await this.props.aisle.sections.map(function (o: any) { return o.id; });
                                        let myIndex: number = -1;
                                        for (let i = 0; i < arrIndexID.length; i++) {
                                            if (section.id === arrIndexID[i]) { myIndex = i; break }
                                        }

                                        //  switching between the section so it will "push" the new section between the exsisting sections
                                        if (myIndex != -1) {
                                            for (let j = this.props.aisle.sections.length; j > myIndex + 1; j--) {
                                                switchSections(this.props.aisle.sections[j - 2], this.props.aisle.sections[j - 1]);
                                            }
                                        }

                                    }
                                }}
                            />
                        </MenuProvider>
                    </React.Fragment>
                })}
               {this.state.duplicateModal ? returnModal(duplicateModalDiv[0]) : null}
            </div >
        ))
    }
}

const PlanogramAisleDroppable = DraggableSource(DroppableTarget(connect(mapStateToProps, mapDispatchToProps)(PlanogramAisleComponent)));

const mainMapStateToProps = (state: AppState, ownProps: RouteComponentProps<{ store_id?: string, aisle_index: string, second_aisle_index: string}> & { scale?: number }) => {
    return {
        ...ownProps,
        productsMap: state.newPlano ? state.newPlano.productsMap : null,
        Zoom: state.Zoom,
        position: state.position,
        hideAisle: state.displayOptions. hideAisle,
        store: state.planogram.store,
        aisle: state.planogram.virtualStore.aisleMap[ownProps.match.params.aisle_index] ? state.planogram.virtualStore.aisleMap[ownProps.match.params.aisle_index] : null,
        second_aisle: state.planogram.virtualStore.aisleMap[ownProps.match.params.second_aisle_index] ? state.planogram.virtualStore.aisleMap[ownProps.match.params.second_aisle_index] : null,
        shelvesDetails: state.planogram.virtualStore.shelfDetails,
        aisleShelves: state.planogram.virtualStore.shelfMap,
        aisleSaveArray: state.planogram.display.aisleSaveArray,
        aisleSaveIndex: state.planogram.display.aisleSaveIndex,
        user: state.auth.user,
    };
}
const mainMapDispatchToProps = (dispatch: Dispatch) => ({
    hideAisleToggle: (bool:boolean) => {
        dispatch({
            type: NEWPLANO_ACTION.SET_NEWPLANO_DISPLAY_OPTIONS,
            payload: {hideAisle: bool}
        });
    },
    setAlert: () => {
        dispatch({
            type: NEWPLANO_ACTION.SET_ALERT,
            payload: {
                timeMs: 1000,
                type: 'success',
                text: 'aisle_saved'
            }
        });
    },
    resetPosition: () => dispatch({
        type: NEWPLANO_ACTION.SET_POSITION,
        payload: {x:(window.innerWidth), y:window.innerHeight * 0.45}
    }),
    setZoom: (zoom: number) => { dispatch(setZoom(zoom)); },
    addSection: (aisle: PlanogramElementId, data: addSectionActionProps) => dispatch(addSectionAction(aisle, data)),
    duplicateSection: (aisleId: PlanogramElementId, section: PlanogramSection) => dispatch(duplicateSectionAction(aisleId, section)),
    updateProductDimensions: (barcode: number, dimensions: DimensionObject, afterAction: Action) => {
        dispatch(editProductDimensions(barcode, dimensions));
        dispatch(afterAction);
    },
    setStoreAisle: (aisle: PlanogramAisle, aislePid?: PlanogramElementId) => {
        dispatch(setAisleAction(aisle, aislePid));
    },
    deleteAisle: (aisleId: PlanogramElementId) => dispatch(removeAisleAction(aisleId)),
    editAisleName: (aisleId: PlanogramElementId, name: string) => dispatch(editAisleName(aisleId, name)),
    setAisleSaveArray: (aisleSaveArray: PlanogramAisle[]) => {
        dispatch(setAisleSaveArray(aisleSaveArray));
    },
    setAisleSaveIndex: (aisleSaveIndex: number) => {
        dispatch(setAisleSaveIndex(aisleSaveIndex));
    },
    setStore: (store: PlanogramStore) => { dispatch(setStore(store)); },
    setSummaryObjects: (summaryObjects: any) => { dispatch(SetSummaryObjects(summaryObjects)) },
});
type AisleContainerProps = ReturnType<typeof mainMapStateToProps> & ReturnType<typeof mainMapDispatchToProps>;

class PlanogramAisleContainer extends React.Component<AisleContainerProps> {

    containerRef = React.createRef<HTMLDivElement>();
    AisleRef = React.createRef<HTMLDivElement>();
    state = {
        render: false,
        zoom: 0.3,
        containerHeight: 0,
        containerWidth: 0,
        yPos: window.innerHeight * 0.3,
        xPos: window.innerWidth,
        newShow: false,
        showPrintInput: false,
        printStatus: false,
        printFromInput: 1,
        printToInput: undefined,
        globalLastChangeSavedState: globalLastChangeSaved,
        saving: false,
        isLoading: false,
        display: true,
        duplicateModal:false
        ,duplicateAmount:0,
        activeButton:null,
        duplicateData:{onClick:()=>{}}
    }

    componentDidMount() {
        const { aisle, second_aisle } = this.props;
        if (aisle && aisle.aisle_id != 0 && aisle.empty) this.getEmptyAisle(aisle.aisle_id);
        if (second_aisle && second_aisle.aisle_id != 0 && second_aisle.empty) this.getEmptyAisle(second_aisle.aisle_id);
        document.addEventListener('keydown', this.keydownHandler);
    }

    componentDidUpdate = (prevProps: any) => {
        if (this.props.aisle && this.props.aisle != undefined && prevProps.aisle && this.props.aisle.id != prevProps.aisle.id) {
            this.setState({ display: false })
            this.props.resetPosition()
            this.resetMyZoom()
            this.props.setSummaryObjects([]);
            setTimeout(() => {
                this.setState({ display: true })
            }, 500);
        }
        if (this.props.aisle && prevProps.aisle && this.props.aisle.id != prevProps.aisle.id) {
            globalLastChangeSaved = true;
        }
        if (this.state.globalLastChangeSavedState != globalLastChangeSaved)
            this.setState({ globalLastChangeSavedState: globalLastChangeSaved });
    }
    componentWillUnmount() {
        document.removeEventListener('keydown', this.keydownHandler);
    }
    keydownHandler = (e: any) => {
        if ((e.keyCode === 83 || e.key === "S" || e.key === "s" || e.key === "ד") && e.ctrlKey) {
            console.log('Ctrl+s');
            e.preventDefault();
            e.stopPropagation();
            this.saveAisleSpecial();
        }
        if ((e.keyCode === 68 || e.key === "D" || e.key === "d" || e.key === "ג") && e.ctrlKey) {
            console.log('Ctrl+d');
            e.preventDefault();
            e.stopPropagation();
            this.saveAisleSpecial();
        }
        if ((e.keyCode === 90 || e.key === "Z" || e.key === "z" || e.key === "ז") && e.ctrlKey) {
            e.preventDefault();
            e.stopPropagation();
            let {
                aisleSaveArray,
                aisleSaveIndex,
                setAisleSaveIndex,
                setStoreAisle
            } = this.props;
            let newIndex = aisleSaveIndex - 1;
            if (newIndex > 8) newIndex = 8;
            if (newIndex < 0) newIndex = 0;
            let newAisle = aisleSaveArray[newIndex];
            if (newAisle) {
                setStoreAisle(aisleSaveArray[newIndex]);
                setAisleSaveIndex(newIndex);
            }
        }
        if ((e.keyCode === 89 || e.key === "Y" || e.key === "y" || e.key === "ט") && e.ctrlKey) {
            e.preventDefault();
            e.stopPropagation();
            let {
                aisleSaveArray,
                aisleSaveIndex,
                setAisleSaveIndex,
                setStoreAisle
            } = this.props;
            console.log('Ctrl+y', aisleSaveArray, aisleSaveIndex);
            let newIndex = aisleSaveIndex + 1;
            if (newIndex > 9) newIndex = 9;
            if (newIndex < 0) newIndex = 0;
            let newAisle = aisleSaveArray[newIndex];
            if (newAisle) {
                setStoreAisle(aisleSaveArray[newIndex]);
                setAisleSaveIndex(newIndex);
            }
        }
    }
    saveAisle = async () => {
        const { store, aisle } = this.props;
        const { setStoreAisle } = this.props;
        globalLastChangeSaved = true;
        this.setState({ globalLastChangeSavedState: globalLastChangeSaved, saving: true });
        if (!store || !aisle || !aisle.aisle_id)
            return alert("Unable to save current aisle");
        planogramApi.saveStoreAisle(store.store_id, aisle).then((_aisle: PlanogramAisle) => {
            setStoreAisle(_aisle);
            this.setState({ saving: false });
            alert("Aisle was successfully saved.");
        }).catch((err) => {
            this.setState({ saving: false });
            alert("Unable to save aisle at this time...");
        });
    }
    saveAisleSpecial = async () => {
        const { store, aisle } = this.props;
        const { setStoreAisle } = this.props;
        this.setState({ globalLastChangeSavedState: true });
        if (store && aisle) {
            let _aisle: any = await SaveAisle(store, aisle)
            if (_aisle) {
                await this.props.setAlert()
                await setStoreAisle(_aisle);
            }
        }
    }
    alignThisAisleToLeft = async (targetAisle: PlanogramAisle) => {
        // this function will align all continuous shelves in this aisle
        // for that we need to go over each section, each shelf and align it as if it was continuous
        const { aisleShelves, shelvesDetails } = this.props;
        for (let i = 0; i < targetAisle.sections.length; i++) {
            let thisSection = targetAisle.sections[i];
            for (let m = 0; m < thisSection.shelves.length; m++) {
                let thisShelf = thisSection.shelves[m];

                // this is the data for the actual shelf
                let shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[thisShelf.id]));

                // no matter where the user clicked we need to go back and work on the main shelf
                while (shelfDetails.main_shelf) {
                    let ids = JSON.parse(JSON.stringify(shelfDetails.main_shelf));
                    shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[ids]));
                    thisShelf = aisleShelves[ids];
                }

                if (shelfDetails == null) {
                    return null;
                }

                let { margin_bottom, display, combined } = shelfDetails;
                if (margin_bottom == null) margin_bottom = 0;
                if (display == null) display = true;
                if (combined == null) combined = [];

                let realWidth = thisShelf.dimensions.width + (display && combined.length > 0 ? combined.map((sh: any) => {
                    const aShelf = aisleShelves[sh];
                    return aShelf.dimensions.width;
                }).reduce((a: any, b: any) => a + b) : 0);
                let combinedShelves = [thisShelf].concat(combined.map((id: any) => aisleShelves[id]));
                // let shelfItems = combinedShelves.map(sh => sh.items).reduce((p, c) => p.concat(c));
                // save initial state of shelvs with items ordered by distFromStart (distance from start of shelf)
                for (let i = 0; i < combinedShelves.length; i++) {
                    // sort shelf items by distFromStart
                    combinedShelves[i].items.sort(function (a: any, b: any) {
                        let aNumber = a.placement.distFromStart ? a.placement.distFromStart : 0;
                        let bNumber = b.placement.distFromStart ? b.placement.distFromStart : 0;
                        let aId = a.Id;
                        let bId = b.Id;
                        return (aNumber > bNumber) ? 1 : (aNumber < bNumber) ? -1 : ((aId > bId) ? 1 : (aId < bId) ? -1 : 0);
                    });
                }
                let viewCombinedShelves = JSON.parse(JSON.stringify(combinedShelves));
                // empty all items from current shelvs
                for (let i = 0; i < combinedShelves.length; i++) {
                    if (combinedShelves[i].freezer != 1 && combinedShelves[i].freezer != 2) combinedShelves[i].items = [];
                }

                let distFromStart = 0;
                let distInShelf = 0;
                let index = 0;
                let startOfShelfId = '';
                let spaceInShelf = 0;
                // go over the initial state and populate each shelf in combinedShelves with the correct items in the correct order
                let shelfId = 0;
                for (let i = 0; i < viewCombinedShelves.length; i++) {
                    if (viewCombinedShelves[i].freezer === 1) {
                        continue;
                    };
                    if (i === 0) {
                        startOfShelfId = viewCombinedShelves[i].id;
                        spaceInShelf = viewCombinedShelves[shelfId].dimensions.width;
                    }
                    for (let n = 0; n < viewCombinedShelves[i].items.length; n++) {
                        let item = JSON.parse(JSON.stringify(viewCombinedShelves[i].items[n]));
                        /* Barak 26.11.20 - return items above main line (item index >= 100) back into the shelf without altering anything else */
                        let itemInd = parseInt(item.id.substring(item.id.indexOf('I') + 1));
                        if (itemInd >= 100) {
                            let shelfInd = combinedShelves.findIndex(line => line.id === item.id.substring(0, item.id.indexOf('I')));
                            if (shelfInd >= 0) combinedShelves[shelfInd].items.push(item);
                        }
                        else {
                            if (spaceInShelf <= 0) {
                                shelfId++; // the shelf id in the viewCombinedShelves array
                                if (viewCombinedShelves[shelfId]) {
                                    // there is a next shelf so we update the shelfNumber, startOfShelfId, spaceInShelf and index
                                    // let shelfNumber = parseInt(JSON.parse(JSON.stringify(startOfShelfId.substring(startOfShelfId.indexOf('SE') + 2, startOfShelfId.indexOf('SH'))))) + 1;
                                    let old_sec_num = parseInt(JSON.parse(JSON.stringify(startOfShelfId.substring(startOfShelfId.indexOf('SE') + 2, startOfShelfId.indexOf('SH')))));
                                    let new_sec_num = parseInt(JSON.parse(JSON.stringify(viewCombinedShelves[shelfId].id.substring(viewCombinedShelves[shelfId].id.indexOf('SE') + 2, viewCombinedShelves[shelfId].id.indexOf('SH')))))
                                    startOfShelfId = JSON.parse(JSON.stringify(startOfShelfId.substring(0, startOfShelfId.indexOf('SE') + 2) + new_sec_num + startOfShelfId.substring(startOfShelfId.indexOf('SH'))));
                                    if (new_sec_num - old_sec_num === 1) {
                                        distInShelf = (spaceInShelf * -1);
                                        distFromStart = (spaceInShelf * -1);
                                        spaceInShelf = viewCombinedShelves[shelfId].dimensions.width - distInShelf;
                                    } else {
                                        distInShelf = 0;
                                        distFromStart = 0;
                                        spaceInShelf = 0;
                                    }
                                    index = 0;
                                } else
                                    // this shelf dosn't exist in the arry so we stay in this shelf
                                    shelfId--;
                            }
                            item.id = startOfShelfId + 'I' + index;
                            index++;
                            item.placement.distFromStart = distFromStart;
                            let itemWidth = viewCombinedShelves[i].items[n].placement.pWidth;
                            distFromStart += (itemWidth != undefined ? itemWidth : ProductDefaultDimensions.width) * item.placement.faces;
                            distInShelf += (itemWidth != undefined ? itemWidth : ProductDefaultDimensions.width) * item.placement.faces;
                            if (viewCombinedShelves[shelfId]) {
                                spaceInShelf = viewCombinedShelves[shelfId].dimensions.width - distInShelf;
                                combinedShelves[shelfId].items.push(item);
                            }
                        }
                    }
                }
            }
        }



        let aisle: PlanogramAisle = JSON.parse(JSON.stringify(blankPlanogramAisle));
        let aisles = this.props.store ? this.props.store.aisles : undefined;
        let aisleId = targetAisle.id;
        if (aisles) {
            for (let i = 0; i < aisles.length; i++) {
                if (aisles[i].id === aisleId) {
                    aisle = aisles[i];
                    break;
                }
            }
        }
        if (aisle.id === aisleId) {
            await this.props.setStoreAisle(aisle);
            await this.checkLevel2(aisle);
        }
    }
    alignThisAisleToJustify = async (targetAisle: PlanogramAisle) => {
        // this function will align all continuous shelves in this aisle
        // for that we need to go over each section, each shelf and align it as if it was continuous
        const { aisleShelves, shelvesDetails } = this.props;
        for (let i = 0; i < targetAisle.sections.length; i++) {
            let thisSection = targetAisle.sections[i];
            for (let m = 0; m < thisSection.shelves.length; m++) {
                let thisShelf = thisSection.shelves[m];

                // this is the data for the actual shelf
                let shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[thisShelf.id]));

                // no matter where the user clicked we need to go back and work on the main shelf
                while (shelfDetails.main_shelf) {
                    let ids = JSON.parse(JSON.stringify(shelfDetails.main_shelf));
                    shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[ids]));
                    thisShelf = aisleShelves[ids];
                }

                if (shelfDetails == null) {
                    return null;
                }

                let { margin_bottom, display, combined } = shelfDetails;
                if (margin_bottom == null) margin_bottom = 0;
                if (display == null) display = true;
                if (combined == null) combined = [];

                let realWidth = thisShelf.dimensions.width + (display && combined.length > 0 ? combined.map((sh: any) => {
                    const aShelf = aisleShelves[sh];
                    return aShelf.dimensions.width;
                }).reduce((a: any, b: any) => a + b) : 0);
                let combinedShelves = [thisShelf].concat(combined.map((id: any) => aisleShelves[id]));
                // let shelfItems = combinedShelves.map(sh => sh.items).reduce((p, c) => p.concat(c));
                // save initial state of shelvs with items ordered by distFromStart (distance from start of shelf)
                let combinedShelvesLength = 0;
                let combinedItems: PlanogramItem[] = [];
                for (let i = 0; i < combinedShelves.length; i++) {
                    // sort shelf items by distFromStart
                    combinedShelves[i].items.sort(function (a: any, b: any) {
                        let aNumber = a.placement.distFromStart ? a.placement.distFromStart : 0;
                        let bNumber = b.placement.distFromStart ? b.placement.distFromStart : 0;
                        let aId = a.Id;
                        let bId = b.Id;
                        return (aNumber > bNumber) ? 1 : (aNumber < bNumber) ? -1 : ((aId > bId) ? 1 : (aId < bId) ? -1 : 0);
                    });
                    combinedItems = combinedItems.concat(combinedShelves[i].items);
                    // get the combined length of the entire shelf
                    combinedShelvesLength += combinedShelves[i].dimensions.width;
                }
                let viewCombinedShelves = JSON.parse(JSON.stringify(combinedShelves));
                // empty all items from current shelvs
                for (let i = 0; i < combinedShelves.length; i++) {
                    if (combinedShelves[i].freezer != 1 && combinedShelves[i].freezer != 2) combinedShelves[i].items = [];
                }

                // get the combined length of all items on the shelf
                let combinedItemsLength = 0;
                for (let i = 0; i < combinedItems.length; i++) {
                    let itemWidth = combinedItems[i].placement.pWidth;
                    /* Barak 26.11.20 - count items directly on the shelf only (item index < 100) */
                    let itemInd = parseInt(combinedItems[i].id.substring(combinedItems[i].id.indexOf('I') + 1));
                    if (itemInd < 100)
                        combinedItemsLength += (itemWidth != undefined ? itemWidth : ProductDefaultDimensions.width) * combinedItems[i].placement.faces;
                }

                // get the space we need to add between each item on this shelf to get an even spread of items
                let evenSpaceAdded = (combinedShelvesLength - combinedItemsLength) / combinedItems.length;

                let distFromStart = 0;
                let distInShelf = 0;
                let index = 0;
                let startOfShelfId = '';
                let spaceInShelf = 0;
                // go over the initial state and populate each shelf in combinedShelves with the correct items in the correct order
                let shelfId = 0;
                let combinedItemsIndex = 0;
                for (let i = 0; i < viewCombinedShelves.length; i++) {
                    if (viewCombinedShelves[i].freezer === 1) {
                        continue;
                    };
                    if (i === 0) {
                        startOfShelfId = viewCombinedShelves[i].id;
                        spaceInShelf = viewCombinedShelves[shelfId].dimensions.width;
                    }
                    for (let n = 0; n < viewCombinedShelves[i].items.length; n++) {
                        let item = JSON.parse(JSON.stringify(viewCombinedShelves[i].items[n]));
                        /* Barak 26.11.20 - return items above main line (item index >= 100) back into the shelf without altering anything else */
                        let itemInd = parseInt(item.id.substring(item.id.indexOf('I') + 1));
                        if (itemInd >= 100) {
                            let shelfInd = combinedShelves.findIndex(line => line.id === item.id.substring(0, item.id.indexOf('I')));
                            if (shelfInd >= 0) combinedShelves[shelfInd].items.push(item);
                        }
                        else {
                            if (evenSpaceAdded > 0) {
                                // if there is just one item on the shelf
                                if (combinedItems.length === 1) {
                                    distFromStart += evenSpaceAdded / 2;
                                    distInShelf += evenSpaceAdded / 2;
                                    spaceInShelf -= evenSpaceAdded / 2;
                                }
                                // if there are two item on the shelf and this is the second item 
                                if (combinedItems.length === 2 && combinedItemsIndex === 1) {
                                    distFromStart += evenSpaceAdded * 2;
                                    distInShelf += evenSpaceAdded * 2;
                                    spaceInShelf -= evenSpaceAdded * 2;
                                }
                                // if there are more than two item on the shelf and this is NOT the first one or the last one
                                if (combinedItems.length > 2 && combinedItemsIndex > 0 && combinedItemsIndex < combinedItems.length) {
                                    distFromStart += evenSpaceAdded * (combinedItems.length / (combinedItems.length - 1));
                                    distInShelf += evenSpaceAdded * (combinedItems.length / (combinedItems.length - 1));
                                    spaceInShelf -= evenSpaceAdded * (combinedItems.length / (combinedItems.length - 1));
                                }
                            }
                            if (spaceInShelf <= 0) {
                                shelfId++; // the shelf id in the viewCombinedShelves array
                                if (viewCombinedShelves[shelfId]) {
                                    // there is a next shelf so we update the shelfNumber, startOfShelfId, spaceInShelf and index
                                    // let shelfNumber = parseInt(JSON.parse(JSON.stringify(startOfShelfId.substring(startOfShelfId.indexOf('SE') + 2, startOfShelfId.indexOf('SH'))))) + 1;
                                    let old_sec_num = parseInt(JSON.parse(JSON.stringify(startOfShelfId.substring(startOfShelfId.indexOf('SE') + 2, startOfShelfId.indexOf('SH')))));
                                    let new_sec_num = parseInt(JSON.parse(JSON.stringify(viewCombinedShelves[shelfId].id.substring(viewCombinedShelves[shelfId].id.indexOf('SE') + 2, viewCombinedShelves[shelfId].id.indexOf('SH')))))
                                    startOfShelfId = JSON.parse(JSON.stringify(startOfShelfId.substring(0, startOfShelfId.indexOf('SE') + 2) + new_sec_num + startOfShelfId.substring(startOfShelfId.indexOf('SH'))));
                                    if (new_sec_num - old_sec_num === 1) {
                                        distInShelf = (spaceInShelf * -1);
                                        distFromStart = (spaceInShelf * -1);
                                        spaceInShelf = viewCombinedShelves[shelfId].dimensions.width - distInShelf;
                                    } else {
                                        distInShelf = 0;
                                        distFromStart = 0;
                                        spaceInShelf = 0;
                                    }
                                    index = 0;
                                } else
                                    // this shelf dosn't exist in the arry so we stay in this shelf
                                    shelfId--;
                            }
                            item.id = startOfShelfId + 'I' + index;
                            index++;
                            item.placement.distFromStart = distFromStart;
                            let itemWidth = viewCombinedShelves[i].items[n].placement.pWidth;
                            distFromStart += (itemWidth != undefined ? itemWidth : ProductDefaultDimensions.width) * item.placement.faces;
                            distInShelf += (itemWidth != undefined ? itemWidth : ProductDefaultDimensions.width) * item.placement.faces;
                            combinedItemsIndex++;
                            if (viewCombinedShelves[shelfId]) {
                                spaceInShelf = viewCombinedShelves[shelfId].dimensions.width - distInShelf;
                                combinedShelves[shelfId].items.push(item);
                            }
                        }
                    }
                }
            }
        }


        let aisle: PlanogramAisle = JSON.parse(JSON.stringify(blankPlanogramAisle));
        let aisles = this.props.store ? this.props.store.aisles : undefined;
        let aisleId = targetAisle.id;
        if (aisles) {
            for (let i = 0; i < aisles.length; i++) {
                if (aisles[i].id === aisleId) {
                    aisle = aisles[i];
                    break;
                }
            }
        }
        if (aisle.id === aisleId) {
            await this.props.setStoreAisle(aisle);
            await this.checkLevel2(aisle);
        }
    }
    alignThisAisleToRight = async (targetAisle: PlanogramAisle) => {
        // this function will align all continuous shelves in this aisle
        // for that we need to go over each section, each shelf and align it as if it was continuous
        const { aisleShelves, shelvesDetails } = this.props;
        for (let i = targetAisle.sections.length - 1; i >= 0; i--) {
            let thisSection = targetAisle.sections[i];
            for (let m = thisSection.shelves.length - 1; m >= 0; m--) {
                let thisShelf = thisSection.shelves[m];

                // this is the data for the actual shelf
                let shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[thisShelf.id]));

                // no matter where the user clicked we need to go back and work on the main shelf
                while (shelfDetails.main_shelf) {
                    let ids = JSON.parse(JSON.stringify(shelfDetails.main_shelf));
                    shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[ids]));
                    thisShelf = aisleShelves[ids];
                }

                if (shelfDetails == null) {
                    return null;
                }

                let { margin_bottom, display, combined } = shelfDetails;
                if (margin_bottom == null) margin_bottom = 0;
                if (display == null) display = true;
                if (combined == null) combined = [];

                let realWidth = thisShelf.dimensions.width + (display && combined.length > 0 ? combined.map((sh: any) => {
                    const aShelf = aisleShelves[sh];
                    return aShelf.dimensions.width;
                }).reduce((a: any, b: any) => a + b) : 0);
                let combinedShelves = [thisShelf].concat(combined.map((id: any) => aisleShelves[id]));
                // let shelfItems = combinedShelves.map(sh => sh.items).reduce((p, c) => p.concat(c));
                // save initial state of shelvs with items ordered by distFromStart (distance from start of shelf)
                let combinedItems: PlanogramItem[] = [];
                let combinedShelvesLength = 0;
                for (let i = 0; i < combinedShelves.length; i++) {
                    // sort shelf items by distFromStart
                    combinedShelves[i].items.sort(function (a: any, b: any) {
                        let aNumber = a.placement.distFromStart ? a.placement.distFromStart : 0;
                        let bNumber = b.placement.distFromStart ? b.placement.distFromStart : 0;
                        let aId = a.Id;
                        let bId = b.Id;
                        return (aNumber > bNumber) ? 1 : (aNumber < bNumber) ? -1 : ((aId > bId) ? 1 : (aId < bId) ? -1 : 0);
                    });
                    combinedItems = combinedItems.concat(combinedShelves[i].items);
                    // get the combined length of the entire shelf
                    combinedShelvesLength += combinedShelves[i].dimensions.width;
                }
                let viewCombinedShelves = JSON.parse(JSON.stringify(combinedShelves));

                // get the combined length of all items on the shelf
                let combinedItemsLength = 0;
                for (let i = 0; i < combinedItems.length; i++) {
                    let itemWidth = combinedItems[i].placement.pWidth;
                    combinedItemsLength += (itemWidth != undefined ? itemWidth : ProductDefaultDimensions.width) * combinedItems[i].placement.faces;
                }

                // if the total length of the items is longer than the total width of the shelf DO NOT ALLOW TO ALIGN RIGHT
                if (combinedItemsLength > combinedShelvesLength) {
                    // let errorMsg = 'לא ניתן לבצע הצמדה לימין במדף ' + viewCombinedShelves[0].id + ' - יש בו מוצרים גולשים';
                    let errorMsg = 'Cannot align shelf - ' + viewCombinedShelves[0].id + ' to the right - it has overflowing items';
                    alert(errorMsg);
                    continue;
                }

                // empty all items from current shelvs
                for (let i = 0; i < combinedShelves.length; i++) {
                    if (combinedShelves[i].freezer != 1 && combinedShelves[i].freezer != 2) combinedShelves[i].items = [];
                }

                let distFromStart = viewCombinedShelves[viewCombinedShelves.length - 1].dimensions.width;
                let distInShelf = viewCombinedShelves[viewCombinedShelves.length - 1].dimensions.width;
                let index = viewCombinedShelves[viewCombinedShelves.length - 1].items.length - 1;
                let startOfShelfId = '';
                let spaceInShelf = viewCombinedShelves[viewCombinedShelves.length - 1].dimensions.width;
                // go over the initial state and populate each shelf in combinedShelves with the correct items in the correct order
                let shelfId = viewCombinedShelves.length - 1;
                for (let i = viewCombinedShelves.length - 1; i >= 0; i--) {
                    if (viewCombinedShelves[i].freezer === 1) {
                        continue;
                    };
                    if (i === viewCombinedShelves.length - 1) {
                        startOfShelfId = viewCombinedShelves[i].id;
                        spaceInShelf = viewCombinedShelves[shelfId].dimensions.width;
                    }
                    for (let n = viewCombinedShelves[i].items.length - 1; n >= 0; n--) {
                        let item = JSON.parse(JSON.stringify(viewCombinedShelves[i].items[n]));
                        /* return items above main line (item index >= 100) back into the shelf without altering anything else */
                        let itemInd = parseInt(item.id.substring(item.id.indexOf('I') + 1));
                        if (itemInd >= 100) {
                            let shelfInd = combinedShelves.findIndex(line => line.id === item.id.substring(0, item.id.indexOf('I')));
                            if (shelfInd >= 0) combinedShelves[shelfInd].items.push(item);
                        }
                        else {
                            if (spaceInShelf <= 0) {
                                shelfId--; // the shelf id in the viewCombinedShelves array
                                if (viewCombinedShelves[shelfId]) {
                                    // there is a next shelf so we update the shelfNumber, startOfShelfId, spaceInShelf and index
                                    // let shelfNumber = parseInt(JSON.parse(JSON.stringify(startOfShelfId.substring(startOfShelfId.indexOf('SE') + 2, startOfShelfId.indexOf('SH'))))) - 1;
                                    let old_sec_num = parseInt(JSON.parse(JSON.stringify(startOfShelfId.substring(startOfShelfId.indexOf('SE') + 2, startOfShelfId.indexOf('SH')))));
                                    let new_sec_num = parseInt(JSON.parse(JSON.stringify(viewCombinedShelves[shelfId].id.substring(viewCombinedShelves[shelfId].id.indexOf('SE') + 2, viewCombinedShelves[shelfId].id.indexOf('SH')))))
                                    startOfShelfId = JSON.parse(JSON.stringify(startOfShelfId.substring(0, startOfShelfId.indexOf('SE') + 2) + new_sec_num + startOfShelfId.substring(startOfShelfId.indexOf('SH'))));
                                    // startOfShelfId = JSON.parse(JSON.stringify(startOfShelfId.substring(0, startOfShelfId.indexOf('SE') + 2) + shelfNumber + startOfShelfId.substring(startOfShelfId.indexOf('SH'))));
                                    if (new_sec_num - old_sec_num === -1) {
                                        distInShelf = viewCombinedShelves[viewCombinedShelves.length - 1].dimensions.width - (spaceInShelf * -1);
                                        distFromStart = viewCombinedShelves[viewCombinedShelves.length - 1].dimensions.width - (spaceInShelf * -1);
                                        spaceInShelf = distInShelf; // viewCombinedShelves[shelfId].dimensions.width - distInShelf;
                                    } else {
                                        distInShelf = viewCombinedShelves[viewCombinedShelves.length - 1].dimensions.width;
                                        distFromStart = viewCombinedShelves[viewCombinedShelves.length - 1].dimensions.width;
                                        spaceInShelf = distInShelf;;
                                    }
                                    index = viewCombinedShelves[shelfId].items.length - 1;
                                } else
                                    // this shelf dosn't exist in the arry so we stay in this shelf
                                    shelfId++;
                            }
                            item.id = startOfShelfId + 'I' + index;
                            index--;
                            let itemWidth = viewCombinedShelves[i].items[n].placement.pWidth;
                            distFromStart -= (itemWidth != undefined ? itemWidth : ProductDefaultDimensions.width) * item.placement.faces;
                            distInShelf -= (itemWidth != undefined ? itemWidth : ProductDefaultDimensions.width) * item.placement.faces;
                            item.placement.distFromStart = distFromStart;
                            if (viewCombinedShelves[shelfId]) {
                                spaceInShelf = distInShelf; // viewCombinedShelves[shelfId].dimensions.width - distInShelf;
                                combinedShelves[shelfId].items.push(item);
                            }
                        }
                    }
                }
            }
        }



        let aisle: PlanogramAisle = JSON.parse(JSON.stringify(blankPlanogramAisle));
        let aisles = this.props.store ? this.props.store.aisles : undefined;
        let aisleId = targetAisle.id;
        if (aisles) {
            for (let i = 0; i < aisles.length; i++) {
                if (aisles[i].id === aisleId) {
                    aisle = aisles[i];
                    break;
                }
            }
        }
        if (aisle.id === aisleId) {
            await this.props.setStoreAisle(aisle);
            await this.checkLevel2(aisle);
        }
    }
    checkLevel2 = async (aisle: PlanogramAisle) => {
        let combinedItems: any[] = [];
        for (let a = 0; a < aisle.sections.length; a++) {
            for (let b = 0; b < aisle.sections[a].shelves.length; b++) {
                combinedItems = combinedItems.concat(aisle.sections[a].shelves[b].items);
            }
        }
        for (let c = 0; c < combinedItems.length; c++) {
            if (combinedItems[c].linkedItemUp) {
                let upItem = combinedItems.filter((line: any) => line.id === combinedItems[c].linkedItemUp);
                if (upItem && upItem[0]) {
                    let itemToMove = JSON.parse(JSON.stringify(upItem[0]));
                    // find location of item to move
                    let moveSec = parseInt(itemToMove.id.substring(itemToMove.id.indexOf('SE') + 2, itemToMove.id.indexOf('SH')));
                    let moveSh = parseInt(itemToMove.id.substring(itemToMove.id.indexOf('SH') + 2, itemToMove.id.indexOf('I')));
                    let index = aisle.sections[moveSec].shelves[moveSh].items.findIndex(line => line.id === itemToMove.id);
                    // remove this item from original shelf
                    aisle.sections[moveSec].shelves[moveSh].items.splice(index, 1);
                    // update itemToMove properties
                    itemToMove.placement.distFromStart = combinedItems[c].placement.distFromStart;
                    itemToMove.linkedItemDown = combinedItems[c].id;
                    // itemToMove.id = combinedItems[c].id.substring(0, combinedItems[c].id.indexOf('I')) + 'I' + itemToMove.id.substring(upItem[0].id.indexOf('I') + 1);
                    itemToMove.id = ''
                    // find new location of item and create new item id
                    let destinationSec = parseInt(combinedItems[c].id.substring(combinedItems[c].id.indexOf('SE') + 2, combinedItems[c].id.indexOf('SH')));
                    let destinationSh = parseInt(combinedItems[c].id.substring(combinedItems[c].id.indexOf('SH') + 2, combinedItems[c].id.indexOf('I')));
                    if (aisle.sections[destinationSec].shelves[destinationSh].items.length > 1) {
                        let lastI = parseInt(aisle.sections[destinationSec].shelves[destinationSh].items[aisle.sections[destinationSec].shelves[destinationSh].items.length - 1].id.substring(aisle.sections[destinationSec].shelves[destinationSh].items[aisle.sections[destinationSec].shelves[destinationSh].items.length - 1].id.indexOf('I') + 1));
                        let newI = 0;
                        if (lastI < 100) newI = lastI + 100;
                        else newI = lastI + 1;
                        itemToMove.id = aisle.sections[destinationSec].shelves[destinationSh].id + "I" + newI;
                    } else {
                        itemToMove.id = aisle.sections[destinationSec].shelves[destinationSh].id + "I" + 100;
                    }
                    // add itemToMove to its new location
                    aisle.sections[destinationSec].shelves[destinationSh].items.push(itemToMove);
                    // update the linkedItemUp with the new item id 
                    combinedItems[c].linkedItemUp = itemToMove.id;
                }
                else {
                    combinedItems[c].linkedItemUp = null;
                }
            }
        }
        await this.props.setStoreAisle(aisle);
    }
    saveSnapShotAisle = async (thisAisle: PlanogramAisle) => {
        // check if the array is empty or for a different aisle
        let tArray = this.props.aisleSaveArray;
        let tIndex = this.props.aisleSaveIndex;
        if (tArray.length === 0 || (tArray.length > 0 && tArray[0].id != thisAisle.id)) {
            let newAisle = await planogramProvider.getStoreAisle(this.props.store ? this.props.store.store_id : 0, thisAisle.aisle_id);
            tArray = [JSON.parse(JSON.stringify(newAisle))];
            tIndex = 0;
            await this.props.setAisleSaveArray(tArray);
            await this.props.setAisleSaveIndex(tIndex);
        }
        let aisle: PlanogramAisle = JSON.parse(JSON.stringify(blankPlanogramAisle));
        let aisles = this.props.store ? this.props.store.aisles : undefined;
        let aisleId = thisAisle.id;
        if (aisles) {
            for (let i = 0; i < aisles.length; i++) {
                if (aisles[i].id === aisleId) {
                    aisle = aisles[i];
                    break;
                }
            }
        }
        if (aisle.id === aisleId) {
            // before we save the new aisle we'll also add it to the aisleSaveArray
            // let tArray = this.props.aisleSaveArray;
            tIndex++;
            // insert into array tArray item aisle at location tIndex, deleting 0 items first
            tArray.splice(tIndex, 0, JSON.parse(JSON.stringify(aisle)));
            if (tArray.length > tIndex + 1) {
                tArray.length = tIndex + 1;
            }
            await this.props.setAisleSaveArray(tArray);
            await this.props.setAisleSaveIndex(tIndex);
            await this.props.setStoreAisle(aisle);
        }
    }
    getEmptyAisle = async (aisleId: number) => {
        if (!this.state.isLoading) {
            this.setState({ isLoading: true });
            let newAisle = await planogramApi.fetchPlanogramAisle(this.props.store ? this.props.store.store_id : '', aisleId, this.props.user ? this.props.user.db : '');
            if (!newAisle || newAisle === undefined) {
                alert("Problem retrieving the aisle from the server");
                return;
            }
            if (this.props.store) {
                let store = this.props.store;
                for (let i = 0; i < store.aisles.length; i++) {
                    if (store.aisles[i] != undefined && newAisle != undefined && store.aisles[i].aisle_id === newAisle.aisle_id) {
                        store.aisles[i] = newAisle;
                        await this.props.setStore(store);
                        break;
                    }
                }
            }
        }
        this.setState({ isLoading: false });
    }

    resetMyZoom = async () => {
        const { store } = this.props;
        if (!store || !this.props.aisle) return null;
        const aisle = store.aisles.filter(line => line.aisle_id === (this.props.aisle ? this.props.aisle.aisle_id : 0))[0];
        let aisleWidth = aisle.sections.map(v => v.dimensions.width).reduce((p, c) => p + c, 0);
        if (aisleWidth < aisle.dimensions.width)
            aisleWidth = aisle.dimensions.width;
        let aisleHeight = aisle.sections.map(v => v.dimensions.height).reduce((p, c) => Math.max(p, c), 0);
        if (aisleHeight < aisle.dimensions.height)
            aisleHeight = aisle.dimensions.height;
        /* osher 17.9.20 - changing the zoom of the aisle according to it's length */
        let myZoom = 0, yPos = 50;
        if (aisleWidth <= 8000) {
            myZoom = 0.4;
            yPos = 100;
        }
        if (aisleWidth > 8000 && aisleWidth <= 10000) {
            myZoom = 0.33;
            yPos = 120;
        }
        if (aisleWidth > 10000 && aisleWidth <= 12000) {
            myZoom = 0.28;
            yPos = 150;
        }
        if (aisleWidth > 12000 && aisleWidth <= 15000) {
            myZoom = 0.22;
            yPos = 200;
        }
        if (aisleWidth > 15000 && aisleWidth <= 18000) {
            myZoom = 0.19;
            yPos = 220;
        }
        if (aisleWidth > 18000) {
            myZoom = 0.17;
            yPos = 250;
        }
        // return myZoom;
        // await this.setState({ myZoom });
        this.props.setZoom(myZoom);
    }
    toggleModal = () =>{
        this.setState({duplicateModal: !this.state.duplicateModal})
    }
    render() {
        let {
            position,
            hideAisle,
            hideAisleToggle,
            store,
            aisleSaveArray,
            setAisleSaveArray,
            setAisleSaveIndex,
            addSection, duplicateSection, editAisleName, deleteAisle,
        } = this.props;
        const customizer = { direction: document.getElementsByTagName("html")[0].dir }

        const { display } = this.state
        if (!display) return null
        if (!store || !this.props.aisle) return null;
        /* check if aisle is empty. if so, get aisle from server */
        const aisle = store.aisles.filter(line => line.aisle_id === (this.props.aisle ? this.props.aisle.aisle_id : 0))[0];
        if (!aisle || aisle.aisle_id === 0) return null



        let aisleWidth = aisle.sections.map(v => v.dimensions.width).reduce((p, c) => p + c, 0);
        if (aisleWidth < aisle.dimensions.width)
            aisleWidth = aisle.dimensions.width;
        let aisleHeight = aisle.sections.map(v => v.dimensions.height).reduce((p, c) => Math.max(p, c), 0);
        if (aisleHeight < aisle.dimensions.height)
            aisleHeight = aisle.dimensions.height;

        if (aisleSaveArray.length === 0) {
            let tArray = [JSON.parse(JSON.stringify(aisle))];
            setAisleSaveArray(tArray);
            setAisleSaveIndex(0);
        }


        
        const returnModal = (modal: any) => {
            return (<Modal zIndex={999999999} isOpen={modal.isOpen} toggle={modal.toggle} className='modal-dialog-centered modal-md text-center width-10-per min-width-25-rem height-18-rem'>
                <ModalHeader onClose={modal.toggle} classNames={modal.headerClasses}>
                    <h5 className="modal-title">{modal.header}</h5>
                </ModalHeader>
                <ModalBody>
                    {modal.body}
                </ModalBody>
                <ModalFooter>
                    <div className="m-auto">
                        {modal.buttons.map((ele: any, index: number) => <Button outline={ele.outline ? true : false} className='round ml-05 mr-05' key={index} color={ele.color} onClick={ele.onClick} disabled={ele.disabled}>{ele.label}</Button>)}
                    </div>
                </ModalFooter>
            </Modal>
            )
        }
       

        const duplicateModalDiv = [{
            headerClasses: 'mr-auto',
            isOpen: this.state.duplicateModal, toggle: () => this.setState({duplicateModal:false}), header: (
            <div className='d-flex-column-w'>
                <span className="font-weight-bold mb-1 align-left"><FormattedMessage id="duplicate-object"/></span>
                <div className='d-flex-column-w'>
                    <span className='align-left'><FormattedMessage id="youChoseDuplicateSection"/></span>
                    <span className='align-left'><FormattedMessage id="howMuchTimesDuplicate"/></span>
                </div>
                </div>
                ),
            body: (
            <div className='d-flex-reverse-row justify-content-center'>
                <div className={classnames('duplicateButton', { 'border-turquoise-2': this.state.activeButton == 0 })} onClick={()=>{this.setState({duplicateAmount:1,activeButton:0})}}>1</div>
                <div className={classnames('duplicateButton', { 'border-turquoise-2': this.state.activeButton == 1 })} onClick={()=>{this.setState({duplicateAmount:2,activeButton:1})}}>2</div>
                <div  className={classnames('duplicateButton', { 'border-turquoise-2': this.state.activeButton == 2 })} onClick={()=>{this.setState({duplicateAmount:5,activeButton:2})}}>5</div>
                <FormattedMessage id='other'>
                    {placeholder=>
                                    <input min="1" max="10" type='number' onClick={()=>{this.setState({activeButton:3})}} onChange={(e:any)=>{console.log(e.target.value);this.setState({duplicateAmount:Number(e.target.value)})}} 
                                    placeholder={`${placeholder?placeholder:''}`} className={classnames('duplicateButton', { 'border-turquoise-2': this.state.activeButton == 3 })}/>
                    }
                </FormattedMessage>

            </div>
                 ),
            buttons: [
                {
                    color: 'primary', onClick: () => {

                        this.state.duplicateData.onClick()
                        hideAisleToggle(true)
                        this.setState({duplicateAmount:0,activeButton:null})
                        this.setState({duplicateData:{onClick:()=>{}}})
                        setTimeout(()=>{this.toggleModal();  hideAisleToggle(false)}, 150)

                    }, label: <FormattedMessage id="duplicate"/>,
                },
                { color: 'primary', outline: true, onClick: () => this.setState({duplicateModal:false}), label: <FormattedMessage id="cancel"/> }
            ]
        }]

        return (
            <div className="planogram-view-inner">
                {/* <Prompt
                     when={!this.state.globalLastChangeSavedState}
                    message="יש שינויים שלא נשמרו. האם ברצונך לעזוב בכל זאת?"
                /> */}
                {/* ********************************************************* */}
                {/* <button className="zindex-10004"
                style={{zIndex: 99999999999999999999999999999999 ,fontSize:50, position: 'absolute', top:0,left:0, }}
                onClick={()=>{this.setState({render: !this.state.render})}} >click</button> */}
                {!hideAisle && (
                <TransformWrapper
                    onZoomChange={(obj: any) => { if (this.props.Zoom !== obj.scale) this.props.setZoom(obj.scale) }}
                    scale={this.props.Zoom != 0 ? this.props.Zoom : 0.4}
                    pan={{
                        disabled: true,
                    }}
                    wheel={{
                        step: 85,
                        wheelEnabled: true,
                        touchPadEnabled: true,
                        limitsOnWheel: false,
                    }}
                    options={{
                        limitToBounds: false,
                        minScale: 0.1,
                        maxScale: 10,
                        centerContent: false,
                    }}
                >
                    {(props: any) => <React.Fragment>
                        <div className="planogram-scale zindex-10001">
                            <div className="scale-label"><FormattedMessage id="zoom" />: x{props.scale.toFixed(1)}</div>
                        </div>
                        <TransformComponent>
                            <div style={{ minHeight: '93vh', maxHeight: '93vh', minWidth: '100vw' }}> 
                            {/* minHeight: '90.5vh', maxHeight: '90.5vh', minWidth: aisleWidth */}
                                <MenuProvider id={"aisle_menu"} key={"aisle_menu_provider"} className="planogram-body">
                                    <div className="planogram-aisle-container">
                                        <Draggable
                                            cancel=".planogram-section, .planogram-context-window, input"
                                            defaultClassNameDragging="dragged"
                                            defaultPosition={{ x: position.x, y: position.y }}
                                            scale={props.scale}
                                            bounds={{ left: -(aisleWidth), top: -aisleHeight, right: aisleWidth, bottom: aisleHeight }}>
                                            <div id="planogram-container" className={classnames("planogram-container", { 'd-rtl': customizer.direction == 'rtl' })}>
                                                <div className="planogram-handle">
                                                    <EditableText text={aisle.name || (`${<FormattedMessage id="AISLE:" />}` + " " + aisle.aisle_id)} onNewText={(newText) => {
                                                        globalLastChangeSaved = false;
                                                        editAisleName(aisle.id, newText);
                                                    }}>{aisle.name || `${<FormattedMessage id="AISLE:" />}` + " " + aisle.aisle_id}</EditableText>
                                                </div>
                                                <PlanogramAisleDroppable
                                                    aisle={aisle}
                                                    AisleRef={this.AisleRef}
                                                    printStatus={Boolean(localStorage.printStatus)}
                                                    printFromInput={this.state.printFromInput}
                                                    printToInput={this.state.printToInput ? this.state.printFromInput : document.getElementsByClassName('planogram-section-wrapper') ? document.getElementsByClassName('planogram-section-wrapper').length : 1}
                                                    onDrop={async (item: any) => {
                                                        if (item.type === PlanogramDragDropTypes.SERIES_SIDEBAR) {
                                                            let classname = "flash-bg-opacity"
                                                            let section_btn: any = document.getElementById('section_btn')
                                                            FlashBtn(classname, section_btn)
                                                        }
                                                        if (item.type === PlanogramDragDropTypes.PRODUCT_SIDEBAR) {
                                                            let classname = "flash-bg-opacity"
                                                            let section_btn: any = document.getElementById('section_btn')
                                                            FlashBtn(classname, section_btn)
                                                            //     const product = productsMap[item.payload];
                                                            //     /* Barak 10.9.20 *  const productDimensions: DimensionObject = catalogProductDimensionObject(product)*/
                                                            //     /* Barak 10.9.20 */
                                                            //     const productDimensions: DimensionObject = {
                                                            //         width: item.payload.placement && item.payload.placement.pWidth ? item.payload.placement.pWidth : ProductDefaultDimensions.width,
                                                            //         height: item.payload.placement && item.payload.placement.pHeight ? item.payload.placement.pHeight : ProductDefaultDimensions.height,
                                                            //         depth: item.payload.placement && item.payload.placement.pDepth ? item.payload.placement.pDepth : ProductDefaultDimensions.depth
                                                            //     }
                                                            //     /****************/

                                                            //     if (!validateDimensions(productDimensions)) {
                                                            //         setModal(() => <DimensionModal
                                                            //             init={ProductDefaultDimensions}
                                                            //             title={"Section Dimensions"}
                                                            //             onSubmit={(dimensions) => {
                                                            //                 /* Barak 23.1.20 */ globalLastChangeSaved = false;
                                                            //                 planogramApi.updateProductDimensions(product.BarCode, dimensions).then((res) => {
                                                            //                     updateProductDimensions(product.BarCode, dimensions, addSection(aisle.id, {
                                                            //                         product: product.BarCode,
                                                            //                         item: {
                                                            //                             id: "",
                                                            //                             product: product.BarCode,
                                                            //                             placement: {
                                                            //                                 ...ItemDefualtPlacement,
                                                            //                                 faces:item.faces,
                                                            //                             },
                                                            //                             /* Barak 24.6.2020 - Add lowSales flag to the item */
                                                            //                             lowSales: false,
                                                            //                             /* Barak 23.7.2020 - Add missing to the item */
                                                            //                             missing: 0,
                                                            //                             /* Barak 28.6.2020 - Add branch_id */
                                                            //                             branch_id: store.branch_id,
                                                            //                             /* Barak 21.12.20 - Add linking item option */
                                                            //                             linkedItemUp: null,
                                                            //                             linkedItemDown: null,
                                                            //                             shelfLevel: 1,
                                                            //                             /********************************************/
                                                            //                         }
                                                            //                     }));
                                                            //                     /* Barak 22.11.20 */ this.saveSnapShotAisle(aisle);
                                                            //                     toggleModal();
                                                            //                 }).catch(console.error)
                                                            //             }} />)
                                                            //   }
                                                            //     else {
                                                            //         /* Barak 23.1.20 */ globalLastChangeSaved = false;
                                                            //         addSection(aisle.id, { product: product.BarCode,
                                                            //             item: {
                                                            //                 id: "",
                                                            //                 product: product.BarCode,
                                                            //                 placement: {
                                                            //                     ...ItemDefualtPlacement,
                                                            //                     faces:item.faces
                                                            //                 },
                                                            //                 /* Barak 24.6.2020 - Add lowSales flag to the item */
                                                            //                 lowSales: false,
                                                            //                 /* Barak 23.7.2020 - Add missing to the item */
                                                            //                 missing: 0,
                                                            //                 /* Barak 28.6.2020 - Add branch_id */
                                                            //                 branch_id: store.branch_id,
                                                            //                 /* Barak 21.12.20 - Add linking item option */
                                                            //                 linkedItemUp: null,
                                                            //                 linkedItemDown: null,
                                                            //                 shelfLevel: 1,
                                                            //                 /********************************************/
                                                            //             }
                                                            //          });
                                                            //         // createNewItem(aisle, item.payload);
                                                            //         /* Barak 22.11.20 */this.saveSnapShotAisle(aisle);
                                                            //     }
                                                        }
                                                        // else 
                                                        if (item.type === PlanogramDragDropTypes.SECTION_SIDEBAR) {

                                                            /* Barak 23.1.20 */ globalLastChangeSaved = false;
                                                            /* Barak 14.9.20 * addSection(aisle.id, {});*/
                                                            /* Barak 14.9.20 - Add default sizes for section and shelf  */
                                                            addSection(aisle.id, {
                                                                dimensions: {
                                                                    width: this.props.store && this.props.store.Section_width ? this.props.store.Section_width : SectionDefaultDimension.width,
                                                                    height: this.props.store && this.props.store.Section_height ? this.props.store.Section_height : SectionDefaultDimension.height,
                                                                    depth: this.props.store && this.props.store.Section_depth ? this.props.store.Section_depth : SectionDefaultDimension.depth,
                                                                }
                                                            });
                                                            /* Barak 22.11.20 */ this.saveSnapShotAisle(aisle);
                                                            /*******************************************************/
                                                        }
                                                        else if (item.type === PlanogramDragDropTypes.SHELF_SIDEBAR
                                                            || item.type === PlanogramDragDropTypes.SHELF
                                                            || item.type === PlanogramDragDropTypes.SHELF_SIDEBAR_FREEZER) {
                                                            let classname = "flash-bg-opacity"
                                                            let section_btn: any = document.getElementById('section_btn')
                                                            FlashBtn(classname, section_btn)
                                                            globalLastChangeSaved = false;
                                                            addSection(aisle.id, {
                                                                shelf: item.payload
                                                            });
                                                            this.saveSnapShotAisle(aisle);
                                                        }
                                                        else if (item.type === PlanogramDragDropTypes.SECTION) {
                                                            this.setState({duplicateData:{onClick: async ()=>{
                                                                globalLastChangeSaved = false
                                                                let duplicateAm = Number(this.state.duplicateAmount)
                                                                for (let i = 0; i < duplicateAm; i++) {
                                                                    this.props.duplicateSection(aisle.id, item.payload);
                                                                }
                                                                setTimeout(async() => {
                                                                await this.saveSnapShotAisle(aisle);
                                                                }, 500);
                                                            }}})
                                                            this.setState({ duplicateModal:true})

                                                            // globalLastChangeSaved = false;
                                                            // let numberDuplicateString = prompt('כמה פעמים לשכפל?');
                                                            // // if the numberDuplicateString is null that means the user pressed 'cancel' so we do nothing
                                                            // if (numberDuplicateString) {
                                                            //     let numberDuplicate = 1;
                                                            //     if (numberDuplicateString && !isNaN(parseInt(numberDuplicateString))) numberDuplicate = parseInt(numberDuplicateString);
                                                            //     if (isNaN(parseInt(numberDuplicateString))) numberDuplicate = 0;
                                                            //     if (numberDuplicate < 0) numberDuplicate = 0;
                                                            //     for (let i = 0; i < numberDuplicate; i++) {
                                                            //         duplicateSection(aisle.id, item.payload);
                                                            //     }
                                                            //     this.saveSnapShotAisle(aisle);
                                                            // }
                                                        }
                                                    }}
                                                />
                                            </div>
                                        </Draggable>
                                    </div>
                                </MenuProvider>
                                <Menu
                                    onHidden={() => {
                                        localStorage.setItem('printFromInput', String(1))
                                        localStorage.setItem('printToInput', String(document.getElementsByClassName('planogram-section-wrapper').length))
                                        this.setState({
                                            showPrintInput: false,
                                            printFromInput: 1,
                                            printToInput: document.getElementsByClassName('planogram-section-wrapper') ? document.getElementsByClassName('planogram-section-wrapper').length : undefined
                                        })
                                    }}
                                    onOutsideClick={() => {
                                        localStorage.setItem('printFromInput', String(1))
                                        localStorage.setItem('printToInput', String(document.getElementsByClassName('planogram-section-wrapper').length))
                                        this.setState({
                                            showPrintInput: false,
                                            printFromInput: 1,
                                            printToInput: document.getElementsByClassName('planogram-section-wrapper') ? document.getElementsByClassName('planogram-section-wrapper').length : undefined
                                        })
                                    }}
                                    id={"aisle_menu"} animation="pop" key={"aisle_menu_context"}>
                                    <div className="planogram-context-window">
                                        <div className="context-title"><FormattedMessage id="Aisle" />: {aisle.name || aisle.aisle_id}</div>

                                        <div className="input-row">
                                            <button onClick={async (e) => {
                                                await this.alignThisAisleToLeft(aisle);
                                                this.saveSnapShotAisle(aisle);
                                            }}>
                                                <FontAwesomeIcon icon={faAlignLeft} />
                                                <span><FormattedMessage id="align-to-left" /></span>
                                            </button>
                                        </div>
                                        <div className="input-row">
                                            <button onClick={async (e) => {
                                                await this.alignThisAisleToRight(aisle);
                                                this.saveSnapShotAisle(aisle);
                                            }}>
                                                <FontAwesomeIcon icon={faAlignRight} />
                                                <span><FormattedMessage id="align-to-right" /></span>
                                            </button>
                                        </div>
                                        <div className="input-row">
                                            <button onClick={async (e) => {
                                                await this.alignThisAisleToJustify(aisle);
                                                this.saveSnapShotAisle(aisle);
                                            }}>
                                                <FontAwesomeIcon icon={faAlignJustify} />
                                                <span><FormattedMessage id="align-justify" /></span>
                                            </button>
                                        </div>
                                        <div className="input-row">
                                            <button onClick={this.saveAisle}>
                                                <FontAwesomeIcon icon={faSave} />
                                                <span><FormattedMessage id="save-aisle" /></span>
                                            </button>
                                        </div>
                                        <div className="input-row">
                                            <FileLoadingButton onData={(data) => {
                                                try {
                                                    data = JSON.parse(data);
                                                    if (!data || !data.sections)
                                                        return;
                                                    globalLastChangeSaved = false;
                                                    this.props.setStoreAisle(data, aisle.id);
                                                    this.saveSnapShotAisle(aisle);
                                                } catch (err) {
                                                    console.error(err);
                                                    alert("Unable to load aisle");
                                                }
                                            }}>
                                                <FontAwesomeIcon icon={faUpload} />
                                                <span><FormattedMessage id="load-aisle" /></span>
                                            </FileLoadingButton>
                                        </div>
                                        {this.props.user && this.props.user.level === 0
                                            ?
                                            <div className="input-row">
                                                <button onClick={async (e) => {
                                                    let IdString = prompt("What is the Blob's Id?");
                                                    if (IdString) {
                                                        this.setState({ isLoading: true });
                                                        let newAisle = await planogramApi.fetchPlanogramBlobById(parseInt(IdString), this.props.store ? this.props.store.store_id : '', aisle.aisle_id, this.props.user ? this.props.user.db : '');
                                                        if (this.props.store) {
                                                            let store = this.props.store;
                                                            for (let i = 0; i < store.aisles.length; i++) {
                                                                if (store.aisles[i] != undefined && newAisle != undefined && store.aisles[i].aisle_id === newAisle.aisle_id) {
                                                                    store.aisles[i] = newAisle;
                                                                    await this.props.setStore(store);
                                                                    break;
                                                                }
                                                            }
                                                        }
                                                        this.setState({ isLoading: false });
                                                    }
                                                }}>
                                                    <FontAwesomeIcon icon={faCloudUploadAlt} />
                                                    <span>Load Aisle From Backup</span>
                                                </button>
                                            </div>
                                            : null}
                                        <div className="input-row">
                                            <JsonDownloadButton filename={fileReadyCurrentTime() + "_" + (store ? store.store_id + "_" : "") + aisle.aisle_id + " - " + aisle.name} getData={() => aisle} >
                                                <FontAwesomeIcon icon={faDownload} />
                                                <span><FormattedMessage id="download-aisle" /></span>
                                            </JsonDownloadButton>
                                        </div>
                                        <div className="input-row">
                                            <button onClick={e => {
                                                if (!store || !aisle.aisle_id) return;
                                                if (window.confirm("Are you sure you want to delete: " + (aisle.name || "Aisle " + aisle.id))) {
                                                    globalLastChangeSaved = false;
                                                    planogramApi.deleteStoreAisle(store.store_id, aisle.aisle_id).then(() => {
                                                        deleteAisle(aisle.id);
                                                        alert("Aisle was successfully deleted.");
                                                    }).catch(err => {
                                                        console.error(err);
                                                        alert("Unable to delete aisle")
                                                    });
                                                }
                                            }}>
                                                <FontAwesomeIcon icon={faTrash} />
                                                <span><FormattedMessage id="delete-aisle" /></span>
                                            </button>
                                        </div>
                                        <div className="input-row">
                                            <button
                                                onClick={() => {
                                                    localStorage.setItem('printStatus', String(true))
                                                    this.setState({
                                                        printStatus: true
                                                    }, () => {
                                                        window.print();
                                                    })
                                                }}
                                            >
                                                <FontAwesomeIcon icon={faPrint} />
                                                <span><FormattedMessage id="print-section" /></span>
                                            </button>
                                        </div>
                                        {this.state.showPrintInput ?
                                            <div className="input-row">
                                                <span>from:</span>
                                                <input type="number"
                                                    value={this.state.printFromInput}
                                                    onChange={(e) => {
                                                        this.setState({ printFromInput: e.target.value });
                                                        localStorage.setItem('printFromInput', String(e.target.value))
                                                    }}
                                                    min={1} max={document.getElementsByClassName('planogram-section-wrapper').length} />
                                                <span>to:</span>
                                                <input type="number"
                                                    min={1}
                                                    max={document.getElementsByClassName('planogram-section-wrapper').length}
                                                    onChange={(e) => {
                                                        this.setState({ printToInput: e.target.value });
                                                        localStorage.setItem('printToInput', String(e.target.value))
                                                    }}
                                                    value={this.state.printToInput ? this.state.printToInput : document.getElementsByClassName('planogram-section-wrapper').length}
                                                />
                                                {/* <ReactToPrint
                                                    copyStyles
                                                    onBeforeGetContent={()=>this.setState({printStatus: true})}
                                                    onAfterPrint={()=>this.setState({printStatus: false})}
                                                    trigger={() => }
                                                    content={() => this.AisleRef.current}
                                                 /> */}
                                                <button onClick={() => {
                                                    localStorage.setItem('printStatus', String(true))
                                                    this.setState({
                                                        printStatus: true
                                                    }, () => {
                                                        window.print();
                                                    })
                                                }}>
                                                    Print</button>
                                            </div>
                                            : null}
                                    </div>
                                </Menu>
                            </div >
                        </TransformComponent>
                    </React.Fragment>}
                </TransformWrapper>
                )}
                {this.state.duplicateModal ? returnModal(duplicateModalDiv[0]) : null}

            </div >
        );
    };
}

export default withRouter(connect(mainMapStateToProps, mainMapDispatchToProps)(PlanogramAisleContainer));
