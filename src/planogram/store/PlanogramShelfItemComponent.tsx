import * as React from "react";
import { PlanogramDragDropTypes } from "../generic/DragAndDropType";
import { DropTarget, DragElementWrapper, DragSource } from 'react-dnd';
import { DragDropResultBase } from "../generic/DragDropWrapper";
import { PlanogramItem, PlacementObject, DimensionObject, PlanogramShelf, PlanogramElementId, PlanogramAisle } from "@src/planogram/shared/store/planogram/planogram.types";
import { connect } from "react-redux";
import { heightDensity, widthDensity } from "@src/planogram/provider/calculation.service";
import { productPredictedSales } from "@src/planogram/shared/api/planogram.provider";
import { DimensionModal } from "../modals/DimensionModal";
import * as planogramApi from '@src/planogram/shared/api/planogram.provider';
import { AppState } from "@src/planogram/shared/store/app.reducer";
import { editProductDimensions, setWeeklySale } from "@src/planogram/shared/store/catalog/catalog.action";
import { catalogProductDimensionObject } from "@src/planogram/provider/planogram.service";
import { Dispatch } from "redux";
import { setProductDetailer,  setMultiSelectId, setCurrentSelectedId, setAisleSaveArray, setAisleSaveIndex } from "@src/planogram/shared/store/planogram/planogram.actions";
import { CatalogBarcode } from "@src/planogram/shared/interfaces/models/CatalogProduct";
import { ThunkDispatch } from "redux-thunk";
import noImage from "@assets/images/planogram/no-image.jpg"
import { editShelfItemAction} from "@src/planogram/shared/store/planogram/store/item/item.actions";
import { contextMenu, Menu } from "../generic/ContextMenu";
import { blankItem, blankPlanogramAisle, ItemMaxPlacement, ProductDefaultDimensions } from "@src/planogram/shared/store/planogram/planogram.defaults";
import { barcodeImageSrc } from "../generic/BarcodeImage";
import { setAisleAction } from "@src/planogram/shared/store/planogram/store/aisle/aisle.actions";
import * as planogramProvider from "@src/planogram/shared/api/planogram.provider";
interface SourceCollectProps {
    connectDragSource: DragElementWrapper<any>,
    isDragging: boolean,
}

interface TargetCollectProps {
    connectDropTarget: DragElementWrapper<any>,
    canDrop: boolean,
}

interface PlanogramItemComponentProps {
    printItem?: Boolean
    printItemObj?: any
    item: PlanogramItem,
    shelf: PlanogramShelf,
    aisleId: number,
    netw: string,
    userPhone: string,
    dragImg: any,
    shelfHeight: number | null,
    editItem?: () => void,
    onEndDrag?: (item: DragDropResultBase, targetItem: DragDropResultBase) => void,
    onDrop?: (item: DragDropResultBase) => void,
    move?: (item: DragDropResultBase, targetItem: DragDropResultBase) => void,
    remove?: (item: DragDropResultBase) => void,
    verifyDrop: (droppedItem: DragDropResultBase) => boolean,
}

const DraggableSource = DragSource<PlanogramItemComponentProps, SourceCollectProps & SourceCollectProps>(
    PlanogramDragDropTypes.SHELF_ITEM, {
    beginDrag(props, monitor) {
        const { item } = props;
        return ({
            type: PlanogramDragDropTypes.SHELF_ITEM,
            payload: item,
        });
    },
    endDrag(props, monitor) {
        if (!monitor.didDrop())
            return;
        if (props.onEndDrag)
            props.onEndDrag(monitor.getItem(), monitor.getDropResult());
    },
}, (connect, monitor) => ({
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging(),
}));

const dropTypes = [
    PlanogramDragDropTypes.PRODUCT_SIDEBAR,
    PlanogramDragDropTypes.SERIES_SIDEBAR,
    PlanogramDragDropTypes.SHELF_ITEM
]

const DroppableTarget = DropTarget<PlanogramItemComponentProps, TargetCollectProps>(
    dropTypes, {
    drop(props, monitor) {
        const { item } = props;
        if (props.onDrop)
            props.onDrop(monitor.getItem());
        return ({
            type: dropTypes,
            payload: item,
            distFromStart: 'item'
        });
    },
    canDrop(props, monitor) {
        if (monitor.didDrop() || !monitor.isOver({ shallow: true }))
            return false;
        return props.verifyDrop(monitor.getItem());
    }
}, (connect, monitor) => ({
    connectDropTarget: connect.dropTarget(),
    // canDrop: monitor.canDrop() && monitor.isOver({ shallow: true }),
    canDrop: monitor.canDrop(),
}));

type ItemProps = PlanogramItemComponentProps & SourceCollectProps & TargetCollectProps;
type DispatchProps = ReturnType<typeof mapDispatchToProps>;
type StateProps = ReturnType<typeof mapStateToProps>;
function mapDispatchToProps(dispatch: Dispatch) {
    return {
        editShelfItemPlacement: (item: PlanogramElementId, placement: PlacementObject) => {
            dispatch(editShelfItemAction(item, placement));
        },
        updateProductDimensions: (barcode: number, dimensions: DimensionObject) => {
            dispatch(editProductDimensions(barcode, dimensions));
        },
        setProductDetailerDisplay: (barcode: CatalogBarcode, lowSales: boolean, shelfFaces: number, missing: number, netw: string, userPhone: string) => {
            dispatch(setProductDetailer(barcode, lowSales, shelfFaces, missing, netw, userPhone));
        },
        setCurrentSelectedId: (currentSelectedId: string) => dispatch(setCurrentSelectedId(currentSelectedId)),
        setMultiSelectId: (multiSelectId: string[]) => dispatch(setMultiSelectId(multiSelectId)),
        setStoreAisle: (aisle: PlanogramAisle, aislePid?: PlanogramElementId) => {
            dispatch(setAisleAction(aisle, aislePid));
        },
        setAisleSaveArray: (aisleSaveArray: PlanogramAisle[]) => {
            dispatch(setAisleSaveArray(aisleSaveArray));
        },
        setAisleSaveIndex: (aisleSaveIndex: number) => {
            dispatch(setAisleSaveIndex(aisleSaveIndex));
        },
    }
}
function mapStateToProps(state: AppState, ownProps: ItemProps) {
    const catalogProduct: any = state.newPlano && state.newPlano.data ? state.newPlano.data.filter((item:any)=> item.BarCode === ownProps.item.product)[0] : null;
    return {
        ...ownProps,
        catalogProduct,
        productDetail: state.planogram.productDetails[ownProps.item.product],
        hoverProduct: state.planogram.display.productDetailer.barcode,
        hideShelfItems: state.displayOptions?.hideProducts,
        showRowItems: state.displayOptions?.showProdutDepth,
        colorReports: state.colorReports,
        overlayObj: catalogProduct && state.colorReports && state.colorReports.rows && state.colorReports.merged && catalogProduct[state.colorReports.merged.id] > -1 ?
            state.colorReports.rows.filter((row: any) => { return row.id === catalogProduct[state.colorReports.merged.id] })[0] : null,
        multiSelectId: state.planogram.display.multiSelectId,
        currentSelectedId: state.planogram.display.currentSelectedId,
        colorBy: state.planogram.display.colorBy,
        store: state.planogram.store,
        shelvesDetails: state.planogram.virtualStore.shelfDetails,
        aisleShelves: state.planogram.virtualStore.shelfMap,
        virtualStore: state.planogram.virtualStore,
        aisleSaveArray: state.planogram.display.aisleSaveArray,
        aisleSaveIndex: state.planogram.display.aisleSaveIndex,
        hideSaleTags: state.displayOptions?.hideTags,
        user: state.auth.user,
    }
}
const StoreConnector = connect(mapStateToProps, mapDispatchToProps);
const pixelSideStack = 3;
const pixelTopStack = 3;
function rangeArray(num: number, shelf?: any, item?: any, printItemObj?: any) {

    const list = [];
    for (let i = 1; i <= num; i++) {
        list.push(i);
    }

    if (localStorage.printStatus === 'true' &&
        item && shelf && printItemObj && printItemObj.item.placement.pWidth &&
        shelf.dimensions.width < (printItemObj.item.placement.pWidth * printItemObj.item.placement.faces)) {
        return list.filter((item, index) => { if (index < printItemObj.times) return item })
    }
    return list;
}
class PlanogramShelfItemComponent extends React.Component<DispatchProps & StateProps> {
    keyboardTimeout?: NodeJS.Timeout;
    keyboardCollector = "";

    state = {
        dimensionMenu: false,
        dragImg: new Image(),
        clicked: false,
    }

    handleKeyboard = (e: KeyboardEvent) => { if (this.state.dimensionMenu) return; }
    bindKeyboard = () => { window.addEventListener('keydown', this.handleKeyboard); }
    unBindKeyboard = () => { window.removeEventListener('keydown', this.handleKeyboard); }

    modifyDragImage = (dataTransfer: any) => {
        // let img = new Image();
        // img.src = 'https://www.google.no/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png';
        // dataTransfer.setDragImage(this.props.dragImg, 0, 0);
        var elem = document.createElement("div");
        elem.id = "drag-ghost";
        elem.style.backgroundColor = "yellow";
        elem.style.width = "200px";
        elem.style.height = "200px";
        elem.style.zIndex = "9999";
        document.body.appendChild(elem);
        var crt = document.createElement("div");
        crt.style.backgroundColor = "red";
        crt.style.position = "absolute"; crt.style.top = "0px"; crt.style.right = "0px";
        document.body.appendChild(crt);
        // dataTransfer.setDragImage(crt, 0, 0);
    }

    alignThisShelfToLeft = async (shelf: PlanogramShelf) => {
        const { store, aisleShelves, shelvesDetails } = this.props;

        // check if we need to align multiple shelves
        let all_shelves_to_align: PlanogramShelf[] = [];
        if (this.props.multiSelectId.length > 1) {
            // sort multiSelectId by shelf and section
            this.props.multiSelectId.sort(function (a: any, b: any) {
                let currSE_A: number = parseInt(a.substring(a.indexOf('SE') + 2, a.indexOf('SH')));
                let currSH_A: number = parseInt(a.substring(a.indexOf('SH') + 2, a.indexOf('I')));
                let currSE_B: number = parseInt(b.substring(b.indexOf('SE') + 2, b.indexOf('SH')));
                let currSH_B: number = parseInt(b.substring(b.indexOf('SH') + 2, b.indexOf('I')));
                // if it's the same shelf order by section    
                if (currSH_A === currSH_B)
                    return (currSE_A > currSE_B) ? 1 : (currSE_A < currSE_B) ? -1 : 0;
                else return (currSH_A > currSH_B) ? 1 : (currSH_A < currSH_B) ? -1 : 0;
            });
            // find each shelf represented in multiSelectId and add them to all_shelves_to_align
            for (let a = 0; a < this.props.multiSelectId.length; a++) {
                let target_A = this.props.multiSelectId[a].substring(0, this.props.multiSelectId[a].indexOf('SE'));
                let target_SE = parseInt(this.props.multiSelectId[a].substring(this.props.multiSelectId[a].indexOf('SE') + 2, this.props.multiSelectId[a].indexOf('SH')));
                let target_SH = parseInt(this.props.multiSelectId[a].substring(this.props.multiSelectId[a].indexOf('SH') + 2));
                if (store) {
                    for (let i = 0; i < store.aisles.length; i++) {
                        if (store.aisles[i].id === target_A) {
                            let thisShelf = store.aisles[i].sections[target_SE].shelves[target_SH];
                            let index = all_shelves_to_align.findIndex(shelf => shelf.id === thisShelf.id);
                            if (index < 0 && thisShelf.freezer != 1 && thisShelf.freezer != 2) all_shelves_to_align.push(thisShelf);
                            // now that we've added the shelf the item is actually on, we need to check if the item covers more than one shelf.
                            // since we know this is the shelf the item starts in we can get the item's details from it.
                            let item = thisShelf.items.filter(line => line.id === this.props.multiSelectId[a]);
                            let itemTotalWidth = (item[0].placement.distFromStart ? item[0].placement.distFromStart : 0) + ((item[0].placement.pWidth ? item[0].placement.pWidth : ProductDefaultDimensions.width) * item[0].placement.faces);
                            let shelfWidth = thisShelf.dimensions.width;
                            if (itemTotalWidth > shelfWidth) {
                                while (itemTotalWidth > shelfWidth) {
                                    target_SE++;
                                    let nextShelf = store.aisles[i].sections[target_SE].shelves[target_SH];
                                    let index = all_shelves_to_align.findIndex(shelf => shelf.id === nextShelf.id);
                                    if (index < 0 && nextShelf.freezer != 1 && nextShelf.freezer != 2) all_shelves_to_align.push(nextShelf);
                                    itemTotalWidth -= shelfWidth;
                                    shelfWidth = nextShelf.dimensions.width;
                                }
                            }
                        }
                    }
                }
            }
        } else if (shelf.freezer != 1 && shelf.freezer != 2) all_shelves_to_align.push(shelf);

        // now that we have all shelvs that need to be aligned we can align them one after the other
        for (let aa = 0; aa < all_shelves_to_align.length; aa++) {
            // find latest version of this shelf in the store aisle
            let thisShelf = all_shelves_to_align[aa];
            let target_A = all_shelves_to_align[aa].id.substring(0, all_shelves_to_align[aa].id.indexOf('SE'));
            let target_SE = parseInt(all_shelves_to_align[aa].id.substring(all_shelves_to_align[aa].id.indexOf('SE') + 2, all_shelves_to_align[aa].id.indexOf('SH')));
            let target_SH = parseInt(all_shelves_to_align[aa].id.substring(all_shelves_to_align[aa].id.indexOf('SH') + 2));
            if (store) {
                for (let i = 0; i < store.aisles.length; i++) {
                    if (store.aisles[i].id === target_A) {
                        thisShelf = store.aisles[i].sections[target_SE].shelves[target_SH];
                    }
                }
            }

            // get this shelf's combined shelves and only leave in the array shelves that exist in all_shelves_to_align
            let shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[all_shelves_to_align[aa].id]));
            while (shelfDetails.main_shelf) {
                let ids = JSON.parse(JSON.stringify(shelfDetails.main_shelf));
                shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[ids]));
                thisShelf = aisleShelves[ids];
            }
            let { combined } = shelfDetails;

            let combinedShelves = [thisShelf].concat(combined.map((id: any) => aisleShelves[id]));
            // remove from combinedShelves any shelves that do not appear in all_shelves_to_align
            let newCombinedShelves = [];
            for (let i = 0; i < combinedShelves.length; i++) {
                let index = all_shelves_to_align.findIndex(shelf => shelf.id === combinedShelves[i].id);
                if (index >= 0) newCombinedShelves.push(combinedShelves[i]);
            }
            combinedShelves = newCombinedShelves;

            let A_Id = combinedShelves[0].id.substring(0, combinedShelves[0].id.indexOf('SE'));
            let SE_Index = parseInt(combinedShelves[0].id.substring(combinedShelves[0].id.indexOf('SE') + 2, combinedShelves[0].id.indexOf('SH')));
            let SH_Index = parseInt(combinedShelves[0].id.substring(combinedShelves[0].id.indexOf('SH') + 2));

            // check if this aisle has a shelf in the same height, in previous section
            let firstDistFromStart = 0;
            if (store) {
                let store_aisle: PlanogramAisle = store.aisles.filter(aisle => aisle.id === A_Id)[0];
                if (store_aisle && store_aisle.sections[SE_Index - 1] && store_aisle.sections[SE_Index - 1].shelves[SH_Index]
                    &&
                    ((store_aisle.sections[SE_Index - 1].shelves[SH_Index].dimensions.height === store_aisle.sections[SE_Index].shelves[SH_Index].dimensions.height)
                        || SH_Index === 0
                    )) {                    // get all items in the previous shelf
                    let prev_shelf_items = JSON.parse(JSON.stringify(store_aisle.sections[SE_Index - 1].shelves[SH_Index].items));
                    // sort the items by distFromStart in case they are not already sorted
                    prev_shelf_items.sort(function (a: any, b: any) {
                        let aNumber = a.placement.distFromStart ? a.placement.distFromStart : 0;
                        let bNumber = b.placement.distFromStart ? b.placement.distFromStart : 0;
                        let aId = a.Id;
                        let bId = b.Id;
                        return (aNumber > bNumber) ? 1 : (aNumber < bNumber) ? -1 : ((aId > bId) ? 1 : (aId < bId) ? -1 : 0);
                    });

                    // if so check if the last item (based on distFromStart) on the previous shelf intrudes into this shelf
                    let lastItem = prev_shelf_items[prev_shelf_items.length - 1];
                    if (lastItem) {
                        let endPoint = (lastItem.placement.distFromStart ? lastItem.placement.distFromStart : 0) + ((lastItem.placement.pWidth != undefined ? lastItem.placement.pWidth : ProductDefaultDimensions.width) * lastItem.placement.faces);
                        let leftInShelf = store_aisle.sections[SE_Index].shelves[SH_Index].dimensions.width - endPoint;
                        if (leftInShelf < 0) firstDistFromStart = (-1 * leftInShelf);
                    }
                }
            }

            // save initial state of shelvs with items ordered by distFromStart (distance from start of shelf)
            for (let i = 0; i < combinedShelves.length; i++) {
                // sort shelf items by distFromStart
                combinedShelves[i].items.sort(function (a: any, b: any) {
                    let aNumber = a.placement.distFromStart ? a.placement.distFromStart : 0;
                    let bNumber = b.placement.distFromStart ? b.placement.distFromStart : 0;
                    let aId = a.Id;
                    let bId = b.Id;
                    return (aNumber > bNumber) ? 1 : (aNumber < bNumber) ? -1 : ((aId > bId) ? 1 : (aId < bId) ? -1 : 0);
                });
            }
            let viewCombinedShelves = JSON.parse(JSON.stringify(combinedShelves));
            // empty all items from current shelvs
            for (let i = 0; i < combinedShelves.length; i++) {
                combinedShelves[i].items = [];
            }

            let distFromStart = firstDistFromStart;
            let distInShelf = firstDistFromStart;
            let index = 0;
            let startOfShelfId = '';
            let spaceInShelf = 0;
            // go over the initial state and populate each shelf in combinedShelves with the correct items in the correct order
            let shelfId = 0;
            for (let i = 0; i < viewCombinedShelves.length; i++) {
                if (i === 0) {
                    startOfShelfId = viewCombinedShelves[i].id;
                    spaceInShelf = viewCombinedShelves[shelfId].dimensions.width;
                }
                for (let n = 0; n < viewCombinedShelves[i].items.length; n++) {
                    let item = JSON.parse(JSON.stringify(viewCombinedShelves[i].items[n]));
                    /* Barak 26.11.20 - return items above main line (item index >= 100) back into the shelf without altering anything else */
                    let itemInd = parseInt(item.id.substring(item.id.indexOf('I') + 1));
                    if (itemInd >= 100) {
                        if (item.shelfLevel === 1 || item.shelfLevel === undefined) combinedShelves[shelfId].items.push(item); // for items in freezer === 2
                        /* Barak 21.12.20 - verify items with shelfLevel 2 position */
                        else { // for items with shelfLevel > 1
                            // we need to return the item to it's original shelf
                            let destinationSec = parseInt(item.id.substring(item.id.indexOf('SE') + 2, item.id.indexOf('SH')));
                            let destinationSh = parseInt(item.id.substring(item.id.indexOf('SH') + 2, item.id.indexOf('I')));
                            let aisle: PlanogramAisle = JSON.parse(JSON.stringify(blankPlanogramAisle));
                            let aisles = this.props.store ? this.props.store.aisles : undefined;
                            let aisleId = shelf.id.substring(0, shelf.id.indexOf('SE'));
                            if (aisles) {
                                for (let i = 0; i < aisles.length; i++) {
                                    if (aisles[i].id === aisleId) {
                                        aisle = aisles[i];
                                        break;
                                    }
                                }
                            }
                            aisle.sections[destinationSec].shelves[destinationSh].items.push(item);
                        }
                        /*************************************************************/
                    }
                    else {
                        /************************************************************************************************************************/
                        if (spaceInShelf <= 0) {
                            shelfId++; // the shelf id in the viewCombinedShelves array
                            if (viewCombinedShelves[shelfId]) {
                                // there is a next shelf so we update the shelfNumber, startOfShelfId, spaceInShelf and index
                                // let shelfNumber = parseInt(JSON.parse(JSON.stringify(startOfShelfId.substring(startOfShelfId.indexOf('SE') + 2, startOfShelfId.indexOf('SH'))))) + 1;
                                let old_sec_num = parseInt(JSON.parse(JSON.stringify(startOfShelfId.substring(startOfShelfId.indexOf('SE') + 2, startOfShelfId.indexOf('SH')))));
                                let new_sec_num = parseInt(JSON.parse(JSON.stringify(viewCombinedShelves[shelfId].id.substring(viewCombinedShelves[shelfId].id.indexOf('SE') + 2, viewCombinedShelves[shelfId].id.indexOf('SH')))))
                                startOfShelfId = JSON.parse(JSON.stringify(startOfShelfId.substring(0, startOfShelfId.indexOf('SE') + 2) + new_sec_num + startOfShelfId.substring(startOfShelfId.indexOf('SH'))));
                                if (new_sec_num - old_sec_num === 1) {
                                    distInShelf = (spaceInShelf * -1);
                                    distFromStart = (spaceInShelf * -1);
                                    spaceInShelf = viewCombinedShelves[shelfId].dimensions.width - distInShelf;
                                } else {
                                    distInShelf = 0;
                                    distFromStart = 0;
                                    spaceInShelf = 0;
                                }
                                index = 0;
                            } else
                                // this shelf dosn't exist in the arry so we stay in this shelf
                                shelfId--;
                        }
                        item.id = startOfShelfId + 'I' + index;
                        index++;
                        item.placement.distFromStart = distFromStart;
                        let itemWidth = viewCombinedShelves[i].items[n].placement.pWidth;
                        distFromStart += (itemWidth != undefined ? itemWidth : ProductDefaultDimensions.width) * item.placement.faces;
                        distInShelf += (itemWidth != undefined ? itemWidth : ProductDefaultDimensions.width) * item.placement.faces;
                        if (viewCombinedShelves[shelfId]) {
                            spaceInShelf = viewCombinedShelves[shelfId].dimensions.width - distInShelf;
                            combinedShelves[shelfId].items.push(item);
                        }
                    }
                }
            }

            /* Barak 22.11.20 - Add saving array for aisle, allowing to do 
                Ctrl+Z to move back and Ctrl+Y to move forward *
                await this.saveSnapShot(shelf);
            *************************************************************/
            /* Barak 22.11.20 */ await this.saveAisleShelf(shelf);
        }
    }

    addShelfItem = async (shelf: PlanogramShelf, newItem: PlanogramItem, shlfStart?: number, newItems?: PlanogramItem[]) => {
        console.log('addShelfItem section', shelf, newItem);
        // make sure shelf has no undefined items
        let initItems = JSON.parse(JSON.stringify(shelf.items));
        shelf.items = [];
        for (let i = 0; i < initItems.length; i++) {
            if (initItems[i] && typeof initItems[i] != 'undefined')
                shelf.items.push(initItems[i]);
        }
        if (newItems) {
            initItems = JSON.parse(JSON.stringify(newItems));
            newItems = [];
            for (let i = 0; i < initItems.length; i++) {
                if (initItems[i] && typeof initItems[i] != 'undefined') {
                    initItems[i].placement.distFromStart = i;
                    newItems.push(initItems[i]);
                }
            }
        }
        const { virtualStore } = this.props;
        const shelvesDetails = virtualStore.shelfDetails;
        const aisleShelves = virtualStore.shelfMap;

        // update shelf.distFromStart if shlfStart was sent
        if (shlfStart && shlfStart >= 0) {
            shelf.distFromStart = shlfStart;
            /***************************/
            let aisles_1 = this.props.store ? this.props.store.aisles : undefined;
            let aisleId_1 = shelf.id.substring(0, shelf.id.indexOf('SE'));
            let aisle_1: PlanogramAisle = JSON.parse(JSON.stringify(blankPlanogramAisle));
            if (aisles_1) {
                for (let i = 0; i < aisles_1.length; i++) {
                    if (aisles_1[i].id === aisleId_1) {
                        aisle_1 = aisles_1[i];
                        let seI = parseInt(shelf.id.substring(shelf.id.indexOf('SE') + 2, shelf.id.indexOf('SH')));
                        let shI = parseInt(shelf.id.substring(shelf.id.indexOf('SH') + 2));
                        if (aisle_1.sections[seI] && aisle_1.sections[seI].shelves[shI])
                            aisle_1.sections[seI].shelves[shI] = shelf;
                        break;
                    }
                }
            }
            if (aisle_1.id === aisleId_1) {
                await this.props.setStoreAisle(aisle_1);
                console.log('shelf after saving dist from start', JSON.parse(JSON.stringify(shelf)), 'aisle after saving new shelf distFromStart', aisle_1);
            }
            /***************************/
        }
        // make sure, in case no shlfStart was sent that the shelf.distFromStart fits with the first Item (ordered by distFromStart) on that shelf
        else {
            if (shelf.distFromStart > 0) {
                let num: number = shelf.items[0] && shelf.items[0].placement.distFromStart ? shelf.items[0].placement.distFromStart : 0;
                if (shelf.items.length >= 1 && num >= 0) shelf.distFromStart = num;
            }
        }

        // check if this shelf even has room to add a new item;
        if (shelf.distFromStart === shelf.dimensions.width && newItem && newItem.product > 0 && shelf.freezer != 1 && shelf.freezer != 2 && (newItem.shelfLevel === 1 || newItem.shelfLevel === undefined)) {
            // if there is a next shelf activate function with the rest of the items 
            let fullShelf = JSON.parse(JSON.stringify(shelf));
            // this is the data for the actual shelf
            let shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[shelf.id]));

            // get the full combined shelves
            while (shelfDetails.main_shelf) {
                let ids = JSON.parse(JSON.stringify(shelfDetails.main_shelf));
                shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[ids]));
                fullShelf = aisleShelves[ids];
            }
            let { combined } = shelfDetails;
            if (combined == null) combined = [];
            let combinedShelves = [fullShelf].concat(combined.map((id: any) => aisleShelves[id]));
            // find the index of the current shelf 
            let currI = combinedShelves.findIndex(object => object.id === shelf.id);
            if (currI >= 0 && combinedShelves[currI + 1]) {
                // move item/s to the next shelf
                console.log('call addShelfItem for shelf', JSON.parse(JSON.stringify(combinedShelves[currI + 1])), 'add new item', newItem);
                await this.addShelfItem(JSON.parse(JSON.stringify(combinedShelves[currI + 1])), newItem);
            }
            return;
        }
        // if there is a new item add it at the end of the shelf
        if (newItem && newItem.product > 0) {
            /* Barak 26.11.20 - add special numbering for items in shelf.freezer === 2 */
            if (shelf.freezer === 2
                /* && newItem.placement.distFromTop != shelf.dimensions.height - ((newItem.placement.pHeight ? newItem.placement.pHeight : ProductDefaultDimensions.height) * newItem.placement.stack) */
            ) {
                if (newItem.id === '') {
                    console.log('newItem.id === ""');
                    if (shelf.items.length > 1) {
                        let lastI = parseInt(shelf.items[shelf.items.length - 1].id.substring(shelf.items[shelf.items.length - 1].id.indexOf('I') + 1));
                        let newI = 0;
                        if (lastI < 100) newI = lastI + 100;
                        else newI = lastI + 1;
                        newItem.id = shelf.id + "I" + newI;
                        console.log('1. newItem.id = ', newItem.id);
                    } else {
                        newItem.id = shelf.id + "I" + 100;
                        console.log('2. newItem.id = ', newItem.id);
                    }
                }
                console.log('addShelfItem freezer === 2 newItem', newItem);
            }
            /***************************************************************************/
            /* Barak 21.12.20 - add special numbering for items in shelfLevel > 1 */
            if (newItem.shelfLevel > 1) {
                if (newItem.id === '') {
                    console.log('newItem.id === ""');
                    if (shelf.items.length > 1) {
                        let lastI = parseInt(shelf.items[shelf.items.length - 1].id.substring(shelf.items[shelf.items.length - 1].id.indexOf('I') + 1));
                        let newI = 0;
                        if (lastI < 100) newI = lastI + 100;
                        else newI = lastI + 1;
                        newItem.id = shelf.id + "I" + newI;
                        console.log('1. newItem.id = ', newItem.id);
                    } else {
                        newItem.id = shelf.id + "I" + 100;
                        console.log('2. newItem.id = ', newItem.id);
                    }
                }
                // now that we have an id to the new item we can save it in the linked bottom level item as linkedItemUp
                let sItem = shelf.items.filter(line => line.id === this.props.multiSelectId[0]);
                if (sItem && sItem[0]) sItem[0].linkedItemUp = newItem.id;
            }
            /************************************************************************/
            shelf.items.push(newItem);
        }
        // if there are multiple items to add to the begining of the shelf add them
        if (newItems && newItems.length > 0) {
            // push all items on the shelf by 1 to allow to push new items at the begining of the shelf
            for (let i = 0; i < shelf.items.length; i++) {
                let itemDist = 0;
                if (shelf.items[i].placement.distFromStart != undefined)
                    itemDist = parseInt(JSON.stringify(shelf.items[i].placement.distFromStart));
                // if (shlfStart) itemDist += shlfStart;
                // shelf.items[i].placement.distFromStart = itemDist + newItems.length;
                shelf.items[i].placement.distFromStart = itemDist + 1;
            }
            console.log('1. newItems', JSON.parse(JSON.stringify(shelf)));
            // add the new items at the begining of the shelf items array
            for (let i = newItems.length - 1; i >= 0; i--) {
                if (newItems[i]) shelf.items.unshift(newItems[i]);
            }
            console.log('2. newItems', JSON.parse(JSON.stringify(shelf)));
            // go over all the items on the shelf and re-do their distance from start of shelf
            for (let i = 0; i < shelf.items.length; i++) {
                if (shelf.items[i]) {
                    let distToNextItem = 0;
                    let itemEndPoint = parseInt(JSON.stringify(shelf.items[i].placement.distFromStart)) + (shelf.items[i].placement.faces * parseInt(JSON.stringify(shelf.items[i].placement.pWidth)));
                    if (shelf.items[i + 1] && (i + 1 <= newItems.length || i > newItems.length)) distToNextItem = parseInt(JSON.stringify(shelf.items[i + 1].placement.distFromStart)) - itemEndPoint;
                    if (i === 0 && shlfStart != undefined) {
                        shelf.items[0].placement.distFromStart = shlfStart;
                        itemEndPoint += shlfStart;
                    }
                    if (shelf.items[i + 1]) shelf.items[i + 1].placement.distFromStart = itemEndPoint + (distToNextItem > 0 ? distToNextItem : 0);
                }
            }
            console.log('3. newItems', JSON.parse(JSON.stringify(shelf)));
        }
        console.log('original shelf after adding new items', JSON.parse(JSON.stringify(shelf)));

        /* Barak 26.11.20 - item distFromTop on a freezer shelf */
        if (shelf.freezer === 1 || shelf.freezer === 2) {
            // /* Barak 22.11.20 */ await this.saveSnapShot(shelf);
            /* Barak 22.11.20 */await this.saveAisleShelf(shelf);
            return;
        }
        console.log('shelf.freezer === 1 || shelf.freezer === 2 after return');
        /********************************************************/

        // save initial state of shelf with items ordered by distFromStart (distance from start of shelf)
        shelf.items.sort(function (a: any, b: any) {
            let aNumber = a.placement.distFromStart ? a.placement.distFromStart : 0;
            let bNumber = b.placement.distFromStart ? b.placement.distFromStart : 0;
            let aId = a.Id;
            let bId = b.Id;
            return (aNumber > bNumber) ? 1 : (aNumber < bNumber) ? -1 : ((aId > bId) ? 1 : (aId < bId) ? -1 : 0);
        });
        console.log('original shelf after sorting by distFromStart', JSON.parse(JSON.stringify(shelf)));

        // go over shelf and if there are 2 items occupying the same space and they are the same product 
        // - increase faces to the first and remove the second
        for (let i = 0; i < shelf.items.length; i++) {
            if (shelf.items[i]) {
                if (parseInt(JSON.stringify(shelf.items[i].placement.distFromStart)) < shelf.distFromStart)
                    shelf.items[i].placement.distFromStart = shelf.distFromStart;
                if (shelf.items[i + 1] && (shelf.items[i + 1].shelfLevel === 1 || shelf.items[i + 1].shelfLevel === undefined)) {
                    console.log('check item a', shelf.items[i], 'vs item a+1', shelf.items[i + 1]);
                    let first_dist: number = parseInt(JSON.stringify(shelf.items[i].placement.distFromStart));
                    let first_end = first_dist + (parseInt(JSON.stringify(shelf.items[i].placement.pWidth)) * shelf.items[i].placement.faces);
                    let second_dist: number = parseInt(JSON.stringify(shelf.items[i + 1].placement.distFromStart));
                    let second_end = second_dist + (parseInt(JSON.stringify(shelf.items[i + 1].placement.pWidth)) * shelf.items[i + 1].placement.faces);
                    if (((second_dist <= first_dist && second_end <= first_end) || (second_dist >= first_dist && second_end <= first_end) || (second_dist >= first_dist && second_end > first_end))
                        && shelf.items[i].product === shelf.items[i + 1].product) {
                        // same item - increase faces
                        shelf.items[i].placement.faces += shelf.items[i + 1].placement.faces;
                        // remove second item 
                        shelf.items.splice(i + 1, 1);
                    }
                }
            }
        }
        console.log('original shelf after consolidating same items', JSON.parse(JSON.stringify(shelf)));

        let viewShelf = JSON.parse(JSON.stringify(shelf));
        // empty all items from current shelf
        shelf.items = [];
        console.log('viewShelf initial', viewShelf, 'shelf without items', JSON.parse(JSON.stringify(shelf)));

        let distFromStart = viewShelf.items.length > 0 && viewShelf.items[0] ? viewShelf.items[0].placement.distFromStart : 0;
        let distInShelf = viewShelf.items.length > 0 && viewShelf.items[0] ? viewShelf.items[0].placement.distFromStart : 0;
        let index = 0;
        let startOfShelfId = viewShelf.id;
        let spaceInShelf = 0;
        // go over the initial state and populate each shelf in combinedShelves with the correct items in the correct order
        for (let n = 0; n < viewShelf.items.length; n++) {
            let item = JSON.parse(JSON.stringify(viewShelf.items[n]));
            /* Barak 26.11.20 - return items above main line (item index >= 100) back into the shelf without altering anything else */
            let itemInd = parseInt(item.id.substring(item.id.indexOf('I') + 1));
            if (itemInd >= 100) shelf.items.push(item);
            else {
                /************************************************************************************************************************/
                let nextItem = null;
                if (viewShelf.items[n + 1]) nextItem = viewShelf.items[n + 1];
                // console.log('item', item, 'itemExists', itemExists, 'nextItem', nextItem);
                if (spaceInShelf < 0) {
                    // if there is a next shelf activate function with the rest of the items 
                    let fullShelf = JSON.parse(JSON.stringify(shelf));
                    // this is the data for the actual shelf
                    let shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[shelf.id]));

                    // get the full combined shelves
                    while (shelfDetails.main_shelf) {
                        let ids = JSON.parse(JSON.stringify(shelfDetails.main_shelf));
                        shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[ids]));
                        fullShelf = aisleShelves[ids];
                    }
                    let { combined } = shelfDetails;
                    if (combined == null) combined = [];
                    let combinedShelves = [fullShelf].concat(combined.map((id: any) => aisleShelves[id]));
                    // find the index of the current shelf 
                    let currI = combinedShelves.findIndex(object => object.id === viewShelf.id);
                    if (currI >= 0 && combinedShelves[currI + 1]) {
                        while (combinedShelves[currI + 1] && spaceInShelf * -1 > combinedShelves[currI + 1].dimensions.width) {
                            spaceInShelf += combinedShelves[currI + 1].dimensions.width;
                            console.log('call addShelfItem for shelf', JSON.parse(JSON.stringify(combinedShelves[currI + 1])), 'changing start to', combinedShelves[currI + 1].dimensions.width);
                            await this.addShelfItem(JSON.parse(JSON.stringify(combinedShelves[currI + 1])), blankItem, combinedShelves[currI + 1].dimensions.width, []);
                            currI++;
                        }
                        let newItemsToSend: PlanogramItem[] = [];
                        if (n < viewShelf.items.length) {
                            // get all items left over into a new array
                            for (let i = n; i < viewShelf.items.length; i++) {
                                newItemsToSend.push(viewShelf.items[i]);
                            }
                            // // remove the items we're passing to the next shelf from the array of this shelf
                            // viewShelf.items.splice(n, viewShelf.items.length - n);
                        }
                        console.log('call addShelfItem for shelf', JSON.parse(JSON.stringify(combinedShelves[currI + 1])), 'changing start to', spaceInShelf * -1, 'new items', newItemsToSend);
                        await this.addShelfItem(JSON.parse(JSON.stringify(combinedShelves[currI + 1])), blankItem, (spaceInShelf * -1), newItemsToSend);
                        console.log('after call addShelfItem');
                        spaceInShelf = 0;
                        // leave the loop since there are no more items in this shelf to take care of  
                        //  - all the left over items are moved to the next shelf
                        break;
                    }
                }
                if (item.placement.distFromStart > distFromStart && (startOfShelfId === item.id.substring(0, item.id.indexOf('I')) || item.id === ''))
                    distFromStart = item.placement.distFromStart;
                item.id = startOfShelfId + 'I' + index;
                index++;
                item.placement.distFromStart = distFromStart;
                let itemWidth = viewShelf.items[n].placement.pWidth;
                let itemSpace = (itemWidth != undefined ? itemWidth : ProductDefaultDimensions.width) * item.placement.faces;
                distFromStart += itemSpace;
                distInShelf += itemSpace;
                spaceInShelf = viewShelf.dimensions.width - distInShelf;
                // add item back into the shelf
                shelf.items.push(item);


                // check if there is a next shelf 
                let fullShelf = JSON.parse(JSON.stringify(shelf));
                // this is the data for the actual shelf
                let shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[shelf.id]));
                // get the full combined shelves
                while (shelfDetails.main_shelf) {
                    let ids = JSON.parse(JSON.stringify(shelfDetails.main_shelf));
                    shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[ids]));
                    fullShelf = aisleShelves[ids];
                }
                let { combined } = shelfDetails;
                if (combined == null) combined = [];
                let combinedShelves = [fullShelf].concat(combined.map((id: any) => aisleShelves[id]));
                // find the index of the current shelf 
                let currI = combinedShelves.findIndex(object => object.id === viewShelf.id);


                if (distFromStart > viewShelf.dimensions.width && currI >= 0 && combinedShelves[currI + 1]) {
                    // only of there is a next shelf can we reduce the viewShelf.dimensions.width from the next distFromStart 
                    spaceInShelf = (distFromStart - viewShelf.dimensions.width) * -1;
                    distFromStart = distFromStart - viewShelf.dimensions.width;
                }
                console.log('--- item', item, 'distInShelf', distInShelf, 'distFromStart next', distFromStart, 'spaceInShelf', spaceInShelf, "shelf", JSON.parse(JSON.stringify(shelf)));
            }
        }
        // in case the last item on the shelf is the one that extends into the next shelf, 
        // after we're done going over all items we check if the spaceInShelf has extended byond the current shelf
        if (spaceInShelf < 0) {
            console.log('last item on shelf extends beyond this shelf. spaceInShelf', spaceInShelf);
            // if there is a next shelf activate function to update the next shelf's distFromStart 
            let fullShelf = JSON.parse(JSON.stringify(shelf));
            // this is the data for the actual shelf
            let shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[shelf.id]));

            // get the full combined shelves
            while (shelfDetails.main_shelf) {
                let ids = JSON.parse(JSON.stringify(shelfDetails.main_shelf));
                shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[ids]));
                fullShelf = aisleShelves[ids];
            }
            let { combined } = shelfDetails;
            if (combined == null) combined = [];
            let combinedShelves = [fullShelf].concat(combined.map((id: any) => aisleShelves[id]));
            // find the index of the current shelf 
            let currI = combinedShelves.findIndex(object => object.id === viewShelf.id);
            if (currI >= 0 && combinedShelves[currI + 1]) {
                while (combinedShelves[currI + 1] && spaceInShelf * -1 > combinedShelves[currI + 1].dimensions.width) {
                    spaceInShelf += combinedShelves[currI + 1].dimensions.width;
                    console.log('call addShelfItem for shelf', JSON.parse(JSON.stringify(combinedShelves[currI + 1])), 'changing start to', combinedShelves[currI + 1].dimensions.width);
                    await this.addShelfItem(JSON.parse(JSON.stringify(combinedShelves[currI + 1])), blankItem, combinedShelves[currI + 1].dimensions.width, []);
                    currI++;
                }
                let newItemsToSend: PlanogramItem[] = [];
                console.log('call addShelfItem for shelf', JSON.parse(JSON.stringify(combinedShelves[currI + 1])), 'changing start to', spaceInShelf * -1, 'new items', newItemsToSend);
                await this.addShelfItem(JSON.parse(JSON.stringify(combinedShelves[currI + 1])), blankItem, (spaceInShelf * -1), newItemsToSend);
                console.log('after call addShelfItem');
            }
        }

        /* Barak 21.12.20 - verify items with shelfLevel 2 position */
        for (let b = 0; b < shelf.items.length; b++) {
            if (shelf.items[b].linkedItemUp) {
                let upItem = shelf.items.filter(line => line.id === shelf.items[b].linkedItemUp);
                if (upItem && upItem[0]) {
                    upItem[0].placement.distFromStart = shelf.items[b].placement.distFromStart;
                    upItem[0].linkedItemDown = shelf.items[b].id;
                    upItem[0].id = shelf.items[b].id.substring(0, shelf.items[b].id.indexOf('I')) + 'I' + upItem[0].id.substring(upItem[0].id.indexOf('I') + 1);
                    shelf.items[b].linkedItemUp = upItem[0].id;
                }
                console.log('linkedItemUp', shelf.items[b], upItem[0], shelf);
            }
        }
        /************************************************************/

        /* Barak 22.11.20 - Add saving array for aisle, allowing to do 
            Ctrl+Z to move back and Ctrl+Y to move forward *
            await this.saveSnapShot(shelf);
        *************************************************************/
       /* Barak 22.11.20 */ await this.saveAisleShelf(shelf);
    }
    saveSnapShot = async (shelf: PlanogramShelf) => {
        const { store, aisleShelves, shelvesDetails } = this.props;
        /* Barak 22.11.20 - Add saving array for aisle, allowing to do 
            Ctrl+Z to move back and Ctrl+Y to move forward */
        // check if the array is empty or for a different aisle
        let tArray = this.props.aisleSaveArray;
        let tIndex = this.props.aisleSaveIndex;
        console.log('tArray intial check', tArray, tIndex);
        if (tArray.length === 0 || (tArray.length > 0 && tArray[0].id != shelf.id.substring(0, shelf.id.indexOf('SE')))) {
            let newAisle = await planogramProvider.getStoreAisle(this.props.store ? this.props.store.store_id : 0, this.props.aisleId != undefined ? this.props.aisleId : 0);
            console.log('in Section, newAisle', newAisle);
            tArray = [JSON.parse(JSON.stringify(newAisle))];
            tIndex = 0;
            console.log('in Section, initialize save aisle array', tArray, 0);
            await this.props.setAisleSaveArray(tArray);
            await this.props.setAisleSaveIndex(tIndex);
        }
        let aisle: PlanogramAisle = JSON.parse(JSON.stringify(blankPlanogramAisle));
        let aisles = this.props.store ? this.props.store.aisles : undefined;
        let aisleId = shelf.id.substring(0, shelf.id.indexOf('SE'));
        if (aisles) {
            for (let i = 0; i < aisles.length; i++) {
                if (aisles[i].id === aisleId) {
                    aisle = aisles[i];
                    break;
                }
            }
        }
        if (aisle.id === aisleId) {
            /* Barak 22.11.20 - Add saving array for aisle, allowing to do 
                Ctrl+Z to move back and Ctrl+Y to move forward */
            // before we save the new aisle we'll also add it to the aisleSaveArray
            // let tArray = this.props.aisleSaveArray;
            tIndex++;
            // insert into array tArray item aisle at location tIndex, deleting 0 items first
            tArray.splice(tIndex, 0, JSON.parse(JSON.stringify(aisle)));
            if (tArray.length > tIndex + 1) {
                tArray.length = tIndex + 1;
            }
            // tArray.push(JSON.parse(JSON.stringify(aisle)));
            console.log('tArray in shelfItem', tArray, 'current index', tIndex);
            await this.props.setAisleSaveArray(tArray);
            await this.props.setAisleSaveIndex(tIndex);
            let sectionInd = parseInt(shelf.id.substring(shelf.id.indexOf('SE') + 2, shelf.id.indexOf('SH')));
            let shelfInd = parseInt(shelf.id.substring(shelf.id.indexOf('SH') + 2));
            aisle.sections[sectionInd].shelves[shelfInd] = shelf;
            console.log('before setStoreAisle', aisle);
            /*************************************************************/
            await this.props.setStoreAisle(aisle);
        }
    }
    saveAisleShelf = async (shelf: PlanogramShelf) => {
        let aisle: PlanogramAisle = JSON.parse(JSON.stringify(blankPlanogramAisle));
        let aisles = this.props.store ? this.props.store.aisles : undefined;
        let aisleId = shelf.id.substring(0, shelf.id.indexOf('SE'));
        if (aisles) {
            for (let i = 0; i < aisles.length; i++) {
                if (aisles[i].id === aisleId) {
                    aisle = aisles[i];
                    let seI = parseInt(shelf.id.substring(shelf.id.indexOf('SE') + 2, shelf.id.indexOf('SH')));
                    let shI = parseInt(shelf.id.substring(shelf.id.indexOf('SH') + 2));
                    if (aisle.sections[seI] && aisle.sections[seI].shelves[shI]) {
                        aisle.sections[seI].shelves[shI] = shelf;
                    }
                    break;
                }
            }
        }
        if (aisle.id === aisleId) {
            await this.props.setStoreAisle(aisle);
            /* Barak 21.12.20 */ await this.checkLevel2(aisle);
        }
    }


    /* Barak 21.12.20 - verify items with shelfLevel 2 position */
    checkLevel2 = async (aisle: PlanogramAisle) => {
        let combinedItems: any[] = [];
        for (let a = 0; a < aisle.sections.length; a++) {
            for (let b = 0; b < aisle.sections[a].shelves.length; b++) {
                combinedItems = combinedItems.concat(aisle.sections[a].shelves[b].items);
            }
        }
        for (let c = 0; c < combinedItems.length; c++) {
            if (combinedItems[c].linkedItemUp) {
                let upItem = combinedItems.filter((line: any) => line.id === combinedItems[c].linkedItemUp);
                if (upItem && upItem[0]) {
                    let itemToMove = JSON.parse(JSON.stringify(upItem[0]));
                    // find location of item to move
                    let moveSec = parseInt(itemToMove.id.substring(itemToMove.id.indexOf('SE') + 2, itemToMove.id.indexOf('SH')));
                    let moveSh = parseInt(itemToMove.id.substring(itemToMove.id.indexOf('SH') + 2, itemToMove.id.indexOf('I')));
                    let index = aisle.sections[moveSec].shelves[moveSh].items.findIndex(line => line.id === itemToMove.id);
                    // remove this item from original shelf
                    aisle.sections[moveSec].shelves[moveSh].items.splice(index, 1);
                    // update itemToMove properties
                    itemToMove.placement.distFromStart = combinedItems[c].placement.distFromStart;
                    itemToMove.linkedItemDown = combinedItems[c].id;
                    // itemToMove.id = combinedItems[c].id.substring(0, combinedItems[c].id.indexOf('I')) + 'I' + itemToMove.id.substring(upItem[0].id.indexOf('I') + 1);
                    itemToMove.id = ''
                    // find new location of item and create new item id
                    let destinationSec = parseInt(combinedItems[c].id.substring(combinedItems[c].id.indexOf('SE') + 2, combinedItems[c].id.indexOf('SH')));
                    let destinationSh = parseInt(combinedItems[c].id.substring(combinedItems[c].id.indexOf('SH') + 2, combinedItems[c].id.indexOf('I')));
                    // console.log('checkLevel2 destinationSec',destinationSec,'destinationSh',destinationSh);
                    if (aisle.sections[destinationSec].shelves[destinationSh].items.length > 1) {
                        let lastI = parseInt(aisle.sections[destinationSec].shelves[destinationSh].items[aisle.sections[destinationSec].shelves[destinationSh].items.length - 1].id.substring(aisle.sections[destinationSec].shelves[destinationSh].items[aisle.sections[destinationSec].shelves[destinationSh].items.length - 1].id.indexOf('I') + 1));
                        let newI = 0;
                        if (lastI < 100) newI = lastI + 100;
                        else newI = lastI + 1;
                        itemToMove.id = aisle.sections[destinationSec].shelves[destinationSh].id + "I" + newI;
                        // console.log('checkLevel2 lastI',lastI,'newI',newI,'itemToMove.id',itemToMove.id);
                    } else {
                        itemToMove.id = aisle.sections[destinationSec].shelves[destinationSh].id + "I" + 100;
                    }
                    // add itemToMove to its new location
                    // console.log('checkLevel2 push',itemToMove);
                    aisle.sections[destinationSec].shelves[destinationSh].items.push(itemToMove);
                    // update the linkedItemUp with the new item id 
                    combinedItems[c].linkedItemUp = itemToMove.id;
                }
                else {
                    combinedItems[c].linkedItemUp = null;
                }
            }
        }
        await this.props.setStoreAisle(aisle);
    }
    /*************************************************************/

    render() {
        let { placement, product, id } = this.props.item;
        let { lowSales, missing } = this.props.item;
        const { shelf } = this.props;

        const {
            hideShelfItems,
            hideSaleTags,
            catalogProduct,
            hoverProduct,
            updateProductDimensions,
            editShelfItemPlacement,
            setProductDetailerDisplay,
            showRowItems,
            overlayObj,
            netw,
            userPhone,
        } = this.props;

        if (hideShelfItems) return null;
        const productDimensions = catalogProductDimensionObject(catalogProduct);
        const { height: productHeight, width: productWidth } = productDimensions;
        const { connectDragSource, connectDropTarget, isDragging, canDrop } = this.props;
        const {
            stack,
            faces,
            row,
            manual_row_only,
            position,
            distFromStart,
            distFromTop
        } = placement;
        let { pWidth, pHeight, pDepth } = placement;
        let ItemRender = rangeArray(stack <= ItemMaxPlacement.stack ? stack : ItemMaxPlacement.stack, shelf, this.props.item, this.props.printItemObj);
        let facesRender = rangeArray(faces <= ItemMaxPlacement.faces ? faces : ItemMaxPlacement.faces, shelf, this.props.item, this.props.printItemObj)
        let rowRender = rangeArray(row <= ItemMaxPlacement.row ? row : ItemMaxPlacement.row, shelf, this.props.item, this.props.printItemObj)
        // const stackArray = Array.apply(null, Array(stack || 1));
        // const facesArray = Array.apply(null, Array(faces || 1));
        // const rowArray = Array.apply(null, Array(row || 1));

        let productHasDimensions = catalogProduct ? (catalogProduct.width && catalogProduct.height && catalogProduct.length) : true;
        /* Barak 17.6.2020 - Add action to mark Archive products */
        let productIsArchive = '';
        /* Barak 24.6.2020 - Add color for lowSales */
        if (lowSales) productIsArchive = 'rgba(160, 140, 1, 0.5)'; // dirty-brown - דל מכר
        if (catalogProduct) {
            if (catalogProduct.Bdate_buy && catalogProduct.Bdate_buy != '') productIsArchive = 'rgba(255,153,51,0.5)'; // orange - חסום רכש
            if (catalogProduct.Bdate_sale && catalogProduct.Bdate_sale != '') productIsArchive = 'rgba(203, 188, 58, 0.5)'; // dirty-yellow - חסום מכירה
            if (catalogProduct.Archives === 1) productIsArchive = 'rgba(255,255,0,0.5)'; // yellow - ארכיון
        }
        // if selected item is identocal to this item
        if (this.props.currentSelectedId === id && !this.state.clicked) {
            let x = document.getElementById(id);
            // find the element on screen and simulate a mouse click on it
            if (x) x.click();
        }
        if ((this.props.currentSelectedId != id || this.props.currentSelectedId === '') && this.state.clicked) this.setState({ clicked: false });
        /**************************************************/

        //if (catalogProduct.BarCode === 7290100701577) console.log('7290100701577', catalogProduct, 'this.props.item.placment', pWidth, this.props.item.placement)
        let distFromStart_disp = this.props.item.placement.distFromStart ? this.props.item.placement.distFromStart : 0;
        // if (this.props.item.id === 'A446SE0SH1I0') distFromStart_disp += 500;
        /* Barak 26.11.20 */
        let distFromTop_disp = this.props.item.placement.distFromTop != null ? this.props.item.placement.distFromTop : null;
        if (distFromTop_disp === null && shelf.freezer === 1) distFromTop_disp = 0; // regular freezer - item will be on top
        if (distFromTop_disp === null && shelf.freezer === 2) // temp freezer - item will be on bottom
            distFromTop_disp = shelf.dimensions.height - ((this.props.item && this.props.item.placement && this.props.item.placement.pHeight ? this.props.item.placement.pHeight : ProductDefaultDimensions.height) * this.props.item.placement.stack);
        // if (shelf.freezer != 1 && shelf.freezer != 2) distFromTop_disp = null; // not a freezer so no support for distFromTop of any kind
        /******************/

        // if (this.props.item.product === 7290000113203 && shelf.id === 'A1023SE3SH0')
        //     console.log('7290000113203 item', this.props.item, 'distFromTop', this.props.item.placement.distFromTop, 'distFromTop_disp', distFromTop_disp, 'distFromStart_disp', distFromStart_disp, 'shelf.dimensions.height', shelf.dimensions.height, 'pHeight', pHeight,
        //         'bottom', distFromTop_disp != null ? shelf.dimensions.height - (distFromTop_disp + (pHeight ? pHeight : 0)) : '');


        let my_div = document.getElementById("sh_id_" + shelf.id);
        let box: any;
        try {
            if (my_div) box = my_div.getBoundingClientRect();
        } catch (e) { }
        // console.log('box',box);
        // let pixelDensity = (box ? box.width : 500) / (shelf && shelf.dimensions && shelf.dimensions.width ? shelf.dimensions.width : ShelfDefaultDimension.width); // how many mm to 1px

        // console.log('subGroupColor',subGroupColor,'segmentColor',segmentColor,'modelColor',modelColor);

        // if (this.props.item.product === 7290000113203) console.log('7290000113203 item', this.props.item);

        return connectDropTarget(connectDragSource(
            <div
                className={"planogram-shelf-item" + (canDrop ? " droppable" : "") + (hoverProduct === product ? " active" : "")}
                /* Barak 4.8.20 */ id={id}
                /* Barak 5.5.20 - change drag image */
                onDragStart={(e) => {
                    // console.log('onDragStart', e,'e.dataTransfer',e.dataTransfer);
                    // let img = document.createElement("img");
                    // img.src = "http://kryogenix.org/images/hackergotchi-simpler.png";
                    // e.dataTransfer.setDragImage(this.state.dragImg, 0, 0);
                    this.modifyDragImage(e.dataTransfer);
                }}
                style={{
                    left: this.props.printItem ? 0 : widthDensity(distFromStart_disp),
                    bottom: distFromTop_disp != null ? heightDensity(shelf.dimensions.height - (distFromTop_disp + ((pHeight ? pHeight : 0) * stack))) : '',
                    opacity: isDragging ? 0.5 : 1,
                }}
                onClick={(e) => {
                    if (!e.ctrlKey) {
                        e.stopPropagation();
                        setProductDetailerDisplay(product, lowSales, this.props.item.placement.faces, missing, netw, userPhone);
                        this.props.setMultiSelectId([id]);
                        this.props.setCurrentSelectedId(id);
                        this.setState({ clicked: true });
                    }
                }}
                onMouseOver={(e) => {
                    this.bindKeyboard();
                }}
                onMouseOut={(e) => {
                    this.unBindKeyboard();
                }}
                onDoubleClick={e => {
                    e.stopPropagation();
                    contextMenu.show({
                        id: "pde_" + id + "_" + catalogProduct.BarCode,
                        event: e
                    });
                }}>
                {hoverProduct === product ? <div className="shelf-item-cover" style={{ backgroundColor: "#3a404d", opacity: 0.5 }}></div> : null}
                {overlayObj ?
                    <div className="shelf-item-cover" style={{
                        backgroundColor: overlayObj.color,
                        opacity: 0.8
                    }}></div>
                    : null}


                <Menu
                    onHidden={() => this.setState({ dimensionMenu: false })}
                    onShown={() => this.setState({ dimensionMenu: true })}
                    id={catalogProduct ? "pde_" + id + "_" + catalogProduct.BarCode : "pde_" + id + "_"}
                    className="planogram-context-window" >
                    <DimensionModal
                        init={productDimensions}
                        title={catalogProduct ? "Product Dimensions " + catalogProduct.Name : "Product Dimensions "}
                        subtitle={catalogProduct ? "Barcode " + catalogProduct.BarCode : "Barcode "}
                        onSubmit={(dimensions) => {
                            planogramApi.updateProductDimensions(catalogProduct.BarCode, dimensions).then(async (res) => {
                                updateProductDimensions(
                                    catalogProduct.BarCode,
                                    dimensions);
                                /* Barak 20.1.21 - get new item dimentions based on update and position */
                                // get current aisle
                                let aisles_1 = this.props.store ? this.props.store.aisles : undefined;
                                let aisleId_1 = shelf.id.substring(0, shelf.id.indexOf('SE'));
                                let aisle_1: PlanogramAisle = JSON.parse(JSON.stringify(blankPlanogramAisle));
                                if (aisles_1) {
                                    for (let i = 0; i < aisles_1.length; i++) {
                                        if (aisles_1[i].id === aisleId_1) {
                                            aisle_1 = aisles_1[i];
                                            let seI = parseInt(shelf.id.substring(shelf.id.indexOf('SE') + 2, shelf.id.indexOf('SH')));
                                            let shI = parseInt(shelf.id.substring(shelf.id.indexOf('SH') + 2));
                                            if (aisle_1.sections[seI] && aisle_1.sections[seI].shelves[shI])
                                                aisle_1.sections[seI].shelves[shI] = shelf;
                                            break;
                                        }
                                    }
                                }
                                if (aisle_1.id === aisleId_1) {
                                    // in this aisle, go over all items and for each item that matches this barcode update dimensions based on position
                                    for (let a = 0; a < aisle_1.sections.length; a++) {
                                        for (let b = 0; b < aisle_1.sections[a].shelves.length; b++) {
                                            for (let c = 0; c < aisle_1.sections[a].shelves[b].items.length; c++) {
                                                let item = aisle_1.sections[a].shelves[b].items[c];
                                                if (item.product === catalogProduct.BarCode) {
                                                    let tWidth;
                                                    let tHeight;
                                                    let tDepth;
                                                    switch (item.placement.position) {
                                                        case 0: { tWidth = dimensions.width; tHeight = dimensions.height; tDepth = dimensions.depth; break; }
                                                        case 1: { tWidth = dimensions.height; tHeight = dimensions.width; tDepth = dimensions.depth; break; }
                                                        case 2: { tWidth = dimensions.width; tHeight = dimensions.depth; tDepth = dimensions.height; break; }
                                                        case 3: { tWidth = dimensions.height; tHeight = dimensions.depth; tDepth = dimensions.width; break; }
                                                        case 4: { tWidth = dimensions.depth; tHeight = dimensions.height; tDepth = dimensions.width; break; }
                                                        case 5: { tWidth = dimensions.depth; tHeight = dimensions.width; tDepth = dimensions.height; break; }
                                                        default: { tWidth = dimensions.width; tHeight = dimensions.height; tDepth = dimensions.depth; }
                                                    }
                                                    item.placement.pWidth = tWidth;
                                                    item.placement.pHeight = tHeight;
                                                    item.placement.pDepth = tDepth;
                                                }
                                            }
                                        }
                                    }
                                    await this.props.setStoreAisle(aisle_1);
                                    console.log('save aisle after change in item dimensions', JSON.parse(JSON.stringify(aisle_1)));
                                }
                                // make sure this item (the one pressed is also updated)
                                let tWidth;
                                let tHeight;
                                let tDepth;
                                switch (position) {
                                    case 0: { tWidth = dimensions.width; tHeight = dimensions.height; tDepth = dimensions.depth; break; }
                                    case 1: { tWidth = dimensions.height; tHeight = dimensions.width; tDepth = dimensions.depth; break; }
                                    case 2: { tWidth = dimensions.width; tHeight = dimensions.depth; tDepth = dimensions.height; break; }
                                    case 3: { tWidth = dimensions.height; tHeight = dimensions.depth; tDepth = dimensions.width; break; }
                                    case 4: { tWidth = dimensions.depth; tHeight = dimensions.height; tDepth = dimensions.width; break; }
                                    case 5: { tWidth = dimensions.depth; tHeight = dimensions.width; tDepth = dimensions.height; break; }
                                    default: { tWidth = dimensions.width; tHeight = dimensions.height; tDepth = dimensions.depth; }
                                }
                                pWidth = tWidth;
                                pHeight = tHeight;
                                pDepth = tDepth;
                                // if not manual_row_only we re-calculate the row
                                if (!manual_row_only) {
                                    editShelfItemPlacement(id, {
                                        faces,
                                        stack,
                                        row: Math.floor(shelf.dimensions.depth / dimensions.depth),
                                        manual_row_only,
                                        position,
                                        pWidth,
                                        pHeight,
                                        pDepth,
                                        distFromStart,
                                        distFromTop
                                    })
                                }
                                // if manual_row_only we bring row as is from shelf_item    
                                else {
                                    editShelfItemPlacement(id, {
                                        faces,
                                        stack,
                                        row,
                                        manual_row_only,
                                        position,
                                        pWidth,
                                        pHeight,
                                        pDepth,
                                        distFromStart,
                                        distFromTop
                                    })
                                }
                                await this.addShelfItem(this.props.aisleShelves[shelf.id], blankItem);
                                if (this.props.alignToLeft)
                                    await this.alignThisShelfToLeft(this.props.aisleShelves[shelf.id]);
                                this.saveSnapShot(this.props.aisleShelves[shelf.id]);
                                alert("Edited product successfully.");
                                contextMenu.hideAll();
                            }).catch((err) => {
                                console.error(err);
                                alert("Unable to edit product dimensions.");
                                contextMenu.hideAll();
                            });
                        }} />
                </Menu>
                {facesRender.map((s, i) => (
                    <div
                        style={{}}
                        className="shelf-item-stack"
                        key={id + "_" + i}>
                        {
                            ItemRender.map((v, _i) =>
                            (
                                <div
                                    className="shelf-item-product"
                                    style={{
                                        width: widthDensity(this.props.item.placement.pWidth && (this.props.item.placement.pWidth > 0) ? this.props.item.placement.pWidth : productWidth),
                                        height: heightDensity(this.props.item.placement.pHeight && (this.props.item.placement.pHeight > 0) ? this.props.item.placement.pHeight : productHeight),

                                    }}
                                    onClick={(e) => {
                                        if (e.ctrlKey) {
                                            let multi: any[] = this.props.multiSelectId;
                                            if (this.props.multiSelectId.findIndex(lineId => lineId === id) < 0)
                                                multi.push(id);
                                            this.props.setMultiSelectId(multi);
                                        }
                                    }}
                                    key={id + "_" + i + "_" + _i}>
                                    {/* Barak 21.4.20 - Add highlight on ctrl + clicked products (multi selection) */}
                                    {this.props.multiSelectId.findIndex(lineId => lineId === id) >= 0
                                        ? <div style={{ backgroundColor: "#3a404d", opacity: 0.5, position: 'absolute', top: '0', height: '100%', width: '100%', zIndex: 500 }}></div>
                                        : null}
                                    {/******************************************************************************/}
                                    {showRowItems ? rowRender.map((r, __i) => {
                                        return <img
                                            style={{
                                                zIndex: 300 - (__i),
                                                left: (__i * pixelSideStack) + "px",
                                                bottom: (__i * pixelTopStack) + "px",
                                                width: widthDensity(this.props.item.placement.pWidth && (this.props.item.placement.pWidth > 0) ? this.props.item.placement.pWidth : 0),
                                                height: heightDensity(this.props.item.placement.pHeight && (this.props.item.placement.pHeight > 0) ? this.props.item.placement.pHeight : 0),
                                            }}
                                            alt=""
                                            onError={e => {
                                                e.currentTarget.src = noImage;
                                            }}
                                            src={catalogProduct ? barcodeImageSrc((catalogProduct.client_image ? catalogProduct.BarCode : catalogProduct.BarCode), this.props.item.placement.position, this.props.user ? this.props.user.db : '') : ''}
                                            key={id + "_" + i + "_" + _i + "_" + __i}
                                        />
                                    }) : (
                                        <img
                                            style={{
                                                zIndex: 300 - (0),
                                                left: (0 * pixelSideStack) + "px",
                                                bottom: (0 * pixelTopStack) + "px",
                                                width: widthDensity(this.props.item.placement.pWidth && (this.props.item.placement.pWidth > 0) ? this.props.item.placement.pWidth : 0),
                                                height: heightDensity(this.props.item.placement.pHeight && (this.props.item.placement.pHeight > 0) ? this.props.item.placement.pHeight : 0),
                                            }}
                                            alt=""
                                            onError={e => {
                                                e.currentTarget.src = noImage;
                                            }}
                                            src={catalogProduct ? JSON.parse(JSON.stringify(barcodeImageSrc((catalogProduct.client_image ? catalogProduct.client_image : catalogProduct.BarCode), this.props.item.placement.position, this.props.user ? this.props.user.db : ''))) : ''}
                                            // /* Barak 3.1.21 */ src={catalogProduct ? barcodeImageSrc((this.props.item.placement.position && catalogProduct.client_image_arr 
                                            //                                                         && catalogProduct.client_image_arr[this.props.item.placement.position] 
                                            //                                                         ? catalogProduct.client_image_arr[this.props.item.placement.position] : catalogProduct.BarCode), this.props.item.placement.position) : ''}
                                            key={id + "_" + i + "_" + _i + "_" + 0}
                                        />
                                    )}
                                </div>
                            )
                            )}
                    </div>
                ))}
                {shelf.freezer != 1 && !hideSaleTags
                    ? <ActiveProductTag key={"tag_" + id} barcode={catalogProduct ? catalogProduct.BarCode : 0} />
                    : null}
            </div>
        ));
    }
}
type ActiveProductTagComponentProps = { barcode: number }
const activeProductTagMapStateToProps = (state: AppState, ownProps: ActiveProductTagComponentProps) => ({
    ...ownProps,
    branch_id: state.planogram.store ? state.planogram.store.branch_id : "",
    weeklySale: state.catalog.productSales[ownProps.barcode] ? state.catalog.productSales[ownProps.barcode].weekly : undefined,
    statusClaMlay: state.catalog.productSales[ownProps.barcode] ? state.catalog.productSales[ownProps.barcode].statusClaMlay : undefined,
    amount: state.planogram.productDetails[ownProps.barcode] ? state.planogram.productDetails[ownProps.barcode].maxAmount : 0,
    statusClacInv: state.planogram.store ? state.planogram.store.statusCalcInv : 0,
    calcInvDays: state.planogram.store ? state.planogram.store.calcInvDays : 0,
    Status0Percent: state.planogram.store ? state.planogram.store.Status0Percent : 100,
    Status1Percent: state.planogram.store ? state.planogram.store.Status1Percent : 100,
    Status2Percent: state.planogram.store ? state.planogram.store.Status2Percent : 100,
    Status3Percent: state.planogram.store ? state.planogram.store.Status3Percent : 60,
    Status4Percent: state.planogram.store ? state.planogram.store.Status4Percent : 100,
})
const activeProductTagMapDispatchToProps = (dispatch: ThunkDispatch<AppState, any, any>) => ({
    setWeeklySale: (barcode: number, weeklySale: number | null, SetStatusCalMlay: number | null, hourly: number | null, createDate: Date) => dispatch(setWeeklySale(barcode, weeklySale, SetStatusCalMlay, hourly, createDate))
})
class ActiveProductTagComponent extends React.Component<ReturnType<typeof activeProductTagMapStateToProps> & ReturnType<typeof activeProductTagMapDispatchToProps>> {
    isActive = false;
    componentDidMount = async () => {
        if (this.props.weeklySale === undefined) {
            this.isActive = true;
            productPredictedSales(this.props.barcode, this.props.branch_id
                // Add StatusClacInv and calcInvDays to function call
                , this.props.statusClacInv, this.props.calcInvDays
            ).then((saleItem) => {
                if (this.isActive && saleItem != null) {
                    this.props.setWeeklySale(this.props.barcode, saleItem.WeeklyAverage, saleItem.statusCalMlay, saleItem.HourlyAverage, saleItem.Create_Date);
                }
            }).catch((err) => {
                console.error(err);
            });
        }
    }
    componentWillUnmount() {
        this.isActive = false;
    }
    render() {
        const { amount, weeklySale, statusClaMlay } = this.props;
        let colorTag: string = 'good';
        let sale: number = 0;
        if (weeklySale != null) {
            sale = Math.round(weeklySale);
        }
        let result = 0;
        let { Status0Percent, Status1Percent, Status2Percent, Status3Percent, Status4Percent } = this.props;
        let percent = 1;
        if (statusClaMlay === 0) percent = Status0Percent / 100;
        if (statusClaMlay === 1) percent = Status1Percent / 100;
        if (statusClaMlay === 2) percent = Status2Percent / 100;
        if (statusClaMlay === 3) percent = Status3Percent / 100;
        if (statusClaMlay === 4) percent = Status4Percent / 100;
        result = (sale * percent) / amount;
        if (result < 0.8) colorTag = 'good'; // Green tag
        if (result >= 0.8 && result <= 1.2) colorTag = 'middleColor'; // Yellow tag 
        if (result > 1.2) colorTag = 'bad'; // Red tag
        if (this.props.barcode != 123456789)
            return (
                <div className={"shelf-item-tag " + (colorTag)}>
                    {(weeklySale != null ? Math.round(weeklySale) : "~")}
                    /
                    {amount}
                </div>
            )
        else {
            return null;
        }

    }
}
const ActiveProductTag = connect(activeProductTagMapStateToProps, activeProductTagMapDispatchToProps)(ActiveProductTagComponent);
export default DraggableSource(DroppableTarget(StoreConnector(PlanogramShelfItemComponent)));
