import * as React from "react";
import { Switch, Route, withRouter, RouteComponentProps, Redirect } from 'react-router-dom';
import PlanogramAisleComponent from './PlanogramAisleComponent';
import { AnyAction } from 'redux';
import { AppState } from '@src/planogram/shared/store/app.reducer';
import { connect } from 'react-redux';
import { PlanogramElementId, PlanogramShelf, PlacementObject, PlanogramItem, PlanogramAisle, PlanogramStore } from '@src/planogram/shared/store/planogram/planogram.types';
import { setStore } from '@src/planogram/shared/store/planogram/store/store.actions';
import {  hideProductDetailer,  setMultiSelectId, setCurrentSelectedId,  setAisleSaveArray, setAisleSaveIndex } from '@src/planogram/shared/store/planogram/planogram.actions';
import { CatalogProduct } from '@src/planogram/shared/interfaces/models/CatalogProduct';
import { ThunkDispatch } from 'redux-thunk';
import * as planogramApi from '@src/planogram/shared/api/planogram.provider';
import * as planogramProvider from "@src/planogram/shared/api/planogram.provider";
import { uiNotify } from '@src/planogram/shared/components/Toast';
import { deleteItemAction, editShelfItemAction } from '@src/planogram/shared/store/planogram/store/item/item.actions';
import { blankItem, blankPlanogramAisle, ProductDefaultDimensions } from '@src/planogram/shared/store/planogram/planogram.defaults';
import { setAisleAction } from '@src/planogram/shared/store/planogram/store/aisle/aisle.actions';

type ContainerComponentProps = {
} & RouteComponentProps<{
    store_id?: string,
    branch_id?: string,
}> & {
}

const mapStateToProps = (state: AppState, ownProps: ContainerComponentProps) => ({
    ...ownProps,
    products: state.catalog.products,
    productMap: state.newPlano ? state.newPlano.productsMap : null,
    planogram: state.planogram,
    store: state.planogram.store,
    productDetailerState: state.planogram.display.productDetailer.barcode != null,
    multiSelectId: state.planogram.display.multiSelectId,
    currentSelectedId: state.planogram.display.currentSelectedId,
    shelvesDetails: state.planogram.virtualStore.shelfDetails,
    aisleShelves: state.planogram.virtualStore.shelfMap,
    alignToLeft: state.planogram.display.alignToLeft,
    virtualStore: state.planogram.virtualStore,
    newPlano: state.newPlano,
    aisleSaveArray: state.planogram.display.aisleSaveArray,
    aisleSaveIndex: state.planogram.display.aisleSaveIndex,
    storeProductMap: state.planogram.store ? state.planogram.store.storeProductMap : {},
    user: state.auth.user,
});
const mapDispatchToProps = (dispatch: ThunkDispatch<AppState, {}, AnyAction>) => ({
    hideProductDisplayerBarcode: () => {
        dispatch(hideProductDetailer());
    },
    deleteItem: (shelf: PlanogramElementId, item: PlanogramElementId) => {
        dispatch(deleteItemAction(shelf, item))
    },
    setMultiSelectId: (multiSelectId: string[]) => dispatch(setMultiSelectId(multiSelectId)),
    setCurrentSelectedId: (currentSelectedId: string) => dispatch(setCurrentSelectedId(currentSelectedId)),
    editShelfItemPlacement: (item: PlanogramElementId, placement: PlacementObject) => {
        dispatch(editShelfItemAction(item, placement));
    },
    setStoreAisle: (aisle: PlanogramAisle, aislePid?: PlanogramElementId) => {
        dispatch(setAisleAction(aisle, aislePid));
    },
    setAisleSaveArray: (aisleSaveArray: PlanogramAisle[]) => {
        dispatch(setAisleSaveArray(aisleSaveArray));
    },
    setAisleSaveIndex: (aisleSaveIndex: number) => {
        dispatch(setAisleSaveIndex(aisleSaveIndex));
    },
    setStore: (store: PlanogramStore) => { dispatch(setStore(store)); },
});

type ContainerProps = ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>;

class PlanogramStoreComponentContainer extends React.Component<ContainerProps> {
    keyboardTimeout?: NodeJS.Timeout;
    keyboardCollector = "";

    state = {
        searchFilter: '',
        isLoading: false
    }

    storePosition(barcode: number) {
        let length = this.props.products.length;
        let initialResult: any[] = [];
        let result: any[] = [];
        for (let i = 0; i < length; i++) {
            // check if input barcode appears as part of this.props.products[i].BarCode
            if (this.props.products[i].BarCode.toString().includes(barcode.toString())) {
                // check if we have an object in that location
                /* replace this.props.planogram.productDetails with this.props.store.storeProductMap *
                if (this.props.planogram.productDetails[this.props.products[i].BarCode]) {
                    // concat is used to append one array to another
                    initialResult = initialResult.concat(this.props.planogram.productDetails[this.props.products[i].BarCode].position);
                }
                ******************************************************************************************************/
                /*  replace this.props.planogram.productDetails with this.props.store.storeProductMap */
                if (this.props.store && this.props.store.storeProductMap[this.props.products[i].BarCode]) {
                    // concat is used to append one array to another
                    initialResult = initialResult.concat(this.props.store.storeProductMap[this.props.products[i].BarCode].position);
                }
                /*****************************************************************************************************/
            }
        }
        // at this point result may include many duplicate shelvs so we need to filter it to unique aisle_id only
        let aisleIds = initialResult.map(obj => { return { aisleID: obj.aisle_id, productID: 'A' + obj.aisle_id + 'SE' + obj.section + 'SH' + obj.shelf + "I" + obj.itemIndex } });
        // aisleIds = aisleIds.filter((v, i) => { return aisleIds.indexOf(v) == i; });
        let output: any[] = [];
        for (let i = 0; i < aisleIds.length; i++) {
            let index = output.findIndex(line => line.aisleID === aisleIds[i].aisleID);
            if (index < 0) {
                output.push(aisleIds[i]);
            }
        }
        /****************/
        // at this point aisleIds includce only the unique values of aisle_id
        // now we need to run over aisleIds and enter a single object from initialResult into result for each aisle_id
        for (let n = 0; n < output.length; n++) {
            let index = initialResult.findIndex(obj => obj.aisle_id === output[n].aisleID)
            if (index != -1) {
                let rec = {
                    aisleID: output[n].aisleID,
                    productID: output[n].productID,
                    empty: this.props.store  && this.props.store.aisles && this.props.store.aisles.filter(line => line.aisle_id === output[n].aisleID)[0] 
                                            ? this.props.store.aisles.filter(line => line.aisle_id === output[n].aisleID)[0].empty : false,
                }
                result.push(rec);
            }
        }
        return result;
    }

    /* Add currentSelectedId for movment with arrows */
    handleKeyboard = async (e: KeyboardEvent) => {
        let store = this.props.store;
        let item = JSON.parse(JSON.stringify(blankItem));
        let catalogProduct: CatalogProduct = {
            Id: 0,
            BarCode: 0,
            Name: '',
            Bdate_buy: '',
            Bdate_sale: '',
            IsSubBar: false,
            SegmentId: 0,
            ModelId: 0,
            Create_Date: new Date()
        };
        let currentAisleId = parseInt(this.props.currentSelectedId.substring(this.props.currentSelectedId.indexOf('A') + 1,
            this.props.currentSelectedId.indexOf('SE')));
        let currentAisle: any = null;
        if (store) {
            for (let i = 0; i < store.aisles.length; i++) {
                if (store.aisles[i].aisle_id === currentAisleId) {
                    currentAisle = store.aisles[i];
                    break;
                }
            }
        }

        let value = parseInt(e.key);
        if (value >= 0 && value <= 9 || e.key === 'f' || e.key === 'כ') {
            let currSE: number = parseInt(this.props.currentSelectedId.substring(this.props.currentSelectedId.indexOf('SE') + 2,
                this.props.currentSelectedId.indexOf('SH')));
            let currSH: number = parseInt(this.props.currentSelectedId.substring(this.props.currentSelectedId.indexOf('SH') + 2,
                this.props.currentSelectedId.indexOf('I')));
            let currI: number = parseInt(this.props.currentSelectedId.substring(this.props.currentSelectedId.indexOf('I') + 1));

            if (currentAisle && currentAisle.sections[currSE]
                && currentAisle.sections[currSE].shelves[currSH]
                && currentAisle.sections[currSE].shelves[currSH].items[currI]) {
                item = currentAisle.sections[currSE].shelves[currSH].items[currI];
                catalogProduct = this.props.productMap[item.product];
            }
            else {
                item.id = '';
                catalogProduct = this.props.productMap[0];
            }
            if (item.id != '') {
                if (this.keyboardTimeout)
                    clearTimeout(this.keyboardTimeout);
                this.keyboardCollector += e.key;
                this.keyboardTimeout = setTimeout(async () => {
                    // const { item, catalogProduct } = this.props;
                    if ((this.keyboardCollector[0] === 'f' || this.keyboardCollector[0] === 'כ') && this.keyboardCollector.length >= 2) {
                        let index = this.keyboardCollector.lastIndexOf('f');
                        if (index === -1) index = this.keyboardCollector.lastIndexOf('כ');
                        let newFaces = parseInt(this.keyboardCollector.slice(index + 1));
                        if (newFaces >= 1 && newFaces <= 20) {
                            if (this.props.multiSelectId.length <= 1) {
                                this.props.editShelfItemPlacement(this.props.currentSelectedId, {
                                    ...item.placement,
                                    faces: newFaces
                                });
                                /* if alignToLeft is true we need to immediately align the entire new shelf to the left */
                                // get this item's shelfId
                                let shelfId = this.props.currentSelectedId.substring(0, this.props.currentSelectedId.indexOf('I'));
                                // after changing placment (faces, position etc') we need to re-order the shelf
                                // this.reOrderThisShelfPosition(this.props.aisleShelves[shelfId]);
                                if (this.props.aisleShelves[shelfId] != undefined) {
                                    await this.addShelfItem(this.props.aisleShelves[shelfId], blankItem);
                                    if (this.props.alignToLeft)
                                        await this.alignThisShelfToLeft(this.props.aisleShelves[shelfId]);
                                    alert(`Successfully updated faces for product: ${(catalogProduct && catalogProduct.Name) || item.product}`);
                                } else alert(`problem finding the shelf`);
                            }
                            else alert(`Cannot updated faces on multiple items at the same time`);
                        }
                        this.keyboardCollector = "";
                    }

                    else this.keyboardCollector = "";

                }, 1000);
            }
            /*  Allow Ctrl+x/Ctrl+y */
            let aisleId = this.props.currentSelectedId.substring(0, this.props.currentSelectedId.indexOf('SE'))
            let aisleIndex = this.props.store && this.props.store.aisles && this.props.store.aisles.length >= 1 ? this.props.store.aisles.findIndex(line => line.id === aisleId) : -1;
            if (this.props.store && aisleIndex >= 0) {
                this.saveSnapShotAisle(this.props.store.aisles[aisleIndex]);
            }
        }

        /*  Add Delete on Del when this.props.multiSelectId contains id's */
        if (e.keyCode == 46 || e.key === "Delete") {
            // the Delete key was pressed
            if (this.props.multiSelectId.length >= 1) {
                // sort the multiSelectId array so that it's in order of section, shelf and item index 
                // so that I can run the delete backwards on the array without loosing any items,
                // no matter the order in which they were entered into the array
                this.props.multiSelectId.sort(function (a: any, b: any) {
                    let currSE_A: number = parseInt(a.substring(a.indexOf('SE') + 2, a.indexOf('SH')));
                    let currSH_A: number = parseInt(a.substring(a.indexOf('SH') + 2, a.indexOf('I')));
                    let currI_A: number = parseInt(a.substring(a.indexOf('I') + 1));
                    let currSE_B: number = parseInt(b.substring(b.indexOf('SE') + 2, b.indexOf('SH')));
                    let currSH_B: number = parseInt(b.substring(b.indexOf('SH') + 2, b.indexOf('I')));
                    let currI_B: number = parseInt(b.substring(b.indexOf('I') + 1));
                    // if it's the same section and shelf, order by item index
                    if (currSE_A === currSE_B && currSH_A === currSH_B)
                        return (currI_A > currI_B) ? 1 : (currI_A < currI_B) ? -1 : 0;
                    // if it's the same section order by shelf    
                    else if (currSE_A === currSE_B)
                        return (currSH_A > currSH_B) ? 1 : (currSH_A < currSH_B) ? -1 : 0;
                    else return (currSE_A > currSE_B) ? 1 : (currSE_A < currSE_B) ? -1 : 0;
                });
                // delete each item saved in multiSelectId from this shelf
                console.log('multiSelectId length for delete', this.props.multiSelectId.length, this.props.multiSelectId);
                // I'm preforming the delete backwards to avoid a deleted item changing the entire shelf in a way that 
                // would make it impossible to delete the other items in multiSelectId
                for (let i = this.props.multiSelectId.length - 1; i >= 0; i--) {
                    /* Barak 21.12.20 - if item.linkedItemUp is not null - DO NOT ALLOW IT TO BE DELETED */
                    let checkSec = parseInt(this.props.multiSelectId[i].substring(this.props.multiSelectId[i].indexOf('SE') + 2, this.props.multiSelectId[i].indexOf('SH')));
                    let checkSh = parseInt(this.props.multiSelectId[i].substring(this.props.multiSelectId[i].indexOf('SH') + 2, this.props.multiSelectId[i].indexOf('I')));
                    let aisle: PlanogramAisle = JSON.parse(JSON.stringify(blankPlanogramAisle));
                    let aisles = this.props.store ? this.props.store.aisles : undefined;
                    let aisleId = this.props.multiSelectId[i].substring(0, this.props.multiSelectId[i].indexOf('SE'));
                    if (aisles) {
                        for (let i = 0; i < aisles.length; i++) {
                            if (aisles[i].id === aisleId) {
                                aisle = aisles[i];
                                break;
                            }
                        }
                    }
                    let item: any = null;
                    if (aisle) {
                        let checkInd = aisle.sections[checkSec].shelves[checkSh].items.findIndex(line => line.id === this.props.multiSelectId[i]);
                        item = aisle.sections[checkSec].shelves[checkSh].items[checkInd];
                    }
                    if (item && item.linkedItemUp != null) {
                        let errorMsg = 'Cannot delete an item if there is another item above it';
                        alert(errorMsg);
                        continue;
                    }
                    if (item && item.linkedItemDown != null) {
                        // we're deleting the second story item so we need to change the item.linkedItemUp of the original item to null
                        let checkSecD = parseInt(item.linkedItemDown.substring(item.linkedItemDown.indexOf('SE') + 2, item.linkedItemDown.indexOf('SH')));
                        let checkShD = parseInt(item.linkedItemDown.substring(item.linkedItemDown.indexOf('SH') + 2, item.linkedItemDown.indexOf('I')));
                        if (aisle) {
                            let checkIndD = aisle.sections[checkSecD].shelves[checkShD].items.findIndex(line => line.id === item.linkedItemDown);
                            let itemToUpdate = aisle.sections[checkSecD].shelves[checkShD].items[checkIndD];
                            if (itemToUpdate) itemToUpdate.linkedItemUp = null;
                        }
                    }
                    /******************************************************************************/
                    let shelfId = this.props.currentSelectedId.substring(0, this.props.currentSelectedId.indexOf('I'));
                    await this.props.deleteItem(shelfId, this.props.multiSelectId[i]);
                    if (this.props.alignToLeft)
                        await this.alignThisShelfToLeft(this.props.aisleShelves[shelfId]);
                }
                let aisleId = this.props.multiSelectId[0].substring(0, this.props.multiSelectId[0].indexOf('SE'))
                let aisleIndex = this.props.store && this.props.store.aisles && this.props.store.aisles.length >= 1 ? this.props.store.aisles.findIndex(line => line.id === aisleId) : -1;
                if (this.props.store && aisleIndex >= 0) {
                    this.saveSnapShotAisle(this.props.store.aisles[aisleIndex]);
                }
                /***************************************/
                this.props.setMultiSelectId([]);
            }
        }
        if (e.keyCode == 39 || e.key === "ArrowRight") {
            // right arrow >
            let currSE: number = parseInt(this.props.currentSelectedId.substring(this.props.currentSelectedId.indexOf('SE') + 2,
                this.props.currentSelectedId.indexOf('SH')));
            let currSH: number = parseInt(this.props.currentSelectedId.substring(this.props.currentSelectedId.indexOf('SH') + 2,
                this.props.currentSelectedId.indexOf('I')));
            let currI: number = parseInt(this.props.currentSelectedId.substring(this.props.currentSelectedId.indexOf('I') + 1)) + 1;
            let newSelected: string = '';
            if (currentAisle && currentAisle.sections[currSE] && currentAisle.sections[currSE].shelves[currSH] && currentAisle.sections[currSE].shelves[currSH].items[currI])
                newSelected = this.props.currentSelectedId.substring(0, this.props.currentSelectedId.indexOf('I') + 1) + currI;
            else {
                let nCurrI = 0;
                currSE += 1;
                if (currentAisle && currentAisle.sections[currSE] && currentAisle.sections[currSE].shelves[currSH] && currentAisle.sections[currSE].shelves[currSH].items[nCurrI])
                    newSelected = this.props.currentSelectedId.substring(0, this.props.currentSelectedId.indexOf('SE') + 2) + currSE + 'SH' + currSH + "I" + nCurrI;
                else newSelected = this.props.currentSelectedId.substring(0, this.props.currentSelectedId.indexOf('SE') + 2) + (currSE - 1) + 'SH' + currSH + "I" + (currI - 1);
            }
            this.props.setCurrentSelectedId(newSelected);
        }
        if (e.keyCode == 37 || e.key === "ArrowLeft") {
            // left arrow <
            let currSE: number = parseInt(this.props.currentSelectedId.substring(this.props.currentSelectedId.indexOf('SE') + 2,
                this.props.currentSelectedId.indexOf('SH')));
            let currSH: number = parseInt(this.props.currentSelectedId.substring(this.props.currentSelectedId.indexOf('SH') + 2,
                this.props.currentSelectedId.indexOf('I')));
            let currI: number = parseInt(this.props.currentSelectedId.substring(this.props.currentSelectedId.indexOf('I') + 1)) - 1;

            let newSelected: string = '';
            if (currentAisle && currentAisle.sections[currSE] && currentAisle.sections[currSE].shelves[currSH] && currentAisle.sections[currSE].shelves[currSH].items[currI])
                newSelected = this.props.currentSelectedId.substring(0, this.props.currentSelectedId.indexOf('I') + 1) + currI;
            else {
                let nCurrI = 0;
                currSE -= 1;
                if (currentAisle && currentAisle.sections[currSE] && currentAisle.sections[currSE].shelves[currSH]) {
                    // get last item index of pervious section 
                    nCurrI = currentAisle.sections[currSE].shelves[currSH].items.length - 1;
                    newSelected = this.props.currentSelectedId.substring(0, this.props.currentSelectedId.indexOf('SE') + 2) + currSE + 'SH' + currSH + "I" + nCurrI;
                }
                else newSelected = this.props.currentSelectedId.substring(0, this.props.currentSelectedId.indexOf('SE') + 2) + (currSE + 1) + 'SH' + currSH + "I" + (currI + 1);
            }
            this.props.setCurrentSelectedId(newSelected);
        }
        if (e.keyCode == 38 || e.key === "ArrowUp") {
            let currSE: number = parseInt(this.props.currentSelectedId.substring(this.props.currentSelectedId.indexOf('SE') + 2,
                this.props.currentSelectedId.indexOf('SH')));
            let currSH: number = parseInt(this.props.currentSelectedId.substring(this.props.currentSelectedId.indexOf('SH') + 2,
                this.props.currentSelectedId.indexOf('I'))) + 1;
            let currI: number = parseInt(this.props.currentSelectedId.substring(this.props.currentSelectedId.indexOf('I') + 1));
            let newSelected: string = '';
            if (currentAisle && currentAisle.sections[currSE] && currentAisle.sections[currSE].shelves[currSH] && currentAisle.sections[currSE].shelves[currSH].items[currI])
                newSelected = this.props.currentSelectedId.substring(0, this.props.currentSelectedId.indexOf('SE') + 2) + currSE + 'SH' + currSH + "I" + currI;
            else {
                newSelected = this.props.currentSelectedId.substring(0, this.props.currentSelectedId.indexOf('SE') + 2) + currSE + 'SH' + (currSH - 1) + "I" + currI;
            }
            this.props.setCurrentSelectedId(newSelected);
        }
        if (e.keyCode == 40 || e.key === "ArrowDown") {
            let currSE: number = parseInt(this.props.currentSelectedId.substring(this.props.currentSelectedId.indexOf('SE') + 2,
                this.props.currentSelectedId.indexOf('SH')));
            let currSH: number = parseInt(this.props.currentSelectedId.substring(this.props.currentSelectedId.indexOf('SH') + 2,
                this.props.currentSelectedId.indexOf('I'))) - 1;
            let currI: number = parseInt(this.props.currentSelectedId.substring(this.props.currentSelectedId.indexOf('I') + 1));
            let newSelected: string = '';
            if (currentAisle && currentAisle.sections[currSE] && currentAisle.sections[currSE].shelves[currSH] && currentAisle.sections[currSE].shelves[currSH].items[currI])
                newSelected = this.props.currentSelectedId.substring(0, this.props.currentSelectedId.indexOf('SE') + 2) + currSE + 'SH' + currSH + "I" + currI;
            else {
                newSelected = this.props.currentSelectedId.substring(0, this.props.currentSelectedId.indexOf('SE') + 2) + currSE + 'SH' + (currSH + 1) + "I" + currI;
            }
            this.props.setCurrentSelectedId(newSelected);
        }
    }
    bindKeyboard = () => {
        window.addEventListener('keydown', this.handleKeyboard);
    }
    unBindKeyboard = () => {
        window.removeEventListener('keydown', this.handleKeyboard);
    }
    alignThisShelfToLeft = async (shelf: PlanogramShelf) => {
        const { store, aisleShelves, shelvesDetails } = this.props;

        // check if we need to align multiple shelves
        let all_shelves_to_align: PlanogramShelf[] = [];
        if (this.props.multiSelectId.length > 1) {
            // sort multiSelectId by shelf and section
            this.props.multiSelectId.sort(function (a: any, b: any) {
                let currSE_A: number = parseInt(a.substring(a.indexOf('SE') + 2, a.indexOf('SH')));
                let currSH_A: number = parseInt(a.substring(a.indexOf('SH') + 2, a.indexOf('I')));
                let currSE_B: number = parseInt(b.substring(b.indexOf('SE') + 2, b.indexOf('SH')));
                let currSH_B: number = parseInt(b.substring(b.indexOf('SH') + 2, b.indexOf('I')));
                // if it's the same shelf order by section    
                if (currSH_A === currSH_B)
                    return (currSE_A > currSE_B) ? 1 : (currSE_A < currSE_B) ? -1 : 0;
                else return (currSH_A > currSH_B) ? 1 : (currSH_A < currSH_B) ? -1 : 0;
            });
            // find each shelf represented in multiSelectId and add them to all_shelves_to_align
            for (let a = 0; a < this.props.multiSelectId.length; a++) {
                let target_A = this.props.multiSelectId[a].substring(0, this.props.multiSelectId[a].indexOf('SE'));
                let target_SE = parseInt(this.props.multiSelectId[a].substring(this.props.multiSelectId[a].indexOf('SE') + 2, this.props.multiSelectId[a].indexOf('SH')));
                let target_SH = parseInt(this.props.multiSelectId[a].substring(this.props.multiSelectId[a].indexOf('SH') + 2));
                if (store) {
                    for (let i = 0; i < store.aisles.length; i++) {
                        if (store.aisles[i].id === target_A) {
                            let thisShelf = store.aisles[i].sections[target_SE].shelves[target_SH];
                            let index = all_shelves_to_align.findIndex(shelf => shelf.id === thisShelf.id);
                            if (index < 0 && thisShelf.freezer != 1 && thisShelf.freezer != 2) all_shelves_to_align.push(thisShelf);
                            // now that we've added the shelf the item is actually on, we need to check if the item covers more than one shelf.
                            // since we know this is the shelf the item starts in we can get the item's details from it.
                            let item = thisShelf.items.filter(line => line.id === this.props.multiSelectId[a]);
                            let itemTotalWidth = (item[0].placement.distFromStart ? item[0].placement.distFromStart : 0) + ((item[0].placement.pWidth ? item[0].placement.pWidth : ProductDefaultDimensions.width) * item[0].placement.faces);
                            let shelfWidth = thisShelf.dimensions.width;
                            if (itemTotalWidth > shelfWidth) {
                                while (itemTotalWidth > shelfWidth) {
                                    target_SE++;
                                    let nextShelf = store.aisles[i].sections[target_SE].shelves[target_SH];
                                    let index = all_shelves_to_align.findIndex(shelf => shelf.id === nextShelf.id);
                                    if (index < 0 && nextShelf.freezer != 1 && nextShelf.freezer != 2) all_shelves_to_align.push(nextShelf);
                                    itemTotalWidth -= shelfWidth;
                                    shelfWidth = nextShelf.dimensions.width;
                                }
                            }
                        }
                    }
                }
            }
        } else if (shelf.freezer != 1 && shelf.freezer != 2) all_shelves_to_align.push(shelf);

        // now that we have all shelvs that need to be aligned we can align them one after the other
        for (let aa = 0; aa < all_shelves_to_align.length; aa++) {
            // find latest version of this shelf in the store aisle
            let thisShelf = all_shelves_to_align[aa];
            let target_A = all_shelves_to_align[aa].id.substring(0, all_shelves_to_align[aa].id.indexOf('SE'));
            let target_SE = parseInt(all_shelves_to_align[aa].id.substring(all_shelves_to_align[aa].id.indexOf('SE') + 2, all_shelves_to_align[aa].id.indexOf('SH')));
            let target_SH = parseInt(all_shelves_to_align[aa].id.substring(all_shelves_to_align[aa].id.indexOf('SH') + 2));
            if (store) {
                for (let i = 0; i < store.aisles.length; i++) {
                    if (store.aisles[i].id === target_A) {
                        thisShelf = store.aisles[i].sections[target_SE].shelves[target_SH];
                    }
                }
            }

            // get this shelf's combined shelves and only leave in the array shelves that exist in all_shelves_to_align
            let shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[all_shelves_to_align[aa].id]));
            while (shelfDetails.main_shelf) {
                let ids = JSON.parse(JSON.stringify(shelfDetails.main_shelf));
                shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[ids]));
                thisShelf = aisleShelves[ids];
            }
            let { combined } = shelfDetails;

            let combinedShelves = [thisShelf].concat(combined.map((id: any) => aisleShelves[id]));

            // remove from combinedShelves any shelves that do not appear in all_shelves_to_align
            let newCombinedShelves = [];
            for (let i = 0; i < combinedShelves.length; i++) {
                let index = all_shelves_to_align.findIndex(shelf => shelf.id === combinedShelves[i].id);
                if (index >= 0) newCombinedShelves.push(combinedShelves[i]);
            }
            combinedShelves = newCombinedShelves;

            let A_Id = combinedShelves[0].id.substring(0, combinedShelves[0].id.indexOf('SE'));
            let SE_Index = parseInt(combinedShelves[0].id.substring(combinedShelves[0].id.indexOf('SE') + 2, combinedShelves[0].id.indexOf('SH')));
            let SH_Index = parseInt(combinedShelves[0].id.substring(combinedShelves[0].id.indexOf('SH') + 2));

            // check if this aisle has a shelf in the same height, in previous section
            let firstDistFromStart = 0;
            if (store) {
                let store_aisle: PlanogramAisle = store.aisles.filter(aisle => aisle.id === A_Id)[0];
                if (store_aisle && store_aisle.sections[SE_Index - 1] && store_aisle.sections[SE_Index - 1].shelves[SH_Index]
                    &&
                    ((store_aisle.sections[SE_Index - 1].shelves[SH_Index].dimensions.height === store_aisle.sections[SE_Index].shelves[SH_Index].dimensions.height)
                        || SH_Index === 0
                    )) {                    // get all items in the previous shelf
                    let prev_shelf_items = JSON.parse(JSON.stringify(store_aisle.sections[SE_Index - 1].shelves[SH_Index].items));
                    // sort the items by distFromStart in case they are not already sorted
                    prev_shelf_items.sort(function (a: any, b: any) {
                        let aNumber = a.placement.distFromStart ? a.placement.distFromStart : 0;
                        let bNumber = b.placement.distFromStart ? b.placement.distFromStart : 0;
                        let aId = a.Id;
                        let bId = b.Id;
                        return (aNumber > bNumber) ? 1 : (aNumber < bNumber) ? -1 : ((aId > bId) ? 1 : (aId < bId) ? -1 : 0);
                    });

                    // if so check if the last item (based on distFromStart) on the previous shelf intrudes into this shelf
                    let lastItem = prev_shelf_items[prev_shelf_items.length - 1];
                    console.log('lastItem', lastItem);
                    if (lastItem) {
                        let endPoint = (lastItem.placement.distFromStart ? lastItem.placement.distFromStart : 0) + ((lastItem.placement.pWidth != undefined ? lastItem.placement.pWidth : ProductDefaultDimensions.width) * lastItem.placement.faces);
                        let leftInShelf = store_aisle.sections[SE_Index].shelves[SH_Index].dimensions.width - endPoint;
                        if (leftInShelf < 0) firstDistFromStart = (-1 * leftInShelf);
                        console.log('leftInShelf', leftInShelf, 'firstDistFromStart', firstDistFromStart);
                    }
                }
            }
            for (let i = 0; i < combinedShelves.length; i++) {
                // sort shelf items by distFromStart
                combinedShelves[i].items.sort(function (a: any, b: any) {
                    let aNumber = a.placement.distFromStart ? a.placement.distFromStart : 0;
                    let bNumber = b.placement.distFromStart ? b.placement.distFromStart : 0;
                    let aId = a.Id;
                    let bId = b.Id;
                    return (aNumber > bNumber) ? 1 : (aNumber < bNumber) ? -1 : ((aId > bId) ? 1 : (aId < bId) ? -1 : 0);
                });
            }
            let viewCombinedShelves = JSON.parse(JSON.stringify(combinedShelves));
            console.log('combinedShelves initial', viewCombinedShelves);
            // empty all items from current shelvs
            for (let i = 0; i < combinedShelves.length; i++) {
                combinedShelves[i].items = [];
            }

            let distFromStart = firstDistFromStart;
            let distInShelf = firstDistFromStart;
            let index = 0;
            let startOfShelfId = '';
            let spaceInShelf = 0;
            // go over the initial state and populate each shelf in combinedShelves with the correct items in the correct order
            let shelfId = 0;
            for (let i = 0; i < viewCombinedShelves.length; i++) {
                if (i === 0) {
                    startOfShelfId = viewCombinedShelves[i].id;
                    spaceInShelf = viewCombinedShelves[shelfId].dimensions.width;
                }
                for (let n = 0; n < viewCombinedShelves[i].items.length; n++) {
                    let item = JSON.parse(JSON.stringify(viewCombinedShelves[i].items[n]));
                    /* Barak 26.11.20 - return items above main line (item index >= 100) back into the shelf without altering anything else */
                    let itemInd = parseInt(item.id.substring(item.id.indexOf('I') + 1));
                    if (itemInd >= 100) {
                        if (item.shelfLevel === 1 || item.shelfLevel === undefined) combinedShelves[shelfId].items.push(item); // for items in freezer === 2
                        /* Barak 21.12.20 - verify items with shelfLevel 2 position */
                        else { // for items with shelfLevel > 1
                            // we need to return the item to it's original shelf
                            let destinationSec = parseInt(item.id.substring(item.id.indexOf('SE') + 2, item.id.indexOf('SH')));
                            let destinationSh = parseInt(item.id.substring(item.id.indexOf('SH') + 2, item.id.indexOf('I')));
                            let aisle: PlanogramAisle = JSON.parse(JSON.stringify(blankPlanogramAisle));
                            let aisles = this.props.store ? this.props.store.aisles : undefined;
                            let aisleId = shelf.id.substring(0, shelf.id.indexOf('SE'));
                            if (aisles) {
                                for (let i = 0; i < aisles.length; i++) {
                                    if (aisles[i].id === aisleId) {
                                        aisle = aisles[i];
                                        break;
                                    }
                                }
                            }
                            aisle.sections[destinationSec].shelves[destinationSh].items.push(item);
                        }
                    }
                    else {
                        if (spaceInShelf <= 0) {
                            shelfId++; // the shelf id in the viewCombinedShelves array
                            if (viewCombinedShelves[shelfId]) {
                                // there is a next shelf so we update the shelfNumber, startOfShelfId, spaceInShelf and index
                                // let shelfNumber = parseInt(JSON.parse(JSON.stringify(startOfShelfId.substring(startOfShelfId.indexOf('SE') + 2, startOfShelfId.indexOf('SH'))))) + 1;
                                let old_sec_num = parseInt(JSON.parse(JSON.stringify(startOfShelfId.substring(startOfShelfId.indexOf('SE') + 2, startOfShelfId.indexOf('SH')))));
                                let new_sec_num = parseInt(JSON.parse(JSON.stringify(viewCombinedShelves[shelfId].id.substring(viewCombinedShelves[shelfId].id.indexOf('SE') + 2, viewCombinedShelves[shelfId].id.indexOf('SH')))))
                                startOfShelfId = JSON.parse(JSON.stringify(startOfShelfId.substring(0, startOfShelfId.indexOf('SE') + 2) + new_sec_num + startOfShelfId.substring(startOfShelfId.indexOf('SH'))));
                                if (new_sec_num - old_sec_num === 1) {
                                    distInShelf = (spaceInShelf * -1);
                                    distFromStart = (spaceInShelf * -1);
                                    spaceInShelf = viewCombinedShelves[shelfId].dimensions.width - distInShelf;
                                } else {
                                    distInShelf = 0;
                                    distFromStart = 0;
                                    spaceInShelf = 0;
                                }
                                index = 0;
                            } else
                                // this shelf dosn't exist in the arry so we stay in this shelf
                                shelfId--;
                            console.log('startOfShelfId', startOfShelfId, 'shelfId', shelfId);
                        }
                        item.id = startOfShelfId + 'I' + index;
                        index++;
                        item.placement.distFromStart = distFromStart;
                        let itemWidth = viewCombinedShelves[i].items[n].placement.pWidth;
                        distFromStart += (itemWidth != undefined ? itemWidth : ProductDefaultDimensions.width) * item.placement.faces;
                        distInShelf += (itemWidth != undefined ? itemWidth : ProductDefaultDimensions.width) * item.placement.faces;
                        if (viewCombinedShelves[shelfId]) {
                            spaceInShelf = viewCombinedShelves[shelfId].dimensions.width - distInShelf;
                            combinedShelves[shelfId].items.push(item);
                        }
                        console.log('--- item', // viewCombinedShelves[i].items[n], 'new item',
                            item, 'distInShelf', distInShelf, 'spaceInShelf', spaceInShelf);
                    }
                }
            }

            /*   Add saving array for aisle, allowing to do 
                Ctrl+Z to move back and Ctrl+Y to move forward *
                await this.saveSnapShot(shelf);
            *************************************************************/
            await this.saveAisleShelf(shelf);
        }
    }
    addShelfItem = async (shelf: PlanogramShelf, newItem: PlanogramItem, shlfStart?: number, newItems?: PlanogramItem[]) => {
        // make sure shelf has no undefined items
        let initItems = JSON.parse(JSON.stringify(shelf.items));
        shelf.items = [];
        for (let i = 0; i < initItems.length; i++) {
            if (initItems[i] && typeof initItems[i] != 'undefined')
                shelf.items.push(initItems[i]);
        }
        if (newItems) {
            initItems = JSON.parse(JSON.stringify(newItems));
            newItems = [];
            for (let i = 0; i < initItems.length; i++) {
                if (initItems[i] && typeof initItems[i] != 'undefined') {
                    initItems[i].placement.distFromStart = i;
                    newItems.push(initItems[i]);
                }
            }
        }
        const { virtualStore } = this.props;
        const shelvesDetails = virtualStore.shelfDetails;
        const aisleShelves = virtualStore.shelfMap;

        // update shelf.distFromStart if shlfStart was sent
        if (shlfStart && shlfStart >= 0) {
            shelf.distFromStart = shlfStart;
            /***************************/
            let aisles_1 = this.props.store ? this.props.store.aisles : undefined;
            let aisleId_1 = shelf.id.substring(0, shelf.id.indexOf('SE'));
            let aisle_1: PlanogramAisle = JSON.parse(JSON.stringify(blankPlanogramAisle));
            if (aisles_1) {
                for (let i = 0; i < aisles_1.length; i++) {
                    if (aisles_1[i].id === aisleId_1) {
                        aisle_1 = aisles_1[i];
                        let seI = parseInt(shelf.id.substring(shelf.id.indexOf('SE') + 2, shelf.id.indexOf('SH')));
                        let shI = parseInt(shelf.id.substring(shelf.id.indexOf('SH') + 2));
                        if (aisle_1.sections[seI] && aisle_1.sections[seI].shelves[shI])
                            aisle_1.sections[seI].shelves[shI] = shelf;
                        break;
                    }
                }
            }
            if (aisle_1.id === aisleId_1) {
                await this.props.setStoreAisle(aisle_1);
            }
            /***************************/
        }
        // make sure, in case no shlfStart was sent that the shelf.distFromStart fits with the first Item (ordered by distFromStart) on that shelf
        else {
            if (shelf.distFromStart > 0) {
                let num: number = shelf.items[0] && shelf.items[0].placement.distFromStart ? shelf.items[0].placement.distFromStart : 0;
                if (shelf.items.length >= 1 && num >= 0) shelf.distFromStart = num;
            }
        }

        // check if this shelf even has room to add a new item;
        if (shelf.distFromStart === shelf.dimensions.width && newItem && newItem.product > 0 && shelf.freezer != 1 && shelf.freezer != 2 && (newItem.shelfLevel === 1 || newItem.shelfLevel === undefined)) {
            // if there is a next shelf activate function with the rest of the items 
            let fullShelf = JSON.parse(JSON.stringify(shelf));
            // this is the data for the actual shelf
            let shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[shelf.id]));

            // get the full combined shelves
            while (shelfDetails.main_shelf) {
                let ids = JSON.parse(JSON.stringify(shelfDetails.main_shelf));
                shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[ids]));
                fullShelf = aisleShelves[ids];
            }
            let { combined } = shelfDetails;
            if (combined == null) combined = [];
            let combinedShelves = [fullShelf].concat(combined.map((id: any) => aisleShelves[id]));
            // find the index of the current shelf 
            let currI = combinedShelves.findIndex(object => object.id === shelf.id);
            if (currI >= 0 && combinedShelves[currI + 1]) {
                // move item/s to the next shelf
                await this.addShelfItem(JSON.parse(JSON.stringify(combinedShelves[currI + 1])), newItem);
            }
            return;
        }
        // if there is a new item add it at the end of the shelf
        if (newItem && newItem.product > 0) {
            /* Barak 26.11.20 - add special numbering for items in shelf.freezer === 2 */
            if (shelf.freezer === 2
                /* && newItem.placement.distFromTop != shelf.dimensions.height - ((newItem.placement.pHeight ? newItem.placement.pHeight : ProductDefaultDimensions.height) * newItem.placement.stack) */
            ) {
                if (newItem.id === '') {
                    if (shelf.items.length > 1) {
                        let lastI = parseInt(shelf.items[shelf.items.length - 1].id.substring(shelf.items[shelf.items.length - 1].id.indexOf('I') + 1));
                        let newI = 0;
                        if (lastI < 100) newI = lastI + 100;
                        else newI = lastI + 1;
                        newItem.id = shelf.id + "I" + newI;
                    } else {
                        newItem.id = shelf.id + "I" + 100;
                    }
                }
            }
            /***************************************************************************/
            /* Barak 21.12.20 - add special numbering for items in shelfLevel > 1 */
            if (newItem.shelfLevel > 1) {
                if (newItem.id === '') {
                    if (shelf.items.length > 1) {
                        let lastI = parseInt(shelf.items[shelf.items.length - 1].id.substring(shelf.items[shelf.items.length - 1].id.indexOf('I') + 1));
                        let newI = 0;
                        if (lastI < 100) newI = lastI + 100;
                        else newI = lastI + 1;
                        newItem.id = shelf.id + "I" + newI;
                    } else {
                        newItem.id = shelf.id + "I" + 100;
                    }
                }
                // now that we have an id to the new item we can save it in the linked bottom level item as linkedItemUp
                let sItem = shelf.items.filter(line => line.id === this.props.multiSelectId[0]);
                if (sItem && sItem[0]) sItem[0].linkedItemUp = newItem.id;
            }
            /************************************************************************/
            shelf.items.push(newItem);
        }
        // if there are multiple items to add to the begining of the shelf add them
        if (newItems && newItems.length > 0) {
            // push all items on the shelf by 1 to allow to push new items at the begining of the shelf
            for (let i = 0; i < shelf.items.length; i++) {
                let itemDist = 0;
                if (shelf.items[i].placement.distFromStart != undefined)
                    itemDist = parseInt(JSON.stringify(shelf.items[i].placement.distFromStart));
                // if (shlfStart) itemDist += shlfStart;
                // shelf.items[i].placement.distFromStart = itemDist + newItems.length;
                shelf.items[i].placement.distFromStart = itemDist + 1;
            }
            // add the new items at the begining of the shelf items array
            for (let i = newItems.length - 1; i >= 0; i--) {
                if (newItems[i]) shelf.items.unshift(newItems[i]);
            }
            // go over all the items on the shelf and re-do their distance from start of shelf
            for (let i = 0; i < shelf.items.length; i++) {
                if (shelf.items[i]) {
                    let distToNextItem = 0;
                    let itemEndPoint = parseInt(JSON.stringify(shelf.items[i].placement.distFromStart)) + (shelf.items[i].placement.faces * parseInt(JSON.stringify(shelf.items[i].placement.pWidth)));
                    if (shelf.items[i + 1] && (i + 1 <= newItems.length || i > newItems.length)) distToNextItem = parseInt(JSON.stringify(shelf.items[i + 1].placement.distFromStart)) - itemEndPoint;
                    if (i === 0 && shlfStart != undefined) {
                        shelf.items[0].placement.distFromStart = shlfStart;
                        itemEndPoint += shlfStart;
                    }
                    if (shelf.items[i + 1]) shelf.items[i + 1].placement.distFromStart = itemEndPoint + (distToNextItem > 0 ? distToNextItem : 0);
                }
            }
        }

        /* Barak 26.11.20 - item distFromTop on a freezer shelf */
        if (shelf.freezer === 1 || shelf.freezer === 2) {
            // /* Barak 22.11.20 */ await this.saveSnapShot(shelf);
            /* Barak 22.11.20 */await this.saveAisleShelf(shelf);
            return;
        }
        /********************************************************/

        // save initial state of shelf with items ordered by distFromStart (distance from start of shelf)
        shelf.items.sort(function (a: any, b: any) {
            let aNumber = a.placement.distFromStart ? a.placement.distFromStart : 0;
            let bNumber = b.placement.distFromStart ? b.placement.distFromStart : 0;
            let aId = a.Id;
            let bId = b.Id;
            return (aNumber > bNumber) ? 1 : (aNumber < bNumber) ? -1 : ((aId > bId) ? 1 : (aId < bId) ? -1 : 0);
        });

        // go over shelf and if there are 2 items occupying the same space and they are the same product 
        // - increase faces to the first and remove the second
        for (let i = 0; i < shelf.items.length; i++) {
            if (shelf.items[i]) {
                if (parseInt(JSON.stringify(shelf.items[i].placement.distFromStart)) < shelf.distFromStart)
                    shelf.items[i].placement.distFromStart = shelf.distFromStart;
                if (shelf.items[i + 1] && (shelf.items[i + 1].shelfLevel === 1 || shelf.items[i + 1].shelfLevel === undefined)) {
                    let first_dist: number = parseInt(JSON.stringify(shelf.items[i].placement.distFromStart));
                    let first_end = first_dist + (parseInt(JSON.stringify(shelf.items[i].placement.pWidth)) * shelf.items[i].placement.faces);
                    let second_dist: number = parseInt(JSON.stringify(shelf.items[i + 1].placement.distFromStart));
                    let second_end = second_dist + (parseInt(JSON.stringify(shelf.items[i + 1].placement.pWidth)) * shelf.items[i + 1].placement.faces);
                    if (((second_dist <= first_dist && second_end <= first_end) || (second_dist >= first_dist && second_end <= first_end) || (second_dist >= first_dist && second_end > first_end))
                        && shelf.items[i].product === shelf.items[i + 1].product) {
                        // same item - increase faces
                        shelf.items[i].placement.faces += shelf.items[i + 1].placement.faces;
                        // remove second item 
                        shelf.items.splice(i + 1, 1);
                    }
                }
            }
        }

        let viewShelf = JSON.parse(JSON.stringify(shelf));
        // empty all items from current shelf
        shelf.items = [];

        let distFromStart = viewShelf.items.length > 0 && viewShelf.items[0] ? viewShelf.items[0].placement.distFromStart : 0;
        let distInShelf = viewShelf.items.length > 0 && viewShelf.items[0] ? viewShelf.items[0].placement.distFromStart : 0;
        let index = 0;
        let startOfShelfId = viewShelf.id;
        let spaceInShelf = 0;
        // go over the initial state and populate each shelf in combinedShelves with the correct items in the correct order
        for (let n = 0; n < viewShelf.items.length; n++) {
            let item = JSON.parse(JSON.stringify(viewShelf.items[n]));
            /* Barak 26.11.20 - return items above main line (item index >= 100) back into the shelf without altering anything else */
            let itemInd = parseInt(item.id.substring(item.id.indexOf('I') + 1));
            if (itemInd >= 100) shelf.items.push(item);
            else {
                /************************************************************************************************************************/
                let nextItem = null;
                if (viewShelf.items[n + 1]) nextItem = viewShelf.items[n + 1];
                // console.log('item', item, 'itemExists', itemExists, 'nextItem', nextItem);
                if (spaceInShelf < 0) {
                    // if there is a next shelf activate function with the rest of the items 
                    let fullShelf = JSON.parse(JSON.stringify(shelf));
                    // this is the data for the actual shelf
                    let shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[shelf.id]));

                    // get the full combined shelves
                    while (shelfDetails.main_shelf) {
                        let ids = JSON.parse(JSON.stringify(shelfDetails.main_shelf));
                        shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[ids]));
                        fullShelf = aisleShelves[ids];
                    }
                    let { combined } = shelfDetails;
                    if (combined == null) combined = [];
                    let combinedShelves = [fullShelf].concat(combined.map((id: any) => aisleShelves[id]));
                    // find the index of the current shelf 
                    let currI = combinedShelves.findIndex(object => object.id === viewShelf.id);
                    if (currI >= 0 && combinedShelves[currI + 1]) {
                        while (combinedShelves[currI + 1] && spaceInShelf * -1 > combinedShelves[currI + 1].dimensions.width) {
                            spaceInShelf += combinedShelves[currI + 1].dimensions.width;
                            await this.addShelfItem(JSON.parse(JSON.stringify(combinedShelves[currI + 1])), blankItem, combinedShelves[currI + 1].dimensions.width, []);
                            currI++;
                        }
                        let newItemsToSend: PlanogramItem[] = [];
                        if (n < viewShelf.items.length) {
                            // get all items left over into a new array
                            for (let i = n; i < viewShelf.items.length; i++) {
                                newItemsToSend.push(viewShelf.items[i]);
                            }
                            // // remove the items we're passing to the next shelf from the array of this shelf
                            // viewShelf.items.splice(n, viewShelf.items.length - n);
                        }
                        await this.addShelfItem(JSON.parse(JSON.stringify(combinedShelves[currI + 1])), blankItem, (spaceInShelf * -1), newItemsToSend);
                        spaceInShelf = 0;
                        // leave the loop since there are no more items in this shelf to take care of  
                        //  - all the left over items are moved to the next shelf
                        break;
                    }
                }
                if (item.placement.distFromStart > distFromStart && (startOfShelfId === item.id.substring(0, item.id.indexOf('I')) || item.id === ''))
                    distFromStart = item.placement.distFromStart;
                item.id = startOfShelfId + 'I' + index;
                index++;
                item.placement.distFromStart = distFromStart;
                let itemWidth = viewShelf.items[n].placement.pWidth;
                let itemSpace = (itemWidth != undefined ? itemWidth : ProductDefaultDimensions.width) * item.placement.faces;
                distFromStart += itemSpace;
                distInShelf += itemSpace;
                spaceInShelf = viewShelf.dimensions.width - distInShelf;
                // add item back into the shelf
                shelf.items.push(item);


                // check if there is a next shelf 
                let fullShelf = JSON.parse(JSON.stringify(shelf));
                // this is the data for the actual shelf
                let shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[shelf.id]));
                // get the full combined shelves
                while (shelfDetails.main_shelf) {
                    let ids = JSON.parse(JSON.stringify(shelfDetails.main_shelf));
                    shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[ids]));
                    fullShelf = aisleShelves[ids];
                }
                let { combined } = shelfDetails;
                if (combined == null) combined = [];
                let combinedShelves = [fullShelf].concat(combined.map((id: any) => aisleShelves[id]));
                // find the index of the current shelf 
                let currI = combinedShelves.findIndex(object => object.id === viewShelf.id);


                if (distFromStart > viewShelf.dimensions.width && currI >= 0 && combinedShelves[currI + 1]) {
                    // only of there is a next shelf can we reduce the viewShelf.dimensions.width from the next distFromStart 
                    spaceInShelf = (distFromStart - viewShelf.dimensions.width) * -1;
                    distFromStart = distFromStart - viewShelf.dimensions.width;
                }
            }
        }
        // in case the last item on the shelf is the one that extends into the next shelf, 
        // after we're done going over all items we check if the spaceInShelf has extended byond the current shelf
        if (spaceInShelf < 0) {
            // if there is a next shelf activate function to update the next shelf's distFromStart 
            let fullShelf = JSON.parse(JSON.stringify(shelf));
            // this is the data for the actual shelf
            let shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[shelf.id]));

            // get the full combined shelves
            while (shelfDetails.main_shelf) {
                let ids = JSON.parse(JSON.stringify(shelfDetails.main_shelf));
                shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[ids]));
                fullShelf = aisleShelves[ids];
            }
            let { combined } = shelfDetails;
            if (combined == null) combined = [];
            let combinedShelves = [fullShelf].concat(combined.map((id: any) => aisleShelves[id]));
            // find the index of the current shelf 
            let currI = combinedShelves.findIndex(object => object.id === viewShelf.id);
            if (currI >= 0 && combinedShelves[currI + 1]) {
                while (combinedShelves[currI + 1] && spaceInShelf * -1 > combinedShelves[currI + 1].dimensions.width) {
                    spaceInShelf += combinedShelves[currI + 1].dimensions.width;
                    await this.addShelfItem(JSON.parse(JSON.stringify(combinedShelves[currI + 1])), blankItem, combinedShelves[currI + 1].dimensions.width, []);
                    currI++;
                }
                let newItemsToSend: PlanogramItem[] = [];
                await this.addShelfItem(JSON.parse(JSON.stringify(combinedShelves[currI + 1])), blankItem, (spaceInShelf * -1), newItemsToSend);
            }
        }

        /* Barak 21.12.20 - verify items with shelfLevel 2 position */
        for (let b = 0; b < shelf.items.length; b++) {
            if (shelf.items[b].linkedItemUp) {
                let upItem = shelf.items.filter(line => line.id === shelf.items[b].linkedItemUp);
                if (upItem && upItem[0]) {
                    upItem[0].placement.distFromStart = shelf.items[b].placement.distFromStart;
                    upItem[0].linkedItemDown = shelf.items[b].id;
                    upItem[0].id = shelf.items[b].id.substring(0, shelf.items[b].id.indexOf('I')) + 'I' + upItem[0].id.substring(upItem[0].id.indexOf('I') + 1);
                    shelf.items[b].linkedItemUp = upItem[0].id;
                }
                console.log('linkedItemUp', shelf.items[b], upItem[0], shelf);
            }
        }
        /************************************************************/

        /* Barak 22.11.20 - Add saving array for aisle, allowing to do 
            Ctrl+Z to move back and Ctrl+Y to move forward *
            await this.saveSnapShot(shelf);
        *************************************************************/
      await this.saveAisleShelf(shelf);
    }
    saveSnapShotAisle = async (thisAisle: PlanogramAisle) => {
        let tArray = this.props.aisleSaveArray;
        let tIndex = this.props.aisleSaveIndex;
        if (tArray.length === 0 || (tArray.length > 0 && tArray[0].id != thisAisle.id)) {
            let newAisle = await planogramProvider.getStoreAisle(this.props.store ? this.props.store.store_id : 0, thisAisle.aisle_id);
            tArray = [JSON.parse(JSON.stringify(newAisle))];
            tIndex = 0;
            await this.props.setAisleSaveArray(tArray);
            await this.props.setAisleSaveIndex(tIndex);
        }
        let aisle: PlanogramAisle = JSON.parse(JSON.stringify(blankPlanogramAisle));
        let aisles = this.props.store ? this.props.store.aisles : undefined;
        let aisleId = thisAisle.id;
        if (aisles) {
            for (let i = 0; i < aisles.length; i++) {
                if (aisles[i].id === aisleId) {
                    aisle = aisles[i];
                    break;
                }
            }
        }
        if (aisle.id === aisleId) {
            // before we save the new aisle we'll also add it to the aisleSaveArray
            // let tArray = this.props.aisleSaveArray;
            tIndex++;
            // insert into array tArray item aisle at location tIndex, deleting 0 items first
            tArray.splice(tIndex, 0, JSON.parse(JSON.stringify(aisle)));
            if (tArray.length > tIndex + 1) {
                tArray.length = tIndex + 1;
            }
            // tArray.push(JSON.parse(JSON.stringify(aisle)));
            await this.props.setAisleSaveArray(tArray);
            await this.props.setAisleSaveIndex(tIndex);
            await this.props.setStoreAisle(aisle);
        }
    }
    saveAisleShelf = async (shelf: PlanogramShelf) => {
        let aisle: PlanogramAisle = JSON.parse(JSON.stringify(blankPlanogramAisle));
        let aisles = this.props.store ? this.props.store.aisles : undefined;
        let aisleId = shelf.id.substring(0, shelf.id.indexOf('SE'));
        if (aisles) {
            for (let i = 0; i < aisles.length; i++) {
                if (aisles[i].id === aisleId) {
                    aisle = aisles[i];
                    let seI = parseInt(shelf.id.substring(shelf.id.indexOf('SE') + 2, shelf.id.indexOf('SH')));
                    let shI = parseInt(shelf.id.substring(shelf.id.indexOf('SH') + 2));
                    if (aisle.sections[seI] && aisle.sections[seI].shelves[shI]) {
                        aisle.sections[seI].shelves[shI] = shelf;
                    }
                    break;
                }
            }
        }
        if (aisle.id === aisleId) {
            await this.props.setStoreAisle(aisle);
            await this.checkLevel2(aisle);
        }
    }
    checkLevel2 = async (aisle: PlanogramAisle) => {
        let combinedItems: any[] = [];
        for (let a = 0; a < aisle.sections.length; a++) {
            for (let b = 0; b < aisle.sections[a].shelves.length; b++) {
                combinedItems = combinedItems.concat(aisle.sections[a].shelves[b].items);
            }
        }
        for (let c = 0; c < combinedItems.length; c++) {
            if (combinedItems[c].linkedItemUp) {
                let upItem = combinedItems.filter((line: any) => line.id === combinedItems[c].linkedItemUp);
                if (upItem && upItem[0]) {
                    let itemToMove = JSON.parse(JSON.stringify(upItem[0]));
                    // find location of item to move
                    let moveSec = parseInt(itemToMove.id.substring(itemToMove.id.indexOf('SE') + 2, itemToMove.id.indexOf('SH')));
                    let moveSh = parseInt(itemToMove.id.substring(itemToMove.id.indexOf('SH') + 2, itemToMove.id.indexOf('I')));
                    let index = aisle.sections[moveSec].shelves[moveSh].items.findIndex(line => line.id === itemToMove.id);
                    // remove this item from original shelf
                    aisle.sections[moveSec].shelves[moveSh].items.splice(index, 1);
                    // update itemToMove properties
                    itemToMove.placement.distFromStart = combinedItems[c].placement.distFromStart;
                    itemToMove.linkedItemDown = combinedItems[c].id;
                    // itemToMove.id = combinedItems[c].id.substring(0, combinedItems[c].id.indexOf('I')) + 'I' + itemToMove.id.substring(upItem[0].id.indexOf('I') + 1);
                    itemToMove.id = ''
                    // find new location of item and create new item id
                    let destinationSec = parseInt(combinedItems[c].id.substring(combinedItems[c].id.indexOf('SE') + 2, combinedItems[c].id.indexOf('SH')));
                    let destinationSh = parseInt(combinedItems[c].id.substring(combinedItems[c].id.indexOf('SH') + 2, combinedItems[c].id.indexOf('I')));
                    // console.log('checkLevel2 destinationSec',destinationSec,'destinationSh',destinationSh);
                    if (aisle.sections[destinationSec].shelves[destinationSh].items.length > 1) {
                        let lastI = parseInt(aisle.sections[destinationSec].shelves[destinationSh].items[aisle.sections[destinationSec].shelves[destinationSh].items.length - 1].id.substring(aisle.sections[destinationSec].shelves[destinationSh].items[aisle.sections[destinationSec].shelves[destinationSh].items.length - 1].id.indexOf('I') + 1));
                        let newI = 0;
                        if (lastI < 100) newI = lastI + 100;
                        else newI = lastI + 1;
                        itemToMove.id = aisle.sections[destinationSec].shelves[destinationSh].id + "I" + newI;
                        // console.log('checkLevel2 lastI',lastI,'newI',newI,'itemToMove.id',itemToMove.id);
                    } else {
                        itemToMove.id = aisle.sections[destinationSec].shelves[destinationSh].id + "I" + 100;
                    }
                    // add itemToMove to its new location
                    // console.log('checkLevel2 push',itemToMove);
                    aisle.sections[destinationSec].shelves[destinationSh].items.push(itemToMove);
                    // update the linkedItemUp with the new item id 
                    combinedItems[c].linkedItemUp = itemToMove.id;
                }
                else {
                    combinedItems[c].linkedItemUp = null;
                }
            }
        }
        await this.props.setStoreAisle(aisle);
    }
    getEmptyAisle = async (aisleId: number) => {
        this.setState({ isLoading: true });
        if ( !this.state.isLoading ) {
        let newAisle = await planogramApi.fetchPlanogramAisle(this.props.store ? this.props.store.store_id : '', aisleId, this.props.user ? this.props.user.db : '');
        if (this.props.store) {
            let store = this.props.store;
            for (let i = 0; i < store.aisles.length; i++) {
                if (store.aisles[i] != undefined && newAisle != undefined && store.aisles[i].aisle_id === newAisle.aisle_id) {
                    store.aisles[i] = newAisle;
                    await this.props.setStore(store);
                    break;
                }
            }
        }
        }
        this.setState({ isLoading: false });
    }
    render() {
        const {
            store,
            productDetailerState,
            hideProductDisplayerBarcode,
        } = this.props;
        let leftSearchBar = false;

        const url = window.location.href;
        const urlElements = url.split('/');
        let currentSecondAisleIndex = parseInt(urlElements[9]);
        if (store == null) return null;
        return (
            <div className="planogram-view height-100-vh full-width-vw"
                onClick={(e) => {
                    e.stopPropagation();
                    this.props.setCurrentSelectedId('');
                    this.props.setMultiSelectId([]);

                    if (productDetailerState)
                        hideProductDisplayerBarcode();
                    if (!leftSearchBar)
                         hideProductDisplayerBarcode();
                }}
                onMouseOver={(e) => {
                    this.bindKeyboard();
                }}
                onMouseOut={(e) => {
                    this.unBindKeyboard();
                }}
            >
                <Switch>
                    <Route path={[`${this.props.match.url}/:aisle_index`,`${this.props.match.url}/:aisle_index/:second_aisle_index`]} component={PlanogramAisleComponent} />
                    <Route render={(props) => (<Redirect to={`${this.props.match.url}${store.aisles[0] ? "/" + store.aisles[0].aisle_id : ""}`} />)} />
                </Switch>
            </div>
        )
    }
}

export const PlanogramStoreComponent = withRouter(connect(mapStateToProps, mapDispatchToProps)(PlanogramStoreComponentContainer));