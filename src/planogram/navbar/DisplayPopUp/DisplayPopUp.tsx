import * as React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import classnames from "classnames"
import { BlankPopUp } from '@components';
import { Spinner, CustomInput } from 'reactstrap'
import { AppState } from '@src/planogram/shared/store/app.reducer';
import { FormattedMessage, useIntl } from "react-intl"
import { NEWPLANO_ACTION } from "@src/planogram/shared/store/newPlano/newPlano.types";


interface Props {
    setDisplayPopUp: any
    setDisplayOptions: any
    displayOptions: any
}

export const DisplayPopUp: React.FC<Props> = (props: Props) => {
    const { setDisplayPopUp, displayOptions, setDisplayOptions } = props
    const dispatch = useDispatch();
    const { formatMessage } = useIntl();
    const displayOptionsState = useSelector((state: AppState) => state.displayOptions)


    const changeToggle = (id: any) => {
        const newOptions = displayOptions
        newOptions[String(id)] = !displayOptions[String(id)]
        setDisplayOptions([])
        setTimeout(() => { setDisplayOptions(newOptions) }, 1);

        dispatch({
            type: NEWPLANO_ACTION.SET_NEWPLANO_DISPLAY_OPTIONS,
            payload: newOptions
        });
    }
    return (
        <div className='position-absolute position-left-0'>
            <BlankPopUp noXbutton
                onOutsideClick={setDisplayPopUp}
                style={{ width: '13rem', height: '16rem', top: '1.5rem' }}>
                <div className='d-flex-column width-80-per m-auto-t-1-5 text-black font-bold-400'>
                    {[{ title: formatMessage({ id: 'hideProducts' }), id: 'hideProducts' }, { title: formatMessage({ id: 'hideTags' }), id: 'hideTags' }
                        , { title: formatMessage({ id: 'showProdutDepth' }), id: 'showProdutDepth' }, { title: formatMessage({ id: 'productWithoutSize' }), id: 'productWithoutSize' }]
                        .map((item: any) => {
                            return (
                                <div className='d-flex justify-content-between mb-1-5'>
                                    <span>{item.title}</span>
                                    <CustomInput checked={displayOptionsState && displayOptions && displayOptions[item.id] ? true : false} bsSize='sm' onClick={() => changeToggle(item.id)} type="switch" id={item.id} name={item.title} />
                                </div>
                            )
                        })}
                </div>
            </BlankPopUp>
        </div>
    )
};

export default DisplayPopUp;
