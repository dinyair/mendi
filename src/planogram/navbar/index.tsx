import * as React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { AppState } from '@src/planogram/shared/store/app.reducer';
import { getEmptyAisle, NextSavedStore, PreviousSavedStore, SaveAisle } from '@src/helpers/planogram';
import { editShelfItemAction } from "@src/planogram/shared/store/planogram/store/item/item.actions";
import { FormattedMessage, useIntl } from "react-intl"
import { ItemDefualtPlacement } from "@src/planogram/shared/store/planogram/planogram.defaults";
import { config } from '@src/config'
import { Icon, Popup } from '@components';
import classnames from "classnames"
import { setStore } from '@src/planogram/shared/store/planogram/store/store.actions';
import { catalogProductDimensionObject } from "@src/planogram/provider/planogram.service";
import { TypedDragItem } from '@src/planogram/generic/TypedDropZone';
import { PlanogramDragDropTypes } from '@src/planogram/generic/DragAndDropType';
import { setDisplayAisle } from '@src/planogram/shared/store/planogram/planogram.actions';
import { useHistory, useParams } from 'react-router-dom';
import { setZoom } from '@src/planogram/shared/store/zoom/zoom.actions';
import { setAisleSaveIndex } from "@src/planogram/shared/store/planogram/planogram.actions";
import { setAisleAction } from "@src/planogram/shared/store/planogram/store/aisle/aisle.actions";
import { Spinner } from 'reactstrap'
import { PLANOGRAM_BASE_URL } from '@src/PlanoApp';
import { NEWPLANO_ACTION } from "@src/planogram/shared/store/newPlano/newPlano.types";
import * as planogramProvider from "@src/planogram/shared/api/planogram.provider";
import { ColorReport } from './ColorReport/ColorReport'
import { DisplayPopUp } from './DisplayPopUp/DisplayPopUp'
import * as planogramApi from '@src/planogram/shared/api/planogram.provider';
import { setCurrentSelectedId, hideProductDetailer, setMultiSelectId } from '@src/planogram/shared/store/planogram/planogram.actions';
import * as _html2canvas from "html2canvas";
const html2canvas: any = _html2canvas;
interface Props {
	readonly newPlano: any;
	readonly isSaving: boolean
}


export const Navbar: React.FC<Props> = (props: Props) => {
	const { newPlano, isSaving } = props
	const { formatMessage } = useIntl();

	const dispatch = useDispatch();
	const url = window.location.href;
	const urlElements = url.split('/');
	let currentType = urlElements[4];
	let currentPageType = urlElements[5];
	let currentBranchId = parseInt(urlElements[6]);
	let layoutStoreId = parseInt(urlElements[7]);
	let currentAisleIndex = parseInt(urlElements[8]);
	const [spinner, setSpinner] = React.useState<boolean>(false);
	const [dataImage, setDataImage] = React.useState<any>(null);
	const Zoom = useSelector((state: AppState) => state.Zoom)
	const currentSelectedId = useSelector((state: AppState) => state.planogram.display.currentSelectedId)
	const productsMap = useSelector((state: AppState) => state.newPlano ? state.newPlano.productsMap : null)
	const aisle: any = useSelector((state: AppState) => state.planogram && state.planogram.store && state.planogram.store.aisles ? state.planogram.store.aisles.filter((aisle: any) => aisle.id === currentSelectedId.split('SE')[0]) : null)
	const aisleData: any = useSelector((state: AppState) => state.planogram && state.planogram.store && state.planogram.store.aisles ? state.planogram.store.aisles.filter((aisle: any) => aisle.aisle_id === currentAisleIndex) : null)
	let section = aisle && aisle[0] && aisle[0].sections ? aisle[0].sections.filter((section: any) => section.id === currentSelectedId.split('SH')[0]) : null
	let shelf = section && section[0] && section[0].shelves ? section[0].shelves.filter((shelf: any) => shelf.id === currentSelectedId.split('I')[0]) : null
	let item = shelf && shelf[0] && shelf[0].items ? shelf[0].items.filter((item: any) => item.id === currentSelectedId)[0] : null

	const productDimensions = item && productsMap[item.product] ? catalogProductDimensionObject(productsMap[item.product]) : null;

	const displayOptionsState = useSelector((state: AppState) => state.displayOptions)
	const colorReports = useSelector((state: AppState) => state.colorReports)
	const history = useHistory();
	const params: any = useParams()
	const aisleSaveArray = useSelector((state: AppState) => state.planogram.display.aisleSaveArray)
	const aisleSaveIndex = useSelector((state: AppState) => state.planogram.display.aisleSaveIndex)
	const [isAislesPopupOpen, setIsAislesPopupOpen] = React.useState<boolean>(false);
	const [isRotatePopupOpen, setIsRotatePopupOpen] = React.useState<boolean>(false);
	const store = useSelector((state: AppState) => state.planogram.store)
	let currentAisle: any = store && store.aisles && store.aisles.length && currentAisleIndex ? store.aisles.filter((a: any) => a.aisle_id === currentAisleIndex)[0] : undefined
	const user = useSelector((state: AppState) => state.auth.user)
	const Divider = (<div className="width-01vw height-100-per py-2 bg-dark-gray mx-1" />)
	const [displayPopUp, setDisplayPopUp] = React.useState<boolean>(false)
	const [displayOptions, setDisplayOptions] = React.useState<any>(displayOptionsState ? displayOptionsState : { showProdutDepth: false, hideProducts: false, hideTags: false, productWithoutSize: false })
	const [displayColorReport, setDisplayColorReport] = React.useState<boolean>(false)
	const [colorReportMerged, setColorReportMerged] = React.useState<any>(colorReports && colorReports.merged ? [colorReports.merged] : [])

	const setFieldsByGroup = (group: string) => {
		let fieldName = ''
		switch (group) {
			case 'Supplier': fieldName = 'supplierWName'; break;
			case 'Segment': fieldName = 'segmentName'; break;
			case 'Brand': fieldName = 'brandName'; break;
			case 'SubgroupName': fieldName = 'subgroupWName'; break;
			default: fieldName = 'noField'; break;
		}
		return [{
			text: "color", type: 'color', width: window.innerWidth * 0.2, minWidth: 85, cellRenderer: 'ColorCell',
		},
		{
			text: fieldName, type: 'string', width: window.innerWidth * 0.2, minWidth: 85,
		},
		{
			text: "calculated", type: 'percentage', width: window.innerWidth * 0.2, minWidth: 85,
		},
		{
			text: "ground", type: 'percentage', width: window.innerWidth * 0.2, minWidth: 85,
		},]
	}
	const [isColorReportDataReady, setIsColorReportDataReady] = React.useState<boolean>(colorReports && colorReports.merged ? true : false)
	const [colorRows, setColorRows] = React.useState<any[]>(colorReports && colorReports.rows ? colorReports.rows : [])
	const [colorFields, setColorFields] = React.useState<any>(colorReports && colorReports.merged ? setFieldsByGroup(colorReports.merged.field) : [])

	const customizer = { direction: document.getElementsByTagName("html")[0].dir }
	const NavButton = (icon: string, text: string, classnames?: string | null, iconClassName?: string) => {
		return (
			<div id={`${text}_btn`} className={`d-flex-column align-item-center justify-content-center py-05 px-1 border-radius-05rem cursor-pointer text-gray bg-opacity ${classnames}`}>
				<div className="d-flex justify-content-center">
					<Icon src={`../../${config.iconsPath}planogram/${icon}.svg`}
						className={iconClassName} style={{ height: '0.35rem', width: '0.35rem' }} />
				</div>
				<div><FormattedMessage id={text} /></div>
			</div>
		)
	}


	return (<>
		<div id='planogram-navbar' className="zindex-10002 position-fixed position-left-0 position-top-0 height-5-rem width-100-per navbar-light d-flex align-items-center">
			<div className="d-flex margin-right-auto">
				<div className="px-1 d-flex align-items-center">

					<div className="cursor-pointer"
						onClick={() => history.push('/')}>
						<img src={'../../' + config.imagesPath + "/planogram/logo-icon.png"} style={{ height: '1.5rem', width: '1.5rem' }} />
					</div>
				</div>
				<div className="px-1 d-flex align-items-center">

					<div onClick={async () => {
						await dispatch(setStore(null));

						dispatch({
							type: NEWPLANO_ACTION.SET_NEWPLANO,
							payload: null
						});

						history.push(PLANOGRAM_BASE_URL + '/' + (currentType === 'layout' ? 'layouts' : 'branches') + '/' + currentBranchId + '/' + layoutStoreId);

					}} >
						<div className="cursor-pointer d-flex align-items-center ">
							<Icon className={classnames("svg-white", {
								"rotate-180": customizer.direction == 'ltr',
							})} src={'../../' + config.iconsPath + "navbar/arrow-right.svg"}
								style={{ height: '1.5rem', width: '0.5rem' }} />
						</div>
					</div>
				</div>
				<div className="position-relative">
					<div
						onClick={() => setIsAislesPopupOpen(!isAislesPopupOpen)}
						className={classnames("d-flex justify-content-between border-turquoise align-items-center  font-small-3 cursor-pointer py-05 border-2 px-1 elipsis width-10-rem max-width-10-rem  text-bold-700  mr-05 border-radius-2rem", {
							"bg-turquoise": currentAisle,
							"text-white": currentAisle,
							"text-turquoise": !currentAisle
						})}>
						<div>{currentAisle ? currentAisle.name : <><FormattedMessage id="choose" /> <FormattedMessage id="aisle" /></>}</div>
						{currentAisle && currentAisle.name ?
							<Icon src={`../../${config.iconsPath}general/arrow-down-white.svg`}
								className="mr-05" style={{ height: '0.5rem', width: '0.5rem' }} />
							:
							<Icon src={`../../${config.iconsPath}general/arrow-down.svg`}
								className="mr-05" style={{ height: '0.5rem', width: '0.5rem' }} />}
					</div>
					<Popup
						varient="navLinks"
						className="width-10-rem mt-05 height-30-vh square text-black d-flex justify-content-center"
						// style={store && store.aisles ? {height:`${3*5}vh`}: {height: '10vh'}}
						// style={{height:'20vh'}}
						multiple
						divider
						// search
						// add={currentType === 'layout' ? {
						// 	text: formatMessage({ id: 'add-new-aisle' }), onClick: async () => {
						// 		setBlockNewAisleButton(true)
						// 		setTimeout(() => {
						// 			setBlockNewAisleButton(false)
						// 		}, 1000);
						// 		if (!blockNewAisleButton) {
						// 			let newAisle: any = await planogramProvider.createStoreAisle(layoutStoreId);
						// 			if (newAisle) {
						// 				dispatch(aisleActions.addAisleAction(newAisle))
						// 			}
						// 		}
						// 	}
						// } : null}
						isOpen={isAislesPopupOpen}
						onClick={(): void => {
							return;
						}}
						options={store && store.aisles ?
							store.aisles.filter((a: any, i: number) => i > 0 && a.newplano !== false || currentType === 'branch')
								.map((aisle: any, i: number) => {
									return {
										text: aisle.name,
										className: currentAisleIndex === aisle.aisle_id ? 'bg-primary text-white font-small-3 d-flex justify-content-center' : 'd-flex justify-content-center font-small-3',
										link: `/planogram/${currentType}/${currentPageType}/${params.branch_id}/${params.store_id}/${aisle.aisle_id}`,
										action: async () => {
											let newStore: any = store
											if (aisle.empty) { newStore = await getEmptyAisle(store, user, aisle.aisle_id); }
											dispatch(hideProductDetailer())
											dispatch(setMultiSelectId([]))
											dispatch(setCurrentSelectedId(''))
											setColorReportMerged([])
											setDisplayColorReport(false)
											setDisplayOptions({ showProdutDepth: false, hideProducts: false, hideTags: false, productWithoutSize: false })
											dispatch({ type: NEWPLANO_ACTION.SET_NEWPLANO_COLOR_REPORT, payload: {} });
											dispatch({ type: NEWPLANO_ACTION.SET_NEWPLANO_DISPLAY_OPTIONS, payload: {} });
											dispatch(setStore(null))
											dispatch(setStore(newStore))
											dispatch(setDisplayAisle(i))
											// let planoContainerElem:any = document.getElementById('planogram-container')
											// planoContainerElem.style.transform = `translate(45%,150px)`
											setIsAislesPopupOpen(false)
										},
									}
								}) : []}
						onOutsideClick={(): void => setIsAislesPopupOpen(false)}
					/>
				</div>
			</div>


			<div className="d-flex align-items-center">
				<TypedDragItem type={PlanogramDragDropTypes.SHELF_SIDEBAR_FREEZER} payload={{ name: "freezer" }}>
					{NavButton('refrigerator', 'refrigerator', !currentAisle || currentPageType !== 'editor' ? 'opacity-03' : '')}
				</TypedDragItem>
				<TypedDragItem type={PlanogramDragDropTypes.SHELF_SIDEBAR} payload={{ name: "shelf" }}>
					{NavButton('shelf', 'shelf', !currentAisle || currentPageType !== 'editor' ? 'opacity-03' : '')}
				</TypedDragItem>
				<TypedDragItem type={PlanogramDragDropTypes.SECTION_SIDEBAR} payload={{ name: "section" }}>
					{NavButton('section', 'section', !currentAisle || currentPageType !== 'editor' ? 'opacity-03' : '')}
				</TypedDragItem>
				{Divider}
				<div className="position-relative" onClick={() => { if (item && currentPageType === 'editor') { setIsRotatePopupOpen(!isRotatePopupOpen) } }}>
					{NavButton('rotate', 'rotate', !item || currentPageType !== 'editor' ? 'opacity-03' : '')}
					<Popup
						style={{ top: '8vh', marginRight: '-3vw' }}
						className="width-10-rem height-30-vh square text-black d-flex justify-content-center"
						isOpen={isRotatePopupOpen && item}
						onClick={(): void => {
							return;
						}}
						options={
							[0, 1, 2, 3, 4, 5]
								.map((position: any, i: number) => ({
									className: item && item.placement && (position === item.placement.position) ? 'bg-primary text-white px-2  font-small-3 d-flex justify-content-center' : 'd-flex justify-content-center font-small-3',
									text: `${formatMessage({ id: 'position' })} ${position + 1}`,
									action: async () => {
										let width, height, depth;
										if (productDimensions) {
											switch (position) {
												case 0: {
													width = productDimensions.width; height = productDimensions.height; depth = productDimensions.depth;
													break;
												}
												case 1: {
													width = productDimensions.height; height = productDimensions.width; depth = productDimensions.depth;
													break;
												}
												case 2: {
													width = productDimensions.width; height = productDimensions.depth; depth = productDimensions.height;
													break;
												}
												case 3: {
													width = productDimensions.height; height = productDimensions.depth; depth = productDimensions.width;
													break;
												}
												case 4: {
													width = productDimensions.depth; height = productDimensions.height; depth = productDimensions.width;
													break;
												}
												case 5: {
													width = productDimensions.depth; height = productDimensions.width; depth = productDimensions.height;
													break;
												}
												default: {
													width = productDimensions.width; height = productDimensions.height; depth = productDimensions.depth;
												}
											}
											if (parseInt(position) && parseInt(position) != 0) {
												planogramProvider.checkImage(item.product, parseInt(position));
											}


											const product = productsMap ? productsMap[item.product] : null;
											let clientImage: string = JSON.stringify(item.product);
											if (product && product.client_image) clientImage = JSON.stringify(product.client_image);
											let res = planogramApi.makeImageVersion(clientImage, position);

											let originalPlacement = item.placement
											item.placement = {
												...ItemDefualtPlacement,
												...item.placement,
												position: parseInt(position) || 0,
												pWidth: width,
												pHeight: height,
												pDepth: depth,
												row: Math.floor((shelf.dimensions ? shelf.dimensions.depth : 1) / depth)
											};
											dispatch(editShelfItemAction(item.id, item.placement));
										}
									}

								}))}
						onOutsideClick={(): void => setIsRotatePopupOpen(false)}
					/>
				</div>

				{Divider}
				<div onClick={() => {
					if (currentPageType === 'editor') {
						let store = NextSavedStore(aisleSaveArray, aisleSaveIndex)
						if (store && store.aisleSaveArray) {
							dispatch(setAisleAction(store.aisleSaveArray));
							dispatch(setAisleSaveIndex(store.newIndex));
						}
					}
				}}>{NavButton('forward', 'forward', `${currentPageType !== 'editor' ? 'opacity-03' : ''}`)}</div>
				<div onClick={() => {
					if (currentPageType === 'editor') {
						let store = PreviousSavedStore(aisleSaveArray, aisleSaveIndex)
						if (store && store.aisleSaveArray) {
							dispatch(setAisleAction(store.aisleSaveArray));
							dispatch(setAisleSaveIndex(store.newIndex));
						}
					}
				}}>{NavButton('forward', 'backward', `${currentPageType !== 'editor' ? 'opacity-03' : ''}`, 'rotate-180-inverse')}</div>
				<div onClick={() => dispatch(setZoom(Zoom - 0.01))}>{NavButton('zoomout', 'zoomout')}</div>
				<div onClick={() => dispatch(setZoom(Zoom + 0.01))}>{NavButton('zoomin', 'zoomin')}</div>


				{Divider}
				<div
					onClick={() => { setDisplayPopUp(true) }}
					className={classnames("border-turquoise d-flex justify-content-center align-items-center position-relative font-small-3 cursor-pointer py-05 border-2 px-1 width-7-rem text-turquoise text-bold-700  mr-05 border-radius-2rem", {
						"border-turquoise": displayOptionsState && Object.values(displayOptionsState).filter(item => item).length >= 1,
						'bg-turquoise': displayOptionsState && Object.values(displayOptionsState).filter(item => item).length >= 1
					})}>
					<div className={classnames("", {
						'text-white': displayOptionsState && Object.values(displayOptionsState).filter(item => item).length >= 1
					})}><FormattedMessage id="display" /></div>
					{displayPopUp ? <DisplayPopUp displayOptions={displayOptions} setDisplayOptions={setDisplayOptions} setDisplayPopUp={setDisplayPopUp} /> : null}
				</div>
				<div
					onClick={() => setDisplayColorReport(true)}
					className={classnames("min-width-8-rem border-turquoise d-flex justify-content-center align-items-center position-relative font-small-3 cursor-pointer py-05 border-2 px-1 width-7-rem text-turquoise text-bold-700  mr-05 border-radius-2rem", {
						'bg-turquoise': colorReportMerged.length
					})}>
					<div className={classnames("", {
						'text-white': colorReportMerged.length
					})}><FormattedMessage id="colors_report" /></div>
					{displayColorReport &&
						<ColorReport
							isDataReady={isColorReportDataReady}
							setIsDataReady={setIsColorReportDataReady}
							rows={colorRows}
							setRows={setColorRows}
							allRows={newPlano ? newPlano.data : null}
							colorReportMerged={colorReportMerged}
							setColorReportMerged={setColorReportMerged}
							setDisplayColorReport={setDisplayColorReport}
							setFields={setColorFields}
							fields={colorFields} />}
				</div>
			</div>

			<div className="d-flex margin-left-auto">

				{currentAisle && currentAisle.aisle_id &&
					newPlano && newPlano.data && !isSaving ?
					<div
						onClick={() => {
							dispatch({
								type: NEWPLANO_ACTION.SET_NEWPLANO,
								payload: newPlano
							});
							history.push(PLANOGRAM_BASE_URL + '/layouts/' + currentBranchId + '/' + layoutStoreId + '/' + currentAisle.aisle_id)
						}}>
						{NavButton('data_report', 'data_report')}
					</div> :
					<div className={`d-flex-column align-item-center justify-content-center py-05 px-1 border-radius-05rem cursor-pointer text-gray bg-opacity`}>
						<div className="d-flex justify-content-center">
							<Spinner />
						</div>
						<div><FormattedMessage id={'data_report'} /></div>
					</div>
				}

				<div onClick={ () => {
					if( document.getElementsByClassName('planogram-container')[0] ) {
					// let aisleDimensions: number = 0
					// aisleData[0].sections.forEach((aisle: any) => {
					// 	aisleDimensions += aisle.dimensions.width
					// })
					// let a = aisleDimensions / 1000
					// let x = a < 10 ? Number(`0.0${a}`) : (Number(`0.${a}`))
					// let newZoom = 0.45 - x * 2
					// let previousZoom = Zoom

					// dispatch(setZoom(0.2))
					// setTimeout(() => { if (document.getElementsByClassName('planogram-container')[0]) window.print(); }, 500)
					// setTimeout(() => { dispatch(setZoom(previousZoom)) }, 1500);


					const capture = async () => {
						const screenshotTarget = document.body;

						html2canvas(screenshotTarget,{ letterRendering: 1, allowTaint : true, useCORS:true, onrendered :  (canvas:any) => { 
							const base64image = canvas.toDataURL("image/png");
							setDataImage(base64image)
							setTimeout(() => window.print(),5000)
							
							window.addEventListener('afterprint', (event) => {
								setDataImage(null)

							});
						} }).then((canvas:any) => {
							
						});
					}
					  capture()

					//TODO: when print is closed return the previous zoom
					// dispatch(setZoom(previousZoom))

					// window.onafterprint = function () {
					// }
				}}}>
					{dataImage && (
					<img src={dataImage}
					style={{position: 'fixed', zIndex: 999999999, top: 0, left: 0, height: '100vh', width: '100vw'}}></img>
					)}
					{NavButton('print', 'print', `mr-1 ${!document.getElementsByClassName('planogram-container')[0] ? 'opacity-03' : ''}`)}
				</div>
				<div onClick={() => {
					if (currentAisle && currentPageType === 'editor') {
						let _aisle: any = SaveAisle(store, currentAisle)
						if (_aisle) {
							dispatch({
								type: NEWPLANO_ACTION.SET_ALERT,
								payload: {
									timeMs: 1000,
									type: 'success',
									text: 'aisle_saved'
								}
							});
							dispatch(setAisleAction(_aisle));
						}
					}
				}}>
					{NavButton('save', 'save', `mr-1 ${!currentAisle || currentPageType !== 'editor' ? 'opacity-03' : ''}`)}
				</div>
			</div>

			{spinner ?
				<div className="position-fixed" style={{ top: '45vh', left: '50vh' }}>
					<Spinner />
				</div> : null}
		</div>


	</>);
};

export default Navbar;
