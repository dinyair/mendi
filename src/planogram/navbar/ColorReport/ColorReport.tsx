import * as React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import classnames from "classnames"
import { BlankPopUp } from '@components';
import { AgGrid } from '@components';
import { Spinner } from 'reactstrap'
import { AppState } from '@src/planogram/shared/store/app.reducer';
import { Badge } from "reactstrap"
import { FormattedMessage, useIntl } from "react-intl"
import { groupBy } from '@src/helpers/helpers'
import { NEWPLANO_ACTION } from "@src/planogram/shared/store/newPlano/newPlano.types";
import { useDrag } from 'react-dnd'


interface Props {
    setDisplayColorReport: any;
    setColorReportMerged: any;
    colorReportMerged: any;
    allRows: any
    rows: any
    setRows: any
    setFields: any
    fields: any
    isDataReady: any
    setIsDataReady: any
}

export const ColorReport: React.FC<Props> = (props: Props) => {
    const dispatch = useDispatch();
    const { formatMessage } = useIntl();
    const colorReports = useSelector((state: AppState) => state.colorReports)
    // const colorReportsShelfs = useSelector((state: AppState) => state.planogram.virtualStore.shelfMap)
    const productsAndShelfs = useSelector((state: AppState) => state.planogram)
    const productsMap = useSelector((state: AppState) => state.newPlano ? state.newPlano.productsMap : null)
    const url = window.location.href;
    const urlElements = url.split('/');
    let aisleId = parseInt(urlElements[8]);

    const getSpacePercent = (groupType: any) => {
        let newProductsArr: any = []
        let totalShelfsWidth = 0
        let pAs = productsAndShelfs.virtualStore.aisleMap[aisleId].sections
        pAs.forEach((section: any) => {
            if (section.shelves) {
                section.shelves.forEach((shelf: any) => {
                    totalShelfsWidth += shelf.dimensions.width ? shelf.dimensions.width : 0
                    if (shelf.items) {
                        shelf.items.forEach((item: any) => {
                            newProductsArr.push([productsMap[item.product], item.placement])
                        })
                    }
                })
            }
        })
        let final: any = {}
        newProductsArr.forEach((product: any) => {
            if( product[0] )  final[product[0] && product[0][groupType.id] ? product[0][groupType.id] : 0] =  final[product[0][groupType.id]] ? final[product[0][groupType.id]] + product[1].pWidth * product[1].faces : product[1].pWidth * product[1].faces
        })
        return { totalShelf: totalShelfsWidth, idVal: final }
    }
    const { allRows, setDisplayColorReport, setColorReportMerged, colorReportMerged, isDataReady, setIsDataReady, rows, setRows, setFields, fields } = props

    let groups = [{ field: 'Supplier', id: 'SapakId' }, { field: 'Segment', id: 'SegmentId' },
    { field: 'Brand', id: 'ModelId' }, { field: 'SubgroupName', id: 'SubGroupId' }]

    const setFieldsByGroup = (group: string) => {
        let fieldName = ''
        switch (group) {
            case 'Supplier': fieldName = 'supplierWName'; break;
            case 'Segment': fieldName = 'segmentName'; break;
            case 'Brand': fieldName = 'Brand'; break;
            case 'SubgroupName': fieldName = 'subgroupWName'; break;
            default: fieldName = 'noField'; break;
        }
        setFields([{
            text: "color", type: 'color', width: window.innerWidth * 0.2, minWidth: 85, cellRenderer: 'ColorCell',
        },
        {
            text: fieldName, type: 'string', width: window.innerWidth * 0.2, minWidth: 85,
        },
        {
            text: "calculated", type: 'percentage', width: window.innerWidth * 0.2, minWidth: 85,
        },
        {
            text: "ground", type: 'percentage', width: window.innerWidth * 0.2, minWidth: 85,
        },])
        return fieldName
    }
    const Groups = (
        <div className="d-flex py-1 mb-2 width-80-per justify-content-around m-auto">
            {groups && groups.length && (
                groups.map((group: any, index: number) => {
                    let pos = colorReportMerged.findIndex((field: any) => field.field == group.field)
                    return (< div
                        key={index}
                        onClick={pos === -1 ? (e) => { colorMerge(group) } : (e) => { colorMerge() }}
                        className={classnames("d-flex justify-content-center align-items-center bg-gray position-relative font-small-3 cursor-pointer py-05 border-2 px-1 text-black text-bold-700  mr-05 border-radius-2rem", {
                            "border-turquoise": pos >= 0,
                        })}>
                        {<FormattedMessage id={group.field} />}
                    </div>
                    )

                }))
            }
        </div >
    )
    const colorArr = [
        "#63b598", "#ce7d78", "#ea9e70", "#a48a9e", "#c6e1e8", "#648177", "#0d5ac1",
        "#f205e6", "#1c0365", "#14a9ad", "#4ca2f9", "#a4e43f", "#d298e2", "#6119d0",
        "#d2737d", "#c0a43c", "#f2510e", "#651be6", "#79806e", "#61da5e", "#cd2f00",
        "#9348af", "#01ac53", "#c5a4fb", "#996635", "#b11573", "#4bb473", "#75d89e",
        "#2f3f94", "#2f7b99", "#da967d", "#34891f", "#b0d87b", "#ca4751", "#7e50a8",
        "#c4d647", "#e0eeb8", "#11dec1", "#289812", "#566ca0", "#ffdbe1", "#2f1179",
        "#935b6d", "#916988", "#513d98", "#aead3a", "#9e6d71", "#4b5bdc", "#0cd36d",
        "#250662", "#cb5bea", "#228916", "#ac3e1b", "#df514a", "#539397", "#880977",
        "#f697c1", "#ba96ce", "#679c9d", "#c6c42c", "#5d2c52", "#48b41b", "#e1cf3b",
        "#5be4f0", "#57c4d8", "#a4d17a", "#225b8", "#be608b", "#96b00c", "#088baf",
        "#f158bf", "#e145ba", "#ee91e3", "#05d371", "#5426e0", "#4834d0", "#802234",
        "#6749e8", "#0971f0", "#8fb413", "#b2b4f0", "#c3c89d", "#c9a941", "#41d158",
        "#fb21a3", "#51aed9", "#5bb32d", "#807fb", "#21538e", "#89d534", "#d36647",
        "#7fb411", "#0023b8", "#3b8c2a", "#986b53", "#f50422", "#983f7a", "#ea24a3",
        "#79352c", "#521250", "#c79ed2", "#d6dd92", "#e33e52", "#b2be57", "#fa06ec",
        "#1bb699", "#6b2e5f", "#64820f", "#1c271", "#21538e", "#89d534", "#d36647",
        "#7fb411", "#0023b8", "#3b8c2a", "#986b53", "#f50422", "#983f7a", "#ea24a3",
        "#79352c", "#521250", "#c79ed2", "#d6dd92", "#e33e52", "#b2be57", "#fa06ec",
        "#1bb699", "#6b2e5f", "#64820f", "#1c271", "#9cb64a", "#996c48", "#9ab9b7",
        "#06e052", "#e3a481", "#0eb621", "#fc458e", "#b2db15", "#aa226d", "#792ed8",
        "#73872a", "#520d3a", "#cefcb8", "#a5b3d9", "#7d1d85", "#c4fd57", "#f1ae16",
        "#8fe22a", "#ef6e3c", "#243eeb", "#1dc18", "#dd93fd", "#3f8473", "#e7dbce",
        "#421f79", "#7a3d93", "#635f6d", "#93f2d7", "#9b5c2a", "#15b9ee", "#0f5997",
        "#409188", "#911e20", "#1350ce", "#10e5b1", "#fff4d7", "#cb2582", "#ce00be",
        "#32d5d6", "#17232", "#608572", "#c79bc2", "#00f87c", "#77772a", "#6995ba",
        "#fc6b57", "#f07815", "#8fd883", "#060e27", "#96e591", "#21d52e", "#d00043",
        "#b47162", "#1ec227", "#4f0f6f", "#1d1d58", "#947002", "#bde052", "#e08c56",
        "#28fcfd", "#bb09b", "#36486a", "#d02e29", "#1ae6db", "#3e464c", "#a84a8f",
        "#911e7e", "#3f16d9", "#0f525f", "#ac7c0a", "#b4c086", "#c9d730", "#30cc49",
        "#3d6751", "#fb4c03", "#640fc1", "#62c03e", "#d3493a", "#88aa0b", "#406df9",
        "#615af0", "#4be47", "#2a3434", "#4a543f", "#79bca0", "#a8b8d4", "#00efd4",
        "#7ad236", "#7260d8", "#1deaa7", "#06f43a", "#823c59", "#e3d94c", "#dc1c06",
        "#f53b2a", "#b46238", "#2dfff6", "#a82b89", "#1a8011", "#436a9f", "#1a806a",
        "#4cf09d", "#c188a2", "#67eb4b", "#b308d3", "#fc7e41", "#af3101", "#ff065",
        "#71b1f4", "#a2f8a5", "#e23dd0", "#d3486d", "#00f7f9", "#474893", "#3cec35",
        "#1c65cb", "#5d1d0c", "#2d7d2a", "#ff3420", "#5cdd87", "#a259a4", "#e4ac44",
        "#1bede6", "#8798a4", "#d7790f", "#b2c24f", "#de73c2", "#d70a9c", "#25b67",
        "#88e9b8", "#c2b0e2", "#86e98f", "#ae90e2", "#1a806b", "#436a9e", "#0ec0ff",
        "#f812b3", "#b17fc9", "#8d6c2f", "#d3277a", "#2ca1ae", "#9685eb", "#8a96c6",
        "#dba2e6", "#76fc1b", "#608fa4", "#20f6ba", "#07d7f6", "#dce77a", "#77ecca"]

    const colorMerge = (field?: any) => {
        setIsDataReady(false)
        let groupForGrid
        if (field) {
            let fieldName = setFieldsByGroup(field.field)
            let spacePercent = getSpacePercent(field)
            setColorReportMerged([field])
            let groups: any = groupBy(allRows, [field],0,[],formatMessage)
            groupForGrid = groups.map((row: any, index: number) => {
                return {
                    id: row.id,
                    color: colorArr[index],
                    [fieldName]: row.value ? row.value : formatMessage({ id: 'other' }),
                    calculated: row.percentAvg.toFixed(2),
                    ground: spacePercent.idVal[row.id] ? Math.round(((spacePercent.idVal[row.id] * 100) / spacePercent.totalShelf)) : 0
                }
            })
            setRows([])
            setRows(groupForGrid)
            setTimeout(() => { setIsDataReady(true) }, 100);
        } else {
            setColorReportMerged([])
            setIsDataReady(true)
        }
        if (groupForGrid && field) {
            dispatch({
                type: NEWPLANO_ACTION.SET_NEWPLANO_COLOR_REPORT,
                payload: { rows: groupForGrid, merged: field }
            });
        }
        else {
            dispatch({
                type: NEWPLANO_ACTION.SET_NEWPLANO_COLOR_REPORT,
                payload: {}
            });
        }
    };

    React.useEffect(()=> {
        setTimeout(()=> {
            dragElement(document.getElementById("colorReports"));
        },200)
       
    },[])

 
    return (
        <div className='position-absolute' id="colorReports"
            style={{left: '0!important'}}>
            <BlankPopUp
                onOutsideClick={setDisplayColorReport}
                style={{ width: '28rem', height: colorReportMerged.length ? `${10.5 + 13 + rows.length * 1.5}rem` : '10.5rem', top: '1.5rem' }}>
                <div className='d-flex flex-column'>
                    <span className='mb-1 text-bold-700 font-medium-3 text-black mt-2 ml-auto mr-auto width-90-per'>{formatMessage({ id: 'colorReport' })}</span>

                    {(Groups)}

                    {colorReportMerged && colorReportMerged.length ?
                        <div className='width-90-per m-auto'>
                            {isDataReady ?
                                <AgGrid
                                    showOther
                                    resizable
                                    rowBufferAmount={25}
                                    defaultSortFieldNum={3}
                                    descSort
                                    gridHeight={`${12 + rows.length * 1.5}rem`}
                                    floatFilter={false}
                                    translateHeader
                                    fields={fields}
                                    groups={[]}
                                    totalRows={[]}
                                    rows={rows}
                                    checkboxFirstColumn={false}
                                />
                                :
                                <div className='text-black'>
                                    <Spinner />
                                </div>
                            }
                        </div>
                        : null}

                </div>
            </BlankPopUp>
        </div>
    )
};



function dragElement(elmnt:any) {
    console.log('elmnt', elmnt)
    if( elmnt ) {
    var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
      elmnt.onmousedown = dragMouseDown;
  
    function dragMouseDown(e:any) {
      e = e || window.event;
      e.preventDefault();
      // get the mouse cursor position at startup:
      pos3 = e.clientX;
      pos4 = e.clientY;
      document.onmouseup = closeDragElement;
      // call a function whenever the cursor moves:
      document.onmousemove = elementDrag;
    }
    
  
    function elementDrag(e:any) {
      e = e || window.event;
      e.preventDefault();
      // calculate the new cursor position:
      pos1 = pos3 - e.clientX;
      pos2 = pos4 - e.clientY;
      pos3 = e.clientX;
      pos4 = e.clientY;
      // set the element's new position:
      elmnt.style.top = (elmnt.offsetTop - pos2) + "px" ;
      elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
    }
  
    function closeDragElement() {
      // stop moving when mouse button is released:
      document.onmouseup = null;
      document.onmousemove = null;
    }
    }
  }
  
export default ColorReport;
