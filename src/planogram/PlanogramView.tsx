import * as React from "react"
import HTML5Backend from 'react-dnd-html5-backend';
import TouchBackend from 'react-dnd-touch-backend';
import { connect } from 'react-redux';
import { withRouter, RouteComponentProps } from 'react-router';
import { AppState } from '@src/planogram/shared/store/app.reducer';
import { ThunkDispatch } from 'redux-thunk';
import { AnyAction } from 'redux';
import { setStore } from '@src/planogram/shared/store/planogram/store/store.actions';
import { fetchPlanogramStore, productsPredictedSalesAvg, productsPredictedSalesSl } from '@src/planogram/shared/api/planogram.provider';
import { PlanogramStore } from '@src/planogram/shared/store/planogram/planogram.types';
import * as systemApi from '@src/planogram/shared/api/settings.provider';
import * as planogramApi from '@src/planogram/shared/api/planogram.provider';
import * as moment from 'moment';
import classnames from 'classnames';
import {  setCurrentSelectedId, setMultiSelectId, hideProductDetailer } from '@src/planogram/shared/store/planogram/planogram.actions';
import { DndProvider } from 'react-dnd';
import { isMobile } from 'react-device-detect';
import { TransformWrapper, TransformComponent } from "react-zoom-pan-pinch";
import { PlanogramShelfComponent } from './store/PlanogramShelfComponent';
import { widthDensity, heightDensity } from './provider/calculation.service';
import { ProductSaleMap } from '@src/planogram/shared/store/catalog/catalog.types';
import { setWeeklySaleButch } from '@src/planogram/shared/store/catalog/catalog.action';
import { CatalogBarcode } from '@src/planogram/shared/interfaces/models/CatalogProduct';
import { deepExtractKey } from '@src/planogram/shared/store/planogram/virtualize/deep-extract';
import { PLANOGRAM_BASE_URL } from '@src/PlanoApp';
import PlanogramProductDetail from './components/PlanogramProductDetail';
import { Branch, Supplier, Class, Group, SubGroup, Serie, Segment } from '@src/planogram/shared/interfaces/models/System';
import { setBranches, setSuppliers, setClasses, setGroups, setSubGroups, setSeries, setSegments } from '@src/planogram/shared/store/system/data/data.actions';
import { AuthUser } from '@src/planogram/shared/interfaces/models/User';
import { compress, decompress } from 'lz-string'
import { setZoom } from "@src/planogram/shared/store/zoom/zoom.actions";
import Draggable from "react-draggable";

const mapStateToProps = (state: AppState, ownProps: RouteComponentProps<{
    store_id: string,
    aisle_index: string
}>) => ({
    ...ownProps,
    productMap: state.newPlano ? state.newPlano.productsMap : null,
    Zoom: state.Zoom,
    position: state.position,
    products: state.newPlano ? state.newPlano.data : null,
    planogram: state.planogram,
    store: state.planogram.store,
    selectedAisle: state.planogram.display.aisleIndex,
    suppliers: state.system.data.suppliers,
    classes: state.system.data.classes,
    groups: state.system.data.groups,
    subGroups: state.system.data.subGroups,
    branches: state.system.data.branches,
    series: state.system.data.series,
    segments: state.system.data.segments,
    user: state.auth.user,
})
const mapDispatchToProps = (dispatch: ThunkDispatch<AppState, {}, AnyAction>) => ({
    setStore: (store: PlanogramStore, user: AuthUser | undefined) => dispatch(async (dispatch, getState) => {
        const barcodes: CatalogBarcode[] = deepExtractKey(store, "product");
        try {
            let sales = null;
            // check local storage to see if we already have this store's sales for this day 
            let localSalesMap: any = {};
            let localSalesMapS = localStorage.getItem('localSalesMapAvg');
            let localSalesMapB = null;
            if (localSalesMapS) localSalesMapB = decompress(localSalesMapS);
            if (localSalesMapB) localSalesMap = JSON.parse(localSalesMapB);
            if (localSalesMap && localSalesMap[store.store_id] && localSalesMap[store.store_id] != undefined
                && localSalesMap[store.store_id].db === (user ? user.db : '')
                && localSalesMap[store.store_id].date === moment(new Date()).format('YYYY-MM-DD')) {
                // store avg sales are saved in localstorage - load them into the store 
                sales = localSalesMap[store.store_id].sales;
            } else {
                // there is no correct avg sales saved - get sales from DB 
                sales = await productsPredictedSalesAvg(store.store_id);
                // now that we have the sales - save them to localstorage
                localSalesMap[store.store_id] = { date: moment(new Date()).format('YYYY-MM-DD'), sales: sales };
                localStorage.setItem('localSalesMapAvg', compress(JSON.stringify(localSalesMap)));
            }
            // in any event - get the hourly sales from the DB
            const salesHourly = await productsPredictedSalesSl(store.store_id);
            const salesMap: ProductSaleMap = {}
            for (let i = 0; i < sales.length; i++) {
                const sale = sales[i];
                salesMap[sale.BarCode] = {
                    weekly: sale.WeeklyAverage,
                    monthly: null,
                    statusClaMlay: sale.statusCalMlay,
                    hourly: sale.HourlyAverage,
                    Create_Date: sale.Create_Date
                };
            }
            for (let i = 0; i < salesHourly.length; i++) {
                if (salesMap[salesHourly[i].BarCode])
                    salesMap[salesHourly[i].BarCode].hourly = salesHourly[i].HourlyAverage;
                else {
                    salesMap[salesHourly[i].BarCode] = {
                        weekly: null,
                        monthly: null,
                        statusClaMlay: 0,
                        hourly: salesHourly[i].HourlyAverage,
                        Create_Date: new Date()
                    }
                }
            }
            dispatch((dispatch) => dispatch(setWeeklySaleButch(salesMap)));
        } catch (error) {
            console.error(error);
        }
        dispatch((dispatch) => dispatch(setStore(store)));
    }),
    setZoom: (zoom: number) => { dispatch(setZoom(zoom)); },
    setMultiSelectId: (multiSelectId: string[]) => dispatch(setMultiSelectId(multiSelectId)),
    setCurrentSelectedId: (currentSelectedId: string) => dispatch(setCurrentSelectedId(currentSelectedId)),
    setBranches: (branches: Branch[]) => dispatch((dispatch) => dispatch(setBranches(branches))),
    setSuppliers: (suppliers: Supplier[]) => dispatch((dispatch) => dispatch(setSuppliers(suppliers))),
    setClasses: (classes: Class[]) => dispatch((dispatch) => dispatch(setClasses(classes))),
    setGroups: (groups: Group[]) => dispatch((dispatch) => dispatch(setGroups(groups))),
    setSubGroups: (subGroups: SubGroup[]) => dispatch((dispatch) => dispatch(setSubGroups(subGroups))),
    setSeries: (groups: Serie[]) => dispatch((dispatch) => dispatch(setSeries(groups))),
    setSegments: (segments: Segment[]) => dispatch((dispatch) => dispatch(setSegments(segments))),
    clearStore: () => dispatch(setStore(null)),
    hideProductDisplayerBarcode: () => { dispatch(hideProductDetailer()); },
})
type PlanogramViewProps = ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>;
export class PlanogramView extends React.Component<PlanogramViewProps> {
    containerRef = React.createRef<HTMLDivElement>();
    state = {
        containerHeight: 0,
        containerWidth: 0,
        searchFilter: '',
        isLoading: false,
        display: true,
    }

    componentDidMount() {
        const { history, match } = this.props;
        const { params } = match;

        if (!params.store_id)
            return history.push(PLANOGRAM_BASE_URL);
        if (this.containerRef.current) {
            this.setState({
                containerHeight: this.containerRef.current.clientHeight,
                containerWidth: this.containerRef.current.clientWidth,
            })
        }
        
    }
    loadStore = async (storeId: string) => {
        const { setStore, history } = this.props;
        try {
            const store = await fetchPlanogramStore(storeId, this.props.user ? this.props.user.db : '');
            await setStore(store, this.props.user);
            await this.loadData();
            this.setState({
                loading: false
            });
        } catch (error) {
            console.error(error);
            history.goBack();
        }
    }

    /* Add some data to PlanogramView */
    loadData = async () => {
        const {
            branches,
            suppliers,
            classes,
            groups,
            subGroups,
            series,
            segments,
            setBranches,
            setSuppliers,
            setClasses,
            setGroups,
            setSubGroups,
            setSeries,
            setSegments,
        } = this.props;
            if (!this.state.isLoading ) {
                this.setState({isLoading: true});
                let list: any = await systemApi.fetchAllTables({
                    branches: !branches || !branches.length,
                    classes: !classes || !classes.length,
                    groups: !groups || !groups.length,
                    segments: !segments || !segments.length,
                    series:  !series || !series.length,
                    subgroups: !subGroups || !subGroups.length,
                    suppliers: !suppliers  || !suppliers.length
                })
                setClasses(list.classes);
                setGroups(list.groups);
                setSegments(list.segments);
                setSeries(list.series);
                setSubGroups(list.subgroups);
                setSuppliers(list.suppliers);
                setBranches(list.branches);
                this.setState({isLoading: false});
        }
    }
    storePosition(barcode: number) {
        let length = this.props.products.length;
        let initialResult: any[] = [];
        let result: any[] = [];
        for (let i = 0; i < length; i++) {
            if (this.props.products[i].BarCode.toString().includes(barcode.toString())) {
                if (this.props.store && this.props.store.storeProductMap[this.props.products[i].BarCode]) {
                    // concat is used to append one array to another
                    initialResult = initialResult.concat(this.props.store.storeProductMap[this.props.products[i].BarCode].position);
                }
            }
        }

        // at this point result may include many duplicate shelvs so we need to filter it to unique aisle_id only
        let aisleIds = initialResult.map(obj => { return { aisleID: obj.aisle_id, productID: 'A' + obj.aisle_id + 'SE' + obj.section + 'SH' + obj.shelf + "I" + obj.itemIndex } });
        // aisleIds = aisleIds.filter((v, i) => { return aisleIds.indexOf(v) == i; });
        let output: any[] = [];
        for (let i = 0; i < aisleIds.length; i++) {
            let index = output.findIndex(line => line.aisleID === aisleIds[i].aisleID);
            if (index < 0) {
                output.push(aisleIds[i]);
            }
        }
        // at this point aisleIds includce only the unique values of aisle_id
        // now we need to run over aisleIds and enter a single object from initialResult into result for each aisle_id
        for (let n = 0; n < output.length; n++) {
            let index = initialResult.findIndex(obj => obj.aisle_id === output[n].aisleID)
            if (index != -1) {
                let rec = {
                    aisleID: output[n].aisleID,
                    productID: output[n].productID,
                    empty: this.props.store &&  this.props.store.aisles ? this.props.store.aisles.filter(line => line.aisle_id === output[n].aisleID)[0].empty : false,
                }
                result.push(rec);
            }
        }
        return result;
    }
    getEmptyAisle = async (aisleId: number) => {
        if (!this.state.isLoading) {
            this.setState({ isLoading: true });
            let newAisle = await planogramApi.fetchPlanogramAisle(this.props.store ? this.props.store.store_id : '', aisleId, this.props.user ? this.props.user.db : '');
            if (!newAisle || newAisle === undefined) {
                alert("Problem retrieving the aisle from the server");
                return;
            }

            if (this.props.store) {
                let store = this.props.store;
                for (let i = 0; i < store.aisles.length; i++) {
                    if (store.aisles[i] != undefined && newAisle != undefined && store.aisles[i].aisle_id === newAisle.aisle_id) {
                        store.aisles[i] = newAisle;
                        await this.props.setStore(store, this.props.user);
                        break;
                    }
                }
            }
        }
        this.setState({ isLoading: true });

    }
    componentDidUpdate = (prevProps: any) => {
        const { selectedAisle } = this.props;
        if (selectedAisle != prevProps.selectedAisle) {
            this.setState({ display: false })
            setTimeout(() => {
                this.setState({ display: true })
            }, 500);
        }
    }

    render() {

        const { display } = this.state
        if (!display) return null
        const { store, position } = this.props;
        const customizer = { direction: document.getElementsByTagName("html")[0].dir }
        const url = window.location.href;
        const urlElements = url.split('/');
        let currentAisleIndex = parseInt(urlElements[8]);
        let aisle:any = store && store.aisles ? store.aisles.filter((aisle: any) => aisle.aisle_id === currentAisleIndex)[0] : null
        if (aisle && aisle.aisle_id != 0 && aisle.empty) this.getEmptyAisle(aisle.aisle_id);


        /* if we picked a different store than the active one, make active store blank before loading a new store */
        if (store && store.store_id != parseInt(this.props.match.params.store_id)) this.props.clearStore();

        if (store == null || aisle == null )
            return (<div className="planogram-wrapper loader"></div>);

        let containerHeight = this.state.containerHeight;
        let containerWidth = this.state.containerWidth;

        /* function to return a reversed array */
        function reverseArray(array: any[]): any[] {
            let myArray = [];
            let size = array.length;
            for (let i = size - 1; i >= 0; i--) {
                myArray.push(array[i]);
            }
            return myArray;
        }

        /* calculate myzoom */
        let aisleWidth = aisle.sections.map((v:any) => v.dimensions.width).reduce((p:number, c:number) => p + c, 0);
        if (aisleWidth < aisle.dimensions.width)
            aisleWidth = aisle.dimensions.width;
        let aisleHeight = aisle.sections.map((v:any) => v.dimensions.height).reduce((p:number, c:number) => Math.max(p, c), 0);
        if (aisleHeight < aisle.dimensions.height)
            aisleHeight = aisle.dimensions.height;
        /*  changing the zoom of the aisle according to it's length */
        let myZoom = 0, yPos = 50;
        if (aisleWidth <= 8000) {
            myZoom = 0.4;
            yPos = 100;
        }
        if (aisleWidth > 8000 && aisleWidth <= 10000) {
            myZoom = 0.33;
            yPos = 120;
        }
        if (aisleWidth > 10000 && aisleWidth <= 12000) {
            myZoom = 0.28;
            yPos = 150;
        }
        if (aisleWidth > 12000 && aisleWidth <= 15000) {
            myZoom = 0.22;
            yPos = 200;
        }
        if (aisleWidth > 15000 && aisleWidth <= 18000) {
            myZoom = 0.19;
            yPos = 220;
        }
        if (aisleWidth > 18000) {
            myZoom = 0.17;
            yPos = 250;
        }
        let branch = this.props.branches ? this.props.branches.filter(line => line.BranchId === store.branch_id)[0] : null;
        const branchName = branch && branch.Name ? branch.Name : '';

        return (
            <div className='planogram-view-inner'>
                <DndProvider backend={!isMobile ? HTML5Backend : TouchBackend}>
                    <div className="planogram-wrapper" style={{ direction: 'rtl' }}>
                        <div className="planogram-component" style={{ overflow: "auto" }}>
                            <PlanogramProductDetail />
                            <div className="planogram-view"
                                onClick={e => {
                                    e.stopPropagation();

                                    if (!e.ctrlKey) {
                                        e.stopPropagation();
                                        this.props.setCurrentSelectedId('');
                                        this.props.setMultiSelectId([]);
                                        this.props.hideProductDisplayerBarcode();
                                    }
                                }}>
                                {aisle && !aisle.empty
                                    ?
                                    <div id="planogram-aisle-container"
                                        className="planogram-body" >
                                        <TransformWrapper
                                            onZoomChange={(obj: any) => { if (this.props.Zoom !== obj.scale) this.props.setZoom(obj.scale) }}
                                            scale={this.props.Zoom != 0 ? this.props.Zoom : 0.4}
                                            pan={{
                                                disabled: true,
                                            }}
                                            wheel={{
                                                step: 85,
                                                wheelEnabled: true,
                                                touchPadEnabled: true,
                                                limitsOnWheel: false,
                                            }}
                                            options={{
                                                limitToBounds: false,
                                                minScale: 0.1,
                                                maxScale: 10,
                                                centerContent: false,
                                            }}
                                        >

                                            <TransformComponent>
                                                <div style={{ minHeight: '93vh', maxHeight: '93vh', minWidth: '100vw'  }}>

                                                    <Draggable
                                                        cancel=".planogram-section, .planogram-context-window, input"
                                                        defaultClassNameDragging="dragged"
                                                        defaultPosition={{ x: position.x + 750, y: position.y }}
                                                        // scale={props.scale}
                                                        bounds={{ left: -(aisleWidth), top: -aisleHeight, right: aisleWidth, bottom: aisleHeight }}>

                                                        <div id="planogram-container" className={classnames("planogram-container", { 'd-rtl': customizer.direction == 'rtl' })}>
                                                            <div className="planogram-handle">
                                                                {aisle.name}
                                                            </div>
                                                            <div
                                                                ref={(element) => {
                                                                    if (element && (!containerHeight || !containerWidth))
                                                                        this.setState({
                                                                            containerWidth: window.innerWidth > element.clientWidth ? window.innerWidth : element.clientWidth,
                                                                            containerHeight: window.innerHeight > element.clientHeight ? window.innerHeight : element.clientHeight,
                                                                        })
                                                                }}
                                                                className="planogram-aisle"
                                                                onClick={e => {
                                                                    e.stopPropagation();

                                                                    if (!e.ctrlKey) {
                                                                        e.stopPropagation();
                                                                        this.props.setCurrentSelectedId('');
                                                                        this.props.setMultiSelectId([]);
                                                                        this.props.hideProductDisplayerBarcode();
                                                                    }
                                                                }}
                                                                style={{
                                                                    minWidth: widthDensity(aisle.dimensions.width)
                                                                }}>
                                                                {aisle.sections.map((section:any, sectionIndex:number) => {
                                                                    /*  find sectionBorders */
                                                                    let sectionBorders = 'none';
                                                                    let i = sectionIndex;
                                                                    let sections = aisle.sections;
                                                                    if (i === 0) {
                                                                        if (section.section_layout > 0 && ((sections[i + 1] && sections[i + 1].section_layout === 0) || (!sections[i + 1]))) sectionBorders = 'both';
                                                                        if (section.section_layout > 0 && (sections[i + 1] && sections[i + 1].section_layout > 0) && section.section_layout != sections[i + 1].section_layout) sectionBorders = 'both';
                                                                        if (section.section_layout > 0 && (sections[i + 1] && sections[i + 1].section_layout > 0) && section.section_layout === sections[i + 1].section_layout) sectionBorders = 'left';
                                                                    }
                                                                    if (i > 0 && i < sections.length - 1) {
                                                                        if (section.section_layout > 0 && (sections[i - 1] && sections[i - 1].section_layout === 0) && (sections[i + 1] && sections[i + 1].section_layout === 0)) sectionBorders = 'both';
                                                                        if (section.section_layout > 0 && (sections[i - 1] && sections[i - 1].section_layout > 0) && section.section_layout === sections[i - 1].section_layout && (sections[i + 1] && sections[i + 1].section_layout === 0)) sectionBorders = 'right';
                                                                        if (section.section_layout > 0 && (sections[i - 1] && sections[i - 1].section_layout === 0) && (sections[i + 1] && sections[i + 1].section_layout > 0) && section.section_layout === sections[i + 1].section_layout) sectionBorders = 'left';
                                                                        if (section.section_layout > 0 && (sections[i - 1] && sections[i - 1].section_layout > 0) && (sections[i + 1] && sections[i + 1].section_layout > 0)
                                                                            && section.section_layout != sections[i - 1].section_layout && section.section_layout != sections[i + 1].section_layout) sectionBorders = 'both';
                                                                        if (section.section_layout > 0 && (sections[i - 1] && sections[i - 1].section_layout === 0) && (sections[i + 1] && sections[i + 1].section_layout > 0)
                                                                            && section.section_layout != sections[i + 1].section_layout) sectionBorders = 'both';
                                                                        if (section.section_layout > 0 && (sections[i + 1] && sections[i + 1].section_layout === 0) && (sections[i - 1] && sections[i - 1].section_layout > 0)
                                                                            && section.section_layout != sections[i - 1].section_layout) sectionBorders = 'both';
                                                                    }
                                                                    if (i > 0 && i === sections.length - 1) {
                                                                        if (section.section_layout > 0 && (sections[i - 1] && sections[i - 1].section_layout > 0) && section.section_layout === sections[i - 1].section_layout) sectionBorders = 'right';
                                                                        if (section.section_layout > 0 && (sections[i - 1] && sections[i - 1].section_layout === 0)) sectionBorders = 'both';
                                                                        if (section.section_layout > 0 && (sections[i - 1] && sections[i - 1].section_layout > 0) && section.section_layout != sections[i - 1].section_layout) sectionBorders = 'both';
                                                                    }
                                                                    /***************************************/
                                                                    return (
                                                                        <div key={"rcc_" + section.id} className="planogram-section"
                                                                            onClick={e => {
                                                                                e.stopPropagation();
                                                                                if (!e.ctrlKey) {
                                                                                    e.stopPropagation();
                                                                                    this.props.setCurrentSelectedId('');
                                                                                    this.props.setMultiSelectId([]);
                                                                                    this.props.hideProductDisplayerBarcode();
                                                                                }
                                                                            }}
                                                                            style={{
                                                                                width: widthDensity(section.dimensions.width),
                                                                                height: heightDensity(section.dimensions.height + (0.25 * section.dimensions.height)) + 20,
                                                                                borderRight: sectionBorders === "both" || sectionBorders === "right" ? '#00A69A 0.2em solid' : '',
                                                                                borderLeft: sectionBorders === "both" || sectionBorders === "left" ? '#00A69A 0.2em dashed' : '#cacaca 0.1em dashed'
                                                                            }}>
                                                                            <div className="section-title">
                                                                                Section: {section.id}
                                                                            </div>
                                                                            <div className="planogram-section-container">
                                                                                {/* section.shelves.map((shelf, shelf_index) => <PlanogramShelfComponent key={"C_"+shelf_index} shelf={shelf} shelfIndex={shelf_index} />)} */}
                                                                                {/*  second correction - {section.shelves.reverse().map((shelf, shelf_index) => <PlanogramShelfComponent key={"C_"+shelf_index} shelf={shelf} shelfIndex={shelf_index} />)} */}
                                                                                {reverseArray(section.shelves).map((shelf, shelf_index) =>
                                                                                    <PlanogramShelfComponent key={"C_" + shelf_index} shelf={shelf} shelfIndex={shelf_index} />
                                                                                )}
                                                                            </div>
                                                                        </div>
                                                                    )
                                                                })}
                                                            </div>
                                                        </div>
                                                    </Draggable>
                                                </div>
                                            </TransformComponent>
                                        </TransformWrapper>
                                    </div>
                                    : null}
                            </div>
                        </div>
                    </div>
                </DndProvider >
            </div>
        )
    }
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(PlanogramView))
