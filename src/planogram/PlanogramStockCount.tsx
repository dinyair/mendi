import * as React from 'react'
import { connect } from 'react-redux';
import { AppState } from '@src/planogram/shared/store/app.reducer';
import { withRouter, RouteComponentProps } from 'react-router';
import { useGesture } from "react-use-gesture";
import { ThunkDispatch } from 'redux-thunk';
import { AnyAction } from 'redux';
import { TextBox, Button, SelectBox, NumberBox } from 'devextreme-react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronLeft, faCheck, faArrowCircleLeft, faSearch, faEdit } from '@fortawesome/free-solid-svg-icons';
import { CatalogBarcode, CatalogProduct } from '@src/planogram/shared/interfaces/models/CatalogProduct';
import * as planogramApi from '@src/planogram/shared/api/planogram.provider';
import { setStore } from '@src/planogram/shared/store/planogram/store/store.actions';
import { PlanogramStore, PlanogramElementId, PlacementObject, PlanogramItem, PlanogramShelf } from '@src/planogram/shared/store/planogram/planogram.types';
import { uiNotify } from '@src/planogram/shared/components/Toast';
import { BarcodeImage } from './generic/BarcodeImage';
import { addProductAction, deleteItemAction, editShelfItemAction } from '@src/planogram/shared/store/planogram/store/item/item.actions';
import { GroupSection } from '@src/planogram/shared/store/planogram/virtualize/virtualize.reducer';
import { fetchPlanogramYitra, addBarCodeQuantity, updateBarCodeQuantity, fetchLastYitra, fetchLastStock } from '@src/planogram/shared/api/sales.provider';

const mapStateToProps = (state: AppState, ownProps: RouteComponentProps<{
    store_id: string,
    branch_id: string,
}>) => ({
    ...ownProps,
    storeId: state.planogram.store ? state.planogram.store.store_id : null,
    sectionGroups: state.planogram.virtualStore.sectionGroups.groupList,
    catalogMap: state.newPlano ? state.newPlano.productsMap : null,
    store: state.planogram.store,
})

const mapDispatchToProps = (dispatch: ThunkDispatch<AppState, any, AnyAction>) => ({
    setStore: (store: PlanogramStore) => dispatch(setStore(store)),
    addBarcodeToShelf: (shelfId: PlanogramElementId, barcode: CatalogBarcode, placement?: PlacementObject) =>
        dispatch(addProductAction(shelfId, barcode, placement)),
    updateShelfItem: (item: PlanogramElementId, placement: PlacementObject, barcode?: CatalogBarcode) =>
        dispatch(editShelfItemAction(item, placement, barcode))
})

const highlightOnFocus = (e: any) => {
    if (e.event && e.event.srcElement)
        e.event.srcElement.select();
};

type PlanogramStockCountComponentProps = ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>;
class PlanogramStockCountComponent extends React.Component<PlanogramStockCountComponentProps> {
    barcodeInputRef = React.createRef<TextBox>();
    facesInputRef = React.createRef<NumberBox>();
    shelfContainerRef = React.createRef<HTMLDivElement>();
    state = {
        loading: false,
        selectedItem: 0,
        barcode: 0,
        quantity: 0,
        storeItems: [],
        lastStock: {}
    }
    componentDidMount = async () => {
        const { match, history, setStore } = this.props;
        const storeId = match.params.store_id;
        const branchId = match.params.branch_id;
        let storeItems = await fetchPlanogramYitra(parseInt(branchId));
        if (storeItems.length >= 1) this.setState({ storeItems });
    }
    setStage = (stage: string) => {
        this.setState({
            stage
        })
    }

    insertBarcode = () => {
        let index = this.state.storeItems.findIndex((line: any) => JSON.stringify(line.BarCode) === JSON.stringify(this.state.barcode));
        if (index < 0) {
            this.addBarcode(parseInt(this.props.match.params.branch_id), this.state.barcode, this.state.quantity);
        } else this.updateItem(index, parseInt(this.props.match.params.branch_id), this.state.barcode, this.state.quantity);
        this.focusBarcodeInput();
    }

    addBarcode = (branchId: number, barcode: number, quantity: number) => {
        addBarCodeQuantity(branchId, barcode, quantity);
        let storeItems = JSON.parse(JSON.stringify(this.state.storeItems));
        let record = {
            BarCode: barcode,
            Yitra_Shelf_Count: quantity
        }
        storeItems.push(record);
        this.setState({ storeItems });
    }
    updateItem = (index: number, branchId: number, barcode: number, quantity: number) => {
        updateBarCodeQuantity(branchId, barcode, quantity);
        let storeItems = JSON.parse(JSON.stringify(this.state.storeItems));
        storeItems[index].Yitra_Shelf_Count = quantity;
        this.setState({ storeItems });
    }

    focusBarcodeInput = () => {
        if (this.barcodeInputRef.current != null)
            this.barcodeInputRef.current.instance.focus();
    }

    getQuantity = async (branchId: number, barcode: number) => {
        if (!isNaN(barcode))
            await fetchLastYitra(branchId, barcode).then((res) => {
                this.setState({ barcode: barcode, selectedItem: barcode, quantity: res });
            })
        // return await fetchLastYitra(branchId, barcode);
    }

    render() {
        let { loading, barcode, selectedItem, storeItems } = this.state;
        const { branch_id } = this.props.match.params;
        if (branch_id == null || loading)
            return <div className="planogram-document loader"></div>
        if (this.shelfContainerRef.current)
            this.shelfContainerRef.current.scrollLeft = this.shelfContainerRef.current.scrollWidth;
        this.focusBarcodeInput();

        let product = barcode ? this.props.catalogMap[barcode] || this.props.catalogMap[barcode] : null;

        return <div className="planogram-document">
            <div className="container">
                <div className="stage-barcode">
                    <h2 style={{ marginBottom: "0.5em" }}>סריקת ברקוד - ספירת מלאי מדף</h2>
                    <div style={{ marginBottom: "0.5em" }}>
                        <div
                            ref={this.shelfContainerRef}
                            className="scroller horizontal"
                            style={{ display: "flex", overflowX: "auto", flexDirection: "row-reverse", paddingBottom: "0.5em" }}>
                            {this.state.storeItems ? this.state.storeItems.map((item: any) => {
                                return <DocumentShelfItem
                                    key={"ITEM_" + item.BarCode + "_" + item.Yitra_Shelf_Count}
                                    item={item}
                                    selectedItem={this.state.selectedItem}
                                    product={this.props.catalogMap[JSON.stringify(item.BarCode)]}
                                    setState={(state: any) => this.setState(state)}
                                />
                            }) : null}
                        </div>
                    </div>
                    <div className="input-row" style={{ display: "flex" }}>
                        <TextBox
                            tabIndex={1}
                            style={{ flex: 1 }}
                            ref={this.barcodeInputRef}
                            rtlEnabled
                            value={this.state.barcode > 0 ? JSON.stringify(this.state.barcode) : ''}
                            onFocusIn={highlightOnFocus}
                            onValueChanged={async (e) => {
                                let storeItemsL = JSON.parse(JSON.stringify(storeItems));
                                let index = storeItemsL.findIndex((line: any) => JSON.stringify(line.BarCode) === e.value);
                                let quantity = 0;
                                if (index >= 0) {
                                    quantity = storeItemsL[index] ? storeItemsL[index].Yitra_Shelf_Count : 0;
                                    await this.setState({ barcode: parseInt(e.value), selectedItem: e.value, quantity });
                                } else {
                                        await this.setState({ barcode: parseInt(e.value), selectedItem: e.value, quantity });
                                }
                            }}
                            className="row-input"
                            placeholder="נא לסרוק ברקוד..." />
                        <Button
                            className="row-input">
                            <FontAwesomeIcon icon={faSearch} />
                        </Button>
                    </div>
                    {product != null ? <div className="barcode-data" style={{ borderTop: "1px solid #dedede" }}>
                        <div style={{ float: "left" }}>
                            <BarcodeImage barcode={product.BarCode} style={{ maxHeight: "70px", width: "auto", paddingLeft: "0.5em", paddingTop: "0.2em" }} />
                        </div>
                        <h4 style={{ marginBottom: "0.2em", marginTop: "0.5em" }}>{product != null ? product.BarCode : barcode}</h4>
                        <h4 style={{ marginBottom: "0.2em", marginTop: "0.2em", maxWidth: "10em", overflow: "hidden", whiteSpace: "nowrap", textOverflow: "ellipsis" }}>
                            {product != null ? product.Name : null}</h4>
                        {product.Archives != null && product.Archives == 1 ?
                            <div style={{
                                width: "100%",
                                color: "#fafafa",
                                background: "red",
                                padding: "0.5em"
                            }}>מוצר ארכיון!</div>
                            : null}
                        <div style={{ display: "flex", width: "100%", marginBottom: "0.2em" }}>
                            <div style={{ flex: 1, }}>
                                <div>כמות יחידות במדף</div>
                                <NumberBox
                                    id="numberBox1"
                                    ref={this.facesInputRef}
                                    onFocusIn={highlightOnFocus}
                                    tabIndex={2}
                                    rtlEnabled
                                    min={0}
                                    step={1}
                                    width="100%"
                                    value={this.state.quantity}
                                    onValueChanged={(e) => {
                                        this.setState({ quantity: e.value })
                                    }}
                                    className="row-input" />
                            </div>
                            <div style={{ flex: 1, }}>
                                <div>{selectedItem ? "עדכן" : "הוסף"}</div>
                                <Button
                                    id="button1"
                                    tabIndex={5}
                                    useSubmitBehavior={true}
                                    rtlEnabled
                                    style={{ width: "100%", padding: "2px" }}
                                    disabled={product == null}
                                    className="row-input"
                                    onClick={this.insertBarcode}
                                >
                                    <FontAwesomeIcon icon={selectedItem ? faEdit : faCheck} />
                                </Button>
                            </div>

                        </div>
                    </div> : null}
                    <div className="input-row">

                    </div>
                </div>
            </div>
        </div>
    }
}

type DocumentShelfItemProps = {
    item: any,
    selectedItem?: number,
    product?: CatalogProduct,
    setState: Function,
};
class DocumentShelfItem extends React.Component<DocumentShelfItemProps> {

    onClick = (e: any) => {
        const { item, selectedItem } = this.props;
        if (selectedItem === item.id) {
            console.log('selectedItem === item.id', 'item', item, 'selectedItem', selectedItem);
            this.props.setState({
                selectedItem: 0,
            })
        }
        else {
            console.log('selectedItem != item.id', 'item', item, 'selectedItem', selectedItem);
            this.props.setState({
                selectedItem: item.BarCode,
                barcode: item.BarCode,
                quantity: item.Yitra_Shelf_Count
            })
        }
    }
    shouldComponentUpdate(nextProps: DocumentShelfItemProps) {
        if (nextProps.selectedItem !== this.props.selectedItem)
            return true;
        if (nextProps.item.product !== this.props.item.product)
            return true;
        return false;
    }
    render() {
        const { item, product } = this.props;
        const { selectedItem } = this.props;
        return (<div
            onClick={this.onClick}
            key={item.BarCode}
            className="noselect"
            style={{
                maxWidth: "100px",
                display: "flex",
                flexFlow: "column",
                alignContent: "center",
                alignItems: "center",
                fontSize: "0.8em",
                border: "1px solid #dadada",
                color: selectedItem === item.id ? "#ECF0F1" : "inherit",
                background: selectedItem === item.id ? "#273a48" : "none"
            }}>
            <div style={{ padding: "0.2em", fontSize: "1em", fontWeight: "bold" }}>{product ? product.BarCode : item.BarCode}</div>
            <div style={{ display: "flex" }}>
                <div className="input-row" style={{ flex: 1, display: "flex", padding: "0.3em", width: "100%", justifyContent: "space-between" }}>
                    <div style={{ fontSize: "0.7em" }}>כמות&nbsp;</div>
                    <div style={{ fontWeight: "bold" }}>{item.Yitra_Shelf_Count}</div>
                </div>
            </div>
            <div style={{ maxWidth: "30px", maxHeight: "30px" }}>
                <BarcodeImage barcode={item.BarCode} style={{ maxHeight: "30px", width: "auto" }} />
            </div>
        </div>
        )
    }
}


export const PlanogramStockCount = withRouter(connect(mapStateToProps, mapDispatchToProps)(PlanogramStockCountComponent));
