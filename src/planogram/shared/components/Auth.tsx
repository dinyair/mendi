import React, { ComponentProps } from 'react'
import { connect } from "react-redux";
import { AppState } from '@src/planogram/shared/store/app.reducer';

export type AuthWrapperProps = {
    required?: boolean,
    level?: number,
}

const mapStateToProps = (state: AppState, ownProps: {
    children: JSX.Element,
} & AuthWrapperProps) => ({
    ...ownProps,
    isLogged: state.auth.user != null && state.auth.token != null,
    userLevel: state.auth.user && state.auth.user.level != null ? state.auth.user.level : 100,
});

export const AuthRenderer = connect(mapStateToProps)((props: ReturnType<typeof mapStateToProps>): any => {
    const { children, level } = props;
    const { isLogged, userLevel } = props;
    let hasAccess = false;
    if (isLogged) {
        hasAccess = true;
        if (level != null)
            hasAccess = userLevel <= level;
    }
    return hasAccess ? children : null;
})