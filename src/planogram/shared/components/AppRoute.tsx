import * as React from "react";
import { Route, Redirect, RouteProps } from "react-router-dom";
import { connect } from "react-redux";
import { AppState } from "@src/planogram/shared/store/app.reducer";
import { isAuth } from '@containers/user/initial-state';

type AuthRouteProps = RouteProps & {
  title?: string,
  component: any,
  required?: boolean,
  level?: number,
}

const mapStateToProps = (state: AppState, ownProps: AuthRouteProps) => ({
  ...ownProps,
});
class ProtectedRouteComponent extends Route<ReturnType<typeof mapStateToProps>> {


  render() {
    const { component: RouteComponent, ...rest } = this.props;

    return (
       <      Route {...rest} render={ (props:any) => <RouteComponent exact={props.exact} {...props} /> } />
           );
  }
}
class OnlyPublicRouteComponenet extends Route<ReturnType<typeof mapStateToProps>> {
  render() {
    const { exact, component: RouteComponent, ...rest } = this.props;
    return (<Route exact ={exact} {...rest} render={props =>
      !isAuth() ? <RouteComponent {...props} /> : <Redirect to={"/"} />} />);
  }
}

export const ProtectedRoute = connect(mapStateToProps)(ProtectedRouteComponent);
export const OnlyPublicRoute = connect(mapStateToProps)(OnlyPublicRouteComponenet);