// This file is used to check the page version for the browser (as opposed to the phone app) screens

// This class checks if the version saved in the local storage is the same 
// as the latest version in the manifest.json
// If the local version and the last version are not the same we save the new version number
// and refresh the page to get the new version

import * as React from "react";
import { RouteComponentProps, Route } from 'react-router';
import { unregister } from 'serviceWorker';
import config from '@src/planogram/shared/config';
import { connect } from 'http2';
import { AppState, AppAction } from '@src/planogram/shared/store/app.reducer';
import { Dispatch } from 'redux';
import { SYSTEM_ACTION } from '@src/planogram/shared/store/system/system.types';
import { setAppVersion } from '@src/planogram/shared/store/system/system.actions';
import { store } from '@src/planogram/shared/store';
import { logout, refresh } from '@src/planogram/shared/auth/auth.service';
import { httpClientEmpty } from '@src/planogram/shared/http';

// const mapStateToProps = (state: AppState, ownProps: any) => ({
//     version: state.system.app_version,
// })

// const mapDispatchToProps = (dispatch: Dispatch) => ({
//     setVersion: (version: string) => {
//         dispatch(setAppVersion(version));
//     }
// })

export async function getLatestVersion(): Promise<string> {
    // go to manifest.json to get latest version
    let result: string = '';
    try {
        let temp = await httpClientEmpty.get<any>(`/manifest.json`, { baseURL: config.PUBLIC_URL });
        console.log('getLatestVersion',temp.data);
        result = temp.data.version;
    } catch (error) {
        console.error(error);
    }
    return result;
}

// this function checks the local version against the last version on the manifest.json file
// if the two do not match we save the latest version and do a reload
// this function is global and can be called from anywhere
// we use it in AppRoute.tsx that define the behaviour of ProtectedRoute
// - that means that unless the user is logged in they cannot go to the path they intended
// Now we add to the route a version check. That means that the check will happen every time 
// we move from one page to another 
export async function checkVersion() {

    let latestVersion = '';

    let local_token = localStorage.getItem('user_token');
    if (!local_token) {
      let local_error = localStorage.getItem('errors');
      if (!local_error) local_error = '';
      local_error += '. token was removed from local storage - forced logout';
      localStorage.setItem('errors', local_error);
      console.log('token was removed from local storage - forced logout')
    //   logout();
      refresh();
    }
  
    // get version from manifest.json
    getLatestVersion().then((version) => {

        // update latestVersion in version assuming it was received
        if (version && version != '') latestVersion = version;
        // if we received no version initiate it to 1.0.0
        else latestVersion = "1.0.0";

        let localVersion = localStorage.getItem('localVersion');

        if (localVersion && localVersion === latestVersion) {
            console.log('checkVersion localVersion is the same as latestVersion - store.dispatch setAppVersion ', latestVersion);
            store.dispatch(setAppVersion(latestVersion));
        }
        if (!localVersion || localVersion === '') {
            console.log('checkVersion localVersion is blank - now initiating to', latestVersion);
            localStorage.setItem('localVersion', latestVersion);
            store.dispatch(setAppVersion(latestVersion));
            localVersion = latestVersion;
        }

        if (localVersion !== latestVersion) {
            localStorage.setItem('localVersion', latestVersion);
            localVersion = latestVersion;
            store.dispatch(setAppVersion(latestVersion));
            console.log('refresh location checkVersion', window.location.href, 'localVersion', localVersion, 'latestVersion', latestVersion)

            unregister();
            // logout();
            refresh();

            // window.location.reload(true);
        }
    }).catch(err => {
        console.error(err);
    })
}

export async function checkPageVersion() {
    // let latestVersion = await getLatestVersion();

    // // if we received no version initiate it to 1.0.0
    // if (!latestVersion || latestVersion === '') latestVersion = "1.0.0";

    // let localVersion = localStorage.getItem('localVersion');

    // if (localVersion && localVersion === latestVersion) {
    //     console.log('checkPageVersion localVersion is the same as latestVersion - store.dispatch setAppVersion ', latestVersion);
    //     store.dispatch(setAppVersion(latestVersion));
    // }
    // if (!localVersion || localVersion === '') {
    //     console.log('checkPageVersion localVersion is blank - now initiating to', latestVersion);
    //     localStorage.setItem('localVersion', latestVersion);
    //     store.dispatch(setAppVersion(latestVersion));
    //     localVersion = latestVersion;
    // }
    // if (localVersion !== latestVersion) {
    //     console.log('checkPageVersion refresh location checkPageVersion', window.location.href, 'localVersion', localVersion, 'latestVersion', latestVersion)

    //     unregister();
    //     window.location.reload(true);
    // }
    let latestVersion = '';

    let local_token = localStorage.getItem('user_token');
    if (!local_token) {
      let local_error = localStorage.getItem('errors');
      if (!local_error) local_error = '';
      local_error += '. token was removed from local storage - forced logout';
      localStorage.setItem('errors', local_error);
      console.log('token was removed from local storage - forced logout')
      logout();
      refresh();
    }  
    
    // get version from manifest.json
    getLatestVersion().then((version) => {

        // update latestVersion in version assuming it was received
        if (version && version != '') latestVersion = version;
        // if we received no version initiate it to 1.0.0
        else latestVersion = "1.0.0";

        let localVersion = localStorage.getItem('localVersion');

        if (localVersion && localVersion === latestVersion) {
            console.log('checkPageVersion localVersion is the same as latestVersion - store.dispatch setAppVersion ', latestVersion);
            store.dispatch(setAppVersion(latestVersion));
        }
        if (!localVersion || localVersion === '') {
            console.log('checkPageVersion localVersion is blank - now initiating to', latestVersion);
            localStorage.setItem('localVersion', latestVersion);
            store.dispatch(setAppVersion(latestVersion));
            localVersion = latestVersion;
        }

        if (localVersion !== latestVersion) {
            localStorage.setItem('localVersion', latestVersion);
            localVersion = latestVersion;
            store.dispatch(setAppVersion(latestVersion));
            console.log('checkPageVersion refresh location', window.location.href, 'localVersion', localVersion, 'latestVersion', latestVersion)

            unregister();
            // logout();
            refresh();
            // window.location.reload(true);
        }
    }).catch(err => {
        console.error(err);
    })
}

// export default connect(mapStateToProps, mapDispatchToProps)(getLatestVersion)