
import * as React from "react";
import LogoImage from 'assets/images/brand-logo.png'

const isIos = () => /iphone|ipad|ipod/.test(window.navigator.userAgent.toLowerCase());
const isInStandaloneMode = () => 'standalone' in window.navigator || window.matchMedia('(display-mode: standalone)').matches;

type AddToHomescreenProps = {
    onAddToHomescreenClick?: Function,
    title?: string,
    icon?: string,
}

let deferredPrompt: any = null;
window.addEventListener('beforeinstallprompt', (e) => {
    console.log(e);
    
    deferredPrompt = e
    console.log(e);
});

class AddToHomescreen extends React.Component<AddToHomescreenProps> {
    state = {
        bannerVisible: true,
        ready: !!deferredPrompt,
        installed: false,
    };
    componentDidMount() {
        window.addEventListener('appinstalled', (evt) => {
            console.log('a2hs installed');
            this.setState({
                installed: true
            })
        });
    }
    onAddToHomescreenClick = () => {
        if (!deferredPrompt) return console.log("Not ready");
        deferredPrompt.prompt();
        deferredPrompt.userChoice
            .then((choiceResult: any) => {
                if (choiceResult.outcome === 'accepted') {
                    console.log('User accepted the A2HS prompt');
                    this.setState({ bannerVisible: true })
                }
                else {
                    console.log('User dismissed the A2HS prompt');
                    this.setState({ bannerVisible: true })
                }

                // deferredPrompt = null;
            });
    };

    handleCloseBannerBtnClick = () => this.setState({ bannerVisible: false });

    render() {
        const { title, icon } = this.props;
        const { bannerVisible } = this.state;
        if (!deferredPrompt || isIos() || isInStandaloneMode())
            return this.props.children;
        return (
            <React.Fragment>
                {bannerVisible ? <div style={{ position: "fixed", top: "0", height: "100%", width: "100%", background: "#fafafa", zIndex: 9000 }}>
                    <div style={{position:"absolute", top:"50%",left:"50%", transform:"translate(-50%, -50%)", display:"flex", flexFlow:"column", alignContent:"center"}}>
                            <img src={LogoImage} alt="" style={{marginBottom:"2em"}}/>
                            <button onClick={this.onAddToHomescreenClick}>Add To Homescreen</button>
                    </div>
                </div> : null}
                {this.props.children}
            </React.Fragment>
        )
    }
}

export default AddToHomescreen;