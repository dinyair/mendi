import placeholderImage from '@src/assets/images/planogram/placeholder-image.png';

export function errorImageHandler(e: any) {
    e.preventDefault();
    e.target.src = placeholderImage
}