import thunkMiddleware, { ThunkMiddleware } from 'redux-thunk'
import { createLogger } from 'redux-logger'
import { createStore, applyMiddleware, AnyAction } from 'redux'
import { rootReducer, AppState } from './store/app.reducer';
import { apiMiddleware } from './store/api.middleware';
import { localStorageMiddleware } from './store/localstorage.middleware';
import config from './config';
import { routerMiddleware } from 'connected-react-router';
import { composeWithDevTools } from 'redux-devtools-extension';
import { History, createBrowserHistory } from 'history';
import createSagaMiddleware, { Saga, SagaMiddleware } from 'redux-saga';
import { Store, Middleware, compose } from 'redux';
import { history } from '@src/store';

const historyMiddleware: Middleware = routerMiddleware(history)

export const store = createStore(
    rootReducer,
    config.ENV !== "production" ? applyMiddleware(
        thunkMiddleware as ThunkMiddleware<AppState, AnyAction>,
        createLogger(),
        apiMiddleware,
        localStorageMiddleware,
        historyMiddleware,
    ) : applyMiddleware(
        thunkMiddleware as ThunkMiddleware<AppState, AnyAction>,
        apiMiddleware,
        localStorageMiddleware,
        historyMiddleware
    )
);