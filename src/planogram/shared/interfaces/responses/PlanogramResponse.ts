import { PlanogramAisle, PlanogramStore } from "@src/planogram/shared/store/planogram/planogram.types";

export interface PlanogramMainStructureResponse {
    aisles: any
}

export interface FetchPlanogramStoreResponse {
    status: "ok" | "empty",
    store: PlanogramStore | null
}

export interface FetchPlanogramAisleResponse {
    status: "ok" | "empty",
    aisle: PlanogramAisle | null,
}