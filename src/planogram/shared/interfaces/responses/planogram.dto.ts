
export type ProductPredictedSalesResponse = {
    barcode: number,
    amount: number
}

export type ProductWeekSales = {
    Id: number,
    BarCode: number,
    BranchId: number,
    WeeklyAverage: number | null,
    /* Barak 13.5.2020*/ statusCalMlay: number | null,
    /* Barak 24.6.2020*/ HourlyAverage: number | null,
    /* Barak 3.1.2021*/ Create_Date: Date
}

/* Barak 14.12.20 */
export type ProductHourSales = {
    Id: number,
    BarCode: number,
    HourlyAverage: number | null,
}
/******************/