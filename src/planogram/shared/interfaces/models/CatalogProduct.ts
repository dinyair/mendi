export type CatalogBarcode = number;

export interface CatalogProduct {
    Id: number,
    BarCode: CatalogBarcode,
    Name: string,
    CatalogId?: number,
    ClassesId?: number,
    GroupId?: number,
    SubGroupId?: number,
    SapakId?: number,
    DegemId?: number,
    Ariza?: number,
    Archives?: number,
    length?: number,
    width?: number,
    height?: number,
    weightGross?: number,
    ts?: String | Date,
    DegemName?: string,
    Bdate_buy: string,
    Bdate_sale: string,
    IsSubBar: boolean
    SegmentId: number,
    ModelId: number,
    client_image?:number | string,
    client_image_arr?: any[],
    Create_Date:Date
}

export interface BarcodeStatus {
    Id: number,
    BarCode: CatalogBarcode,
    Message?: string,
    UserId?: number
    UpdatedAt: String | Date,
}