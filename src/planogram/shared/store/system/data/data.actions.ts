import { CompanyActionTypes } from "@src/containers/user/enums";
import { Branch, Supplier, Group, Class, Serie, SubGroup, Segment, Layout } from "@src/planogram/shared/interfaces/models/System";
import {
    SetBranchesAction, SYSTEM_DATA_ACTION, SetSuppliersAction, SetGroupsAction, SetStoresAction,
    SetClassesAction, SetSeriesAction, SetSubGroupsAction, SetSegmentsAction, SetModelsAction, SetUserReportTemplatesAction, SetLayoutsAction
} from "./data.types";

export const setBranches = (branches: Branch[]): SetBranchesAction => ({
    type: SYSTEM_DATA_ACTION.SET_BRANCHES,
    branches
});
export const setSuppliers = (suppliers: Supplier[]): SetSuppliersAction => ({
    type: SYSTEM_DATA_ACTION.SET_SUPPLIERS,
    suppliers
});
export const setGroups = (groups: Group[]): SetGroupsAction => ({
    type: SYSTEM_DATA_ACTION.SET_GROUPS,
    groups
});
export const setSubGroups = (subGroups: SubGroup[]): SetSubGroupsAction => ({
    type: SYSTEM_DATA_ACTION.SET_SUBGROUPS,
    subGroups
});
/* Barak 13.12.20 */
export const setLayouts = (layouts: Layout[]): SetLayoutsAction => ({
    type: SYSTEM_DATA_ACTION.SET_LAYOUTS,
    layouts
}); 



/******************/
export const setClasses = (classes: Class[]): SetClassesAction => ({
    type: SYSTEM_DATA_ACTION.SET_CLASSES,
    classes
});
export const setSeries = (series: Serie[]): SetSeriesAction => ({
    type: SYSTEM_DATA_ACTION.SET_SERIES,
    series
});
/* Barak 18.8.20 */
export const setSegments = (segments: Segment[]): SetSegmentsAction => ({
    type: SYSTEM_DATA_ACTION.SET_SEGMENTS,
    segments
});
/*****************/
/* Barak 6.12.20 */

export const setModels = (models:any[]) => {
    return (dispatch: any) => {
        dispatch({
            type: CompanyActionTypes.SET_MODELS,
            payload:models
        })
    }
}

// export const setModels = (models: any[]) => {
//     return (dispatch: any) => {
//         dispatch({
//             type: CompanyActionTypes.SET_MODELS,
//             payload: models
//         });
//     }
// }


export const setNewLayouts = (layouts: any[]) => {
    return (dispatch: any) => {
        dispatch({
            type: CompanyActionTypes.SET_LAYOUTS,
            payload: layouts
        });
    }
}
/*****************/
/* Barak 7.12.20 */
export const setUserReportTemplates = (userReportTemplates: any[]): SetUserReportTemplatesAction => ({
    type: SYSTEM_DATA_ACTION.SET_USER_REPORT_TEMPLATES,
    userReportTemplates
});
/*****************/
/* Barak 24.12.20 */
export const setStores = (stores: any[]): SetStoresAction => ({
    type: SYSTEM_DATA_ACTION.SET_STORES,
    stores
});
/*****************/