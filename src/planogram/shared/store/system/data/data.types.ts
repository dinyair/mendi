import { PlanogramStoreRecord } from "@src/planogram/shared/api/planogram.provider";
import * as System from "@src/planogram/shared/interfaces/models/System";

export interface SystemDataState {
    branches: System.Branch[],
    suppliers: System.Supplier[],
    groups: System.Group[],
    subGroups: System.SubGroup[],
    classes: System.Class[],
    series: System.Serie[],
    /* Barak 18.8.20 */ segments: System.Segment[],
    /* Barak 6.12.20 */ models: System.Model[],
    /* Barak 13.12.20 */ layouts: System.Layout[],
    branchesMap: { [key: number]: System.Branch },
    suppliersMap: { [key: number]: System.Supplier },
    groupsMap: { [key: number]: System.Group },
    subGroupsMap: { [key: number]: System.SubGroup },
    classesMap: { [key: number]: System.Class },
    seriesMap: { [key: number]: System.Serie },
    /* Barak 18.8.20 */ segmentsMap: { [key: number]: System.Segment },
    /* Barak 6.12.20 */ modelsMap: { [key: number]: System.Model },
    /* Barak 7.12.20 */ userReportTemplates: any[],
    /* Barak 24.12.20 */ stores: PlanogramStoreRecord[],
}

export enum SYSTEM_DATA_ACTION {
    SET_BRANCHES = "SET_BRANCHES",
    SET_SUPPLIERS = "SET_SUPPLIERS",
    SET_GROUPS = "SET_GROUPS",
    SET_SUBGROUPS = "SET_SUBGROUPS",
    /* Barak 13.12.20 */ SET_LAYOUTS = "SET_LAYOUTS",
    SET_CLASSES = "SET_CLASSES",
    SET_SERIES = "SET_SERIES",
    /* Barak 18.8.20 */ SET_SEGMENTS = "SET_SEGMENTS",
    /* Barak 6.12.20 */ SET_MODELS = "SET_MODELS",
    /* Barak 7.12.20 */ SET_USER_REPORT_TEMPLATES = "SET_USER_REPORT_TEMPLATES",
    /* Barak 24.12.20 */ SET_STORES = "SET_STORES"
}


export type SetBranchesAction = {
    type: SYSTEM_DATA_ACTION.SET_BRANCHES
    branches: System.Branch[]
};
export type SetSuppliersAction = {
    type: SYSTEM_DATA_ACTION.SET_SUPPLIERS
    suppliers: System.Supplier[]
};
export type SetGroupsAction = {
    type: SYSTEM_DATA_ACTION.SET_GROUPS
    groups: System.Group[]
};
export type SetSubGroupsAction = {
    type: SYSTEM_DATA_ACTION.SET_SUBGROUPS
    subGroups: System.SubGroup[]
};
/* Barak 13.12.20 */
export type SetLayoutsAction = {
    type: SYSTEM_DATA_ACTION.SET_LAYOUTS
    layouts: System.Layout[]
}; 
/******************/
export type SetClassesAction = {
    type: SYSTEM_DATA_ACTION.SET_CLASSES
    classes: System.Class[]
};
export type SetSeriesAction = {
    type: SYSTEM_DATA_ACTION.SET_SERIES
    series: System.Serie[]
};
/* Barak 18.8.20 */
export type SetSegmentsAction = {
    type: SYSTEM_DATA_ACTION.SET_SEGMENTS
    segments: System.Segment[]
};
/*****************/
/* Barak 6.12.20 */
export type SetModelsAction = {
    type: SYSTEM_DATA_ACTION.SET_MODELS
    models: System.Model[]
};
/*****************/
/* Barak 7.12.20 */
export type SetUserReportTemplatesAction = {
    type: SYSTEM_DATA_ACTION.SET_USER_REPORT_TEMPLATES
    userReportTemplates: any[]
};
/*****************/
/* Barak 24.12.20 */
export type SetStoresAction = {
    type: SYSTEM_DATA_ACTION.SET_STORES
    stores: any[]
};
/*****************/

export type SystemDataActionTypes = SetBranchesAction | SetSuppliersAction | SetGroupsAction | SetSubGroupsAction
    | SetClassesAction | SetSeriesAction | SetSegmentsAction | SetModelsAction | SetUserReportTemplatesAction
    | SetLayoutsAction | SetStoresAction;