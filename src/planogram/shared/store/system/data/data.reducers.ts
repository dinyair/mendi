
import { Reducer } from 'redux'
import { SystemDataState, SYSTEM_DATA_ACTION, SystemDataActionTypes } from './data.types'
import { toMap } from '@src/helpers/planogram';

export const initialSystemData: SystemDataState = {
    branches: [],
    branchesMap: {},
    suppliers: [],
    suppliersMap: {},
    classes: [],
    classesMap: {},
    groups: [],
    groupsMap: {},
    subGroups: [],
    subGroupsMap: {},
    /* Barak 13.12.20 */ layouts: [],
    series: [],
    seriesMap: {},
    /* Barak 18.8.20 */
    segments: [],
    segmentsMap: {},
    /*****************/
    /* Barak 6.12.20 */
    models: [],
    modelsMap: {},
    /*****************/
    /* Barak 7.12.20 */ userReportTemplates:[],
    /* Barak 24.12.20 */ stores: []
}

export const systemDataReducer: Reducer<SystemDataState, SystemDataActionTypes> = (state = initialSystemData, action) => {
    switch (action.type) {
        case SYSTEM_DATA_ACTION.SET_BRANCHES:
            return {
                ...state,
                branches: action.branches,
                branchesMap: toMap(action.branches, "BranchId")
            }
        case SYSTEM_DATA_ACTION.SET_SUPPLIERS:
            return {
                ...state,
                suppliers: action.suppliers,
                suppliersMap: toMap(action.suppliers, "Id")
            }
        case SYSTEM_DATA_ACTION.SET_GROUPS:
            return {
                ...state,
                groups: action.groups,
                groupsMap: toMap(action.groups, "Id")
            }
        case SYSTEM_DATA_ACTION.SET_SUBGROUPS:
            return {
                ...state,
                subGroups: action.subGroups,
                subGroupsMap: toMap(action.subGroups, "Id")
            }
        /* Barak 13.12.20 */
        case SYSTEM_DATA_ACTION.SET_LAYOUTS:
            return {
                ...state,
                layouts: action.layouts,
            } 
        /******************/
        case SYSTEM_DATA_ACTION.SET_CLASSES:
            return {
                ...state,
                classes: action.classes,
                classesMap: toMap(action.classes, "Id")
            }
        case SYSTEM_DATA_ACTION.SET_SERIES:
            return {
                ...state,
                series: action.series,
                seriesMap: toMap(action.series, "Id")
            }
        /* Barak 18.8.20 */
        case SYSTEM_DATA_ACTION.SET_SEGMENTS:
            return {
                ...state,
                segments: action.segments,
                segmentsMap: toMap(action.segments, "Id")
            }
        /*****************/
        /* Barak 6.12.20 */
        case SYSTEM_DATA_ACTION.SET_MODELS:
            return {
                ...state,
                models: action.models,
                modelsMap: toMap(action.models, "Id")
            }
        /*****************/
        /* Barak 7.12.20 */
        case SYSTEM_DATA_ACTION.SET_USER_REPORT_TEMPLATES:
            return {
                ...state,
                userReportTemplates: action.userReportTemplates,
            }
        /*****************/
        /* Barak 24.12.20 */
        case SYSTEM_DATA_ACTION.SET_STORES:
            return {
                ...state,
                stores: action.stores,
            }
        /*****************/

        default:
            return state
    }
}
