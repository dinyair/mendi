import { planogramReducer, intitialPlanogramState } from './planogram/planogram.reducers'
import { systemReducer, initialSystemState } from './system/system.reducers'
import { combineReducers, Reducer } from 'redux';
import { catalogReducer, initialCatalogState } from './catalog/catalog.reducer';
import { newPlanoReducer, colorReportsReducer, displayOptionsReducer, alertReducer,positionReducer } from './newPlano/newPlano.reducer'
import { authReducer, initialAuthState } from './auth/auth.reducer';
import { ZoomReducer, initialZoommState } from './zoom/zoom.reducer';
import { SystemActions } from './system/system.types';
import { AuthActions } from './auth/auth.types';
import { PlanogramActions } from './planogram/planogram.types';
import { CatalogActions } from './catalog/catalog.types';
import {store} from '@src/index'
export type AppState = {
    Zoom: ReturnType<typeof ZoomReducer>,
    position: ReturnType<typeof positionReducer>,
    auth: ReturnType<typeof authReducer>,
    system: ReturnType<typeof systemReducer>,
    planogram: ReturnType<typeof planogramReducer>,
    catalog: ReturnType<typeof catalogReducer>,
    newPlano: ReturnType<typeof newPlanoReducer>,
    colorReports: ReturnType<typeof colorReportsReducer>,
    displayOptions: ReturnType<typeof displayOptionsReducer>,
    alert: ReturnType<typeof alertReducer>
}

export const initialAppState: AppState = {
    Zoom: initialZoommState,
    position: {x:(window.innerWidth), y:window.innerHeight * 0.45},
    auth: initialAuthState,
    catalog: initialCatalogState,
    system: initialSystemState,
    planogram: intitialPlanogramState,
    colorReports: {},
    newPlano: [],
    displayOptions: {},
    alert:{}
}

export type AppAction = AuthActions & SystemActions & PlanogramActions & CatalogActions;

export const rootReducer: Reducer<AppState, AppAction> = (state = initialAppState, action) => ({
    ...store?.getState(),
    Zoom: ZoomReducer(state.Zoom, action),
    auth: authReducer(state.auth, action),
    system: systemReducer(state.system, action),
    planogram: planogramReducer(state, action),
    catalog: catalogReducer(state.catalog, action),
    newPlano: newPlanoReducer(state, action),
    position: positionReducer(state, action),
    colorReports: colorReportsReducer(state, action),
    displayOptions: displayOptionsReducer(state, action),
    alert: alertReducer(state, action),
})
