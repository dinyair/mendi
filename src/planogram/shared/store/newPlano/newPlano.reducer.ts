import { NEWPLANO_ACTION } from "./newPlano.types";
import { toMap } from '@src/helpers/planogram';


export function colorReportsReducer(state: any = {}, action: any): any {
    switch (action.type) {
        case NEWPLANO_ACTION.SET_NEWPLANO_COLOR_REPORT:
            return action.payload;
        default:
            return state.colorReports
    }
}
export function displayOptionsReducer(state: any = {}, action: any): any {
    switch (action.type) {
        case NEWPLANO_ACTION.SET_NEWPLANO_DISPLAY_OPTIONS:
            return {...state, ...action.payload};
        default:
            return state.displayOptions
    }
}

export function newPlanoReducer(state: any = {}, action: any): any {
    switch (action.type) {
        case NEWPLANO_ACTION.SET_NEWPLANO:
             if (action.payload && action.payload.data) {
                 return {
                ...action.payload,
                productsMap: toMap(action.payload.data, "BarCode")
            }} else return null
        default:
            return state.newPlano
    }
}
export function alertReducer(state: any = {}, action: any): any {
    switch (action.type) {
        case NEWPLANO_ACTION.SET_ALERT:
            return action.payload;
        default:
            return state.alert
    }
}
export function positionReducer(state: any = {}, action: any): any {
    switch (action.type) {
        case NEWPLANO_ACTION.SET_POSITION:
            return action.payload;
        default:
            return state.position
    }
}