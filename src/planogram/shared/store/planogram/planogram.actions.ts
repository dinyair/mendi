import {
    SetLoadingActionType, SetDisplayAisleActionType, PLANOGRAM_ACTIONS, SetDisplayBranchActionType,
    SetProductDetailerActionType, HideProductDetailerActionType, ToggleRowItemsAction,
    ToggleSettingsAction, SetColorBy, SetMultiSelectIdActionType, ToggleAlignLeftAction, SetCurrentSelectedIdActionType, PlanogramAisle, SetAisleSaveArrayActionType, SetAisleSaveIndexActionType
} from "./planogram.types";
import { CatalogBarcode } from "@src/planogram/shared/interfaces/models/CatalogProduct";
import { PlanogramDisplayColorBy } from "./display/display.types";


export function setPlanogramLoading(state: boolean): SetLoadingActionType {
    return {
        type: PLANOGRAM_ACTIONS.SET_LOADING,
        isLoading: state
    }
}
export function setDisplayAisle(aisleIndex: number): SetDisplayAisleActionType {
    return {
        type: PLANOGRAM_ACTIONS.SET_DISPLAY_AISLE,
        aisleIndex
    }
}
export function setDisplayBranch(branchId: number): SetDisplayBranchActionType {
    return {
        type: PLANOGRAM_ACTIONS.SET_DISPLAY_BRANCH,
        branchId
    }
}
/* Barak 24.6.2020 - Remove *
export function setProductDetailer(barcode: CatalogBarcode): SetProductDetailerActionType {
    return {
        type: PLANOGRAM_ACTIONS.SET_PRODUCT_DETAILER,
        barcode
    }
}
*/
/* Barak 24.6.2020 - Add lowSales to the setProductDetailer function */
/* Barak 23.7.20 *
export function setProductDetailer(barcode: CatalogBarcode, lowSales: boolean): SetProductDetailerActionType {
    return {
        type: PLANOGRAM_ACTIONS.SET_PRODUCT_DETAILER,
        barcode, 
        lowSales
    }
}
*/
/* Barak 23.7.20 */
export function setProductDetailer(barcode: CatalogBarcode, lowSales: boolean, shelfFaces:number, missing: number, netw: string, userPhone: string): SetProductDetailerActionType {
    return {
        type: PLANOGRAM_ACTIONS.SET_PRODUCT_DETAILER,
        barcode,
        lowSales,
        /* Barak 17.12.20 */ shelfFaces,
        missing,
        netw,
        userPhone,
    }
}
/* Barak 21.4.20 - Add multi selection for delete */
export function setMultiSelectId(multiSelectId: string[]): SetMultiSelectIdActionType {
    return {
        type: PLANOGRAM_ACTIONS.SET_PRODUCT_MULTISELECTID,
        multiSelectId,
    }
}
/**************************************************/
/* Barak 29.9.20 - Align all items to left or not align */
export function toggleAlignLeft(): ToggleAlignLeftAction {
    return {
        type: PLANOGRAM_ACTIONS.TOGGLE_ALIGN_LEFT,
    }
}
/**************************************************/
/* Barak 4.8.20 - Add currentSelectedId for movment with arrows */
export function setCurrentSelectedId(currentSelectedId: string): SetCurrentSelectedIdActionType {
    return {
        type: PLANOGRAM_ACTIONS.SET_PRODUCT_CURRENTSELECTEDID,
        currentSelectedId,
    }
}
/**************************************************/
export function hideProductDetailer(): HideProductDetailerActionType {
    return {
        type: PLANOGRAM_ACTIONS.HIDE_PRODUCT_DETAILER,
    }
}
export function toggleRowItems(): ToggleRowItemsAction {
    return {
        type: PLANOGRAM_ACTIONS.TOGGLE_ROW_ITEMS,
    }
}
export function toggleSettings(): ToggleSettingsAction {
    return {
        type: PLANOGRAM_ACTIONS.TOGGLE_SETTINGS,
    }
}
export function setColorBy(color_by: PlanogramDisplayColorBy): SetColorBy {
    return {
        type: PLANOGRAM_ACTIONS.COLOR_BY,
        color_by
    }
}
/* Barak 22.11.20 - Add saving array for aisle, allowing to do 
   Ctrl+Z to move back and Ctrl+Y to move forward */
export function setAisleSaveArray(aisleSaveArray:PlanogramAisle[]): SetAisleSaveArrayActionType {
    return {
        type: PLANOGRAM_ACTIONS.SET_AISLE_SAVE_ARRAY,
        aisleSaveArray,
    }
}
export function setAisleSaveIndex(aisleSaveIndex:number): SetAisleSaveIndexActionType {
    return {
        type: PLANOGRAM_ACTIONS.SET_AISLE_SAVE_INDEX,
        aisleSaveIndex,
    }
}
/*************************************************************/
   