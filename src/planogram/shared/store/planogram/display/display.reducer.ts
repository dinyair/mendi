import { PlanogramState, PlanogramActions, PLANOGRAM_ACTIONS } from "../planogram.types"
import { Reducer } from "redux"
import { PlanogramDisplayState } from "./display.types"

export const initialDisplayState: PlanogramDisplayState = {
    aisleIndex: 0,
    hideShelfItems: false,
    showRowItems: false,
    showSettings: false,
    markBadProducts: true,
    /* Barak 17.6.2020 */ markArchiveProducts: false,
    displaySalesReport: false,
    /* Barak 24.6.2020 * productDetailer: null, */
    productDetailer: {
        barcode: null,
        lowSales: null,
        /* Barak 17.12.20 */ shelfFaces: 0,
        /* Barak 23.7.20 */
        missing: 0,
        netw: '',
        userPhone: '',
        /******************/
    },
    colorBy: null,
    /* Barak 21.4.20 - Add multi selection for delete */ multiSelectId: [],
    /* Barak 4.8.20 - Add currentSelectedId for movment with arrows */ currentSelectedId: '',
    /* Barak 29.9.20 - add flag fot aligning to left or not */ alignToLeft: false,
    /* Barak 22.11.20 - Add saving array for aisle, allowing to do 
       Ctrl+Z to move back and Ctrl+Y to move forward */
    aisleSaveArray: [],
    aisleSaveIndex: 0,
    /*************************************************************/
    /* Barak 8.12.20 - add store sectionGroups from DB */ sectionGroups: {},
    /* Barak 24.12.20 - allow hiding of sales tags */ hideSaleTags: false,
    /* Barak 29.12.20 - display color pop up */ displayColorPopUp: false,
    /* Barak 29.12.20 - contain display aisle summaryObjects */ summaryObjects:[],
    /* Barak 29.12.20 - color map display parameter */ colorPopUpColorBy:null
}

export const planogramDisplayReducer: Reducer<PlanogramDisplayState, PlanogramActions> = (state = initialDisplayState, action) => {
    if (action.type === PLANOGRAM_ACTIONS.SET_DISPLAY_AISLE) {
        if (state.aisleIndex === action.aisleIndex)
            return state;
        return {
            ...state,
            aisleIndex: action.aisleIndex
            // virtualStore: virtualizeReducer(state.store ? state.store.aisles[action.aisleIndex] : undefined, action)
        }
    }
    else if (action.type === PLANOGRAM_ACTIONS.SHOW_SALES_REPORT) {
        return {
            ...state,
            displaySalesReport: true
        }
    }
    else if (action.type === PLANOGRAM_ACTIONS.HIDE_SALES_REPORT) {
        return {
            ...state,
            displaySalesReport: false
        }
    }
    else if (action.type === PLANOGRAM_ACTIONS.SET_PRODUCT_DETAILER) {
        /* Barak 24.6.2020 *
        if (state.productDetailer === action.barcode)
            return state;
        return {
            ...state,
            productDetailer: action.barcode
        }
        ********************/
        /* Barak 24.6.2020 */
        if (state.productDetailer.barcode === action.barcode
            && state.productDetailer.lowSales === action.lowSales
            /* Barak 17.12.20 */ && state.productDetailer.shelfFaces === action.shelfFaces
            /* Barak 23.7.20 */
            && state.productDetailer.missing === action.missing
            && state.productDetailer.netw === action.netw
            && state.productDetailer.userPhone === action.userPhone)
            /******************/
            return state;
        return {
            ...state,
            productDetailer: {
                barcode: action.barcode,
                lowSales: action.lowSales,
                /* Barak 17.12.20 */ shelfFaces: action.shelfFaces,
                /* Barak 23.7.20 */
                missing: action.missing,
                netw: action.netw,
                userPhone: action.userPhone,
                /******************/
            }
        }
    }
    else if (action.type === PLANOGRAM_ACTIONS.HIDE_PRODUCT_DETAILER) {
        /* Barak 24.6.2020 * 
        if (state.productDetailer === null) 
            return state;
        return {
            ...state,
            productDetailer: null
        }
        */
        /* Barak 24.6.2020 */
        if (state.productDetailer.barcode === null)
            return state;
        return {
            ...state,
            productDetailer: {
                barcode: null,
                lowSales: null,
                /* Barak 23.7.20 */
                missing: 0,
                netw: 'Admin',
                userPhone: ''
            }
        }
    }
    else if (action.type === PLANOGRAM_ACTIONS.COLOR_BY) {
        let lastColorBy = state.colorBy;
        return {
            ...state,
            colorBy: action.color_by,
            colorPopUpColorBy: action.color_by ? action.color_by : lastColorBy
        }
    }
    /* Barak 21.4.20 - Add multi selection for delete */
    else if (action.type === PLANOGRAM_ACTIONS.SET_PRODUCT_MULTISELECTID) {
        return {
            ...state,
            multiSelectId: action.multiSelectId
        }
    }
    /**************************************************/
    /* Barak 29.9.20 - Align all items to left or not align */
    else if (action.type === PLANOGRAM_ACTIONS.TOGGLE_ALIGN_LEFT) {
        return {
            ...state,
            alignToLeft: !state.alignToLeft,
        }
    }
    /**************************************************/
    /* Barak 4.8.20 - Add currentSelectedId for movment with arrows */
    else if (action.type === PLANOGRAM_ACTIONS.SET_PRODUCT_CURRENTSELECTEDID) {
        return {
            ...state,
            currentSelectedId: action.currentSelectedId
        }
    }
    /**************************************************/
    else if (action.type === PLANOGRAM_ACTIONS.TOGGLE_SHELF_ITEMS) {
        return {
            ...state,
            hideShelfItems: !state.hideShelfItems,
        }
    }
    /* Barak 24.12.20 - allow hiding of sales tags */ 
    else if (action.type === PLANOGRAM_ACTIONS.TOGGLE_SALE_TAGS) {
        return {
            ...state,
            hideSaleTags: !state.hideSaleTags
        }
    }
    /***********************************************/
    else if (action.type === PLANOGRAM_ACTIONS.TOGGLE_ROW_ITEMS) {
        return {
            ...state,
            showRowItems: !state.showRowItems,
        }
    }
    else if (action.type === PLANOGRAM_ACTIONS.TOGGLE_SETTINGS) {
        console.log('showSettings', state.showSettings);
        return {
            ...state,
            showSettings: !state.showSettings
        }
    }
    else if (action.type === PLANOGRAM_ACTIONS.TOGGLE_BAD_PRODUCTS_MARKER) {
        return {
            ...state,
            markBadProducts: !state.markBadProducts
        }
    }
    /* Barak 17.6.2020 - Add action to mark Archive products */
    else if (action.type === PLANOGRAM_ACTIONS.TOGGLE_ARCHIVE_PRODUCTS_MARKER) {
        return {
            ...state,
            markArchiveProducts: !state.markArchiveProducts
        }
    }
    else if (action.type === PLANOGRAM_ACTIONS.SET_AISLE_SAVE_ARRAY) {
        let testArray = action.aisleSaveArray;
        while (testArray.length > 10) {
            // remove the first itemin the array
            testArray.shift();
        }
        return {
            ...state,
            aisleSaveArray: testArray
        }
    }
    else if (action.type === PLANOGRAM_ACTIONS.SET_AISLE_SAVE_INDEX) {
        let testIndex = action.aisleSaveIndex;
        if (testIndex > 9) testIndex = 9;
        if (testIndex < 0) testIndex = 0;
        return {
            ...state,
            aisleSaveIndex: action.aisleSaveIndex
        }
    }
   
    /*  add store sectionGroups from DB */
    else if (action.type === PLANOGRAM_ACTIONS.SET_SECTION_GROUPS) {
        console.log('setSectionGroups SET_SECTION_GROUPS', action);
        return {
            ...state,
            sectionGroups: action.sectionGroups
        }
    }
    /*  display color pop up */
    else if (action.type === PLANOGRAM_ACTIONS.SHOW_COLOR_POPUP) {
        return {
            ...state,
            displayColorPopUp: true
        }
    }
    else if (action.type === PLANOGRAM_ACTIONS.HIDE_COLOR_POPUP) {
        return {
            ...state,
            displayColorPopUp: false
        }
    }
    else if (action.type === PLANOGRAM_ACTIONS.SET_SUMMARY_OBJECTS) {
        return {
            ...state,
            summaryObjects: action.summaryObjects
        }
    }
    return state;
}