import { CatalogBarcode } from "@src/planogram/shared/interfaces/models/CatalogProduct";
import { PlanogramAisle } from "../planogram.types";

/* Barak 7.9.20 *  export type PlanogramDisplayColorBy = "supplier" | null; */
/* Barak 7.9.20 */ export type PlanogramDisplayColorBy = string | null;

export type PlanogramDisplayState = {
    aisleIndex: number,
    hideShelfItems: boolean,
    showRowItems: boolean,
    showSettings: boolean,
    markBadProducts: boolean,
    /* Barak 17.6.2020 */ markArchiveProducts: boolean,
    displaySalesReport: boolean,
    /* Barak 24.6.2020 * productDetailer: CatalogBarcode | null, */
    /* Barak 24.6.2020 */ productDetailer: any,
    colorBy: PlanogramDisplayColorBy, // display supplier summary color report
    /* Barak 21.4.20 - Add multi selection on items so we can delete more than one */ multiSelectId: string[],
    /* Barak 4.8.20 - Add currentSelectedId for movment with arrows */ currentSelectedId: string,
    /* Barak 29.9.20 - add flag fot aligning to left or not */ alignToLeft: boolean,
    /* Barak 22.11.20 - Add saving array for aisle, allowing to do 
       Ctrl+Z to move back and Ctrl+Y to move forward */ 
    aisleSaveArray:PlanogramAisle[],
    aisleSaveIndex:number,   
    /*************************************************************/
    /* Barak 8.12.20 - add store sectionGroups from DB */ sectionGroups: any,
    /* Barak 24.12.20 - allow hiding of sales tags */ hideSaleTags: boolean,
    /* Barak 29.12.20 - display color pop up */ displayColorPopUp: boolean,
    /* Barak 29.12.20 - contain display aisle summaryObjects */ summaryObjects:any,
    /* Barak 29.12.20 - color map display parameter */ colorPopUpColorBy:PlanogramDisplayColorBy
};