import { ShowSalesReportAction, PLANOGRAM_ACTIONS, HideSalesReportAction, ToggleShelfItemsAction, ToggleBadProductsMarkerAction, 
    ToggleArchiveProductsMarkerAction, SetSectionGroupsAction, ToggleSaleTagsAction, HideColorPopUpAction, ShowColorPopUpAction, SetSummaryObjectsActionType,  } from "../planogram.types";

export const showSalesReport = (): ShowSalesReportAction => ({
    type: PLANOGRAM_ACTIONS.SHOW_SALES_REPORT
})
export const hideSalesReport = (): HideSalesReportAction => ({
    type: PLANOGRAM_ACTIONS.HIDE_SALES_REPORT
})
export const toggleShelfItems = (): ToggleShelfItemsAction => ({
    type: PLANOGRAM_ACTIONS.TOGGLE_SHELF_ITEMS
})
/* Barak 24.12.20 - allow hiding of sales tags */ 
export const toggleSaleTags = (): ToggleSaleTagsAction => ({
    type: PLANOGRAM_ACTIONS.TOGGLE_SALE_TAGS
})
/***********************************************/
export const toggleBadProductsMarker = (): ToggleBadProductsMarkerAction => ({
    type: PLANOGRAM_ACTIONS.TOGGLE_BAD_PRODUCTS_MARKER
})
/* Barak 17.6.2020 - Add action to mark Archive products */
export const toggleArchiveProductsMarker = (): ToggleArchiveProductsMarkerAction => ({
    type: PLANOGRAM_ACTIONS.TOGGLE_ARCHIVE_PRODUCTS_MARKER
})
/*********************************************************/
/* Barak 8.12.20 - add store sectionGroups from DB */
export const setSectionGroups = (sectionGroups: any): SetSectionGroupsAction => (
    {
        type: PLANOGRAM_ACTIONS.SET_SECTION_GROUPS,
        sectionGroups
    }
)
/*********************************************************/
/* Barak 29.12.20 - display color pop up */
export const showColorPopUp = (): ShowColorPopUpAction => ({
    type: PLANOGRAM_ACTIONS.SHOW_COLOR_POPUP
})
export const hideColorPopUp = (): HideColorPopUpAction => ({
    type: PLANOGRAM_ACTIONS.HIDE_COLOR_POPUP
})
export const SetSummaryObjects = (summaryObjects: any): SetSummaryObjectsActionType => ({
    type: PLANOGRAM_ACTIONS.SET_SUMMARY_OBJECTS, 
    summaryObjects
})
/*****************************************/