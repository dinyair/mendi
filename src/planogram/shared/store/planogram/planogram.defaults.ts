import { DimensionObject, PlacementObject, PlanogramAisle, PlanogramItem } from "./planogram.types";

export enum PLANOGRAM_ID {
    AISLE = "A",
    SECTION = "SE",
    SHELF = "SH",
    ITEM = "I",
    PRODUCT = "P",
}

export const AisleDefaultDimension: DimensionObject = {
    height: 400,
    width: 5000,
    depth: 450
}
export const SectionDefaultDimension: DimensionObject = {
    height: 2000,
    width: 1000,
    depth: 450
}
export const SectionMaxDimension: DimensionObject = {
    height: 5000,
    /* Barak 4.8.20 * width: 3000, */
    /* Barak 7.8.20 * Barak 4.8.20 * width: 9000, */
    /* Barak 7.8.20 */ width: 15000,
    depth: 1500
}
export const ShelfDefaultDimension: DimensionObject = {
    height: 400,
    width: 1000,
    depth: 450
}

export const ProductDefaultDimensions: DimensionObject = {
    height: 150,
    width: 150,
    depth: 150
}
export const ItemDefualtPlacement: PlacementObject = {
    faces: 1,
    row: 1,
    stack: 1,
    manual_row_only: 0,
    /* Barak & Osher 10.9.20 - support rotating products*/
    position: 0,
    pWidth: ProductDefaultDimensions.width,
    pHeight: ProductDefaultDimensions.height,
    pDepth: ProductDefaultDimensions.depth,
    /****************************************************/
    /* Barak 29.9.20*/ distFromStart: ProductDefaultDimensions.width,
    /* Barak 26.11.20 */ distFromTop: null
}
export const ItemMaxPlacement: PlacementObject = {
    /* Barak 23.1.20 * faces: 30, */
    /* Barak 23.1.20 */ faces: 100,
    row: 30,
    stack: 10,
    manual_row_only: 0,
    /* Barak & Osher 10.9.20 - support rotating products*/
    position: 0,
    pWidth: ProductDefaultDimensions.width,
    pHeight: ProductDefaultDimensions.height,
    pDepth: ProductDefaultDimensions.depth,
    /****************************************************/
    /* Barak 29.9.20*/ distFromStart: ProductDefaultDimensions.width * 100,
    /* Barak 26.11.20 */ distFromTop: ShelfDefaultDimension.height * 100
}

/* Barak 8.12.20 - add blankPlanogramAisle default*/
export let blankPlanogramAisle: PlanogramAisle = {
    id: '',
    aisle_id: 0,
    index: 0,
    aisle_number: 0,
    aisle_plano: null,
    newplano: false,
    name: '',
    sections: [],
    colorMap: {},
    suppColorMap: {},
    classColorMap: {},
    groupColorMap: {},
    subGroupColorMap: {},
    segmentColorMap: {},
    modelColorMap: {},
    dimensions: AisleDefaultDimension,
    empty: false,
    aisle_version: 0,
}
/**************************************************/
/* Barak 21.12.20 - add blankPlanogramAisle default*/
export let blankItem: PlanogramItem = {
    id: '',
    placement: {
        faces: 1,
        stack: 1,
        row: 1,
        manual_row_only: 0,
        position: 0,
        pWidth: ProductDefaultDimensions.width,
        pHeight: ProductDefaultDimensions.height,
        pDepth: ProductDefaultDimensions.depth,
        distFromStart: 0,
        distFromTop: null
    },
    product: 0,
    lowSales: false,
    missing: 0,
    branch_id: 0,
    linkedItemUp: null,
    linkedItemDown: null,
    shelfLevel: 1,
};
/**************************************************/