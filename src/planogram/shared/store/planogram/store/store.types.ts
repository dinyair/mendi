import { PlanogramStore } from "../planogram.types";
import { SectionActionTypes } from "./section/section.types";

export type PlanogramViewState = PlanogramStore | null;

export enum STORE_ACTIONS {
    INIT_FETCH = 'INIT_FETCH',
    SET_VIEW = "SET_VIEW",
    SET_ERROR = "SET_ERROR",
    RENAME = "RENAME",
    SET_STORE_PRODUCT_MAP = "SET_STORE_PRODUCT_MAP",
}

export type InitViewActionType = {
    type: STORE_ACTIONS.INIT_FETCH
};
export type SetViewActionType = {
    type: STORE_ACTIONS.SET_VIEW,
    store: PlanogramStore | null
};
export type SetErrorActionType = {
    type: STORE_ACTIONS.SET_ERROR,
    error: any
};
export type RenameStoreActionType = {
    type: STORE_ACTIONS.RENAME,
    name: string
};
/* Barak 8.12.20 - set store.storeProductMap */
export type SetStoreProductMapActionType = {
    type: STORE_ACTIONS.SET_STORE_PRODUCT_MAP,
    storeProductMap: any
}
/*********************************************/

export type PlanogramViewActionTypes = SetErrorActionType | SetViewActionType | InitViewActionType | SectionActionTypes | RenameStoreActionType
                                        | SetStoreProductMapActionType;
