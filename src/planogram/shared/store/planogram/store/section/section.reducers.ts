import { PlanogramActions, PlanogramAisle, PlanogramShelf, PlanogramItem } from "../../planogram.types";
import { SECTION_ACTIONS } from "./section.types";
import { shelfReducer } from "../shelf/shelf.reducers";
import { SHELF_ACTIONS } from "../shelf/shelf.types";
import { ItemDefualtPlacement, SectionDefaultDimension, ShelfDefaultDimension, PLANOGRAM_ID } from "../../planogram.defaults";
import { ITEM_ACTIONS } from "../item/item.types";

export function sectionReducer(state: PlanogramAisle, action: PlanogramActions): PlanogramAisle {
    switch (action.type) {
        case SECTION_ACTIONS.ADD_SECTION: {
            if (action.aisle !== state.id)
                return state;
            const sectionId = state.id + PLANOGRAM_ID.SECTION + (state.sections.length);
            return {
                ...state,
                sections: [
                    ...state.sections,
                    {
                        id: sectionId,
                        dimensions: action.dimensions || SectionDefaultDimension,
                        shelves: action.shelf || action.product ?
                            (action.shelf ? [{
                                // dimensions: ShelfDefaultDimension,
                                ...action.shelf,
                                // height_position: heightPosition(0, action.shelf.dimensions ? action.shelf.dimensions.height : ShelfDefaultDimension.height),
                                items: action.shelf.items ? action.shelf.items.map((item, i) => ({
                                    ...item,
                                    id: sectionId + PLANOGRAM_ID.SHELF + "0" + PLANOGRAM_ID.ITEM + i
                                })) : [],
                                id: sectionId + PLANOGRAM_ID.SHELF + "0",
                                /* Barak 29.9.20 */ distFromStart: action.shelf.items && action.shelf.items[0] && action.shelf.items[0].placement ? parseInt(JSON.stringify(action.shelf.items[0].placement.distFromStart)) : 0,
                                /* Barak 26.11.20 */ freezer: action.shelf.freezer ? action.shelf.freezer : 0,
                            }] : (action.product ? [{
                                id: sectionId + PLANOGRAM_ID.SHELF + "0",
                                dimensions: ShelfDefaultDimension,
                                margin_bottom: 0,
                                section_height: 0,
                                // height_position: heightPosition(0, ShelfDefaultDimension.height),
                                items: [{
                                    id: sectionId + PLANOGRAM_ID.SHELF + "0" + PLANOGRAM_ID.ITEM + "0",
                                    placement: ItemDefualtPlacement,
                                    product: action.product,
                                    /* Barak 24.6.2020 - Add lowSales flag to the item */
                                    lowSales: false,
                                    /* Barak 23.7.2020 - Add missing shelf amount flag to the item */
                                    missing: 0,
                                    /* Barak 28.6.2020 - Add branch_id default*/
                                    branch_id: 0,
                                    /* Barak 21.12.20 - Add linking item option */
                                    linkedItemUp: null,
                                    linkedItemDown: null,
                                    shelfLevel: 1,
                                    /********************************************/

                                }],
                                /* Barak 29.9.20 */ distFromStart: 0,
                                /* Barak 26.11.20 */ freezer: 0
                            }] : [])) : [],
                        /* Barak 3.12.20 */ section_layout: 0
                    }
                ]
            }
        }
        case SECTION_ACTIONS.DUPLICATE_SECTION: {
            if (action.aisle !== state.id)
                return state;
            const aisleId = state.id;
            return {
                ...state,
                sections: [
                   
                    ...state.sections,
                    {
                        ...action.section,
                        id: aisleId + PLANOGRAM_ID.SECTION + state.sections.length
                    },
                    
                ].map((s, i) => ({
                    ...s,
                    shelves: s.shelves.map((sh, _i) => ({
                        ...sh,
                        items: sh.items.map((item, __i) => ({
                            ...item,
                            id: aisleId + PLANOGRAM_ID.SECTION + i + PLANOGRAM_ID.SHELF + _i + PLANOGRAM_ID.ITEM + __i
                        })),
                        // height_position: calculateHeightPosition(s.shelves, _i),
                        id: aisleId + PLANOGRAM_ID.SECTION + i + PLANOGRAM_ID.SHELF + _i
                    })),
                    id: aisleId + PLANOGRAM_ID.SECTION + i
                }))
            }
        }
        case SECTION_ACTIONS.DELETE_SECTION: {
            if (action.aisle !== state.id)
                return state;
            const sectionIndex = state.sections.findIndex(s => s === action.section || s.id === action.section.id);
            if (sectionIndex === -1)
                return state;
            return {
                ...state,
                sections: [
                    ...state.sections.slice(0, sectionIndex),
                    ...state.sections.slice(sectionIndex + 1),
                ].map((s, i) => ({
                    ...s,
                    id: state.id + PLANOGRAM_ID.SECTION + i
                })),
            };
        }
        case SECTION_ACTIONS.EDIT_SECTION_DIMENSION: {
            return {
                ...state,
                sections: state.sections.map((s, i) => {
                    if (s === action.section || s.id === action.section.id)
                        return {
                            ...s,
                            dimensions: action.dimension
                        };
                    return s
                }),
            };
        }
        /* Barak 3.12.20 - Save section */
        case SECTION_ACTIONS.SAVE_SECTION: {
            return {
                ...state,
                sections: state.sections.map((s, i) => {
                    if (s === action.section || s.id === action.section.id)
                        return {
                            ...s,
                        };
                    return s
                }),
            };
        }
        /********************************/
        case SECTION_ACTIONS.SWITCH_SECTIONS: {
            let baseIndex = state.sections.findIndex(se => se.id === action.base.id);
            let remoteIndex = state.sections.findIndex(se => se.id === action.remote.id);
            if (baseIndex === -1 || remoteIndex === -1)
                return state;
            const sections = [...state.sections];
            [sections[baseIndex], sections[remoteIndex]] = [sections[remoteIndex], sections[baseIndex]];
            return {
                ...state,
                sections: sections.map((s, i) => ({
                    ...s,
                    shelves: s.shelves.map((sh, _i) => ({
                        ...sh,
                        items: sh.items.map((item, __i) => ({
                            ...item,
                            id: state.id + PLANOGRAM_ID.SECTION + i + PLANOGRAM_ID.SHELF + _i + PLANOGRAM_ID.ITEM + __i
                        })),
                        id: state.id + PLANOGRAM_ID.SECTION + i + PLANOGRAM_ID.SHELF + _i
                    })),
                    id: state.id + PLANOGRAM_ID.SECTION + i
                }))
            }
        }
        case SECTION_ACTIONS.REMOVE_ITEMS: {
            if (action.aisle !== state.id)
                return state;
            const sectionId: string = action.section;
            return {
                ...state,
                sections: state.sections.map((s, i) => ({
                    ...s,
                    shelves: s.shelves.map((sh, _i) => ({
                        ...sh,
                        items: sectionId === s.id ? [] : sh.items.map((item, __i) => ({
                            ...item,
                            id: state.id + PLANOGRAM_ID.SECTION + i + PLANOGRAM_ID.SHELF + _i + PLANOGRAM_ID.ITEM + __i
                        })),
                        id: state.id + PLANOGRAM_ID.SECTION + i + PLANOGRAM_ID.SHELF + _i
                    })),
                    id: state.id + PLANOGRAM_ID.SECTION + i
                }))
            }
        }
        case SHELF_ACTIONS.SWITCH_SHELVES: {
            const base = action.shelf;
            const remote = action.remote;
            if (base === remote)
                return state;

            let baseShelf: PlanogramShelf | null = null;
            let remoteShelf: PlanogramShelf | null = null;

            for (let i = 0; i < state.sections.length; i++) {
                const section = state.sections[i];
                for (let k = 0; k < section.shelves.length; k++) {
                    const shelf = section.shelves[k];
                    if (base === shelf.id) {
                        baseShelf = shelf;
                    }
                    else if (remote === shelf.id) {
                        remoteShelf = shelf;
                    }
                }
            }
            if (!baseShelf || !remoteShelf)
                return state;

            return {
                ...state,
                sections: state.sections.map((se, i) => ({
                    ...se,
                    shelves: se.shelves.map((sh, _i) => ({
                        ...sh,
                        ...(sh.id === base ? remoteShelf : (sh.id === remote ? baseShelf : sh)),
                        id: se.id + PLANOGRAM_ID.SHELF + _i,
                        items: (sh.id === base && remoteShelf ? remoteShelf.items : (sh.id === remote && baseShelf ? baseShelf.items : sh.items)).map((item, __i) => ({
                            ...item,
                            id: se.id + PLANOGRAM_ID.SHELF + _i + PLANOGRAM_ID.ITEM + __i
                        }))
                    }))
                }))
            }
        }
        case ITEM_ACTIONS.SWITCH_ITEMS: {
            console.log('case ITEM_ACTIONS.SWITCH_ITEMS section.reducers', state);
            const base = action.item;
            const remote = action.remote;
            /***** Barak 21.4.20 *****/
            let base_id = base.substring(0, base.indexOf('I') + 1);
            let start_index = parseInt(base.substring(base.indexOf('I') + 1, base.length));
            let end_index = parseInt(remote.substring(remote.indexOf('I') + 1, remote.length));
            // console.log('id_base', id_base, start_index, end_index);
            /*************************/
            if (base === remote)
                return state;

            let baseItem: PlanogramItem | null = null;
            let remoteItem: PlanogramItem | null = null;
            /* Barak 21.4.20 */ let shelf_items: any;
            for (let i = 0; i < state.sections.length; i++) {
                const section = state.sections[i];
                for (let k = 0; k < section.shelves.length; k++) {
                    const items = section.shelves[k].items;
                    for (let j = 0; j < items.length; j++) {
                        if (base === items[j].id) {
                            /* Barak 21.4.20 - make sure we have an array of this shelf items */ shelf_items = section.shelves[k].items;
                            baseItem = items[j];
                        }
                        else if (remote === items[j].id) {
                            remoteItem = items[j];
                        }
                    }
                }
            }
            if (baseItem == null || remoteItem == null)
                return state;

            /***** Barak remove 21.4.20 *****
            return {
                ...state,
                sections: state.sections.map((se) => ({
                    ...se,
                    shelves: se.shelves.map((sh) => ({
                        ...sh,
                        items: sh.items.map((item, __i) => ({
                            ...item,
                            ...(item.id === base ? remoteItem : (item.id === remote ? baseItem : item)),
                            id: sh.id + PLANOGRAM_ID.ITEM + __i
                        }))
                    }))
                }))
            }
            *************************/
            /***** Barak 21.4.20 *****/
            function replace(i: number, item: PlanogramItem) {
                let currBaseId = item.id.substring(0, item.id.indexOf('I') + 1);
                if (currBaseId === base_id) {
                    // we only do the replacement on the same shelf as the base item
                    if (start_index < end_index) {
                        // if we are dragging an item from right to left
                        if (i === start_index) {
                            console.log('shelf items', shelf_items);
                            console.log('i === start_index', i, shelf_items[end_index] ? shelf_items[end_index].id : 'Not Available');
                            if (shelf_items[end_index]) return shelf_items[end_index];
                            else return item;
                        }
                        else {
                            if (i > start_index && i <= end_index) {
                                console.log('i > start_index && i <= end_index', i, shelf_items[i - 1] ? shelf_items[i - 1].id : 'Not Available');
                                if (shelf_items[i - 1]) return shelf_items[i - 1];
                                else return item;
                            }
                            else {
                                return item;
                            }
                        }
                    }
                    if (end_index < start_index) {
                        // if we are dragging an item from left to right
                        if (i >= end_index && i < start_index) {
                            if (shelf_items[i + 1]) return shelf_items[i + 1];
                            else return item;
                        } else {
                            if (i === start_index) {
                                if (shelf_items[end_index]) return shelf_items[end_index];
                                else return item;
                            } else {
                                return item;
                            }
                        }
                    }
                } else return item;
            }

            /* Barak 12.8.20 *
            return {
                ...state,
                sections: state.sections.map((se) => ({
                    ...se,
                    shelves: se.shelves.map((sh) => ({
                        ...sh,
                        items: sh.items.map((item, __i) => ({
                            ...item,
                            ...(replace(__i, item)),
                            id: sh.id + PLANOGRAM_ID.ITEM + __i
                        }))
                    }))
                }))
            }
            ***************/

            /* Barak 12.8.20 - after the switch go over the shelf and make sure there are no identical items next to each other */
            state = {
                ...state,
                sections: state.sections.map((se) => ({
                    ...se,
                    shelves: se.shelves.map((sh) => ({
                        ...sh,
                        items: sh.items.map((item, __i) => ({
                            ...item,
                            ...(replace(__i, item)),
                            id: sh.id + PLANOGRAM_ID.ITEM + __i
                        }))
                    }))
                }))
            }

            function fixItemIdForShelf(shelf: PlanogramShelf) {
                // this function is meant to make sure that all item ID on this shelf are consecutive
                // I've added it since there were cases after consolidateShelf, especially when deleting an item
                // that the numbering went all wrong and had gaps 
                for (let i = shelf.items.length - 1; i >= 0; i--) {
                    let currA = parseInt(shelf.items[i].id.substring(shelf.items[i].id.indexOf('A') + 1, shelf.items[i].id.indexOf('SE')));
                    let currSE: number = parseInt(shelf.items[i].id.substring(shelf.items[i].id.indexOf('SE') + 2, shelf.items[i].id.indexOf('SH')));
                    let currSH: number = parseInt(shelf.items[i].id.substring(shelf.items[i].id.indexOf('SH') + 2, shelf.items[i].id.indexOf('I')));
                    let currI: number = i;
                    let newId = "A" + currA + "SE" + currSE + "SH" + currSH + "I" + currI;
                    if (newId != shelf.items[i].id) {
                        console.log('shelf item gets new id, old id', shelf.items[i].id, 'new id', newId);
                        shelf.items[i].id = newId;
                    }
                }
                return shelf;
            }

            function consolidateShelf(shelf: PlanogramShelf) {
                // console.log("shelf after replacing", currentShelf);
                for (let i = shelf.items.length - 1; i >= 0; i--) {
                    //console.log("REPLACE CHECK i", i, 'currentShelf.items[i]', currentShelf.items[i], "i-1", i - 1, 'currentShelf.items[i - 1]', currentShelf.items[i - 1]);
                    if (shelf.items[i] && shelf.items[i - 1] && shelf.items[i].product === shelf.items[i - 1].product) {
                        console.log('replacing same product - update faces');
                        shelf.items[i - 1].placement.faces += shelf.items[i].placement.faces;
                        shelf.items.splice(i, 1);
                    }
                }
                return fixItemIdForShelf(shelf);
            }

            state = {
                ...state,
                sections: state.sections.map((se) => ({
                    ...se,
                    shelves: se.shelves.map((sh) => (consolidateShelf(sh)))
                }))
            }
            return state;
            /********************************************************************************************************************/

            /*************************/

            
        }
        case SHELF_ACTIONS.ADD_SHELF:
        case SHELF_ACTIONS.DELETE_SHELF:
        case SHELF_ACTIONS.DUPLICATE_SHELF:
        case SHELF_ACTIONS.EDIT_SHELF_DIMENSIONS:
        case ITEM_ACTIONS.ADD_PRODUCT:
        case ITEM_ACTIONS.DELETE_ITEM:
        case ITEM_ACTIONS.DUPLICATE_ITEM:
        case ITEM_ACTIONS.EDIT_ITEM_PLACEMENT:
            return {
                ...state,
                sections: state.sections.map((s) => shelfReducer(s, action))
            }
        default:
            return state;
    }
}