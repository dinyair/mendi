import { PlanogramShelf, PlanogramItem, PlacementObject, PlanogramElementId } from "../../planogram.types";
import { ITEM_ACTIONS, addItemActionType, duplicateItemActionType, deleteItemActionType, switchItemsActionType, EditItemActionType, moveItemActionType, } from "./item.types";
import { CatalogBarcode } from "@src/planogram/shared/interfaces/models/CatalogProduct";

export function addProductAction(shelf: PlanogramElementId, product: CatalogBarcode, placement?: PlacementObject,
    /* Barak 21.12.20 - Add linking item option */
    linkedItemUp?: PlanogramElementId,
    linkedItemDown?: PlanogramElementId,
    shelfLevel?: number,
    /********************************************/
): addItemActionType {
    return {
        type: ITEM_ACTIONS.ADD_PRODUCT,
        shelf,
        product,
        placement,
        /* Barak 21.12.20 - Add linking item option */
        linkedItemUp,
        linkedItemDown,
        shelfLevel,
        /********************************************/
    }
}
export function duplicateItemAction(shelf: PlanogramElementId, item: PlanogramItem): duplicateItemActionType {
    return {
        type: ITEM_ACTIONS.DUPLICATE_ITEM,
        shelf,
        item
    }
}
export function moveShelfItem(shelf: PlanogramElementId, item: PlanogramElementId): moveItemActionType {
    return {
        type: ITEM_ACTIONS.MOVE_ITEM,
        shelf,
        item
    }
}
export function switchItemsAction(item: PlanogramElementId, remote: PlanogramElementId): switchItemsActionType {
    return {
        type: ITEM_ACTIONS.SWITCH_ITEMS,
        // shelf,
        item,
        remote
    }
}
export function deleteItemAction(shelf: PlanogramElementId, item: PlanogramElementId): deleteItemActionType {
    return {
        type: ITEM_ACTIONS.DELETE_ITEM,
        shelf,
        item
    }
}

export function editShelfItemAction(item: PlanogramElementId, placement: PlacementObject, barcode?: CatalogBarcode): EditItemActionType {
    return {
        type: ITEM_ACTIONS.EDIT_ITEM_PLACEMENT,
        // shelf,
        item,
        placement,
        barcode
    }
}