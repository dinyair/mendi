import { PlanogramActions, PlanogramShelf, PlanogramItem } from "../../planogram.types";
import { ITEM_ACTIONS } from "./item.types";
import { PLANOGRAM_ID, ProductDefaultDimensions } from "../../planogram.defaults";

export function itemReducer(state: PlanogramShelf, action: PlanogramActions): PlanogramShelf {
    switch (action.type) {
        case ITEM_ACTIONS.ADD_PRODUCT: {
            if (state.id === action.shelf) {
                /* Barak 29.9.20 */
                // get the distance from start of shelf based on all previous items
                let distFromStart: any = 0;
                let distFromTop: any = 0;
                if (action.placement) {
                    distFromStart = action.placement.distFromStart;
                    distFromTop = action.placement.distFromTop;
                    console.log('adding item', action.product, 'distFromStart', action.placement.distFromStart);
                }
                if (state.items.length > 0 && distFromStart == 0) {
                    distFromStart = 0;
                    for (let i = 0; i < state.items.length; i++) {
                        if (state.items[i] && state.items[i].placement && state.items[i].placement.distFromStart) {
                            let num: any = state.items[i].placement.distFromStart;
                            distFromStart += num;
                        }
                    }
                }
                /*****************/
                return {
                    ...state,
                    items: [
                        ...state.items.map((item, i) => ({
                            ...item,
                            id: item.id.length === 0 || (item.id.length > 0 && parseInt(item.id.substring(item.id.indexOf('I') + 1)) < 100) ? state.id + PLANOGRAM_ID.ITEM + i : item.id
                        })),
                        {
                            id: state.id + PLANOGRAM_ID.ITEM + state.items.length,
                            placement: action.placement || {
                                faces: 1,
                                row: 1,
                                stack: 1,
                                manual_row_only: 0,
                                position: 0,
                                pWidth: ProductDefaultDimensions.width,
                                pHeight: ProductDefaultDimensions.height,
                                pDepth: ProductDefaultDimensions.depth,
                                distFromStart: distFromStart,
                                distFromTop: distFromTop
                            },
                            product: action.product,
                            lowSales: false,
                            missing: 0,
                            branch_id: 0,
                            linkedItemUp: action.linkedItemUp || null,
                            linkedItemDown: action.linkedItemDown || null,
                            shelfLevel: action.shelfLevel || 1,
                            /********************************************/
                        }
                    ]
                }
            }
            return state;
        }
        // case ITEM_ACTIONS.SWITCH_ITEMS: {
        //     if (state.id === action.shelf) {
        //         const items = state.items;
        //         const base = action.item;
        //         const remote = action.remote;
        //         const baseIndex = items.findIndex(s => base === s || base.id === s.id);
        //         const remoteIndex = items.findIndex(s => remote === s || remote.id === s.id);
        //         if (baseIndex === -1 || remoteIndex === -1) {
        //             return state;
        //         }
        //         const _items = [...items];
        //         [_items[baseIndex], _items[remoteIndex]] = [remote, base];

        //         return {
        //             ...state,
        //             items: _items.map((item, i) => ({
        //                 ...item,
        //                 id: state.id + PLANOGRAM_ID.ITEM + i
        //             }))
        //         }
        //     }
        //     return state;
        // }
        case ITEM_ACTIONS.DUPLICATE_ITEM: {
            if (state.id === action.shelf) {
                return {
                    ...state,
                    items: [
                        ...state.items.map((item, i) => ({
                            ...item,
                            /* Barak 26.11.20 * id: state.id + PLANOGRAM_ID.ITEM + i */
                            /* Barak 26.11.20 */ id: item.id.length === 0 || (item.id.length > 0 && parseInt(item.id.substring(item.id.indexOf('I') + 1)) < 100) ? state.id + PLANOGRAM_ID.ITEM + i : item.id
                        })),
                        {
                            ...action.item,
                            id: state.id + PLANOGRAM_ID.ITEM + (state.items.length),
                        }
                    ]
                }
            }
            return state;
        }
        case ITEM_ACTIONS.DELETE_ITEM: {
            // if (state.id === action.shelf) {
            const itemIndex = state.items.findIndex(s => s.id === action.item);
            if (itemIndex === -1)
                return state;
            return {
                ...state,
                items: [
                    ...state.items.slice(0, itemIndex),
                    ...state.items.slice(itemIndex + 1),
                ].map((item, i) => ({
                    ...item,
                    d: item.id.length === 0 || (item.id.length > 0 && parseInt(item.id.substring(item.id.indexOf('I') + 1)) < 100) ? state.id + PLANOGRAM_ID.ITEM + i : item.id
                }))
            }
        }
        case ITEM_ACTIONS.EDIT_ITEM_PLACEMENT: {
            const exists = state.items.findIndex(item => item.id === action.item);
            if (exists === -1)
                return state;
            return {
                ...state,
                items: state.items.map((item, i) => {
                    if (item.id === action.item)
                        return {
                            ...item,
                            barcode: action.barcode || item.product,
                            placement: action.placement
                        }
                    return item
                })
            }
        }
        default:
            return state;
    }
}
