import { PlanogramViewState, PlanogramViewActionTypes } from "./store/store.types";
import { PlanogramSidebarState, PlanogramSidebarActionTypes } from "./sidebar/sidebar.types";
import { AisleActionTypes } from "./store/aisle/aisle.types";
import { SectionActionTypes } from "./store/section/section.types";
import { ShelfActionTypes } from "./store/shelf/shelf.types";
import { ItemActionTypes } from "./store/item/item.types";
import { VirtualStoreState, ColorMap } from "./virtualize/virtualize.reducer";
import { CatalogBarcode } from "@src/planogram/shared/interfaces/models/CatalogProduct";
import { PlanogramDisplayState, PlanogramDisplayColorBy } from "./display/display.types";
import { ProductDetailState, ProductDetailActions } from "./product-details/product-details.type";

export enum SHELF_TYPE {
    SINGLE = "SINGLE",
    COMBINED = "COMBINED"
}

// export namespace Planogram {
export type PlanogramElementId = string;
export type PlanogramElement = {
    id: PlanogramElementId,
    dimensions: DimensionObject,
    name?: string
}
export interface DimensionObject {
    width: number,
    height: number,
    depth: number,
    weight?: number,
    /* Barak 11.11.2020 */  heightFromFloor?: number,
}
export interface ScateredDimensionObject {
    width?: number,
    height?: number,
    depth?: number,
    weight?: number,
}
export interface PlacementObject {
    faces: number,
    stack: number,
    row: number,
    manual_row_only: number, // 0 (regular calculation of row) or 1 (only user input can change the row)
    /* Barak & Osher 10.9.20 - support rotating products*/
    position: number,
    pWidth: number | undefined,
    pHeight: number | undefined,
    pDepth: number | undefined
    /****************************************************/
    /* Barak 29.9.20*/ distFromStart: number | undefined,
    /* Barak 26.11.20*/ distFromTop: number | null | undefined,
}

export type PlanogramStore = {
    store_id: number,
    name?: string,
    branch_id: number,
    aisle_counter: number,
    aisles: PlanogramAisle[],
    /* Barak 9.6.2020 - Add averages calculation status to Store */
    statusCalcInv: number,
    calcInvDays: number,
    /* osher 14.9.2020 - Add flag to display item tags by weekly sales or by minimum stock count */
    itemTagFlag: number,
    newplano:boolean,
    /* Barak 23.6.2020 - Add statusCalMlay options to the averages tab */
    Status0Percent: number,
    Status1Percent: number,
    Status2Percent: number,
    Status3Percent: number,
    Status4Percent: number,
    /* Barak 8.9.2020 - Add default sizes for section and shelf for this store */
    Section_height: number,
    Section_width: number,
    Section_depth: number,
    Shelf_height: number,
    Shelf_width: number,
    Shelf_depth: number,
    /* Barak 8.12.20 - add full product map to enable search */ storeProductMap: any,
}

export interface PlanogramAisle extends PlanogramElement {
    aisle_id: number,
    index: number,
    aisle_number: number,
    name: string,
    sections: PlanogramSection[],
    aisle_plano: number | null,
    /* Barak 2.8.20 */ colorMap: ColorMap,
    /* Barak 7.9.20 */
    suppColorMap: ColorMap,
    classColorMap: ColorMap,
    groupColorMap: ColorMap,
    subGroupColorMap: ColorMap,
    segmentColorMap: ColorMap,
    newplano: boolean,
    /****************/
    /* Barak 6.12.20 */ modelColorMap: ColorMap,
    /* Barak 8.12.20 */
    empty: boolean,
    aisle_version: number,
    /*****************/
}

export interface PlanogramSection extends PlanogramElement {
    section_id?: number,
    shelves: PlanogramShelf[],
    /* Barak 3.12.20 */ section_layout: number
}

export interface PlanogramShelf extends PlanogramElement {
    type?: SHELF_TYPE.SINGLE,
    // shelf_id?: number,
    items: PlanogramItem[],
    // height_position: string | null,
    section_height: number | null,
    margin_bottom: number | null,
    /* Barak 29.9.2020 - Add distFromStart */ distFromStart: number,
    /* Barak 26.11.2020 - Add freezer flag */ freezer: number,
    offset?: number,
}
export interface PlanogramCombinedShelf extends PlanogramElement {
    type: SHELF_TYPE.COMBINED,
    shelves: PlanogramShelf[]
}

export interface PlanogramItem {
    id: PlanogramElementId,
    // item_id?: number,
    placement: PlacementObject,
    product: CatalogBarcode,
    /* Barak 24.6.2020 - Add lowSales flag to the item */
    lowSales: boolean,
    /* Barak 23.7.2020 - Add missing shelf amount to the item */
    missing: number,
    /* Barak 28.6.2020 - Add branch_id */
    branch_id: number,
    /* Barak 21.12.20 - Add linking item option */
    linkedItemUp: PlanogramElementId | null,
    linkedItemDown: PlanogramElementId | null,
    shelfLevel: number,
    /********************************************/
}
// export type PlanogramProduct = number;
export interface PlanogramSidebarProduct {
    id: number,
    barcode: number,
    name: string,
    dimensions: ScateredDimensionObject
}
// }

export interface PlanogramState {
    display: PlanogramDisplayState,
    store: PlanogramViewState,
    // storeList: any[],
    Zoom: number,
    sidebar: PlanogramSidebarState,
    productDetails: ProductDetailState,
    virtualStore: VirtualStoreState,
    // /* Barak 21.4.20 - Add multi selection for delete */ multiSelectId: string[],
    // /* Barak 4.8.20 - Add currentSelectedId for movment with arrows */ currentSelectedId: string,
}


export enum PLANOGRAM_ACTIONS {
    DONE_LOADING = 'DONE_LOADING',
    SET_LOADING = "SET_LOADING",
    SET_DISPLAY_AISLE = "SET_DISPLAY_AISLE",
    SET_DISPLAY_BRANCH = "SET_DISPLAY_BRANCH",
    SET_PRODUCT_DETAILER = "SET_PRODUCT_DETAILER",
    /* Barak 21.4.20 - Add multi selection for delete */
    SET_PRODUCT_MULTISELECTID = "SET_PRODUCT_MULTISELECTID",
    /**************************************************/
    /* Barak 29.9.20 - Add distance from start of shelf on dropped item */
    SET_PRODUCT_DISTFROMSTART = "SET_PRODUCT_DISTFROMSTART",
    /**************************************************/
    /* Barak 4.8.20 - Add currentSelectedId for movment with arrows */
    SET_PRODUCT_CURRENTSELECTEDID = "SET_PRODUCT_CURRENTSELECTEDID",
    /**************************************************/
    HIDE_PRODUCT_DETAILER = "HIDE_PRODUCT_DETAILER",
    SHOW_SALES_REPORT = "SHOW_SALES_REPORT",
    HIDE_SALES_REPORT = "HIDE_SALES_REPORT",
    TOGGLE_SHELF_ITEMS = "TOGGLE_SHELF_ITEMS",
    TOGGLE_ROW_ITEMS = "TOGGLE_ROW_ITEMS",
    TOGGLE_SETTINGS = "TOGGLE_SETTINGS",
    TOGGLE_BAD_PRODUCTS_MARKER = "TOGGLE_BAD_PRODUCTS_MARKER",
    TOGGLE_ARCHIVE_PRODUCTS_MARKER = "TOGGLE_ARCHIVE_PRODUCTS_MARKER",
    COLOR_BY = "COLOR_BY",
    /* Barak 29.9.20 - Align all items to left or not align */
    TOGGLE_ALIGN_LEFT = "TOGGLE_ALIGN_LEFT",
    /**************************************************/
    /* Barak 22.11.20 - Add saving array for aisle, allowing to do
       Ctrl+Z to move back and Ctrl+Y to move forward */
    SET_AISLE_SAVE_ARRAY = "SET_AISLE_SAVE_ARRAY",
    SET_AISLE_SAVE_INDEX = "SET_AISLE_SAVE_INDEX",
    SET_SECTION_GROUPS = "SET_SECTION_GROUPS",
    /* Barak 24.12.20 - allow hiding of sales tags */ TOGGLE_SALE_TAGS = "TOGGLE_SALE_TAGS",
    /* Barak 29.12.20 - display color pop up */
    SHOW_COLOR_POPUP = "SHOW_COLOR_POPUP",
    HIDE_COLOR_POPUP = "HIDE_COLOR_POPUP",
    SET_SUMMARY_OBJECTS = "SET_SUMMARY_OBJECTS",
    /*****************************************/
}
export type SetLoadingActionType = { type: PLANOGRAM_ACTIONS.SET_LOADING, isLoading: boolean, };
export type SetDisplayAisleActionType = { type: PLANOGRAM_ACTIONS.SET_DISPLAY_AISLE, aisleIndex: number };
export type SetDisplayBranchActionType = { type: PLANOGRAM_ACTIONS.SET_DISPLAY_BRANCH, branchId: number };
/* Barak 24.6.2020 - Remove * export type SetProductDetailerActionType = { type: PLANOGRAM_ACTIONS.SET_PRODUCT_DETAILER, barcode: CatalogBarcode };*/
/* Barak 24.6.2020 - Add lowSales to the SetProductDetailerActionType function */
/* Barak 23.7.20 export type SetProductDetailerActionType = { type: PLANOGRAM_ACTIONS.SET_PRODUCT_DETAILER, barcode: CatalogBarcode, lowSales: boolean };*/
/* Barak 23.7.20 */ export type SetProductDetailerActionType = { type: PLANOGRAM_ACTIONS.SET_PRODUCT_DETAILER, barcode: CatalogBarcode, lowSales: boolean, shelfFaces: number, missing: number, netw: string, userPhone: string };
/* Barak 21.4.20 - Add multi selection for delete */ export type SetMultiSelectIdActionType = { type: PLANOGRAM_ACTIONS.SET_PRODUCT_MULTISELECTID, multiSelectId: string[] };
/* Barak 4.8.20 - Add currentSelectedId for movment with arrows */ export type SetCurrentSelectedIdActionType = { type: PLANOGRAM_ACTIONS.SET_PRODUCT_CURRENTSELECTEDID, currentSelectedId: string };
/* Barak 29.9.20 - Align all items to left or not align */
export type ToggleAlignLeftAction = { type: PLANOGRAM_ACTIONS.TOGGLE_ALIGN_LEFT };
export type HideProductDetailerActionType = { type: PLANOGRAM_ACTIONS.HIDE_PRODUCT_DETAILER };
export type ShowSalesReportAction = { type: PLANOGRAM_ACTIONS.SHOW_SALES_REPORT };
export type HideSalesReportAction = { type: PLANOGRAM_ACTIONS.HIDE_SALES_REPORT };
/* Barak 29.12.20 - display color pop up */
export type ShowColorPopUpAction = { type: PLANOGRAM_ACTIONS.SHOW_COLOR_POPUP };
export type HideColorPopUpAction = { type: PLANOGRAM_ACTIONS.HIDE_COLOR_POPUP };
export type SetSummaryObjectsActionType = { type: PLANOGRAM_ACTIONS.SET_SUMMARY_OBJECTS, summaryObjects: any, };
/*****************************************/
export type ToggleShelfItemsAction = { type: PLANOGRAM_ACTIONS.TOGGLE_SHELF_ITEMS };
/* Barak 24.12.20 */ export type ToggleSaleTagsAction = { type: PLANOGRAM_ACTIONS.TOGGLE_SALE_TAGS };
export type ToggleRowItemsAction = { type: PLANOGRAM_ACTIONS.TOGGLE_ROW_ITEMS };
export type ToggleSettingsAction = { type: PLANOGRAM_ACTIONS.TOGGLE_SETTINGS };
export type ToggleBadProductsMarkerAction = { type: PLANOGRAM_ACTIONS.TOGGLE_BAD_PRODUCTS_MARKER };

/* Barak 17.6.2020 - Add action to mark Archive products */
export type ToggleArchiveProductsMarkerAction = { type: PLANOGRAM_ACTIONS.TOGGLE_ARCHIVE_PRODUCTS_MARKER };
/*********************************************************/
/* Barak 8.12.20 - add store sectionGroups from DB */
export type SetSectionGroupsAction = { type: PLANOGRAM_ACTIONS.SET_SECTION_GROUPS, sectionGroups: any };
/*********************************************************/

export type SetColorBy = {
    type: PLANOGRAM_ACTIONS.COLOR_BY,
    color_by: PlanogramDisplayColorBy
};

/* Barak 22.11.20 - Add saving array for aisle, allowing to do 
   Ctrl+Z to move back and Ctrl+Y to move forward */
export type SetAisleSaveArrayActionType = { type: PLANOGRAM_ACTIONS.SET_AISLE_SAVE_ARRAY, aisleSaveArray: PlanogramAisle[] };
export type SetAisleSaveIndexActionType = { type: PLANOGRAM_ACTIONS.SET_AISLE_SAVE_INDEX, aisleSaveIndex: number };
/*************************************************************/


export type PlanogramStoreAction = SetColorBy | ToggleRowItemsAction | AisleActionTypes | SectionActionTypes | ShelfActionTypes | ItemActionTypes;
export type PlanogramActions = ToggleArchiveProductsMarkerAction | ToggleBadProductsMarkerAction | ToggleSettingsAction | ToggleShelfItemsAction |
    ShowSalesReportAction | HideSalesReportAction | SetDisplayAisleActionType | HideProductDetailerActionType | SetProductDetailerActionType |
    SetDisplayBranchActionType | SetLoadingActionType | PlanogramStoreAction | PlanogramViewActionTypes | ProductDetailActions |
    PlanogramSidebarActionTypes | SetMultiSelectIdActionType | SetCurrentSelectedIdActionType | ToggleAlignLeftAction | SetAisleSaveArrayActionType |
    SetAisleSaveIndexActionType | SetSectionGroupsAction | ToggleSaleTagsAction | ShowColorPopUpAction | HideColorPopUpAction |
    SetSummaryObjectsActionType;