
export const ColorMap = {
    green: "#0be30b",
    gold: "#ffd700",
    cyan: "#00ffff",
    blue: "#0000ff",
    fuchsia: "#ff00ff",
    pink: "#ffc0cb",
    indigo: "#4b0082",
    // navy: "#000080",
    olive: "#808000",
    orange: "#ffa500",
    red: "#ff0000",
    // magenta: "#ff00ff",
    teal: "#008080",
    darkblue: "#00008b",
    khaki: "#f0e68c",
    beige: "#f5f5dc",
    brown: "#a52a2a",
    // maroon: "#800000",
    purple: "#800080",
    // violet: "#800080",
    // white: "#ffffff",
    honeydew: "#FFCC99",
    yellow: "#ffff00",
    // darkcyan: "#008b8b",
    // darkgrey: "#a9a9a9",
    darkgreen: "#006400",
    darkkhaki: "#bdb76b",
    darkmagenta: "#8b008b",
    darkolivegreen: "#556b2f",
    darkorange: "#ff8c00",
    darkorchid: "#9932cc",
    darkred: "#8b0000",
    darksalmon: "#e9967a",
    darkviolet: "#9400d3",
    iron: "#808080",
    // lime: "#00ff00",
    lightblue: "#add8e6",
    lightcyan: "#e0ffff",
    lightgreen: "#90ee90",
    lightgrey: "#d3d3d3",
    lightpink: "#ffb6c1",
    lightyellow: "#ffffe0",
    // azure: "#f0ffff",
    silver: "#c0c0c0",
    black: "#000000",
};
export const ColorList = Object.values(ColorMap);

export const getRandomUniqueColor = (pre?: Set<string>): string => {
    let list = ColorList;
    // if (pre && typeof pre === "string")
    //     list = list.filter(v => pre !== v);
    if (pre && pre instanceof Set)
        list = list.filter(v => !pre.has(v));
    if (list.length === 0)
        return getRandomUniqueColor();
    return list[Math.round(Math.random() * (list.length - 1))];
}

export const getIndexUniqueColor = (index: number): string => {
    let list = ColorList;
    return list[index];
}
