
import { ZOOM_ACTION } from './zoom.actions' 
export const initialZoommState: number = 0.4

export function ZoomReducer(state = initialZoommState, action: any): any {
    switch (action.type) {
        case ZOOM_ACTION.SET_ZOOM:
            return action.zoom
        default:
            return state
    }
}
