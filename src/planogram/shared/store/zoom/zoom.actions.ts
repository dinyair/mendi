import { AuthUser } from "@src/planogram/shared/interfaces/models/User";
export enum ZOOM_ACTION {
    SET_ZOOM = "SET_ZOOM",
}
export const setZoom = (zoom: number): any => ({
    type: ZOOM_ACTION.SET_ZOOM,
    zoom
})
