import { AuthUser } from "@src/planogram/shared/interfaces/models/User";
import { AUTH_ACTION, SetUserAction, LogoutAction, SetAuthAction, RemoveAuthAction } from "./auth.types";

export const setAuth = (user: AuthUser, token: string): SetAuthAction => ({
    type: AUTH_ACTION.SET_AUTH,
    user: user,
    token: token,
})
export const removeAuth = (): RemoveAuthAction => ({
    type: AUTH_ACTION.REMOVE_AUTH,
})
export const setUser = (user?: any | AuthUser): SetUserAction => ({
    type: AUTH_ACTION.SET_USER,
    user: user
})
export const logoutRedux = (): LogoutAction => ({
    type: AUTH_ACTION.LOGOUT
})
