import { AUTH_ACTION, AuthState, AuthActions } from "./auth.types"
import { AuthUser } from "@src/planogram/shared/interfaces/models/User"
import { loadStorageAccessToken, loadStorageUser } from "@src/planogram/shared/auth/auth.service";

export const initialUser: AuthUser = {
    id: 47,
    name: "Admin",
    level: 0,
    tafkid: "Admin",
    tel: "0551234123",
    db: "nyocha",
    branches: [24]
}

// const user = loadStorageUser();
// const accessToken = loadStorageAccessToken();

export const initialAuthState: AuthState = {}
// if (user && accessToken) {
//     initialState.user = user;
//     initialState.token = accessToken;
// }

export function authReducer(state = initialAuthState, action: AuthActions): AuthState {
   
    switch (action.type) {
        case AUTH_ACTION.SET_AUTH:
            return {
                ...state,
                user: action.user,
                token: action.token,
            }
        case AUTH_ACTION.SET_TOKEN:
            return {
                ...state,
                token: action.token
            }
        case AUTH_ACTION.SET_USER:
            return {
                ...state,
                user: action.user
            }
        case AUTH_ACTION.REMOVE_AUTH:
        case AUTH_ACTION.LOGOUT:
            return {}
        default:
            return state
    }
}
