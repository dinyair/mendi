import { User, AuthUser } from "@src/planogram/shared/interfaces/models/User";
import { AnyAction } from "redux";


export type AuthState = {
    user?: AuthUser,
    token?: string,
    refresh_token?: string
}

export enum AUTH_ACTION {
    SET_USER = "SET_USER",
    LOGOUT = "LOGOUT",
    SET_TOKEN = "SET_TOKEN",
    SET_AUTH = "SET_AUTH",
    REMOVE_AUTH = "REMOVE_AUTH",
}

export type RemoveAuthAction = {
    type: AUTH_ACTION.REMOVE_AUTH,
}
export type SetAuthAction = {
    type: AUTH_ACTION.SET_AUTH,
    token?: string,
    user?: AuthUser,
}
export type SetTokenAction = {
    type: AUTH_ACTION.SET_TOKEN,
    token?: string
}
export type SetUserAction = {
    type: AUTH_ACTION.SET_USER,
    user?: AuthUser
}
export type LogoutAction = {
    type: AUTH_ACTION.LOGOUT
}

export type AuthActions = SetUserAction | LogoutAction | SetTokenAction | SetAuthAction | RemoveAuthAction;