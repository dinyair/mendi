import { FetchPlanogramAisleResponse, FetchPlanogramStoreResponse } from "../interfaces/responses/PlanogramResponse";
import { DimensionObject, PlanogramStore, PlanogramElementId, PlacementObject, PlanogramAisle, ScateredDimensionObject, PlanogramItem } from "../store/planogram/planogram.types";
import { getRequest, putRequest, postRequest, deleteRequest  } from "@src/http";

import { CatalogProduct, CatalogBarcode } from "@src/planogram/shared/interfaces/models/CatalogProduct";
import { ProductHourSales, ProductWeekSales } from "../interfaces/responses/planogram.dto";
import { AisleId } from "@src/planogram/shared/store/planogram/store/aisle/aisle.types";
import { blankPlanogramAisle } from "@src/planogram/shared/store/planogram/planogram.defaults";
import * as moment from 'moment';
import { ExistingFileRecord } from "@src/planogram/planogramImageUpload/PlanogramImageUpload";
import { compress, decompress } from 'lz-string'

import { uiNotify } from "@src/planogram/shared/components/Toast";

export type PlanogramStoreRecord = {
    id: number,
    branch_id: number,
    priority: 0 | 1 | 2,
    name?: string,
    aisle_counter: number,
    version?: string,
}


export const fetchStores = async (): Promise<PlanogramStoreRecord[]> => {
    const resp = await getRequest<{
        stores: PlanogramStoreRecord[]
    }>('planogram/stores');
    return resp.stores;
}
/* Barak 19.1.20 - Add getImageVersion */
export const getImageVersion = async (): Promise<number> => {
    const resp = await getRequest<{
        version: number
    }>('planogram/imageVersion');
    return resp.version;
}
/***************************************/
/* Barak 11.1.21 - Add makeImageVersion */
export const makeImageVersion = async (clientImage: string, position: number): Promise<any> => {
    const resp = await getRequest<{
        version: any
    }>('planogram/imageVersion/position', {
        params: {
            clientImage,
            position
        }
    });
    return resp.version;
}
/***************************************/
export const fetchBranchStores = async (branchId: number | string): Promise<PlanogramStoreRecord[]> => {
    const resp = await getRequest<{
        stores: PlanogramStoreRecord[]
    }>('planogram/' + branchId);
    return resp.stores;
}
/*******  Barak 9.6.2020 - remove this function  *******/
// export const productPredictedSales = async (barcode: CatalogBarcode, branch_id: string | number): Promise<ProductWeekSales | undefined> => {
//     const resp = await getRequest<{
//         status: "ok" | "empty",
//         sale: ProductWeekSales,
//     }>('planogram/sales/barcode', {
//         params: {
//             barcode,
//             branch_id
//         }
//     });
//     return resp.sale;
// }
/*******************************************************/
/*******  Barak 9.6.2020 - Add statusCalcInv, calcInvDays to function call  *******/
export const productPredictedSales = async (barcode: CatalogBarcode, branch_id: string | number, statusCalcInv: number, calcInvDays: number): Promise<ProductWeekSales | undefined> => {
    const resp = await getRequest<{
        status: "ok" | "empty",
        sale: ProductWeekSales,
    }>('planogram/sales/barcode', {
        params: {
            barcode,
            branch_id,
            statusCalcInv,
            calcInvDays
        }
    });
    return resp.sale;
}
/**********************************************************************************/
/* Barak 13.5.2020 - Add statusCalMlay check to barcode*/
export const getStatusCalM = async (barcode: number | string): Promise<number> => {
    // console.log('planogram/statusCalMlay',barcode);
    const resp = await getRequest<{
        statusCalMlay: number
    }>('planogram/statusCalMlay/' + barcode);
    return resp.statusCalMlay;
}
/*******************************************************/
/*******  Barak 9.6.2020 - remove this function  *******/
// export const productsPredictedSales = async (branchId: number, barcodes: CatalogBarcode[]): Promise<ProductWeekSales[]> => {
//     console.log('productsPredictedSales branchId', branchId);
//     const resp = await postRequest<{ status: string, sales: ProductWeekSales[] }>('planogram/sales/batch', {
//         branchId,
//         /* Barak 25.2.20 - Remove barcodes array from call to server. Shuki updated function getBranchSalesBulk on the server (catalog.service.js)
//         barcodes 
//         */
//     });
//     return resp.sales;
// }
/*******************************************************/
/*******  Barak 9.6.2020 - Add statusCalcInv, calcInvDays to function call  *******/
export const productsPredictedSales = async (branchId: number, statusCalcInv: number, calcInvDays: number): Promise<ProductWeekSales[]> => {
    console.log('productsPredictedSales branchId', branchId);
    const resp = await postRequest<{ status: string, sales: ProductWeekSales[] }>('planogram/sales/batch', {
        branchId,
        statusCalcInv,
        calcInvDays
    });
    console.log('productsPredictedSales resp.sales', resp.sales);
    return resp.sales;
}
/**********************************************************************************/

export const updateProductDimensions = async (barcode: number, dimensions: DimensionObject): Promise<void> => {
    await putRequest<any>('planogram/catalog/' + barcode, {
        barcode,
        dimensions
    });
}


export const fetchPlanogramStore = async (storeId: number | string, netw: string): Promise<PlanogramStore> => {
    /* Barak 8.12.20 - replaced in server from getStore to getEmptyStore */
    const resp = await getRequest<FetchPlanogramStoreResponse>('planogram/store/' + (storeId || "") + '/New');
    if (!resp.store) throw new Error("No store was found.");
    /* Barak 8.12.20 - before returning the store, check if any of the aisles are in localStorage */
    // add aisle 0 at the begining of the aisle list of this store
    let emptyAisle = blankPlanogramAisle;
    emptyAisle.empty = true;
    resp.store.aisles.unshift(emptyAisle);
    // load any saved aisles from localStorage (if there are any)
    let localAisleMap: any = {};
    let localAisleMapS = localStorage.getItem('localAisleMap');
    let localAisleMapB = null;
    if (localAisleMapS) localAisleMapB = decompress(localAisleMapS);
    if (localAisleMapB) localAisleMap = JSON.parse(localAisleMapB);
    /* Barak 14.12.20 *********************
    let localSalesMap: any = {};
    let localSalesMapS = localStorage.getItem('localSalesMapAvg');
    if (localSalesMapS) localSalesMap = JSON.parse(localSalesMapS);
    console.log('localSalesMapAvg', localAisleMap);
    **************************************/
    for (let i = 0; i < resp.store.aisles.length; i++) {
        // console.log('localAiidsleMap',resp.store.aisles[i].id,'localAisleMap',localAisleMap[resp.store.aisles[i].id]);
        if (localAisleMap[resp.store.aisles[i].id] && localAisleMap[resp.store.aisles[i].id].db === netw) {
            // the aisle is saved in localStorage - check version number
            let dbVersion = await getAisleVersion(resp.store.aisles[i].aisle_id);
            if (dbVersion === localAisleMap[resp.store.aisles[i].id].version) {
                // version in DB and in local storage are the same - add the local storage aisle to the store
                resp.store.aisles[i] = localAisleMap[resp.store.aisles[i].id].aisle;
                /* Barak 14.12.20 *********************
                // check if we have the AVG sales for this aisle for this date in local storage
                // check if localSalesMap has a record for this aisle for this date
                if (localSalesMap[resp.store.aisles[i].id] != undefined && localSalesMap[resp.store.aisles[i].id].date === moment(new Date()).format('YYYY-MM-DD')) {
                    // there are sales of the appropriate date in local storage - place them in aisle
                    resp.store.aisles[i].productSales = localSalesMap[resp.store.aisles[i].id].sales;
                }
                **************************************/
            }
        }
    }
    /**********************************************************************************************/
    return resp.store;
}
/* Barak 8.12.20 - get single aisle from server */
export const fetchPlanogramAisle = async (storeId: number | string, aisleId: number, netw: string): Promise<any> => {
    const resp = await getRequest<FetchPlanogramAisleResponse>('planogram/aisle/' + (storeId || "") + '/' + (aisleId) + '/New');
    if (!resp.aisle) throw new Error("No aisle was found.");
    // before returning the aisle make sure we save in to localAisleMap in localStoirage
    let localAisleMap: any = {};
    let localAisleMapS = localStorage.getItem('localAisleMap');
    let localAisleMapB = null;
    if (localAisleMapS) localAisleMapB = decompress(localAisleMapS);
    if (localAisleMapB) localAisleMap = JSON.parse(localAisleMapB);
    if (!resp.aisle.aisle_version) resp.aisle.aisle_version = await getAisleVersion(resp.aisle.aisle_id);
    localAisleMap[resp.aisle.id] = { version: resp.aisle.aisle_version, db: netw, aisle: resp.aisle };
    localStorage.setItem('localAisleMap', compress(JSON.stringify(localAisleMap)));
    // return both aisle and sales
    return resp.aisle;
}
/* Barak 31.1.21 - get single aisle blob from planogram_backup by Id */
export const fetchPlanogramBlobById = async (blobId: number | string, storeId: number | string, aisleId: number, netw: string): Promise<any> => {
    const resp = await getRequest<FetchPlanogramAisleResponse>('planogram/aisle/blob/' + (blobId || ""));
    if (!resp.aisle) throw new Error("No aisle was found.");
    // if (resp.aisle.aisle_id != aisleId) throw new Error("This is not the correct Aisle Id - Please try again.");
    if (resp.aisle.aisle_id != aisleId) {
        uiNotify("This is not the correct Aisle Id - Please try again.", "error", 5000);
        return undefined;
    }
    else {
        console.log('fetchPlanogramBlobById resp', resp, 'storeId', storeId, 'aisleId', aisleId);
        // before returning the aisle make sure we save in to localAisleMap in localStoirage
        let localAisleMap: any = {};
        let localAisleMapS = localStorage.getItem('localAisleMap');
        let localAisleMapB = null;
        if (localAisleMapS) localAisleMapB = decompress(localAisleMapS);
        if (localAisleMapB) localAisleMap = JSON.parse(localAisleMapB);
        if (!resp.aisle.aisle_version) resp.aisle.aisle_version = await getAisleVersion(resp.aisle.aisle_id);
        localAisleMap[resp.aisle.id] = { version: resp.aisle.aisle_version, db: netw, aisle: resp.aisle };
        localStorage.setItem('localAisleMap', compress(JSON.stringify(localAisleMap)));
        // return both aisle and sales
        return resp.aisle;
    }
}
/****************************************************************/
/* Barak 14.12.20 - get single aisle sales */
export const fetchSingleAisleSalesAvg = async (storeId: number | string, aisleId: number): Promise<any> => {
    const resp = await getRequest<{ status: string, sales: any }>('planogram/aisle_sales_avglast/' + (storeId || "") + '/' + (aisleId));
    console.log('fetchSingleAisleSalesAvg resp', resp);
    // return sales AvgLast
    return resp.sales;
}
export const fetchSingleAisleSalesSl = async (storeId: number | string, aisleId: number): Promise<any> => {
    const resp = await getRequest<{ status: string, sales: any }>('planogram/aisle_sales_sl/' + (storeId || "") + '/' + (aisleId));
    // return sales SL (Hourly)
    return resp.sales;
}
export const productsPredictedSalesAvg = async (storeId: number): Promise<ProductWeekSales[]> => {
    const resp = await postRequest<{ status: string, sales: ProductWeekSales[] }>('planogram/sales_avg/batch', {
        storeId,
    });
    return resp.sales;
}
export const productsPredictedSalesSl = async (storeId: number): Promise<ProductHourSales[]> => {
    const resp = await postRequest<{ status: string, sales: ProductHourSales[] }>('planogram/sales_sl/batch', {
        storeId,
    });
    return resp.sales;
}
/*******************************************/
/* Barak 15.12.20 */
export const fetchFileList = async (netw: string): Promise<ExistingFileRecord[]> => {
    const resp = await getRequest<{ status: string, files: ExistingFileRecord[] }>('planogram/network/files/' + (netw || ""));
    // return file list
    return resp.files;
}
export const uploadFiles = async (data: any): Promise<any> => {
    const resp = await postRequest<{ status: string }>('planogram/network/files', {
        data,
    });
    // return file list
    return resp;
}
/******************/
export const getAisleVersion = async (aisleId: number): Promise<number> => {
    const resp = await getRequest<{ status: string, version: number }>('planogram/aisle_version/' + (aisleId));
    return resp.version;
}
export const getSectionGroups = async (storeId: number): Promise<any> => {
    const resp = await getRequest<{ status: string, sectionGroups: any }>('planogram/section_groups/' + (storeId));
    return resp.sectionGroups;
}
/************************************************/
export const createNewStore = async (branchId: number | string): Promise<PlanogramStore> => {
    const resp = await postRequest<{ status: string, store: PlanogramStore }>(`planogram/${branchId}/new`, {});
    return resp.store;
}
export const renameStore = async (storeId: number, name: string) => {
    await putRequest<{ status: string, store: PlanogramStore }>(`planogram/store/${storeId}/name`, {
        name
    });
}
export const savePlanogramStore = async (store: PlanogramStore): Promise<PlanogramStore> => {
    const resp = await postRequest<{ status: string, store: PlanogramStore }>('planogram/store/' + (store.store_id), {
        store
    });
    return resp.store;
}

/* Barak 9.6.2020 - Save statusCalcInv, calcInvDays for store */
export const savePlanogramStoreStatusCalc = async (store: PlanogramStore): Promise<any> => {
    const resp = await postRequest<{ status: string, store: PlanogramStore }>('planogram/store/' + (store.store_id), {
        store
    });
    return resp;
}
/*************************************************************/

/* Barak 29.7.2020 - check if store is primary */
export const getStorePrimary = async (store_id: string | number): Promise<any> => {
    const resp = await getRequest<any>('planogram/store/primary', {
        params: {
            store_id
        }
    });
    return resp.primary;
}
export const setStorePrimary = async (store_id: string | number, primary: boolean): Promise<any> => {
    const resp = await postRequest<any>('planogram/store/primary', {
        store_id,
        primary
    });
    return resp;
}
/*************************************************************/

export const deletePlanogramStore = async (storeId: number | string): Promise<void> => {
    await deleteRequest('planogram/store/' + storeId);
}
export const createStoreAisle = async (storeId: number | string): Promise<PlanogramAisle> => {
    const result = await getRequest<{ status: string, aisle: PlanogramAisle }>(`planogram/store/${storeId}/aisle/create`);
    return result.aisle;
}
export const deleteStoreAisle = async (storeId: number | string, aisleId: AisleId) => {
    await deleteRequest<{ status: string, message: string }>(`/planogram/store/${storeId}/aisle/${aisleId}/New`);
}

export const deleteAllRelatedAisles = async (aisleId: AisleId) => {
    await deleteRequest<{ status: string, message: string }>(`/planogram/related_aisles/${aisleId}`);
}


/*  Barak 22.11.20 - Add saving array for aisle, allowing to do 
    Ctrl+Z to move back and Ctrl+Y to move forward */
export const getStoreAisle = async (storeId: number | string, aisleId: AisleId): Promise<PlanogramAisle> => {
    const result = await getRequest<{ status: string, aisle: PlanogramAisle }>(`planogram/store/${storeId}/aisle/${aisleId}`);
    return result.aisle;
}
/*************************************************************/
/*  Barak 22.11.20 - check if file in sent url exists on server */
export const checkFile = async (url: string): Promise<boolean> => {
    const result = await postRequest<{ status: string, exists: boolean }>(`/planogram/file/url`, {
        url
    });
    return result.exists;
}
/*************************************************************/
/*  Barak 23.11.20 - check if image for barcode and position exists, else - generate it */
export const checkImage = async (barcode: number, position: number): Promise<boolean> => {
    const result = await postRequest<{ status: string, exists: boolean }>(`/planogram/image/position`, {
        barcode,
        position
    });
    return result.exists;
}
/*************************************************************/

export const saveStoreAisle = async (storeId: number | string, aisle: PlanogramAisle): Promise<PlanogramAisle> => {
    console.log('postRequest saveStoreAisle');
    /* Barak 8.12.20 - before saving the aisle in DB, save it in localStorage */
    // before returning the aisle make sure we save in to localAisleMap in localStoirage
    let localAisleMap: any = {};
    let localAisleMapS = localStorage.getItem('localAisleMap');
    let localAisleMapB = null;
    if (localAisleMapS) localAisleMapB = decompress(localAisleMapS);
    if (localAisleMapB) localAisleMap = JSON.parse(localAisleMapB);
    // get the latest aisle version from the DB
    let dbVersion = await getAisleVersion(aisle.aisle_id);
    localAisleMap[aisle.id] = { version: dbVersion + 1, aisle: aisle };
    localStorage.setItem('localAisleMap', compress(JSON.stringify(localAisleMap)));
    /**************************************************************************/
    const result = await postRequest<{
        status: string,
        aisle: PlanogramAisle
    }>(`/planogram/store/${storeId}/aisle/${aisle.aisle_id}/New`, {
        aisle
    });
    return result.aisle;
}
/* Barak 19.11.20 - Add special Save Aisle (that delets record in planogram_storage) on Ctrl+D */
export const saveStoreAisleSpecial = async (storeId: number | string, aisle: PlanogramAisle): Promise<PlanogramAisle> => {
    /* Barak 8.12.20 - before saving the aisle in DB, save it in localStorage */
    // before returning the aisle make sure we save in to localAisleMap in localStoirage
    let localAisleMap: any = {};
    let localAisleMapS = localStorage.getItem('localAisleMap');
    let localAisleMapB = null;
    if (localAisleMapS) localAisleMapB = decompress(localAisleMapS);
    if (localAisleMapB) localAisleMap = JSON.parse(localAisleMapB);
    // get the latest aisle version from the DB
    let dbVersion = await getAisleVersion(aisle.aisle_id);
    localAisleMap[aisle.id] = { version: dbVersion + 1, aisle: aisle };
    localStorage.setItem('localAisleMap', compress(JSON.stringify(localAisleMap)));
    /**************************************************************************/
    const result = await postRequest<{
        status: string,
        aisle: PlanogramAisle
    }>(`/planogram/special/store/${storeId}/aisle/${aisle.aisle_id}/New`, {
        aisle
    });
    return result.aisle;
}
/***********************************************************************************************/
// export const deleteStoreAisle =
export type UpdateProductItem = {
    barcode?: number;
    dimensions: ScateredDimensionObject;
};
export const updateMultipleProductsDimensions = async (products: UpdateProductItem[]): Promise<CatalogProduct[]> => {
    return await postRequest<CatalogProduct[]>('planogram/catalog/dimensions', {
        products
    });
};



export const addShelfItem = async (storeId: number, shelf_id: PlanogramElementId, barcode: string | CatalogBarcode, placement: PlacementObject, previous_barcode?: string | CatalogBarcode) => {
    await postRequest(`planogram/store/${storeId}/item`, {
        shelf_id,
        barcode,
        placement,
        previous_barcode
    });
}
/* Barak 29.9.20 - add function to add an item json object to store shelf */
export const addShelfItemNew = async (storeId: number, item: PlanogramItem) => {
    await postRequest(`planogram/store/${storeId}/item/New`, { item });
}
/********************************************************************/
export const updateShelfItem = async (storeId: number, item_id: PlanogramElementId, shelf_id: PlanogramElementId, placement: PlacementObject, barcode?: string | CatalogBarcode) => {
    await putRequest(`planogram/store/${storeId}/item/${item_id}`, {
        shelf_id,
        item_id,
        placement,
        barcode
    });
}
export const deleteShelfItem = async (storeId: number, shelfItem: PlanogramElementId) => {
    // await deleteRequest(`planogram/store/${storeId}/item/${shelfItem}`, {});
    /* Barak Change 16.4.20 */ await deleteRequest(`planogram/store/${storeId}/singleItem/${shelfItem}`, {});
}


export const duplciateAisle = async (branchId: number, storeId: number | null, aisleId: string | number, targetStoreId: string | number) => {
    return await postRequest('planogram/store/aisle/duplicate', {
        branch_id: branchId,
        store_id: storeId,
        source_aisle_pid: aisleId,
        target_store_id: targetStoreId
    });
}


export type AisleModel = {
    id: number,
    store_id: number,
    pid: string,
    name: string,
    aisle_number: number,
    height: number,
    width: number,
    depth: number
}

export const fetchAisleRecord = async (aisleId: any): Promise<AisleModel> => {
    const result = await getRequest<{ status: string, aisle: AisleModel }>(`planogram/aisle/details/${aisleId}`);
    return result.aisle;
}