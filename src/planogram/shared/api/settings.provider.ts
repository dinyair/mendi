import { getRequest, putRequest, postRequest, deleteRequest  } from "@src/http";
import * as System from "@src/planogram/shared/interfaces/models/System";
import { PlanogramStoreRecord } from "./planogram.provider";

export const fetchBranchDetail = async (branchId: number | string): Promise<System.Branch> => {
    const result = await getRequest<{
        status: string,
        branch: System.Branch
    }>('planogram/data/branch/' + branchId);
    return result.branch;
}
export const fetchAllTables = async (shouldUpdate:any): Promise<void> => {
    return await postRequest('planogram/data/allTables',{
        shouldUpdate
    });
}
export const fetchBranches = async (): Promise<System.Branch[]> => {
    return await getRequest<System.Branch[]>('planogram/data/branches');
}
export const fetchStores = async (): Promise<PlanogramStoreRecord[]> => {
    const resp = await getRequest<{
        stores: PlanogramStoreRecord[]
    }>('planogram/stores');
    return resp.stores;
}
export const fetchSuppliers = async (): Promise<System.Supplier[]> => {
    return await getRequest<System.Supplier[]>('planogram/data/suppliers');
}
export const fetchGroups = async (): Promise<System.Group[]> => {
    return await getRequest<System.Group[]>('planogram/data/groups');
}
export const fetchSubGroups = async (): Promise<System.SubGroup[]> => {
    /* Barak 3.12.20 *  return await getRequest<System.SubGroup[]>('planogram/data/subgroups'); */
    /* Barak 3.12.20 */ 
    let res:System.SubGroup[] = await getRequest<System.SubGroup[]>('planogram/data/subgroups');
    res.unshift({Id:0, Name:'', GroupId:0});
    return res;
    /*****************/
}
/* Barak 13.12.20 - fetch layouts */
export const fetchLayouts = async (): Promise<System.Layout[]> => {
    let res:System.Layout[] = await getRequest<System.Layout[]>('planogram/data/layouts');
    res.unshift({Id:0, Name:''});
    return res;
}
export const fetchPlanoLayouts = async (layoutId:number): Promise<System.Layout[]> => {
    let res:any = await getRequest<any>('planogram/getPlanoLayout/'+ layoutId);
    return res.layoutRecords;
}
/**********************************/
export const fetchClasses = async (): Promise<System.Class[]> => {
    return await getRequest<System.Class[]>('planogram/data/classes');
}
export const fetchSeries = async (): Promise<System.Serie[]> => {
    return await getRequest<System.Serie[]>('planogram/data/series');
}
/* Barak 18.8.20 */
export const fetchSegments = async (): Promise<System.Segment[]> => {
    return await getRequest<System.Segment[]>('planogram/data/segments');
}
/*****************/
/* Barak 6.12.20 - fetch Models */
export const fetchModels = async (): Promise<System.Model[]> => {
    let models = await getRequest<System.Model[]>('planogram/data/models');
    console.log('fetchModels',models);
    return models;
}
/*******************************/
/* Barak 7.12.20 - fetch Models */
export const fetchUserReportTemplates = async (): Promise<any[]> => {
    let userReportTemplates = await getRequest<any[]>('planogram/data/userReportTemplates');
    console.log('fetchUserReportTemplates',userReportTemplates);
    return userReportTemplates;
}
/*******************************/
