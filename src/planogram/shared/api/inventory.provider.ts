import { getRequest, postRequest } from "@src/http";
import { Branch } from "@src/planogram/shared/interfaces/models/System";

export async function getUserBranches(): Promise<Branch[]> {
    return await getRequest<Branch[]>('/data/branch/UserBranches');
}
