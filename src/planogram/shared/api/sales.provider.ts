import { getRequest, putRequest, postRequest, deleteRequest  } from "@src/http";

export type CatalogSaleRecord = {
    BarCode: number,
    BranchId?: number,
    // DateBuy: string,
    TotalAmount: number,
    TotalPrice: number,
    SupplierId?: number,
    GroupId?: number,
    SubGroupId?: number,
    ClassId?: number,
    /* Barak 18.8.20 - Add fields */
    SegmentId?: number,
    DegemId?: number,
    SalePrice: number,
    SalePriceNoVAT: number,
    BuyPrice: number,
    Profit: number,
    ProfitPercent: number,
    /* Barak 1.9.20 */
    MinStock: number,
    CrDate: Date,
    ExDate: Date,
    /****************/
    /******************************/
    /* Barak 6.12.20 */ ModelId?:number,
    /* Barak 29.11.20 */ client_image?: number,
    /* Barak 3.1.21 */ client_image_arr?:any[]
}

export const fetchCatalogSales = async (data: {
    beginDate: Date,
    endDate: Date,
    branch?: number[],
    supplier?: number[],
    group?: number[],
    class?: number[],
    subGroup?: number[],
    degem?: number[],
    /* Barak 18.8.20 - Add segment */ segment?: number[],
    /* Barak 6.12.20 - Add model */ model?: number[],
    fullNetwork?: boolean
}): Promise<CatalogSaleRecord[]> => {
    const response = await getRequest<{
        status: string,
        sales: CatalogSaleRecord[]
    }>('planogram/sales', {
        params: data,
    });
    console.log('fetchCatalogSales response.sales.length',response.sales.length);
    return response.sales;
}

/* Barak 13.12.20 - fetchLayoutSales */
export const fetchLayoutSales = async (data: {
    beginDate: Date,
    endDate: Date,
    branchId: number,
    storeId: number,
}): Promise<CatalogSaleRecord[]> => {
    const response = await getRequest<{
        status: string,
        sales: CatalogSaleRecord[]
    }>('planogram/layout_sales', {
        params: data,
    });
    return response.sales;
}
/*************************************/

/* Barak 23.1.20 * Get data from table truma and truma_snif for all Barcodes */
export const fetchTrumaSnif = async (BranchId: number[] | undefined): Promise<any[]> => {
    const response = await getRequest<{
        status: string,
        trumaRecords: any[]
    }>('planogram/truma_snif/' + BranchId);
    return response.trumaRecords;
}
export const fetchTruma = async (): Promise<any[]> => {
    const response = await getRequest<{
        status: string,
        trumaRecords: any[]
    }>('planogram/truma');
    return response.trumaRecords;
}
export const fetchTrumaSnifBySubGroup = async (BranchId: number, SubGroup: number): Promise<any[]> => {
    const response = await getRequest<{
        status: string,
        trumaRecords: any[]
    }>('planogram/truma_snif/subgroup/' + BranchId + "/" + SubGroup);
    return response.trumaRecords;
}
export const fetchTrumaSnifByGroup = async (BranchId: number, Group: number): Promise<any[]> => {
    const response = await getRequest<{
        status: string,
        trumaRecords: any[]
    }>('planogram/truma_snif/group/' + BranchId + "/" + Group);
    return response.trumaRecords;
}
export const fetchTrumaSnifByDepartment = async (BranchId: number, Department: number): Promise<any[]> => {
    const response = await getRequest<{
        status: string,
        trumaRecords: any[]
    }>('planogram/truma_snif/department/' + BranchId + "/" + Department);
    return response.trumaRecords;
}
export const fetchTrumaByGroup = async (Group: number): Promise<any[]> => {
    const response = await getRequest<{
        status: string,
        trumaRecords: any[]
    }>('planogram/truma/group/' + Group);
    return response.trumaRecords;
}
export const fetchTrumaBySubGroup = async (SubGroup: number): Promise<any[]> => {
    const response = await getRequest<{
        status: string,
        trumaRecords: any[]
    }>('planogram/truma/subgroup/' + SubGroup);
    return response.trumaRecords;
}
export const fetchTrumaByDepartment = async (Department: number): Promise<any[]> => {
    const response = await getRequest<{
        status: string,
        trumaRecords: any[]
    }>('planogram/truma/department/' + Department);
    return response.trumaRecords;
}
export const fetchTotalShelfWidthForStore = async (BranchId: number): Promise<any> => {
    const response = await getRequest<{
        status: string,
        total_width: any
    }>('planogram/total_shelf/' + BranchId);
    return response.total_width;
}
export const fetchTotalShelfPerSupplierPerStore = async (BranchId: number): Promise<any[]> => {
    const response = await getRequest<{
        status: string,
        supplier_total_shelf: any[]
    }>('planogram/supplier_total_shelf/' + BranchId);
    return response.supplier_total_shelf;
}
/* Barak 2.8.20 */
export const fetchTotalShelfPerSupplierPerAisle = async (AisleId: number, departmentId: number, groupId: number, subGroupId: number, segmentId: number): Promise<any[]> => {
    const response = await getRequest<{
        status: string,
        supplier_total_shelf_aisle: any[]
    }>('planogram/supplier_total_shelf_aisle/' + AisleId + "/" + departmentId + "/" + groupId + "/" + subGroupId + "/" + segmentId);
    return response.supplier_total_shelf_aisle;
}
export const fetchTotalShelfForAisle = async (AisleId: number): Promise<number> => {
    const response = await getRequest<{
        status: string,
        total_shelf_aisle: number
    }>('planogram/total_shelf_aisle/' + AisleId);
    return response.total_shelf_aisle;
}
export const fetchTrumaAisleSnif = async (BranchId: number, AisleId: number): Promise<any[]> => {
    const response = await getRequest<{
        status: string,
        trumaRecords: any[]
    }>('planogram/truma_snif_aisle/' + BranchId + '/' + AisleId);
    return response.trumaRecords;
}
export const fetchTrumaByAisle = async (AisleId: number): Promise<any[]> => {
    const response = await getRequest<{
        status: string,
        trumaRecords: any[]
    }>('planogram/truma_aisle/' + AisleId);
    return response.trumaRecords;
}
export const fetchTrumaSnifBySelection = async (BranchId: number, Department: number, Group: number, SubGroup: number, Segment: number): Promise<any[]> => {
    const response = await getRequest<{
        status: string,
        trumaRecords: any[]
    }>('planogram/truma_snif/selection/' + BranchId + "/" + Department + "/" + Group + "/" + SubGroup + "/" + Segment);
    return response.trumaRecords;
}
export const fetchTrumaSnifBySelectionArr = async (data: {
    branch?:number[]
    class?: number[],
    group?: number[],
    subGroup?: number[],
    segment?: number[],
}): Promise<any[]> => {
    const response = await getRequest<{
        status: string,
        trumaRecords: any[]
    }>('planogram/truma_snif/selection/Arr',{params: data});
    return response.trumaRecords;
}
export const fetchTrumaBySelection = async (Department: number, Group: number, SubGroup: number, Segment: number): Promise<any[]> => {
    const response = await getRequest<{
        status: string,
        trumaRecords: any[]
    }>('planogram/truma/selection/' + Department + "/" + Group + "/" + SubGroup + "/" + Segment);
    return response.trumaRecords;
}
export const fetchTrumaBySelectionArr = async (data: {
    class?: number[],
    group?: number[],
    subGroup?: number[],
    segment?: number[],
}): Promise<any[]> => {
    const response = await getRequest<{
        status: string,
        trumaRecords: any[]
    }>('planogram/truma/selection/Arr',{params: data});
    return response.trumaRecords;
}
/***************/
/* Barak 7.9.20 */
export const fetchTotalShelfPerItemPerAisle = async (AisleId: number, departmentId: number, groupId: number, subGroupId: number, segmentId: number): Promise<any[]> => {
    const response = await getRequest<{
        status: string,
        item_total_shelf_aisle: any[]
    }>('planogram/item_total_shelf_aisle/' + AisleId + "/" + departmentId + "/" + groupId + "/" + subGroupId + "/" + segmentId);
    return response.item_total_shelf_aisle;
}
export const fetchTotalShelfPerItemPerAisleArr = async (AisleId: number, data: {
    class?: number[],
    group?: number[],
    subGroup?: number[],
    segment?: number[],
}): Promise<any[]> => {
    const response = await getRequest<{
        status: string,
        item_total_shelf_aisle: any[]
    }>('planogram/item_total_shelf_aisle/Arr/' + AisleId, {
        params: data,
    });
    return response.item_total_shelf_aisle;
}
/****************/
/* Barak 9.8.20 */
export const getBranchByStore = async (StoreId: number): Promise<number> => {
    const response = await getRequest<{
        status: string,
        BranchId: number
    }>('planogram/branch_by_store/' + StoreId);
    return response.BranchId;
}
export const getStoreByAisle = async (AisleId: number): Promise<number> => {
    const response = await getRequest<{
        status: string,
        StoreId: number
    }>('planogram/store_by_aisle/' + AisleId);
    return response.StoreId;
}
export const fetchCatalogSalesExpanded = async (data: {
    beginDate: Date,
    endDate: Date,
    branch?: number[],
    supplier?: number[],
    group?: number[],
    class?: number[],
    subGroup?: number[],
    degem?: number[],
    fullNetwork?: boolean
}): Promise<CatalogSaleRecord[]> => {
    const response = await getRequest<{
        status: string,
        sales: CatalogSaleRecord[]
    }>('planogram/sales/expanded', {
        params: data,
    });
    return response.sales;
}
/****************/
/* Barak 9.9.20 */
export const fetchPlanogramYitra = async (branch_id: number): Promise<any[]> => {
    let res = await getRequest<{
        status: string,
        yitraRecords: any[]
    }>('planogram/yitra/' + branch_id);
    return res.yitraRecords;
}
export const fetchLastYitra = async (branch_id: number, barcode: number): Promise<number> => {
    let res = await getRequest<{
        status: string,
        yitraRecords: number
    }>('planogram/yitra/last/' + branch_id + "/" + barcode);
    return res.yitraRecords;
}
export const fetchLastStock = async (branch_id: number): Promise<any> => {
    let res = await getRequest<{
        status: string,
        yitraRecords: any
    }>('planogram/stock/last/' + branch_id);
    return res.yitraRecords;
}
export const updateBarCodeQuantity = async (branchId: number, barcode: number, quantity: number): Promise<void> => {
    return await postRequest('planogram/yitra/quantity/' + branchId + "/" + barcode, { quantity });
}
export const addBarCodeQuantity = async (branchId: number, barcode: number, quantity: number): Promise<void> => {
    return await putRequest('planogram/yitra/quantity/' + branchId + "/" + barcode, { quantity });
}
/****************/
/* Barak 7.12.20 - save user report state */
export const setUserReportState = async (report: string, userId: number, state:string): Promise<any> => {
    const resp = await postRequest<any>('planogram/pattern/', {
        report,
        userId,
        state
    });
    return resp;
}
export const getUserReportState = async (report: string, userId: number): Promise<any> => {
    const resp = await getRequest<{
        status: string,
        state: any
    }>('planogram/pattern/' + report + '/' + userId);
    console.log('getUserReportState',resp);
    return resp.state;
}
/******************************************/