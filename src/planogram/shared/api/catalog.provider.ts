import { CatalogProduct, CatalogBarcode } from "@src/planogram/shared/interfaces/models/CatalogProduct";
import { getRequest, putRequest, postRequest } from "@src/http";
import { BarcodeStatus } from "@src/planogram/shared/interfaces/models/CatalogProduct";
import { DimensionObject, PlanogramStore } from "@src/planogram/shared/store/planogram/planogram.types";
import { ProductWeekSales } from "@src/planogram/shared/interfaces/responses/planogram.dto";
import { PlanogramStoreRecord } from "./planogram.provider";
import { FetchPlanogramStoreResponse } from "@src/planogram/shared/interfaces/responses/PlanogramResponse";
import * as System from "@src/planogram/shared/interfaces/models/System";
import { AuthUser } from "@src/planogram/shared/interfaces/models/User";

export const fetchCatalog = async (): Promise<CatalogProduct[]> => {
    let res = await getRequest<CatalogProduct[]>('planogram/catalog');
    return res;
}
export const chkprm = async (user:AuthUser | undefined): Promise<any> => {
    return await postRequest<any>('planogram/chkprm',{
        user
    });
}
/****************************************/
export const fetchBarcodeStatuses = async (): Promise<BarcodeStatus[]> => {
    return await getRequest<BarcodeStatus[]>('planogram/barcode/statuses');
}
export const updateBarcodeStatus = async (barcode: CatalogBarcode, message: string): Promise<void> => {
    return await putRequest('planogram/barcode/status/' + barcode, {
        message
    });
}
export const fetchCatalogByBarCode = async (barcode: number): Promise<CatalogProduct[]> => {
    return await getRequest<CatalogProduct[]>('planogram/catalog/' + barcode);
}
export const updateBarCodeDimensions = async (barcode: number, dimensions: DimensionObject): Promise<void> => {
    return await putRequest('planogram/catalog/' + barcode, { dimensions });
}
export const fetchCatalogByDegem = async (degemId: number): Promise<CatalogProduct[]> => {
    return await getRequest<CatalogProduct[]>('planogram/degem/' + degemId);
}

/* Barak 4.11.20 - get substitute items for barcode */
export const fetchSubstituteBarcodes = async (barcode: CatalogBarcode): Promise<CatalogBarcode[]> => {
    return await getRequest<CatalogBarcode[]>('planogram/barcode/substitute/' + barcode);
}
/****************************************************/

/* Barak 22.7.20 - Add special paths for the PlanogramSpecialView page to bypass verified login user */
// export const fetchSpecialCatalog = async (netw: string): Promise<CatalogProduct[]> => {
//     const resp = await getRequest<CatalogProduct[]>('special/planogram/catalog?netw=' + netw + '&aid=2020');
//     console.log('fetchSpecialCatalog resp', resp);
//     return resp;
// }
// export const productsSpecialPredictedSales = async (netw: string, branchId: number, statusCalcInv: number, calcInvDays: number): Promise<ProductWeekSales[]> => {
//     console.log('productsSpecialPredictedSales branchId', branchId, statusCalcInv, calcInvDays);
//     const resp = await postRequest<{ status: string, sales: ProductWeekSales[] }>('special/planogram/sales/batch?netw=' + netw + '&aid=2020&branchId=' + branchId
//         + '&statusCalcInv=' + statusCalcInv + '&calcInvDays=' + calcInvDays, {
//         branchId,
//         statusCalcInv,
//         calcInvDays
//     });
//     console.log('productsSpecialPredictedSales resp.sales', resp.sales);
//     return resp.sales;
// }
// export const fetchSpecialBranchStores = async (netw: string, branchId: number | string): Promise<PlanogramStoreRecord[]> => {
//     const resp = await getRequest<{
//         stores: PlanogramStoreRecord[]
//     }>('special/planogram?netw=' + netw + '&aid=2020&branchId=' + branchId);
//     console.log('fetchSpecialBranchStores', resp);
//     return resp.stores;
// }
// export const fetchSpecialPlanogramStore = async (netw: string, storeId: number | string): Promise<PlanogramStore> => {
//     const resp = await getRequest<FetchPlanogramStoreResponse>('special/planogram/store?netw=' + netw + '&aid=2020&storeId=' + storeId);
//     if (!resp.store) throw new Error("No store was found.");
//     console.log('fetchSpecialPlanogramStore resp', resp);
//     return resp.store;
// }
export const fetchSpecialCatalog = async (netw: string, branchId: number): Promise<CatalogProduct[]> => {
    let specialCatalog = localStorage.getItem('specialCatalog');
    console.log('initial specialCatalog', (specialCatalog ? JSON.parse(specialCatalog).length : '0'));
    if (specialCatalog && JSON.parse(specialCatalog).length >= 1
        && JSON.parse(specialCatalog)[0].netw === netw
        && JSON.parse(specialCatalog)[0].branchId === branchId) {
        return JSON.parse(specialCatalog)[0].specialCatalogResp;
    } else {
        const aid = '2020';
        const resp = await postRequest<CatalogProduct[]>('special/planogram/catalog', {
            netw,
            aid,
            branchId
        });
        console.log('fetchSpecialCatalog resp', resp);
        let specialSpecialCatalog = {
            netw: netw,
            branchId: branchId,
            specialCatalogResp: resp
        }
        // console.log('save specialCatalog', specialSpecialCatalog);
        localStorage.setItem('specialCatalog', JSON.stringify(specialSpecialCatalog));
        // console.log('was specialCatalog saved', localStorage.getItem('specialCatalog'));
        return resp;
    }
    // const aid = '2020';
    // const resp = await postRequest<CatalogProduct[]>('special/planogram/catalog', {
    //     netw,
    //     aid
    // });
    // console.log('fetchSpecialCatalog resp', resp);
    // return resp;
}
export const productsSpecialPredictedSales = async (netw: string, branchId: number, statusCalcInv: number, calcInvDays: number): Promise<ProductWeekSales[]> => {
    let specialPredictedSales = localStorage.getItem('specialPredictedSales');
    console.log('initial SpecialPredictedSales', (specialPredictedSales ? JSON.parse(specialPredictedSales).length : '0'));
    if (specialPredictedSales && JSON.parse(specialPredictedSales).length >= 1
        && JSON.parse(specialPredictedSales)[0].netw === netw
        && JSON.parse(specialPredictedSales)[0].branchId === branchId
        && JSON.parse(specialPredictedSales)[0].statusCalcInv === statusCalcInv
        && JSON.parse(specialPredictedSales)[0].calcInvDays === calcInvDays) {
        return JSON.parse(specialPredictedSales)[0].specialPredictedSalesResp;
    } else {
        const aid = '2020';
        const resp = await postRequest<{ status: string, sales: ProductWeekSales[] }>('special/planogram/sales/batch', {
            aid,
            netw,
            branchId,
            statusCalcInv,
            calcInvDays
        });
        console.log('productsSpecialPredictedSales resp.sales', resp.sales);
        let specialPredictedSales = {
            netw: netw,
            branchId: branchId,
            statusCalcInv: statusCalcInv,
            calcInvDays: calcInvDays,
            specialPredictedSalesResp: resp.sales
        }
        localStorage.setItem('specialPredictedSales', JSON.stringify(specialPredictedSales));
        return resp.sales;
    }
}
export const fetchSpecialBranchStores = async (netw: string, branchId: number | string): Promise<PlanogramStoreRecord[]> => {
    let specialBranchStores = localStorage.getItem('specialBranchStores');
    console.log('initial specialBranchStores', (specialBranchStores ? JSON.parse(specialBranchStores).length : '0'));
    if (specialBranchStores && JSON.parse(specialBranchStores).length >= 1
        && JSON.parse(specialBranchStores)[0].netw === netw
        && JSON.parse(specialBranchStores)[0].branchId === branchId) {
        return JSON.parse(specialBranchStores)[0].specialBranchStoresResp;
    } else {
        const aid = '2020';
        const resp = await postRequest<{
            stores: PlanogramStoreRecord[]
        }>('special/planogram', {
            aid,
            netw,
            branchId,
        });
        console.log('fetchSpecialBranchStores resp.stores', resp.stores);
        let specialBranchStores = {
            netw: netw,
            branchId: branchId,
            specialBranchStoresResp: resp.stores
        }
        localStorage.setItem('specialBranchStores', JSON.stringify(specialBranchStores));
        return resp.stores;
    }
}
export const fetchSpecialPlanogramStore = async (netw: string, storeId: number | string): Promise<PlanogramStore> => {
    let specialPlanogramStore = localStorage.getItem('specialPlanogramStore');
    console.log('initial specialPlanogramStore', (specialPlanogramStore ? JSON.parse(specialPlanogramStore).length : '0'));
    if (specialPlanogramStore && JSON.parse(specialPlanogramStore).length >= 1
        && JSON.parse(specialPlanogramStore)[0].netw === netw
        && JSON.parse(specialPlanogramStore)[0].storeId === storeId) {
        return JSON.parse(specialPlanogramStore)[0].specialPlanogramStoreResp;
    } else {
        const aid = '2020';
        const resp = await postRequest<FetchPlanogramStoreResponse>('special/planogram/store', {
            aid,
            netw,
            storeId,
        });
        if (!resp.store) throw new Error("No store was found.");
        console.log('fetchSpecialPlanogramStore resp.store', resp.store);
        let specialPlanogramStore = {
            netw: netw,
            storeId: storeId,
            specialPlanogramStoreResp: resp.store
        }
        localStorage.setItem('specialPlanogramStore', JSON.stringify(specialPlanogramStore));
        return resp.store;
    }
}

export const fetchSpecialBranches = async (netw: string): Promise<System.Branch[]> => {
    let specialBranches = localStorage.getItem('specialBranches');
    console.log('initial specialBranches', (specialBranches ? JSON.parse(specialBranches).length : '0'));
    if (specialBranches && JSON.parse(specialBranches).length >= 1
        && JSON.parse(specialBranches)[0].netw === netw) {
        return JSON.parse(specialBranches)[0].specialBranchesResp;
    } else {
        const aid = '2020';
        const resp = await postRequest<System.Branch[]>('special/planogram/data/branches', {
            aid,
            netw,
        });
        console.log('fetchSpecialBranches resp', resp);
        let specialBranches = {
            netw: netw,
            specialBranchesResp: resp
        }
        localStorage.setItem('specialBranches', JSON.stringify(specialBranches));
        return resp;
    }
}
export const fetchSpecialSuppliers = async (netw: string): Promise<System.Supplier[]> => {
    let specialSuppliers = localStorage.getItem('specialSuppliers');
    console.log('initial specialSuppliers', (specialSuppliers ? JSON.parse(specialSuppliers).length : '0'));
    if (specialSuppliers && JSON.parse(specialSuppliers).length >= 1
        && JSON.parse(specialSuppliers)[0].netw === netw) {
        return JSON.parse(specialSuppliers)[0].specialSuppliersResp;
    } else {
        const aid = '2020';
        const resp = await postRequest<System.Supplier[]>('special/planogram/data/suppliers', {
            aid,
            netw,
        });
        console.log('fetchSpecialSuppliers resp', resp);
        let specialSuppliers = {
            netw: netw,
            specialSuppliersResp: resp
        }
        localStorage.setItem('specialSuppliers', JSON.stringify(specialSuppliers));
        return resp;
    }
}
export const fetchSpecialGroups = async (netw: string): Promise<System.Group[]> => {
    let specialGroups = localStorage.getItem('specialGroups');
    console.log('initial specialGroups', (specialGroups ? JSON.parse(specialGroups).length : '0'));
    if (specialGroups && JSON.parse(specialGroups).length >= 1
        && JSON.parse(specialGroups)[0].netw === netw) {
        return JSON.parse(specialGroups)[0].specialGroupsResp;
    } else {
        const aid = '2020';
        const resp = await postRequest<System.Supplier[]>('special/planogram/data/groups', {
            aid,
            netw,
        });
        console.log('fetchSpecialGroups resp', resp);
        let specialGroups = {
            netw: netw,
            specialGroupsResp: resp
        }
        localStorage.setItem('specialGroups', JSON.stringify(specialGroups));
        return resp;
    }
}
export const fetchSpecialSubGroups = async (netw: string): Promise<System.SubGroup[]> => {
    let specialSubGroups = localStorage.getItem('specialSubGroups');
    console.log('initial specialSubGroups', (specialSubGroups ? JSON.parse(specialSubGroups).length : '0'));
    if (specialSubGroups && JSON.parse(specialSubGroups).length >= 1
        && JSON.parse(specialSubGroups)[0].netw === netw) {
        return JSON.parse(specialSubGroups)[0].specialSubGroupsResp;
    } else {
        const aid = '2020';
        const resp = await postRequest<System.SubGroup[]>('special/planogram/data/subgroups', {
            aid,
            netw,
        });
        console.log('fetchSpecialSubGroups resp', resp);
        let specialSubGroups = {
            netw: netw,
            specialSubGroupsResp: resp
        }
        localStorage.setItem('specialSubGroups', JSON.stringify(specialSubGroups));
        return resp;
    }
}
export const fetchSpecialClasses = async (netw: string): Promise<System.Class[]> => {
    let specialClasses = localStorage.getItem('specialClasses');
    console.log('initial specialClasses', (specialClasses ? JSON.parse(specialClasses).length : '0'));
    if (specialClasses && JSON.parse(specialClasses).length >= 1
        && JSON.parse(specialClasses)[0].netw === netw) {
        return JSON.parse(specialClasses)[0].specialClassesResp;
    } else {
        const aid = '2020';
        const resp = await postRequest<System.Class[]>('special/planogram/data/classes', {
            aid,
            netw,
        });
        console.log('fetchSpecialClasses resp', resp);
        let specialClasses = {
            netw: netw,
            specialClassesResp: resp
        }
        localStorage.setItem('specialClasses', JSON.stringify(specialClasses));
        return resp;
    }
}
export const fetchSpecialSeries = async (netw: string): Promise<System.Serie[]> => {
    let specialSeries = localStorage.getItem('specialSeries');
    console.log('initial specialSeries', (specialSeries ? JSON.parse(specialSeries).length : '0'));
    if (specialSeries && JSON.parse(specialSeries).length >= 1
        && JSON.parse(specialSeries)[0].netw === netw) {
        return JSON.parse(specialSeries)[0].specialSeriesResp;
    } else {
        const aid = '2020';
        const resp = await postRequest<System.Serie[]>('special/planogram/data/series', {
            aid,
            netw,
        });
        console.log('fetchSpecialSeries resp', resp);
        let specialSeries = {
            netw: netw,
            specialSeriesResp: resp
        }
        localStorage.setItem('specialSeries', JSON.stringify(specialSeries));
        return resp;
    }
}
export const saveItemMissing = async (netw: string, userPhone: string, barcode: number, shelf: string, missing: number): Promise<any> => {
    const aid = '2020';
    const resp = await postRequest<any>('special/saveItemMissing', {
        aid,
        netw,
        userPhone,
        barcode,
        shelf,
        missing
    });
    console.log('saveItemMissing resp', resp);
    return resp;
}
/********************************************************************************************/
