import axios, {
    AxiosRequestConfig, AxiosInstance, AxiosResponse
} from "axios";
import config from "./config";
import { store } from "./store";
import { setAppState } from "./store/system/system.actions";
import { logOut } from "@src/containers/user/initial-state";

export const httpClient: AxiosInstance = axios.create({
    baseURL: config.API_URL,
    /* Barak 10.8.20 * timeout: 30000, */
    /* Barak 10.8.20 */ timeout: 300000,
    headers: {}
});
export const httpClientEmpty: AxiosInstance = axios.create({
    // timeout: 30000,  // 30 seconds 
    timeout: 10800000,  // 3 hours
    headers: {}
});
httpClient.defaults.withCredentials = true;
httpClient.interceptors.request.use((req: AxiosRequestConfig) => {
    const token = store.getState().auth.token;
    if (token != null)
        req.headers["Authorization"] = "Bearer " + token;
    return req;
});
httpClient.interceptors.response.use(resp => resp, error => {
    if (error && error.response && error.response.status && error.response.status === 401) {
        logOut()
    }
    else if (error && error.message === "Network Error")
        store.dispatch(setAppState("NO_NETWORK"));

    return Promise.reject(error);
});

export async function postRequest<T>(url: string, data: any, config?: AxiosRequestConfig): Promise<T> {
    const response = await httpClient.post<T, AxiosResponse<T>>(url, data, config);
    if (!response || response.status !== 200)
        throw new Error("HttpClient:ERROR - " + response.status + ":" + response.statusText);
    return response.data;
}
export async function putRequest<T>(url: string, data: any, config?: AxiosRequestConfig): Promise<T> {
    const response = await httpClient.put<T, AxiosResponse<T>>(url, data, config);
    if (!response || response.status !== 200)
        throw new Error("HttpClient:ERROR - " + response.status + ":" + response.statusText);
    return response.data;
}
export async function deleteRequest<T>(url: string, config?: AxiosRequestConfig): Promise<T> {
    const response = await httpClient.delete(url, config);
    if (!response || response.status !== 200)
        throw new Error("HttpClient:ERROR - " + response.status + ":" + response.statusText);
    return response.data;
}
export async function getRequest<T>(url: string, config?: AxiosRequestConfig): Promise<T> {
    let response = await httpClient.get(url, config);
    if (!response || response.status !== 200)
        throw new Error("HttpClient:ERROR - " + response.status + ":" + response.statusText);
    return response.data;
}

export class HttpClientError extends Error { }