import * as React from 'react';
import { Navbar } from '@src/planogram/navbar';
import { Sidebar } from '@src/planogram/sidebar';
import { useIntl } from "react-intl"
import { Spinner } from 'reactstrap'
import * as moment from 'moment';
import { useSelector, useDispatch } from 'react-redux';
import { AppState } from "@src/planogram/shared/store/app.reducer";
import { MenuProvider } from 'react-contexify';
import { isMobile } from 'react-device-detect';
import HTML5Backend from 'react-dnd-html5-backend';
import { PlanogramStore } from '@src/planogram/shared/store/planogram/planogram.types';
import { TouchBackend } from 'react-dnd-touch-backend'
import { DndProvider } from 'react-dnd';
import { PlanogramMainContextMenu } from './PlanogramMainContextMenu';
import PlanogramTrashComponent from './components/PlanogramTrashComponent';
import { PlanogramStoreComponent } from './store/PlanogramStoreContainer';
import PlanogramSettings from './components/PlanogramSettings';
import PlanogramProductDetail from './components/PlanogramProductDetail';
import PlangoramView from './PlanogramView';
import * as systemApi from '@src/planogram/shared/api/settings.provider';
import { PLANOGRAM_BASE_URL } from '@src/PlanoApp';
import { fetchPlanogramStore, productsPredictedSalesAvg, productsPredictedSalesSl } from '@src/planogram/shared/api/planogram.provider';
import { resetStore, setStore } from '@src/planogram/shared/store/planogram/store/store.actions';
import { useHistory } from "react-router-dom";
import { ProductSaleMap } from '@src/planogram/shared/store/catalog/catalog.types';
import { AuthUser } from '@src/planogram/shared/interfaces/models/User';
import { compress, decompress } from 'lz-string'
import { setBranches, setSuppliers, setClasses, setGroups, setSeries, setSubGroups, setSegments, setModels, setLayouts } from '@src/planogram/shared/store/system/data/data.actions';
import { setWeeklySaleButch } from '@src/planogram/shared/store/catalog/catalog.action';
import { fetchAndUpdatePlanogram } from '@src/helpers/planogram';
import '@src/planogram/planogram.scss';
import '@src/planogram/print.scss';

let d:any = document
const hasNative =
d && (d.elementsFromPoint || d.msElementsFromPoint)

function getDropTargetElementsAtPoint(x:number, y:number, dropTargets:any) {
return dropTargets.filter((t:any) => {
  const rect = t.getBoundingClientRect()
  return (
    x >= rect.left &&
    x <= rect.right &&
    y <= rect.bottom &&
    y >= rect.top
  )
})
}

export const PlanogramEditor: React.FC<any> = () => {
    const dispatch = useDispatch();
    const history = useHistory();
    const { formatMessage } = useIntl();
    const store = useSelector((state: AppState) =>  state.planogram.store )
    const newPlanoState = useSelector((state: AppState) =>  state.newPlano )
	const user = useSelector((state: AppState) => state.auth.user)
	const suppliers = useSelector((state: AppState) => state.system.data.suppliers)
	const classes = useSelector((state: AppState) => state.system.data.classes)
	const groups = useSelector((state: AppState) => state.system.data.groups)
	const subGroups = useSelector((state: AppState) => state.system.data.subGroups)
	const layouts = useSelector((state: AppState) => state.system.data.layouts)
	const branches = useSelector((state: AppState) => state.system.data.branches)
	const series = useSelector((state: AppState) => state.system.data.series)
	const segments = useSelector((state: AppState) => state.system.data.segments)
	const models = useSelector((state: AppState) => state.system.data.models)
    const url = window.location.href;
    const urlElements = url.split('/');
    let currentType = urlElements[4];
    let PageType = urlElements[5];
    let currentStore:any = parseInt(urlElements[7]);
    let currentAisleIndex = parseInt(urlElements[8]);
    const [newPlano, setNewPlano] = React.useState<any>(null);
    const [products, setProducts] = React.useState<any[]>([]);
    const [loading, setLoading] = React.useState<boolean>(false);

    React.useEffect(() => {
        if (!currentStore)
        return history.push(PLANOGRAM_BASE_URL);

        if (!store || (store && store.store_id.toString() !== currentStore))
            alert(1)
            loadStore(currentStore);
    },[])

   const setStoreFunc = async (store: PlanogramStore, user: AuthUser | undefined)  => {
        // const barcodes: CatalogBarcode[] = deepExtractKey(store, "product");
        try {
            let sales = null;
            let localSalesMap: any = {};
            let localSalesMapS = localStorage.getItem('localSalesMapAvg');
            let localSalesMapB = null;
            if (localSalesMapS) localSalesMapB = decompress(localSalesMapS);
            if (localSalesMapB) localSalesMap = JSON.parse(localSalesMapB);
            if (localSalesMap && localSalesMap[store.store_id] && localSalesMap[store.store_id] != undefined
                && localSalesMap[store.store_id].db === (user ? user.db : '')
                && localSalesMap[store.store_id].date === moment(new Date()).format('YYYY-MM-DD')) {
                // store avg sales are saved in localstorage - load them into the store 
                sales = localSalesMap[store.store_id].sales;
            } else {
                // there is no correct avg sales saved - get sales from DB 
                sales = await productsPredictedSalesAvg(store.store_id);
                // now that we have the sales - save them to localstorage
                localSalesMap[store.store_id] = { date: moment(new Date()).format('YYYY-MM-DD'), db: (user ? user.db : ''), sales: sales };
                localStorage.setItem('localSalesMapAvg', compress(JSON.stringify(localSalesMap)));
            }
            // in any event - get the hourly sales from the DB
            const salesHourly = await productsPredictedSalesSl(store.store_id);
            let salesLength = sales.length;
            const salesMap: ProductSaleMap = {}
            for (let i = 0; i < salesLength; i++) {
                const sale = sales[i];
                salesMap[sale.BarCode] = {
                    weekly: sale.WeeklyAverage,
                    monthly: null,
                    statusClaMlay: sale.statusCalMlay,
                    hourly: sale.HourlyAverage,
                    Create_Date: sale.Create_Date
                };
            }
            for (let i = 0; i < salesHourly.length; i++) {
                if (salesMap[salesHourly[i].BarCode])
                    salesMap[salesHourly[i].BarCode].hourly = salesHourly[i].HourlyAverage;
                else {
                    salesMap[salesHourly[i].BarCode] = {
                        weekly: null,
                        monthly: null,
                        statusClaMlay: 0,
                        hourly: salesHourly[i].HourlyAverage,
                        Create_Date: new Date()
                    }
                }
            }
            dispatch(setWeeklySaleButch(salesMap))
        } catch (error) {
            console.error(error);
        }
        dispatch(setStore(store));
    }
    


    const loadStore = async (storeId: string) => {
            const store = await fetchPlanogramStore(storeId, user ? user.db : '');
            dispatch(resetStore(0))
            setStoreFunc(store, user);
            loadData();
    }


   const loadData = async () => {
            if (!loading ){
                setLoading(true)
                let list: any = await systemApi.fetchAllTables({
                    branches: !branches || !branches.length,
                    layouts: !layouts || !layouts.length,
                    classes: !classes || !classes.length,
                    groups: !groups || !groups.length,
                    models:  !models || !models.length,
                    segments: !segments || !segments.length,
                    series:  !series || !series.length,
                    subgroups: !subGroups || !subGroups.length,
                    suppliers: !suppliers  || !suppliers.length
                })
                dispatch(setBranches(list.branches));
                dispatch(setLayouts(list.layouts));
                dispatch(setClasses(list.classes));
                dispatch(setGroups(list.groups));
                dispatch(setModels(list.models));
                dispatch(setSegments(list.segments));
                dispatch(setSeries(list.series));
                dispatch(setSubGroups(list.subgroups));
                dispatch(setSuppliers(list.suppliers));
                setLoading(false)
            }
    }
    
    React.useEffect(() => {
        if (!loading && currentAisleIndex > 0) {
            setLoading(true)
            setProducts([])
            async function updatePlanogram() {
                let fetchedData = await fetchAndUpdatePlanogram([{state: newPlano, type: 'newplano', forceUpdate:true, 
                params:{updatedb: 0, aisle_id: currentAisleIndex, copy: currentType === 'branch'},changeRows: formatMessage}],dispatch)
                if(fetchedData.newPlano){
                  setNewPlano(fetchedData.newPlano)
                  setProducts(fetchedData.newPlano.data)
                  setLoading(false)
                }
            }
            updatePlanogram()
        }
    }, [currentAisleIndex])

    React.useEffect(() => {
        if(newPlanoState) { setNewPlano(newPlanoState); setProducts(newPlanoState.data ? newPlanoState.data : []) }
    }, [newPlanoState])


const [isSaving,setIsSaving] = React.useState<boolean>(false)

    if (!store || (products && !products.length) || !newPlano ) return <div className="position-absolute" style={{left: window.innerWidth * 0.48, top: window.innerHeight * 0.4}}><Spinner /></div>
    else return (
        <>
            <DndProvider backend={!isMobile ? HTML5Backend : TouchBackend} 
            options={{getDropTargetElementsAtPoint: !hasNative && getDropTargetElementsAtPoint}}>
                <div className="planogram-wrapper">
                    <MenuProvider id="PLANOGRAM_VIEW_MENU" className="planogram-component">
                        <React.Fragment>
                        <Sidebar products={products} setIsSaving={setIsSaving}/>
                        <Navbar newPlano={newPlano} isSaving={isSaving}/>
                            {PageType === 'editor' ? <>
                                <PlanogramStoreComponent />
                                <PlanogramProductDetail />
                                <PlanogramTrashComponent />
                            </> : <PlangoramView />}
                            <PlanogramSettings />
                        </React.Fragment>
                    </MenuProvider>
                    <PlanogramMainContextMenu />
                </div>
            </DndProvider >
        </>
    );


};

export default PlanogramEditor;
