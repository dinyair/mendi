import * as React from 'react';
import classnames from 'classnames';
import { Icon, Popup, BlankPopUp, Divider } from '@components';
import { FormattedMessage, useIntl } from 'react-intl';
import { Sorting } from '@src/helpers/helpers';
import { ChevronDown } from 'react-feather';
import { groupBy } from '@src/helpers/helpers';

interface Props {
	readonly activeSort: any;
	readonly setSortByBox: any;
	readonly setActiveSort: (type?: any) => void;
	readonly setProducts: any;
	readonly products: any;
	readonly sortByBox: any;
	readonly mergedGroups: any[];
	readonly setProductsPerRow: (arr: any[]) => void;
	readonly productsPerRow: any[];
}

export const SortBy: React.FC<Props> = (props: Props) => {
	const { formatMessage } = useIntl();
	const { activeSort, setActiveSort, productsPerRow, setSortByBox, setProducts, products, sortByBox, mergedGroups } =
		props;

	const sortBy = (type: any) => {
		setActiveSort(type);
	};

	React.useEffect(() => {
		let activeSortT = '';

		if (activeSort) {
			switch (activeSort.type) {
				case 'totalPrice':activeSortT = 'Percent_TotalPrice';break;
				case 'totalAmount':activeSortT = 'Percent_TotalAmount';break;
				case 'avg':activeSortT = 'Percent_Avg';	break;
				case 'profit':activeSortT = 'Percent_Profit';break;
				default:setActiveSort();break;
			}
			if (activeSortT.length) {
				setProducts(
					mergedGroups && mergedGroups.length
						? groupBy(
								Sorting(productsPerRow, { field: activeSortT, type: 'number', asc: false, times: 0 }),
								mergedGroups,
								0,
								[],
								formatMessage
						  )
						: Sorting(products, { field: activeSortT, type: 'number', asc: false, times: 0 })
				);
				setSortByBox(false);
			}
		}
	}, [activeSort]);

	React.useEffect(() => {
		setActiveSort();
	}, []);

	return (
		<div>
			<div
				onClick={() => setSortByBox(!sortByBox)}
				className={classnames(
					'd-flex justify-content-center width-8-5-rem bg-white border-light-table position-relative font-small-4 cursor-pointer mb-05 py-05 border-2 px-2 text-black text-bold-700  mr-05 border-radius-2rem',
					{
						'border-turquoise': activeSort
					}
				)}
			>
				{<FormattedMessage id={'sortBy'} />}
				<ChevronDown
					color={sortByBox ? '#31baab' : 'black'}
					size={17}
					className={classnames('align-self-center ml-05', {
						'rotate-180': sortByBox
					})}
				/>
			</div>

			{sortByBox ? (
				<BlankPopUp noXbutton onOutsideClick={setSortByBox} style={{ width: '12.5rem', height: '16.5rem' }}>
					<div className="d-flex flex-column justify-content-center mt-1">
						<span className="text-black font-medium-3 font-bold text-bold-700 align-self-start ml-1">
							{formatMessage({ id: 'sortBy' })}
						</span>
					</div>
					<div className="d-flex flex-column justify-content-center mt-1">
						{[
							{ title: formatMessage({ id: 'TotalPrice' }), type: 'totalPrice' },
							{ title: formatMessage({ id: 'TotalAmountt' }), type: 'totalAmount' },
							{ title: formatMessage({ id: 'Avg' }), type: 'avg' },
							{ title: formatMessage({ id: 'Profit' }), type: 'profit' }
						].map((item: any, index: number) => {
							return (
								<div
									onClick={() => sortBy({ title: item.title, type: item.type, index })}
									className={classnames(
										'cursor-pointer d-flex flex-column justify-content-center height-3-rem',
										{
											'bg-turquoise': activeSort && activeSort.index === index
										}
									)}
								>
									{activeSort ? (
										activeSort.index !== index ? (
											<div>
												{activeSort && activeSort.index === index - 1 ? (
													<Divider className="visibility-hidden position-absolute" />
												) : (
													<Divider />
												)}
											</div>
										) : (
											<Divider className="visibility-hidden position-absolute" />
										)
									) : (
										<Divider />
									)}
									<span
										className={classnames('mt-05 mb-05 ml-1 text-bold-600', {
											'text-white': activeSort && activeSort.index === index
										})}
									>
										% {item.title}
									</span>
								</div>
							);
						})}
					</div>
				</BlankPopUp>
			) : null}
		</div>
	);
};

export default SortBy;
