import * as React from 'react';
import { PlanogramDragDropTypes } from '@src/planogram/generic/DragAndDropType';
import { useDrag } from 'react-dnd';
import { SidebarProductDragable } from '@src/planogram/components/SidebarProductDragable';
import { useSelector, useDispatch } from 'react-redux';
import { AppState } from '@src/planogram/shared/store/app.reducer';

interface Props {
    readonly series: any;
    readonly clickedProduct: number | null;
    readonly isSideBarExpaned: boolean;
    readonly setClickedProduct: (clickedProduct: number | null) => void;
    readonly findIdInShelf: (BarCode: number) => void;
    readonly currentSelectedId: string | number
    readonly aisleItems: any
    readonly shouldDisplayImg:any
}


export const SeriesProducts: React.FC<Props> = (props: Props) => {
    const { series, clickedProduct, isSideBarExpaned, setClickedProduct, findIdInShelf, currentSelectedId, aisleItems,shouldDisplayImg } = props;
    const productDetails = useSelector((state: AppState) => state.planogram.productDetails)

    const [{ isDragging }, dragRef] = useDrag({
        item: {
            type: PlanogramDragDropTypes.SERIES_SIDEBAR,
            payload: series.rows
        },
        // canDrag: monitor => product.dimensions && product.dimensions.height != null && product.dimensions.width != null && product.dimensions.depth != null,
        collect: monitor => ({
            isDragging: monitor.canDrag() && monitor.isDragging(),
        }),
    });

    let seriesProducts = (
        series.rows.map((product: any, pi: number) => {
            return (
                <div className='d-ltr px-05' onClick={() => { setClickedProduct(pi); findIdInShelf(product.BarCode) }}>
                    <SidebarProductDragable
                        picture
                        noCheckBox
                        disableDrag
                        isOnAisle={productDetails[product.BarCode]}
                        isSelected={currentSelectedId && aisleItems && product.BarCode === aisleItems.product}
                        shouldDisplayImg={shouldDisplayImg}
                        product={product}
                        index={pi}
                        clicked={clickedProduct === pi}
                        isSideBarExpaned={isSideBarExpaned}
                        key={"sidebar_product_" + product.BarCode}
                        className={"sidebar-item sidebar-product"}
                    />
                </div>
            )
        })
    )
    return (<div className='d-ltr cursor-pointer width-100-per'>

        {!series.value ?
            <div ref={dragRef} className='d-flex flex-wrap mt-2 mb-2'>{seriesProducts}</div>
            :

            <div className='mt-05 width-100-per'>
                <div className='width-100-per m-auto bg-white border-radius-02rem'>
                    <span className='noselect width-100-per text-bold-700 font-medium-3 text-black m-1 p-1'>{series.value}</span>
                    <div ref={dragRef} className="d-flex flex-wrap ml-1">
                        {seriesProducts}
                    </div>
                </div>
            </div>
        }
    </div>
    );
};

export default SeriesProducts;
