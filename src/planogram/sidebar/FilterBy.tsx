import * as React from 'react';
import classnames from 'classnames';
import { BlankPopUp } from '@components';
import { FormattedMessage, useIntl } from 'react-intl';
import { ChevronDown } from 'react-feather';
import { Accordion } from '@src/components/accordion';
import { groupBy } from '@src/helpers/helpers';
import CustomCheckBoxFilterBy from './CustomCheckBoxFilterBy';
import Select,{components} from 'react-select';

interface Props {
	readonly activeFilter: any;
	readonly setFilterByBox: any;
	readonly setActiveFilter: (type?: any) => void;
	readonly setProductsPerRow: (arr: any[]) => void;
	readonly setProducts: any;
	readonly products: any;
	readonly originalProducts: any;
	readonly filterByBox: any;
	selectedSuppliers: any;
	setSelectedSuppliers: any;
	selectedSegments: any;
	setSelectedSegments: any;
	selectedSubGroups: any;
	setSelectedSubGroups: any;
	selectedBrands: any;
	setSelectedBrands: any;
	mergedGroups: any;
	lowSaleOption: any;
	setLowSaleOption: any;
	setNewProductOption: any;
	newProductOption: any;
	setIsOnPlanoOption: any;
	isOnPlanoOption: any;
}

export const FilterBy: React.FC<Props> = (props: Props) => {
	const { formatMessage } = useIntl();
	const {
		setActiveFilter,
		setFilterByBox,

		setProductsPerRow,
		filterByBox,
		originalProducts,
		selectedSuppliers,
		setSelectedSuppliers,
		selectedSegments,
		setSelectedSegments,
		selectedSubGroups,
		setSelectedSubGroups,
		selectedBrands,
		setSelectedBrands,
		lowSaleOption,
		setLowSaleOption,
		setNewProductOption,
		newProductOption,
		setIsOnPlanoOption,
		isOnPlanoOption
	} = props;
	

	const [suppliers, setSuppliers] = React.useState<any[]>(
		groupBy(originalProducts, [{ field: 'Supplier', id: 'SapakId' }], 0, [], formatMessage)
	);
	const [segment, setSegment] = React.useState<any[]>(
		groupBy(originalProducts, [{ field: 'Segment', id: 'SegmentId' }], 0, [], formatMessage)
	);
	const [subgroupName, setSubgroupName] = React.useState<any[]>(
		groupBy(originalProducts, [{ field: 'SubgroupName', id: 'SubGroupId' }], 0, [], formatMessage)
	);
	const [brand, setBrand] = React.useState<any[]>(
		groupBy(originalProducts, [{ field: 'Brand', id: '' }], 0, [], formatMessage)
	);

	React.useEffect(() => {
		setActiveFilter();
	}, []);

	let selectOptions = [
		{
			label: formatMessage({ id: 'including' }),
			value: 0
		},
		{
			label: formatMessage({ id: 'without' }),
			value: 1
		},
		{
			label: formatMessage({ id: 'only' }),
			value: 2
		}
	];

	const asyncStyle = {
		container: (base: any) => ({
			...base
		}),
		option: (provided: any, state: any) => ({
			...provided,

			'&:hover': {
				color: '#ffffff',
				backgroundColor: '#31baab'
			},
			color: state.isSelected ? '#ffffff' : '#1e1e20',
			backgroundColor: state.isSelected ? '#027b7a' : '#ffffff',
			borderBottom: '1px dotted lightgray',
			padding: 7
		}),
		control: () => ({
			width: 150,
			height: 40
		})
	};
	const Option = (props: any) => {
		return (
			<div>
				<components.Option {...props}>
					<div className='d-flex no-wrap justify-content-start align-items-center'>
					<input type="checkbox" checked={props.isSelected} onChange={() => null} />{' '}
					<label className='elipsis'>{props.label}</label>
					</div>
				</components.Option>
			</div>
		);
	};

	const MultiValue = (props: any) => (
		<components.MultiValue {...props}>
			<span>{props.data.label}</span>
		</components.MultiValue>
	);
	return (
		<div>
			<div
				onClick={() => setFilterByBox(!filterByBox)}
				className={classnames(
					'd-flex justify-content-center width-8-5-rem bg-white border-light-table position-relative font-small-4 cursor-pointer mb-05 py-05 border-2 px-2 text-black text-bold-700  mr-05 border-radius-2rem',
					{
						'border-turquoise': filterByBox
					}
				)}
			>
				{<FormattedMessage id={'filterBy'} />}
				<ChevronDown
					color={filterByBox ? '#31baab' : 'black'}
					size={17}
					className={classnames('align-self-center ml-05', {
						'rotate-180': filterByBox
					})}
				/>
			</div>
			{filterByBox ? (
				<BlankPopUp
					noXbutton
					onOutsideClick={setFilterByBox}
					style={{ width: '16.5rem', height: '32rem' }}
					className="overflow-y-scroll overflow-x-auto"
				>
					<div className="d-flex flex-column justify-content-center mt-1 mb-2 ml-05">
						<span className="text-black font-medium-3 font-bold text-bold-700 align-self-start ml-1">
							{formatMessage({ id: 'filterBy' })}
						</span>
					</div>
					<div className="d-flex flex-column ml-1 mb-2">
						{[
							{ text: 'lowsale', state: lowSaleOption, setState: setLowSaleOption },
							{ text: 'newBarcode', state: newProductOption, setState: setNewProductOption },
							{ text: 'inPlanogram', state: isOnPlanoOption, setState: setIsOnPlanoOption }
						].map((i: any) => {
							return (
								<div className="mb-05 d-flex justify-content-center align-items-center">
									<label className="mr-auto text-small-3">{formatMessage({ id: i.text })}</label>
									<div className="mr-05">
										<Select
											name={i.text}
											isDisabled={false}
											isClearable={false}
											styles={asyncStyle}
											defaultValue={selectOptions[i.state]}
											options={selectOptions}
											onChange={(e: any, actionMeta: any) => {
												if (e) {
													i.setState(e.value);
												}
											}}
											classNamePrefix="select_filter"
											noOptionsMessage={() => formatMessage({ id: 'trending_noResults' })}
											placeholder={''}
										/>
									</div>
								</div>
							);
						})}
					</div>

					<div className="d-flex flex-column justify-content-center mt-1 mb-2 ml-05">
						<span className="text-black font-medium-3 font-bold text-bold-700 align-self-start ml-1">
							{formatMessage({ id: 'dataInTable' })}
						</span>
					</div>
					{[
						{
							title: formatMessage({ id: 'SupplierName' }),
							text:formatMessage({ id: 'allSuppliers' }),
							data: suppliers,
							selectedData: selectedSuppliers,
							setSelectedData: setSelectedSuppliers
						},
						{
							title: formatMessage({ id: 'Segment' }),
							text:formatMessage({ id: 'allSegments' }),
							data: segment,
							selectedData: selectedSegments,
							setSelectedData: setSelectedSegments
						},
						{
							title: formatMessage({ id: 'SubgroupName' }),
							text:formatMessage({ id: 'allSubGroups' }),
							data: subgroupName,
							selectedData: selectedSubGroups,
							setSelectedData: setSelectedSubGroups
						},
						{
							title: formatMessage({ id: 'Brand' }),
							text:formatMessage({ id: 'allBrands' }),
							data: brand,
							selectedData: selectedBrands,
							setSelectedData: setSelectedBrands
						}
					].map((i: any) => {
	
						return (
							<div className="mb-05 d-flex justify-content-center align-items-center">
								<label className="mr-auto text-small-3">{formatMessage({ id: i.title })}</label>
								<div className="mr-05">
									<Select
										components={{ Option, MultiValue }}
										name={i.title}
										placeholder={i.text}
										isDisabled={false}
										isClearable={true}
										isMulti={true}
										closeMenuOnSelect={false}
										hideSelectedOptions={false}
										styles={asyncStyle}
										defaultValue={i.selectedData.length == i.data.length ? []: i.selectedData.map((i: any) => {
											return { value: i, label: i };
										})}
										options={i.data.map((i: any) => {
											return { value: i.value, label: i.value };
										})}
										onChange={(e: any, actionMeta: any) => {
											if (e && e.length) {
												i.setSelectedData(e.map((it:any)=>{return it.value}))
											}else{
												i.setSelectedData(i.data.map((i: any) => {
													return i.value
												}))
											}
										}}
										classNamePrefix="select_filterA"
										noOptionsMessage={() => formatMessage({ id: 'noResults' })}
									/>
								</div>
							</div>
						);
						// <Accordion
						// 	className="p-ac"
						// 	caption={item.title}
						// 	captionClassName="font-medium-2 ml-05 d-flex"
						// 	childClassName="d-flex flex-wrap width-100-per"
						// 	buttonClassName="d-flex justify-content-center align-items-center"
						// 	style={{
						// 		borderLeft: 0,
						// 		borderRight: 0,
						// 		borderTop: '1px solid #f3f3f5',
						// 		borderBottom: '1px solid #f3f3f5'
						// 	}}
						// >
						// 	<div className="d-flex flex-column">
						// 		{item.data.map((i: any, index: number) => {
						// 			return (
						// 				<CustomCheckBoxFilterBy
						// 					data={i}
						// 					selectedData={item.selectedData}
						// 					setSelectedData={item.setSelectedData}

						// 				/>
						// 			);
						// 		})}
						// 	</div>
						// </Accordion>
					})}
				</BlankPopUp>
			) : null}
		</div>
	);
};

export default FilterBy;
