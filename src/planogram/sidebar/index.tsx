import * as React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { AppState } from '@src/planogram/shared/store/app.reducer';
import classnames from 'classnames';
import { config } from '@src/config';
import { Icon } from '@components';
import { SidebarProductDragable } from '@src/planogram/components/SidebarProductDragable';
import {
	setCurrentSelectedId,
	hideProductDetailer,
	setMultiSelectId
} from '@src/planogram/shared/store/planogram/planogram.actions';
import { fetchAndUpdatePlanogram } from '@src/helpers/planogram';
import { FormattedMessage, useIntl } from 'react-intl';
import { Badge } from 'reactstrap';
import { groupBy, Sorting } from '@src/helpers/helpers';
import { Accordion } from '@src/components/accordion';
import { SeriesProducts } from './SeriesProducts';
import { SortBy } from './SortBy';
import { FilterBy } from './FilterBy';
import { ColumnDisplayer } from './ColumnDisplayer';
import './sidebar.scss';
import { useEffect } from 'react';
import { ModalInterface } from '@src/components/GenericModals/interfaces';
import { GenericModals } from '@src/components/GenericModals/index';
import { removeProductsFromAisle } from '@src/planogram/shared/store/planogram/store/aisle/aisle.actions';
import { NEWPLANO_ACTION } from '@src/planogram/shared/store/newPlano/newPlano.types';
import { rowData } from '../DataReports';

interface Props {
	readonly products: any[];
	readonly setIsSaving: (b: boolean) => void;
}
export const Sidebar: React.FC<Props> = (props: Props) => {
	const { setIsSaving } = props;
	const dispatch = useDispatch();
	const { formatMessage } = useIntl();
	const url = window.location.href;
	const urlElements = url.split('/');
	let currentAisleIndex = parseInt(urlElements[8]);
	const store = useSelector((state: AppState) => state.planogram.store);
	const newPlano = useSelector((state: AppState) => state.newPlano);
	const catalog = useSelector((state: AppState) => state.catalog.products);
	const productDetails = useSelector((state: AppState) => state.planogram.productDetails);
	let currentType = urlElements[4];
	let PageType = urlElements[5];
	let productNameKey = currentType === 'branch' ? 'Name' : 'ProductName';
	const productsOnAisles = useSelector((state: AppState) => state.planogram.productDetails);
	const shelfMap = useSelector((state: AppState) => state.planogram.virtualStore.shelfMap);
	const currentSelectedId = useSelector((state: AppState) => state.planogram.display.currentSelectedId);
	const aisle: any =
		store && store.aisles
			? store.aisles.filter((aisle: any) => aisle.id == currentSelectedId.split('SE')[0])
			: null;
	let section =
		aisle && aisle[0] && aisle[0].sections
			? aisle[0].sections.filter((section: any) => section.id === currentSelectedId.split('SH')[0])
			: null;
	let shelf =
		section && section[0] && section[0].shelves
			? section[0].shelves.filter((shelf: any) => shelf.id === currentSelectedId.split('I')[0])
			: null;
	let item =
		shelf && shelf[0] && shelf[0].items
			? shelf[0].items.filter((item: any) => item.id === currentSelectedId)[0]
			: null;

	let currentAisle: any =
		store && store.aisles && store.aisles.length && currentAisleIndex
			? store.aisles.filter((a: any) => a.aisle_id === currentAisleIndex)[0]
			: undefined;

	const [allProducts, setAllProducts] = React.useState<any[]>(
		props.products
			? props.products.sort(function (a: any, b: any) {
					return b.isSelected - a.isSelected;
			  })
			: []
	);
	const [products, setProducts] = React.useState<any[]>(
		props.products
			? props.products.sort(function (a, b) {
					return b.isSelected - a.isSelected;
			  })
			: []
	);

	const [productsPerRow, setProductsPerRow] = React.useState<any[]>([]);
	const [sideBarExpanedLevel, setSideBarExpanedLevel] = React.useState<number>(1);
	const [isColumnDisplayerPopupOpen, setIsColumnsDisplayerPopupOpen] = React.useState<boolean>(false);
	const [serieses, setSerieses] = React.useState<boolean>(false);
	const [filter, setFilter] = React.useState<string>('');
	const [option, setOption] = React.useState<string>('');
	const [columns, setColumns] = React.useState<any[string]>([
		{ name: 'TotalPrice', selected: false },
		{ name: 'TotalAmount', selected: false },
		{ name: 'Percent_TotalPrice', prefix: '%', selected: true },
		{ name: 'Percent_TotalAmount', prefix: '%', selected: true },
		{ name: 'Percent_Avg', prefix: '%', selected: true },
		{ name: 'Percent_Profit', prefix: '%', selected: true }
	]);
	const [columnsDisplayed, setColumnsDisplayed] = React.useState<any>([]);
	useEffect(() => {
		let newColumnsHeader = columns
			.map((column: any, index: number) => {
				if (!column.selected) return null;
				return column;
			})
			.filter((g: any) => g);
		setColumnsDisplayed(newColumnsHeader);
	}, [columns]);

	useEffect(() => {
		if (currentSelectedId && item && item.product) {
			let selectedIdDiv = document.getElementById(`A-${item.product}`);
			let divPositionFromParrent = getPosition(selectedIdDiv);
			let selectedDivHeight = 0;
			if (selectedIdDiv) {
				selectedDivHeight = selectedIdDiv.offsetHeight;
			}
			let parrentElement = document.getElementById('productsContainer');
			if (parrentElement && divPositionFromParrent) {
				if (selectedIdDiv) {
					let extraScroll = 0;
					if (option) extraScroll = 60;
					parrentElement.scrollTo({
						top: divPositionFromParrent.y - selectedDivHeight - selectedDivHeight / 1.7 - extraScroll,
						behavior: 'smooth'
					});
				} else {
					let parrentElement: any = document.getElementById('productsContainer');
					let noImgDivs: any = [];
					if (parrentElement && parrentElement.lastChild) {
						parrentElement.lastChild.childNodes.forEach((i: any) => {
							if (i.className.includes('imgIsNotDisplayed')) noImgDivs.push(i);
						});
						if (noImgDivs[0]) {
							let divPositionFromParrent = getPosition(noImgDivs[0]);
							handleScroll(null, divPositionFromParrent.y - 200 - noImgDivs[0].offsetHeight);
						}
					}
				}
			}
		}
	}, [currentSelectedId]);

	const [clickedProduct, setClickedProduct] = React.useState<number | null>(null);
	let groups = [
		{ field: 'Supplier', id: 'SapakId' },
		{ field: 'Segment', id: 'SegmentId' },
		{ field: 'Brand', id: '' },
		{ field: 'Series', id: 'DegemId' },
		{ field: 'SubgroupName', id: 'SubGroupId' }
	];
	const [mergedGroups, setMergedGroups] = React.useState<any>([]);
	const [tempMergedGroups, setTempMergedGroups] = React.useState<any>([]);
	const customizer = { direction: document.getElementsByTagName('html')[0].dir };
	const [sortByBox, setSortByBox] = React.useState<boolean>(false);
	const [activeSort, setActiveSort] = React.useState<any>();
	const [filterByBox, setFilterByBox] = React.useState<boolean>(false);
	const [activeFilter, setActiveFilter] = React.useState<boolean>(false);

	const [selectedSuppliers, setSelectedSuppliers] = React.useState<any>(null);
	const [selectedSegments, setSelectedSegments] = React.useState<any>(null);
	const [selectedSubGroups, setSelectedSubGroups] = React.useState<any>(null);
	const [selectedBrands, setSelectedBrands] = React.useState<any>(null);
	const [limitedProductsRows, setLimitedProductsRows] = React.useState<number>(10);
	const [limitedSeriesRows, setLimitedSeriesRows] = React.useState<number>(6);

	const [checkboxModal, toggleCheckboxModal] = React.useState<boolean>(false);
	const [clickedCheckboxId, setClickedCheckboxId] = React.useState<any>(null);

	React.useEffect(() => {
		setProducts(allProducts);
		setProductsPerRow(allProducts);

		if (selectedSuppliers == null) {
			setSelectedSuppliers(
				groupBy(allProducts, [{ field: 'Supplier', id: 'SapakId' }], 0, [], formatMessage).map(
					(supplier: any) => {
						return supplier.value;
					}
				)
			);
		}
		if (selectedSegments == null) {
			setSelectedSegments(
				groupBy(allProducts, [{ field: 'Segment', id: 'SegmentId' }], 0, [], formatMessage).map(
					(segment: any) => {
						return segment.value;
					}
				)
			);
		}
		if (selectedSubGroups == null) {
			setSelectedSubGroups(
				groupBy(allProducts, [{ field: 'SubgroupName', id: 'SubGroupId' }], 0, [], formatMessage).map(
					(subgroupName: any) => {
						return subgroupName.value;
					}
				)
			);
		}
		if (selectedBrands == null) {
			setSelectedBrands(
				groupBy(allProducts, [{ field: 'Brand', id: '' }], 0, [], formatMessage).map((brand: any) => {
					return brand.value;
				})
			);
		}
	}, [allProducts]);

	React.useEffect(() => {
		if (clickedCheckboxId) toggleCheckboxModal(true);
	}, [clickedCheckboxId]);

	function getPosition(element: any) {
		var xPosition = 0;
		var yPosition = 0;

		while (element) {
			xPosition += element.offsetLeft - element.scrollLeft + element.clientLeft;
			yPosition += element.offsetTop - element.scrollTop + element.clientTop;
			element = element.offsetParent;
		}

		return { x: xPosition, y: yPosition };
	}

	const mergeByGroups = (mergedGroups: any[], field?: any, reverse?: boolean) => {
		if (field) {
			let newMergedGroups = [...mergedGroups, field];
			if (reverse) {
				newMergedGroups = mergedGroups.filter((f: any) => f.field !== field.field);
				setMergedGroups(newMergedGroups);
			} else setMergedGroups([...mergedGroups, field]);

			let groups: any = groupBy(
				productsPerRow[0] && productsPerRow[0].BarCode ? productsPerRow : allProducts,
				newMergedGroups,
				0,
				[],
				formatMessage
			);
			setProducts(newMergedGroups && newMergedGroups.length ? groups : allProducts);
			setProductsPerRow(productsPerRow[0] && productsPerRow[0].BarCode ? productsPerRow : allProducts);
			setOption(option);
		}
	};

	const [lowSaleOption, setLowSaleOption] = React.useState<any>(0);
	const [newProductOption, setNewProductOption] = React.useState<any>(0);
	const [isOnPlanoOption, setIsOnPlanoOption] = React.useState<any>(0);

	const updateProductsByFilter = (productsArr: any) => {
		let canFilter = false;
		let afterFilterProducts = [];

		let allSelected: any = {
			suppliers: selectedSuppliers,
			segments: selectedSegments,
			subGroups: selectedSubGroups,
			brands: selectedBrands
		};
		if (
			allSelected.suppliers != undefined &&
			allSelected.segments != undefined &&
			allSelected.subGroups != undefined &&
			allSelected.brands != undefined
		) {
			canFilter = true;
		}

		if (canFilter) {
			afterFilterProducts = productsArr.filter((product: any) => {
				let checkAmount = 7;
				let trueAmount = 0;

				if (checkTypeOption(lowSaleOption, product, 'LowSale')) trueAmount++;
				if (checkTypeOption(newProductOption, product, 'NewBarCode')) trueAmount++;
				if (checkTypeOption(isOnPlanoOption, product, 'isOnPlano')) trueAmount++;
				if (allSelected.suppliers.includes(product.Supplier)) trueAmount++;
				if (allSelected.subGroups.includes(product.SubgroupName)) trueAmount++;
				if (allSelected.segments.includes(product.Segment)) trueAmount++;
				if (allSelected.brands.includes(product.Brand)) trueAmount++;

				return checkAmount === trueAmount ? true : false;
			});
		}
		return canFilter ? afterFilterProducts : productsArr;
	};
	const checkTypeOption = (value: any, product: any, flag: any) => {
		if (flag != 'isOnPlano') {
			if (value == 0) return true;
			else if (value == 1) {
				if (product[flag] == false) return true;
				else return false;
			} else if (value == 2) {
				if (product[flag] == true) return true;
				else return false;
			} else return true;
		} else {
			let isOnMyAisle: boolean = false;
			productDetails &&
				productDetails[product.BarCode] &&
				productDetails[product.BarCode].position.forEach((i: any) => {
					if (i.aisle_id == currentAisleIndex) isOnMyAisle = true;
				});
			if (value == 0) return true;
			else if (value == 1) {
				if (isOnMyAisle == false) return true;
				else return false;
			} else if (value == 2) {
				if (isOnMyAisle) return true;
				else return false;
			} else return true;
		}
	};
	const optionIcon = (option: any) => {
		return (
			<div
				className={classnames('border-radius-3 cursor-pointer ml-05 p-05', {
					'bg-turquoise': option.isSelected,
					'bg-light-gray': !option.isSelected
				})}
				onClick={
					option.onClick
						? option.isSelected
							? () => {
									setOption('');
									setMergedGroups([]);
									setProductsPerRow(allProducts);
									setProducts(allProducts);
							  }
							: option.onClick
						: () => {}
				}
			>
				<Icon
					src={`../../../${config.iconsPath}planogram/sidebar/${
						option.isSelected ? option.iconSelected : option.icon
					}.svg`}
					style={{ height: '1.5rem', width: '0.5rem' }}
				/>
			</div>
		);
	};

	const getOption = (option: string) => {
		switch (option) {
			case 'merging':
				return (
					<div className="width-100-per flex-wrap  d-flex pl-1 mb-05">
						{groups &&
							groups.length &&
							groups.map((group: any, index: number) => {
								let pos = mergedGroups.findIndex((field: any) => field.field == group.field);
								return (
									<div
										key={index}
										onClick={
											pos === -1
												? () => {
														mergeByGroups(mergedGroups, group);
												  }
												: () => {
														mergeByGroups(mergedGroups, group, true);
												  }
										}
										className={classnames(
											'd-flex align-items-center bg-gray position-relative font-small-3 cursor-pointer py-05 border-2 px-1 text-black text-bold-700  mr-05 border-radius-2rem',
											{
												'border-turquoise': pos >= 0
											}
										)}
									>
										{<FormattedMessage id={group.field} />}
										<Badge
											pill
											color="primary"
											className="badge-up bg-turquoise border-white border-1 text-bold-600 text-white width-1-rem position-left-0 position-top-02"
										>
											{mergedGroups.findIndex((mg: any) => mg.field === group.field) > -1
												? mergedGroups.findIndex((mg: any) => mg.field === group.field) + 1
												: null}
										</Badge>
									</div>
								);
							})}
					</div>
				);
			case 'search':
				return (
					<div
						className="d-flex align-items-center min-width-15-rem pl-2 mb-1 ml-05 height-1-rem py-1 border-radius-2rem"
						style={{ border: '1px solid #f3f3f5' }}
					>
						<Icon
							src={'../../' + config.iconsPath + 'planogram/sidebar/search.svg'}
							style={{ height: '0.5rem', width: '0.5rem' }}
						/>
						<input
							placeholder={formatMessage({ id: 'search_product' })}
							className="width-100-per no-border placeholder-text-bold-700 placeholder-text-black text-bold-700 ml-05"
							type="text"
							value={filter}
							onChange={(e: any) => setFilter(e.target.value)}
						/>
					</div>
				);
			case 'filter':
				return (
					<div className="width-100-per flex-wrap  d-flex pl-1 mb-05 ml-05">
						<FilterBy
							activeFilter={activeFilter}
							setFilterByBox={setFilterByBox}
							setActiveFilter={setActiveFilter}
							setProducts={setProducts}
							setProductsPerRow={setProductsPerRow}
							products={products}
							filterByBox={filterByBox}
							mergedGroups={mergedGroups}
							originalProducts={allProducts}
							selectedSuppliers={selectedSuppliers}
							setSelectedSuppliers={setSelectedSuppliers}
							selectedSegments={selectedSegments}
							setSelectedSegments={setSelectedSegments}
							selectedSubGroups={selectedSubGroups}
							setSelectedSubGroups={setSelectedSubGroups}
							selectedBrands={selectedBrands}
							setSelectedBrands={setSelectedBrands}
							setLowSaleOption={setLowSaleOption}
							lowSaleOption={lowSaleOption}
							newProductOption={newProductOption}
							setNewProductOption={setNewProductOption}
							isOnPlanoOption={isOnPlanoOption}
							setIsOnPlanoOption={setIsOnPlanoOption}
						/>

						<SortBy
							activeSort={activeSort}
							mergedGroups={mergedGroups}
							setSortByBox={setSortByBox}
							setActiveSort={setActiveSort}
							setProducts={setProducts}
							productsPerRow={productsPerRow}
							setProductsPerRow={setProductsPerRow}
							products={products}
							sortByBox={sortByBox}
						/>
					</div>
				);
			default:
				return;
		}
	};

	const seriesDisplay = (productsRow: any) => {
		let sortedBySeries: any = groupBy(productsRow, [{ field: 'Series', id: '' }], 0, [], formatMessage);
		let sortedSeriesNullLast = Sorting(sortedBySeries, {
			field: 'value',
			type: 'string',
			asc: true,
			times: 0
		}).reverse();
		let sortedSeriesNoUnSelected = sortedSeriesNullLast
			.map((series: any) => {
				let seriesRowWithoutSelected: any = [];
				if (series.rows && series.rows.length) {
					seriesRowWithoutSelected = series.rows
						.map((sRow: any) => {
							if (sRow.isSelected) return { ...sRow };
							else return '';
						})
						.filter((r: any) => r);
					if (seriesRowWithoutSelected.length) return { ...series, rows: seriesRowWithoutSelected };
					else return '';
				}
			})
			.filter((y: any) => y);
		let seriesSorted: any = sortedSeriesNoUnSelected;
		let activeSortT = '';
		if (activeSort) {
			switch (activeSort.type) {
				case 'totalPrice':
					activeSortT = 'percentPrice';
					break;
				case 'totalAmount':
					activeSortT = 'percentAmount';
					break;
				case 'avg':
					activeSortT = 'percentAvg';
					break;
				case 'profit':
					activeSortT = 'percentTotalProfit';
					break;
				default:
					activeSortT = '';
					break;
			}
		}
		if (activeSortT.length) {
			seriesSorted = Sorting(sortedSeriesNoUnSelected, {
				field: activeSortT,
				type: 'number',
				asc: false,
				times: 0
			});
		}
		let seriesDis = seriesSorted.map((series: any, index: number) =>
			limitedSeriesRows > index ? (
				<SeriesProducts
					clickedProduct={clickedProduct}
					setClickedProduct={setClickedProduct}
					series={series}
					shouldDisplayImg={limitedSeriesRows > index}
					isSideBarExpaned={sideBarExpanedLevel == 2}
					findIdInShelf={findIdInShelf}
					currentSelectedId={currentSelectedId}
					aisleItems={item}
				/>
			) : (
				<></>
			)
		);
		return seriesDis;
	};

	const findIdInShelf = (BarCode: any) => {
		if (
			productsOnAisles &&
			productsOnAisles[BarCode] &&
			productsOnAisles[BarCode].position &&
			productsOnAisles[BarCode].position[0]
		) {
			// productsOnAisles[BarCode].position[0].aisle_id == currentAisleIndex &&
			let isInThisSection: boolean = false;
			let foundIndex: number = 0;
			productsOnAisles[BarCode].position.forEach((p: any, index: number) => {
				if (p.aisle_id == currentAisleIndex) {
					foundIndex = index;
					isInThisSection = true;
				}
			});
			if (isInThisSection) {
				let shelfId = productsOnAisles[BarCode].position[foundIndex].shelf;
				let shelfProducts = shelfMap[shelfId].items;

				let chosenProduct: string = '';
				shelfProducts.forEach((product: any) => {
					if (product.product == BarCode) chosenProduct = product.id;
				});
				dispatch(setCurrentSelectedId(chosenProduct));
			} else {
				dispatch(hideProductDetailer());
				dispatch(setMultiSelectId([]));
				dispatch(setCurrentSelectedId(''));
			}
		} else {
			dispatch(hideProductDetailer());
			dispatch(setMultiSelectId([]));
			dispatch(setCurrentSelectedId(''));
		}
	};

	const checkboxModalData: ModalInterface = {
		classNames: 'width-35-per min-width-30-rem max-width-30-rem',
		isOpen: checkboxModal,
		toggle: () => {
			toggleCheckboxModal(!checkboxModal);
			setTimeout(() => {
				setClickedCheckboxId(null);
			}, 5);
		},
		header: clickedCheckboxId
			? productsPerRow.filter((g: any) => g.id == clickedCheckboxId && g.isSelected && g.status > 0).length > 0
				? formatMessage({ id: 'warchingBeforeRemove' })
				: formatMessage({ id: 'warchingBeforeAdd' })
			: formatMessage({ id: 'warchingBeforeAdd' }),
		body: clickedCheckboxId
			? productsPerRow.filter((g: any) => g.id == clickedCheckboxId && g.isSelected && g.status > 0).length > 0
				? formatMessage({ id: 'areYouSureUncheck' })
				: formatMessage({ id: 'areYouSureCheck' })
			: formatMessage({ id: 'areYouSureCheck' }),
		buttons: [
			{
				color: 'primary',
				outline: true,
				onClick: () => {
					onCheckBoxClicked();
				},
				label:
					clickedCheckboxId &&
					productsPerRow.filter((g: any) => g.id == clickedCheckboxId && g.isSelected && g.status > 0)
						.length > 0
						? formatMessage({ id: 'yesDelete' })
						: formatMessage({ id: 'yesAdd' })
			},
			{
				color: 'primary',
				onClick: () => {
					toggleCheckboxModal(false);
					setTimeout(() => {
						setClickedCheckboxId(null);
					}, 5);
				},
				label: formatMessage({ id: 'no' })
			}
		]
	};

	const onCheckBoxClicked = async () => {
		let productsAfterCheckboxChange = allProducts.map((p: any) => {
			let isSelected = p.isSelected;
			let status = p.status;
			let faces = p.faces;
			if (p.id == clickedCheckboxId) {
				isSelected = !p.isSelected;
				status = p.status == 1 ? 0 : 1;
				if (isSelected && p.faces == 0) faces = 1;
			}
			return { ...p, isSelected, status, faces };
		});
		// allProducts.forEach((i:any)=>{if(i.BarCode==7290002543237 ||i.BarCode==7290000114897 ) console.log(i)})

		let product: any = allProducts.filter((p: any) => p.id === clickedCheckboxId)[0];
		dispatch(removeProductsFromAisle(currentAisle.aisle_id, [product ? product.BarCode : null]));
		let rowsAfterReCalculation = rowData(productsAfterCheckboxChange, formatMessage);

		setAllProducts(rowsAfterReCalculation);
		setProductsPerRow(rowsAfterReCalculation);

		if (mergedGroups && mergedGroups.length) {
			setProducts(groupBy(rowsAfterReCalculation, mergedGroups, 0, [], formatMessage));
		} else {
			setProducts(rowsAfterReCalculation);
		}

		setClickedCheckboxId(null);
		toggleCheckboxModal(false);
		setIsSaving(true);
		//todo: save aisle only if product exists on any shelf
		let saveBtn: any = document.getElementById('save_btn');
		setTimeout(() => saveBtn.click(), 2000);
		dispatch({
			type: NEWPLANO_ACTION.SET_NEWPLANO,
			payload: {
				FromDate: newPlano.FromDate ? newPlano.FromDate : null,
				ToDate: newPlano.ToDate ? newPlano.ToDate : null,
				data: productsAfterCheckboxChange
			}
		});
		await fetchAndUpdatePlanogram(
			[
				{
					forceUpdate: true,
					type: 'newplano',
					dontUpdateState: true,
					setDataRows: true,
					changeRows: formatMessage,
					params: { updatedb: 2, aisle_id: currentAisleIndex, newplano: productsAfterCheckboxChange }
				}
			],
			dispatch
		);
		setIsSaving(false);
	};

	const mainProducts = (product: any, pi: any, limitedProductsRows: number) => {
		let isOnMyAisle = false;
		productDetails[product.BarCode] &&
			productDetails[product.BarCode].position.forEach((i: any) => {
				if (i.aisle_id == currentAisleIndex) isOnMyAisle = true;
			});

		return (
			<div
				className={`d-ltr ${limitedProductsRows > pi ? 'imgIsDisplayed' : 'imgIsNotDisplayed'}`}
				onClick={() => {
					setClickedProduct(pi);
					findIdInShelf(product.BarCode);
				}}
				id={`A-${product.BarCode}`}
			>
				<SidebarProductDragable
					onCheckBoxClicked={PageType === 'editor' ? setClickedCheckboxId : undefined}
					picture={sideBarExpanedLevel === 3}
					columns={columns}
					isSelected={currentSelectedId && item && product.BarCode === item.product}
					isOnAisle={isOnMyAisle}
					product={product}
					index={pi}
					shouldDisplayImg={limitedProductsRows > pi}
					clicked={clickedProduct === pi}
					isSideBarExpaned={sideBarExpanedLevel == 2}
					key={'sidebar_product_' + product.BarCode}
					className={'sidebar-item sidebar-product'}
				/>
			</div>
		);
	};
	const customAccordionCaption = (data: any) => {
		if (sideBarExpanedLevel == 2 && !serieses) {
			return (
				<div className="d-flex justify-content-between flex-direction-row-reverse">
					<span className="d-ltr elipsis width-90-per">
						{data.value ? data.value : formatMessage({ id: 'other' })}
					</span>
					<span className="divider-small"></span>
					{/* <div className="width-50-per d-flex justify-content-between text-black font-small-3-03 mr-03"> */}
					<div className="mr-03 width-100-per d-flex-reverse-row justify-content-around align-items-center text-bold-700 text-black">
						{columns.map((column: any, index: number) => {
							let nData = data;
							nData.TotalPrice = nData.totalRowsPrice ? nData.totalRowsPrice.toLocaleString() : '';
							nData.TotalAmount = nData.totalRowsAmount ? nData.totalRowsAmount.toLocaleString() : '';
							nData.Percent_TotalPrice = `${nData.percentPrice.toFixed(2)}%`;
							nData.Percent_TotalAmount = `${nData.percentAmount.toFixed(2)}%`;
							nData.Percent_Avg = `${nData.percentAvg.toFixed(2)}%`;

							if (!column.selected) return null;
							else if (column.name == 'Percent_Profit')
								return <div className="visibility-hidden">100%</div>;
							else return <div>{nData[column.name] ? nData[column.name] : 0}</div>;
						})}
					</div>
				</div>
			);
		} else {
			return <span>{data.value ? data.value : formatMessage({ id: 'other' })}</span>;
		}
	};
	const accordionProducts = (productsRows: any[], ind: number) => {

		return (
			<div className="d-flex flex-wrap width-100-per mt-05">
				{productsRows && productsRows.length
					? productsRows.map((product: any, index: number) => (
							<Accordion
								data={product}
								caption={customAccordionCaption(product)}
								// caption={product.value ? product.value : formatMessage({ id: 'other' })}
								captionClassName="font-small-3-5 width-100-per"
								childClassName="d-flex flex-wrap width-100-per"
								buttonClassName={classnames(
									'width-100-per align-left bg-white border-radius-02rem d-flex justify-content-end',
									{
										'p-1-5': sideBarExpanedLevel == 2,
										'p-05': sideBarExpanedLevel == 1
									}
								)}
								childNum={ind}
								borderBottomOnOpen={true}
								keepHeaderOnScroll={true}
								dataClass={'width-100-per'}
								noMargin
								className={`width-100-per sidebarAccordion border-radius-02rem`}
								style={{
									borderLeft: 0,
									borderRight: 0,
									borderTop: '1px solid #f3f3f5',
									borderBottom: '1px solid #f3f3f5'
								}}
							>
								{product.rows && product.rows[0] && !product.rows[0].BarCode && mergedGroups[1] ? (
									accordionProducts(product.rows, ind + 1)
								) : (
									<div className={classnames('d-flex flex-wrap width-100-per', {})}>
										{serieses
											? seriesDisplay(updateProductsByFilter(product.rows))
											: product.rows && product.rows[0]
											? product.rows.map((product: any, pi: number) => (
													<div
														className="d-ltr border-radius-02rem sidebar-item sidebar-product width-100-per d-flex"
														onClick={() => findIdInShelf(product.BarCode)}
														id={`A-${product.BarCode}`}
													>
														<SidebarProductDragable
															onCheckBoxClicked={
																PageType === 'editor' ? setClickedCheckboxId : undefined
															}
															isOnAisle={productDetails[product.BarCode]}
															columns={columns}
															isSelected={item && product.BarCode === item.product}
															product={product}
															index={pi}
															clicked={clickedProduct === pi}
															isSideBarExpaned={sideBarExpanedLevel == 2}
															key={'sidebar_product_' + product.BarCode}
															className={classnames(
																'border-radius-02rem sidebar-item sidebar-product width-100-per d-flex',
																{}
															)}
														/>
													</div>
											  ))
											: null}
									</div>
								)}
							</Accordion>
					  ))
					: null}
			</div>
		);
	};

	const handleScroll = (e?: any, manualScrollNum?: any) => {
		const element = e ? e.target : manualScrollNum;
		// console.log(element.scrollHeight)
		// console.log(element.scrollTop)
		// console.log(element.clientHeight)

		let parrentElement: any = document.getElementById('productsContainer');
		let noImgDivs: any = [];

		if (parrentElement && parrentElement.lastChild) {
			parrentElement.lastChild.childNodes.forEach((i: any) => {
				if (i.className.includes('imgIsNotDisplayed')) noImgDivs.push(i);
			});

			if (noImgDivs[0]) {
				let divPositionFromParrent = getPosition(noImgDivs[0]);
				if (
					divPositionFromParrent.y - 200 - noImgDivs[0].offsetHeight > manualScrollNum
						? manualScrollNum
						: element.scrollTop
				) {
					serieses
						? setLimitedSeriesRows(limitedSeriesRows + 3)
						: setLimitedProductsRows(limitedProductsRows + 10);
				}
			}
		}
		// if (
		// 	element.scrollHeight - Math.ceil(element.scrollTop) === element.clientHeight ||
		// 	element.scrollHeight - Math.floor(element.scrollTop) === element.clientHeight
		// ) {
		// 	serieses ? setLimitedSeriesRows(limitedSeriesRows + 3) : setLimitedProductsRows(limitedProductsRows + 10);
		// }
	};
	return (
		<>
			<GenericModals key={`checkboxModal`} modal={checkboxModalData} />
			<div
				style={{ transition: 'width 0.5s' }}
				id="planogram-sidebar"
				className={classnames(
					'zindex-10001 position-fixed position-left-0 position-top-5 full-height bg-white d-flex-column',
					{
						'width-30-rem': sideBarExpanedLevel === 1,
						'width-45-rem': sideBarExpanedLevel === 2,
						'width-8-rem': sideBarExpanedLevel === 3
					}
				)}
			>
				<div
					onClick={() => {
						setSideBarExpanedLevel(
							sideBarExpanedLevel === 1
								? 2
								: sideBarExpanedLevel === 2
								? 3
								: sideBarExpanedLevel === 3
								? 1
								: 1
						);
						if (sideBarExpanedLevel === 2) {
							setSerieses(false);
							setTempMergedGroups(mergedGroups && mergedGroups.length ? mergedGroups : []);
							setMergedGroups([]);
							setProductsPerRow(
								(productsPerRow ? productsPerRow : allProducts).sort(function (a, b) {
									return b.isSelected - a.isSelected;
								})
							);
							setProducts(
								(productsPerRow ? productsPerRow : allProducts).sort(function (a, b) {
									return b.isSelected - a.isSelected;
								})
							);
						} else if (sideBarExpanedLevel === 3) {
							setMergedGroups(tempMergedGroups);
							setTempMergedGroups([]);
						}
					}}
					className={classnames(
						'position-absolute box-shadow-1 position-right-minus-1-rem position-top-1-5 cursor-pointer rounded bg-white d-flex align-items-center justify-content-center',
						{
							'bg-turquoise': sideBarExpanedLevel === 2
						}
					)}
					style={{ padding: '0.3rem' }}
				>
					<div
						className={classnames('', {
							'rotate-180': customizer.direction == 'ltr'
						})}
					>
						<Icon
							className={classnames('', {
								'rotate-180': sideBarExpanedLevel === 2
							})}
							src={`../../${config.iconsPath}planogram/sidebar/${
								sideBarExpanedLevel === 2 ? 'expand-white' : 'expand'
							}.svg`}
							style={{ height: '1.5rem', width: '0.5rem' }}
						/>
					</div>
				</div>

				<>
					<div className="height-5-rem d-flex align-items-center justify-content-center px-2">
						{sideBarExpanedLevel != 3 && (
							<div
								onClick={
									serieses
										? () => {
												setSerieses(!serieses);
												setMergedGroups([]);
												setProductsPerRow(
													allProducts.sort(function (a, b) {
														return b.isSelected - a.isSelected;
													})
												);
												setProducts(
													allProducts.sort(function (a, b) {
														return b.isSelected - a.isSelected;
													})
												);
										  }
										: () => {
												setSerieses(!serieses);
										  }
								}
								className={classnames(
									'd-flex justify-content-center border-turquoise align-items-center font-small-3 cursor-pointer py-05 border-2 px-1 elipsis width-5-rem   text-bold-700  mr-05 border-radius-2rem',
									{
										'bg-turquoise': serieses,
										'text-white': serieses,
										'text-turquoise': !serieses
									}
								)}
							>
								{<FormattedMessage id={'Serieses'} />}
							</div>
						)}

						<div
							className={classnames('d-flex align-items-center', {
								'ml-auto': sideBarExpanedLevel === 1
							})}
						>
							{sideBarExpanedLevel != 3 && (
								<div
									className={classnames('d-flex align-items-center', {
										'ml-auto': sideBarExpanedLevel === 1
									})}
								>
									{optionIcon({
										icon: 'filter',
										iconSelected: 'filter_white',
										isSelected: option === 'filter',
										onClick: () => {
											setOption('filter');
										}
									})}
									{optionIcon({
										icon: 'search',
										iconSelected: 'search_white',
										isSelected: option === 'search',
										onClick: () => {
											setOption('search');
										}
									})}
									{optionIcon({
										icon: 'fullscreen_exit',
										iconSelected: 'fullscreen_exit_white',
										isSelected: option === 'merging',
										onClick: () => {
											setOption('merging');
										}
									})}
								</div>
							)}
						</div>
						{sideBarExpanedLevel === 2 ? (
							<div className="position-relative  ml-auto">
								<ColumnDisplayer
									setColumns={setColumns}
									columns={columns}
									isColumnDisplayerPopupOpen={isColumnDisplayerPopupOpen}
									setIsColumnsDisplayerPopupOpen={setIsColumnsDisplayerPopupOpen}
								/>
							</div>
						) : null}
					</div>

					{sideBarExpanedLevel != 3 && <div>{getOption(option)}</div>}
					<div
						id="productsContainer"
						onScroll={handleScroll}
						className="d-rtl bg-gray height-80-vh mt-auto overflow-auto px-05"
					>
						<div className="d-ltr d-flex-column align-items-center"></div>
						{sideBarExpanedLevel === 2 && !serieses ? (
							<div className="d-ltr">
								{columnsDisplayed && columnsDisplayed.length ? (
									<div className="text-black mb-05 mt-1 bg-white ml-auto width-50-per p-05 d-flex justify-content-evenly align-items-center">
										{columnsDisplayed.map((column: any, index: number) => {
											return (
												<>
													<div className="text-bold-700">
														<FormattedMessage id={column.name} />
													</div>
													{index != columnsDisplayed.length - 1 && (
														<div className="bg-gray width-2 height-2-rem" />
													)}
												</>
											);
										})}
									</div>
								) : null}
							</div>
						) : null}

						{!mergedGroups.length && serieses ? (
							products && products.length ? (
								seriesDisplay(updateProductsByFilter(allProducts))
							) : null
						) : (
							<div>
								{!mergedGroups.length
									? (products && products.length
											? updateProductsByFilter(
													products.filter((product: any) => {
														if (!filter) return true;
														else
															return (
																product[productNameKey].includes(filter) ||
																String(product.BarCode).includes(filter)
															);
													})
											  )
											: []
									  ).map((product: any, pi: number) =>
											pi < 300 ? mainProducts(product, pi, limitedProductsRows) : null
									  )
									: accordionProducts(
											productsPerRow && products && productsPerRow.length
												? groupBy(
														updateProductsByFilter(
															productsPerRow.filter((product: any) => {
																if (!filter) return true;
																else
																	return (
																		product[productNameKey].includes(filter) ||
																		String(product.BarCode).includes(filter)
																	);
															})
														),
														mergedGroups,
														0,
														[],
														formatMessage
												  )
												: [],
											0
									  )}
							</div>
						)}
					</div>
				</>
			</div>
		</>
	);
};

export default Sidebar;
