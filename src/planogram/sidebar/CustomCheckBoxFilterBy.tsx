import * as React from 'react';
import { CustomInput } from 'reactstrap';
import { groupBy } from '@src/helpers/helpers';
import { FormattedMessage, useIntl } from 'react-intl';

interface Props {
	data: any;
	setSelectedData: any;
	selectedData: any;

}

export const CustomCheckBoxFilterBy: React.FC<Props> = (props: Props) => {
	const {
		data,
		selectedData,
		setSelectedData,

	} = props;

	const changeSelected = () => {
		if (selectedData.includes(data.value)) {
			let newSelectedData = selectedData.filter((item: any) => data.value !== item);
			setSelectedData(newSelectedData);
		} else {
			let newSelectedData = [...selectedData, data.value];
			setSelectedData(newSelectedData);
		}
	};


	return (
		<label className="combobox__drop-down_label ml-05" htmlFor={data.value}>
			<CustomInput
				id={data.value}
				onChange={changeSelected}
				type="checkbox"
				value={data.value}
				checked={selectedData.includes(data.value) ? true : false}
			/>
			{data.value}
		</label>
	);
};

export default CustomCheckBoxFilterBy;
