import * as React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { BlankPopUp, Icon } from '@components';
import { FormattedMessage, useIntl } from "react-intl"
import { config } from '@src/config'
import classnames from 'classnames'
import { CustomInput } from "reactstrap"
import { Button } from "reactstrap"

interface Props {
    setIsColumnsDisplayerPopupOpen: any;
    columns: any;
    setColumns: any;
    isColumnDisplayerPopupOpen: any
}

export const ColumnDisplayer: React.FC<Props> = (props: Props) => {
    const dispatch = useDispatch();
    const { formatMessage } = useIntl();
    const [message, setMessage] = React.useState<boolean>(false);

    const { setIsColumnsDisplayerPopupOpen, columns, setColumns, isColumnDisplayerPopupOpen } = props

    const [columnsBeforeSave, setColumnsBeforeSave] = React.useState<any>(columns)
    const [selectedArr, setSelectedArr] = React.useState<any>(columns.map((column: any) => {
        if ( !column.selected ) return;
        else return column.name
    }).filter(a=>a))

    const handleCheckBoxChange = (column: any) => {
       
        let newColumns = columnsBeforeSave.map((item: any) => {
            let selected = item.selected
            if (column.name === item.name) {
                selected = !item.selected
            }
            return { ...item, selected }
        })
        let newSelectedArr:any[] = newColumns.map((item: any) => {
            if (item.selected) return item.name
            else return null
        }).filter((a:any)=>a)
        if (newSelectedArr && newSelectedArr.length <= 4) {
            setSelectedArr(newSelectedArr)
            setColumnsBeforeSave(newColumns)
        } else{
            setMessage(true)
            setTimeout(()=>setMessage(false), 2000)
        }
        
      
    
    }
    const handleSave = () => {
        if (columnsBeforeSave && columnsBeforeSave.length) {
            setColumns(columnsBeforeSave)
            setIsColumnsDisplayerPopupOpen(false)
        }
    }

    return (
        <div>
            <div className="position-relative  ml-auto" onClick={() => setIsColumnsDisplayerPopupOpen(!isColumnDisplayerPopupOpen)}>
                <div
                    className={classnames("border-radius-3 cursor-pointer ml-05 p-05 bg-light-gray", {
                        // "bg-turquoise": isColumnDisplayerPopupOpen,
                        // "bg-light-gray": !isColumnDisplayerPopupOpen
                    })}>
                    <Icon src={'../../' + config.iconsPath + "table/column-view.svg"} style={{ height: '0.5rem', width: '0.5rem' }} />
                </div>
            </div>
            {isColumnDisplayerPopupOpen ?
                <BlankPopUp noXbutton onOutsideClick={setIsColumnsDisplayerPopupOpen} className='left-7-rem' style={{ top: '0.5rem',width: '11.8rem', height: '25rem' }} >
                    <div className='d-flex flex-column'>
                        <div className='d-flex flex-column justify-content-center mt-1-2 mb-2 ml-1'>
                            <span className='text-black font-medium-3 font-bold text-bold-700 align-self-start'>{formatMessage({ id: 'tableColumns' })}</span>
                        </div>

                        <div className='d-flex flex-column'>
                            {columns.map((column: any) => {
                                return (
                                    <label className='combobox__drop-down_label ml-1 mb-08' htmlFor={column.name}>
                                        <CustomInput id={column.name} onChange={() => handleCheckBoxChange(column)} type='checkbox' value={column.name}
                                            checked={selectedArr.includes(column.name)}
                                        />
                                        <span className='text-bold-600 ml-03'> {formatMessage({ id: column.name })}</span>
                                    </label>
                                )
                            })}
                        </div>

                        {message && (<span className="ml-1 font-small-2 text-danger text-bold-600"><FormattedMessage id="You can display up to 4 columns" /></span>)}

                        <Button
                            onClick={handleSave}
                            color="primary"
                            className="position-absolute position-bottom-1-5-rem ml-1 height-2-2-rem width-6-rem min-width-5-rem round text-bold-400 d-flex justify-content-center align-items-center cursor-pointer btn-primary"
                        >
                            <FormattedMessage id="save" />
                        </Button>
                    </div>
                </BlankPopUp > : null}
        </div>
    );
};

export default ColumnDisplayer;
