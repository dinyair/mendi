import * as React from 'react';
import classnames from 'classnames'
import { useSelector, useDispatch } from 'react-redux';
import { FormattedMessage, useIntl } from "react-intl"
import { Button } from "reactstrap"
import { useHistory } from "react-router-dom";
import { PLANOGRAM_BASE_URL } from '@src/PlanoApp';
import { Icon } from '@components';
import { config } from '@src/config'
import { Badge } from "reactstrap"
import { groupBy, aggFunction } from '@src/helpers/helpers'
import { GroupItem } from './setting-supplier-areas/GroupItem'
import { ChevronLeft } from 'react-feather';
import { ShelfMapping } from './setting-supplier-areas/ShelfMapping'
import { postShelfSpace, getShelfSpace, updateFaces } from './setting-supplier-areas/initial-state'
import { Spinner } from 'reactstrap'
import { NEWPLANO_ACTION } from "@src/planogram/shared/store/newPlano/newPlano.types";
import ExportExcel from '@src/components/ExportExcel/ExportExcel';

interface Props {
	rows: any[],
	// aggFunction: (bool:boolean) => void
	toogle: (bool: boolean) => void
}

export const SettingSuppliersAreas: React.FC<Props> = (props: Props): any => {
	let selectedRows = props.rows.map((row: any) => { if (row.isSelected) { return row } }).filter((r: any) => r)
	const customizer = { direction: document.getElementsByTagName("html")[0].dir }
	const dispatch = useDispatch();
	const { formatMessage } = useIntl();
	const { toogle } = props

	const history = useHistory();
	const url = window.location.href;
	const urlElements = url.split('/');
	let currentBranchId = parseInt(urlElements[5]);
	let selectedOption = urlElements[6]


	let groups = [{ field: 'Supplier', id: 'SapakId' }, { field: 'Segment', id: 'SegmentId' },
	{ field: 'Brand', id: '' }, { field: 'Series', id: 'DegemId' }, { field: 'SubgroupName', id: 'SubGroupId' }]
	const [rows, setRows] = React.useState<any>(null);
	let layoutStoreId = parseInt(urlElements[6]);
	let currentAisleId = parseInt(urlElements[7]);
	const [loading, setLoading] = React.useState(false)
	const [isSaveButtonDisabled, setIsSaveButtonDisabled] = React.useState<any>(false)
	const [mergedGroups, setMergedGroups] = React.useState<any>([]);
	const [groupsCollapse, setGroupsCollapse] = React.useState<any>([]);
	const [totalAvg, setTotalAvg] = React.useState<any>(100)
	const [shelfRow, setShelfRows] = React.useState<any>([{ index: 0, fromData: 1, untilData: 1, units: 5, mm: 1000 }])
	const [shelfSpaceSaved, setShelfSpaceSaved] = React.useState<boolean>(false)
	const [rowsToExport, setRowsToExport] = React.useState<any>(null)
	const [fieldsToExport, setFieldsToExport] = React.useState<any>(null)
	React.useEffect(() => {
		getShelfSpaceSetting()
	}, [])


	React.useEffect(() => {
		/* Export excel effect */ 
		setRowsToExport(null)
		setFieldsToExport(null)
		const excelRows: any = [];
		const excelFields: any = [];

		const searchRows = (object: any) => {
			for (let i = 0; i < object.rows.length; i++) {
				let row = object.rows[i];

				if (!row.BarCode) {
					pushRow(row)
					searchRows(row)
				}
			}
		}
		const pushRow = (object: any) => {
			const { editedPercentAvg, field, percentAmount, percentAvg, percentMarketPrice, percentPrice, value } = object
			const row = {
				[field]: value,
				Percent_TotalPrice: percentPrice.toFixed(2),
				Percent_TotalAmount: percentAmount.toFixed(2),
				Percent_Avg: percentAvg.toFixed(2),
				Percent_MarketAmount: percentMarketPrice.toFixed(2),
				P_Reccomendation: editedPercentAvg ? Math.round(editedPercentAvg) : Math.round(percentAvg)
			}
			excelRows.push(row)
		}

		if (rows && rows.length) {
			for (let a = 0; a < rows.length; a++) {
				pushRow(rows[a])
				searchRows(rows[a])
			}

			for (let index = 0; index < mergedGroups.length; index++) {
				excelFields.push({ text: mergedGroups[index].field })
			}
		}

		excelFields.push({ text: "Percent_TotalPrice" },
			{ text: "Percent_TotalAmount" },
			{ text: "Percent_Avg" },
			{ text: "Percent_MarketAmount" },
			{ text: "P_Reccomendation" })

		setFieldsToExport(excelFields)
		setRowsToExport(excelRows)
	}, [mergedGroups, rows])


	const getShelfSpaceSetting = async () => {
		setLoading(true)
		let shelfSpace = await getShelfSpace(currentAisleId)
		if (shelfSpace.json1) {
			setShelfRows(shelfSpace.json1.shelfMapping)
			if (shelfSpace.json1.grouppedRowsString && shelfSpace.json1.grouppedRowsString.length) {
				setMergedGroups(shelfSpace.json1.mergedGroupsOrder);
				convertStringToGroupes(shelfSpace.json1.grouppedRowsString, shelfSpace.json1.mergedGroupsOrder)
			} else {
				setMergedGroups([{ id: "SapakId", field: "Supplier" }])
				mergeByGroups([], { id: "SapakId", field: "Supplier" })
				setLoading(false)
			}

		} else {
			setMergedGroups([{ id: "SapakId", field: "Supplier" }])
			mergeByGroups([], { id: "SapakId", field: "Supplier" })
			setLoading(false)
		}

	}

	const mergeByGroups = (mergedGroups: any[], field?: any, reverse?: boolean) => {
		if (field) {
			let newMergedGroups = [...mergedGroups, field]
			if (reverse) {
				newMergedGroups = mergedGroups.filter((f: any) => f.field !== field.field)
				if (newMergedGroups.length === 0) { setGroupsCollapse([]) }
				setMergedGroups(newMergedGroups)
			} else setMergedGroups([...mergedGroups, field])
			let selectedRows = props.rows.map((row: any) => { if (row.isSelected) { return row } }).filter((r: any) => r)
			let groups: any = newMergedGroups && newMergedGroups.length ? groupBy(selectedRows, newMergedGroups, 0, [], formatMessage) : []
			setRows(groups)
		}
	};
	const Groups = (
		<div className="d-flex py-1">
			{groups && groups.length && (
				groups.map((group: any, index: number) => {
					let pos = mergedGroups.findIndex((field: any) => field.field == group.field)
					return (< div
						key={index}
						onClick={pos === -1 ? (e) => { mergeByGroups(mergedGroups, group) } : (e) => { mergeByGroups(mergedGroups, group, true) }}
						className={classnames("d-flex align-items-center bg-gray position-relative font-small-3 cursor-pointer py-05 border-2 px-1 text-black text-bold-700  mr-05 border-radius-2rem", {
							"border-turquoise": pos >= 0,
						})}>
						{<FormattedMessage id={group.field} />}
						<Badge pill color='primary' className='badge-up bg-turquoise border-white border-1 text-bold-600 text-white width-1-rem position-left-0 position-top-02'>
							{mergedGroups.findIndex((mg: any) => mg.field === group.field) > -1 ? mergedGroups.findIndex((mg: any) => mg.field === group.field) + 1 : null}
						</Badge>
					</div>
					)

				}))
			}

		</div >
	)
	React.useEffect(() => {
		if (rows && rows.length) {
			updateSum()
		}
	}, [rows])

	const updateSum = () => {
		let sum = 0
		rows.forEach((row: any) => {
			sum += Number(row.editedPercentAvg) || row.editedPercentAvg === 0 ? Number(row.editedPercentAvg) : row.percentAvg
		})
		setTotalAvg(sum.toFixed(2))
	}

	const Rows = (childRows: any[]) => {
		return (
			<div className={classnames("align-items-center d-flex-column", {
			})}>
				{
					childRows && childRows.length ? (
						childRows.map((row: any) => {
							let childNum: number = -1
							if (row.field) childNum = mergedGroups.findIndex((mg: any) => mg.field === row.field)

							return (
								<GroupItem
									dontAllowClick={row.rows && row.rows[0] && row.rows[0].BarCode ? true : false}
									hideArrow={row.rows && row.rows[0] && !row.rows[0].BarCode ? false : true}
									className='width-100-per m-0'
									isArrowRed={totalAvg > 100 && childNum === 0 ? true : false}
									childNum={childNum}
									row={row.rows}
									rows={childRows}
									mainGroupingRows={rows}
									mainRow={row}
									setRows={setRows}
									totalAvg={totalAvg}
									mergedGroups={mergedGroups}
								>
									{row.rows && row.rows[0] && !row.rows[0].BarCode && mergedGroups[1]
										?
										Rows(row.rows)
										:
										null
									}
								</GroupItem >
							)
						}
						)) : null

				}
			</div>

		)
	}

	const createStringFromGroups = () => {
		let finalString = ''
		if (mergedGroups && mergedGroups.length) {
			let mappedRows = rows.map((row: any) => {
				const rowLevel1: boolean = row.rows && row.rows[0] && !row.rows[0].BarCode ? true : false
				const rowLevel2: boolean = row.rows && row.rows[0] && row.rows[0].rows && row.rows[0].rows[0] && row.rows[0].rows.length && !row.rows[0].rows[0].BarCode ? true : false
				const rowLevel3: boolean = row.rows && row.rows[0] && row.rows[0].rows && row.rows[0].rows[0].rows && row.rows[0].rows[0].rows[0] && !row.rows[0].rows[0].rows[0].BarCode ? true : false
				const rowLevel4: boolean = row.rows && row.rows[0] && row.rows[0].rows && row.rows[0].rows[0].rows && row.rows[0].rows[0].rows[0].rows && !row.rows[0].rows[0].rows[0].rows[0].BarCode ? true : false
				let didAddSecondTitle: boolean = false
				let didAddThreeTitle: boolean = false
				let didAddFourthTitle: boolean = false
				let stringForServer: string = ''
				let string2: string = ''
				let string3: string = ''
				let string4: string = ''

				stringForServer += `#${row.field}#${row.id}-:${row.didEdit ? row.editedPercentAvg.toFixed(0) : null},`

				if (rowLevel1) {
					stringForServer += `#${row.rows[0].field}#,`
					row.rows.forEach((one: any, oneIndex: number) => {
						stringForServer += `${row.id}-${one.id}-:${one.didEdit ? one.editedPercentAvg.toFixed(0) : null},`

						if (rowLevel2) {
							row.rows[oneIndex].rows.forEach((two: any, twoIndex: number) => {
								if (!didAddSecondTitle) string2 += `#${two.field}#,`
								didAddSecondTitle = true
								string2 += `${row.id}-${one.id}-${two.id}-:${two.didEdit ? two.editedPercentAvg.toFixed(0) : null},`

								if (rowLevel3) {
									row.rows[oneIndex].rows[twoIndex].rows.forEach((three: any, threeIndex: number) => {
										if (!didAddThreeTitle) string3 += `#${three.field}#,`
										didAddThreeTitle = true

										string3 += `${row.id}-${one.id}-${two.id}-${three.id}-:${three.didEdit ? three.editedPercentAvg.toFixed(0) : null},`
										if (rowLevel4) {
											row.rows[oneIndex].rows[twoIndex].rows[threeIndex].rows.forEach((four: any) => {
												if (!didAddFourthTitle) string4 += `#${four.field}#,`
												didAddFourthTitle = true
												string4 += `${row.id}-${one.id}-${two.id}-${three.id}-${four.id}-:${four.didEdit ? four.editedPercentAvg.toFixed(0) : null},`
											})

										}

									})
								}
							})
						}
					})
					stringForServer += string2
					stringForServer += string3
					stringForServer += string4
				}
				return stringForServer
			})

			mappedRows.forEach((row: any) => {
				finalString += `line:${row}`
			})
		}
		return finalString
	}
	const saveShelfSpace = async () => {
		setIsSaveButtonDisabled(true)
		let finalString = createStringFromGroups()
		await updateShelfSpace(finalString)

		dispatch({
			type: NEWPLANO_ACTION.SET_ALERT,
			payload: {
				timeMs: 1000,
				type: 'success',
				text: 'saved_successfully'
			}
		});
		setIsSaveButtonDisabled(false)
	}
	const updateShelfSpace = async (grouppedString: any) => {
		let shelfSpace = await postShelfSpace({
			mergedGroupsOrder: mergedGroups,
			grouppedRowsString: grouppedString,
			shelfMapping: shelfRow,
			aisleId: currentAisleId
		})
		return shelfSpace
	}

	const getListOfKeyVal = (arr: any, nameKey: any, idKey: any) => {
		const reducedArr = arr && arr[0] ? arr.reduce((acc: any, curr: any) => {
			return [...acc, { nameKey: curr[nameKey], idKey: curr[idKey] }]
			// return [...acc, { nameKey: curr[nameKey], idKey: curr[idKey] }]
		}, []) : []
		const uniqueReducedArr = Array.from(new Set(reducedArr.map((a: any) => a.nameKey))).map((field2: any) => {
			return reducedArr.find((a: any) => a.nameKey === field2)
		})
		let sortedGood: any = {}
		uniqueReducedArr.forEach((item: any) => {
			sortedGood[item.idKey] = item.nameKey
		})
		return sortedGood
	}
	const getPositionInString = (i: number) => {
		switch (i) {
			case 0: return [1, 2];
			case 1: return [3, 4]
			case 2: return [5, 6]
			case 3: return [7, 8]
			case 4: return [9, 10]
			default: return [0, 0]
		}
	}
	const getNameFromId = (groupName: any, id: number) => {
		let suppliersFromRows = getListOfKeyVal(selectedRows, 'Supplier', 'SapakId')
		let segemntsFromRows = getListOfKeyVal(selectedRows, 'Segment', 'SegmentId')
		let brandsFromRows = getListOfKeyVal(selectedRows, 'Brand', 'ClassesId')
		let seriesFromRows = getListOfKeyVal(selectedRows, 'Series', 'DegemId')
		let subGroupsFromRows = getListOfKeyVal(selectedRows, 'SubgroupName', 'SubGroupId')
		// console.log('1',suppliersFromRows)
		if (id === 0) return formatMessage({ id: 'other' })
		switch (groupName.field) {
			case 'Supplier': return suppliersFromRows[id]
			case 'Segment': return segemntsFromRows[id]
			case 'Brand': return brandsFromRows[id]
			case 'Series': return seriesFromRows[id]
			case 'SubgroupName': return subGroupsFromRows[id]
			default: 'err'
		}
	}
	const getThisChildRows = (idArr: any) => {
		let childRows = selectedRows.filter((row: any) => {
			let shouldAddSupplier = true
			let shouldAddSegment = true
			let shouldAddSeries = true
			let shouldAddSubgroupName = true

			idArr.forEach((id: any) => {
				switch (id.merge) {
					case 'Supplier': if (row.SapakId != Number(id.id)) shouldAddSupplier = false; else shouldAddSupplier = true; break;
					case 'Segment': if (row.SegmentId != Number(id.id)) shouldAddSegment = false; else shouldAddSegment = true; break;
					case 'Series': if (row.DegemId != Number(id.id)) shouldAddSeries = false; else shouldAddSeries = true; break;
					case 'SubgroupName': if (row.SubGroupId != Number(id.id)) shouldAddSubgroupName = false; else shouldAddSubgroupName = true; break;
				}
			})
			return shouldAddSegment && shouldAddSupplier && shouldAddSeries && shouldAddSubgroupName
		})
		return childRows
	}
	const removeByIndex = (array: any, index: number) => array.filter((_: any, i: number) => i !== index);


	const convertStringToGroupes = (stringRows: any, mergedGroupsD: any) => {
		let stringArray = stringRows.split('line:')
		let stringArrayFilter = stringArray.filter((a: any) => a.length > 0)
		let allGroups: any = []
		stringArrayFilter.forEach((row: any, fatherIndex: number) => {
			let rowDataString: any = []
			mergedGroupsD.forEach((group: any, index: number) => {
				let position = getPositionInString(index)
				let groupName = row.split('#')[position[0]]
				let groupValues = row.split('#')[position[1]]
				let idAndValues = groupValues.split(',').filter((i: any) => i.length > 0)

				idAndValues.forEach((idvalue: any, indexIds: number) => {
					let subRowData: any = {}
					let editedPercentAvg = idvalue.split(':')[idvalue.split(':').length - 1]
					let ids = idvalue.split('-')
					ids.splice(ids.length - 1, 1)
					let idsWithKey = ids.map((id: any, indexId: number) => {
						return { merge: mergedGroupsD[indexId].field, id: id }
					})
					let fatherIds = removeByIndex(idsWithKey, idsWithKey.length - 1)
					subRowData.index = index === 0 ? fatherIndex : null
					subRowData.value = getNameFromId(mergedGroupsD[index], Number(ids[ids.length - 1]))
					subRowData.idsPath = idsWithKey
					subRowData.field = groupName
					subRowData.editedPercentAvg = Number(editedPercentAvg)
					subRowData.rows = getThisChildRows(idsWithKey)
					subRowData.fatherRows = getThisChildRows(fatherIds)
					subRowData.fathersArray = []
					subRowData.avgTotal = subRowData.rows.reduce((acc: number, row: any) => acc + Number(row['Avg']), 0)
					subRowData.totalRowsPrice = subRowData.rows.reduce((acc: number, row: any) => acc + row['TotalPrice'], 0)
					subRowData.totalRowsAmount = subRowData.rows.reduce((acc: number, row: any) => acc + row['TotalAmount'], 0)
					subRowData.totalMarketAmount = subRowData.rows.reduce((acc: number, row: any) => acc + row['MarketPrice'], 0)
					subRowData.percentPrice = aggFunction(subRowData.fatherRows, subRowData.totalRowsPrice, 'TotalPrice')
					subRowData.percentAmount = aggFunction(subRowData.fatherRows, subRowData.totalRowsAmount, 'TotalAmount')
					subRowData.percentAvg = aggFunction(subRowData.fatherRows, subRowData.avgTotal, 'Avg')
					subRowData.percentMarketPrice = aggFunction(subRowData.fatherRows, subRowData.totalMarketAmount, 'MarketPrice')
					subRowData.didEdit = Math.round(Number(editedPercentAvg)) != Math.round(Number(subRowData.percentAvg)) ? true : false

					subRowData.deepLevel = index
					subRowData.id = Number(ids[ids.length - 1])
					rowDataString.push(subRowData)
				})
			})

			let createGrouping: any = []
			rowDataString.forEach((rowD: any) => {
				rowDataString.forEach((innerRow: any) => {
					if (rowD.idsPath[rowD.idsPath.length - 2] && innerRow.idsPath[innerRow.idsPath.length - 1] &&
						rowD.idsPath[rowD.idsPath.length - 2].id === innerRow.idsPath[innerRow.idsPath.length - 1].id &&
						rowD.deepLevel - 1 === innerRow.deepLevel) {
						let firstLevel = innerRow
						let secondLevel = rowD
						rowD.index = firstLevel.rows && firstLevel.rows[0] && !firstLevel.rows[0].BarCode ? firstLevel.rows.length : 0
						secondLevel.fathersArray = secondLevel.fathersArray.length ? [...secondLevel.fathersArray, firstLevel.index] : [...firstLevel.fathersArray, firstLevel.index]
						const doesExist = innerRow.rows.findIndex((x: any) => x.value == secondLevel.value);
						doesExist ? firstLevel.rows && firstLevel.rows[0] && firstLevel.rows[0].BarCode ? firstLevel.rows = [secondLevel] : firstLevel.rows = [...firstLevel.rows, secondLevel] : null
						firstLevel.deepLevel === 0 && createGrouping.findIndex((x: any) => x.value == firstLevel.value) === -1 && createGrouping.push(firstLevel)
					}
				})
			})
			if (rowDataString.length != 1) {
				allGroups.push(...createGrouping)
			} else {
				allGroups.push(...rowDataString)
			}

		})
		setRows(allGroups)
		setLoading(false)
	}

	return (<>
		{!loading ?
			<div>
				<div className="d-flex mb-2 width-100-per justify-content-between">
					<div className="d-flex">
						<div onClick={() => toogle(false)}
							className="cursor-pointer">
							<Icon src={'../' + config.iconsPath + "table/arrow-right.svg"}
								className={classnames("mr-1", {
									"rotate-180": customizer.direction == 'ltr',
								})} style={{ height: '0.5rem', width: '0.2rem' }} />
						</div>
						<div className="d-flex-column align-items-center justify-content-center">
							<div className="text-bold-700 font-medium-2 text-black"><FormattedMessage id="setting shelf areas" /></div>
							<div className="text-bold-700 font-medium-1 text-turquoise"><FormattedMessage id="This is our recomandation, But it's editable" /></div>

						</div>
					</div>
					<div className='d-flex'>
						{isSaveButtonDisabled &&
							<div className='mr-1 d-flex justify-content-center align-items-center'><Spinner /></div>
						}
						<Button
							onClick={() => { setShelfSpaceSaved(true); saveShelfSpace() }}
							color="primary"
							disabled={isSaveButtonDisabled}
							className="mr-05 round text-bold-400 d-flex justify-content-center align-items-center width-auto cursor-pointer btn-primary"
						>
							<FormattedMessage id="saveShelfSpace" />
						</Button>
						<Button
							onClick={async () => {
								// if(shelfSpaceSaved){
								setIsSaveButtonDisabled(true)
								let finalString = createStringFromGroups()
								await updateShelfSpace(finalString)
								let updatedFacesImport = await updateFaces(currentAisleId)
								if (updatedFacesImport && updatedFacesImport.newPlano) {
									dispatch({
										type: NEWPLANO_ACTION.SET_NEWPLANO,
										payload: updatedFacesImport.newPlano
									});
								}
								setIsSaveButtonDisabled(false)

								// }
								history.push(PLANOGRAM_BASE_URL + '/layout/editor/' + currentBranchId + '/' + layoutStoreId + '/' + currentAisleId)
							}}
							disabled={isSaveButtonDisabled}
							// onClick={() => }
							color="primary"
							className="round text-bold-400 d-flex justify-content-center align-items-center width-auto cursor-pointer btn-primary"
						>
							<FormattedMessage id="import to planogram" />
						</Button>

					</div>
				</div>



				<div className='breakOn1300 max-height-65-vh d-flex mt-05 overflow-y-auto overflow-x-hidden min-width-50-rem justify-content-between'>
					<div className={"min-width-50-rem maxWidthOnBreak mb-2"}>

						<div className="d-flex justify-content-between align-items-center">
							{Groups}

							{rowsToExport ? <ExportExcel fields={fieldsToExport} data={rowsToExport} direction={'ltr'} translateHeader={true} filename={formatMessage({ id: 'setting shelf areas' })}
								icon={<Icon className="cursor-pointer" src={window.location.origin + "/assets/icons/table/excel-download.svg"} />} /> : null}
						</div>

						<div className="d-flex font-medium-1 text-bold-700">
							<div className="text-black"><FormattedMessage id="Area allocation stands at" /></div>
							<div className={classnames("ml-05 text-turquoise-non", {
								'text-warning': totalAvg > 100
							})}>{totalAvg}%</div>
						</div>

						<div className={classnames("min-width-50-rem maxWidthOnBreak mb-2", {
							'border-light-table': mergedGroups && mergedGroups.length
						})}>
							{mergedGroups && mergedGroups.length ? (
								<div className='groupItem width-100-per no-border m-0'>
									<button className='groupItem__button d-flex align-items-center'>
										<span className={classnames("groupItem__button_icon visibility-hidden", {
											"rotate-180": customizer.direction == 'ltr',
										})} ><ChevronLeft size={16} /></span>
										<div className='d-flex justify-content-evenly min-width-37-rem'>
											<div className='align-self-center width-15-rem' />
											<div className='d-flex justify-content-evenly position-relative width-35vw'>

												{[formatMessage({ id: 'Percent_TotalPrice' }),
													'divider',
												formatMessage({ id: 'Percent_TotalAmount' }),
													'divider',
												formatMessage({ id: 'Percent_Avg' }),
													'divider',
												formatMessage({ id: 'Percent_MarketAmount' }),
													'divider',
												formatMessage({ id: 'Recommendation' })].map((item: any, index: number) => {
													if (item === 'divider') {
														return (<span className='border-light-table'></span>)

													}
													return (
														<div className="d-flex" key={index}>
															<div
																className={classnames("width-5-rem font-small-3 text-black d-flex justify-content-center text-bold-700", {
																})}>
																{item}
															</div>
														</div>
													)
												})}


											</div>

										</div>
									</button>


								</div>) : null}

							{Rows(rows)}

						</div>
					</div>

					<div className='mr-3 d-flex flex-column max-width-30-rem mappingFields'>
						<span className='mr-4 align-self-center mb-1 font-medium-3 text-bold-700'>{formatMessage({ id: "MappingFieldsAndShelfs" })}</span>
						<ShelfMapping shelfRow={shelfRow} setShelfRows={setShelfRows} />
					</div>
				</div>
			</div>
			: <Spinner />
		}
	</>);
};


export default SettingSuppliersAreas;


