
import * as React from 'react';
import { RootStore } from '@src/store';
import { Icon } from '@components';
import { config } from '@src/config'
import { useIntl, FormattedMessage } from "react-intl"
import { useSelector, useDispatch } from 'react-redux';
import { components } from "react-select";
import { getNewPlano } from '@containers/user/initial-state';
import { fetchAndUpdatePlanogram } from '@src/helpers/planogram';
import Select from "react-select"
import makeAnimated from "react-select/animated";
import * as moment from 'moment';
import { CalendarComponent } from '@components';
import { AppState } from '@src/planogram/shared/store/app.reducer';
import { CalendarDoubleInput } from '@components';
import classnames from 'classnames';

import {
  Button,
  Modal,
  ModalHeader,
  Spinner,
  ModalBody,
  ModalFooter,
} from "reactstrap"



interface Props {
  readonly isOpen: boolean;
  readonly layoutId: number;
  readonly toogle: (b: boolean) => void;
  readonly date: any;
  readonly setDate: (date: any) => void;
  readonly dates: any;
  readonly setDates: (date: any) => void;
  readonly branchSelects: any[];
  readonly setBranchSelects: (branchSelects: any[]) => void;
  readonly groupsSelects: any[];
  readonly setGroupsSelects: (groupsSelects: any[]) => void;
  readonly subgroupsSelects: any[];
  readonly setSubgroupsSelects: (subgroupsSelects: any[]) => void;
  readonly rowsDataDates: any[];
  readonly setRowsDataDates: (rowsDataDates: any[]) => void;
  readonly setNewPlano: (newPlano: any[]) => void;
  readonly setDate1: any
  readonly setDate2: any
  readonly date1: any
  readonly date2: any
}





export const NewPlanoPopup: React.FunctionComponent<Readonly<Props>> = (props: Readonly<Props>) => {
	const dispatch = useDispatch()
  const customizer = { direction: document.getElementsByTagName("html")[0].dir }
  const { formatMessage } = useIntl();
  const { setNewPlano, isOpen, toogle, date, setDate, layoutId,
    dates, setDates, branchSelects, setBranchSelects,
    groupsSelects, setGroupsSelects, subgroupsSelects,
    setSubgroupsSelects, rowsDataDates, setRowsDataDates,
    setDate1, setDate2, date1, date2
  } = props

  const [niceDate, setNiceDate] = React.useState<any>()
  const [isDatePopupOpen, setIsDatePopupOpen] = React.useState<boolean>(false);
  const [inputValue, setInputValue] = React.useState<any[]>(Array.from({ length: 10 }));
  const [loading, setLoading] = React.useState<boolean>(false);
  const url = window.location.href;
  const urlElements = url.split('/');
  let aisle_id = parseInt(urlElements[7]);
  const branches = useSelector((state: AppState) => state.system.data.branches)
  const catalog = useSelector((state: RootStore) => state._catalog)
  const groups = useSelector((state: RootStore) => state._groups)
  const subgroups = useSelector((state: RootStore) => state._subGroups)


  let subGroupsIds = catalog && catalog.length ? catalog.map((catalog: any) => groupsSelects.map((group: any) => group.value).includes(catalog.GroupId) ? catalog.SubGroupId : undefined).filter(a => a) : []
  let DatesPickerRef: any = document.getElementById('flatPicker')
  const [displayDatePicker, setDisplayDatePicker] = React.useState<boolean>(false)
  const [customDate, setCustomDate] = React.useState<any>('costum_made')

  React.useEffect(() => {
    if (date && date.value === 5) {
      setDisplayDatePicker(true)
    }
  }, [date])

  const Option = (props: any) => {
    return (
      <div>
        <components.Option {...props}>
          <input
            type="checkbox"
            checked={props.isSelected}
            onChange={() => null}
          />{" "}
          <label>{props.label}</label>
        </components.Option>
      </div>
    );
  };


  const DropdownIndicator = (props: any) => {
    return (
      <components.DropdownIndicator {...props}>
        <Icon src={'../' + config.iconsPath + "table/date.svg"} />
      </components.DropdownIndicator>
    );
  };


  const MultiValue = (props: any) => (
    <components.MultiValue {...props}>
      <span>{props.data.label}</span>
    </components.MultiValue>
  );
  const animatedComponents = makeAnimated();

  const selectAllOption = {
    value: "<SELECT_ALL>",
    label: formatMessage({ id: "select_all" })
  };

  React.useEffect(() => {
    displayNiceDate()
  }, [date1])
  React.useEffect(() => {
    displayNiceDate()
  }, [date2])

  const displayNiceDate = () => {
    setCustomDate(date1 && date2 ? `${moment(date1).format('DD.MM.YY')} - ${moment(date2).format('DD.MM.YY')}` : date1 ? `${moment(date1).format('DD.MM.YY')} - 
      ${moment(new Date()).format('DD.MM.YY')}` : date2 ? `${moment(new Date(date2)).subtract(1, 'year').format('DD.MM.YY')} - ${moment(date2).format('DD.MM.YY')}` : 'costum_made')
  }
  return (
    <Modal
      isOpen={isOpen}
      toggle={() => toogle(false)}
      backdrop={'static'}
      className="modal-dialog-centered modal-md"
    >
      {/*
                    <ModalHeader toggle={()=>toogle(false)}>
                    </ModalHeader> 
                  */}
      <ModalBody>
        <div className="my-1 d-flex">
          <div className="cursor-pointer" onClick={() => toogle(false)}>
            <Icon src={'../' + config.iconsPath + "table/arrow-right.svg"}
              				className={classnames("mr-05", {
                        "rotate-180": customizer.direction == 'ltr',
                      })} style={{ height: '0.5rem', width: '0.2rem' }} />
          </div>
          <div className="font-medium-3 text-bold-600">
            ייבוא נתונים לפלנוגרמה חדשה
                      </div>
        </div>
        <div className="d-flex align-items-cente flex-wrap justify-content-center">
          {[
            {
              isMulti: false,
              allowSelectAll: false,
              datePicker: true,
              text: 'date',
              value: date,
              setValue: setDate,
              options: ['beginning of the year', 'year_back', 'last_year', 'half_year', customDate].map((option: any, index: number) => {
                return { index, value: index + 1, label: formatMessage({ id: option }) }
              })
            },
            {
              isMulti: true,
              allowSelectAll: true,
              text: 'branch',
              value: branchSelects,
              setValue: setBranchSelects,
              options: branches && branches[0] ? branches.map((option: any, index: number) => {
                if (option.BranchId === 9999 || option.BranchId === 999 || option.BranchId === 99) return
                return { index, value: option.BranchId, label: option.Name }
              }) : []
            },
            // {
            //   isMulti: true,
            //   allowSelectAll: true,
            //   text: 'group',
            //   value: groupsSelects,
            //   setValue: setGroupsSelects,
            //   options: groups && groups[0] ? groups.map((option: any, index: number) => {
            //     return { index, value: option.Id, label: option.Name }
            //   }) : []
            // },
            // {
            //   isMulti: true,
            //   allowSelectAll: true,
            //   text: 'subgroup',
            //   value: subgroupsSelects,
            //   setValue: setSubgroupsSelects,
            //   options: groupsSelects && groupsSelects.length ?
            //     subgroups && subgroups.length ?
            //       subgroups.filter((sg: any) => subGroupsIds.includes(sg.Id)).map((option: any, index: number) => {
            //         return { index, value: option.Id, label: option.Name }
            //       })
            //       : subgroups.map((option: any, index: number) => {
            //         return { index, value: option.Id, label: option.Name }
            //       }) : []
            // }
          ].map((s: any, si: number) => {
            if (s.text === 'branch') s.options = s.options.filter((x: any) => x)

            return (
              <div key={si} className="position-relative mb-05 newPlanoPopUp" >
                {/* <div><FormattedMessage id={s.text}/>:</div> */}
                <Select
                  isMulti={s.isMulti}
                  inputValue={inputValue[si] ? inputValue[si].value : ''}
                  onInputChange={(query, { action }) => {
                    // Prevents resetting our input after option has been selected
                    if (action !== "set-value") setInputValue(inputValue.map((iv: any, i: number) => {
                      if (i === si) return {
                        ...iv,
                        value: query
                      }
                    }));
                  }
                  }
                  allowSelectAll={s.allowSelectAll}
                  closeMenuOnSelect={s.isMulti ? false : true}
                  hideSelectedOptions={false}
                  blurInputOnSelect={false}
                  components={s.isMulti ? { Option, MultiValue, animatedComponents } : { MultiValue, DropdownIndicator, animatedComponents }}
                  isDisabled={loading ? true : false}
                  // components={{ DropdownIndicator:() => null, IndicatorSeparator:() => null }}	
                  name={s.text} F
                  placeholder={formatMessage({ id: s.text })}
                  styles={{
                    menu: provided => ({ ...provided, zIndex: 9999 }),
                    indicatorsContainer: (prevStyle, state) => false ? ({
                      ...prevStyle,
                      display: 'none'
                    }) : {},
                    valueContainer: (base) => ({
                      ...base,
                      maxHeight: '1.5rem',
                      height: '1.5rem',
                      position: 'initial'
                    }),

                    control: styles => ({
                      ...styles, width: '15rem!important', borderRadius: '2vh!important', maxHeight: '1.5rem', alignItems: 'baseline',
                      height: '1.5rem'
                    })
                  }}
                  className="ml-auto mr-2"
                  classNamePrefix='select'
                  onChange={(e: any, actionMeta: any) => {
                    if (e) {
                      const { action, option, removedValue } = actionMeta;
                      if (s.isMulti) {
                        const { value, index } = e
                        if (action === "select-option" && option.value === selectAllOption.value) {
                          s.setValue([selectAllOption, ...s.options])
                        } else if (
                          (action === "deselect-option" &&
                            option.value === selectAllOption.value) ||
                          (action === "remove-value" &&
                            removedValue.value === selectAllOption.value)
                        ) {
                          s.setValue([])
                        } else if (
                          actionMeta.action === "deselect-option" &&
                          e.length === s.options.length
                        ) {
                          s.setValue(s.options.filter(({ value }) => value !== option.value));
                        } else {
                          s.setValue(e)
                        }
                      } else {
                        if (e === s.value) s.setValue(null);
                        else {

                          if (e.value === 5) setIsDatePopupOpen(true)//if it's costume made date apply date picker 
                          s.setValue(e)
                        }
                      }
                    } else s.setValue(s.isMulti ? [] : null)
                  }}
                  value={si === 0 && s.value && s.value.value === 5 && date1 && date2 ? { index: 4, label: customDate, value: 5 } : s.value}
                  options={s.allowSelectAll ? [selectAllOption, ...s.options] : s.options}
                />
                {s.datePicker &&
                  date && date.value === 5 &&
                  <CalendarDoubleInput
                    displayDatePickerOutside={displayDatePicker}
                    setDisplayDatePickerOutside={setDisplayDatePicker}
                    onlyPopUp
                    setDate1={setDate1}
                    setDate2={setDate2}
                    date2={date2}
                    date1={date1}
                    top={'-3rem'}
                  />
                }
              </div>
            )
          })
          }


        </div>
      </ModalBody>
      <ModalFooter className="justify-content-end no-border">
        {loading && (<Spinner />)}
        <Button
          disabled={loading || !date || !branchSelects.length}
          color="primary"
          className="round text-bold-400 d-flex justify-content-center align-items-center width-7-rem cursor-pointer btn-primary"
          onClick={async () => {
            setLoading(true)

            let branchesFiltered = branchSelects.map((branch: any) => { if (branch.value === '<SELECT_ALL>') { return; } else { return branch.value } }).filter(b => b)
            let groupsFiltered = groupsSelects.map((group: any) => { if (group.value === '<SELECT_ALL>') { return; } else { return group.value } }).filter(g => g)
            let subGroupFiltered = subgroupsSelects.map((subgroup: any) => { if (subgroup.value === '<SELECT_ALL>') { return; } else { return subgroup.value } }).filter(s => s)

            let fetchedData = await fetchAndUpdatePlanogram([{type: 'newplano', forceUpdate:true, 
            params:{updatedb: 1, aisle_id,
              date_flag: date ? date.value : null,
              fromDate: date1 ? moment(new Date(date1)).format('YYYY-MM-DD') : null,
              toDate: date2 ? moment(new Date(date2)).format('YYYY-MM-DD') : null,
              branches: branchesFiltered,
              layoutId,
              groups: null,
              subgroups: null},changeRows: formatMessage}],dispatch)


            if (fetchedData.newPlano.FromDate && fetchedData.newPlano.ToDate) setRowsDataDates([fetchedData.newPlano.FromDate, fetchedData.newPlano.ToDate])
            if (fetchedData.newPlano.data) { setLoading(false); 
            }
            toogle(false)
          }}
        >
          <FormattedMessage id="click here" />
        </Button>
      </ModalFooter>
    </Modal>
  )
}