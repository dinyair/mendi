import * as React from 'react';
import classnames from 'classnames'
import { useSelector, useDispatch } from 'react-redux';
import { FormattedMessage, useIntl } from "react-intl"
import { ShelfMappingSingleRow } from './ShelfMappingSingleRow'

interface Props {
    shelfRow:any
    setShelfRows:any
}

export const ShelfMapping: React.FC<Props> = (props: Props): any => {
    const {shelfRow, setShelfRows} = props

    const addNewShelfLine = () => {
        setShelfRows([...shelfRow,
        {
            index: shelfRow.length,
            fromData: shelfRow[shelfRow.length - 1].untilData + 1,
            untilData: shelfRow[shelfRow.length - 1].untilData + 1,
            units: 5, mm: 1000
        }])
    }

    const addedShelfs = () => {
        return (
            <div>
                {shelfRow.map((row: any) => {
                    return (
                        <ShelfMappingSingleRow setShelfRows={setShelfRows} allRows={shelfRow} row={row} />
                    )
                })}
            </div>
        )
    }

    return (<>
        <div className='d-flex flex-column mb-1'>
            {addedShelfs()}
            <div className="text-bold-700 font-medium-1 text-turquoise"><span className='cursor-pointer' onClick={addNewShelfLine}><FormattedMessage id="addNewValue+" /></span></div>
        </div>
    </>);
};


export default ShelfMapping;


