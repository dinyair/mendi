import * as React from 'react';
import { FormattedMessage, useIntl } from "react-intl"
import classnames from 'classnames'
import { Tooltip } from '@material-ui/core';

interface InputProps {
    totalAvg: number
    percent: any
    data: any
    rows: any
    setRows: (groups: any) => void;
    setRedBorder: (boolean: boolean) => void;
    childNum: number
    redBorder?: boolean
    row?: any
    mergedGroups: any
    childAvgs?: any
}

export const InputAvg: React.FC<InputProps> = (props: InputProps): any => {
    const { formatMessage } = useIntl();
    const { percent, totalAvg, data, rows, setRows, setRedBorder,
        childNum, row, redBorder, mergedGroups, childAvgs } = props
    const [inputVal, setInputVal] = React.useState<any>(percent ? percent : data.percentAvg === 100 ? data.percentAvg.toFixed(0) : data.percentAvg.toFixed(0))

    const handleInputChange = (e: any) => {
        let afterDecimal = e.target.value.toString().split('.')
        let maxAfterDecimal = 3

        if (e.target.value <= 100 && afterDecimal[1] && afterDecimal[1].length <= maxAfterDecimal || e.target.value <= 100 && !afterDecimal[1]) {
            if (afterDecimal.length === 3) {
                setInputVal(e.target.value.toFixed(0))
                updateData(e.target.value.toFixed(0))

            } else {
                setInputVal(e.target.value)
                updateData(e.target.value)
            }
        }
    }

    React.useEffect(() => {
        if (row && row[0] && !row[0].BarCode) {
            let avgAmount = row.reduce((acc: number, rowa: any) => {
                if (rowa.editedPercentAvg || rowa.editedPercentAvg === 0) {
                    return acc + Number(rowa['editedPercentAvg'])
                } else {
                    return acc + Number(rowa['percentAvg'])
                }
            }, 0)
            if (avgAmount > 100) setRedBorder(true)
            else setRedBorder(false)
        }
    }, [rows])


    React.useEffect(() => {
        setInputVal(percent && percent > 0 ? percent : data.percentAvg.toFixed(0))
        setRedBorder(false)
    }, [mergedGroups])


    const updateData = (val: any) => {

        let myIndex = data.index
        let newRows = rows

        switch (childNum) {
            case 0:
                newRows[data.index].editedPercentAvg = Number(val)
                newRows[data.index].didEdit = true
                break;
            case 1:
                newRows[data.fathersArray[0]].rows[myIndex].editedPercentAvg = Number(val)
                newRows[data.fathersArray[0]].rows[myIndex].didEdit = true
                break;
            case 2:
                newRows[data.fathersArray[0]].rows[data.fathersArray[1]].rows[myIndex].editedPercentAvg = Number(val)
                newRows[data.fathersArray[0]].rows[data.fathersArray[1]].rows[myIndex].didEdit = true
                break;
            case 3:
                newRows[data.fathersArray[0]].rows[data.fathersArray[1]].rows[data.fathersArray[2]].rows[myIndex].editedPercentAvg = Number(val)
                newRows[data.fathersArray[0]].rows[data.fathersArray[1]].rows[data.fathersArray[2]].rows[myIndex].didEdit = true
                break;
            case 4:
                newRows[data.fathersArray[0]].rows[data.fathersArray[1]].rows[data.fathersArray[2]].rows[data.fathersArray[3]].rows[myIndex].editedPercentAvg = Number(val)
                newRows[data.fathersArray[0]].rows[data.fathersArray[1]].rows[data.fathersArray[2]].rows[data.fathersArray[3]].rows[myIndex].didEdit = true
                break;
        }
        setRows([...newRows])
    }

    return (
        <Tooltip arrow placement="top" title={`${formatMessage({ id: 'algoretail recommendation' })} ${data.percentAvg.toFixed(0)}%`}>
            <span onClick={(e) => e.stopPropagation()} className={classnames("height-2-2-rem overflow-hidden width-5-rem min-width-5-rem d-flex justify-content-center bg-gray position-relative font-small-3 cursor-pointer py-05 border-2 text-black text-bold-700 border-radius-2rem",
                {
                    'border-warning': totalAvg > 100 && childNum === 0 || redBorder || childAvgs > 100.1,
                })}>
                <span className="ml-1 align-self-center">%</span>
                <input type="number" onChange={(e) => handleInputChange(e)} value={inputVal} className={classnames("width-3-rem align-left no-outline d-rtl no-border d-flex align-self-center bg-gray justify-content-center position-relative font-small-3 cursor-pointer border-2 text-black text-bold-700 border-radius-2rem", {
                })} />
            </span>
        </Tooltip>
    )

}
export default InputAvg