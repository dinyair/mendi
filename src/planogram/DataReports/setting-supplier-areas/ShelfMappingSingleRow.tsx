import * as React from 'react';
import classnames from 'classnames'
import { useSelector, useDispatch } from 'react-redux';
import { FormattedMessage, useIntl } from "react-intl"
import { Tooltip } from '@material-ui/core';
import { Icon } from '@components';
import { config } from '@src/config'

interface Props {
    row: any
    allRows: any[]
    setShelfRows: any
}

export const ShelfMappingSingleRow: React.FC<Props> = (props: Props): any => {
    const { formatMessage } = useIntl();
    const { allRows, row, setShelfRows } = props


    const editData = (e: any, type: string) => {
        switch (type) {
            case 'toInput':
                setShelfRows(allRows.map((rowS: any) => {
                    let untilData: number = Number(rowS.untilData)
                    if (rowS.index === row.index) untilData = Number(e.target.value)
                    return { ...rowS, untilData }
                }))
                break;
            case 'unitsInput':
                setShelfRows(allRows.map((rowS: any) => {
                    let units: number = Number(rowS.units)
                    if (rowS.index === row.index) units = Number(e.target.value)
                    return { ...rowS, units }
                }))
                break;
            case 'mmInput':
                setShelfRows(allRows.map((rowS: any) => {
                    let mm: number = Number(rowS.mm)
                    if (rowS.index === row.index) mm = Number(e.target.value)
                    return { ...rowS, mm }
                }))
                break;
        }
    }
    const deleteLastRow = () => {
        setShelfRows(allRows.map((row: any) => {
            if (row.index === allRows.length - 1) return ''
            else return row
        }).filter((r: any) => r))
    }
    return (
        <>
            <div className='d-flex mb-1'> 
            <span className='d-flex justify-content-center align-items-center'>{formatMessage({ id: 'fields' })}</span>
                {[
                    { text: formatMessage({ id: 'from-' }), type: 'fromInput', readonly: true, onChange: () => null, stateSet: null, state: row.fromData },
                    { text: formatMessage({ id: 'until-' }), type: 'toInput', onChange: editData, state: row.untilData },
                    { text: formatMessage({ id: 'units' }), preText: formatMessage({ id: 'shelfs' }), type: 'unitsInput', onChange: editData, state: row.units },
                    { text: formatMessage({ id: 'mm' }), preText: formatMessage({ id: 'length' }), type: 'mmInput', onChange: editData, state: row.mm }
                ].map((item: any) => {
                    return (
                        <div className='d-flex'>
                            {item.preText && <span className='align-self-center ml-05'>{item.preText}</span>}
                            <span className={classnames("ml-05 height-2-2-rem overflow-hidden width-5-rem d-flex justify-content-center bg-gray position-relative font-small-3 cursor-pointer py-05 border-2 text-black text-bold-700 border-radius-2rem",
                                {})}>
                                {item.type === 'fromInput' || item.type === 'toInput' ? <span className="ml-1 align-self-center">{item.text}</span> : null}
                                {item.readonly || row.index != allRows.length - 1 && item.type === 'toInput'
                                    ?
                                    <input type="number" readOnly onChange={(e) => item.onChange(e)} value={item.state} className={classnames("align-left no-outline d-rtl no-border d-flex align-self-center bg-gray justify-content-center position-relative font-small-3 cursor-pointer border-2 text-black text-bold-700 border-radius-2rem", {
                                        'width-2-rem': item.type === 'unitsInput' || item.type === 'mmInput',
                                        'width-3-rem': item.type === 'fromInput' || item.type === 'toInput'
                                    })
                                    } /> :
                                    <input type="number" onChange={(e) => item.onChange(e, item.type)} value={item.state} className={classnames("align-left no-outline d-rtl no-border d-flex align-self-center bg-gray justify-content-center position-relative font-small-3 cursor-pointer border-2 text-black text-bold-700 border-radius-2rem", {
                                        'width-2-rem': item.type === 'unitsInput' || item.type === 'mmInput',
                                        'width-3-rem': item.type === 'fromInput' || item.type === 'toInput'
                                    })} />
                                }
                                {item.type === 'unitsInput' || item.type === 'mmInput' ? <span className="align-self-center">{item.text}</span> : null}
                            </span>
                        </div>
                    )
                })}
                    <Tooltip arrow className={classnames("d-flex ml-05 visibility-hidden", {
                        'visibility-shown': allRows.length - 1 === row.index && row.index != 0
                    })} placement="left" title={formatMessage({ id: 'delete' })}>
                        <div onClick={deleteLastRow}> <Icon src={'../' + config.iconsPath + "planogram/trash.svg"}
                            className="mr-05 cursor-pointer align-self-center" style={{ height: '0.5rem', width: '0.2rem' }} /></div>
                    </Tooltip>
            </div>
        </>
    );
};


export default ShelfMappingSingleRow;


