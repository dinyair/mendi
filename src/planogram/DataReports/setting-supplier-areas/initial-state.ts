import { getRequest, postRequest } from "@src/http";

// export const getCatalogUpdated = async () => {
//     const resp = await postRequest<any>(`getcatalog`, {});
//     return resp
// }
export const postShelfSpace = async (rec: any) => {
    const resp = await postRequest<any>('/planogram/shelfSetting', {
        rec
    });
    return resp
}
export const getShelfSpace = async (aisleId: any) => {
    const resp = await getRequest<any>(`/planogram/getShelfSetting/${aisleId}`)
    return resp
}
export const updateFaces = async (aisleId: any) => {
    const resp = await getRequest<any>(`/planogram/updateFaces/${aisleId}`)
    return resp
}
