import * as React from 'react';
import classnames from "classnames";
import { ChevronLeft } from 'react-feather';
import { InputAvg } from './InputAvg'
import { useIntl } from "react-intl"

import './groupItem.scss';
import CustomCheckBox from '../../generic/customCheckBox';

interface Props {
	// caption: any;
	children: React.ReactNode;
	disabled?: boolean;
	style?: object;
	className?: string;
	endArrow?: boolean;
	hideArrow?: boolean;
	dontAllowClick?: boolean;
	childNum: number
	isArrowRed?: boolean
	row?: any;
	rows?: any
	mainRow?: any
	mainGroupingRows?: any
	totalAvg?: any
	setRows?: any
	mergedGroups?: any
	checkbox?: boolean
	onCheckboxChanged?: (role: string, data: any) => void;
	data?: any
	updateData?: (data: any) => void;
	buttonClasses?: string
	buttonDivClass?: string
	zebraBg?: boolean
}

export const GroupItem: React.FC<Props> = props => {
	const { formatMessage } = useIntl();
	const observerRef = React.useRef<ResizeObserver>();
	const childRef = React.useRef<HTMLDivElement>(null);
	const parentRef = React.useRef<HTMLDivElement>(null);
	const [isOpen, setStateContent] = React.useState(false);
	const { totalAvg, disabled, setRows, children, style, className = '', endArrow = false, hideArrow,
		dontAllowClick, childNum, isArrowRed, row, rows, mainRow, mainGroupingRows, mergedGroups, checkbox, onCheckboxChanged, updateData, buttonClasses, buttonDivClass, zebraBg } = props;

	const [redBorder, setRedBorder] = React.useState<any>(false)
	const customizer = { direction: document.getElementsByTagName("html")[0].dir }
	const togglegroupItem = () => {
		if (!dontAllowClick) {
			setTimeout(() => { setStateContent(prevState => !prevState); }, 5);
		}
	}

	const setCheckbox = async (e:any,data: any, type: string, childrens: any[]) => {
		e &&  e.stopPropagation();
		if (updateData && onCheckboxChanged && props.data && props.data[data.navLink]) {
			var flag: number = props.data[data.navLink][type + '_flag'] === 0 ? 1 : 0
			var newData: any = props.data
			if (childrens && childrens.length) {
				newData[data.navLink][type + '_flag'] = flag
				if (type === 'read' && flag === 0) newData[data.navLink]['edit_flag'] = 0
				if (type === 'edit' && flag === 1) newData[data.navLink]['read_flag'] = 1
				for (var i = 0; i < childrens.length; i++) {
					let navLink = childrens[i].navLink
					if (newData[navLink]) {
						newData[navLink][type + '_flag'] = flag
						if (type === 'read' && flag === 0) newData[navLink]['edit_flag'] = 0
						if (type === 'edit' && flag === 1) newData[navLink]['read_flag'] = 1
					}
				}
			}
			else {
				newData[data.navLink][type + '_flag'] = flag
				if (type === 'read' && flag === 0) newData[data.navLink]['edit_flag'] = 0
				if (type === 'edit' && flag === 1 ) newData[data.navLink]['read_flag'] = 1

			}
			await onCheckboxChanged(newData[data.navLink].role, newData[data.navLink])

			updateData({ ...newData })
		}
	}

	React.useEffect(() => {

		if (rows && rows.length && !rows[0].BarCode) {
			let avgAmount = rows.reduce((acc: number, rowa: any) => {
				if (rowa.editedPercentAvg || rowa.editedPercentAvg === 0) {
					return acc + Number(rowa['editedPercentAvg'])
				} else {
					return acc + Number(rowa['percentAvg'])
				}
			}, 0)
			if (avgAmount > 100.1) setRedBorder(true)
			else setRedBorder(false)
		}
	}, [mainGroupingRows])


	const adaptResize = React.useCallback(entries => {
		if (parentRef.current) {
			for (const entry of entries) {
				parentRef.current.style.maxHeight = `${entry.contentRect.height}px`
			}
		}
	}, []);

	React.useEffect(() => {
		observerRef.current = new ResizeObserver(adaptResize);
		return () => {
			if (observerRef.current) {
				observerRef.current.disconnect();
			}
		}
	}, []);

	React.useEffect(() => {
		if (observerRef.current && childRef.current) {
			if (isOpen) {
				observerRef.current.observe(childRef.current)
			} else {
				observerRef.current.unobserve(childRef.current)
			}
		}
	}, [observerRef.current, childRef.current, isOpen]);

	const classesButton = classnames(`groupItem__button d-flex align-items-center ${buttonClasses}`, {
		'active-ltr': isOpen && customizer.direction == 'ltr',
		'active-rtl': isOpen && customizer.direction == 'rtl',
		'groupItem__button_end-arrow': endArrow
	});

	const classes = classnames('groupItem__content', {
		'is-open': isOpen,
	});
	const { offsetHeight } = childRef.current || { offsetHeight: 0 };
	const rootClasses = classnames('groupItem', {
		[className]: className,
		'bg-gray': zebraBg && childNum==0,
		'fatherBorder': childNum === 0,
	});
	const isIndeterminate = (ref:any,option:any,data:any)=>{
		let x = indeterminate(option)
		if (ref.current &&data.children && data.children.length&& x) {
            ref.current.indeterminate = true;
          }
	}
	let indeterminate = (type: string) => {
		let chckedCounter = 0
		rows.forEach((child: any) => {
			if (props.data && props.data[child.navLink] && props.data[child.navLink][type + '_flag'] > 0) chckedCounter++
		});
		return chckedCounter < rows.length && chckedCounter > 0 ? true : false
	};
	const groupItemHeader = (title: string, childNum: number, data: any) => {
		let rows = data.rows ? data.rows : data.children ? data.children : []
		let indeterminate = (type: string) => {
			let chckedCounter = 0
			rows.forEach((child: any) => {
				if (props.data && props.data[child.navLink] && props.data[child.navLink][type + '_flag'] > 0) chckedCounter++
			});
			return chckedCounter < rows.length && chckedCounter > 0 ? true : false
		};

		let childAvgs = rows.reduce((acc: number, rowa: any) => {
			if (rowa.editedPercentAvg || rowa.editedPercentAvg === 0) {
				return acc + Number(rowa['editedPercentAvg'])
			} else {
				return acc + Number(rowa['percentAvg'])
			}
		}, 0)

		let leftAmount = `${childNum * 1.2}rem`
		return (
			<div className="d-flex justify-content-between width-100-per" >
				<span 
					className={classnames("align-self-center width-15-rem elipsis align-left text-bold-500", {
						'text-warning': totalAvg > 100 && childNum === 0 || childAvgs > 100.01 || redBorder,
						'text-bold-700': childNum === 0
					})}>{title}</span>

				<div className={`d-flex justify-content-evenly position-relative width-35vw ${buttonDivClass}`} style={!checkbox ? { left: leftAmount } : rows.length !== 0 ? { left: '0rem' } : {}}>
					{checkbox ?
						['read', 'edit'].map((item: any, index: number) => (
							<>
								<div className="d-flex"	>
									<div key={index}
										className={classnames("mr-2 height-2-2-rem width-5-rem d-flex justify-content-center  position-relative font-small-3 cursor-pointer py-05 border-2 text-black text-bold-500 border-radius-2rem", {
										})}>
											{/* <input
											name={data.children && data.children.length ? 'main' : ''}
											type="checkbox" className="mr-05 bg-turquoise min-height-1-2-rem width-1-5-rem"
											onClick={(e:any) => setCheckbox(e,data, item, data.children)}
											disabled={disabled}
											ref={el => data.children && data.children.length && el && (el.indeterminate = indeterminate(item))}
											checked={props.data && props.data[data.navLink] && props.data[data.navLink][item + '_flag'] > 0}
										></input> */}
										<CustomCheckBox disabled={disabled} indeterminate={{rows,data,dProps:props}}  option={item} checked={props.data && props.data[data.navLink] && props.data[data.navLink][item + '_flag'] > 0} onClick={(e:any) => setCheckbox(e,data, item, data.children)} />
										{/* <CustomCheckBox rows={rows} pp={props} isIndeterminate={isIndeterminate} data={data}  option={item} checked={props.data && props.data[data.navLink] && props.data[data.navLink][item + '_flag'] > 0} onClick={(e:any) => setCheckbox(e,data, item, data.children)} /> */}
									</div>
								</div>

							</>
						))
						:
						[
							{ data: data.percentPrice },
							{ divider: true },
							{ data: data.percentAmount },
							{ divider: true },
							{ data: data.percentAvg },
							{ divider: true },
							{ data: data.percentMarketPrice },
							{ divider: true },
							{ data: data.editedPercentAvg, input: true, suggestedData: data.percentAvg },
						].map((item: any, index: number) => {
							if (item.divider) {
								return (<span className='border-light-table'></span>)
							}
							let percent = item.data >= 0 ? Number(item.data).toFixed(2) : null
							if (item.input) percent = item.data ? Number(item.data).toFixed(0) : item.suggestedData.toFixed(0)

							if (!item.input) {
								return (
									<div className="d-flex">
										<div key={index}
											className={classnames("height-2-2-rem width-5-rem d-flex justify-content-center bg-gray position-relative font-small-3 cursor-pointer py-05 border-2 text-black text-bold-700 border-radius-2rem", {
											})}>
											{`${percent}%`}
										</div>
									</div>
								)
							} else {
								return (
									<InputAvg
										data={data}
										totalAvg={totalAvg}
										percent={percent}
										rows={mainGroupingRows}
										row={row}
										setRows={setRows}
										childNum={childNum}
										childAvgs={childAvgs}
										redBorder={redBorder}
										setRedBorder={setRedBorder}
										mergedGroups={mergedGroups}
									/>
								)
							}

						})}

				</div>
			</div>
		)
	}

	return (
		<div className={rootClasses} style={{
			borderRight: redBorder && childNum === 0 ? '0.5px solid #d03639' : 'none',
			borderLeft: redBorder && childNum === 0 ? '2px solid red' : 'none'
		}}>
			<button className={classesButton}
			onClick={row && row.length ? togglegroupItem : () => { }}
			//onClick={ (mainRow.rows && mainRow.rows.length) ||  (mainRow.children && mainRow.children.length) ? togglegroupItem : null}
			>
				<span className={classnames("groupItem__button_icon", {
					"rotate-180": customizer.direction == 'ltr',
					'visibility-hidden': hideArrow
				})}><ChevronLeft color={redBorder || isArrowRed ? 'red' : 'black'} size={14} /></span>
				{groupItemHeader(checkbox ? formatMessage({ id: mainRow.title }) : mainRow.value === null ? formatMessage({ id: 'other' }) : mainRow.value === 'null' ? formatMessage({ id: 'other' }) : mainRow.value, childNum, mainRow)}
			</button>
			<div ref={parentRef} className={classes} style={{ maxHeight: isOpen ? offsetHeight : 0 }}>
				<div ref={childRef}>
					{children}
				</div>
			</div>
		</div>
	)
}
