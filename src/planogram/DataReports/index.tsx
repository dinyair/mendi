import * as React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Button } from "reactstrap"
import { AgGrid } from '@components'
import * as moment from 'moment'
import { Icon } from '@components'
import { config } from '@src/config'
import { FormattedMessage, useIntl } from "react-intl"
import { NewPlanoPopup } from './NewPlanoPopup'
import { NEWPLANO_ACTION } from "@src/planogram/shared/store/newPlano/newPlano.types"
import { AppState } from '@src/planogram/shared/store/app.reducer'
import { SettingSuppliersAreas } from './settingSuppliersAreas'
import { fetchAndUpdatePlanogram } from '@src/helpers/planogram';
import { useHistory } from 'react-router-dom'
import classnames from 'classnames'

export const rowData = (rows: any[], formatMessage: any) => {
	let newRows = rows ? rows.map((p: any, index: number) => {
		let mergedRows = rows.filter((g: any) => g.isSelected)//mergedGroups[mergedGroups.length-1] ? rowData.filter((row:any)=> row[mergedGroups[mergedGroups.length-1]] === p[mergedGroups[mergedGroups.length-1]] ) : rowData
		let totalRowsPrice = mergedRows.reduce((acc: number, row: any) => acc + row['TotalPrice'], 0)
		let Percent_TotalPrice: number = p['TotalPrice'] > 0 && totalRowsPrice > 0 ? ((p['TotalPrice'] / totalRowsPrice) * 100) : 0
		let totalRowsAmount = mergedRows.reduce((acc: number, row: any) => acc + row['TotalAmount'], 0)
		let Percent_TotalAmount: number = p['TotalAmount'] && totalRowsAmount ? ((p['TotalAmount'] / totalRowsAmount) * 100) : 0
		let Avg = p['TotalPrice'] && p['TotalAmount'] ? ((p['TotalPrice'] + p['TotalAmount']) / 2) : 0
		let Percent_Avg = Percent_TotalPrice && Percent_TotalAmount ? ((Percent_TotalPrice + Percent_TotalAmount) / 2) : 0
		let Percent_Profit: number = p.TotalPrice_buy && p.TotalAmount_buy && p.TotalPrice && p.TotalAmount ? (1 - ((p.TotalPrice_buy / p.TotalAmount_buy) / ((p.TotalPrice / p.TotalAmount) / 1.17))) * 100 : 0
		let totalRowsMarket = mergedRows.reduce((acc: number, row: any) => acc + row['TotalMarket'], 0)
		let Percent_MarketAmount: number = p['TotalMarket'] && totalRowsMarket ? ((p['TotalMarket'] / totalRowsMarket) * 100) : 0
		let Series = p.Series && p.Series != 'null' ? p.Series : formatMessage({ id: 'other' })
		let Segment = p.Segment && p.Segment != 'null' ? p.Segment : formatMessage({ id: 'other' })
		let Brand = p.Brand && p.Brand != 'null' ? p.Brand : formatMessage({ id: 'other' })
		let Status = p.Blocked ? formatMessage({ id: 'Blocked' }) : p.NewBarCode ? formatMessage({ id: 'NewBarCode' }) : p.LowSale ? formatMessage({ id: 'LowSale' }) : formatMessage({ id: 'Regular' })

		return {
			...p,
			Status,
			Series,
			isSelected: p.isSelected?true:false,
			Segment,
			Brand,
			TotalPrice: p.TotalPrice ? Number(p.TotalPrice.toFixed(0)) : 0,
			Percent_TotalPrice: p.isSelected ? Number(Percent_TotalPrice.toFixed(Percent_TotalPrice > 0 ? 2 : 0)) : 0,
			Percent_TotalAmount: p.isSelected ? Number(Percent_TotalAmount.toFixed(Percent_TotalAmount > 0 ? 2 : 0)) : 0,
			Avg: Avg,
			Percent_Avg: p.isSelected ? Number(Percent_Avg.toFixed(Percent_Avg > 0 ? 2 : 0)) : 0,
			MarketPrice: p.MarketPrice ? Number(p.MarketPrice.toFixed(2)) : 0,
			Percent_Profit: p.isSelected ? Number(Percent_Profit.toFixed(Percent_Profit > 0 ? 2 : 0)) : 0,
			Percent_MarketAmount: p.isSelected ? Number(Percent_MarketAmount.toFixed(Percent_MarketAmount > 0 ? 2 : 0)) : 0
		}
	}) : []
	return newRows
}

interface Props {
	readonly layoutId: number
	readonly showNewAlgoPoup: boolean
	readonly setShowNewAlgoPoup: (bool: boolean) => void
}

export const Reports: React.FC<Props> = (props: Props): any => {
	const dispatch = useDispatch()
	const history = useHistory()
	const { formatMessage } = useIntl()
	const { showNewAlgoPoup, layoutId, setShowNewAlgoPoup } = props
	const [date, setDate] = React.useState<any>('beginning of the year')
	const [dates, setDates] = React.useState<any>([])
	const [rowsDataDates, setRowsDataDates] = React.useState<any>([])
	const [branchSelects, setBranchSelects] = React.useState<any>([])
	const [isBranchesPopupOpen, setIsBranchesPopupOpen] = React.useState<boolean>(false)
	const [groupsSelects, setGroupsSelects] = React.useState<any>([])
	const [isGroupsPopupOpen, setIsGroupsPopupOpen] = React.useState<boolean>(false)
	const [subgroupsSelects, setSubgroupsSelects] = React.useState<any>([])
	const [isSubGroupsPopupOpen, setIsSubGroupsPopupOpen] = React.useState<boolean>(false)

	const [isSettingSuppliersAreas, setIsSettingSuppliersAreas] = React.useState<boolean>(false)
	const customizer = { direction: document.getElementsByTagName("html")[0].dir }

	const [backButtonDisabled, setBackButtonDisabled] = React.useState<boolean>(false)
	const [date1, setDate1] = React.useState<any>()
	const [date2, setDate2] = React.useState<any>()
	const newPlano = useSelector((state: AppState) => state.newPlano)
	const [isDataReady, setIsDataReady] = React.useState<boolean>(true)
	const [render, setRender] = React.useState<any>()
	const url = window.location.href
	const urlElements = url.split('/')
	let currentAisleIndex = parseInt(urlElements[7])


	const [rows, setRows] = React.useState<any>(newPlano && newPlano.data ? rowData(newPlano.data, formatMessage) : [])
	const [dataReady, setDataReady] = React.useState<boolean>(false)

	const setNewPlano = (newPlano: any) => {
		if (newPlano.data) { newPlano.data = rowData(newPlano.data, formatMessage) }
		if (newPlano) {
			dispatch({
				type: NEWPLANO_ACTION.SET_NEWPLANO,
				payload: newPlano
			})
		}
	}

	React.useEffect(() => {
		if (newPlano) {
			setIsDataReady(false)
			setRows(newPlano.data ? rowData(newPlano.data, formatMessage) : [])
			setTimeout(() => { setIsDataReady(true) }, 1);
		}
	}, [newPlano])
	React.useEffect(() => {
		setDataReady(false)
		setIsDataReady(false)

		if (rows && rows.length) {
			setTimeout(() => { setIsDataReady(true); setDataReady(true) }, 1);
		}
	}, [rows])




	const [fields, setFields] = React.useState<any>([
		{
			text: "choose", type: 'checkbox', pinned: customizer.direction == 'ltr' ? 'left' : 'right', width: window.innerWidth * 0.045, minWidth:customizer.direction == 'ltr' ? 135 : 125, filter: 'booleanFilter',

		},
		{
			text: "BarCode", type: 'string', width: window.innerWidth * 0.2, minWidth: 155,
			filter: 'agMultiColumnFilter', filterParams: { filters: [{ filter: 'agTextColumnFilter' }, { filter: 'agSetColumnFilter', filterParams: { suppressMiniFilter: true } }] }
		},
		{
			text: "ProductName", type: 'string', width: window.innerWidth * 0.12, minWidth: 100,
			filter: 'agMultiColumnFilter', filterParams: { filters: [{ filter: 'agTextColumnFilter' }, { filter: 'agSetColumnFilter', filterParams: { suppressMiniFilter: true } }] }
		},
		{
			text: "Supplier", type: 'string', width: window.innerWidth * 0.12, minWidth: 100,
			filter: 'agMultiColumnFilter', filterParams: { filters: [{ filter: 'agTextColumnFilter' }, { filter: 'agSetColumnFilter', filterParams: { suppressMiniFilter: true } }] }
		},
		{
			text: "Segment", type: 'string', width: window.innerWidth * 0.12, minWidth: 100,
			filter: 'agMultiColumnFilter', filterParams: { filters: [{ filter: 'agTextColumnFilter' }, { filter: 'agSetColumnFilter', filterParams: { suppressMiniFilter: true } }] }
		},
		{
			text: "Brand", type: 'string', width: window.innerWidth * 0.12, minWidth: 100,
			filter: 'agMultiColumnFilter', filterParams: { filters: [{ filter: 'agTextColumnFilter' }, { filter: 'agSetColumnFilter', filterParams: { suppressMiniFilter: true } }] }
		},
		{
			text: "Series", type: 'string', width: window.innerWidth * 0.12, minWidth: 100,
			filter: 'agMultiColumnFilter', filterParams: { filters: [{ filter: 'agTextColumnFilter' }, { filter: 'agSetColumnFilter', filterParams: { suppressMiniFilter: true } }] }
		},
		{
			text: "SubgroupName", type: 'string', hide: true, width: window.innerWidth * 0.12, minWidth: 100,
			filter: 'agMultiColumnFilter', filterParams: { filters: [{ filter: 'agTextColumnFilter' }, { filter: 'agSetColumnFilter', filterParams: { suppressMiniFilter: true } }] }
		},
		{
			text: "TotalPrice", type: 'fullNumber', aggFunc: 'mySum', width: window.innerWidth * 0.1, minWidth: 95,
			filter: 'agMultiColumnFilter', filterParams: { filters: [{ filter: 'agNumberColumnFilter' }, { filter: 'agSetColumnFilter', filterParams: { suppressMiniFilter: true } }] }
		},
		{
			text: "Percent_TotalPrice", type: 'percentage', aggFunc: 'calculation', width: window.innerWidth * 0.1, minWidth: 95,
			filter: 'agMultiColumnFilter', filterParams: { filters: [{ filter: 'agNumberColumnFilter' }, { filter: 'agSetColumnFilter', filterParams: { suppressMiniFilter: true } }] },
			aggFunction: (rows: any[], params: any) => {
				if (params.rowNode.aggData && params.rowNode.aggData['TotalPrice']) {
					let totalRowsPrice = rows.reduce((acc: number, row: any) => acc + Number(row['TotalPrice']), 0)
					let Percent_TotalPrice: number = params.rowNode.aggData['TotalPrice'] && totalRowsPrice > 0 ? ((params.rowNode.aggData['TotalPrice'] / totalRowsPrice) * 100) : 0
					return Percent_TotalPrice.toFixed(2)
				} else {
					return 0
				}
			}
		},
		{
			text: "TotalAmount", type: 'fullNumber', aggFunc: 'mySum', width: window.innerWidth * 0.1, minWidth: 95,
			filter: 'agMultiColumnFilter', filterParams: { filters: [{ filter: 'agNumberColumnFilter' }, { filter: 'agSetColumnFilter', filterParams: { suppressMiniFilter: true } }] }
		},
		{
			text: "Percent_TotalAmount", type: 'percentage', aggFunc: 'calculation', width: window.innerWidth * 0.1, minWidth: 95,
			filter: 'agMultiColumnFilter', filterParams: { filters: [{ filter: 'agNumberColumnFilter' }, { filter: 'agSetColumnFilter', filterParams: { suppressMiniFilter: true } }] },
			aggFunction: (rows: any[], params: any) => {
				if (params.rowNode.aggData && params.rowNode.aggData['TotalAmount']) {
					let totalRowsAmount = rows.reduce((acc: number, row: any) => acc + Number(row['TotalAmount']), 0)
					let Percent_TotalAmount: number = totalRowsAmount ? ((params.rowNode.aggData['TotalAmount'] / totalRowsAmount) * 100) : 0
					return Percent_TotalAmount.toFixed(2)
				} else { return 0 }
			}
		},
		{
			text: "Percent_Avg", type: 'percentage', aggFunc: 'calculation', width: window.innerWidth * 0.1, minWidth: 95,
			filter: 'agMultiColumnFilter', filterParams: { filters: [{ filter: 'agNumberColumnFilter' }, { filter: 'agSetColumnFilter', filterParams: { suppressMiniFilter: true } }] },
			aggFunction: (rows: any[], params: any) => {
				if (params.rowNode.aggData && params.rowNode.aggData['TotalPrice'] && params.rowNode.aggData['TotalAmount']) {
					let totalRowsPrice = rows.reduce((acc: number, row: any) => acc + Number(row['TotalPrice']), 0)
					let totalRowsAmount = rows.reduce((acc: number, row: any) => acc + Number(row['TotalAmount']), 0)
					let Percent_TotalPrice: number = totalRowsPrice ? params.rowNode.aggData['TotalPrice'] : 0
					let Percent_TotalAmount: number = totalRowsAmount ? params.rowNode.aggData['TotalAmount'] : 0
					let rowCalculated: number = Percent_TotalAmount + Percent_TotalPrice / 2
					let totalCalcualted: number = totalRowsAmount + totalRowsPrice / 2
					let avg = ((rowCalculated / totalCalcualted) * 100).toFixed(2)
					return avg
				} else { return 0 }
			}
		},
		{
			text: "Percent_Profit", type: 'percentage', aggFunc: 'calculation', width: window.innerWidth * 0.1, minWidth: 95,
			filter: 'agMultiColumnFilter', filterParams: { filters: [{ filter: 'agNumberColumnFilter' }, { filter: 'agSetColumnFilter', filterParams: { suppressMiniFilter: true } }] },
			aggFunction: (rows: any[], params: any) => {
				if (params.rowNode.aggData && params.rowNode.aggData['TotalAmount_buy'] && params.rowNode.aggData['TotalPrice_buy'] && params.rowNode.aggData['TotalAmount'] && params.rowNode.aggData['TotalPrice']) {
					let totalAmountBuy: number = params.rowNode.aggData['TotalAmount_buy']
					let totalPriceBuy: number = params.rowNode.aggData['TotalPrice_buy']
					let totalPrice: number = params.rowNode.aggData['TotalPrice']
					let TotalAmount: number = params.rowNode.aggData['TotalAmount']
					let profit: number = (1 - ((totalPriceBuy / totalAmountBuy) / ((totalPrice / TotalAmount) / 1.17))) * 100
					return profit.toFixed(2)
				} else { return 0 }
			}
		},
		{
			text: "Percent_MarketAmount", type: 'percentage', aggFunc: 'calculation', width: window.innerWidth * 0.1, minWidth: 95,
			filter: 'agMultiColumnFilter', filterParams: { filters: [{ filter: 'agNumberColumnFilter' }, { filter: 'agSetColumnFilter', filterParams: { suppressMiniFilter: true } }] },
			aggFunction: (rows: any[], params: any) => {
				if (params.rowNode.aggData) {
					let totalRowsMarket = rows.reduce((acc: number, row: any) => acc + Number(row['MarketPrice']), 0)
					let totalMarketSum = 0
					let selectedRows = params.rowNode.allLeafChildren.map((row: any) => { if (row.selected) { return row } }).filter((r: any) => r)
					selectedRows.forEach((row: any) => {
						totalMarketSum += Number(row.data['MarketPrice'])
					})
					let totalMarket = totalMarketSum
					let Percent_MarketAmount: number = totalRowsMarket ? ((totalMarket / totalRowsMarket) * 100) : 0
					return Percent_MarketAmount.toFixed(2)
				} else { return 0 }
			}
		},
		{
			text: "MarketPrice", type: 'fixedNumber', aggFunc: 'dontDisplayOnMain', width: window.innerWidth * 0.1, minWidth: 95,
			filter: 'agMultiColumnFilter', filterParams: { filters: [{ filter: 'agNumberColumnFilter' }, { filter: 'agSetColumnFilter', filterParams: { suppressMiniFilter: true } }] }
		},

		{
			text: "Status", type: 'string', width: window.innerWidth * 0.1, minWidth: 110, cellRenderer: 'IconRender', renderIcons: { Blocked: '#31baab', LowSale: '#1a56b0', NewBarCode: '#ff705d' },
			filter: 'agMultiColumnFilter', filterParams: { filters: [{ filter: 'agSetColumnFilter', filterParams: { suppressMiniFilter: true } }] }
		},
		{
			text: "Remark", type: 'string', cellEditorParams:{maxLength:10}, width: window.innerWidth * 0.23, minWidth: 140, editable: true,
			filter: 'agMultiColumnFilter', filterParams: { filters: [{ filter: 'agTextColumnFilter' }, { filter: 'agSetColumnFilter', filterParams: { suppressMiniFilter: true } }] }
		},
	])



	return (<>
		<div>
			{/* <Button onClick={() => setShowNewAlgoPoup(!showNewAlgoPoup)} >Click</Button> */}
			{layoutId && (
				<NewPlanoPopup
					isOpen={showNewAlgoPoup}
					layoutId={layoutId}
					toogle={setShowNewAlgoPoup}
					date={date}
					setDate={setDate}
					setDate1={setDate1}
					setDate2={setDate2}
					date1={date1}
					date2={date2}
					dates={dates}
					setDates={setDates}
					branchSelects={branchSelects}
					setBranchSelects={setBranchSelects}
					groupsSelects={groupsSelects}
					setGroupsSelects={setGroupsSelects}
					subgroupsSelects={subgroupsSelects}
					setSubgroupsSelects={setSubgroupsSelects}
					rowsDataDates={rowsDataDates}
					setRowsDataDates={setRowsDataDates}
					setNewPlano={setNewPlano}
				/>
			)}
		</div>

		{newPlano && newPlano.data && newPlano.data.length && dataReady ?
			isSettingSuppliersAreas ? <SettingSuppliersAreas
				rows={rows}
				toogle={setIsSettingSuppliersAreas} /> :
				<>
					<div className="width-100-per d-flex justify-content-between align-items-center">
						<div className="width-100-per d-flex">
							<div className="cursor-pointer" onClick={(e: any) => { e.preventDefault(); if (!backButtonDisabled) { setBackButtonDisabled(true); history.goBack(); setTimeout(() => history.goBack(), 10) } }}>
								<Icon src={'../' + config.iconsPath + "table/arrow-right.svg"}
									className={classnames("mr-05", {
										"rotate-180": customizer.direction == 'ltr',
									})} style={{ height: '0.5rem', width: '0.2rem' }} />
							</div>

							<div className="d-flex-column">
								<div className="d-flex font-medium-2 text-bold-700">
									<FormattedMessage id="report_planning_planogram" />
								</div>
								<div className="d-flex font-small-3 text-turquoise text-bold-600">
									&nbsp;
									{subgroupsSelects.length ? <>
										<div className="text-black"><FormattedMessage id="SubgroupName" /> -</div>
										&nbsp;
									</> : null}
									{subgroupsSelects.map((g: any) => g.label).toString()}
									&nbsp;
									<div className="text-black"><FormattedMessage id="Range of dates" /> -</div>
									&nbsp;
									&nbsp;
									<div className="cursor-pointer" onClick={() => setShowNewAlgoPoup(true)}>
										{moment(newPlano.FromDate).format('DD.MM.YY')} -  {moment(newPlano.ToDate).format('DD.MM.YY')}
									</div>
								</div>
							</div>
						</div>
						<Button
							onClick={() => setIsSettingSuppliersAreas(true)}
							color="primary"
							className="width-12-rem round text-bold-400 d-flex justify-content-center align-items-center width-auto cursor-pointer btn-primary"
						>
							<FormattedMessage id="setting shelf areas" />
						</Button>
					</div>
					<div className="p-1" style={{ height: '75vh' }}>

						<AgGrid
							gridHeight={'100%'}
							id={'BarCode'}
							hideWhenGroup={'Percent_Profit'}
							leftBorderField={'choose'}
							fields={fields}
							floatFilter
							checkbox
							rowBufferAmount={120}
							displayExcel
							displayColumnDisplayer
							displayPrint
							translateHeader
							checkboxOptions
							checkboxFirstColumn
							resizable
							changeRowDataOnSelectionChange
							customSortField={'isSelected'}
							customSortType={'boolean'}
							saveOption
							onClickSave={async (rows: any[]) => {
								const FromDate = moment(new Date(newPlano.FromDate)).format('MM.DD.YYYY')
								const ToDate = moment(new Date(newPlano.ToDate)).format('MM.DD.YYYY')
								setNewPlano({ newplano: rows, data: rows, aisle_id: currentAisleIndex, FromDate, ToDate })

								await fetchAndUpdatePlanogram([{
									forceUpdate: true, dontUpdateState: true, type: 'newplano', setDataRows: true, changeRows: formatMessage,
									params: { updatedb: 2, aisle_id: currentAisleIndex, newplano: rows }
								}], dispatch)

							}}
							groups={['Supplier', 'Segment', 'Brand', 'Series', 'SubgroupName']}
							totalRows={[fields[0].text, 'TotalPrice', 'Percent_TotalPrice', 'TotalAmount', 'Percent_TotalAmount', 'Percent_Avg', 'Percent_MarketAmount']}
							rows={rows}
							aggFunction={(rows: any[], params: any, field: string) => {
								return fields.filter((f: any) => f.text === field)[0].aggFunction(rows, params)
							}}
							editFields={[{ field: 'Remark'}]}
							displayLoadingScreen={!isDataReady}
						/>
					</div>
				</>
			: null}
	</>)
}

export default Reports


