import * as React from 'react'
import { connect } from 'react-redux'
import { AppState } from '@src/planogram/shared/store/app.reducer'
import { withRouter, RouteComponentProps } from 'react-router'
import { useGesture } from "react-use-gesture"
import { ThunkDispatch } from 'redux-thunk'
import { AnyAction } from 'redux'
import { TextBox, Button, SelectBox, NumberBox } from 'devextreme-react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChevronLeft, faCheck, faArrowCircleLeft, faSearch, faEdit, faArrowCircleUp, faArrowCircleDown, faArrowUp } from '@fortawesome/free-solid-svg-icons'
import { CatalogBarcode, CatalogProduct } from '@src/planogram/shared/interfaces/models/CatalogProduct'
import * as planogramApi from '@src/planogram/shared/api/planogram.provider'
import { setStore } from '@src/planogram/shared/store/planogram/store/store.actions'
import { PlanogramStore, PlanogramElementId, PlacementObject, PlanogramItem, PlanogramShelf, PlanogramAisle } from '@src/planogram/shared/store/planogram/planogram.types'
import { uiNotify } from '@src/planogram/shared/components/Toast'
import { BarcodeImage } from './generic/BarcodeImage'
import { addProductAction, deleteItemAction, editShelfItemAction } from '@src/planogram/shared/store/planogram/store/item/item.actions'
import { GroupSection } from '@src/planogram/shared/store/planogram/virtualize/virtualize.reducer'
import { AisleDefaultDimension, blankItem, blankPlanogramAisle, ProductDefaultDimensions } from '@src/planogram/shared/store/planogram/planogram.defaults'
import { setAisleAction } from '@src/planogram/shared/store/planogram/store/aisle/aisle.actions'
import { setSectionGroups } from '@src/planogram/shared/store/planogram/display/display.actions'
import { ProductSaleMap } from '@src/planogram/shared/store/catalog/catalog.types'
import { setWeeklySaleButch } from '@src/planogram/shared/store/catalog/catalog.action'

const mapStateToProps = (state: AppState, ownProps: RouteComponentProps<{
    store_id: string
}>) => ({
    ...ownProps,
    storeId: state.planogram.store ? state.planogram.store.store_id : null,
    sectionGroups: state.planogram.virtualStore.sectionGroups.groupList,
    virtualStore: state.planogram.virtualStore,
    store: state.planogram.store,
    productMap: state.newPlano ? state.newPlano.productsMap : null,
    shelvesDetails: state.planogram.virtualStore.shelfDetails,
    aisleShelves: state.planogram.virtualStore.shelfMap,
    newPlano: state.newPlano,
    groupMap: state.planogram.virtualStore.sectionGroups.groupMap,
    /* get sectionGroups from display (DB) rather than from virtualStore */
    sectionGroupsDisp: Object.keys(state.planogram.display.sectionGroups).map(v => state.planogram.display.sectionGroups[v]).filter(v => v != null),
    groupMapDisp: state.planogram.display.sectionGroups,
    user: state.auth.user,
})

const mapDispatchToProps = (dispatch: ThunkDispatch<AppState, any, AnyAction>) => ({
    setStore: (store: PlanogramStore) => dispatch(setStore(store)),
    addBarcodeToShelf: (shelfId: PlanogramElementId, barcode: CatalogBarcode, placement?: PlacementObject) =>
        dispatch(addProductAction(shelfId, barcode, placement)),
    updateShelfItem: (item: PlanogramElementId, placement: PlacementObject, barcode?: CatalogBarcode) =>
        dispatch(editShelfItemAction(item, placement, barcode)),
    setStoreAisle: (aisle: PlanogramAisle, aislePid?: PlanogramElementId) => {
        dispatch(setAisleAction(aisle, aislePid));
    },
    setSectionGroups: (sectionGroups: any) => {
        dispatch(setSectionGroups(sectionGroups));
    },
    setWeeklySaleBatch: (sales: any) => { dispatch(setWeeklySaleButch(sales)); },

})

const highlightOnFocus = (e: any) => {
    if (e.event && e.event.srcElement)
        e.event.srcElement.select();
};

type PlanogramDocumentComponentProps = ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>;
class PlanogramDocumentComponent extends React.Component<PlanogramDocumentComponentProps> {
    state = {
        loading: false,
        stage: "position",
        selectedGroup: "",
        selectedShelf: "",
        selectedItem: "",
        barcode: "",
        faces: 1,
        stack: 1,
        row: 1,
        manual_row_only: 0,
        position: 0,
        pWidth: 0,
        pHeight: 0,
        pDepth: 0,
        distFromStart: 0,
        distFromTop: null,
        originalStack: 1,
    }
    componentDidMount() {
        const { match, history, setStore } = this.props;
        const storeId = match.params.store_id;
        planogramApi.fetchPlanogramStore(storeId, this.props.user ? this.props.user.db : '').then((store) => {
            if (store == null)
                return history.push('/planogram', {
                    error: "Store was not found"
                });
            setStore(store);
        }).catch((err) => {
            console.error(err);
            history.push('/planogram', {
                error: "Error getting planogram"
            });
        });
    }
    setStage = (stage: string) => {
        this.setState({
            stage
        })
    }
    /*  add function to add an item to shelf and / or reorder the shelf */
    addShelfItem = async (selectedGroup: string, shelf: PlanogramShelf, newItem: PlanogramItem, shlfStart?: number, newItems?: PlanogramItem[]) => {
        // make sure there is a valid shelf
        if (!shelf || shelf === undefined) {
            console.log('PlanogramDocument addShelfItem no valid shelf', selectedGroup, shelf, newItem);
            return;
        }
        // make sure shelf has no undefined items
        let initItems = JSON.parse(JSON.stringify(shelf.items));
        shelf.items = [];
        for (let i = 0; i < initItems.length; i++) {
            if (initItems[i] && typeof initItems[i] != 'undefined')
                shelf.items.push(initItems[i]);
        }
        if (newItems) {
            initItems = JSON.parse(JSON.stringify(newItems));
            newItems = [];
            for (let i = 0; i < initItems.length; i++) {
                if (initItems[i] && typeof initItems[i] != 'undefined') {
                    initItems[i].placement.distFromStart = i;
                    newItems.push(initItems[i]);
                }
            }
        }
        console.log('addShelfItem document. shelf', JSON.parse(JSON.stringify(shelf)), 'new item', newItem, 'shlfStart', shlfStart, 'new items', newItems);
        const { virtualStore } = this.props;
        const shelvesDetails = virtualStore.shelfDetails;
        const aisleShelves = virtualStore.shelfMap;
        const groupList = virtualStore.sectionGroups.groupList;
        const sectionGroups = groupList.map(v => virtualStore.sectionGroups.groupMap[v]).filter(v => v != null);
        let shelfGroup: any = {};
        for (let i = 0; i < sectionGroups.length; i++) {
            for (let a = 0; a < sectionGroups[i].shelves.length; a++) {
                shelfGroup[sectionGroups[i].shelves[a]] = sectionGroups[i].group_id;
            }
        }

        // update shelf.distFromStart if shlfStart was sent
        if (shlfStart && shlfStart >= 0) {
            shelf.distFromStart = shlfStart;
            /***************************/
            let aisles_1 = this.props.store ? this.props.store.aisles : undefined;
            let aisleId_1 = shelf.id.substring(0, shelf.id.indexOf('SE'));
            let aisle_1: PlanogramAisle = JSON.parse(JSON.stringify(blankPlanogramAisle));
            if (aisles_1) {
                for (let i = 0; i < aisles_1.length; i++) {
                    if (aisles_1[i].id === aisleId_1) {
                        aisle_1 = aisles_1[i];
                        let seI = parseInt(shelf.id.substring(shelf.id.indexOf('SE') + 2, shelf.id.indexOf('SH')));
                        let shI = parseInt(shelf.id.substring(shelf.id.indexOf('SH') + 2));
                        if (aisle_1.sections[seI] && aisle_1.sections[seI].shelves[shI])
                            aisle_1.sections[seI].shelves[shI] = shelf;
                        break;
                    }
                }
            }
            if (aisle_1.id === aisleId_1) {
                await this.props.setStoreAisle(aisle_1);
                console.log('shelf after saving dist from start', JSON.parse(JSON.stringify(shelf)), 'aisle after saving new shelf distFromStart', aisle_1);
            }
            /***************************/
        }
        // make sure, in case no shlfStart was sent that the shelf.distFromStart fits with the first Item (ordered by distFromStart) on that shelf
        else {
            if (shelf.distFromStart > 0) {
                let num: number = shelf.items[0] && shelf.items[0].placement.distFromStart ? shelf.items[0].placement.distFromStart : 0;
                if (shelf.items.length >= 1 && num >= 0) shelf.distFromStart = num;
            }
        }
        console.log('new shelf distFromStart', shelf.distFromStart);

        // check if this shelf even has room to add a new item;
        if (shelf.distFromStart === shelf.dimensions.width && newItem && newItem.product > 0) {
            // if there is a next shelf activate function with the rest of the items 
            let fullShelf = JSON.parse(JSON.stringify(shelf));
            // this is the data for the actual shelf
            let shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[shelf.id]));

            // get the full combined shelves
            while (shelfDetails.main_shelf) {
                let ids = JSON.parse(JSON.stringify(shelfDetails.main_shelf));
                shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[ids]));
                fullShelf = aisleShelves[ids];
            }
            let { combined } = shelfDetails;
            if (combined == null) combined = [];
            let combinedShelves = [fullShelf].concat(combined.map((id: any) => aisleShelves[id]));
            let tCombinedShelves = [];
            // go over the combined shelves and remove any shelf with a different group_id than selectedGroup based on shelfGroup array 
            for (let i = 0; i < combinedShelves.length; i++) {
                let groupId = shelfGroup[combinedShelves[i].id];
                if ((groupId === undefined && combinedShelves[i + 1] && shelfGroup[combinedShelves[i + 1].id] != selectedGroup.toString()) || groupId === selectedGroup.toString())
                    tCombinedShelves.push(combinedShelves[i]);
            }
            combinedShelves = JSON.parse(JSON.stringify(tCombinedShelves));
            // find the index of the current shelf 
            let currI = combinedShelves.findIndex(object => object.id === shelf.id);
            if (currI >= 0 && combinedShelves[currI + 1]) {
                // move item/s to the next shelf
                console.log('call addShelfItem for shelf', JSON.parse(JSON.stringify(combinedShelves[currI + 1])), 'add new item', newItem);
                await this.addShelfItem(selectedGroup, JSON.parse(JSON.stringify(combinedShelves[currI + 1])), newItem);
            }
            console.log('shelf space too small to accep the new item');
            return;
        }
        // if there is a new item add it at the end of the shelf
        if (newItem && newItem.product > 0) {
            /*  item distFromTop on a freezer shelf */
            if (shelf.freezer === 1) {
                newItem.placement.distFromTop = (shelf.dimensions.height - ((newItem.placement.pHeight ? newItem.placement.pHeight : ProductDefaultDimensions.height) * newItem.placement.stack));
                newItem.placement.distFromStart = 0;
                console.log('shelf.freezer === 1',shelf.items.length, newItem.placement.distFromTop);
                if (shelf.items.length >= 1) {
                    let shelfItems = JSON.parse(JSON.stringify(shelf.items));
                    // sort shelf.items by distFromStart to get the actual 'last' (right most) item in the freezer
                    shelfItems.sort(function (a: any, b: any) {
                        let aNumber = a.placement.distFromStart ? a.placement.distFromStart : 0;
                        let bNumber = b.placement.distFromStart ? b.placement.distFromStart : 0;
                        let aId = a.Id;
                        let bId = b.Id;
                        return (aNumber > bNumber) ? 1 : (aNumber < bNumber) ? -1 : ((aId > bId) ? 1 : (aId < bId) ? -1 : 0);
                    });
                    let lastItem = shelfItems[shelfItems.length - 1];
                    newItem.placement.distFromStart = (lastItem.placement.distFromStart ? lastItem.placement.distFromStart : 0) + ((lastItem.placement.pWidth ? lastItem.placement.pWidth : ProductDefaultDimensions.width) * lastItem.placement.faces);
                    console.log('newItem.placement.distFromStart',lastItem,newItem.placement.distFromStart);
                }
            }
            /********************************************************/
            if (shelf.freezer === 2
                /* && newItem.placement.distFromTop != shelf.dimensions.height - ((newItem.placement.pHeight ? newItem.placement.pHeight : ProductDefaultDimensions.height) * newItem.placement.stack) */
            ) {
                if (newItem.id === '') {
                    console.log('newItem.id === ""');
                    if (shelf.items.length > 1) {
                        let lastI = parseInt(shelf.items[shelf.items.length - 1].id.substring(shelf.items[shelf.items.length - 1].id.indexOf('I') + 1));
                        let newI = 0;
                        if (lastI < 100) newI = lastI + 100;
                        else newI = lastI + 1;
                        newItem.id = shelf.id + "I" + newI;
                        console.log('1. newItem.id = ', newItem.id);
                    } else {
                        newItem.id = shelf.id + "I" + 100;
                        console.log('2. newItem.id = ', newItem.id);
                    }
                }
                console.log('addShelfItem freezer === 2 newItem', newItem);
            }
            shelf.items.push(newItem);
        }
        // if there are multiple items to add to the begining of the shelf add them
        if (newItems && newItems.length > 0) {
            // push all items on the shelf by 1 to allow to push new items at the begining of the shelf
            for (let i = 0; i < shelf.items.length; i++) {
                let itemDist = 0;
                if (shelf.items[i].placement.distFromStart != undefined)
                    itemDist = parseInt(JSON.stringify(shelf.items[i].placement.distFromStart));
                // if (shlfStart) itemDist += shlfStart;
                // shelf.items[i].placement.distFromStart = itemDist + newItems.length;
                shelf.items[i].placement.distFromStart = itemDist + 1;
            }
            console.log('1. newItems', JSON.parse(JSON.stringify(shelf)));
            // add the new items at the begining of the shelf items array
            for (let i = newItems.length - 1; i >= 0; i--) {
                if (newItems[i]) shelf.items.unshift(newItems[i]);
            }
            console.log('2. newItems', JSON.parse(JSON.stringify(shelf)));
            // go over all the items on the shelf and re-do their distance from start of shelf
            for (let i = 0; i < shelf.items.length; i++) {
                if (shelf.items[i]) {
                    let distToNextItem = 0;
                    let itemEndPoint = parseInt(JSON.stringify(shelf.items[i].placement.distFromStart)) + (shelf.items[i].placement.faces * parseInt(JSON.stringify(shelf.items[i].placement.pWidth)));
                    if (shelf.items[i + 1] && (i + 1 <= newItems.length || i > newItems.length)) distToNextItem = parseInt(JSON.stringify(shelf.items[i + 1].placement.distFromStart)) - itemEndPoint;
                    if (i === 0 && shlfStart != undefined) {
                        shelf.items[0].placement.distFromStart = shlfStart;
                        itemEndPoint += shlfStart;
                    }
                    if (shelf.items[i + 1]) shelf.items[i + 1].placement.distFromStart = itemEndPoint + (distToNextItem > 0 ? distToNextItem : 0);
                }
            }
            console.log('3. newItems', JSON.parse(JSON.stringify(shelf)));
        }
        console.log('original shelf after adding new items', JSON.parse(JSON.stringify(shelf)));

        /*  item distFromTop on a freezer shelf */
        if (shelf.freezer === 1) {
            // if it's a freezer return after having added this item
            let aisle: PlanogramAisle = JSON.parse(JSON.stringify(blankPlanogramAisle));
            let aisles = this.props.store ? this.props.store.aisles : undefined;
            let aisleId = shelf.id.substring(0, shelf.id.indexOf('SE'));
            if (aisles) {
                for (let i = 0; i < aisles.length; i++) {
                    if (aisles[i].id === aisleId) {
                        aisle = aisles[i];
                        let seI = parseInt(shelf.id.substring(shelf.id.indexOf('SE') + 2, shelf.id.indexOf('SH')));
                        let shI = parseInt(shelf.id.substring(shelf.id.indexOf('SH') + 2));
                        if (aisle.sections[seI] && aisle.sections[seI].shelves[shI])
                            aisle.sections[seI].shelves[shI] = shelf;
                        break;
                    }
                }
            }
            if (aisle.id === aisleId) {
                console.log('setStoreAisle', aisle);
                await this.props.setStoreAisle(aisle);
            }
            return;
        }
        /********************************************************/

        // save initial state of shelf with items ordered by distFromStart (distance from start of shelf)
        shelf.items.sort(function (a: any, b: any) {
            let aNumber = a.placement.distFromStart ? a.placement.distFromStart : 0;
            let bNumber = b.placement.distFromStart ? b.placement.distFromStart : 0;
            let aId = a.Id;
            let bId = b.Id;
            return (aNumber > bNumber) ? 1 : (aNumber < bNumber) ? -1 : ((aId > bId) ? 1 : (aId < bId) ? -1 : 0);
        });
        console.log('original shelf after sorting by distFromStart', JSON.parse(JSON.stringify(shelf)));

        // go over shelf and if there are 2 items occupying the same space and they are the same product 
        // - increase faces to the first and remove the second
        for (let i = 0; i < shelf.items.length; i++) {
            if (shelf.items[i]) {
                if (parseInt(JSON.stringify(shelf.items[i].placement.distFromStart)) < shelf.distFromStart)
                    shelf.items[i].placement.distFromStart = shelf.distFromStart;
                if (shelf.items[i + 1]) {
                    console.log('check item a', shelf.items[i], 'vs item a+1', shelf.items[i + 1]);
                    let first_dist: number = parseInt(JSON.stringify(shelf.items[i].placement.distFromStart));
                    let first_end = first_dist + (parseInt(JSON.stringify(shelf.items[i].placement.pWidth)) * shelf.items[i].placement.faces);
                    let second_dist: number = parseInt(JSON.stringify(shelf.items[i + 1].placement.distFromStart));
                    let second_end = second_dist + (parseInt(JSON.stringify(shelf.items[i + 1].placement.pWidth)) * shelf.items[i + 1].placement.faces);
                    if (((second_dist <= first_dist && second_end <= first_end) || (second_dist >= first_dist && second_end <= first_end) || (second_dist >= first_dist && second_end > first_end))
                        && shelf.items[i].product === shelf.items[i + 1].product) {
                        // same item - increase faces
                        shelf.items[i].placement.faces += shelf.items[i + 1].placement.faces;
                        // remove second item 
                        shelf.items.splice(i + 1, 1);
                    }
                }
            }
        }
        console.log('original shelf after consolidating same items', JSON.parse(JSON.stringify(shelf)));

        let viewShelf = JSON.parse(JSON.stringify(shelf));
        // empty all items from current shelf
        shelf.items = [];
        console.log('viewShelf initial', viewShelf, 'shelf without items', JSON.parse(JSON.stringify(shelf)));

        let distFromStart = viewShelf.items.length > 0 && viewShelf.items[0] ? viewShelf.items[0].placement.distFromStart : 0;
        let distInShelf = viewShelf.items.length > 0 && viewShelf.items[0] ? viewShelf.items[0].placement.distFromStart : 0;
        let index = 0;
        let startOfShelfId = viewShelf.id;
        let spaceInShelf = 0;
        console.log('before starting to add items - distFromStart', distFromStart, viewShelf.items);
        // go over the initial state and populate each shelf in combinedShelves with the correct items in the correct order
        for (let n = 0; n < viewShelf.items.length; n++) {
            let item = JSON.parse(JSON.stringify(viewShelf.items[n]));
            let nextItem = null;
            if (viewShelf.items[n + 1]) nextItem = viewShelf.items[n + 1];
            // console.log('item', item, 'itemExists', itemExists, 'nextItem', nextItem);
            if (spaceInShelf < 0) {
                // if there is a next shelf activate function with the rest of the items 
                let fullShelf = JSON.parse(JSON.stringify(shelf));
                // this is the data for the actual shelf
                let shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[shelf.id]));

                // get the full combined shelves
                while (shelfDetails.main_shelf) {
                    let ids = JSON.parse(JSON.stringify(shelfDetails.main_shelf));
                    shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[ids]));
                    fullShelf = aisleShelves[ids];
                }
                let { combined } = shelfDetails;
                if (combined == null) combined = [];
                let combinedShelves = [fullShelf].concat(combined.map((id: any) => aisleShelves[id]));
                let tCombinedShelves = [];
                // go over the combined shelves and remove any shelf with a different group_id than selectedGroup based on shelfGroup array 
                for (let i = 0; i < combinedShelves.length; i++) {
                    let groupId = shelfGroup[combinedShelves[i].id];
                    if ((groupId === undefined && combinedShelves[i + 1] && shelfGroup[combinedShelves[i + 1].id] != selectedGroup.toString()) || groupId === selectedGroup.toString())
                        tCombinedShelves.push(combinedShelves[i]);
                }
                combinedShelves = JSON.parse(JSON.stringify(tCombinedShelves));

                // find the index of the current shelf 
                let currI = combinedShelves.findIndex(object => object.id === viewShelf.id);
                if (currI >= 0 && combinedShelves[currI + 1]) {
                    while (combinedShelves[currI + 1] && (spaceInShelf * -1) > combinedShelves[currI + 1].dimensions.width) {
                        spaceInShelf += combinedShelves[currI + 1].dimensions.width;
                        console.log('call addShelfItem for shelf', combinedShelves[currI + 1], 'changing start to', combinedShelves[currI + 1].dimensions.width);
                        await this.addShelfItem(selectedGroup, combinedShelves[currI + 1], blankItem, combinedShelves[currI + 1].dimensions.width, []);
                        currI++;
                    }
                    let newItemsToSend: PlanogramItem[] = [];
                    if (n < viewShelf.items.length) {
                        // get all items left over into a new array
                        for (let i = n; i < viewShelf.items.length; i++) {
                            newItemsToSend.push(viewShelf.items[i]);
                        }
                        // // remove the items we're passing to the next shelf from the array of this shelf
                        // viewShelf.items.splice(n, viewShelf.items.length - n);
                    }
                    console.log('call addShelfItem for shelf', JSON.parse(JSON.stringify(combinedShelves[currI + 1])), 'changing start to', (spaceInShelf * -1), 'new items', newItemsToSend);
                    await this.addShelfItem(selectedGroup, JSON.parse(JSON.stringify(combinedShelves[currI + 1])), blankItem, (spaceInShelf * -1), newItemsToSend);
                    console.log('after call addShelfItem');
                    spaceInShelf = 0;
                    // leave the loop since there are no more items in this shelf to take care of  
                    //  - all the left over items are moved to the next shelf
                    break;
                }
            }
            if (item.placement.distFromStart > distFromStart && (startOfShelfId === item.id.substring(0, item.id.indexOf('I')) || item.id === ''))
                distFromStart = item.placement.distFromStart;
            item.id = startOfShelfId + 'I' + index;
            index++;
            item.placement.distFromStart = distFromStart;
            let itemWidth = viewShelf.items[n].placement.pWidth;
            let itemSpace = (itemWidth != undefined ? itemWidth : ProductDefaultDimensions.width) * item.placement.faces;
            distFromStart += itemSpace;
            distInShelf += itemSpace;
            spaceInShelf = viewShelf.dimensions.width - distInShelf;
            // add item back into the shelf
            shelf.items.push(item);


            // check if there is a next shelf 
            let fullShelf = JSON.parse(JSON.stringify(shelf));
            // this is the data for the actual shelf
            let shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[shelf.id]));
            // get the full combined shelves
            while (shelfDetails.main_shelf) {
                let ids = JSON.parse(JSON.stringify(shelfDetails.main_shelf));
                shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[ids]));
                fullShelf = aisleShelves[ids];
            }
            let { combined } = shelfDetails;
            if (combined == null) combined = [];
            let combinedShelves = [fullShelf].concat(combined.map((id: any) => aisleShelves[id]));
            let tCombinedShelves = [];
            // go over the combined shelves and remove any shelf with a different group_id than selectedGroup based on shelfGroup array 
            for (let i = 0; i < combinedShelves.length; i++) {
                let groupId = shelfGroup[combinedShelves[i].id];
                if ((groupId === undefined && combinedShelves[i + 1] && shelfGroup[combinedShelves[i + 1].id] != selectedGroup.toString()) || groupId === selectedGroup.toString())
                    tCombinedShelves.push(combinedShelves[i]);
            }
            combinedShelves = JSON.parse(JSON.stringify(tCombinedShelves));
            // find the index of the current shelf 
            let currI = combinedShelves.findIndex(object => object.id === viewShelf.id);


            if (distFromStart > viewShelf.dimensions.width && currI >= 0 && combinedShelves[currI + 1]) {
                // only of there is a next shelf can we reduce the viewShelf.dimensions.width from the next distFromStart 
                spaceInShelf = (distFromStart - viewShelf.dimensions.width) * -1;
                distFromStart = distFromStart - viewShelf.dimensions.width;
            }
            console.log('--- item', item, 'distInShelf', distInShelf, 'distFromStart next', distFromStart, 'spaceInShelf', spaceInShelf, "shelf", JSON.parse(JSON.stringify(shelf)));
        }
        // in case the last item on the shelf is the one that extends into the next shelf, 
        // after we're done going over all items we check if the spaceInShelf has extended byond the current shelf
        if (spaceInShelf < 0) {
            console.log('last item on shelf extends beyond this shelf. spaceInShelf', spaceInShelf);
            // if there is a next shelf activate function to update the next shelf's distFromStart 
            let fullShelf = JSON.parse(JSON.stringify(shelf));
            // this is the data for the actual shelf
            let shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[shelf.id]));

            // get the full combined shelves
            while (shelfDetails.main_shelf) {
                let ids = JSON.parse(JSON.stringify(shelfDetails.main_shelf));
                shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[ids]));
                fullShelf = aisleShelves[ids];
            }
            let { combined } = shelfDetails;
            if (combined == null) combined = [];
            let combinedShelves = [fullShelf].concat(combined.map((id: any) => aisleShelves[id]));
            let tCombinedShelves = [];
            // go over the combined shelves and remove any shelf with a different group_id than selectedGroup based on shelfGroup array 
            for (let i = 0; i < combinedShelves.length; i++) {
                let groupId = shelfGroup[combinedShelves[i].id];
                if ((groupId === undefined && combinedShelves[i + 1] && shelfGroup[combinedShelves[i + 1].id] != selectedGroup.toString()) || groupId === selectedGroup.toString())
                    tCombinedShelves.push(combinedShelves[i]);
            }
            combinedShelves = JSON.parse(JSON.stringify(tCombinedShelves));
            // find the index of the current shelf 
            let currI = combinedShelves.findIndex(object => object.id === viewShelf.id);
            if (currI >= 0 && combinedShelves[currI + 1]) {
                while (combinedShelves[currI + 1] && spaceInShelf * -1 > combinedShelves[currI + 1].dimensions.width) {
                    spaceInShelf += combinedShelves[currI + 1].dimensions.width;
                    console.log('call addShelfItem for shelf', JSON.parse(JSON.stringify(combinedShelves[currI + 1])), 'changing start to', combinedShelves[currI + 1].dimensions.width);
                    await this.addShelfItem(selectedGroup, JSON.parse(JSON.stringify(combinedShelves[currI + 1])), blankItem, combinedShelves[currI + 1].dimensions.width, []);
                    currI++;
                }
                let newItemsToSend: PlanogramItem[] = [];
                console.log('call addShelfItem for shelf', JSON.parse(JSON.stringify(combinedShelves[currI + 1])), 'changing start to', spaceInShelf * -1, 'new items', newItemsToSend);
                await this.addShelfItem(selectedGroup, JSON.parse(JSON.stringify(combinedShelves[currI + 1])), blankItem, (spaceInShelf * -1), newItemsToSend);
                console.log('after call addShelfItem');
            }
        }

        let aisle: PlanogramAisle = JSON.parse(JSON.stringify(blankPlanogramAisle));
        let aisles = this.props.store ? this.props.store.aisles : undefined;
        let aisleId = shelf.id.substring(0, shelf.id.indexOf('SE'));
        if (aisles) {
            for (let i = 0; i < aisles.length; i++) {
                if (aisles[i].id === aisleId) {
                    aisle = aisles[i];
                    let seI = parseInt(shelf.id.substring(shelf.id.indexOf('SE') + 2, shelf.id.indexOf('SH')));
                    let shI = parseInt(shelf.id.substring(shelf.id.indexOf('SH') + 2));
                    if (aisle.sections[seI] && aisle.sections[seI].shelves[shI]) {
                        aisle.sections[seI].shelves[shI] = shelf;
                    }
                    break;
                }
            }
        }
        if (aisle.id === aisleId) {
            console.log('setStoreAisle', aisle);
            await this.props.setStoreAisle(aisle);
        }
    }
    /***********************************************************************************/
    addBarcode = async () => {
        const { storeId, addBarcodeToShelf, updateShelfItem } = this.props;
        if (storeId == null) return;
        let { selectedShelf, selectedGroup, barcode, faces, stack, row, manual_row_only,
            position,
            distFromStart,
            distFromTop,
            originalStack
        } = this.state;
        let numberBarcode = parseInt(barcode);
        let placement: PlacementObject = {
            faces,
            stack,
            row,
            manual_row_only,
            position,
            pWidth: 0,
            pHeight: 0,
            pDepth: 0,
            distFromStart,
            distFromTop
        };
        this.setState({
            loading: true
        });
        /* get accurate distFromStart info */
        // let { aisleShelves, shelvesDetails, virtualStore } = this.props;
        let groupList = this.props.virtualStore.sectionGroups.groupList;
        let sectionGroups = groupList.map(v => this.props.virtualStore.sectionGroups.groupMap[v]).filter(v => v != null);
        let shelfGroup: any = {};
        for (let i = 0; i < sectionGroups.length; i++) {
            for (let a = 0; a < sectionGroups[i].shelves.length; a++) {
                shelfGroup[sectionGroups[i].shelves[a]] = sectionGroups[i].group_id;
            }
        }
        console.log('shelfGroup', shelfGroup);

        // this is the data for the actual shelf
        let shelfDetails = JSON.parse(JSON.stringify(this.props.shelvesDetails[selectedShelf]));
        let thisShelf: PlanogramShelf = this.props.aisleShelves[selectedShelf];
        // no matter which shelf the user chose we need to go back and work on the main shelf
        while (shelfDetails.main_shelf) {
            let ids = JSON.parse(JSON.stringify(shelfDetails.main_shelf));
            shelfDetails = JSON.parse(JSON.stringify(this.props.shelvesDetails[ids]));
            thisShelf = this.props.aisleShelves[ids];
        }
        let { combined } = shelfDetails;
        let combinedShelves = [thisShelf].concat(combined.map((id: any) => this.props.aisleShelves[id]));
        console.log('initial combined shelves', JSON.parse(JSON.stringify(combinedShelves)));
        let tCombinedShelves = [];
        // go over the combined shelves and remove any shelf with a different group_id than selectedGroup based on shelfGroup array 
        for (let i = 0; i < combinedShelves.length; i++) {
            let groupId = shelfGroup[combinedShelves[i].id];
            if ((groupId === undefined && combinedShelves[i + 1] && shelfGroup[combinedShelves[i + 1].id] != selectedGroup.toString()) || groupId === selectedGroup.toString())
                tCombinedShelves.push(combinedShelves[i]);
        }
        console.log('after group_id check combined shelves', JSON.parse(JSON.stringify(tCombinedShelves)));
        combinedShelves = tCombinedShelves;
        console.log('new combined shelves', JSON.parse(JSON.stringify(combinedShelves)));

        let combinedItems: any[] = [];
        for (let i = 0; i < combinedShelves.length; i++) {
            // sort shelf items by distFromStart
            combinedShelves[i].items.sort(function (a: any, b: any) {
                let aNumber = a.placement.distFromStart ? a.placement.distFromStart : 0;
                let bNumber = b.placement.distFromStart ? b.placement.distFromStart : 0;
                let aId = a.Id;
                let bId = b.Id;
                return (aNumber > bNumber) ? 1 : (aNumber < bNumber) ? -1 : ((aId > bId) ? 1 : (aId < bId) ? -1 : 0);
            });
            for (let a = 0; a < combinedShelves[i].items.length; a++) {
                combinedItems.push(combinedShelves[i].items[a]);
            }
        }
        // since we are adding the new item at the end of the shelf we can use the last item on the shelf as a base for the dist from start 
        const catalogProductDepth = this.props.newPlano && this.props.newPlano.productsMap && this.props.newPlano.productsMap[parseInt(barcode)] && this.props.newPlano.productsMap[parseInt(barcode)].length
            ? this.props.newPlano.productsMap[parseInt(barcode)].length : null
        let maxRow = Math.floor(thisShelf.dimensions.depth / (catalogProductDepth ? catalogProductDepth : 1));
        let product = barcode ? this.props.productMap[barcode] || this.props.productMap[parseInt(barcode)] : null;
        let pWidth = product ? product.width : 0;
        let pHeight = product ? product.height : 0;
        let pDepth = product ? product.length : 0;
        let width, height, depth;
        switch (JSON.stringify(position)) {
            case "0": {
                width = pWidth; height = pHeight; depth = pDepth;
                break;
            }
            case "1": {
                width = pHeight; height = pWidth; depth = pDepth;
                break;
            }
            case "2": {
                width = pWidth; height = pDepth; depth = pHeight;
                break;
            }
            case "3": {
                width = pHeight; height = pDepth; depth = pWidth;
                break;
            }
            case "4": {
                width = pDepth; height = pHeight; depth = pWidth;
                break;
            }
            case "5": {
                width = pDepth; height = pWidth; depth = pHeight;
                break;
            }
            default: {
                width = pWidth; height = pHeight; depth = pDepth;
            }
        }
        this.setState({ pWidth: width, pHeight: height, pDepth: depth });
        pWidth = width;
        pHeight = height;
        pDepth = depth;
        if ((combinedItems.length > 0 && numberBarcode != combinedItems[combinedItems.length - 1].product) || combinedItems.length === 0) {
            let newItem: PlanogramItem = {
                id: '',
                placement: {
                    faces: faces,
                    stack: stack,
                    row: row != 1 && row < maxRow ? row : maxRow,
                    manual_row_only: 0,
                    position: position,
                    pWidth: pWidth,
                    pHeight: pHeight,
                    pDepth: pDepth,
                    distFromStart: 0,
                    distFromTop: null
                },
                product: parseInt(barcode),
                lowSales: false,
                missing: 0,
                branch_id: this.props.store ? this.props.store.branch_id : 0,
                linkedItemUp: null,
                linkedItemDown: null,
                shelfLevel: 1,       
            };
            let loadShelf = this.props.aisleShelves[selectedShelf];
            if (combinedItems.length > 1) {
                let lastItem = combinedItems[combinedItems.length - 1];
                loadShelf = this.props.aisleShelves[lastItem.id.substring(0, lastItem.id.indexOf('I'))];
                let newDistFromStart = (lastItem.placement.distFromStart ? lastItem.placement.distFromStart : 0) + (lastItem.placement.faces * (lastItem.placement.pWidth ? lastItem.placement.pWidth : ProductDefaultDimensions.width))
                newItem.placement.distFromStart = newDistFromStart;
            }
            console.log('before addShelfItem', loadShelf, newItem, combinedShelves);
            /* 26.11.20 - check if it's a freezer and there is room */
            if (loadShelf.freezer === 1) {
                newItem.placement.distFromStart = 0;
                if (loadShelf.items.length > 1) {
                    let shelfItems = JSON.parse(JSON.stringify(loadShelf.items));
                    // sort shelf.items by distFromStart to get the actual 'last' (right most) item in the freezer
                    shelfItems.sort(function (a: any, b: any) {
                        let aNumber = a.placement.distFromStart ? a.placement.distFromStart : 0;
                        let bNumber = b.placement.distFromStart ? b.placement.distFromStart : 0;
                        let aId = a.Id;
                        let bId = b.Id;
                        return (aNumber > bNumber) ? 1 : (aNumber < bNumber) ? -1 : ((aId > bId) ? 1 : (aId < bId) ? -1 : 0);
                    });
                    let lastItem = shelfItems[shelfItems.length - 1];
                    // placed after last item so calculating distFromStart accordingly
                    newItem.placement.distFromStart = (lastItem.placement.distFromStart ? lastItem.placement.distFromStart : 0) + ((lastItem.placement.pWidth ? lastItem.placement.pWidth : ProductDefaultDimensions.width) * lastItem.placement.faces);
                }
                let problem = false;
                // check if the total height of the item extends beyond the freezer
                if ((newItem.placement.pHeight ? newItem.placement.pHeight : ProductDefaultDimensions.height) * newItem.placement.stack > loadShelf.dimensions.height) {
                    console.log('problem: total height > shelf height');
                    problem = true;
                }
                // check if the total width of the item extends beyond the freezer
                if ((newItem.placement.pWidth ? newItem.placement.pWidth : ProductDefaultDimensions.width) * newItem.placement.faces > loadShelf.dimensions.width) {
                    console.log('problem: total width > shelf width');
                    problem = true;
                }
                // make sure no item extends beyond its wall First check end point to the right (max width of shelf), then check end point on top (min 0)
                let itemEndPoint = (newItem.placement.distFromStart ? newItem.placement.distFromStart : 0) + ((newItem.placement.pWidth ? newItem.placement.pWidth : ProductDefaultDimensions.width) * newItem.placement.faces);
                if (itemEndPoint > loadShelf.dimensions.width) {
                    console.log('problem: end point > shelf width');
                    problem = true;
                }
                if (newItem.placement.distFromTop && newItem.placement.distFromTop < 0) {
                    console.log('problem: distFromTop < 0');
                    problem = true;
                }
                if (problem) {
                    uiNotify("Item extends beyond the freezer. Unable to add item into freezer (2).", "error", 8000);
                    this.setState({
                        loading: false,
                        barcode: "",
                        selectedItem: "",
                        faces: 1,
                        stack: 1,
                        row: 1,
                        position: 0,
                        pWidth: 0,
                        pHeight: 0,
                        pDepth: 0,
                        manual_row_only: 0
                    });
                    return;
                }
            }
            /********************************************************/
            await this.addShelfItem(selectedGroup, loadShelf, newItem);
            let newShelf: PlanogramShelf = this.props.aisleShelves[thisShelf.id];
            combinedShelves = [newShelf].concat(combined.map((id: any) => this.props.aisleShelves[id]));
            tCombinedShelves = [];
            // go over the combined shelves and remove any shelf with a different group_id than selectedGroup based on shelfGroup array 
            for (let i = 0; i < combinedShelves.length; i++) {
                let groupId = shelfGroup[combinedShelves[i].id];
                if ((groupId === undefined && combinedShelves[i + 1] && shelfGroup[combinedShelves[i + 1].id] != selectedGroup.toString()) || groupId === selectedGroup.toString())
                    tCombinedShelves.push(combinedShelves[i]);
            }
            combinedShelves = JSON.parse(JSON.stringify(tCombinedShelves));
            console.log('after addShelfItem', this.props.virtualStore, loadShelf, combinedShelves);
            combinedItems = [];
            for (let i = 0; i < combinedShelves.length; i++) {
                // sort shelf items by distFromStart
                combinedShelves[i].items.sort(function (a: any, b: any) {
                    let aNumber = a.placement.distFromStart ? a.placement.distFromStart : 0;
                    let bNumber = b.placement.distFromStart ? b.placement.distFromStart : 0;
                    let aId = a.Id;
                    let bId = b.Id;
                    return (aNumber > bNumber) ? 1 : (aNumber < bNumber) ? -1 : ((aId > bId) ? 1 : (aId < bId) ? -1 : 0);
                });
                for (let a = 0; a < combinedShelves[i].items.length; a++) {
                    combinedItems.push(combinedShelves[i].items[a]);
                }
            }
            console.log('new combinedItems', combinedItems, storeId, combinedItems[combinedItems.length - 1]);
            planogramApi.addShelfItemNew(storeId, combinedItems[combinedItems.length - 1]).then(() => {
                console.log('addedBarcodeToShelf', combinedItems[combinedItems.length - 1]);
                this.setState({
                    loading: false,
                    barcode: "",
                    selectedItem: "",
                    faces: 1,
                    stack: 1,
                    row: 1,
                    position: 0,
                    pWidth: 0,
                    pHeight: 0,
                    pDepth: 0,
                    manual_row_only: 0
                });
            }).catch(err => {
                console.error(err);
                this.setState({
                    loading: false
                });
                uiNotify("Unable to add item into shelf.", "error", 8000);
            });
        }
        else {
            // the item is identical to last item - we update faces and stack only
            let shelfItemId = combinedItems[combinedItems.length - 1].id;
            placement.faces += combinedItems[combinedItems.length - 1].placement.faces;
            if (combinedItems[combinedItems.length - 1].placement.stack > placement.stack)
                placement.stack = combinedItems[combinedItems.length - 1].placement.stack;
            const placement_2 = {
                faces: placement.faces,
                stack: placement.stack,
                row: placement.row,
                manual_row_only: placement.manual_row_only,
                position: combinedItems[combinedItems.length - 1].placement.position,
                pWidth: combinedItems[combinedItems.length - 1].placement.pWidth,
                pHeight: combinedItems[combinedItems.length - 1].placement.pHeight,
                pDepth: combinedItems[combinedItems.length - 1].placement.pDepth,
                distFromStart: combinedItems[combinedItems.length - 1].placement.distFromStart,
                distFromTop: combinedItems[combinedItems.length - 1].placement.distFromTop
            };
            if (placement_2.distFromTop && placement_2.distFromTop > 0) {
                placement_2.distFromTop = placement_2.distFromTop - ((placement_2.pHeight ? placement_2.pHeight : ProductDefaultDimensions.height) * (placement_2.stack - originalStack));
                if (placement_2.distFromTop < 0) placement_2.distFromTop = 0;
            }
            console.log('updateShelfItem storeId', storeId, 'shelfItemId', shelfItemId, 'selectedShelf', selectedShelf, 'placement', placement_2, 'barcode', barcode);
            /*  check if it's a freezer and there is room */
            let loadShelf = this.props.aisleShelves[selectedShelf];
            if (loadShelf.freezer === 1) {
                let newItem: PlanogramItem = {
                    id: '',
                    placement: placement_2,
                    product: parseInt(barcode),
                    lowSales: false,
                    missing: 0,
                    branch_id: this.props.store ? this.props.store.branch_id : 0,
                    /*  Add linking item option */
                    linkedItemUp: null,
                    linkedItemDown: null,
                    shelfLevel: 1,    
                };
                let newItemEndPoint = (newItem.placement.distFromStart ? newItem.placement.distFromStart : 0) + ((newItem.placement.pWidth ? newItem.placement.pWidth : ProductDefaultDimensions.width) * newItem.placement.faces);
                // let newItemBottom = (newItem.placement.distFromTop ? newItem.placement.distFromTop : (loadShelf.dimensions.height - ((newItem.placement.pHeight ? newItem.placement.pHeight : ProductDefaultDimensions.height) * newItem.placement.stack)))
                //     - ((newItem.placement.pHeight ? newItem.placement.pHeight : ProductDefaultDimensions.height) * newItem.placement.stack);
                let newItemTotalHeight = ((newItem.placement.pHeight ? newItem.placement.pHeight : ProductDefaultDimensions.height) * newItem.placement.stack);
                if (newItemEndPoint > loadShelf.dimensions.width || newItemTotalHeight > loadShelf.dimensions.height /*|| newItemBottom < 0*/) {
                    uiNotify("Item extends beyond the freezer. Unable to update item in freezer.", "error", 8000);
                    console.log('newItemTotalHeight', newItemTotalHeight);
                    this.setState({
                        loading: false,
                        barcode: "",
                        selectedItem: "",
                        faces: 1,
                        stack: 1,
                        row: 1,
                        manual_row_only: 0
                    });
                    return;
                }
            }
            /********************************************************/
            planogramApi.updateShelfItem(storeId, shelfItemId, selectedShelf, placement_2, barcode).then(() => {
                let parsedBarcode = barcode ? parseInt(barcode) : undefined;
                updateShelfItem(shelfItemId, placement_2, parsedBarcode);
                this.setState({
                    loading: false,
                    barcode: "",
                    selectedItem: "",
                    faces: 1,
                    stack: 1,
                    row: 1,
                    manual_row_only: 0
                });
            }).catch(err => {
                console.error(err);
                this.setState({
                    loading: false
                });
                uiNotify("Unable to add item into shelf.", "error", 8000);
            });
        }
        /*************************************************/
        /* Barak 29.9.20 replace planogramApi.addShelfItem with planogramApi.addShelfItemNew *
        planogramApi.addShelfItem(storeId, selectedShelf, barcode, placement).then(() => {
            console.log('addBarcodeToShelf', selectedShelf, numberBarcode, placement);
            addBarcodeToShelf(selectedShelf, numberBarcode, placement);
            this.setState({
                loading: false,
                barcode: "",
                selectedItem: "",
                faces: 1,
                stack: 1,
                row: 1,
                position: 0,
                pWidth: 0,
                pHeight: 0,
                pDepth: 0,
                manual_row_only: 0
            });
        }).catch(err => {
            console.error(err);
            this.setState({
                loading: false
            });
            uiNotify("Unable to add item into shelf.");
        });
        **************************************************************************************/
    }
    /* Barak 26.11.20 - add function addBarcodeOnTop */
    addShelfItemOnTop = async (selectedGroup: string, shelf: PlanogramShelf, newItem: PlanogramItem, shlfStart?: number, newItems?: PlanogramItem[]) => {
        // make sure there is a valid shelf
        if (!shelf || shelf === undefined) {
            console.log('PlanogramDocument addShelfItemOnTop no valid shelf', selectedGroup, shelf, newItem);
            return;
        }
        // make sure shelf has no undefined items
        let initItems = JSON.parse(JSON.stringify(shelf.items));
        shelf.items = [];
        for (let i = 0; i < initItems.length; i++) {
            if (initItems[i] && typeof initItems[i] != 'undefined')
                shelf.items.push(initItems[i]);
        }
        if (newItems) {
            initItems = JSON.parse(JSON.stringify(newItems));
            newItems = [];
            for (let i = 0; i < initItems.length; i++) {
                if (initItems[i] && typeof initItems[i] != 'undefined') {
                    initItems[i].placement.distFromStart = i;
                    newItems.push(initItems[i]);
                }
            }
        }
        console.log('addShelfItem document. shelf', JSON.parse(JSON.stringify(shelf)), 'new item', newItem, 'shlfStart', shlfStart, 'new items', newItems);
        const { virtualStore } = this.props;
        const shelvesDetails = virtualStore.shelfDetails;
        const aisleShelves = virtualStore.shelfMap;
        const groupList = virtualStore.sectionGroups.groupList;
        const sectionGroups = groupList.map(v => virtualStore.sectionGroups.groupMap[v]).filter(v => v != null);
        let shelfGroup: any = {};
        for (let i = 0; i < sectionGroups.length; i++) {
            for (let a = 0; a < sectionGroups[i].shelves.length; a++) {
                shelfGroup[sectionGroups[i].shelves[a]] = sectionGroups[i].group_id;
            }
        }

        // update shelf.distFromStart if shlfStart was sent
        if (shlfStart && shlfStart >= 0) {
            shelf.distFromStart = shlfStart;
            /***************************/
            let aisles_1 = this.props.store ? this.props.store.aisles : undefined;
            let aisleId_1 = shelf.id.substring(0, shelf.id.indexOf('SE'));
            let aisle_1: PlanogramAisle = JSON.parse(JSON.stringify(blankPlanogramAisle));
            if (aisles_1) {
                for (let i = 0; i < aisles_1.length; i++) {
                    if (aisles_1[i].id === aisleId_1) {
                        aisle_1 = aisles_1[i];
                        let seI = parseInt(shelf.id.substring(shelf.id.indexOf('SE') + 2, shelf.id.indexOf('SH')));
                        let shI = parseInt(shelf.id.substring(shelf.id.indexOf('SH') + 2));
                        if (aisle_1.sections[seI] && aisle_1.sections[seI].shelves[shI])
                            aisle_1.sections[seI].shelves[shI] = shelf;
                        break;
                    }
                }
            }
            if (aisle_1.id === aisleId_1) {
                await this.props.setStoreAisle(aisle_1);
                console.log('shelf after saving dist from start', JSON.parse(JSON.stringify(shelf)), 'aisle after saving new shelf distFromStart', aisle_1);
            }
            /***************************/
        }
        // make sure, in case no shlfStart was sent that the shelf.distFromStart fits with the first Item (ordered by distFromStart) on that shelf
        else {
            if (shelf.distFromStart > 0) {
                let num: number = shelf.items[0] && shelf.items[0].placement.distFromStart ? shelf.items[0].placement.distFromStart : 0;
                if (shelf.items.length >= 1 && num >= 0) shelf.distFromStart = num;
            }
        }
        console.log('new shelf distFromStart', shelf.distFromStart);

        // check if this shelf even has room to add a new item;
        if (shelf.distFromStart === shelf.dimensions.width && newItem && newItem.product > 0) {
            // if there is a next shelf activate function with the rest of the items 
            let fullShelf = JSON.parse(JSON.stringify(shelf));
            // this is the data for the actual shelf
            let shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[shelf.id]));

            // get the full combined shelves
            while (shelfDetails.main_shelf) {
                let ids = JSON.parse(JSON.stringify(shelfDetails.main_shelf));
                shelfDetails = JSON.parse(JSON.stringify(shelvesDetails[ids]));
                fullShelf = aisleShelves[ids];
            }
            let { combined } = shelfDetails;
            if (combined == null) combined = [];
            let combinedShelves = [fullShelf].concat(combined.map((id: any) => aisleShelves[id]));
            let tCombinedShelves = [];
            // go over the combined shelves and remove any shelf with a different group_id than selectedGroup based on shelfGroup array 
            for (let i = 0; i < combinedShelves.length; i++) {
                let groupId = shelfGroup[combinedShelves[i].id];
                if ((groupId === undefined && combinedShelves[i + 1] && shelfGroup[combinedShelves[i + 1].id] != selectedGroup.toString()) || groupId === selectedGroup.toString())
                    tCombinedShelves.push(combinedShelves[i]);
            }
            combinedShelves = JSON.parse(JSON.stringify(tCombinedShelves));
            // find the index of the current shelf 
            let currI = combinedShelves.findIndex(object => object.id === shelf.id);
            if (currI >= 0 && combinedShelves[currI + 1]) {
                // move item/s to the next shelf
                console.log('call addShelfItem for shelf', JSON.parse(JSON.stringify(combinedShelves[currI + 1])), 'add new item', newItem);
                await this.addShelfItem(selectedGroup, JSON.parse(JSON.stringify(combinedShelves[currI + 1])), newItem);
            }
            console.log('shelf space too small to accep the new item');
            return;
        }
        // if there is a new item add it at the end of the shelf
        if (newItem && newItem.product > 0) {
            /* Barak 26.11.20 - item distFromTop on a freezer shelf */
            if (shelf.freezer === 1 || shelf.freezer === 2) {
                console.log('addShelfItemOnTop shelf.freezer === 1 / 2',shelf.items.length);
                if (shelf.items.length === 0) {
                    newItem.placement.distFromTop = (shelf.dimensions.height - ((newItem.placement.pHeight ? newItem.placement.pHeight : ProductDefaultDimensions.height) * newItem.placement.stack));
                    newItem.placement.distFromStart = 0;
                }
                if (shelf.items.length >= 1) {
                    let lastItem = shelf.items[shelf.items.length - 1];
                    newItem.placement.distFromStart = (lastItem.placement.distFromStart ? lastItem.placement.distFromStart : 0);
                    // since distFromTop always assumes stack = 1, we'll remove 1 and then add (stack * height)
                    let origFromTop = (lastItem.placement.distFromTop
                        ? lastItem.placement.distFromTop
                        : (shelf.dimensions.height - ((newItem.placement.pHeight ? newItem.placement.pHeight : ProductDefaultDimensions.height) * newItem.placement.stack)));
                    newItem.placement.distFromTop = origFromTop - ((newItem.placement.pHeight ? newItem.placement.pHeight : ProductDefaultDimensions.height) * newItem.placement.stack);
                }
                console.log('addShelfItemOnTop newItem.placement',newItem.placement.distFromTop,newItem.placement.distFromStart);
            }
            /********************************************************/
            if (shelf.freezer === 2
                /* && newItem.placement.distFromTop != shelf.dimensions.height - ((newItem.placement.pHeight ? newItem.placement.pHeight : ProductDefaultDimensions.height) * newItem.placement.stack) */
            ) {
                if (newItem.id === '') {
                    console.log('newItem.id === ""');
                    if (shelf.items.length > 1) {
                        let lastI = parseInt(shelf.items[shelf.items.length - 1].id.substring(shelf.items[shelf.items.length - 1].id.indexOf('I') + 1));
                        let newI = 0;
                        if (lastI < 100) newI = lastI + 100;
                        else newI = lastI + 1;
                        newItem.id = shelf.id + "I" + newI;
                        console.log('1. newItem.id = ', newItem.id);
                    } else {
                        newItem.id = shelf.id + "I" + 100;
                        console.log('2. newItem.id = ', newItem.id);
                    }
                }
                console.log('addShelfItem freezer === 2 newItem', newItem);
            }
            shelf.items.push(newItem);
        }
        // if there are multiple items to add to the begining of the shelf add them
        if (newItems && newItems.length > 0) {
            // push all items on the shelf by 1 to allow to push new items at the begining of the shelf
            for (let i = 0; i < shelf.items.length; i++) {
                let itemDist = 0;
                if (shelf.items[i].placement.distFromStart != undefined)
                    itemDist = parseInt(JSON.stringify(shelf.items[i].placement.distFromStart));
                // if (shlfStart) itemDist += shlfStart;
                // shelf.items[i].placement.distFromStart = itemDist + newItems.length;
                shelf.items[i].placement.distFromStart = itemDist + 1;
            }
            console.log('1. newItems', JSON.parse(JSON.stringify(shelf)));
            // add the new items at the begining of the shelf items array
            for (let i = newItems.length - 1; i >= 0; i--) {
                if (newItems[i]) shelf.items.unshift(newItems[i]);
            }
            console.log('2. newItems', JSON.parse(JSON.stringify(shelf)));
            // go over all the items on the shelf and re-do their distance from start of shelf
            for (let i = 0; i < shelf.items.length; i++) {
                if (shelf.items[i]) {
                    let distToNextItem = 0;
                    let itemEndPoint = parseInt(JSON.stringify(shelf.items[i].placement.distFromStart)) + (shelf.items[i].placement.faces * parseInt(JSON.stringify(shelf.items[i].placement.pWidth)));
                    if (shelf.items[i + 1] && (i + 1 <= newItems.length || i > newItems.length)) distToNextItem = parseInt(JSON.stringify(shelf.items[i + 1].placement.distFromStart)) - itemEndPoint;
                    if (i === 0 && shlfStart != undefined) {
                        shelf.items[0].placement.distFromStart = shlfStart;
                        itemEndPoint += shlfStart;
                    }
                    if (shelf.items[i + 1]) shelf.items[i + 1].placement.distFromStart = itemEndPoint + (distToNextItem > 0 ? distToNextItem : 0);
                }
            }
            console.log('3. newItems', JSON.parse(JSON.stringify(shelf)));
        }
        console.log('original shelf after adding new items', JSON.parse(JSON.stringify(shelf)));

        /* Barak 26.11.20 - item distFromTop on a freezer shelf */
        // if (shelf.freezer === 1) { --> this is only done on freezers so it has to be a freezer
        // if it's a freezer return after having added this item
        let aisle: PlanogramAisle = JSON.parse(JSON.stringify(blankPlanogramAisle));
        let aisles = this.props.store ? this.props.store.aisles : undefined;
        let aisleId = shelf.id.substring(0, shelf.id.indexOf('SE'));
        if (aisles) {
            for (let i = 0; i < aisles.length; i++) {
                if (aisles[i].id === aisleId) {
                    aisle = aisles[i];
                    let seI = parseInt(shelf.id.substring(shelf.id.indexOf('SE') + 2, shelf.id.indexOf('SH')));
                    let shI = parseInt(shelf.id.substring(shelf.id.indexOf('SH') + 2));
                    if (aisle.sections[seI] && aisle.sections[seI].shelves[shI]) {
                        aisle.sections[seI].shelves[shI] = shelf;
                    }
                    break;
                }
            }
        }
        if (aisle.id === aisleId) {
            console.log('setStoreAisle', aisle);
            await this.props.setStoreAisle(aisle);
        }
        return;
        // }
        /********************************************************/
    }
    addBarcodeOnTop = async () => {
        const { storeId, updateShelfItem } = this.props;
        if (storeId == null) return;
        let { selectedShelf, selectedGroup, barcode, faces, stack, row, manual_row_only,
            position,
            /* Barak 29.9.20*/ distFromStart,
            /* Barak 26.11.20 */
            distFromTop,
            originalStack
            /******************/
        } = this.state;
        /* Barak log */ console.log('PlanogramDocument addBarcode', storeId, selectedShelf, selectedGroup, barcode);
        let numberBarcode = parseInt(barcode);
        let placement: PlacementObject = {
            faces,
            stack,
            row,
            pWidth: 0,
            pHeight: 0,
            pDepth: 0,
            manual_row_only,
            position,
            /* Barak 29.9.20*/ distFromStart,
            /* Barak 26.11.20 */ distFromTop
        };
        this.setState({
            loading: true
        });

        // this is the data for the actual shelf
        let thisShelf: PlanogramShelf = this.props.aisleShelves[selectedShelf];

        // since we are adding the new item at the end of the shelf we can use the last item on the shelf as a base for the dist from start 
        const catalogProductDepth = this.props.newPlano.productsMap[parseInt(barcode)] && this.props.newPlano.productsMap[parseInt(barcode)].length
            ? this.props.newPlano.productsMap[parseInt(barcode)].length : null
        let maxRow = Math.floor(thisShelf.dimensions.depth / (catalogProductDepth ? catalogProductDepth : 1));
        let product = barcode ? this.props.productMap[barcode] || this.props.productMap[parseInt(barcode)] : null;
        let pWidth = product ? product.width : 0;
        let pHeight = product ? product.height : 0;
        let pDepth = product ? product.length : 0;
        let width, height, depth;
        switch (JSON.stringify(position)) {
            case "0": {
                width = pWidth; height = pHeight; depth = pDepth;
                break;
            }
            case "1": {
                width = pHeight; height = pWidth; depth = pDepth;
                break;
            }
            case "2": {
                width = pWidth; height = pDepth; depth = pHeight;
                break;
            }
            case "3": {
                width = pHeight; height = pDepth; depth = pWidth;
                break;
            }
            case "4": {
                width = pDepth; height = pHeight; depth = pWidth;
                break;
            }
            case "5": {
                width = pDepth; height = pWidth; depth = pHeight;
                break;
            }
            default: {
                width = pWidth; height = pHeight; depth = pDepth;
            }
        }
        this.setState({ pWidth: width, pHeight: height, pDepth: depth });
        pWidth = width;
        pHeight = height;
        pDepth = depth;
        if ((thisShelf.items.length > 0 && numberBarcode != thisShelf.items[thisShelf.items.length - 1].product) || thisShelf.items.length === 0) {
            let newItem: PlanogramItem = {
                id: '',
                placement: {
                    faces: faces,
                    stack: stack,
                    row: row != 1 && row < maxRow ? row : maxRow,
                    manual_row_only: 0,
                    position: position,
                    pWidth: pWidth,
                    pHeight: pHeight,
                    pDepth: pDepth,
                    distFromStart: 0,
                    /* Barak 26.11.20 */ distFromTop: null
                },
                product: parseInt(barcode),
                lowSales: false,
                missing: 0,
                branch_id: this.props.store ? this.props.store.branch_id : 0,
                /* Barak 21.12.20 - Add linking item option */
                linkedItemUp: null,
                linkedItemDown: null,
                shelfLevel: 1,    
                /********************************************/
            };
            let loadShelf = this.props.aisleShelves[selectedShelf];
            if (thisShelf.items.length > 1) {
                let lastItem = thisShelf.items[thisShelf.items.length - 1];
                loadShelf = this.props.aisleShelves[lastItem.id.substring(0, lastItem.id.indexOf('I'))];
                let newDistFromStart = (lastItem.placement.distFromStart ? lastItem.placement.distFromStart : 0) + (lastItem.placement.faces * (lastItem.placement.pWidth ? lastItem.placement.pWidth : ProductDefaultDimensions.width))
                newItem.placement.distFromStart = newDistFromStart;
            }
            /* 26.11.20 - check if it's a freezer and there is room */
            if (loadShelf.freezer === 1) {
                newItem.placement.distFromStart = 0;
                newItem.placement.distFromTop = loadShelf.dimensions.height - ((newItem.placement.pHeight ? newItem.placement.pHeight : ProductDefaultDimensions.height) * newItem.placement.stack);
                if (loadShelf.items.length > 1) {
                    let shelfItems = JSON.parse(JSON.stringify(loadShelf.items));
                    // sort shelf.items by distFromStart to get the actual 'last' (right most) item in the freezer
                    shelfItems.sort(function (a: any, b: any) {
                        let aNumber = a.placement.distFromStart ? a.placement.distFromStart : 0;
                        let bNumber = b.placement.distFromStart ? b.placement.distFromStart : 0;
                        let aId = a.Id;
                        let bId = b.Id;
                        return (aNumber > bNumber) ? 1 : (aNumber < bNumber) ? -1 : ((aId > bId) ? 1 : (aId < bId) ? -1 : 0);
                    });
                    let lastItem = shelfItems[shelfItems.length - 1];
                    // placed on top of previous item so distFromStart is exactly the same
                    newItem.placement.distFromStart = (lastItem.placement.distFromStart ? lastItem.placement.distFromStart : 0);
                    let lastDistFromTop = lastItem.placement.distFromTop ? lastItem.placement.distFromTop : ((lastItem.placement.pHeight ? lastItem.placement.pHeight : ProductDefaultDimensions.height) * lastItem.placement.stack);
                    console.log('lastItem distFromTop', lastDistFromTop, 'new item height', (newItem.placement.pHeight ? newItem.placement.pHeight : ProductDefaultDimensions.height), 'new item faces', newItem.placement.stack);
                    newItem.placement.distFromTop = lastDistFromTop - ((newItem.placement.pHeight ? newItem.placement.pHeight : ProductDefaultDimensions.height) * newItem.placement.stack);
                    console.log('newItem distFromTop', newItem.placement.distFromTop);
                }
                let problem = false;
                // check if the total height of the item extends beyond the freezer
                if ((newItem.placement.pHeight ? newItem.placement.pHeight : ProductDefaultDimensions.height) * newItem.placement.stack > loadShelf.dimensions.height) {
                    console.log('problem: total height > shelf height');
                    problem = true;
                }
                // check if the total width of the item extends beyond the freezer
                if ((newItem.placement.pWidth ? newItem.placement.pWidth : ProductDefaultDimensions.width) * newItem.placement.faces > loadShelf.dimensions.width) {
                    console.log('problem: total width > shelf width');
                    problem = true;
                }
                // make sure no item extends beyond its wall First check end point to the right (max width of shelf), then check end point on top (min 0)
                let itemEndPoint = (newItem.placement.distFromStart ? newItem.placement.distFromStart : 0) + ((newItem.placement.pWidth ? newItem.placement.pWidth : ProductDefaultDimensions.width) * newItem.placement.faces);
                if (itemEndPoint > loadShelf.dimensions.width) {
                    console.log('problem: end point > shelf width');
                    problem = true;
                }
                if (newItem.placement.distFromTop && newItem.placement.distFromTop < 0) {
                    console.log('problem: distFromTop < 0');
                    problem = true;
                }

                if (problem) {
                    uiNotify("Item extends beyond the freezer. Unable to add item into freezer (1).", "error", 8000);
                    this.setState({
                        loading: false,
                        barcode: "",
                        selectedItem: "",
                        faces: 1,
                        stack: 1,
                        row: 1,
                        position: 0,
                        pWidth: 0,
                        pHeight: 0,
                        pDepth: 0,
                        manual_row_only: 0
                    });
                    return;
                }
            }
            /********************************************************/
            console.log('before addShelfItemOnTop', loadShelf, newItem);
            await this.addShelfItemOnTop(selectedGroup, loadShelf, newItem);
            let loadShelf2 = this.props.aisleShelves[selectedShelf];
            console.log('new loadShelf2.items', loadShelf2.items, storeId, loadShelf2.items[loadShelf2.items.length - 1]);
            planogramApi.addShelfItemNew(storeId, loadShelf2.items[loadShelf2.items.length - 1]).then(() => {
                console.log('addedBarcodeToShelf', loadShelf2.items[loadShelf2.items.length - 1]);
                this.setState({
                    loading: false,
                    barcode: "",
                    selectedItem: "",
                    faces: 1,
                    stack: 1,
                    row: 1,
                    position: 0,
                    pWidth: 0,
                    pHeight: 0,
                    pDepth: 0,
                    manual_row_only: 0
                });
            }).catch(err => {
                console.error(err);
                this.setState({
                    loading: false
                });
                uiNotify("Unable to add item into shelf.", "error", 8000);
            });
        }
        else {
            // the item is identical to last item - we update faces and stack only
            let shelfItemId = thisShelf.items[thisShelf.items.length - 1].id;
            if (thisShelf.items[thisShelf.items.length - 1].placement.faces > placement.faces)
                placement.faces = thisShelf.items[thisShelf.items.length - 1].placement.faces;
            // let newDistFromTop = thisShelf.dimensions.height;
            // if (thisShelf.items[thisShelf.items.length - 1].placement.distFromTop && thisShelf.items[thisShelf.items.length - 1].placement.distFromTop != undefined){
            //     newDistFromTop = parseInt(JSON.stringify(thisShelf.items[thisShelf.items.length - 1].placement.distFromTop));
            // }
            // let newHeight = ProductDefaultDimensions.height;
            // if (thisShelf.items[thisShelf.items.length - 1].placement.pHeight && thisShelf.items[thisShelf.items.length - 1].placement.pHeight != undefined){
            //     newHeight = parseInt(JSON.stringify(thisShelf.items[thisShelf.items.length - 1].placement.pHeight));
            // }
            // newHeight *= placement.stack;
            // newDistFromTop -= newHeight;
            placement.stack += thisShelf.items[thisShelf.items.length - 1].placement.stack;
            if (placement.distFromTop && placement.distFromTop > 0) {
                placement.distFromTop = placement.distFromTop - ((placement.pHeight ? placement.pHeight : ProductDefaultDimensions.height) * (placement.stack - originalStack));
                if (placement.distFromTop < 0) placement.distFromTop = 0;
            }
            const placement_2 = {
                faces: placement.faces,
                stack: placement.stack,
                row: placement.row,
                manual_row_only: placement.manual_row_only,
                position: thisShelf.items[thisShelf.items.length - 1].placement.position,
                pWidth: thisShelf.items[thisShelf.items.length - 1].placement.pWidth,
                pHeight: thisShelf.items[thisShelf.items.length - 1].placement.pHeight,
                pDepth: thisShelf.items[thisShelf.items.length - 1].placement.pDepth,
                distFromStart: thisShelf.items[thisShelf.items.length - 1].placement.distFromStart,
                /* Barak 26.11.20 */ distFromTop: thisShelf.items[thisShelf.items.length - 1].placement.distFromTop
            };
            console.log('updateShelfItem storeId', storeId, 'shelfItemId', shelfItemId, 'selectedShelf', selectedShelf, 'placement', placement_2, 'barcode', barcode);
            /* 26.11.20 - check if it's a freezer and there is room */
            let loadShelf = this.props.aisleShelves[selectedShelf];
            if (loadShelf.freezer === 1) {
                let newItem: PlanogramItem = {
                    id: '',
                    placement: placement_2,
                    product: parseInt(barcode),
                    lowSales: false,
                    missing: 0,
                    branch_id: this.props.store ? this.props.store.branch_id : 0,
                    /* Barak 21.12.20 - Add linking item option */
                    linkedItemUp: null,
                    linkedItemDown: null,
                    shelfLevel: 1,    
                    /********************************************/
                };
                let newItemEndPoint = (newItem.placement.distFromStart ? newItem.placement.distFromStart : 0) + ((newItem.placement.pWidth ? newItem.placement.pWidth : ProductDefaultDimensions.width) * newItem.placement.faces);
                // let newItemBottom = (newItem.placement.distFromTop ? newItem.placement.distFromTop : (loadShelf.dimensions.height - ((newItem.placement.pHeight ? newItem.placement.pHeight : ProductDefaultDimensions.height) * newItem.placement.stack)))
                //     - ((newItem.placement.pHeight ? newItem.placement.pHeight : ProductDefaultDimensions.height) * newItem.placement.stack);
                let newItemTotalHeight = ((newItem.placement.pHeight ? newItem.placement.pHeight : ProductDefaultDimensions.height) * newItem.placement.stack);
                if (newItemEndPoint > loadShelf.dimensions.width || newItemTotalHeight > loadShelf.dimensions.height /*|| newItemBottom < 0*/) {
                    uiNotify("Item extends beyond the freezer. Unable to update item in freezer.", "error", 8000);
                    this.setState({
                        loading: false,
                        barcode: "",
                        selectedItem: "",
                        faces: 1,
                        stack: 1,
                        row: 1,
                        manual_row_only: 0
                    });
                    return;
                }
            }
            /********************************************************/
            planogramApi.updateShelfItem(storeId, shelfItemId, selectedShelf, placement_2, barcode).then(() => {
                let parsedBarcode = barcode ? parseInt(barcode) : undefined;
                updateShelfItem(shelfItemId, placement_2, parsedBarcode);
                this.setState({
                    loading: false,
                    barcode: "",
                    selectedItem: "",
                    faces: 1,
                    stack: 1,
                    row: 1,
                    manual_row_only: 0
                });
            }).catch(err => {
                console.error(err);
                this.setState({
                    loading: false
                });
                uiNotify("Unable to add item into shelf.", "error", 8000);
            });
        }
        /*************************************************/
    }
    /*************************************************/
    updateItem = (shelfItemId: PlanogramElementId) => {
        const { storeId, updateShelfItem } = this.props;
        if (storeId == null) return;
        const { selectedShelf, barcode, faces, stack, row, manual_row_only,
            /* Barak & Osher 10.9.20 - support rotating products*/
            position,
            pWidth,
            pHeight,
            pDepth,
            /****************************************************/
            /* Barak 29.9.20*/ distFromStart,
            /* Barak 26.11.20 */
            distFromTop,
            originalStack
            /******************/
        } = this.state;
        let placement: PlacementObject = {
            faces,
            stack,
            row,
            manual_row_only,
            /* Barak & Osher 10.9.20 - support rotating products*/
            position,
            pWidth,
            pHeight,
            pDepth,
            /****************************************************/
            /* Barak 29.9.20*/ distFromStart,
            /* Barak 26.11.20 */ distFromTop
        };
        this.setState({
            loading: true
        });
        console.log('updateShelfItem storeId', storeId, 'shelfItemId', shelfItemId, 'selectedShelf', selectedShelf, 'placement', placement, 'barcode', barcode);
        /* 26.11.20 - check if it's a freezer and there is room */
        let loadShelf = this.props.aisleShelves[selectedShelf];
        if (loadShelf.freezer === 1) {
            if (placement.distFromTop && placement.distFromTop > 0) {
                placement.distFromTop = placement.distFromTop - ((placement.pHeight ? placement.pHeight : ProductDefaultDimensions.height) * (placement.stack - originalStack));
                if (placement.distFromTop < 0) placement.distFromTop = 0;
            }
            let newItem: PlanogramItem = {
                id: '',
                placement: placement,
                product: parseInt(barcode),
                lowSales: false,
                missing: 0,
                branch_id: this.props.store ? this.props.store.branch_id : 0,
                /* Barak 21.12.20 - Add linking item option */
                linkedItemUp: null,
                linkedItemDown: null,
                shelfLevel: 1,    
                /********************************************/
            };
            let newItemEndPoint = (newItem.placement.distFromStart ? newItem.placement.distFromStart : 0) + ((newItem.placement.pWidth ? newItem.placement.pWidth : ProductDefaultDimensions.width) * newItem.placement.faces);
            // let newItemBottom = (newItem.placement.distFromTop ? newItem.placement.distFromTop : (loadShelf.dimensions.height - ((newItem.placement.pHeight ? newItem.placement.pHeight : ProductDefaultDimensions.height) * newItem.placement.stack)))
            //     - ((newItem.placement.pHeight ? newItem.placement.pHeight : ProductDefaultDimensions.height) * newItem.placement.stack);
            let newItemTotalHeight = ((newItem.placement.pHeight ? newItem.placement.pHeight : ProductDefaultDimensions.height) * newItem.placement.stack);
            if (newItemEndPoint > loadShelf.dimensions.width || newItemTotalHeight > loadShelf.dimensions.height /*|| newItemBottom < 0*/) {
                uiNotify("Item extends beyond the freezer. Unable to update item in freezer.", "error", 8000);
                this.setState({
                    loading: false,
                    barcode: "",
                    selectedItem: "",
                    faces: 1,
                    stack: 1,
                    row: 1,
                    manual_row_only: 0
                });
                return;
            }
        }
        /********************************************************/
        planogramApi.updateShelfItem(storeId, shelfItemId, selectedShelf, placement, barcode).then(() => {
            let parsedBarcode = barcode ? parseInt(barcode) : undefined;
            updateShelfItem(shelfItemId, placement, parsedBarcode);
            this.setState({
                loading: false,
                barcode: "",
                selectedItem: "",
                faces: 1,
                stack: 1,
                row: 1,
                manual_row_only: 0
            });
        }).catch(err => {
            console.error(err);
            this.setState({
                loading: false
            });
            uiNotify("Unable to add item into shelf.", "error", 8000);
        });
    }
    /* Barak 23.11.20 - allow to update selectedShelf */
    updateSelectedShelf(shelfId: string) {
        this.setState({ selectedShelf: shelfId });
        /* Barak 25.11.20 */
        // save selectedShelf in localStorage
        localStorage.setItem("selectedShelf", shelfId);
        console.log('Document setItem', shelfId);
        /******************/
    }
    /**************************************************/

    /* Barak 8.12.20 - get sectionGroups from display (DB) rather than from virtualStore */
    getSectionGroups = async (storeId: number) => {
        let sectionGroups = await planogramApi.getSectionGroups(storeId);
        await this.props.setSectionGroups(sectionGroups);
    }
    /*************************************************************************************/
    /* Barak 8.12.20 - get missing aisle from DB */
    getEmptyAisle = async (aisleId: number) => {
        this.setState({ loading: true });
        let newAisle = await planogramApi.fetchPlanogramAisle(this.props.store ? this.props.store.store_id : '', aisleId, this.props.user ? this.props.user.db : '');
        if (!newAisle || newAisle === undefined || newAisle.sections === undefined) {
            this.setState({ loading: false });
            return;
        }
        // let salesLength = resp.sales ? resp.sales.length : 0;
        // const salesMap: ProductSaleMap = {}
        // for (let i = 0; i < salesLength; i++) {
        //     const sale = resp.sales[i];
        //     // if (sale.BarCode === 7290000468006) console.log('productsPredictedSales 7290000468006', sale);
        //     salesMap[sale.BarCode] = {
        //         weekly: sale.WeeklyAverage,
        //         monthly: null,
        //             /* Barak 13.5.2020*/ statusClaMlay: sale.statusCalMlay,
        //             /* Barak 24.6.2020*/ hourly: sale.HourlyAverage,
        //     };
        //     // if (sale.BarCode === 7290000468006) console.log('salesMap[7290000468006]', salesMap[7290000468006]);
        // }
        // this.props.setWeeklySaleBatch(salesMap);
        console.log('after fetchPlanogramAisle time', new Date().getHours(), ':', new Date().getMinutes(), ':', new Date().getSeconds());
        if (this.props.store) {
            let store = this.props.store;
            for (let i = 0; i < store.aisles.length; i++) {
                if (store.aisles[i] != undefined && newAisle != undefined && store.aisles[i].aisle_id === newAisle.aisle_id) {
                    store.aisles[i] = newAisle;
                    await this.props.setStore(store);
                    break;
                }
            }
        }
        this.setState({ loading: false });
    }
    /*********************************************/
    /* Barak 14.12.20 - get aisle sales from DB *
    getAisleSales = async (aisleId: number) => {
        this.setState({ loading: true });
        let sales = await planogramApi.fetchSingleAisleSales(this.props.store ? this.props.store.store_id : '', aisleId);
        let salesLength = sales.length;
        const salesMap: ProductSaleMap = {}
        for (let i = 0; i < salesLength; i++) {
            const sale = sales[i];
            salesMap[sale.BarCode] = {
                weekly: sale.WeeklyAverage,
                monthly: null,
                    statusClaMlay: sale.statusCalMlay,
                    hourly: sale.HourlyAverage,
            };
        }
        this.props.setWeeklySaleBatch(salesMap);
        this.setState({ loading: false });
    }    
    ********************************************/

    render() {
        const { stage, loading } = this.state;
        const { storeId } = this.props;
        if (storeId == null || loading)
            return <div className="planogram-document loader"></div>

        /* Barak 8.12.20 - if groupMapDisp is empty we get it from the server */
        if (Object.keys(this.props.groupMapDisp).length === 0) this.getSectionGroups(storeId);
        if (this.state.selectedShelf != '' && stage === "barcode") {
            let aisleId = this.state.selectedShelf.substring(0, this.state.selectedShelf.indexOf('SE'));
            if (this.props.store) {
                let index = this.props.store.aisles.findIndex(a => a.id === aisleId);
                if (index >= 0 && (this.props.store.aisles[index].empty && this.props.store.aisles[index].aisle_id != 0)) {
                    this.getEmptyAisle(this.props.store.aisles[index].aisle_id);
                }
                /* Barak 14.12.20 - get this aisle sales *
                else { this.getAisleSales(this.props.store.aisles[index].aisle_id); }
                *****************************************/
            }
        }
        /**********************************************************************/

        let stageElement = null;
        if (stage === "position")
            stageElement = <DocumentStagePosition
                storeId={storeId}
                setState={(state) => this.setState(state)}
                selectedGroup={this.state.selectedGroup}
                selectedShelf={this.state.selectedShelf}
            />
        else if (stage === "barcode")
            stageElement = <DocumentStageBarcode
                addBarcode={this.addBarcode}
                addBarcodeOnTop={this.addBarcodeOnTop}
                updateItem={this.updateItem}
                /* Barak 23.11.20 - allow to update selectedShelf */
                updateSelectedShelf={this.updateSelectedShelf}
                shelfList={this.props.groupMap[this.state.selectedGroup] ? this.props.groupMap[this.state.selectedGroup].shelves : null}
                /**************************************************/
                storeId={storeId}
                setState={(state) => this.setState(state)}
                barcode={this.state.barcode}
                selectedShelf={this.state.selectedShelf}
                selectedGroup={this.state.selectedGroup}
                selectedItem={this.state.selectedItem}
                faces={this.state.faces}
                stack={this.state.stack}
                row={this.state.row}
                /* Barak & Osher 10.9.20 - support rotating products*/
                position={this.state.position}
                pWidth={this.state.pWidth}
                pHeight={this.state.pHeight}
                pDepth={this.state.pDepth}
                /****************************************************/
                manual_row_only={this.state.manual_row_only}
            />

        return <div className="planogram-document">
            <div className="container">
                <div className="actions">
                    {stage === "barcode" ?
                        // Barak 5.5.20 Replace
                        // <Button
                        //     style={{ padding: "0.5em", float: "left" }}
                        //     onClick={() => this.setStage("position")}>
                        //     <FontAwesomeIcon icon={faArrowCircleLeft} />
                        //     <span style={{ marginRight: "0.5em" }}>בחירת מיקום</span>
                        // </Button>
                        <Button
                            style={{ padding: "0.2em", float: "left" }}
                            onClick={() => {
                                /* Barak 25.11.20 */
                                // remove selectedShelf and selectedGroup from localStorage
                                localStorage.removeItem("selectedShelf");
                                localStorage.removeItem("selectedGroup");
                                console.log('Document removeItem');
                                /******************/
                                this.setStage("position")
                            }}>
                            <FontAwesomeIcon icon={faArrowCircleLeft} />
                            <span style={{ marginRight: "0.5em" }}>בחירת מיקום</span>
                        </Button>
                        : null}
                </div>
                {stageElement}
            </div>
        </div>
    }
}

type StageComponentBaseProps = { storeId: number, setState: (state: any) => void };

const positionStageMapState = (state: AppState, ownProps: StageComponentBaseProps & {
    selectedGroup: string,
    selectedShelf: string,
}) => ({
    ...ownProps,
    sectionGroups: state.planogram.virtualStore.sectionGroups.groupList.map(v => state.planogram.virtualStore.sectionGroups.groupMap[v]).filter(v => v != null),
    groupMap: state.planogram.virtualStore.sectionGroups.groupMap,
    /* Barak 8.12.20 - get sectionGroups from display (DB) rather than from virtualStore */
    sectionGroupsDisp: Object.keys(state.planogram.display.sectionGroups).map(v => state.planogram.display.sectionGroups[v]).filter(v => v != null),
    groupMapDisp: state.planogram.display.sectionGroups,
    /*************************************************************************************/
});

class DocumentStagePositionComponent extends React.Component<ReturnType<typeof positionStageMapState>> {

    render() {
        const { groupMap, sectionGroups, groupMapDisp, sectionGroupsDisp } = this.props;

        const { selectedGroup, selectedShelf } = this.props;
        /* Barak 8.12.20    const shelfList: string[] | null = groupMap[selectedGroup] ? groupMap[selectedGroup].shelves : null; */
        /* Barak 8.12.20 */ const shelfList: string[] | null = groupMapDisp[selectedGroup] ? JSON.parse(groupMapDisp[selectedGroup].shelves) : null;
        console.log('groupMapDisp', groupMapDisp, 'sectionGroupsDisp', sectionGroupsDisp, 'shelfList', shelfList);

        /* Barak 25.11.20 */
        // check if there is selectedShelf and selectedGroup in localStorage
        // if there is - save them in state and update stage to "barcode"
        let mSelectedShelf = localStorage.getItem("selectedShelf");
        let mSelectedGroup = localStorage.getItem("selectedGroup");
        /* Barak 8.12.20    let index = this.props.sectionGroups.findIndex(line => line.group_id === mSelectedGroup);*/
        /* Barak 8.12.20 */ let index = this.props.sectionGroupsDisp.findIndex(line => line.group_id.toString() === mSelectedGroup);
        console.log('initial Document getItem', this.props.sectionGroupsDisp, mSelectedGroup, mSelectedShelf, index);
        if (mSelectedShelf && mSelectedGroup && index >= 0) {
            this.props.setState({
                selectedGroup: mSelectedGroup, selectedShelf: mSelectedShelf,
                stage: "barcode",
                barcode: '',
                faces: 1,
                row: 1,
                stack: 1,
                pWidth: 0,
                pHeight: 0,
                pDepth: 0,
                selectedItem: ''
            });
        } else {
            // only remove selectedShelf / selectedGroup from local storage if we already got the sectionGroupsDisp and they do not match
            if (this.props.sectionGroupsDisp.length > 0) {
                // remove selectedShelf and selectedGroup from localStorage
                localStorage.removeItem("selectedShelf");
                localStorage.removeItem("selectedGroup");
                console.log('Document removeItem');
                // this.props.setState({stage:"position"});
            }
        }
        /******************/

        console.log('render sectionGroups', sectionGroupsDisp, groupMapDisp);
        return (
            <div className="stage-position">
                {/* Barak 5.5.20 <h1>סריקת מיקום</h1> */}
                <h2>סריקת מיקום</h2>
                <div className="input-row">
                    <div className="row-label" placeholder="קוד מיקום המצויין בחנות...">קוד מיקום: </div>
                    <SelectBox
                        rtlEnabled
                        items={/* Barak 8.12.20 this.props.sectionGroups */ this.props.sectionGroupsDisp}
                        value={selectedGroup}
                        onValueChanged={(e) => this.props.setState({ selectedGroup: e.value, selectedShelf: "" })}
                        valueExpr="group_id"
                        displayExpr={(item?: GroupSection) => item && `${item.group_id} - ${item.aisle_name || item.aisle}`}
                        className="row-input"
                        placeholder="הכנס מיקום בחנות..." />
                </div>
                <div className="input-row">
                    <div className="row-label">מדף: </div>
                    <SelectBox
                        rtlEnabled
                        disabled={shelfList == null || shelfList.length === 0}
                        value={selectedShelf}
                        displayExpr="title"
                        valueExpr="shelf_id"
                        onValueChanged={(e) => this.props.setState({ selectedShelf: e.value })}
                        className="row-input"
                        placeholder="בחירת מדף..."
                        items={shelfList ? shelfList.map((pid, i) => ({ shelf_id: pid, title: "מדף " + (i + 1) })) : []}
                    />
                </div>
                <div className="input-row">
                    <Button
                        rtlEnabled
                        className="row-input"
                        disabled={!selectedGroup || !selectedShelf}
                        style={{ width: "100%" }}
                        onClick={() => {
                            /* Barak 25.11.20 */
                            // save selectedShelf and selectedGroup in localStorage
                            localStorage.setItem("selectedShelf", selectedShelf);
                            localStorage.setItem("selectedGroup", selectedGroup);
                            console.log('Document setItem', selectedShelf, selectedGroup);
                            /******************/
                            this.props.setState({
                                stage: "barcode",
                                barcode: '',
                                faces: 1,
                                row: 1,
                                stack: 1,
                                pWidth: 0,
                                pHeight: 0,
                                pDepth: 0,
                                selectedItem: ''
                            })
                        }} >
                        <FontAwesomeIcon icon={faChevronLeft} />
                        <span style={{ marginRight: "1em" }}>הכנס פריטים</span>
                    </Button>
                </div>
            </div>
        )
    }
}
const DocumentStagePosition = connect(positionStageMapState)(DocumentStagePositionComponent);


type DocumentStageBarcodeOwnProps = {
    barcode?: string,
    faces: number,
    stack: number,
    row: number,
    /* Barak & Osher 10.9.20 - support rotating products*/
    position: number,
    pWidth: number,
    pHeight: number,
    pDepth: number,
    /****************************************************/
    manual_row_only: number,
    selectedShelf: PlanogramElementId,
    selectedGroup: PlanogramElementId,
    selectedItem?: PlanogramElementId,
    addBarcode: () => void,
    addBarcodeOnTop: () => void,
    updateItem: (shelfItemId: PlanogramElementId) => void,
    /* Barak 23.11.20 - allow to update selectedShelf */
    updateSelectedShelf: (shelfId: string) => void,
    shelfList: string[] | null
    /**************************************************/
};
const barcodeMapStateToProps = (state: AppState, ownProps: StageComponentBaseProps & DocumentStageBarcodeOwnProps) => ({
    ...ownProps,
    catalogMap: state.newPlano ? state.newPlano.productsMap : null,
    store: state.planogram.store,
    virtualStore: state.planogram.virtualStore,
    selectedGroup: ownProps.selectedGroup,
    shelf: state.planogram.virtualStore.shelfMap[ownProps.selectedShelf] ? state.planogram.virtualStore.shelfMap[ownProps.selectedShelf] : null,
    shelfMap: state.planogram.virtualStore.shelfMap,
    shelfDetails: state.planogram.virtualStore.shelfDetails[ownProps.selectedShelf] ? state.planogram.virtualStore.shelfDetails[ownProps.selectedShelf] : null,
});
const barcodeMapDispatchToProps = (dispatch: ThunkDispatch<AppState, any, any>) => ({
    deleteShelfItem: (shelfId: PlanogramElementId, itemId: PlanogramElementId) =>
        dispatch(deleteItemAction(shelfId, itemId))
})

type DocumentStageBarcodeProps = ReturnType<typeof barcodeMapStateToProps> & ReturnType<typeof barcodeMapDispatchToProps>;
class DocumentStageBarcodeComponent extends React.Component<DocumentStageBarcodeProps> {
    barcodeInputRef = React.createRef<TextBox>();
    facesInputRef = React.createRef<NumberBox>();
    shelfContainerRef = React.createRef<HTMLDivElement>();

    state = {
        faces: this.props.faces,
        row: this.props.row,
        stack: this.props.stack,
        position: this.props.position,
        pWidth: this.props.pWidth,
        pHeight: this.props.pHeight,
        pDepth: this.props.pDepth,
    }

    componentDidUpdate = (prevProps: any) => {
        if (this.props.barcode != prevProps.barcode) {
            this.setState({
                faces: this.props.faces,
                row: this.props.row,
                stack: this.props.stack,
                position: this.props.position,
                pWidth: this.props.pWidth,
                pHeight: this.props.pHeight,
                pDepth: this.props.pDepth
            });
        }
    }

    // localFaces: number = 0;
    // localStack: number = 0;

    insertBarcode = () => {
        /******
        if (this.localFaces > 0) {
            this.props.setState({ faces: this.localFaces });
            this.localFaces = 0;
        }
        if (this.localStack > 0) {
            this.props.setState({ stack: this.localStack });
            this.localStack = 0;
        }
        ******/
        const { catalogMap, barcode, selectedShelf, selectedItem } = this.props;
        if (barcode == null)
            return uiNotify("Barcode empty", "error", 8000);
        const barcodeNumber = parseInt(barcode);
        if (!catalogMap[barcodeNumber.toString()])
            return uiNotify("Barcode does not exist.", "error", 8000);
        // const shelf = shelfMap[selectedShelf];
        // const item = shelf ? shelf.items.find(item => item.product === barcodeNumber) : null;
        if (selectedItem) {
            console.log('before this.props.updateItem selectedItem', selectedItem);
            this.props.updateItem(selectedItem);
        }
        else {
            this.props.addBarcode();
        }
        this.focusBarcodeInput();
    }
    /* Barak 26.11.20 - add function insertBarcodeOnTop */
    insertBarcodeOnTop = () => {
        const { catalogMap, barcode, selectedShelf, selectedItem } = this.props;
        if (barcode == null)
            return uiNotify("Barcode empty", "error", 8000);
        const barcodeNumber = parseInt(barcode);
        if (!catalogMap[barcodeNumber.toString()])
            return uiNotify("Barcode does not exist.", "error", 8000);

        this.props.addBarcodeOnTop();
        this.focusBarcodeInput();
    }
    /****************************************************/
    componentDidMount() {
        if (this.shelfContainerRef.current)
            this.shelfContainerRef.current.scrollLeft = this.shelfContainerRef.current.scrollWidth;
        this.focusBarcodeInput();
    }
    focusBarcodeInput = () => {
        if (this.barcodeInputRef.current != null)
            this.barcodeInputRef.current.instance.focus();
    }
    deleteBarcode = (shelfId: PlanogramElementId, shelfItemId: PlanogramElementId) => {
        const { storeId, deleteShelfItem } = this.props; //deleteShelfItem
        if (storeId == null) return;
        planogramApi.deleteShelfItem(storeId, shelfItemId).then(() => {
            deleteShelfItem(shelfId, shelfItemId);
            this.setState({
                loading: false,
                barcode: "",
                faces: 1,
                stack: 1,
                row: 1,
                manual_row_only: 0
            });
        }).catch(err => {
            console.error(err);
            this.setState({
                loading: false
            });
            uiNotify("Unable to delete shelf item.", "error", 8000);
        })
    }
    render() {
        const { barcode, catalogMap, shelf, shelfDetails, shelfMap, selectedItem, virtualStore, selectedShelf, selectedGroup, shelfList } = this.props;
        if (!shelf) return null;
        let product = barcode ? catalogMap[barcode] || catalogMap[parseInt(barcode)] : null;
        let items: PlanogramItem[] = [];
        if (shelf && shelfDetails) {
            /* Barak 25.11.20 - update items to reflect only the selected group_id of this Aisle */
            const groupList = virtualStore.sectionGroups.groupList;
            const sectionGroups = groupList.map(v => virtualStore.sectionGroups.groupMap[v]).filter(v => v != null);
            let shelfGroup: any = {};
            for (let i = 0; i < sectionGroups.length; i++) {
                for (let a = 0; a < sectionGroups[i].shelves.length; a++) {
                    shelfGroup[sectionGroups[i].shelves[a]] = sectionGroups[i].group_id;
                }
            }
            let combinedShelves = [shelf].concat(shelfDetails.combined.map((id: any) => shelfMap[id]));
            console.log('1. combinedShelves', JSON.parse(JSON.stringify(combinedShelves)), 'groupId', shelfGroup[combinedShelves[0].id], 'selectedGroup', selectedGroup.toString());
            let tCombinedShelves = [];
            // go over the combined shelves and remove any shelf with a different group_id than selectedGroup based on shelfGroup array 
            for (let i = 0; i < combinedShelves.length; i++) {
                let groupId = shelfGroup[combinedShelves[i].id];
                if ((groupId === undefined && combinedShelves[i + 1] && shelfGroup[combinedShelves[i + 1].id] != selectedGroup.toString()) || groupId === selectedGroup.toString())
                    tCombinedShelves.push(combinedShelves[i]);
            }
            combinedShelves = JSON.parse(JSON.stringify(tCombinedShelves));
            console.log('2. combinedShelves', combinedShelves);
            let combinedItems: any[] = [];
            for (let i = 0; i < combinedShelves.length; i++) {
                // sort shelf items by distFromStart
                combinedShelves[i].items.sort(function (a: any, b: any) {
                    let aNumber = a.placement.distFromStart ? a.placement.distFromStart : 0;
                    let bNumber = b.placement.distFromStart ? b.placement.distFromStart : 0;
                    let aId = a.Id;
                    let bId = b.Id;
                    return (aNumber > bNumber) ? 1 : (aNumber < bNumber) ? -1 : ((aId > bId) ? 1 : (aId < bId) ? -1 : 0);
                });
                for (let a = 0; a < combinedShelves[i].items.length; a++) {
                    combinedItems.push(combinedShelves[i].items[a]);
                }
            }
            items = combinedItems;
            /*****************************************************************************************/
            /* Barak 25.11.20 * items = [shelf.id, ...shelfDetails.combined].map(v => shelfMap[v] ? shelfMap[v].items : []).reduce((p, c) => p.concat(c)); */
            console.log('DocumentStageBarcodeComponent store', virtualStore, 'shelf', shelf, 'shelfDetails', shelfDetails, 'items', items);
        }
        else items = shelf ? shelf.items : [];
        if (product)
            setTimeout(() => {
                if (this.facesInputRef.current)
                    this.facesInputRef.current.instance.focus();
            }, 0);
        /* Barak 6.9.20 */
        let currA = selectedShelf.substring(1, selectedShelf.indexOf('SE'));
        let aisleName = virtualStore.aisleMap[currA].name;
        let shelfNumber = "מדף " + (parseInt(selectedShelf.substring(selectedShelf.indexOf('SH') + 2, selectedShelf.length)) + 1);
        let sectionNumber = "אזור " + selectedGroup;
        /****************/

        /* Barak 23.11.20 - allow to update selectedShelf */
        console.log('PanogramDocument selectedShelf', selectedShelf, 'shelfList', shelfList);
        /**************************************************/

        let pWidth = product ? product.width : 0;
        let pHeight = product ? product.height : 0;
        let pDepth = product ? product.length : 0;

        return (
            <div className="stage-barcode">
                {/* Barak 5.5.20 <h1>סריקת ברקוד</h1> */}
                <h2 style={{ marginBottom: "0.5em" }}>סריקת ברקוד</h2>
                {/* Barak 5.5.20 <h5>גונדולה: {}</h5> */}
                <h5 style={{ marginBottom: "0.5em", marginTop: "0.5em" }}>גונדולה: {/* Barak 6.9.20 */ aisleName + ", " + sectionNumber + ", " + shelfNumber +
                    (shelf && shelf.freezer && shelf.freezer === 1 ? ' (מקרר)' : '')}</h5>
                {/* Barak 5.5.20 <div style={{ marginBottom: "1em" }}> */}
                {/* Barak 23.11.20 - allow to update selectedShelf */}
                <div style={{ display: "flex", flexDirection: "row", justifyContent: "space-between", marginBottom: "0.5em" }}>
                    <Button
                        style={{ padding: "0.05em", float: "left" }}
                        onClick={() => {
                            let index = shelfList ? shelfList.findIndex(line => line === selectedShelf) : -1;
                            if (index >= 0 && index < (shelfList ? shelfList.length : 0) && (shelfList ? shelfList[index + 1] : null)) {
                                this.props.updateSelectedShelf((shelfList ? shelfList[index + 1] : ''));
                            }
                        }}>
                        <FontAwesomeIcon icon={faArrowCircleUp} />
                        <span style={{ marginRight: "0.5em" }}>מדף למעלה</span>
                    </Button>
                    <Button
                        style={{ padding: "0.05em", float: "right" }}
                        onClick={() => {
                            let index = shelfList ? shelfList.findIndex(line => line === selectedShelf) : -1;
                            if (index >= 0 && index < (shelfList ? shelfList.length : 0) && (shelfList ? shelfList[index - 1] : null)) {
                                this.props.updateSelectedShelf((shelfList ? shelfList[index - 1] : ''));
                            }
                        }}>
                        <FontAwesomeIcon icon={faArrowCircleDown} />
                        <span style={{ marginRight: "0.5em" }}>מדף למטה</span>
                    </Button>
                </div>
                {/**************************************************/}
                <div style={{ marginBottom: "0.5em" }}>
                    <div
                        ref={this.shelfContainerRef}
                        className="scroller horizontal"
                        style={{ display: "flex", overflowX: "auto", flexDirection: "row-reverse", paddingBottom: "0.5em" }}>
                        {items ? items.map(item => {
                            return <DocumentShelfItem
                                key={"ITEM_" + item.id}
                                item={item}
                                selectedItem={selectedItem}
                                shelf={shelf}
                                product={catalogMap[item.product]}
                                setState={(state: any) => this.props.setState(state)}
                                deleteBarcode={this.deleteBarcode} />
                        }) : null}
                    </div>
                </div>
                <div className="input-row" style={{ display: "flex" }}>
                    <TextBox
                        tabIndex={1}
                        style={{ flex: 1 }}
                        ref={this.barcodeInputRef}
                        rtlEnabled
                        value={this.props.barcode}
                        // onEnterKey={(e) => {
                        //     if (this.facesInputRef.current)
                        //         this.facesInputRef.current.instance.focus();
                        // }}
                        onFocusIn={highlightOnFocus}
                        onValueChanged={e => {
                            this.props.setState({
                                barcode: e.value,
                                faces: 1,
                                row: 1,
                                stack: 1,
                                position: 0,
                                pWidth: catalogMap[e.value] ? catalogMap[e.value].width : 0,
                                pHeight: catalogMap[e.value] ? catalogMap[e.value].height : 0,
                                pDepth: catalogMap[e.value] ? catalogMap[e.value].length : 0,
                                selectedItem: "",
                            });
                            this.setState({
                                faces: 1,
                                row: 1,
                                stack: 1,
                                position: 0,
                                pWidth: catalogMap[e.value] ? catalogMap[e.value].width : 0,
                                pHeight: catalogMap[e.value] ? catalogMap[e.value].height : 0,
                                pDepth: catalogMap[e.value] ? catalogMap[e.value].length : 0,
                                selectedItem: "",
                            })
                        }}
                        // valueChangeEvent="keyup"
                        className="row-input"
                        placeholder="נא לסרוק ברקוד..." />
                    <Button
                        className="row-input">
                        <FontAwesomeIcon icon={faSearch} />
                    </Button>
                </div>
                {product != null ? <div className="barcode-data" style={{ borderTop: "1px solid #dedede" }}>
                    {/* Barak 28.4.20 Remove
                    <div style={{ float: "left", width: "60px" }}> 
                        <BarcodeImage barcode={product.BarCode}
                    */}
                    {/* Barak 28.4.20 Add */}
                    <div style={{ float: "left" }}>
                        <BarcodeImage barcode={product.BarCode} style={{ maxHeight: "70px", width: "auto", paddingLeft: "0.5em", paddingTop: "0.2em" }} />
                    </div>
                    {/* Barak 28.4.20 Remove
                    <h3 style={{ marginBottom: "0.2em" }}>{product != null ? product.BarCode + " - " + product.Name : barcode}</h3>
                    */}
                    {/* Barak 28.4.20 Add */}
                    <h4 style={{ marginBottom: "0.2em", marginTop: "0.5em" }}>{product != null ? product.BarCode : barcode}</h4>
                    <h4 style={{ marginBottom: "0.2em", marginTop: "0.2em", maxWidth: "10em", overflow: "hidden", whiteSpace: "nowrap", textOverflow: "ellipsis" }}>
                        {product != null ? product.Name : null}</h4>
                    {product.Archives != null && product.Archives == 1 ?
                        <div style={{
                            width: "100%",
                            color: "#fafafa",
                            background: "red",
                            padding: "0.5em"
                        }}>מוצר ארכיון!</div>
                        : null}
                    <div style={{ display: "flex", width: "100%", marginBottom: "0.2em" }}>
                        <div style={{ flex: 1, }}>
                            <div>פייסים</div>
                            <NumberBox
                                id="numberBox1"
                                ref={this.facesInputRef}
                                onFocusIn={highlightOnFocus}
                                tabIndex={2}
                                rtlEnabled
                                min={1}
                                max={80}
                                step={1}
                                width="100%"
                                onValueChanged={(e) => {
                                    if ( /* osher 30.08.20 - e.value > 20  change to -*/ e.value > 80 || e.value <= 0)
                                        return uiNotify("כמות פייסים לא תקנית.");
                                    // this.props.setState({ faces: e.value })
                                    // this.localFaces = e.value;
                                    // console.log('this.localFaces',this.localFaces);
                                    /* Barak 28.4.20 Add *
                                    let x = document.getElementById("numberBox2");
                                    if (x) x.focus();
                                    * ***************** */
                                    // this.setState({ faces: e.value })
                                }}
                                defaultValue={this.props.faces}
                                // value={this.localFaces > 0 ? this.localFaces : this.props.faces}
                                // value={this.state.faces}
                                className="row-input" />
                        </div>
                        <div style={{ flex: 1, }}>
                            <div>ערימה</div>
                            <NumberBox
                                id="numberBox2"
                                onFocusIn={highlightOnFocus}
                                tabIndex={3}
                                rtlEnabled
                                min={1}
                                max={80}
                                step={1}
                                width="100%"
                                onValueChanged={(e) => {
                                    if (e.value > 80 || e.value <= 0)
                                        return uiNotify("כמות ערימה לא תקנית.");
                                    // this.props.setState({ stack: e.value })
                                    // this.localStack = e.value;
                                    /* Barak 28.4.20 Add *
                                    let x = document.getElementById("button1");
                                    if (x) x.focus();
                                    * ***************** */
                                    // this.setState({ stack: e.value })
                                }}
                                defaultValue={this.props.stack}
                                // value={this.localStack > 0 ? this.localStack : this.props.stack}
                                // value={this.state.stack}
                                className="row-input" />
                        </div>
                        {/* osher - 31.08.20 - adding Row value to (+) menu when choosing a specific */}
                        <div style={{ flex: 1, }}>
                            <div>שורות</div>
                            <NumberBox
                                id="numberBox3"
                                onFocusIn={highlightOnFocus}
                                tabIndex={4}
                                rtlEnabled
                                min={1}
                                max={80}
                                step={1}
                                width="100%"
                                onValueChanged={(e) => {
                                    if (e.value > 80 || e.value <= 0)
                                        return uiNotify("כמות שורות לא תקנית.");
                                    // this.props.setState({ row: e.value })
                                    // this.setState({ row: e.value })
                                }}

                                defaultValue={this.props.row}
                                // value={this.localStack > 0 ? this.localStack : this.props.stack}
                                // value={this.state.row}
                                className="row-input" />
                        </div>
                        {/**************************************************************************/}
                        <div style={{ flex: 1, }}>
                            <div>{selectedItem ? "עדכן" : "הוסף"}</div>
                            <Button
                                id="button1"
                                tabIndex={5}
                                useSubmitBehavior={true}
                                rtlEnabled
                                style={{ width: "100%", padding: "2px" }}
                                disabled={product == null}
                                className="row-input"
                                /* osher - 06.09.20 * onClick={this.insertBarcode} */
                                /* osher - 06.09.20 - setting the state not on value change but on click, fixes the bug of jumping between fields */
                                onClick={async (e) => {
                                    let nb1 = document.getElementById("numberBox1");
                                    let nb2 = document.getElementById("numberBox2");
                                    let nb3 = document.getElementById("numberBox3");
                                    let faces;
                                    let stack;
                                    let row;

                                    if (nb1) faces = nb1.getElementsByTagName("input");
                                    if (nb2) stack = nb2.getElementsByTagName("input");
                                    if (nb3) row = nb3.getElementsByTagName("input");

                                    console.log('position', this.state.position);

                                    await this.props.setState({
                                        faces: faces && faces[0] ? parseInt(faces[0].value) : this.props.faces,
                                        originalStack: this.props.stack,
                                        stack: stack && stack[0] ? parseInt(stack[0].value) : this.props.stack,
                                        row: row && row[0] ? parseInt(row[0].value) : this.props.row,
                                        position: this.state.position,
                                        pWidth: pWidth,
                                        pHeight: pHeight,
                                        pDepth: pDepth,
                                    });
                                    // this.setState({
                                    //     faces: faces && faces[0] ? parseInt(faces[0].value) : this.props.faces,
                                    //     stack: stack && stack[0] ? parseInt(stack[0].value) : this.props.stack,
                                    //     row: row && row[0] ? parseInt(row[0].value) : this.props.row,
                                    // });

                                    console.log('state update', this.props.faces, this.props.stack, this.props.row, this.props.position);

                                    this.insertBarcode();
                                }}
                            /**************************************************************************/
                            >
                                <FontAwesomeIcon icon={selectedItem ? faEdit : faCheck} />
                                {/* <span style={{ marginRight: "1em" }}>{selectedItem ? "עדכן ברקוד קיים" : "הוסף ברקוד"}</span> */}
                            </Button>
                        </div>
                        {/*Barak 26.11.20 - adding freezer add ontop button*/}
                        {shelf.freezer === 1 && !selectedItem
                            ? <div style={{ flex: 1, }}>
                                <div>{"הוסף למעלה"}</div>
                                <Button
                                    id="button2"
                                    tabIndex={6}
                                    useSubmitBehavior={true}
                                    rtlEnabled
                                    style={{ width: "100%", padding: "2px", border: '1px solid #2b998a' }}
                                    disabled={product == null}
                                    className="row-input"
                                    /* osher - 06.09.20 * onClick={this.insertBarcode} */
                                    /* osher - 06.09.20 - setting the state not on value change but on click, fixes the bug of jumping between fields */
                                    onClick={async (e) => {
                                        let nb1 = document.getElementById("numberBox1");
                                        let nb2 = document.getElementById("numberBox2");
                                        let nb3 = document.getElementById("numberBox3");
                                        let faces;
                                        let stack;
                                        let row;

                                        if (nb1) faces = nb1.getElementsByTagName("input");
                                        if (nb2) stack = nb2.getElementsByTagName("input");
                                        if (nb3) row = nb3.getElementsByTagName("input");

                                        console.log('position', this.state.position);

                                        await this.props.setState({
                                            faces: faces && faces[0] ? parseInt(faces[0].value) : this.props.faces,
                                            stack: stack && stack[0] ? parseInt(stack[0].value) : this.props.stack,
                                            row: row && row[0] ? parseInt(row[0].value) : this.props.row,
                                            position: this.state.position,
                                            pWidth: pWidth,
                                            pHeight: pHeight,
                                            pDepth: pDepth,
                                        });

                                        console.log('state update', this.props.faces, this.props.stack, this.props.row, this.props.position);

                                        this.insertBarcodeOnTop();
                                    }}
                                /**************************************************************************/
                                >
                                    <FontAwesomeIcon icon={faArrowUp} />
                                </Button>
                            </div>
                            : null}
                        {/**************************************************/}
                    </div>
                    {/* Barak 4.11.20 - Add position to the entered product */}
                    <div style={{ display: "flex", width: "100%", flexDirection: 'column', marginBottom: "0.2em" }}>
                        <div style={{ marginTop: "3px", marginBottom: "6px", padding: "3px", border: "1px solid #888", /*fontWeight: "bold",*/ textAlign: "center", color: "#888" }}>
                            Original - width: {this.props.pWidth}, height: {this.props.pHeight}, depth: {this.props.pDepth}
                        </div>

                        <div className="input-row" style={{ border: "none" }}>
                            <label htmlFor="position">פוזיציה: </label>
                            <select
                                id="selectPosition"
                                name="position"
                                tabIndex={6}
                                // defaultValue={JSON.stringify(this.state.position)}
                                value={JSON.stringify(this.state.position)}
                                onChange={(e) => {
                                    let width, height, depth;
                                    let myString = document.getElementById("afterString") as HTMLElement;
                                    console.log("osher string", myString.innerHTML);

                                    switch (e.target.value) {
                                        case "0": {
                                            width = this.props.pWidth; height = this.props.pHeight; depth = this.props.pDepth;
                                            myString.innerHTML = `New - width: ${width}, height: ${height}, depth: ${depth}`;
                                            break;
                                        }
                                        case "1": {
                                            width = this.props.pHeight; height = this.props.pWidth; depth = this.props.pDepth;
                                            myString.innerHTML = `New - width: ${width}, height: ${height}, depth: ${depth}`;
                                            break;
                                        }
                                        case "2": {
                                            width = this.props.pWidth; height = this.props.pDepth; depth = this.props.pHeight;
                                            myString.innerHTML = `New - width: ${width}, height: ${height}, depth: ${depth}`;
                                            break;
                                        }
                                        case "3": {
                                            width = this.props.pHeight; height = this.props.pDepth; depth = this.props.pWidth;
                                            myString.innerHTML = `New - width: ${width}, height: ${height}, depth: ${depth}`;
                                            break;
                                        }
                                        case "4": {
                                            width = this.props.pDepth; height = this.props.pHeight; depth = this.props.pWidth;
                                            myString.innerHTML = `New - width: ${width}, height: ${height}, depth: ${depth}`;
                                            break;
                                        }
                                        case "5": {
                                            width = this.props.pDepth; height = this.props.pWidth; depth = this.props.pHeight;
                                            myString.innerHTML = `New - width: ${width}, height: ${height}, depth: ${depth}`;
                                            break;
                                        }
                                        default: {
                                            width = this.props.pWidth; height = this.props.pHeight; depth = this.props.pDepth;
                                            myString.innerHTML = `New - width: ${width}, height: ${height}, depth: ${depth}`;
                                        }
                                    }
                                    this.setState({ position: parseInt(e.target.value), pWidth: width, pHeight: height, pDepth: depth });
                                    pWidth = width;
                                    pHeight = height;
                                    pDepth = depth;
                                }}
                            >
                                <option value="0">פוזיציה 1</option>
                                <option value="1">פוזיציה 2</option>
                                <option value="2">פוזיציה 3</option>
                                <option value="3">פוזיציה 4</option>
                                <option value="4">פוזיציה 5</option>
                                <option value="5">פוזיציה 6</option>
                            </select>
                        </div>
                        <div id="afterString" style={{ margin: "5px", padding: "3px", border: "1px solid #888", /*fontWeight: "bold",*/ textAlign: "center", color: "#888" }}>
                            New - width: {pWidth}, height: {pHeight}, depth: {pDepth}
                        </div>
                    </div>
                    {/*******************************************************/}

                </div> : null}
                <div className="input-row">

                </div>
            </div>
        )
    }
}
const DocumentStageBarcode = connect(barcodeMapStateToProps, barcodeMapDispatchToProps)(DocumentStageBarcodeComponent);


type DocumentShelfItemProps = {
    item: PlanogramItem,
    shelf: PlanogramShelf,
    product?: CatalogProduct,
    selectedItem?: string,
    setState: Function,
    deleteBarcode: (shelfId: PlanogramElementId, itemId: PlanogramElementId) => void
};
class DocumentShelfItem extends React.Component<DocumentShelfItemProps> {
    pressTimer: NodeJS.Timeout | null = null;
    holdBegin = (e: any) => {
        if (e.type === "click" && e.button !== 0)
            return;
        this.pressTimer = setTimeout(() => {
            const { item, shelf, deleteBarcode } = this.props;
            if (!shelf || !window.confirm("Are you sure you want to delete?"))
                return;
            deleteBarcode(shelf.id, item.id);
        }, 1500);
    }
    holdEnd = (e: any) => {
        if (this.pressTimer) {
            clearTimeout(this.pressTimer);
            this.pressTimer = null;
        }
    }
    onClick = (e: any) => {
        this.holdEnd(e);
        const { item, selectedItem, product } = this.props;
        if (selectedItem === item.id) {
            console.log('selectedItem === item.id', 'item', item, 'selectedItem', selectedItem, 'product', product);
            this.props.setState({
                selectedItem: "",
            })
        }
        else {
            console.log('selectedItem != item.id', 'item', item, 'selectedItem', selectedItem, 'product', product);
            this.props.setState({
                selectedItem: item.id,
                barcode: item.product + "",
                ...item.placement
            })
        }
    }
    shouldComponentUpdate(nextProps: DocumentShelfItemProps) {
        if (nextProps.selectedItem !== this.props.selectedItem)
            return true;
        if (nextProps.item.product !== this.props.item.product)
            return true;
        if (nextProps.item.placement.faces !== this.props.item.placement.faces
            || nextProps.item.placement.stack !== this.props.item.placement.stack)
            return true;
        return false;
    }
    render() {
        const { item, shelf } = this.props;
        const { selectedItem, product } = this.props;
        return (<div
            onMouseDown={this.holdBegin}
            onTouchStart={this.holdBegin}
            onMouseOut={this.holdEnd}
            onTouchEnd={this.holdEnd}
            onTouchCancel={this.holdEnd}
            onClick={this.onClick}
            key={item.id}
            className="noselect"
            style={{
                maxWidth: "100px",
                display: "flex",
                flexFlow: "column",
                alignContent: "center",
                alignItems: "center",
                fontSize: "0.8em",
                // border: "1px solid #dadada",
                border: shelf.freezer && shelf.freezer === 1 && item.placement.distFromTop != (shelf.dimensions.height - ((item.placement.pHeight ? item.placement.pHeight : ProductDefaultDimensions.height) * item.placement.stack)) ? "1px solid #2b998a" : "1px solid #dadada",
                color: selectedItem === item.id ? "#ECF0F1" : "inherit",
                background: selectedItem === item.id ? "#273a48" : "none"
            }}>
            <div style={{ padding: "0.2em", fontSize: "1em", fontWeight: "bold" }}>{product ? product.BarCode : item.product}</div>
            <div style={{ display: "flex" }}>
                <div className="input-row" style={{ flex: 1, display: "flex", padding: "0.3em", width: "100%", justifyContent: "space-between" }}>
                    <div style={{ fontSize: "0.7em" }}>פייסים</div>
                    <div style={{ fontWeight: "bold" }}>{item.placement.faces}</div>
                </div>
                <div className="input-row" style={{ flex: 1, display: "flex", padding: "0.3em", width: "100%", justifyContent: "space-between" }}>
                    <div style={{ fontSize: "0.7em" }}>ערימה</div>
                    <div style={{ fontWeight: "bold" }}>{item.placement.stack}</div>
                </div>
            </div>
            {/*<div style={{ width: "60px", height: "60px" }}>*/}
            <div style={{ maxWidth: "30px", maxHeight: "30px" }}>
                {/* <BarcodeImage barcode={item.product} style={{ maxHeight: "60px", width: "auto" }} /> */}
                <BarcodeImage barcode={item.product} style={{ maxHeight: "30px", width: "auto" }} />
            </div>
        </div>
        )
    }
}


export const PlanogramDocument = withRouter(connect(mapStateToProps, mapDispatchToProps)(PlanogramDocumentComponent));