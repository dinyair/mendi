import * as React from "react"
import { connect } from 'react-redux'
import { DateBox, SelectBox, Button, TagBox, DataGrid } from 'devextreme-react'
import { Column, Texts, Sorting, Scrolling, HeaderFilter, FilterRow, Export, Summary, TotalItem, GroupPanel, ColumnChooser, LoadPanel, Paging, Pager } from 'devextreme-react/data-grid'
import DataSource from 'devextreme/data/data_source'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { AnyAction, bindActionCreators } from 'redux'
import { ThunkDispatch } from 'redux-thunk'
import { Rnd } from 'react-rnd';

import { AppState } from '@src/planogram/shared/store/app.reducer'
import { fetchCatalogSales, CatalogSaleRecord, fetchTruma, fetchTrumaSnif, setUserReportState, getUserReportState, fetchLayoutSales } from '@src/planogram/shared/api/sales.provider';
import { faEye, faFilter, faWindowClose } from '@fortawesome/free-solid-svg-icons';
import { uiNotify } from '@src/planogram/shared/components/Toast';
import { fetchBarcodeStatuses } from '@src/planogram/shared/api/catalog.provider';
import { setBarcodeStatuses } from '@src/planogram/shared/store/catalog/catalog.action';
import { hideSalesReport } from '@src/planogram/shared/store/planogram/display/display.actions';
import { fetchBranches } from '@src/planogram/shared/api/settings.provider';
import * as System from '@src/planogram/shared/interfaces/models/System';
import * as systemApi from '@src/planogram/shared/api/settings.provider';

import * as moment from 'moment';
import { Branch } from '@src/planogram/shared/interfaces/models/System'
import { setBranches, setStores } from '@src/planogram/shared/store/system/data/data.actions'
import { PlanogramStoreRecord } from '@src/planogram/shared/api/planogram.provider'
// import { fetchStores } from '@src/shared/api/planogram.provider'

type ReportSaleRecord = CatalogSaleRecord & {
    InStore: boolean,
    /*Barak 13.1.20*/ Name: string
    /*Barak 23.1.20*/ PercentOfSubGroupSaleInBranch: number,
    /*Barak 23.1.20*/ PercentOfOfSubGroupSaleInNetwork: number,
};

const mapStateToProps = (state: AppState, ownProps: any) => ({
    user: state.auth.user,
    products: state.catalog.products,
    productsMap: state.newPlano.productsMap,
    subGroups: state.system.data.subGroups,
    branches: state.system.data.branches,
    stores: state.system.data.stores,
})

const mapDispatchToProps = (dispatch: ThunkDispatch<AppState, {}, AnyAction>) => ({

    fetchBarcodeStatuses: () => fetchBarcodeStatuses()
        .then(statuses => dispatch(setBarcodeStatuses(statuses)))
        .catch((err) => {
            uiNotify("Unable to load network barcode statuses")
        }),
    setBranches: (branches: System.Branch[]) => dispatch(setBranches(branches)),
    setStores: (stores: PlanogramStoreRecord[]) => dispatch(setStores(stores))
})

/* Barak 23.1.20 */ let Const_Truma_Records: any[] = [];

type PlanogramLayoutReportProps = ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>
type PlanogramLayoutReportState = {
    loading: boolean,
    loadingTxt: boolean,
    branches: Branch[],
    storeList: PlanogramStoreRecord[],
    filteredStoreList: PlanogramStoreRecord[],
    pageIndex: number,
    pageSize: number,
    salesRecords: any[],
    searchWord: string,
    selectedBranch: number,
    selectedStore: number,
    fromDate: Date,
    toDate: Date,
    height: number,
    width: number,
    columnChooserVisible: boolean
};

// }
class PlanogramLayoutReport extends React.Component<PlanogramLayoutReportProps, PlanogramLayoutReportState> {
    dataGrid: any | null = null;
    state: PlanogramLayoutReportState = {
        loading: false,
        loadingTxt: false,
        branches: [],
        storeList: [],
        filteredStoreList: [],
        pageIndex: 0,
        pageSize: 50,
        searchWord: "",
        fromDate: new Date(),
        toDate: new Date(),
        selectedBranch: 0,
        selectedStore: 0,
        // width: window.innerWidth * 0.5656,
        width: window.innerWidth * 0.8,
        height: window.innerHeight * 0.5656,
        salesRecords: [],
        columnChooserVisible: false
    }

    loadAllBranches = async (): Promise<void> => {
        if (this.props.branches.length === 0)
            await systemApi.fetchBranches().then(async (branches) => {
                console.log('PlanogramLayoutReport fetchBranches', branches);
                await this.props.setBranches(branches);
                await this.setState({ branches });
            }).catch((err) => {
                console.error(err);
                uiNotify("Unable to load branches");
            });
        else await this.setState({ branches: this.props.branches });
        console.log("loadAllBranches this.state.branches", this.state.branches)
    }

    loadAllStores = async (): Promise<void> => {
        if (this.props.stores.length === 0)
            await systemApi.fetchStores().then(async (stores) => {
                console.log('PlanogramLayoutReport fetchStores', stores);
                await this.props.setStores(stores);
                await this.setState({ storeList: stores, /*filteredStoreList: stores*/ });
            }).catch((err) => {
                console.error(err);
                uiNotify("Unable to load stores");
            });
        else await this.setState({ storeList: this.props.stores });
        console.log("loadAllStores this.state.stores", this.props.branches)
    }

    loadTruma = async (): Promise<void> => {
        await fetchTruma().then((trumaRecords) => {
            Const_Truma_Records = trumaRecords;
        }).catch((err) => {
            console.error(err);
            uiNotify("Unable to load truma table", "error");
        })
    }

    init = async () => {
        this.setState({ loading: true });
        console.log('PlanogramLayoutReport before init time', new Date().getHours(), ':', new Date().getMinutes(), ':', new Date().getSeconds());
        await this.loadAllBranches();
        await this.loadAllStores();
        await this.loadTruma();
        console.log('PlanogramLayoutReport after init time', new Date().getHours(), ':', new Date().getMinutes(), ':', new Date().getSeconds());
        this.setState({ loading: false });
    }

    componentDidMount = async () => {
        this.setState({ loading: true });

        this.init().then(() => {
            this.setState({ loading: false });
        }).catch(err => {
            this.setState({ loading: false });
            uiNotify(err, 'error');
        })
    }

    // componentWillReceiveProps(nextProps: any) {
    //     if(this.state.branches.length === 0 || this.state.storeList.length === 0) this.init();
    // }

    onToolbarPreparing = (e: any) => {
        console.log('onToolbarPreparing this.props.branches', this.state.branches);

        e.toolbarOptions.items.unshift(
            {
                location: 'after',
                widget: 'dxButton',
                visible: false,
                options: {
                    icon: 'refresh',
                    onClick: this.refreshDataGrid.bind(this)
                }
            },
            {
                location: 'after',
                widget: 'dxButton',
                options: {
                    icon: 'refresh',
                    onClick: this.refreshDataGrid.bind(this)
                }
            },
            {
                location: 'before',
                widget: 'dxDateBox',
                rtlEnabled: true,
                options: {
                    // searchEnabled: true,
                    // showClearButton: true,
                    placeholder: "מתאריך",
                    displayFormat: "dd/MM/yyyy",
                    width: 150,
                    max: new Date(),
                    value: this.state.fromDate,
                    editEnabled: false,
                    onValueChanged: (e: any) => this.setState({ fromDate: e.value })
                }
            },
            {
                location: 'before',
                widget: 'dxDateBox',
                rtlEnabled: true,
                options: {
                    // searchEnabled: true,
                    // showClearButton: true,
                    placeholder: "עד תאריך",
                    displayFormat: "dd/MM/yyyy",
                    width: 150,
                    max: new Date(),
                    value: this.state.toDate,
                    editEnabled: false,
                    onValueChanged: (e: any) => this.setState({ toDate: e.value })
                }
            },
            {
                location: 'before',
                widget: 'dxSelectBox',
                //locateInMenu: 'auto',
                rtlEnabled: true,
                options: {
                    searchEnabled: true,
                    showClearButton: true,
                    placeholder: "סניף",
                    dataSource: this.state.branches,
                    displayExpr: "Name",
                    valueExpr: "BranchId",
                    value: this.state.selectedBranch,
                    onValueChanged: this.selectBranchChanged.bind(this)
                }
            },
            {
                location: 'before',
                widget: 'dxSelectBox',
                //locateInMenu: 'auto',
                rtlEnabled: true,
                options: {
                    searchEnabled: true,
                    showClearButton: true,
                    placeholder: "חנות",
                    // dataSource: this.state.filteredStoreList,
                    dataSource: {
                        store: this.state.storeList,
                        filter: ['branch_id', '=', this.state.selectedBranch]
                    },
                    displayExpr: "name",
                    valueExpr: "id",
                    value: this.state.selectedStore,
                    onValueChanged: this.selectStoreChanged.bind(this)
                }
            },
            {
                widget: 'dxButton',
                location: "before",
                options: {
                    text: 'בצע',
                    type: 'normal',
                    onClick: this.reloadAllReport
                }
            }
        );
    }

    selectBranchChanged = async (e: any) => {
        this.setState({ loading: true });
        await this.setState({
            selectedBranch: e.value,
            filteredStoreList: this.state.storeList.filter(line => line.branch_id === parseInt(e.value)),
            salesRecords: []
        });
        this.setState({ loading: false });
        console.log('selectBranchChanged filteredStoreList', this.state.selectedBranch);
    }

    selectStoreChanged(e: any) {
        this.setState({ selectedStore: e.value, salesRecords: [] })
    }

    reloadAllReport = async () => {
        if (this.state.selectedBranch === 0) {
            uiNotify("חובה לבחור סניף", "error");
            return;
        }
        if (this.state.selectedStore === 0) {
            uiNotify("חובה לבחור חנות", "error");
            return;
        }

        let today = new Date();
        let tomorrow = new Date();

        let fromDate;
        let toDate;
        if (this.state.fromDate == null) fromDate = today;
        else fromDate = this.state.fromDate;
        if (this.state.toDate == null) toDate = today;
        else toDate = this.state.toDate;

        this.setState({ loadingTxt: true });
        let salesRecords: any[] = await fetchLayoutSales({
            beginDate: fromDate,
            endDate: toDate,
            branchId: this.state.selectedBranch,
            storeId: this.state.selectedStore,
        });
        this.setState({ salesRecords });
        this.setState({ loadingTxt: false });
    }

    xlsOutput = (e: any) => {
        // Export XLSX file with errors:
        // https://github.com/SheetJS/sheetjs/issues/817

        const { salesRecords } = this.state;
        let filteredRecords: any[] = salesRecords;

        let length = salesRecords.length;
        let reportJSON = [];
        for (let i = 0; i < length; i++) {
            let object = {
                LayoutId: filteredRecords[i].LayoutId,
                LayoutName: filteredRecords[i].LayoutName.replace('"', "''"),
                TotalSections: filteredRecords[i].TotalSections,
                TotalShelves: filteredRecords[i].TotalShelves,
                TotalArea: filteredRecords[i].TotalArea,
                AreaPercent: filteredRecords[i].AreaPercent.toFixed(2),
                AmountSold_network: filteredRecords[i].TotalAmountSold_network,
                AmountSoldPercent_network: filteredRecords[i].TotalAmountSoldPercent_network.toFixed(2),
                PriceSold_network: filteredRecords[i].TotalPriceSold_network.toFixed(0),
                PriceSoldPercent_network: filteredRecords[i].TotalPriceSoldPercent_network.toFixed(2),
                AvgResult_network: filteredRecords[i].AvgResult_network.toFixed(2),
                AmountSold: filteredRecords[i].TotalAmountSold,
                AmountSoldPercent: filteredRecords[i].TotalAmountSoldPercent.toFixed(2),
                PriceSold: filteredRecords[i].TotalPriceSold.toFixed(0),
                PriceSoldPercent: filteredRecords[i].TotalPriceSoldPercent.toFixed(2),
                AvgResult: filteredRecords[i].AvgResult.toFixed(2)
            }
            reportJSON.push(object);
        }


        // https://www.npmjs.com/package/json2csv
        const { parse } = require('json2csv');
        let fields = ['LayoutId', 'LayoutName', 'TotalSections', 'TotalShelves', 'TotalArea', 'AreaPercent', 'AmountSold_network', 'AmountSoldPercent_network',
            'PriceSold_network', 'PriceSoldPercent_network', 'AvgResult_network', 'AmountSold', 'AmountSoldPercent', 'PriceSold',
            'PriceSoldPercent', 'AvgResult'];

        const withBOM = true;
        const excelStrings = true;
        const opts = { fields, excelStrings, withBOM };
        try {
            const fileData = parse(reportJSON, opts);
            const blob = new Blob([fileData], { type: "text/plain" });
            const url = URL.createObjectURL(blob);
            const link = document.createElement('a');
            // link.download = 'filename.json';
            link.download = "Category_Report.csv";
            link.href = url;
            link.click();
        } catch (err) {
            console.error(err);
            uiNotify(err, 'error');
        }
    }

    refreshDataGrid() {
        // this.dataGrid.clearFilter();
        // this.reloadAllDepartments();

        this.setState({ loading: true })
        this.reloadAllReport().then(() => {
            this.setState({ loading: false })
        }).catch(err => {
            this.setState({ loading: false })
            uiNotify(err, 'error');
        })
    }

    // bindMouse = () => {
    //     window.addEventListener('mousedown', e => {  
    //         let columnChooser = that.closest(e.target, ".dx-datagrid-column-chooser");  

    //         if (!grid && !columnChooser)  
    //             that.dataGrid.instance.hideColumnChooser();  
    //     });  
    // }
    // unBindMouse = () => {
    //     window.removeEventListener('mousedown', this.handleKeyboard);
    // }  

    render() {
        const { salesRecords } = this.state;
        let filteredRecords = salesRecords;

        if (this.state.loading || this.state.branches.length === 0 || this.state.storeList.length === 0)
            return (
                <div className="app-loader">
                    <div className="loader" />
                </div>
            );

        return (
            <div className='grid-wrapper'
            // onMouseOver={(e) => {
            //     this.bindMouse();
            // }}
            // onMouseOut={(e) => {
            //     // hideProductDisplayerBarcode();
            //     this.unBindMouse();
            // }}
            >
                <div className='grid-header'>
                    <div className='container-wide'>
                        <div className='header-text' style={{ color: "red" }}>{this.state.loadingTxt
                            ? "נא להמתין, מחשב נתונים..."
                            : null}</div>
                    </div>
                </div>
                <div className='grid-body'>
                    <div className='container-wide3'>
                        <div className="report-toolbar">
                            <div className="report-toolbar-container"
                                style={{ display: 'flex', backgroundColor: '#073b4c', padding: '8px', marginLeft: '1em', marginRight: '1em', marginBottom: '8px' }}>
                                <div style={{ display: 'flex', justifyContent: 'flext-end', flexGrow: 1 }}>
                                    <DateBox
                                        rtlEnabled
                                        className="toolbar-item"
                                        style={{ marginLeft: '5px' }}
                                        placeholder="מתאריך"
                                        value={this.state.fromDate}
                                        max={new Date()}
                                        displayFormat="dd/MM/yy"
                                        pickerType={"calendar"}
                                        onValueChanged={(e) => this.setState({ fromDate: e.value })} />
                                    <DateBox
                                        rtlEnabled
                                        className="toolbar-item"
                                        style={{ marginLeft: '5px' }}
                                        placeholder="עד תאריך"
                                        value={this.state.toDate}
                                        max={new Date()}
                                        displayFormat="dd/MM/yy"
                                        pickerType={"calendar"}
                                        onValueChanged={(e) => this.setState({ toDate: e.value })} />
                                    <SelectBox
                                        rtlEnabled
                                        showClearButton
                                        className="toolbar-item"
                                        style={{ marginLeft: '5px' }}
                                        placeholder="סניף"
                                        value={this.state.selectedBranch}
                                        valueExpr="BranchId"
                                        displayExpr="Name"
                                        searchEnabled={true}
                                        searchExpr={["Name", "BranchId"]}
                                        dataSource={new DataSource({
                                            store: this.state.branches,
                                            pageSize: 50
                                        })}
                                        onValueChanged={async (e) => {
                                            this.setState({ loading: true });
                                            await this.setState({
                                                selectedBranch: e.value,
                                                filteredStoreList: this.state.storeList.filter(line => line.branch_id === parseInt(e.value)),
                                                salesRecords: []
                                            });
                                            this.setState({ loading: false });
                                        }} />
                                    <SelectBox
                                        rtlEnabled
                                        showClearButton
                                        className="toolbar-item"
                                        style={{ marginLeft: '5px' }}
                                        placeholder="חנות"
                                        value={this.state.selectedStore}
                                        valueExpr="id"
                                        displayExpr="name"
                                        searchEnabled={true}
                                        searchExpr={["name", "id"]}
                                        dataSource={new DataSource({
                                            store: this.state.storeList,
                                            filter: ['branch_id', '=', this.state.selectedBranch],
                                            pageSize: 50
                                        })}
                                        onValueChanged={(e) => {
                                            this.setState({ selectedStore: e.value, salesRecords: [] })
                                        }} />
                                    <Button
                                        rtlEnabled
                                        onClick={(e) => this.reloadAllReport()}
                                        className="toolbar-item"
                                        text="בצע" />
                                </div>
                                <div>
                                    <Button
                                        rtlEnabled
                                        style={{ marginLeft: '5px' }}
                                        onClick={(e) => {
                                            this.setState({ loading: true })
                                            this.reloadAllReport().then(() => {
                                                this.setState({ loading: false })
                                            }).catch(err => {
                                                this.setState({ loading: false })
                                                uiNotify(err, 'error');
                                            })
                                        }}
                                        className="toolbar-item-small"
                                        icon="refresh" />
                                    <Button
                                        rtlEnabled
                                        // style={{ marginLeft: '5px' }}
                                        onClick={(e) => {
                                            this.xlsOutput(e);
                                        }}
                                        className="toolbar-item-small"
                                        icon="exportxlsx" />
                                    {/* <Button
                                        rtlEnabled
                                        onClick={(e) => {
                                            // if(this.state.columnChooserVisible){
                                            //     this.dataGrid.hideColumnChooser();
                                            //     this.setState({columnChooserVisible:false});
                                            // }else{
                                            this.dataGrid.showColumnChooser();
                                            //     this.setState({columnChooserVisible:true});
                                            // }
                                        }}
                                        className="toolbar-item-small"
                                        // icon={faEye}
                                        icon="columnchooser"
                                        /> */}
                                </div>
                            </div>
                        </div>
                        <DataGrid id={'gridContainer'}
                            onInitialized={(ref) => {
                                this.dataGrid = ref.component;
                                // this.dataGrid.hideColumnChooser(); 
                            }}
                            dataSource={new DataSource({
                                store: filteredRecords,
                            })}
                            showColumnLines={true}
                            showRowLines={true}
                            showBorders={true}
                            rtlEnabled={true}

                            //className="grid-element"
                            width={'100%'} //Hardcoded
                            repaintChangesOnly={true}
                            rowAlternationEnabled={true}
                            // height={"90%"}
                            className="grid-element"
                            // onToolbarPreparing={this.onToolbarPreparing}

                            // allow to save the state of the dataGrid including the columnChooser in localStorage / database table planogram_pattern
                            // based on https://supportcenter.devexpress.com/ticket/details/t550068/datagrid-is-it-possible-to-store-the-selection-of-the-columnchooser
                            stateStoring={{
                                enabled: true,
                                type: "custom",
                                customLoad: async () => {
                                    let state = await getUserReportState('PlanogramLayoutReport', this.props.user ? this.props.user.id : 0);
                                    if (state && state != '{}') {
                                        // remove any filterValue from the retrieved columns
                                        let data = JSON.parse(state);
                                        for(let a=0;a<data.columns.length;a++){
                                            delete data.columns[a].filterValue;
                                        }
                                        console.log('getUserReportState PlanogramLayoutReport', data);
                                        // return JSON.parse(state);
                                        return data;
                                    }
                                    else return {};
                                },
                                customSave: (state) => {
                                    setUserReportState('PlanogramLayoutReport', this.props.user ? this.props.user.id : 0, JSON.stringify(state));
                                },
                            }}
                        >
                            {/* ColumnChooser - https://js.devexpress.com/Demos/WidgetsGallery/Demo/TreeList/ColumnChooser/React/Light/ */}
                            <ColumnChooser enabled={true} allowSearch={true} mode={'select'} />

                            <FilterRow
                                visible={true}
                                applyFilter={'auto'}
                                showAllText={''}
                            ></FilterRow>
                            <HeaderFilter visible={true} />
                            <LoadPanel enabled={true} />


                            <Scrolling mode={'virtual'}></Scrolling>
                            {/* <Paging enabled={true} /> */}
                            <Paging defaultPageSize={100} />
                            <Pager
                                showPageSizeSelector={true}
                                allowedPageSizes={[50, 100, 200]}
                                showInfo={true}
                                infoText={"עמוד {0} מתוך {1} ({2} פריטים)"} />
                            {/* <Export enabled={true} fileName={'layouts'} /> */}

                            <Column dataField={'LayoutName'} caption={"מערך"} dataType={"string"}></Column>
                            <Column dataField={'TotalSections'} caption={"שדות"} dataType={"number"} format={"###,###,###,##0"} ></Column>
                            <Column dataField={'TotalShelves'} caption={"מדפים"} dataType={"number"} format={"###,###,###,##0"} ></Column>
                            <Column dataField={'TotalArea'} caption={"שטח"} dataType={"number"} format={"###,###,###,##0"} ></Column>
                            <Column dataField={'AreaPercent'} caption={"% שטח"} dataType={"number"} format={"##0.##"}></Column>
                            <Column caption={"רשתי"} alignment={"center"}
                                columns={[
                                    {
                                        caption: "מכירות כמותי",
                                        dataField: "TotalAmountSold_network",
                                        dataType: "number",
                                        format: "###,###,###,##0"
                                    }, {
                                        caption: "% מכירות כמותי",
                                        dataField: "TotalAmountSoldPercent_network",
                                        dataType: "number",
                                        format: "##0.##"
                                    }, {
                                        caption: "מכירות כספי",
                                        dataField: "TotalPriceSold_network",
                                        dataType: "number",
                                        format: "###,###,###,##0"
                                    }, {
                                        caption: "% מכירות כספי",
                                        dataField: "TotalPriceSoldPercent_network",
                                        dataType: "number",
                                        format: "##0.##"
                                    }, {
                                        caption: "% משוקלל",
                                        dataField: "AvgResult_network",
                                        dataType: "number",
                                        format: "##0.##"
                                    }
                                ]}
                            ></Column>
                            <Column caption={"סניפי"} alignment={"center"}
                                columns={[
                                    {
                                        caption: "מכירות כמותי",
                                        dataField: "TotalAmountSold",
                                        dataType: "number",
                                        format: "###,###,###,##0"
                                    }, {
                                        caption: "% מכירות כמותי",
                                        dataField: "TotalAmountSoldPercent",
                                        dataType: "number",
                                        format: "##0.##"
                                    }, {
                                        caption: "מכירות כספי",
                                        dataField: "TotalPriceSold",
                                        dataType: "number",
                                        format: "###,###,###,##0"
                                    }, {
                                        caption: "% מכירות כספי",
                                        dataField: "TotalPriceSoldPercent",
                                        dataType: "number",
                                        format: "##0.##"
                                    }, {
                                        caption: "% משוקלל",
                                        dataField: "AvgResult",
                                        dataType: "number",
                                        format: "##0.##"
                                    }
                                ]}
                            ></Column>
                            <Summary>
                                <TotalItem
                                    column={'LayoutName'}
                                    summaryType={'sum'}
                                    displayFormat={'סהכ'}
                                />
                                <TotalItem
                                    column={'TotalSections'}
                                    summaryType={'sum'}
                                    valueFormat={"###,###,##0"}
                                    // displayFormat={'סהכ {0}'}
                                    displayFormat={'{0}'}
                                />
                                <TotalItem
                                    column={'TotalShelves'}
                                    summaryType={'sum'}
                                    valueFormat={"###,###,##0"}
                                    // displayFormat={'סהכ {0}'}
                                    displayFormat={'{0}'}
                                />
                                <TotalItem
                                    column={'TotalArea'}
                                    summaryType={'sum'}
                                    valueFormat={"###,###,##0"}
                                    // displayFormat={'סהכ {0}'}
                                    displayFormat={'{0}'}
                                />
                                <TotalItem
                                    column={'AreaPercent'}
                                    summaryType={'sum'}
                                    valueFormat={"##0"}
                                    // displayFormat={'סהכ {0}'}
                                    displayFormat={'{0}'}
                                />
                                <TotalItem
                                    column={'TotalAmountSold_network'}
                                    summaryType={'sum'}
                                    valueFormat={"###,###,##0"}
                                    // displayFormat={'סהכ {0}'}
                                    displayFormat={'{0}'}
                                />
                                <TotalItem
                                    column={'TotalAmountSoldPercent_network'}
                                    summaryType={'sum'}
                                    valueFormat={"##0"}
                                    // displayFormat={'סהכ {0}'}
                                    displayFormat={'{0}'}
                                />
                                <TotalItem
                                    column={'TotalPriceSold_network'}
                                    summaryType={'sum'}
                                    valueFormat={"###,###,##0"}
                                    // displayFormat={'סהכ {0}'}
                                    displayFormat={'{0}'}
                                />
                                <TotalItem
                                    column={'TotalPriceSoldPercent_network'}
                                    summaryType={'sum'}
                                    valueFormat={"##0"}
                                    // displayFormat={'סהכ {0}'}
                                    displayFormat={'{0}'}
                                />
                                <TotalItem
                                    column={'AvgResult_network'}
                                    summaryType={'sum'}
                                    valueFormat={"##0"}
                                    // displayFormat={'סהכ {0}'}
                                    displayFormat={'{0}'}
                                />
                                <TotalItem
                                    column={'TotalAmountSold'}
                                    summaryType={'sum'}
                                    valueFormat={"###,###,##0"}
                                    // displayFormat={'סהכ {0}'}
                                    displayFormat={'{0}'}
                                />
                                <TotalItem
                                    column={'TotalAmountSoldPercent'}
                                    summaryType={'sum'}
                                    valueFormat={"##0"}
                                    // displayFormat={'סהכ {0}'}
                                    displayFormat={'{0}'}
                                />
                                <TotalItem
                                    column={'TotalPriceSold'}
                                    summaryType={'sum'}
                                    valueFormat={"###,###,##0"}
                                    // displayFormat={'סהכ {0}'}
                                    displayFormat={'{0}'}
                                />
                                <TotalItem
                                    column={'TotalPriceSoldPercent'}
                                    summaryType={'sum'}
                                    valueFormat={"##0"}
                                    // displayFormat={'סהכ {0}'}
                                    displayFormat={'{0}'}
                                />
                                <TotalItem
                                    column={'AvgResult'}
                                    summaryType={'sum'}
                                    valueFormat={"##0"}
                                    // displayFormat={'סהכ {0}'}
                                    displayFormat={'{0}'}
                                />
                            </Summary>
                        </DataGrid>
                    </div>
                </div>
            </div>
        );
    }
}

const reportPositionCellStateToProps = (state: AppState, ownProps: { record: CatalogSaleRecord }) => {
    const product = state.newPlano.productsMap[ownProps.record.BarCode];

    return {
        ...ownProps,
        supplier: product && product.SapakId ? state.system.data.suppliersMap[product.SapakId] : undefined,
        class: product && product.ClassesId ? state.system.data.classesMap[product.ClassesId] : undefined,
        product: product,
        planogramDetail: state.planogram.productDetails[ownProps.record.BarCode],
        /* Barak 24.6.2020 * isOver: state.planogram.display.productDetailer === ownProps.record.BarCode */
        /* Barak 24.6.2020 */ isOver: state.planogram.display.productDetailer.barcode === ownProps.record.BarCode
    }
};
const reportTitleCellStateToProps = (state: AppState, ownProps: { record: CatalogSaleRecord }) => {
    return {
        ...ownProps,
        product: state.newPlano.productsMap[ownProps.record.BarCode],
    }
};

class ReportPositionCellContainer extends React.Component<ReturnType<typeof reportPositionCellStateToProps>> {
    render() {
        console.log('ReportPositionCellContainer this.props', this.props);
        const { product, planogramDetail, isOver } = this.props;

        if (product == null || !planogramDetail || planogramDetail.position.length === 0)
            return null;
        /* Barak 23.1.20 */
        // at this point planogramDetail.position may include many duplicate aisle_ids so we need to filter it to unique aisle_id only
        let aisleIds = planogramDetail.position.map(obj => { return obj.aisle_id; });
        aisleIds = aisleIds.filter((v, i) => { return aisleIds.indexOf(v) == i; });
        // at this point aisleIds includce only the unique values of aisle_id so we can use that to check the length

        return (<div className="detail-section" style={{ background: isOver ? "#dbffdc" : "none" }}>
            <div className="detail-row">
                {/* <label>{planogramDetail.position.length > 1 ? "גונדולות" : "גונדולה"}</label> */}
                <label>{aisleIds.length > 1 ? "גונדולות" : "גונדולה"}</label>
                <span>{planogramDetail.position.map(p => p.aisle_id).filter((p, i, list) => list.indexOf(p) === i).join()}</span>
            </div>
            <div className="detail-row">
                <label>תכולת מדף</label>
                <span>{planogramDetail.maxAmount}</span>
            </div>
        </div>);
    }
}

const ReportPositionCell = connect(reportPositionCellStateToProps)(ReportPositionCellContainer)

export default connect(mapStateToProps, mapDispatchToProps)(PlanogramLayoutReport)