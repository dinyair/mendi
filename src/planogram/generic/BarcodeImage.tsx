import React, { FC } from "react";
import config from "@src/planogram/shared/config";
import { CatalogBarcode } from "@src/planogram/shared/interfaces/models/CatalogProduct";
import { errorImageHandler } from "@src/planogram/shared/components/SmartImage";
import { getImageVersion } from "@src/planogram/shared/api/planogram.provider";
import * as planogramProvider from "@src/planogram/shared/api/planogram.provider";

const CACHE_KEY = "image_cache";
let imageVersion = loadImageVersion();

/* Barak 22.11.20 *
export function barcodeImageSrc(barcode: CatalogBarcode | string) {
/* Barak 19.1.20 - return config.PRODUCT_URL + '/' + barcode + ".jpg" + "?v" + imageVersion; */
/* Barak 19.1.20 - Add variable hour * / return config.PRODUCT_URL + '/' + barcode + ".jpg" + "?v" + imageVersion + "?nocache=" + new Date().getHours();
/* Barak 22.1.20 - Add random variable * return config.PRODUCT_URL + '/' + barcode + ".jpg" + "?v" + imageVersion + "?nocache=" + Math.random();* /
}
*****************/
/* Barak 22.11.20 */
export function barcodeImageSrc(barcode: CatalogBarcode | string, position?: number, netw?:string) {
    /* Barak 29.11.20 - add 729 and 0's to fill in short barcodes */
    if(barcode && barcode.toString().length >= 5 && barcode.toString().length < 10){
        let numOfZero = 10 - barcode.toString().length;
        let newBarCode = '729';
        for(let i=0;i<numOfZero;i++){
            newBarCode += '0';
        }
        newBarCode += barcode.toString();
        barcode = parseInt(newBarCode);
    }
    /**************************************************************/
    
    let newPos = 0;
    if (position != undefined && position > 0) newPos = position;
    // if(barcode === 5000127533196) console.log('barcodeImageSrc', barcode, position,'newPos', newPos);
    if (newPos > 0) {
        let url = config.CHECK_URL + '/' + barcode + "-" + (newPos + 1) + ".jpg";
        // let exists = planogramProvider.checkFile(url);
        // if (exists) {
        //     console.log('position image exists for barcode', barcode, 'position', position);
        //     return config.PRODUCT_URL + '/' + barcode + "-" + (newPos + 1) + ".jpg" + "?v" + imageVersion + "?nocache=" + new Date().getHours();
        // }
        // else {
        //     return config.PRODUCT_URL + '/' + barcode + ".jpg" + "?v" + imageVersion + "?nocache=" + new Date().getHours();
        // }


        // planogramProvider.checkFile(url).then((exists) => {
        //     if (exists) {
        //         console.log('position image exists for barcode', barcode, 'position', position);
        //         return config.PRODUCT_URL + '/' + barcode + "-" + (newPos + 1) + ".jpg" + "?v" + imageVersion + "?nocache=" + new Date().getHours();
        //     }
        //     else {
        //         return config.PRODUCT_URL + '/' + barcode + ".jpg" + "?v" + imageVersion + "?nocache=" + new Date().getHours();
        //     }
        // }).catch((err) => {
        //     return config.PRODUCT_URL + '/' + barcode + ".jpg" + "?v" + imageVersion + "?nocache=" + new Date().getHours();
        // });
        if(typeof barcode === 'string' && barcode.indexOf('7290008330381')>= 0) console.log('7290008330381',url);
        // all position images are always taken from the client's image library
        if(netw){
            let path = config.PRODUCT_URL + '/' + netw + "/" + barcode + "-" + (newPos + 1) + ".jpg" + "?v" + imageVersion + "?nocache=" + new Date().getHours();
            console.log('path',path)
            return path;
        }
        else return config.PRODUCT_URL + '/' + barcode + "-" + (newPos + 1) + ".jpg" + "?v" + imageVersion + "?nocache=" + new Date().getHours();
    }
    else {
        return config.PRODUCT_URL + '/' + barcode + ".jpg" + "?v" + imageVersion + "?nocache=" + new Date().getHours();
    }
}
/******************/
export const BarcodeImage: React.FC<{ barcode: CatalogBarcode } & any> = (props) => <img
    {...props}
/* Barak 19.1.20 - src={config.PRODUCT_URL + '/' + props.barcode + ".jpg" + "?v" + imageVersion}*/
/* Barak 19.1.20 - Add variable hour */ src={config.PRODUCT_URL + '/' + props.barcode + ".jpg" + "?v" + imageVersion + "?nocache=" + new Date().getHours()}
    /* Barak 22.1.20 - Add random variable * src={config.PRODUCT_URL + '/' + props.barcode + ".jpg" + "?v" + imageVersion + "?nocache=" + Math.random()}*/
    onError={errorImageHandler} />

function loadImageVersion() {
    let version = localStorage.getItem(CACHE_KEY);
    if (!version)
        localStorage.setItem(CACHE_KEY, Math.round(Date.now() / 1000).toString());
    return version || config.IMAGE_CACHE_VERSION;
}

export function refreshBarcodeImage() {
    localStorage.setItem(CACHE_KEY, Math.round(Date.now() / 1000).toString());
    window.location.reload();
}