
import { DropTarget, DragElementWrapper, DragSource } from 'react-dnd';


export interface DragDropResultBase {
    type?: string | string[],
    payload: any,
    /* Barak 29.9.20 */ distFromStart: number,
    /* Barak 26.11.20 */ distFromTop: number,
}