import * as React from 'react';
import { CustomInput } from 'reactstrap';


interface Props {
    readonly option: any;
    readonly checked: boolean;
    readonly disabled?: boolean;
    readonly classNames?:string;
    onClick: (id:string) => void;
    readonly indeterminate?:indeterminateInterface
}
interface indeterminateInterface {
	readonly data: any
	readonly rows: any,
	readonly dProps: any,
}
export const CustomCheckBox: React.FC<Props> = props => {
    const {
        option,
        onClick,
        checked,
        classNames,
        indeterminate,
        disabled
    } = props;
    const inputRef = React.useRef();

	const toggleIndeterminate = ()=>{
		let isTrue = indeterminateCheck(option)
		if (indeterminate && inputRef.current &&indeterminate.data.children && indeterminate.data.children.length&& isTrue) {
            inputRef.current.indeterminate = true;
          }else{
              if(inputRef.current){
            inputRef.current.indeterminate = false;
              }
          }
	}
	let indeterminateCheck = (type: string) => {
		let chckedCounter = 0
        if(indeterminate){
		indeterminate.rows.forEach((child: any) => {
			if (indeterminate.dProps.data && indeterminate.dProps.data[child.navLink] && indeterminate.dProps.data[child.navLink][type + '_flag'] > 0) chckedCounter++
		});
		return chckedCounter < indeterminate.rows.length && chckedCounter > 0 ? true : false
        }else return false
	};
    
	React.useEffect(() => {
        if(indeterminate) {
            toggleIndeterminate()
        }
	}, [indeterminate ? indeterminate.dProps: null])


    return (
        <label className='combobox__drop-down_label text-black font-medium-1 cursor-pointer'  onClick={(e) => { if(!disabled) {e.stopPropagation();e.preventDefault(); onClick(option.id)}} }>
            <CustomInput
                innerRef={inputRef}
                // onChange={(e) => {}}
                className={`form-group__checkbox-field_checkbox ${classNames ? classNames : 'ml-2 mr-1 '}`}
                id={option.id}
                type='checkbox'
                name={option.id}
                checked={checked}
                disabled={disabled}
            />
            {option.name}
        </label>
    );
};


export default CustomCheckBox;
