import * as React from "react";
import { Switch } from 'react-router-dom';
import {
    Redirect, RouteComponentProps
} from 'react-router';
import { ThunkDispatch } from 'redux-thunk';
import { AppState } from '@src/planogram/shared/store/app.reducer';
import { AnyAction } from 'redux';
import { connect } from 'react-redux';
import { fetchCatalog, fetchBarcodeStatuses, chkprm } from '@src/planogram/shared/api/catalog.provider';
import { setCatalog, setBarcodeStatuses } from '@src/planogram/shared/store/catalog/catalog.action';
import { errorPlanogramView, setStore } from '@src/planogram/shared/store/planogram/store/store.actions';
import { ProtectedRoute } from '@src/planogram/shared/components/AppRoute';
import { setUser } from '@src/planogram/shared/store/auth/auth.actions';
import PlanogramImageUpload from './PlanogramImageUpload';

export const REPORT_BASE_URL = "/upload"

const mapStateToProps = (state: AppState, ownProps: RouteComponentProps<{}>) => ({
    ...ownProps,
    newPlano: state.newPlano.products,
    user: state.auth.user,
})

const mapDispatchToProps = (dispatch: ThunkDispatch<AppState, {}, AnyAction>) => ({
    fetchCatalog: () => fetchCatalog()
        .then(catalog => dispatch(setCatalog(catalog)))
        .catch((err) => dispatch(errorPlanogramView(err))),
    clearStore: () => dispatch(setStore(null))
})


type UploadScreenComponentProps = ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>;
class UploadScreenComponent extends React.Component<UploadScreenComponentProps> {
    componentDidMount() {
        // this.props.clearStore();
        // this.props.fetchCatalog();
        // this.props.fetchBarcodeStatuses();
    }
    render() {
        console.log('this.props.catalog.length',this.props.catalog.length);
        // if (this.props.catalog.length === 0)
        //     return null;
        let userLevel = this.props.user && this.props.user.level != null ? this.props.user.level : 50;

        return <Switch>
            <ProtectedRoute
                path={REPORT_BASE_URL}
                component={PlanogramImageUpload} />
            <Redirect to={REPORT_BASE_URL} />
        </Switch>;
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(UploadScreenComponent)