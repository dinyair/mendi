import * as React from 'react';
import { connect } from 'react-redux';
import { DateBox, SelectBox, Button, TagBox, DataGrid } from 'devextreme-react'
import { Column, Texts, Sorting, Scrolling, HeaderFilter, FilterRow, Export, Summary, TotalItem, GroupPanel, ColumnChooser, LoadPanel, Paging, Pager } from 'devextreme-react/data-grid'
import DataSource from 'devextreme/data/data_source'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { AnyAction, bindActionCreators } from 'redux'
import { ThunkDispatch } from 'redux-thunk'
import { Rnd } from 'react-rnd';

import { AppState } from '@src/planogram/shared/store/app.reducer'
import { fetchCatalogSales, CatalogSaleRecord, fetchTruma, fetchTrumaSnif, setUserReportState, getUserReportState, fetchLayoutSales } from '@src/planogram/shared/api/sales.provider';
import { faBars, faFile, faFilter, faWindowClose } from '@fortawesome/free-solid-svg-icons';
import { uiNotify } from '@src/planogram/shared/components/Toast';
import { fetchBarcodeStatuses } from '@src/planogram/shared/api/catalog.provider';
import { setBarcodeStatuses } from '@src/planogram/shared/store/catalog/catalog.action';
import { hideSalesReport } from '@src/planogram/shared/store/planogram/display/display.actions';
import { fetchBranches } from '@src/planogram/shared/api/settings.provider';
import * as System from '@src/planogram/shared/interfaces/models/System';

import * as moment from 'moment';
import { Branch } from '@src/planogram/shared/interfaces/models/System'
import { setBranches } from '@src/planogram/shared/store/system/data/data.actions'
import { fetchFileList, fetchStores } from '@src/planogram/shared/api/planogram.provider'
import Dropzone, { DropzoneState } from 'react-dropzone';
import { httpClient, postRequest } from '@src/planogram/shared/http';

export type ExistingFileRecord = string;

const mapStateToProps = (state: AppState, ownProps: any) => ({
    user: state.auth.user,
    products: state.catalog.products,
    productsMap: state.newPlano.productsMap,
    displayAisle: state.planogram.display.aisleIndex != null
        && state.planogram.store
        && state.planogram.store.aisles[state.planogram.display.aisleIndex] ? state.planogram.store.aisles[state.planogram.display.aisleIndex].aisle_id : null,
    displaySalesReport: state.planogram.display.displaySalesReport,
    virtualProductDetails: state.planogram.productDetails,
    planogram: state.planogram,
    suppliers: state.system.data.suppliers,
    classes: state.system.data.classes,
    groups: state.system.data.groups,
    subGroups: state.system.data.subGroups,
    branches: state.system.data.branches,
    series: state.system.data.series,
    /* Barak 18.8.20 */ segments: state.system.data.segments,
    /* Barak 6.12.20 */ models: state.system.data.models,
    /* Barak 7.12.20 */ userReportTemplate: state.system.data.userReportTemplates ? state.system.data.userReportTemplates.filter(line => line.report === 'PlanogramReportThin') : []
})

const mapDispatchToProps = (dispatch: ThunkDispatch<AppState, {}, AnyAction>) => ({
    fetchBarcodeStatuses: () => fetchBarcodeStatuses()
        .then(statuses => dispatch(setBarcodeStatuses(statuses)))
        .catch((err) => {
            uiNotify("Unable to load network barcode statuses")
        }),
    hideSalesReport: () => dispatch(hideSalesReport()),
    setBranches: (branches: System.Branch[]) => dispatch(setBranches(branches))
})

type PlanogramImageUploadProps = ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>
type PlanogramImageUploadState = {
    loading: boolean,
    loader: number,
    files: File[],
    existingFiles: ExistingFileRecord[]
    newFiles: ExistingFileRecord[]
};

class PlanogramImageUpload extends React.Component<PlanogramImageUploadProps, PlanogramImageUploadState> {
    state: PlanogramImageUploadState = {
        loading: true,
        loader: 0,
        files: [],
        existingFiles: [],
        newFiles: []
    }

    componentDidMount = async () => {
        const user = this.props.user;
        if (!user) return uiNotify("לא ניתן לראות קבצים קיימים ברגע זה.");
        let files = await fetchFileList(user.db);
        if (files) {
            this.setState({
                existingFiles: files,
                loading: false,
            });
        } else {
            this.setState({
                existingFiles: [],
                loading: false,
            });
            uiNotify("לא ניתן לראות קבצים קיימים ברגע זה.");
        }
        this.setState({ loading: false });
        // httpClient.get<{ status: string, files: ExistingFileRecord[] }>('/network/files?network=' + user.db).then((resp) => {
        //     const { data } = resp;
        //     this.setState({
        //         existingFiles: data.files,
        //         loading: false,
        //     });
        // }).catch(err => {
        //     console.error(err);
        //     this.setState({
        //         loading: false,
        //         existingFiles:[]
        //     });
        //     uiNotify("לא ניתן לראות קבצים קיימים ברגע זה.");
        // })
    }

    upload = async () => {
        const user = this.props.user;
        if (!user) return uiNotify("לא ניתן להעלאות קבצים ברגע זה.");
        const data = new FormData();
        // data.append("email", user.email);
        // data.append("supplier", user.supplier);
        console.log('this.state.files',this.state.files);
        for (const file of this.state.files) {
            // data.append("file_" + (file.name).replace(" ", "_"), file);
            data.append("file", file);
        }

        this.setState({ loading: true });
        let resp = await postRequest<{ status: string }>
            (encodeURI('planogram/network/upload_files'), data, {
                onUploadProgress: (progressEvent) => {
                    var percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total)
                    this.setState({ loader: percentCompleted })
                }
            }).then(() => {
                uiNotify(`${this.state.files.length} קבצים הועלו בהצלחה`, "success", 10000);
                this.setState({
                    loading: false,
                    loader: 0,
                    files: [],
                    newFiles: [...this.state.newFiles, ...this.state.files.map(v => (v.name))]
                });
            }).catch(err => {
                console.log(err);
                uiNotify("שגיאה בהעלאת קבצים.", "error");
                this.setState({ loading: false, loader: 0, });
            })

        // httpClient.post(encodeURI(`/network/upload?network=${user.db}`), data, {
        //     onUploadProgress: (progressEvent) => {
        //         var percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total)
        //         this.setState({ loader: percentCompleted })
        //     }
        // }).then(() => {
        //     uiNotify(`${this.state.files.length} קבצים הועלו בהצלחה`, "success", 10000);
        //     this.setState({
        //         loading: false,
        //         loader: 0,
        //         files: [],
        //         newFiles: [...this.state.newFiles, ...this.state.files.map(v => (v.name))]
        //     });
        // }).catch(err => {
        //     console.log(err);
        //     uiNotify("שגיאה בהעלאת קבצים.", "error");
        //     this.setState({ loading: false, loader: 0, });
        // })
    }

    render() {
        const { loading, loader, files, existingFiles, newFiles } = this.state;
        const user = this.props.user;
        let loaderPercentage = loader + "%"
        return (
            <div className="dashboard">
                {loader > 0 ? <div style={{ width: "100%" }}>
                    <div style={{ width: loaderPercentage, minHeight: "1em", padding: "0.5em", background: "#3498db" }}>
                        <span>{loaderPercentage}</span>
                    </div>
                </div> : null}
                {!loading ? <section className="dashboard-main" style={{ display: "flex", }}>
                    <section style={{ flex: 1, padding: "1em", minWidth: 400 }} >
                        <div className="container">
                            <header>
                                <h1>העלאת קבצים</h1>
                            </header>
                            <Dropzone
                                multiple
                                // maxSize={100000 * 1000}
                                maxSize={10485760} // the max size of files accepted is 10 Megabytes = 10485760 Bytes
                                accept={".jpg"} // accept only files with extension .jpg
                                onDrop={acceptedFiles => this.setState({ files: [...files, ...acceptedFiles] })}>
                                {(props) => <DropZoneContainer {...props} files={files} />}
                            </Dropzone>
                            <h3>קבצים: </h3>
                            <section style={{ display: 'flex', flexWrap: "wrap", padding: "1em 0" }}>
                                {files
                                    ? files.map(v => <div style={{ display: "flex", flexFlow: "column", alignItems: "center", maxWidth: "33%", padding: "0.2em", border: "1px solid #181818" }}>
                                        <div style={{ fontSize: "4em" }}>
                                            <FontAwesomeIcon icon={faFile} />
                                        </div>
                                        <div style={{ fontSize: "0.8em" }}>{v.name}</div>
                                        <div style={{ fontSize: "0.7em" }}>{v.type}</div>
                                    </div>)
                                    : null}
                            </section>
                            <footer style={{ display: "flex", width: "100%", justifyContent: "center" }}>
                                <button style={{ maxWidth: 500, width: "100%", padding: "1em" }} disabled={!files || files === undefined || (files != undefined && files.length === 0)} onClick={this.upload}>העלאת קבצים</button>
                            </footer>
                        </div>
                    </section>
                    {existingFiles.length || newFiles.length ? <aside style={{ padding: "1em", maxWidth: 300, width: "100%", height: "100%", overflowY: "auto", background: "#e9e9e9", color: "#181818" }}>
                        <div style={{ display: "flex", alignContent: "center", marginBottom: "1em" }}>
                            <div style={{ flex: 1, fontSize: "1.5em", fontWeight: "bold" }}>
                                קבצים שהתקבלו:
                            </div>
                            <div style={{ fontSize: "1.5em", textDecoration: "underline" }}>
                                {existingFiles.length + newFiles.length || ""}
                            </div>
                        </div>
                        <div style={{ direction: "ltr" }}>
                            {existingFiles.map((file, i) => (
                                <FileRecordComponent file={file} key={file + i} />
                            ))}
                        </div>
                        {newFiles.length > 0 ? <h5>קבצים חדשים: </h5> : null}
                        {newFiles.length > 0 ? <div style={{ direction: "ltr" }}>
                            {newFiles.map((file, i) => (
                                <FileRecordComponent file={file} key={file + i} new />
                            ))}
                        </div> : null}
                    </aside> : null}
                </section> : <div className="dashboard-main"><div className="loader"></div></div>}
            </div>
        )
    }
}

const FileRecordComponent: React.FC<{ file: string, new?: boolean }> = (props) => {
    return (<div style={{ width: "100%", marginBottom: "0.2em", whiteSpace: "nowrap", borderBottom: props.new ? "0.2em solid #2ecc71" : "" }}>
        <FontAwesomeIcon icon={faFile} />
        <span style={{ fontSize: "0.7em", marginLeft: "0.2em" }}>
            {props.file}
        </span>
    </div>)
}

class DropZoneContainer extends React.Component<DropzoneState & {
    files: File[]
}>{
    render() {
        const { files } = this.props;
        const { getRootProps, getInputProps, isDragActive } = this.props;
        return <section {...getRootProps()} style={{
            height: 200,
            border: isDragActive ? "5px dashed #27ae60" : "2px dashed #000"
        }}>
            <input {...getInputProps()} />
            <header>
                <p style={{ margin: "4em auto", textAlign: "center" }}>הנח קבצים מבוקשים כאן...
                <br/> <br/>
                רק קבצים מסוג jpg.
                <br/>
                גודל מקסימלי 10 מגה
                </p>
            </header>
        </section>
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(PlanogramImageUpload)