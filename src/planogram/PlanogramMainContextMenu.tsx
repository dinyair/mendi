import * as React from 'react';
import { AnyAction } from 'redux';
import { connect } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import { Menu } from 'react-contexify';
import { AppState } from '@src/planogram/shared/store/app.reducer';
import { PlanogramAisle } from '@src/planogram/shared/store/planogram/planogram.types';
import * as aisleActions from '@src/planogram/shared/store/planogram/store/aisle/aisle.actions';
import * as planogramProvider from "@src/planogram/shared/api/planogram.provider";
import { toggleRowItems, toggleSettings, setColorBy } from '@src/planogram/shared/store/planogram/planogram.actions';
import { uiNotify } from '@src/planogram/shared/components/Toast';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus, faEye, faEyeSlash, faFileArchive, faWindowClose, faCog, faFileInvoice } from '@fortawesome/free-solid-svg-icons';
import { toggleShelfItems, showSalesReport, hideSalesReport, toggleSaleTags } from '@src/planogram/shared/store/planogram/display/display.actions';
import { useIntl, FormattedMessage } from "react-intl"

const mapStateSettingsContext = (state: AppState) => ({
    store: state.planogram.store,
    hideShelfItems: state.displayOptions?.hideProducts,
    showRowItems: state.displayOptions?.showProdutDepth,
    displaySalesReport: state.planogram.display.displaySalesReport,
    displayColorReport: state.planogram.display.colorBy != null,
    hideSaleTags: state.displayOptions?.hideTags,
});
const mapDispatchSettingsContext = (dispatch: ThunkDispatch<AppState, any, AnyAction>) => ({
    toggleShelfItems: () => dispatch(toggleShelfItems()),
    toggleRowItems: () => dispatch(toggleRowItems()),
    addAisle: (aisle: PlanogramAisle) => dispatch(aisleActions.addAisleAction(aisle)),
    toggleSettings: () => dispatch(toggleSettings()),
    toggleSalesReport: (show: boolean) => {
        if (show)
            dispatch(hideSalesReport());
        else
            dispatch(showSalesReport());
    },
    toggleColorReport: (showColorMap: boolean) => dispatch(showColorMap ? setColorBy(null) : setColorBy("report")),
    toggleSaleTags: () => dispatch(toggleSaleTags()),
});
export const PlanogramMainContextMenu = connect(mapStateSettingsContext, mapDispatchSettingsContext)((props: ReturnType<typeof mapStateSettingsContext> & ReturnType<typeof mapDispatchSettingsContext>) => {
    const { hideSaleTags, toggleSaleTags, hideShelfItems, toggleSettings, displaySalesReport, toggleShelfItems, toggleSalesReport, addAisle, store, displayColorReport, toggleColorReport } = props;
    return <Menu id="PLANOGRAM_VIEW_MENU" className="planogram-context-window">
        <div className="context-title"><FormattedMessage id="store-settings"/></div>
        <div className="input-row">
            <button onClick={(e) => toggleSettings()}>
                <FontAwesomeIcon icon={faCog} />
                <span>{<FormattedMessage id="settings"/>}</span>
            </button>
        </div>
        <div className="input-row">
            <button onClick={(e) => toggleShelfItems()}>
                {hideShelfItems ? <FontAwesomeIcon icon={faEye} /> : <FontAwesomeIcon icon={faEyeSlash} />}
                <span>{hideShelfItems ? <FormattedMessage id="show-items" /> : <FormattedMessage id="hide-items" />}</span>
            </button>
        </div>
        <div className="input-row">
            <button onClick={(e) => toggleSaleTags()}>
                {hideSaleTags ? <FontAwesomeIcon icon={faEye} /> : <FontAwesomeIcon icon={faEyeSlash} />}
                <span>{hideSaleTags ? <FormattedMessage id="show-tags" /> : <FormattedMessage id="hide-tags" />}</span>
            </button>
        </div>
        <div className="input-row">
            <button onClick={() => toggleSalesReport(displaySalesReport)}>
                <FontAwesomeIcon icon={faFileArchive} />
                <span>{!displaySalesReport ? <FormattedMessage id="open" /> : <FormattedMessage id="close" />} <FormattedMessage id="sales-report" /></span>
                {displaySalesReport && <FontAwesomeIcon icon={faWindowClose} style={{ float: "left" }} />}
            </button>
        </div>
        <div className="input-row">
            <button onClick={() => toggleColorReport(displayColorReport)}>
                <FontAwesomeIcon icon={faFileInvoice} />
                <span>{!displayColorReport ? <FormattedMessage id="open" /> : <FormattedMessage id="close" />} <FormattedMessage id="color-report"/> </span>
                {displayColorReport && <FontAwesomeIcon icon={faWindowClose} style={{ float: "left" }} />}
            </button>
        </div>
        <div className="input-row">
            <button onClick={() => {

                if (store && store.store_id)
                    planogramProvider.createStoreAisle(store.store_id).then(aisle => {
                        addAisle(aisle);
                    }).catch(err => uiNotify("Unable to add aisle at this time"));
                else {
                    uiNotify("Unable to add aisle into unsaved store");
                }
            }}>
                <FontAwesomeIcon icon={faPlus} />
                <span><FormattedMessage id="add-aisle"/></span>
            </button>
        </div>
    </Menu>;
});
