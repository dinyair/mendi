import * as React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { AppState } from '@src/planogram/shared/store/app.reducer';
import { Button, TabContent, TabPane } from 'reactstrap';
import { Tabs, Tab } from '@src/components/tabs';
import { useHistory } from 'react-router-dom';
import {
	fetchBranchStores,
	deletePlanogramStore,
	createNewStore,
	setStorePrimary
} from '@src/planogram/shared/api/planogram.provider';
import { NavLink, Link } from 'react-router-dom';
import { BranchAssignment } from './BranchAssignment';
import { Layout } from 'react-feather';
import { Icon } from '@components';
import { config } from '@src/config';
import * as planogramApi from '@src/planogram/shared/api/planogram.provider';
import { SaveAisle, fetchAndUpdatePlanogram } from '@src/helpers/planogram';
import { useIntl, FormattedMessage } from 'react-intl';
import * as systemApi from '@src/planogram/shared/api/settings.provider';
import { PLANOGRAM_BASE_URL } from '@src/PlanoApp';
import { Reports } from '@src/planogram/DataReports/index';
import classnames from 'classnames';
import { getRequest } from '@src/http';
import { FetchPlanogramStoreResponse } from '@src/planogram/shared/interfaces/responses/PlanogramResponse';
import * as planogramProvider from '@src/planogram/shared/api/planogram.provider';
import { ModalTypes } from '@src/components/GenericModals/interfaces';
import DeleteModalData from './modals/deleteModalData';
import InputText from '@src/components/input-text';
import CustomCheckBox from '@src/planogram/generic/customCheckBox';
import InfoModalData from './modals/infoModalData';
import { RootStore } from '@src/store';

export const Selection: React.FunctionComponent = () => {
	const dispatch = useDispatch();
	const { formatMessage } = useIntl();
	document.title = formatMessage({ id: 'planogram_planning' })
	const history = useHistory();
	const Divider = <div className="width-100-per height-2 bg-gray" />;
	const customizer = { direction: document.getElementsByTagName('html')[0].dir };
	const user = useSelector((state: RootStore) => state.user)
	const userPermissions = user ? user.permissions : null
	const userEditFlag = userPermissions && userPermissions[PLANOGRAM_BASE_URL] && userPermissions[PLANOGRAM_BASE_URL].edit_flag === 1
	const stores = useSelector((state: AppState) => state.system.data.stores);
	const branches = useSelector((state: AppState) => state.system.data.branches);
	const layouts = useSelector((state: AppState) => state.system.data.layouts);
	const [disaplyCubes, setDisplayCubes] = React.useState<number>(10);
	const [editOptionName, setEditOptionName] = React.useState<number | null>(null);
	const [loading, setLoading] = React.useState<boolean>(false);
	const [store, setStore] = React.useState<any>(null);
	const [options, setOptions] = React.useState<any>(null);
	const [filter, setFilter] = React.useState<string | null>(null);
	const [isBranchAssignment, setIsBranchAssignment] = React.useState<boolean>(false);
	const [showNewAlgoPoup, setShowNewAlgoPoup] = React.useState<boolean>(false);
	const newPlano = useSelector((state: AppState) => (state.newPlano ? state.newPlano.data : undefined));

	const [selectedStore, setSelectedStore] = React.useState<any>();

	const [modalType, setModalType] = React.useState<ModalTypes>(ModalTypes.none);
	const [optionToDelete, setOptionToDelete] = React.useState<any>(null);

	const toggleModal = (newType: ModalTypes) => {
		setModalType(newType);
	};

	const url = window.location.href;
	const urlElements = url.split('/');
	let currentBranchId = parseInt(urlElements[5]);
	let layoutStoreId = parseInt(urlElements[6]);
	let currentAisleId = parseInt(urlElements[7]);
	let currentAisle =
		options && options.length ? options.filter((option: any) => option.aisle_id === currentAisleId)[0] : null;
	let currentAisleIndex =
		options && options.length ? options.findIndex((option: any) => option.aisle_id === currentAisleId) : null;
	const TABS = {
		BRANCHES: formatMessage({ id: 'Branches' }),
		LAYOUT: formatMessage({ id: 'Layouts' })
	};
	const [activeTab, setActiveTab] = React.useState(
		urlElements[4] ? (urlElements[4] === 'branches' ? TABS.BRANCHES : TABS.LAYOUT) : TABS.BRANCHES
	);
	let currentTab: string = activeTab === TABS.LAYOUT ? 'layouts' : 'branches';
	let currentBranch: any =
		activeTab === TABS.BRANCHES && currentBranchId
			? branches && branches.find((branch: any) => branch.BranchId === currentBranchId)
				? branches.find((branch: any) => branch.BranchId === currentBranchId)
				: null
			: layouts && layouts.find((layout: any) => layout.Id === currentBranchId)
				? layouts.find((layout: any) => layout.Id === currentBranchId)
				: null;

	const LinkComponent = (link: any, element: any) => {
		return <Link to={link}>{element}</Link>;
	};



	React.useEffect(() => {
		if (!loading) {
			(async () => {
				setLoading(true);
				await fetchAndUpdatePlanogram([
					{ state: stores, type: 'stores', forceUpdate: true },
					{ state: layouts, type: 'layouts', forceUpdate: true },
					{ state: branches, type: 'branches', forceUpdate: true }
				], dispatch);
				setLoading(false)
			})();
			;
		}
	}, []);

	React.useEffect(() => {
		if (currentBranchId && !options && !loading) {
			setLoading(true);
			if (activeTab === TABS.BRANCHES) {
				fetchBranchStores(currentBranchId).then(stores => {
					setOptions(stores);
				});
			} else if (activeTab === TABS.LAYOUT) {
				systemApi.fetchPlanoLayouts(currentBranchId).then((stores: any) => {
					let storeId: null | number = stores[0] ? stores[0].id : null;

					if (storeId) {
						getRequest<FetchPlanogramStoreResponse>('planogram/store/' + (storeId || '') + '/New').then(
							(resp: any) => {
								if (!resp.store) throw new Error('No store was found.');
								setStore(resp.store);
								setOptions(resp.store.aisles);
								let currentAisleIndex = parseInt(urlElements[7]);
								history.push(
									`${PLANOGRAM_BASE_URL}/layout/${currentBranchId}/${storeId}${currentAisleIndex ? '/' + currentAisleIndex : ''
									}`
								);
							}
						);
					}
				});
			}
		}
		setLoading(false);
	}, [currentBranchId]);

	const handleLayoutDelete = async () => {
		console.log(optionToDelete.aisle_id);
		console.log(layoutStoreId);
		setOptions(options.filter((o: any) => o.aisle_id !== optionToDelete.aisle_id));
		toggleModal(ModalTypes.none);
		await planogramApi.deleteAllRelatedAisles(optionToDelete.aisle_id)
	};

	const handleBranchDelete = () => {
		setLoading(true);

		if (currentBranchId != null)
			deletePlanogramStore(optionToDelete.id)
				.then(() => fetchBranchStores(currentBranchId))
				.then((stores: any) => {
					setLoading(false);
					setOptions(stores);
					toggleModal(ModalTypes.none);
				})
				.catch((err: any) => {
					setLoading(false);
					toggleModal(ModalTypes.none);
				});
	};

	const handlePrimaryChange = () => {
		let storesArr = options;
		if (options[selectedStore].primary) {
			for (let i = 0; i < storesArr.length; i++) {
				storesArr[i].primary = false;
				setStorePrimary(storesArr[i].id, storesArr[i].primary);
			}
		} else {
			for (let i = 0; i < storesArr.length; i++) {
				if (options[selectedStore].id === storesArr[i].id)
					storesArr[i].primary = true;
				else storesArr[i].primary = false;
				setStorePrimary(storesArr[i].id, storesArr[i].primary);
			}
		}
		setOptions([...storesArr]);
		setSelectedStore(null)
		toggleModal(ModalTypes.none)
	}

	const SelectStore = (activeTab: any) => {
		let editButton = (
			<Button color="transparent" className="mr-1 rounded width-min-content p-05 ">
				<Icon src={'../' + config.iconsPath + 'planogram/edit.svg'} />
			</Button>
		);
		return (
			<>
				<div className="d-flex my-2">
					<div
						onClick={() => {
							setOptions(null);
							history.push(PLANOGRAM_BASE_URL + '/' + currentTab);
						}}
						className="cursor-pointer"
					>
						<Icon
							src={'../' + config.iconsPath + 'table/arrow-right.svg'}
							className={classnames('mr-1', {
								'rotate-180': customizer.direction == 'ltr'
							})}
							style={{ height: '0.5rem', width: '0.2rem' }}
						/>
					</div>
					<div className="text-bold-700 font-medium-2 text-black">
						{currentBranch ? currentBranch.Name : ''}{' '}
					</div>
				</div>
				{userEditFlag && ((layoutStoreId && activeTab === TABS.LAYOUT) || activeTab === TABS.BRANCHES )? (
					<Button
						onClick={
							layoutStoreId && activeTab === TABS.LAYOUT
								? async () => {
									let newAisle: any = await planogramProvider.createStoreAisle(layoutStoreId);
									if (newAisle) {
										setOptions([...options, newAisle]);
									}
								}
								: () => {
									setLoading(true);
									createNewStore(currentBranchId).then((store: any) => {
										store.id = store.store_id;
										setOptions([...options, store]);
										setLoading(false);
										// history.push(`${PLANOGRAM_BASE_URL}/editor/${currentBranchId}/${store.store_id}`)
									});
								}
						}
						color="primary"
						className="round text-bold-400 d-flex justify-content-center align-items-center cursor-pointer btn-primary"
					>
						+{' '}
						{activeTab === TABS.LAYOUT ? (
							<FormattedMessage id="new_planogram" />
						) : (
							<FormattedMessage id="new_store" />
						)}
					</Button>
				) : null}

				<div className="d-flex-column width-min-content">
					{options
						? options.map((option: any, index: number) => (
							<>
								<div className="d-flex my-1  align-items-center justify-content-center">
									{userEditFlag && activeTab === TABS.BRANCHES ? (
										<CustomCheckBox
											option={{ id: index }}
											checked={option.primary}
											onClick={(index: any) => {
												setSelectedStore(index)
												toggleModal(ModalTypes.info)
											}}
										/>
									) : null}
									{editOptionName === index ? (
										<input
											onBlur={() => {
												setEditOptionName(null);
												SaveAisle(store, options[index]);
											}}
											type="text"
											value={option.name}
											onChange={e => {
												let newOptions = options.map((option: any, i: number) => {
													if (index !== i) return option;
													else return { ...option, name: e.target.value };
												});
												setOptions(newOptions);
											}}
										/>
									) : (
										<div
											onClick={() => {
												setEditOptionName(index);
											}}
											className="cursor-pointer text-bold-600 font-medium-2 text-black width-15-rem"
										>
											{option.name}
										</div>
									)}
									<div className="d-flex align-items-center">
										{/* {activeTab === TABS.LAYOUT && (
								    <div className="cursor-pointer"
									onClick={()=> {}}
									>
									<Icon src={config.iconsPath + "planogram/space_assignment.svg"}
									className="mr-05" style={{ height: '0.5rem', width: '0.2rem' }} />
								  </div>
							  )} */}
										<div className="cursor-pointer">
											{LinkComponent(
												{
													pathname:
														activeTab === TABS.LAYOUT
															? PLANOGRAM_BASE_URL +
															'/layout/view/' +
															currentBranchId +
															'/' +
															layoutStoreId +
															'/' +
															option.aisle_id
															: PLANOGRAM_BASE_URL +
															(activeTab === TABS.LAYOUT
																? '/layouts'
																: '/branches') +
															'/view/' +
															currentBranchId +
															'/' +
															option.id
												},
												<Button
													color="transparent"
													className="mr-1 rounded width-min-content p-05 "
												>
													<Icon src={'../' + config.iconsPath + 'planogram/view.svg'} />
												</Button>
											)}
										</div>
										{userEditFlag && (<div className="cursor-pointer">
											{activeTab === TABS.LAYOUT ? (
												<div
													onClick={
														!option.newplano
															? () => {
																setShowNewAlgoPoup(true);
																history.push(
																	PLANOGRAM_BASE_URL +
																	'/layout/' +
																	currentBranchId +
																	'/' +
																	layoutStoreId +
																	'/' +
																	option.aisle_id
																);
															}
															: () => {
																history.push(
																	PLANOGRAM_BASE_URL +
																	'/layout/editor/' +
																	currentBranchId +
																	'/' +
																	layoutStoreId +
																	'/' +
																	option.aisle_id
																);
															}
													}
												>
													{editButton}
												</div>
											) : (
												LinkComponent(
													{
														pathname:
															PLANOGRAM_BASE_URL +
															'/branch/editor/' +
															currentBranchId +
															'/' +
															option.id
													},
													editButton
												)
											)}
										</div>)}
										{userEditFlag && (
										<div
											className="cursor-pointer"
											onClick={
												activeTab === TABS.LAYOUT
													? async e => {
														e.preventDefault();
														setOptionToDelete(option);
														toggleModal(ModalTypes.delete);
													}
													: e => {
														toggleModal(ModalTypes.delete);
														setOptionToDelete(option);
														e.preventDefault();
													}
											}
										>
											<Button
												color="transparent"
												className="mr-1 rounded width-min-content p-05 "
											>
												<Icon
													src={'../' + config.iconsPath + 'planogram/trash.svg'}
													style={{ height: '0.5rem', width: '0.2rem' }}
												/>
											</Button>
										</div>)}
									</div>

									{option.newplano && activeTab === TABS.LAYOUT ? (
										<Button
											onClick={(e) => {
												e.preventDefault()
												toggleModal(ModalTypes.add);
												history.push(
													PLANOGRAM_BASE_URL +
													'/layouts/' +
													currentBranchId +
													'/' +
													layoutStoreId +
													'/' +
													option.aisle_id
												);
											}}
											color="white"
											className={classnames(
												'height-3-rem d-flex border-turquoise width-8-rem align-items-center justify-content-center position-relative font-small-4 cursor-pointer py-05 border-2  text-turquoise text-bold-700   border-radius-2rem',
												{}
											)}
										>
											{option.branches ? option.branches.length : 0}{' '}
											<FormattedMessage id="Branches" />
										</Button>
									) : (
										<div className="width-8-rem" />
									)}
								</div>
								{Divider}
							</>
						))
						: null}
				</div>
			</>
		);
	};
	const freeSearchInputRef = React.useRef<any>();

	var searchContainer = (value: any, onChange: any, placeholder: string, className?: string) =>  (
			<div className={`${className ? className : null}`}>
				<InputText
				    value={value}
					searchIcon
					id='search'
					placeholder={placeholder}
					onChange={() => onChange(freeSearchInputRef.current.value)}
					inputRef={freeSearchInputRef}
				/>
			</div>
			// <div className="d-flex-column">

			// 	 <div className="d-flex align-items-center width-25-per pl-1 mb-1 height-2-5-rem py-1 border-radius-2rem" style={{ border: '1px solid #f3f3f5' }}>
			// 		<Icon src={'../' + config.iconsPath + "planogram/sidebar/search.svg"}
			// 			style={{ height: '0.5rem', width: '0.5rem' }} />

			// 		<input placeholder={placeholder} className=" no-border placeholder-text-bold-700 font-small-3 placeholder-text-black text-bold-700 ml-05" type="text"
			// 			value={value} onChange={(e: any) => onChange(e.target.value)} />
			// 	</div>
			// </div>
		);
	const Cube = (option: any) => {
		let id: number = activeTab === TABS.LAYOUT ? option.Id : option.BranchId;
		return (
			<>
				<NavLink key={'option_' + id} to={PLANOGRAM_BASE_URL + '/' + currentTab + '/' + id}>
					<div
						className={classnames(
							'height-12-rem m-1 width-17-rem bg-gray border-radius-05rem d-flex align-items-center text-black text-bold-700 font-medium-2 justify-content-center',
							{
								'text-turquoise': currentBranchId === id,
								'bg-white': currentBranchId === id
							}
						)}
					>
						<div className="d-flex-columm">
							{activeTab === TABS.LAYOUT ? <div className="d-flex justify-content-center align-items-center"> <Layout /> </div> :
								<Icon src={config.iconsPath + 'planogram/branch.svg'} className="margin-auto" />
							}
							<div className="max-width-10-rem elipsis mt-05">{option.Name}</div>
						</div>
					</div>
				</NavLink>
			</>
		);
	};

	return (
		<>
			{!newPlano || !newPlano.length ? (
				<div className="px-2">
					<div className="mb-2 text-black text-bold-700 font-medium-4">
						<FormattedMessage id="planogram_planning" />
					</div>
					<Tabs
						value={activeTab}
						onChangTab={tab => {
							setActiveTab(tab);
							setFilter('')
							let d:any = document.getElementById('search')
							if( d ) d.value = ''
							setDisplayCubes(10);
							setOptions(null);
							history.push(PLANOGRAM_BASE_URL + '/' + (tab === TABS.LAYOUT ? 'layouts' : 'branches'));
						}}
					>
						<Tab className="text-bold-700 font-medium-2" caption={TABS.BRANCHES} name={TABS.BRANCHES} />
						<Tab className="text-bold-700 font-medium-2" caption={TABS.LAYOUT} name={TABS.LAYOUT} />
					</Tabs>
					{!currentBranchId && (
						<>
							<div className="mt-3 mb-1 font-medium-2 text-bold-700 text-black">
								{activeTab === TABS.LAYOUT ? (
									<FormattedMessage id="all_layouts" />
								) : (
									<FormattedMessage id="all branches" />
								)}
								&nbsp;({activeTab === TABS.LAYOUT && layouts ? layouts.length : branches ? branches.length : 0})
							</div>
							{searchContainer(
								filter,
								(value:any) => setFilter(value),
								formatMessage({ id: 'search' }),
								'd-flex align-items-center width-25-per pl-1 mb-1 height-2-5-rem py-1 border-radius-2rem'
							)}
						</>
					)}
					<TabContent activeTab={activeTab}>
						<TabPane tabId={TABS.BRANCHES}>
							{!currentBranch ? (
								<>
									<div className="d-flex flex-wrap justify-content-center">
										{(branches ? filter 
											? branches.filter(branch => branch.Name.includes(filter))
											: branches
											: []).map((branch: any, index: number) =>
												index < disaplyCubes ? Cube(branch) : null
											)}
									</div>

									{branches && branches.length && (filter  ? branches.filter(branch => branch.Name.includes(filter)) : branches).length > disaplyCubes ? (
										<div className="cursor-pointer d-flex align-items-center justify-content-center width-100-per">
											<div
												className="cusror-pointer font-small-4 text-bold-600 text-black"
												onClick={() => setDisplayCubes(disaplyCubes + 5)}
											>
												<FormattedMessage id="view_more" />
											</div>
										</div>
									) : null}
								</>
							) : (
								SelectStore(activeTab)
							)}
						</TabPane>
						<TabPane tabId={TABS.LAYOUT}>
							{!currentBranch ? (
								<>
									<div className="d-flex flex-wrap justify-content-center">
										{layouts && layouts.length
											? filter 
												? layouts
													.filter((layout: any) => layout.Name.includes(filter))
													.map((layout: any, index: number) => {
														if (index < disaplyCubes) return Cube(layout);
														else return null;
													})
												: layouts.map((layout: any, index: number) => {
													if (index > 0 && index < disaplyCubes) return Cube(layout);
													else return null;
												})
											: null}
									</div>

									{layouts && layouts.length > disaplyCubes ?
										(filter ? layouts.filter((layout: any) => layout.Name.includes(filter)) : layouts).length > disaplyCubes && (
											<div className="cursor-pointer d-flex align-items-center justify-content-center width-100-per ">
												<div
													className="cusror-pointer font-small-4 text-bold-600 text-black"
													onClick={() => setDisplayCubes(disaplyCubes + 5)}
												>
													<FormattedMessage id="view_more" />
												</div>
											</div>
										)
										: null
									}
								</>
							) : (
								SelectStore(activeTab)
							)}
						</TabPane>
					</TabContent>
				</div>
			) : null}
			{currentBranchId ? (
				<Reports
					showNewAlgoPoup={showNewAlgoPoup}
					setShowNewAlgoPoup={setShowNewAlgoPoup}
					layoutId={currentBranchId}
				/>
			) : null}

			{modalType === ModalTypes.delete ? (
				<DeleteModalData
					modalHandlers={() => {
						activeTab === TABS.LAYOUT ? handleLayoutDelete() : handleBranchDelete();
					}}
					message={<>
						{formatMessage({ id: 'AreYouSureYouWantToDelete:' })} {optionToDelete.name}?
						{activeTab === TABS.LAYOUT && <p>{formatMessage({ id: 'This action will delete the planogram from all the related branches' })}</p>}
					</>}
					modalType={modalType}
					toggleModal={(type: ModalTypes) => toggleModal(type)}
				/>
			) : null}

			{modalType === ModalTypes.info ? (
				<InfoModalData
					modalHandlers={() => handlePrimaryChange()}
					message={<>
						{formatMessage({ id: 'Are you sure you want to change primary status for this store?' })}
					</>}
					modalType={modalType}
					toggleModal={(type: ModalTypes) => toggleModal(type)}
				/>
			) : null}

			{modalType === ModalTypes.add && currentAisleId && currentAisle ? (
				<BranchAssignment
					options={options.find((ele: any) => ele.aisle_id === currentAisleId).branches}
					setBranches={(branches: any[]) => {
						let newOptions = [
							...options.map((option: any, oi: number) => {
								if (currentAisleIndex === oi) {
									return { ...option, branches };
								} else return option;
							})
						];
						setOptions(newOptions);
					}}
					oldStores={
						currentAisle && currentAisle.branches.length
							? currentAisle.branches.map((branch: any) => branch.id)
							: []
					}
					toggleModal={(type: ModalTypes) => toggleModal(type)}
					modalType={modalType}
					searchContainer={searchContainer}
				/>
			) : null}
		</>
	);
};

export default Selection;
