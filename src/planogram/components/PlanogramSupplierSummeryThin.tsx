import * as React from "react"
import { connect } from 'react-redux'
import { DateBox, Button, TagBox, DataGrid } from 'devextreme-react';
import { Popup as OutsidePopup } from 'devextreme-react/popup';
import SelectBox from 'devextreme-react/select-box';
import { Column, Texts, Sorting, Scrolling, HeaderFilter, FilterRow, Export, Summary, TotalItem, GroupPanel, GroupItem, Grouping, ColumnChooser, SortByGroupSummaryInfo } from 'devextreme-react/data-grid'
import DataSource from 'devextreme/data/data_source'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { AnyAction, bindActionCreators } from 'redux'
import { ThunkDispatch } from 'redux-thunk'
import { Rnd } from 'react-rnd';

import { AppState } from '@src/planogram/shared/store/app.reducer'
import {
    fetchCatalogSales, CatalogSaleRecord, fetchTrumaSnif, fetchTruma,
    fetchTrumaSnifByGroup, fetchTrumaSnifBySubGroup, fetchTrumaSnifByDepartment,
    fetchTrumaByGroup, fetchTrumaBySubGroup, fetchTrumaByDepartment, fetchTotalShelfWidthForStore,
    fetchTotalShelfPerSupplierPerStore,
    fetchTotalShelfPerSupplierPerAisle,
    fetchTotalShelfForAisle,
    fetchTrumaAisleSnif,
    fetchTrumaByAisle,
    fetchTrumaSnifBySelection,
    fetchTrumaBySelection,
    getBranchByStore,
    fetchTotalShelfPerItemPerAisle,
    fetchTotalShelfPerItemPerAisleArr,
    fetchTrumaSnifBySelectionArr,
    fetchTrumaBySelectionArr,
    setUserReportState,
    getUserReportState
} from '@src/planogram/shared/api/sales.provider';
// import { SidebarProductDragable } from './SidebarProductDragable';
import { faEye, faEyeSlash, faFilter, faWindowClose } from '@fortawesome/free-solid-svg-icons';
import { uiNotify } from '@src/planogram/shared/components/Toast';
// import EditableBarcodeStatus from './SaleReport/EditableBarcodeStatus'
import { fetchBarcodeStatuses } from '@src/planogram/shared/api/catalog.provider'
import { setBarcodeStatuses } from '@src/planogram/shared/store/catalog/catalog.action'
import { hideColorPopUp, hideSalesReport, SetSummaryObjects, showColorPopUp } from '@src/planogram/shared/store/planogram/display/display.actions'
import { toggleSettings, toggleRowItems, setColorBy } from '@src/planogram/shared/store/planogram/planogram.actions'
import { CatalogBarcode } from '@src/planogram/shared/interfaces/models/CatalogProduct'
import { deepExtractKey } from '@src/planogram/shared/store/planogram/virtualize/deep-extract'
import { getIndexUniqueColor } from '@src/planogram/shared/store/planogram/virtualize/colors.provider'
import { ColorMap } from '@src/planogram/shared/store/planogram/virtualize/virtualize.reducer'
import Draggable from 'react-draggable';
import { PlanogramAisle, PlanogramElementId } from '@src/planogram/shared/store/planogram/planogram.types';
import { setAisleAction } from '@src/planogram/shared/store/planogram/store/aisle/aisle.actions';

type ReportSaleRecord = CatalogSaleRecord & {
    InStore: boolean,
/*Barak 13.1.20*/ Name: string,
};

// based on ColorMap in colors.provider.ts
const ColorMapRev = [
    {
        Color: '#0be30b',
        Name: 'green'
    },
    {
        Color: '#ffd700',
        Name: 'gold'
    },
    {
        Color: '#00ffff',
        Name: 'cyan'
    },
    {
        Color: '#0000ff',
        Name: 'blue'
    },
    {
        Color: '#ff00ff',
        Name: 'fuchsia'
    },
    {
        Color: '#ffc0cb',
        Name: 'pink'
    },
    {
        Color: '#4b0082',
        Name: 'indigo'
    },
    {
        Color: '#808000',
        Name: 'olive'
    },
    {
        Color: '#ffa500',
        Name: 'orange'
    },
    {
        Color: '#ff0000',
        Name: 'red'
    },
    {
        Color: '#008080',
        Name: 'teal'
    },
    {
        Color: '#00008b',
        Name: 'darkblue'
    },
    {
        Color: '#f0e68c',
        Name: 'khaki'
    },
    {
        Color: '#f5f5dc',
        Name: 'beige'
    },
    {
        Color: '#a52a2a',
        Name: 'brown'
    },
    {
        Color: '#800080',
        Name: 'purple'
    },
    {
        Color: '#FFCC99',
        Name: 'honeydew'
    },
    {
        Color: '#ffff00',
        Name: 'yellow'
    },
    {
        Color: '#006400',
        Name: 'darkgreen'
    },
    {
        Color: '#bdb76b',
        Name: 'darkkhaki'
    },
    {
        Color: '#8b008b',
        Name: 'darkmagenta'
    },
    {
        Color: '#556b2f',
        Name: 'darkolivegreen'
    },
    {
        Color: '#ff8c00',
        Name: 'darkorange'
    },
    {
        Color: '#9932cc',
        Name: 'darkorchid'
    },
    {
        Color: '#8b0000',
        Name: 'darkred'
    },
    {
        Color: '#e9967a',
        Name: 'darksalmon'
    },
    {
        Color: '#9400d3',
        Name: 'darkviolet'
    },
    {
        Color: '#808080',
        Name: 'iron'
    },
    {
        Color: '#add8e6',
        Name: 'lightblue'
    },
    {
        Color: '#e0ffff',
        Name: 'lightcyan'
    },
    {
        Color: '#90ee90',
        Name: 'lightgreen'
    },
    {
        Color: '#d3d3d3',
        Name: 'lightgrey'
    },
    {
        Color: '#ffb6c1',
        Name: 'lightpink'
    },
    {
        Color: '#ffffe0',
        Name: 'lightyellow'
    },
    {
        Color: '#c0c0c0',
        Name: 'silver'
    },
    {
        Color: '#000000',
        Name: 'black'
    }
];

const mapStateToProps = (state: AppState, ownProps: any) => ({
    user: state.auth.user,
    products: state.catalog.products,
    productsMap: state.newPlano ? state.newPlano.productsMap : null,
    aisle: state.planogram.store != null && state.planogram.display.aisleIndex != null ? state.planogram.store.aisles[state.planogram.display.aisleIndex] : null,
    displayAisle: state.planogram.display.aisleIndex != null
        && state.planogram.store
        && state.planogram.store.aisles[state.planogram.display.aisleIndex] ? state.planogram.store.aisles[state.planogram.display.aisleIndex].aisle_id : null,
    displaySalesReport: state.planogram.display.displaySalesReport,
    virtualProductDetails: state.planogram.productDetails,
    virtualStore: state.planogram.virtualStore,
    planogram: state.planogram,
    suppliers: state.system.data.suppliers,
    classes: state.system.data.classes,
    groups: state.system.data.groups,
    subGroups: state.system.data.subGroups,
    segments: state.system.data.segments,
    models: state.system.data.models,
    branches: state.system.data.branches,
    series: state.system.data.series,

    suppliersMap: state.system.data.suppliersMap,
    colorMap: state.planogram.virtualStore.colorMap,
    showColorMap: state.planogram.display.colorBy != null,
    displayColorPopUp: state.planogram.display.displayColorPopUp,
})

const mapDispatchToProps = (dispatch: ThunkDispatch<AppState, {}, AnyAction>) => ({
    fetchBarcodeStatuses: () => fetchBarcodeStatuses()
        .then(statuses => dispatch(setBarcodeStatuses(statuses)))
        .catch((err) => {
            uiNotify("Unable to load network barcode statuses")
        }),
    hideSalesReport: () => dispatch(hideSalesReport()),
    /* Barak 7.9.20 *  toggleColorMap: (showColorMap: boolean) => dispatch(showColorMap ? setColorBy(null) : setColorBy("supplier")), */
    /* Barak 7.9.20 */
    toggleColorMap: (showColorMap: boolean) => dispatch(showColorMap ? setColorBy(null) : setColorBy("report")),
    setColorBy: (setBy: string) => dispatch(setColorBy(setBy)),
    /****************/
    /* Barak 29.12.20 */
    toggleColorPopup: (show: boolean) => {
        if (show)
            dispatch(hideColorPopUp());
        else
            dispatch(showColorPopUp());
    },
    showColorPopUp: () => { dispatch(showColorPopUp()) },
    hideColorPopUp: () => { dispatch(hideColorPopUp()) },
    setStoreAisle: (aisle: PlanogramAisle, aislePid?: PlanogramElementId) => {
        dispatch(setAisleAction(aisle, aislePid));
    },
    setSummaryObjects: (summaryObjects: any) => { dispatch(SetSummaryObjects(summaryObjects)) },
    /*****************/
})

/* Barak 23.1.20 */ let Const_Truma_Records: any[] = [];

/* Barak 2.8.20 - https://medium.com/swlh/how-to-round-to-a-certain-number-of-decimal-places-in-javascript-ed74c471c1b8*/
// const roundAccurately = (number:number, decimalPlaces:number) =>
//     Number(Math.round(Number(number + "e" + decimalPlaces)) + "e-" + decimalPlaces);
const roundAccurately = (number: number, decimalPlaces: number) => {
    const factorOfTen = Math.pow(10, decimalPlaces);
    return Math.round(number * factorOfTen) / factorOfTen;
}
/****************/

type PlanogramSupplierSummeryThinProps = ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>
type PlanogramSupplierSummeryThinState = {
    loading: boolean,
    pageIndex: number,
    pageSize: number,
    salesRecords: ReportSaleRecord[],
    searchWord: string,
    height: number,
    width: number,
    trumaSnifRecords: any[],
    selectedGroup: number[],
    selectedSubGroup: number[],
    selectedDepartment: number[],
    /* Barak 9.6.20 */ selectedSegment: number[],
    /* Barak 6.12.20 */ selectedModel: number[],
    supplierObjects: any[],
    currentAisleOnly: boolean,
    /* Barak 2.8.20 */ colorMap: ColorMap,
    /* Barak 2.8.20 */ firstRun: boolean,
    /* Barak 7.9.20 */
    suppColorMap: ColorMap,
    classColorMap: ColorMap,
    groupColorMap: ColorMap,
    subGroupColorMap: ColorMap,
    segmentColorMap: ColorMap,
    modelColorMap: ColorMap,
    isSuppColorVisible: boolean,
    isClassColorVisible: boolean,
    isGroupColorVisible: boolean,
    isSubGroupColorVisible: boolean,
    isSegmentColorVisible: boolean,
    isModelColorVisible: boolean,
    /****************/
    // isPopupVisible: boolean,
};


class TrumaCollection extends Array {
    sum(key: string) {
        return this.reduce((a, b) => a + (b[key] || 0), 0);
    }
}

class PlanogramSupplierSummeryThin extends React.Component<PlanogramSupplierSummeryThinProps, PlanogramSupplierSummeryThinState> {
    dataGrid: any | null = null;
    /* Barak 6.9.20 */ selectSegmentRef: any =  React.createRef();
    selectSubGroupRef: any =  React.createRef();
    selectGroupRef: any =  React.createRef();
    selectDepartmentRef: any =  React.createRef();
    /* Barak 7.9.20 */
    colorSupp: boolean = false;
    colorClass: boolean = false;
    colorGroup: boolean = false;
    colorSubGroup: boolean = false;
    colorSegment: boolean = false;
    aboutToClose: boolean = false;
    /****************/
    colorModel: boolean = false;
    state: PlanogramSupplierSummeryThinState = {
        loading: false,
        pageIndex: 0,
        pageSize: 50,
        searchWord: "",
        // width: window.innerWidth * 0.5656,
        width: window.innerWidth * 0.6,
        height: window.innerHeight * 0.5656,
        salesRecords: [],
        /*Barak 23.1.20 trumaSnifRecords*/
        trumaSnifRecords: [],
        selectedGroup: [],
        selectedSubGroup: [],
        selectedDepartment: [],
        /* Barak 9.6.20 */ selectedSegment: [],
        selectedModel: [],
        supplierObjects: [],
        currentAisleOnly: false,
        /* Barak 2.8.20 */ colorMap: {},
        /* Barak 2.8.20 */ firstRun: true,
        /* Barak 7.9.20 */
        suppColorMap: {},
        classColorMap: {},
        groupColorMap: {},
        subGroupColorMap: {},
        segmentColorMap: {},
        modelColorMap: {},
        isSuppColorVisible: false,
        isClassColorVisible: false,
        isGroupColorVisible: false,
        isSubGroupColorVisible: false,
        isSegmentColorVisible: false,
        isModelColorVisible: false,
        /****************/
        // isPopupVisible: false,
    }
    get selectSubGroup() {
        return this.selectSubGroupRef.current.instance;
    }
    get selectGroup() {
        return this.selectGroupRef.current.instance;
    }
    get selectDepartment() {
        return this.selectDepartmentRef.current.instance;
    }

    componentDidMount() {
        this.props.fetchBarcodeStatuses();
    }
    getRandomColor() {
        let letters = '0123456789ABCDEF';
        let color = '#';
        for (let i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }
    /* Barak 2.8.20 */
    calculateSupplierPercentInit = async (trumaSnifRecords: any[], currentAisleOnly: boolean): Promise<any[]> => {
        console.log('calculateSupplierPercentInit', trumaSnifRecords);
        const { virtualProductDetails, virtualStore, productsMap } = this.props;
        const url = window.location.href;
        const urlElements = url.split('/');
        const currentBranch = parseInt(urlElements[5]);
        const currentStore = parseInt(urlElements[6]);
        const currentAisle = parseInt(urlElements[8]);

        let filteredTrumaSnifRecords: any[];

        console.log('currentAisle', currentAisle, 'virtualStore.aisleMap[currentAisle]', virtualStore.aisleMap[currentAisle]);

        /* Barak 2.8.20 */ this.setState({
            colorMap: virtualStore.aisleMap[currentAisle].colorMap,
            /* Barak 7.9.20 */
            suppColorMap: virtualStore.aisleMap[currentAisle].suppColorMap,
            classColorMap: virtualStore.aisleMap[currentAisle].classColorMap,
            groupColorMap: virtualStore.aisleMap[currentAisle].groupColorMap,
            subGroupColorMap: virtualStore.aisleMap[currentAisle].subGroupColorMap,
            segmentColorMap: virtualStore.aisleMap[currentAisle].segmentColorMap,
            /****************/
            modelColorMap: virtualStore.aisleMap[currentAisle].modelColorMap,
        });

        /************************************* Barak 7.9.20 ******************************************/
        console.log('calculateItemPercentInit currentAisle', currentAisle);
        const { selectedGroup, selectedSubGroup, selectedDepartment, selectedSegment } = this.state;

        let ColorMap: any[] = [];
        let ColorMapSupp: any[] = [];
        let ColorMapClass: any[] = [];
        let ColorMapGroup: any[] = [];
        let ColorMapSubGroup: any[] = [];
        let ColorMapSegment: any[] = [];
        let ColorMapModel: any[] = [];
        let itemObjects: any[] = [];

        let totalShelfForCategory = 0;
        // let totalShelfItem = await fetchTotalShelfPerItemPerAisle(currentAisle, selectedDepartment, selectedGroup, selectedSubGroup, selectedSegment);
        let totalShelfItem = await fetchTotalShelfPerItemPerAisleArr(currentAisle, { class: selectedDepartment, group: selectedGroup, subGroup: selectedSubGroup, segment: selectedSegment });
        let totalShelfForAisle = await fetchTotalShelfForAisle(currentAisle);

        let totalSalesInNetwork = new TrumaCollection(...Const_Truma_Records).sum('Amount');
        let totalSalesInBranch = new TrumaCollection(...trumaSnifRecords).sum('Amount');
        console.log('totalSalesInNetwork', totalSalesInNetwork, 'totalSalesInBranch', totalSalesInBranch);

        for (let i = 0; i < totalShelfItem.length; i++) {
            let itemRecord = totalShelfItem[i];
            if (itemRecord) totalShelfForCategory += itemRecord.Total;
            let totalItemSalesInNetwork = new TrumaCollection(...Const_Truma_Records.filter(obj => obj.BarCode === totalShelfItem[i].BarCode)).sum('Amount');
            let totalItemSalesInBranch = new TrumaCollection(...trumaSnifRecords.filter(obj => obj.BarCode === totalShelfItem[i].BarCode)).sum('Amount');
            let itemObject = {
                BarCode: itemRecord.BarCode,
                Name: this.props.productsMap[itemRecord.BarCode].Name,
                SupplierId: itemRecord.SapakId,
                SupplierName: itemRecord.SupplierName,
                ClassId: itemRecord.ClassesId,
                ClassName: itemRecord.ClassName,
                GroupId: itemRecord.GroupId,
                GroupName: itemRecord.GroupName,
                SubGroupId: itemRecord.SubGroupId,
                SubGroupName: itemRecord.SubGroupName,
                SegmentId: itemRecord.SegmentId,
                SegmentName: itemRecord.SegmentName,
                ModelId: itemRecord.ModelId,
                ModelName: itemRecord.ModelName,
                // SupplierColor: this.getRandomColor(),
                SupplierColor: '',
                ClassColor: '',
                GroupColor: '',
                SubGroupColor: '',
                SegmentColor: '',
                ModelColor: '',
                Color: '',
                ColorName: '',
                TotalFaces: itemRecord.TotalFaces,
                Width: itemRecord.width,
                RecomendedFaces: 0,
                PercentFromNetwork: (totalItemSalesInNetwork / totalSalesInNetwork) * 100,
                PercentFromBranch: (totalItemSalesInBranch / totalSalesInBranch) * 100,
                PercentFromShelf: 0,
                PercentFromShelfSold: 0
            }
            itemObjects.push(itemObject);
            let colorObjSupp = {
                SupplierId: itemRecord.SapakId,
                color: itemObject.SupplierColor
            }
            ColorMapSupp.push(colorObjSupp);
            let colorObjClass = {
                ClassId: itemRecord.ClassesId,
                color: itemObject.ClassColor
            }
            ColorMapClass.push(colorObjClass);
            let colorObjGroup = {
                GroupId: itemRecord.GroupId,
                color: itemObject.GroupColor
            }
            ColorMapGroup.push(colorObjGroup);
            let colorObjSubGroup = {
                SubGroupId: itemRecord.SubGroupId,
                color: itemObject.SubGroupColor
            }
            ColorMapSubGroup.push(colorObjSubGroup);
            let colorObjSegment = {
                SegmentId: itemRecord.SegmentId,
                color: itemObject.SegmentColor
            }
            ColorMapSegment.push(colorObjSegment);
            let colorObjModel = {
                ModelId: itemRecord.ModelId,
                color: itemObject.ModelColor
            }
            ColorMapModel.push(colorObjModel);
        }

        console.log('itemObjects 0', itemObjects, itemObjects.length);

        for (let i = 0; i < itemObjects.length; i++) {
            let itemRecord = totalShelfItem.filter(obj => obj.BarCode === itemObjects[i].BarCode)[0];
            let itemIndex = itemObjects.findIndex(obj => obj.BarCode === itemObjects[i].BarCode);
            itemObjects[itemIndex].PercentFromShelf = itemRecord ? (itemRecord.Total / totalShelfForAisle) * 100 : 0;
            itemObjects[itemIndex].PercentFromShelfSold = itemRecord ? (itemRecord.Total / totalShelfForCategory) * 100 : 0;
            itemObjects[itemIndex].RecomendedFaces = Math.floor((totalShelfForAisle * 0.8 * (itemObjects[itemIndex].PercentFromBranch / 100)) / itemObjects[itemIndex].Width);
        }

        console.log('itemObjects 1', itemObjects, itemObjects.length);

        // sort itemObjects by a combination of PercentFromBranch, PercentFromNetwork and PercentFromShelf
        itemObjects.sort(function (a: any, b: any) {
            let aNumber = parseFloat(a.PercentFromBranch) + parseFloat(a.PercentFromNetwork);
            let bNumber = parseFloat(b.PercentFromBranch) + parseFloat(b.PercentFromNetwork);
            return (aNumber < bNumber) ? 1 : (aNumber > bNumber) ? -1 : 0;
        });

        for (let i = 0; i < itemObjects.length; i++) {
            itemObjects[i].SupplierColor = virtualStore.aisleMap[currentAisle].suppColorMap[itemObjects[i].SupplierId];
            itemObjects[i].ClassColor = virtualStore.aisleMap[currentAisle].classColorMap[itemObjects[i].ClassId];
            itemObjects[i].GroupColor = virtualStore.aisleMap[currentAisle].groupColorMap[itemObjects[i].GroupId];
            itemObjects[i].SubGroupColor = virtualStore.aisleMap[currentAisle].subGroupColorMap[itemObjects[i].SubGroupId];
            itemObjects[i].SegmentColor = virtualStore.aisleMap[currentAisle].segmentColorMap[itemObjects[i].SegmentId];
            itemObjects[i].ModelColor = virtualStore.aisleMap[currentAisle].modelColorMap[itemObjects[i].ModelId];
        }

        /* Barak 29.12.20 */ await this.props.setSummaryObjects(itemObjects);

        return itemObjects;
    }
    /*********************************************************************************************************/
    calculateSupplierPercent = async (trumaSnifRecords: any[], currentAisleOnly: boolean): Promise<any[]> => {
        const { virtualProductDetails, virtualStore } = this.props;
        const url = window.location.href;
        const urlElements = url.split('/');
        const currentBranch = parseInt(urlElements[5]);
        const currentStore = parseInt(urlElements[6]);
        const currentAisle = parseInt(urlElements[8]);

        /* Barak 2.8.20 */ this.setState({
            colorMap: virtualStore.aisleMap[currentAisle].colorMap,
            /* Barak 7.9.20 */
            suppColorMap: virtualStore.aisleMap[currentAisle].suppColorMap,
            classColorMap: virtualStore.aisleMap[currentAisle].classColorMap,
            groupColorMap: virtualStore.aisleMap[currentAisle].groupColorMap,
            subGroupColorMap: virtualStore.aisleMap[currentAisle].subGroupColorMap,
            segmentColorMap: virtualStore.aisleMap[currentAisle].segmentColorMap,
            /****************/
            modelColorMap: virtualStore.aisleMap[currentAisle].modelColorMap,
        });

        let filteredTrumaSnifRecords: any[];

        /************************************* Barak 7.9.20 ******************************************/
        console.log('calculateItemPercentInit currentAisle', currentAisle);
        const { selectedGroup, selectedSubGroup, selectedDepartment, selectedSegment } = this.state;

        let itemObjects: any[] = [];

        let totalShelfForCategory = 0;
        // let totalShelfItem = await fetchTotalShelfPerItemPerAisle(currentAisle, selectedDepartment, selectedGroup, selectedSubGroup, selectedSegment);
        let totalShelfItem = await fetchTotalShelfPerItemPerAisleArr(currentAisle, { class: selectedDepartment, group: selectedGroup, subGroup: selectedSubGroup, segment: selectedSegment });
        let totalShelfForAisle = await fetchTotalShelfForAisle(currentAisle);

        let totalSalesInNetwork = new TrumaCollection(...Const_Truma_Records).sum('Amount');
        let totalSalesInBranch = new TrumaCollection(...trumaSnifRecords).sum('Amount');
        console.log('totalSalesInNetwork', totalSalesInNetwork, 'totalSalesInBranch', totalSalesInBranch);

        for (let i = 0; i < totalShelfItem.length; i++) {
            let itemRecord = totalShelfItem[i];
            if (itemRecord) totalShelfForCategory += itemRecord.Total;
            let totalItemSalesInNetwork = new TrumaCollection(...Const_Truma_Records.filter(obj => obj.BarCode === totalShelfItem[i].BarCode)).sum('Amount');
            let totalItemSalesInBranch = new TrumaCollection(...trumaSnifRecords.filter(obj => obj.BarCode === totalShelfItem[i].BarCode)).sum('Amount');
            let itemObject = {
                BarCode: itemRecord.BarCode,
                Name: this.props.productsMap[itemRecord.BarCode].Name,
                SupplierId: itemRecord.SapakId,
                SupplierName: itemRecord.SupplierName,
                ClassId: itemRecord.ClassesId,
                ClassName: itemRecord.ClassName,
                GroupId: itemRecord.GroupId,
                GroupName: itemRecord.GroupName,
                SubGroupId: itemRecord.SubGroupId,
                SubGroupName: itemRecord.SubGroupName,
                SegmentId: itemRecord.SegmentId,
                SegmentName: itemRecord.SegmentName,
                ModelId: itemRecord.ModelId,
                ModelName: itemRecord.ModelName,
                TotalFaces: itemRecord.TotalFaces,
                RecomendedFaces: 0,
                Width: itemRecord.width,
                SupplierColor: virtualStore.aisleMap[currentAisle].suppColorMap[itemRecord.SapakId ? itemRecord.SapakId : "none"],
                ClassColor: virtualStore.aisleMap[currentAisle].classColorMap[itemRecord.ClassId ? itemRecord.ClassId : "none"],
                GroupColor: virtualStore.aisleMap[currentAisle].groupColorMap[itemRecord.GroupId ? itemRecord.GroupId : "none"],
                SubGroupColor: virtualStore.aisleMap[currentAisle].subGroupColorMap[itemRecord.SubGroupId ? itemRecord.SubGroupId : "none"],
                SegmentColor: virtualStore.aisleMap[currentAisle].segmentColorMap[itemRecord.SegmentId ? itemRecord.SegmentId : "none"],
                ModelColor: virtualStore.aisleMap[currentAisle].modelColorMap[itemRecord.ModelId ? itemRecord.ModelId : "none"],
                Color: '',
                ColorName: '',
                PercentFromNetwork: (totalItemSalesInNetwork / totalSalesInNetwork) * 100,
                PercentFromBranch: (totalItemSalesInBranch / totalSalesInBranch) * 100,
                PercentFromShelf: 0,
                PercentFromShelfSold: 0
            }
            itemObjects.push(itemObject);
        }

        console.log('itemObjects 0', itemObjects);

        for (let i = 0; i < itemObjects.length; i++) {
            let itemRecord = totalShelfItem.filter(obj => obj.BarCode === itemObjects[i].BarCode)[0];
            let itemIndex = itemObjects.findIndex(obj => obj.BarCode === itemObjects[i].BarCode);
            itemObjects[itemIndex].PercentFromShelf = itemRecord ? (itemRecord.Total / totalShelfForAisle) * 100 : 0;
            itemObjects[itemIndex].PercentFromShelfSold = itemRecord ? (itemRecord.Total / totalShelfForCategory) * 100 : 0;
            itemObjects[itemIndex].RecomendedFaces = Math.floor((totalShelfForAisle * 0.8 * (itemObjects[itemIndex].PercentFromBranch / 100)) / itemObjects[itemIndex].Width);
        }

        console.log('itemObjects 1', itemObjects);

        if (currentAisleOnly && currentAisle != null) {
            filteredTrumaSnifRecords = trumaSnifRecords.filter(v =>
                virtualProductDetails[v.BarCode]
                && virtualProductDetails[v.BarCode].position
                && virtualProductDetails[v.BarCode].position.findIndex(p => p.aisle_id === currentAisle) !== -1);

            let barcodes = filteredTrumaSnifRecords.map(obj => { return obj.BarCode; });
            barcodes = barcodes.filter((v, i) => { return barcodes.indexOf(v) == i; });
            itemObjects = itemObjects.filter(function (item) {
                return barcodes.indexOf(item.BarCode) !== -1;
            });
        }

        // sort itemObjects by a combination of PercentFromBranch, PercentFromNetwork and PercentFromShelf
        itemObjects.sort(function (a: any, b: any) {
            let aNumber = parseFloat(a.PercentFromBranch) + parseFloat(a.PercentFromNetwork);
            let bNumber = parseFloat(b.PercentFromBranch) + parseFloat(b.PercentFromNetwork);
            return (aNumber < bNumber) ? 1 : (aNumber > bNumber) ? -1 : 0;
        });

        /* Barak 29.12.20 */ await this.props.setSummaryObjects(itemObjects);

        return itemObjects;
    }

    onSearch = async (currentAisleOnly: boolean) => {
        const { selectedGroup, selectedSubGroup, selectedDepartment,
            /* Barak 9.6.20 */ selectedSegment
        } = this.state;

        const url = window.location.href;
        const urlElements = url.split('/');
        const currentBranch = parseInt(urlElements[5]);
        const currentStore = parseInt(urlElements[6]);

        // const currentBranch = await getBranchByStore(currentStore);

        /* Barak 2.8.20 *
        if (!selectedGroup && !selectedSubGroup && !selectedDepartment)
            return console.log("No category in search");
        *****************/

        this.setState({
            loading: true,
            /* Barak 2.8.20 - clear the slate on each search */ supplierObjects: [],
            /* Barak 2.8.20 */ firstRun: false,
        });
        /* Barak 29.12.20 */ await this.props.setSummaryObjects([]);

        /* Barak 2.8.20 - get the suppliers relevent for this Aisle only based on items sold in this aisle */
        if (selectedGroup || selectedSubGroup || selectedDepartment || /* Barak 9.6.20 */ selectedSegment) {
            console.log('selectedDepartment', selectedDepartment, 'selectedGroup', selectedGroup, 'selectedSubGroup', selectedSubGroup);
            let department = selectedDepartment ? selectedDepartment : 0;
            let group = selectedGroup ? selectedGroup : 0;
            let subgroup = selectedSubGroup ? selectedSubGroup : 0;
            /* Barak 9.6.20 */ let segment = selectedSegment ? selectedSegment : 0;
            console.log('after selection', selectedGroup, selectedSubGroup, selectedDepartment, selectedSegment);
            // fetchTrumaSnifBySelection(currentBranch, department, group, subgroup, segment).then((trumasnifRecords) => {
            //    fetchTrumaBySelection(department, group, subgroup, segment).then(async (trumaRecords) => {
            fetchTrumaSnifBySelectionArr({ branch: [currentBranch], class: selectedDepartment, group: selectedGroup, subGroup: selectedSubGroup, segment: selectedSegment }).then((trumasnifRecords) => {
                fetchTrumaBySelectionArr({ class: selectedDepartment, group: selectedGroup, subGroup: selectedSubGroup, segment: selectedSegment }).then(async (trumaRecords) => {
                    Const_Truma_Records = trumaRecords && trumaRecords.length > 1 ? trumaRecords : [];
                    this.setState({ supplierObjects: await this.calculateSupplierPercent(trumasnifRecords, currentAisleOnly), loading: false });
                    this.checkVisibleColor();
                }).catch((err) => {
                    console.error(err);
                    uiNotify("Unable to load truma table", "error");
                    this.setState({
                        loading: false
                    })
                })
            }).catch((err) => {
                console.error(err);
                uiNotify("Unable to load sales", "error");
                this.setState({
                    loading: false
                })
            })
        }
        if ((!selectedGroup || selectedGroup === []) && (!selectedSubGroup || selectedSubGroup === []) && (!selectedDepartment || selectedDepartment === [])) {
            console.log('no selection');
            let url = window.location.href;
            const urlElements = url.split('/');
            const currentAisle = parseInt(urlElements[8]);
            fetchTrumaAisleSnif(currentBranch, currentAisle).then((trumasnifRecords) => {
                fetchTrumaByAisle(currentAisle).then(async (trumaRecords) => {
                    Const_Truma_Records = trumaRecords && trumaRecords.length > 1 ? trumaRecords : [];
                    this.setState({ supplierObjects: await this.calculateSupplierPercentInit(trumasnifRecords, true), loading: false });
                    this.checkVisibleColor();
                }).catch((err) => {
                    console.error(err);
                    uiNotify("Unable to load truma table", "error");
                    this.setState({
                        loading: false
                    })
                })
            }).catch((err) => {
                console.error(err);
                uiNotify("Unable to load sales", "error");
                this.setState({
                    loading: false
                })
            })
        }
        /****************/
        /* Barak 2.8.20 *
        this.setState({
            loading: false
        });
        ****************/
    }

    selectCategorySegment = (value: any) => {
        if (value) {
            this.setState({ selectedSegment: value });
        } else {
            this.setState({ selectedSegment: value });
        }
    }

    selectCategorySubGroup = (value: any) => {
        if (value) {
            this.setState({ selectedSubGroup: value });
            // this.selectGroup.option('disabled', true);
            // this.selectDepartment.option('disabled', true);
        } else {
            this.setState({ selectedSubGroup: value });
            // this.selectGroup.option('disabled', false);
            // this.selectDepartment.option('disabled', false);
        }
    }

    selectCategoryGroup = (value: any) => {
        if (value) {
            this.setState({ selectedGroup: value });
            // this.selectSubGroup.option('disabled', true);
            // this.selectDepartment.option('disabled', true);
        } else {
            this.setState({ selectedGroup: value });
            // this.selectSubGroup.option('disabled', false);
            // this.selectDepartment.option('disabled', false);
        }
    }

    selectCategoryDepartment = (value: any) => {
        if (value) {
            this.setState({ selectedDepartment: value });
            // this.selectSubGroup.option('disabled', true);
            // this.selectGroup.option('disabled', true);
        } else {
            this.setState({ selectedDepartment: value });
            // this.selectSubGroup.option('disabled', false);
            // this.selectGroup.option('disabled', false);
        }
    }

    showPopup() {
        // this.setState({ isPopupVisible: true });
        this.props.showColorPopUp();
    }

    hidePopup() {
        // this.setState({ isPopupVisible: false });
        this.props.hideColorPopUp();
    }

    /* Barak 29.9.20 - callXlsOutput, xlsOutput */
    xlsOutput = (e: any) => {
        // write JSON to file on client side: https://stackoverflow.com/questions/53449406/write-to-a-text-or-json-file-in-react-with-node/53449617#53449617

        const { supplierObjects } = this.state;

        let url = window.location.href;
        const urlElements = url.split('/');
        const currentAisle = parseInt(urlElements[8]);

        // now that I have all the information I'm building a new JSON for the excel output
        let length = supplierObjects.length;
        let reportJSON = [];
        for (let i = 0; i < length; i++) {
            let itemRecord = supplierObjects[i];
            let object = {
                BarCode: itemRecord.BarCode,
                Name: itemRecord.Name.replace('"', "''"),
                SupplierId: itemRecord.SupplierId,
                SupplierName: itemRecord.SupplierName.replace('"', "''"),
                ClassId: itemRecord.ClassId,
                ClassName: itemRecord.ClassName.replace('"', "''"),
                GroupId: itemRecord.GroupId,
                GroupName: itemRecord.GroupName.replace('"', "''"),
                SubGroupId: itemRecord.SubGroupId,
                SubGroupName: itemRecord.SubGroupName.replace('"', "''"),
                SegmentId: itemRecord.SegmentId,
                SegmentName: itemRecord.SegmentName.replace('"', "''"),
                ModelId: itemRecord.ModelId,
                ModelName: itemRecord.ModelName.replace('"', "''"),
                TotalFaces: itemRecord.TotalFaces,
                RecomendedFaces: itemRecord.RecomendedFaces,
                Width: itemRecord.Width,
                SupplierColor: itemRecord.SupplierColor,
                ClassColor: itemRecord.ClassColor,
                GroupColor: itemRecord.GroupColor,
                SubGroupColor: itemRecord.SubGroupColor,
                SegmentColor: itemRecord.SegmentColor,
                ModelColor: itemRecord.ModelColor,
                // round results to 2 decimal points
                PercentFromNetwork: Math.round(itemRecord.PercentFromNetwork * Math.pow(10, 2)) / Math.pow(10, 2),
                PercentFromBranch: Math.round(itemRecord.PercentFromBranch * Math.pow(10, 2)) / Math.pow(10, 2),
                PercentFromShelf: Math.round(itemRecord.PercentFromShelf * Math.pow(10, 2)) / Math.pow(10, 2),
                PercentFromShelfSold: Math.round(itemRecord.PercentFromShelfSold * Math.pow(10, 2)) / Math.pow(10, 2)
            }
            reportJSON.push(object);
        }

        // https://www.npmjs.com/package/json2csv
        const { parse } = require('json2csv');
        // let fields = ['BarCode', 'Name', 'SupplierId', 'SupplierName', 'ClassId', 'ClassName',
        //     'GroupId', 'GroupName', 'SubGroupId', 'SubGroupName', 'SegmentId', 'SegmentName',
        //     'ModelId', 'ModelName', 'TotalFaces', 'RecomendedFaces', 'Width', 'SupplierColor',
        //     'ClassColor', 'GroupColor', 'SubGroupColor', 'SegmentColor', 'ModelColor',
        //     'PercentFromNetwork', 'PercentFromBranch', 'PercentFromShelf', 'PercentFromShelfSold'];
        let fields = [];
        let columnNum = this.dataGrid.columnCount();
        console.log('columnCount', columnNum);
        for (let i = 0; i < columnNum; i++) {
            console.log('columnOption', i, this.dataGrid.columnOption(i, 'dataField'), this.dataGrid.columnOption(i, 'visible'));
            let fieldData = this.dataGrid.columnOption(i, 'dataField');
            let visible = this.dataGrid.columnOption(i, 'visible');
            if (fieldData === 'BarCode' && visible) fields.push('BarCode');
            if (fieldData === 'Name' && visible) fields.push('Name');
            if (fieldData === 'SupplierName' && visible) {
                fields.push('SupplierId');
                fields.push('SupplierName');
            }
            if (fieldData === 'ClassName' && visible) {
                fields.push('ClassId');
                fields.push('ClassName');
            }
            if (fieldData === 'GroupName' && visible) {
                fields.push('GroupId');
                fields.push('GroupName');
            }
            if (fieldData === 'SubGroupName' && visible) {
                fields.push('SubGroupId');
                fields.push('SubGroupName');
            }
            if (fieldData === 'ModelName' && visible) {
                fields.push('ModelId');
                fields.push('ModelName');
            }
            if (fieldData === 'SegmentName' && visible) {
                fields.push('SegmentId');
                fields.push('SegmentName');
            }
            if (fieldData === 'TotalFaces' && visible) fields.push('TotalFaces');
            if (fieldData === 'RecomendedFaces' && visible) fields.push('RecomendedFaces');
            if (fieldData === 'PercentFromNetwork' && visible) fields.push('PercentFromNetwork');
            if (fieldData === 'PercentFromBranch' && visible) fields.push('PercentFromBranch');
            if (fieldData === 'PercentFromShelf' && visible) fields.push('PercentFromShelf');
            if (fieldData === 'PercentFromShelfSold' && visible) fields.push('PercentFromShelfSold');
        }
        fields.push('Width', 'SupplierColor', 'ClassColor', 'GroupColor', 'SubGroupColor', 'SegmentColor', 'ModelColor');
        const withBOM = true;
        const excelStrings = true;
        const opts = { fields, excelStrings, withBOM };
        try {
            const fileData = parse(reportJSON, opts);
            const blob = new Blob([fileData], { type: "text/plain" });
            const url = URL.createObjectURL(blob);
            const link = document.createElement('a');
            // link.download = 'filename.json';
            let fileName = 'Summary_Color_Report_For_Aisle_' + currentAisle + '.csv';
            link.download = fileName;
            link.href = url;
            link.click();
        } catch (err) {
            console.error(err);
            uiNotify(err, 'error');
        }
    }

    /* Barak 2.8.20 - When first appearing we need to present the supplier information for the current Branch for the current Aisle suppliers*/
    onCellPrepared = (e: any) => {
        // console.log('onCellPrepared e', e.column);
        if (e.rowType == 'data' && typeof e.column.dataField != 'undefined' && e.column.dataField.indexOf('SupplierColor') >= 0) {
            e.cellElement.style.backgroundColor = e.displayValue; // display background color as the color discribed in the cell value
            e.cellElement.style.color = 'rgba(0, 0, 0, 0)'; // make text invisible
        }
        if (e.rowType == 'data' && typeof e.column.dataField != 'undefined' && e.column.dataField.indexOf('ClassColor') >= 0) {
            e.cellElement.style.backgroundColor = e.displayValue; // display background color as the color discribed in the cell value
            e.cellElement.style.color = 'rgba(0, 0, 0, 0)'; // make text invisible
        }
        if (e.rowType == 'data' && typeof e.column.dataField != 'undefined' && e.column.dataField.indexOf('GroupColor') >= 0) {
            e.cellElement.style.backgroundColor = e.displayValue; // display background color as the color discribed in the cell value
            e.cellElement.style.color = 'rgba(0, 0, 0, 0)'; // make text invisible
        }
        if (e.rowType == 'data' && typeof e.column.dataField != 'undefined' && e.column.dataField.indexOf('SubGroupColor') >= 0) {
            e.cellElement.style.backgroundColor = e.displayValue; // display background color as the color discribed in the cell value
            e.cellElement.style.color = 'rgba(0, 0, 0, 0)'; // make text invisible
        }
        if (e.rowType == 'data' && typeof e.column.dataField != 'undefined' && e.column.dataField.indexOf('SegmentColor') >= 0) {
            e.cellElement.style.backgroundColor = e.displayValue; // display background color as the color discribed in the cell value
            e.cellElement.style.color = 'rgba(0, 0, 0, 0)'; // make text invisible
        }
        if (e.rowType == 'data' && typeof e.column.dataField != 'undefined' && e.column.dataField.indexOf('ModelColor') >= 0) {
            e.cellElement.style.backgroundColor = e.displayValue; // display background color as the color discribed in the cell value
            e.cellElement.style.color = 'rgba(0, 0, 0, 0)'; // make text invisible
        }
        if (e.rowType == 'data' && typeof e.column.dataField != 'undefined' && e.column.dataField.indexOf('Color') >= 0) {
            // display background color as the color discribed in the cell value
            // console.log('onCellPrepared Color', e.displayValue);
            e.cellElement.style.backgroundColor = e.displayValue;
            e.cellElement.style.color = e.displayValue;
            // e.cellElement.style.color = 'rgba(0, 0, 0, 0)'; // make text invisible
        }
        if (e.rowType === "group" && e.summaryItems.length === 1 && typeof e.column.dataField != 'undefined' && e.column.dataField.indexOf('TotalFaces') >= 0) {
            // console.log('rowType==group',e.data.collapsedItems[0].Color);
            if (e.data.collapsedItems && e.data.collapsedItems[0]) {
                e.cellElement.style.backgroundColor = e.data.collapsedItems[0].Color;
                e.cellElement.style.color = e.data.collapsedItems[0].Color;
            }
        }
        //if (e.rowType == 'header' && typeof e.column.dataField != 'undefined' && e.column.dataField == 'PercentFromNetwork' ) {  
        //     // e.cellElement.on('click', function () {  
        //       e.component.option('sortByGroupSummaryInfo[0].sortOrder', e.component.option('sortByGroupSummaryInfo[0].sortOrder') == 'asc' ? 'desc' : 'asc');  
        //    // };  
        //}
    }

    componentWillUnmount() {
        localStorage.setItem('origUrl', '');
        localStorage.setItem('isReportStateLoaded', 'false');
    }
    /*********************************************************************************************************/

    /* Barak 7.9.20 */
    onOptionChanged = async (e: any) => {
        console.log('onOptionChanged e', e.fullName);
        await localStorage.setItem('isReportStateLoaded', 'true');

        if (e.fullName === 'columns[3].groupIndex') this.colorSupp = !this.colorSupp; // supplier
        if (e.fullName === 'columns[4].groupIndex') this.colorClass = !this.colorClass; // class
        if (e.fullName === 'columns[5].groupIndex') this.colorGroup = !this.colorGroup; // group
        if (e.fullName === 'columns[6].groupIndex') this.colorSubGroup = !this.colorSubGroup; // subgroup
        if (e.fullName === 'columns[7].groupIndex') this.colorModel = !this.colorModel; // model
        if (e.fullName === 'columns[8].groupIndex') this.colorSegment = !this.colorSegment; // segment
        this.checkVisibleColor();
    }
    /*********************************************************************************************************/

    checkVisibleColor = () => {
        // console.log('checkVisibleColor Supp',this.colorSupp,'Class',this.colorClass,'Group',this.colorGroup,'SubGroup',this.colorSubGroup,'Model',this.colorModel,'Segment',this.colorSegment);
        if (this.colorSegment) {
            this.setState({
                isSuppColorVisible: false,
                isClassColorVisible: false,
                isGroupColorVisible: false,
                isSubGroupColorVisible: false,
                isModelColorVisible: false,
                isSegmentColorVisible: true,
            });
            this.props.setColorBy('segment');
            let supplierObjects = JSON.parse(JSON.stringify(this.state.supplierObjects));
            for (let i = 0; i < supplierObjects.length; i++) {
                supplierObjects[i].Color = supplierObjects[i].SegmentColor;
                if (supplierObjects[i].SegmentColor === undefined) supplierObjects[i].Color = this.state.segmentColorMap["none"];
                let index = ColorMapRev.findIndex(line => line.Color === supplierObjects[i].Color);
                let colorName: string = '';
                if (index >= 0) colorName = ColorMapRev[index].Name;
                supplierObjects[i].ColorName = colorName;
            }
            this.setState({ supplierObjects });
        }
        else if (this.colorModel) {
            this.setState({
                isSuppColorVisible: false,
                isClassColorVisible: false,
                isGroupColorVisible: false,
                isSubGroupColorVisible: false,
                isModelColorVisible: true,
                isSegmentColorVisible: false,
            });
            this.props.setColorBy('model');
            let supplierObjects = JSON.parse(JSON.stringify(this.state.supplierObjects));
            for (let i = 0; i < supplierObjects.length; i++) {
                supplierObjects[i].Color = supplierObjects[i].ModelColor;
                if (supplierObjects[i].ModelColor === undefined) supplierObjects[i].Color = this.state.modelColorMap["none"];
                let index = ColorMapRev.findIndex(line => line.Color === supplierObjects[i].Color);
                let colorName: string = '';
                if (index >= 0) colorName = ColorMapRev[index].Name;
                supplierObjects[i].ColorName = colorName;
            }
            this.setState({ supplierObjects });
        }
        else if (this.colorSubGroup) {
            this.setState({
                isSuppColorVisible: false,
                isClassColorVisible: false,
                isGroupColorVisible: false,
                isSubGroupColorVisible: true,
                isModelColorVisible: false,
                isSegmentColorVisible: false,
            });
            this.props.setColorBy('subgroup');
            let supplierObjects = JSON.parse(JSON.stringify(this.state.supplierObjects));
            for (let i = 0; i < supplierObjects.length; i++) {
                supplierObjects[i].Color = supplierObjects[i].SubGroupColor;
                if (supplierObjects[i].SubGroupColor === undefined) supplierObjects[i].Color = this.state.subGroupColorMap["none"];
                let index = ColorMapRev.findIndex(line => line.Color === supplierObjects[i].Color);
                let colorName: string = '';
                if (index >= 0) colorName = ColorMapRev[index].Name;
                supplierObjects[i].ColorName = colorName;
            }
            this.setState({ supplierObjects });
        }
        else if (this.colorGroup) {
            this.setState({
                isSuppColorVisible: false,
                isClassColorVisible: false,
                isGroupColorVisible: true,
                isSubGroupColorVisible: false,
                isModelColorVisible: false,
                isSegmentColorVisible: false,
            });
            this.props.setColorBy('group');
            let supplierObjects = JSON.parse(JSON.stringify(this.state.supplierObjects));
            for (let i = 0; i < supplierObjects.length; i++) {
                supplierObjects[i].Color = supplierObjects[i].GroupColor;
                if (supplierObjects[i].GroupColor === undefined) supplierObjects[i].Color = this.state.groupColorMap["none"];
                let index = ColorMapRev.findIndex(line => line.Color === supplierObjects[i].Color);
                let colorName: string = '';
                if (index >= 0) colorName = ColorMapRev[index].Name;
                supplierObjects[i].ColorName = colorName;
            }
            this.setState({ supplierObjects });
        }
        else if (this.colorClass) {
            this.setState({
                isSuppColorVisible: false,
                isClassColorVisible: true,
                isGroupColorVisible: false,
                isSubGroupColorVisible: false,
                isModelColorVisible: false,
                isSegmentColorVisible: false,
            });
            this.props.setColorBy('class');
            let supplierObjects = JSON.parse(JSON.stringify(this.state.supplierObjects));
            for (let i = 0; i < supplierObjects.length; i++) {
                supplierObjects[i].Color = supplierObjects[i].ClassColor;
                if (supplierObjects[i].ClassColor === undefined) supplierObjects[i].Color = this.state.classColorMap["none"];
                let index = ColorMapRev.findIndex(line => line.Color === supplierObjects[i].Color);
                let colorName: string = '';
                if (index >= 0) colorName = ColorMapRev[index].Name;
                supplierObjects[i].ColorName = colorName;
            }
            this.setState({ supplierObjects });
        }
        else if (this.colorSupp) {
            this.setState({
                isSuppColorVisible: true,
                isClassColorVisible: false,
                isGroupColorVisible: false,
                isSubGroupColorVisible: false,
                isModelColorVisible: false,
                isSegmentColorVisible: false,
            });
            this.props.setColorBy('supplier');
            let supplierObjects = JSON.parse(JSON.stringify(this.state.supplierObjects));
            for (let i = 0; i < supplierObjects.length; i++) {
                supplierObjects[i].Color = supplierObjects[i].SupplierColor;
                if (supplierObjects[i].SupplierColor === undefined) supplierObjects[i].Color = this.state.suppColorMap["none"];
                let index = ColorMapRev.findIndex(line => line.Color === supplierObjects[i].Color);
                let colorName: string = '';
                if (index >= 0) colorName = ColorMapRev[index].Name;
                supplierObjects[i].ColorName = colorName;
            }
            this.setState({ supplierObjects });
        }
        else {
            this.setState({
                isSuppColorVisible: false,
                isClassColorVisible: false,
                isGroupColorVisible: false,
                isSubGroupColorVisible: false,
                isModelColorVisible: false,
                isSegmentColorVisible: false,
            });
            this.props.setColorBy('report');
            let supplierObjects = JSON.parse(JSON.stringify(this.state.supplierObjects));
            for (let i = 0; i < supplierObjects.length; i++) {
                supplierObjects[i].Color = '';
            }
            this.setState({ supplierObjects });
        }
        // console.log('see subGroup colors', this.state.isSubGroupColorVisible);
    }

    render() {
        if (!this.props.showColorMap) {
            /* Barak 2.8.20 */
            if (this.state.supplierObjects.length != 0) {
                this.setState({ supplierObjects: [], selectedGroup: [], selectedSubGroup: [], selectedDepartment: [], selectedSegment: [], firstRun: true });
                // /* Barak 29.12.20 */ this.props.setSummaryObjects([]);
            }
            /****************/
            return null;
        }
        const { selectedGroup, selectedSubGroup, selectedDepartment, /* supplierObjects, */ currentAisleOnly, selectedSegment, firstRun } = this.state;

        const { toggleColorMap, displayAisle } = this.props;
        let { classes, groups, subGroups, suppliers, showColorMap, segments, models, products } = this.props;

        const { height: floatHeight, width: floatWidth } = this.state;


        /* Barak 2.8.20 - When first appearing we need to present the supplier information for the current Branch for the current Aisle suppliers*/
        let { supplierObjects, loading } = this.state;
        let origUrl = localStorage.getItem('origUrl');
        let url;
        if (supplierObjects.length === 0)
            localStorage.setItem('origUrl', window.location.href);
        url = window.location.href;
        console.log('origUrl', origUrl, 'url', url);
        const urlElements = url.split('/');
        // const currentStore = parseInt(urlElements[5]);
        const currentBranch = parseInt(urlElements[5]);
        const currentStore = parseInt(urlElements[6]);
        const currentAisle = parseInt(urlElements[8]);

        // const currentBranch = await getBranchByStore(currentStore);

        // supplierObjects = this.initDataStore();
        if (url != origUrl || (supplierObjects.length === 0 && firstRun)) {
            localStorage.setItem('origUrl', window.location.href);
            // const url = window.location.href;
            const urlElements = url.split('/');
            const currentBranch = parseInt(urlElements[5]);
            // const currentBranch = await getBranchByStore(currentStore);
            // let branches = JSON.parse('[' + currentBranch + ']');
            // this.setState({ loading: true });
            // loading = true;
            fetchTrumaAisleSnif(currentBranch, currentAisle).then((trumasnifRecords) => {
                fetchTrumaByAisle(currentAisle).then(async (trumaRecords) => {
                    Const_Truma_Records = trumaRecords;
                    // this.setState({
                    //     supplierObjects: await this.calculateSupplierPercentInit(trumasnifRecords, true),
                    //     loading: false
                    // });
                    // supplierObjects = await this.calculateSupplierPercentInit(trumasnifRecords, true);
                    this.setState({ supplierObjects: await this.calculateSupplierPercentInit(trumasnifRecords, true), firstRun: false });
                    this.checkVisibleColor();
                    // loading = false;
                }).catch((err) => {
                    console.error(err);
                    uiNotify("Unable to load truma table", "error");
                    // this.setState({ loading: false });
                    // loading = false;
                })
            }).catch((err) => {
                console.error(err);
                uiNotify("Unable to load sales", "error");
                // this.setState({ loading: false });
                // loading = false;
            })
        }
        /*****************************************************************************************************************************************/

        const classSelectBoxData = new DataSource({
            store: classes,
            paginate: true,
            pageSize: 10
        });
        const groupSelectBoxData = new DataSource({
            store: groups,
            paginate: true,
            pageSize: 10
        });
        const subgroupSelectBoxData = new DataSource({
            store: subGroups,
            paginate: true,
            pageSize: 10
        });
        const segmentSelectBoxData = new DataSource({
            store: segments,
            paginate: true,
            pageSize: 10
        });
        const modelSelectBoxData = new DataSource({
            store: models,
            paginate: true,
            pageSize: 10
        });

        return (
            <Rnd
                dragHandleClassName="planogram-report-handle"
                cancel="planogram-report-container"
                default={{
                    x: floatWidth / 2,
                    y: floatHeight / 2,
                    width: floatWidth /* / 1.2*/ / 0.75,
                    height: floatHeight / 1.2,
                }}
                className="float-window planogram-report"
                resizeHandleClasses={{
                    bottomRight: "planogram-report-resize-handle bottom-right",
                    // bottomLeft: "planogram-report-resize-handle bottom-left",
                }}>
                <div className="float-window-handle planogram-report-handle">
                    {/* <div className="handle-content">{'Supplier Summary Report For Aisle ' + currentAisle}</div> */}
                    <div className="handle-content">{'Summary Color Report For Aisle ' + currentAisle}</div>
                    {/* <div className="float-window-close" onClick={e => { this.state.isPopupVisible ? this.hidePopup() : this.showPopup(); }}>
                        {this.state.isPopupVisible ? <FontAwesomeIcon icon={faEyeSlash} /> : <FontAwesomeIcon icon={faEye} />}
                    </div> */}
                    <div className="float-window-close" onClick={e => { this.props.displayColorPopUp ? this.hidePopup() : this.showPopup(); }}>
                        {this.props.displayColorPopUp ? <FontAwesomeIcon icon={faEyeSlash} /> : <FontAwesomeIcon icon={faEye} />}
                    </div>
                    <div className="float-window-close" onClick={async (e) => {
                        this.aboutToClose = true;
                        this.colorSupp = false;
                        this.colorClass = false;
                        this.colorGroup = false;
                        this.colorSubGroup = false;
                        this.colorSegment = false;
                        this.colorModel = false;
                        let supplierObjects = JSON.parse(JSON.stringify(this.state.supplierObjects));
                        for (let i = 0; i < supplierObjects.length; i++) {
                            supplierObjects[i].Color = '';
                        }
                        this.setState({ supplierObjects });
                        // /* Barak 29.12.20 */ this.props.setSummaryObjects([]);
                        localStorage.setItem('isReportStateLoaded', 'false');
                        toggleColorMap(showColorMap);
                        // this.hidePopup();
                    }}>
                        <FontAwesomeIcon icon={faWindowClose} />
                    </div>
                </div>
                <div className="float-window-container planogram-report-container">
                    {this.state.loading || (supplierObjects.length === 0 && firstRun) ?
                        <div className="loader"></div>
                        :
                        <React.Fragment>
                            <div className="report-toolbar">
                                <div className="report-toolbar-container">
                                    {/* Barak 6.9.20 - Adding segment */}
                                    <TagBox // SelectBox
                                        ref={this.selectSegmentRef}
                                        rtlEnabled
                                        showClearButton
                                        className="toolbar-item"
                                        placeholder="סגמנט"
                                        value={selectedSegment}
                                        showSelectionControls={true}
                                        applyValueMode="useButtons"
                                        searchEnabled={true}
                                        valueExpr="Id"
                                        displayExpr="Name"
                                        // items={segments}
                                        dataSource={new DataSource({
                                            store: segments,
                                            pageSize: 50
                                        })}
                                        onValueChanged={(e) => this.selectCategorySegment(e.value)}
                                    />
                                    {/*********************************/}
                                    <TagBox // SelectBox
                                        ref={this.selectSubGroupRef}
                                        rtlEnabled
                                        showClearButton
                                        className="toolbar-item"
                                        placeholder="תת קבוצה"
                                        value={selectedSubGroup}
                                        showSelectionControls={true}
                                        applyValueMode="useButtons"
                                        searchEnabled={true}
                                        valueExpr="Id"
                                        displayExpr="Name"
                                        // items={subGroups}
                                        dataSource={new DataSource({
                                            store: subGroups,
                                            pageSize: 50
                                        })}
                                        onValueChanged={(e) => this.selectCategorySubGroup(e.value)} />
                                    <TagBox // SelectBox
                                        ref={this.selectGroupRef}
                                        rtlEnabled
                                        showClearButton
                                        className="toolbar-item"
                                        placeholder="קבוצה"
                                        value={selectedGroup}
                                        showSelectionControls={true}
                                        applyValueMode="useButtons"
                                        searchEnabled={true}
                                        valueExpr="Id"
                                        displayExpr="Name"
                                        // items={groups}
                                        dataSource={new DataSource({
                                            store: groups,
                                            pageSize: 50
                                        })}
                                        onValueChanged={(e) => this.selectCategoryGroup(e.value)} />
                                    <TagBox // SelectBox
                                        ref={this.selectDepartmentRef}
                                        rtlEnabled
                                        showClearButton
                                        className="toolbar-item"
                                        placeholder="מחלקה"
                                        value={selectedDepartment}
                                        showSelectionControls={true}
                                        applyValueMode="useButtons"
                                        searchEnabled={true}
                                        valueExpr="Id"
                                        displayExpr="Name"
                                        // items={classes}
                                        dataSource={new DataSource({
                                            store: classes,
                                            pageSize: 50
                                        })}
                                        onValueChanged={(e) => this.selectCategoryDepartment(e.value)} />
                                </div>
                                <div className="report-toolbar-container">
                                    {/* <Button
                                            rtlEnabled
                                            className="toolbar-item"
                                            disabled={displayAisle == null}
                                            onClick={(e) => this.onSearch(true)}
                                            icon="search"
                                            text="בגונדולה" /> */}
                                    {/* Barak 29.9.20 - xlsOutput button */}
                                    <Button
                                        rtlEnabled
                                        onClick={(e) => {
                                            console.log('click', e);
                                            this.xlsOutput(e);
                                        }}
                                        className="toolbar-item-small"
                                        icon="exportxlsx" />
                                    {/***********************************/}
                                    <Button
                                        rtlEnabled
                                        // disabled={selectedBranches == null || selectedBranches.length === 0}
                                        onClick={(e) => this.onSearch(false)}
                                        className="toolbar-item"
                                        icon="search"
                                        text="הצג" />
                                </div>
                            </div>
                            <div className="report-content">
                                <DataGrid
                                    onInitialized={(ref) => {
                                        this.dataGrid = ref.component;
                                    }}
                                    rtlEnabled
                                    showRowLines
                                    showColumnHeaders
                                    allowColumnResizing
                                    // allowColumnReordering
                                    className="report-group-table"
                                    noDataText="אין נתונים..."
                                    dataSource={new DataSource({
                                        store: this.state.supplierObjects,
                                    })}

                                    onCellPrepared={(e) => this.onCellPrepared(e)}
                                    onOptionChanged={(e) => this.onOptionChanged(e)}


                                    // allow to save the state of the dataGrid including the columnChooser in localStorage / database table planogram_pattern
                                    // based on https://supportcenter.devexpress.com/ticket/details/t550068/datagrid-is-it-possible-to-store-the-selection-of-the-columnchooser

                                    stateStoring={{
                                        enabled: true,
                                        type: "custom",
                                        customLoad: async () => {
                                            // return JSON.parse(getCookie("key") || '{}');

                                            // let localVersion = localStorage.getItem('PlanogramReportThinColumnChooser');
                                            // if (localVersion) {
                                            //     let rec1 = JSON.parse(localVersion);
                                            //     if (this.props.user && rec1.user === this.props.user.id)
                                            //         return rec1.state;
                                            //     else return {};
                                            // }
                                            // else return {};

                                            // if(this.props.userReportTemplate && this.props.userReportTemplate[0]) return JSON.parse(this.props.userReportTemplate[0].state);
                                            // else return {};

                                            let isReportStateLoaded = await localStorage.getItem('isReportStateLoaded');
                                            console.log('check isReportStateLoaded', isReportStateLoaded);
                                            if (isReportStateLoaded === 'false' || !isReportStateLoaded || isReportStateLoaded === undefined) {
                                                // mark isReportStateLoaded as 'true'
                                                await localStorage.setItem('isReportStateLoaded', 'true');
                                                console.log('isReportStateLoaded true');
                                                // no state loaded - load state
                                                let state = await getUserReportState('PlanogramSupplierSummeryThin', this.props.user ? this.props.user.id : 0);
                                                if (state && state != '{}') {
                                                    let data = JSON.parse(state);
                                                    for (let i = 0; i < data.columns.length; i++) {
                                                        // remove any filterValue from the retrieved columns
                                                        delete data.columns[i].filterValue;

                                                        let fieldData = data.columns[i].dataField;
                                                        let groupIndex = data.columns[i].groupIndex;
                                                        if (fieldData === 'SupplierName' && groupIndex != undefined) this.colorSupp = true;
                                                        if (fieldData === 'ClassName' && groupIndex != undefined) this.colorClass = true;
                                                        if (fieldData === 'GroupName' && groupIndex != undefined) this.colorGroup = true;
                                                        if (fieldData === 'SubGroupName' && groupIndex != undefined) this.colorSubGroup = true;
                                                        if (fieldData === 'ModelName' && groupIndex != undefined) this.colorModel = true;
                                                        if (fieldData === 'SegmentName' && groupIndex != undefined) this.colorSegment = true;
                                                    }
                                                    this.checkVisibleColor();
                                                    // return JSON.parse(state);
                                                    console.log('getUserReportState PlanogramSupplierSummeryThin', data);
                                                    return data;
                                                }
                                                else return {};
                                            }
                                        },
                                        customSave: async (state) => {
                                            // setCookie("key", JSON.stringify(state), 1);

                                            // let rec1 = {
                                            //     state: state,
                                            //     user: this.props.user ? this.props.user.id : 0
                                            // }
                                            // localStorage.setItem('PlanogramReportThinColumnChooser', JSON.stringify(rec1));

                                            // if (this.aboutToClose) {
                                            //     this.aboutToClose = false;
                                            // only save state when closing the report (showColorMap becomes null)
                                            await setUserReportState('PlanogramSupplierSummeryThin', this.props.user ? this.props.user.id : 0, JSON.stringify(state));
                                            console.log('setUserReportState PlanogramSupplierSummeryThin', JSON.stringify(state));
                                            await localStorage.setItem('isReportStateLoaded', 'false');
                                            // }
                                        },
                                    }}
                                >
                                    <Texts
                                    />
                                    <FilterRow visible />
                                    <HeaderFilter visible />
                                    <Sorting mode="multiple" />
                                    {/* <Sorting /> */}
                                    <Scrolling mode={'virtual'} />
                                    {/* Barak 6.9.20 - add option to group by column */}
                                    {/* GroupPanel - https://js.devexpress.com/Demos/WidgetsGallery/Demo/DataGrid/RecordGrouping/React/Light/ */}
                                    <GroupPanel visible={true} emptyPanelText={'יש לגרור כותרת טור לכאן על מנת לקבץ לפי טור זה'} />
                                    <Grouping autoExpandAll={false} />
                                    {/************************************************/}
                                    {/* ColumnChooser - https://js.devexpress.com/Demos/WidgetsGallery/Demo/TreeList/ColumnChooser/React/Light/ */}
                                    <ColumnChooser enabled={true} allowSearch={true} mode={'select'} />
                                    {/* Barak 7.9.20 */}
                                    <Column
                                        dataField="Color"
                                        caption="צבע"
                                        width='50px'
                                    ></Column>
                                    <Column
                                        dataField="BarCode"
                                        caption="ברקוד"
                                    >
                                        <HeaderFilter
                                            allowSearch
                                            searchMode="Contains"
                                        />
                                    </Column>
                                    <Column
                                        dataField="Name"
                                        dataType="string"
                                        caption="פריט"
                                    >
                                        <HeaderFilter
                                            allowSearch
                                            searchMode="Contains"
                                        />
                                    </Column>
                                    {/****************/}
                                    <Column
                                        dataField="SupplierName"
                                        caption="ספק"
                                    >
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    <Column
                                        dataField="ClassName"
                                        caption="מחלקה"
                                    >
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    <Column
                                        dataField="GroupName"
                                        caption="קבוצה"
                                    >
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    {/* Barak 7.9.20 */}
                                    <Column
                                        dataField="SubGroupName"
                                        caption="תת-קבוצה"
                                    >
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    <Column
                                        dataField="ModelName"
                                        caption="מותג"
                                    >
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    <Column
                                        dataField="SegmentName"
                                        caption="סגמנט"
                                    >
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    <Column
                                        allowSearch
                                        dataField="TotalFaces"
                                        caption="פייסים"
                                        dataType="number"
                                        format={"##0"}
                                    >
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    <Column
                                        allowSearch
                                        dataField="RecomendedFaces"
                                        caption="פייסים מומלצים"
                                        dataType="number"
                                        format={"##0"}
                                    >
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    {/****************/}
                                    <Column
                                        allowSearch
                                        dataField="PercentFromNetwork"
                                        caption="% תרומה רשתי"
                                        dataType="number"
                                        format={"###,###,##0.##"}
                                    >
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    <Column
                                        allowSearch
                                        dataField="PercentFromBranch"
                                        caption="% תרומה סניפי"
                                        dataType="number"
                                        format={"###,###,##0.##"}
                                    >
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    <Column
                                        allowSearch
                                        dataField="PercentFromShelf"
                                        // caption="% שטח מדף"
                                        caption="% שטח מדף כולל"
                                        dataType="number"
                                        format={"###,###,##0.##"}
                                    >
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    <Column
                                        allowSearch
                                        dataField="PercentFromShelfSold"
                                        // caption="% תרומה מדף"
                                        caption="% שטח מדף תפוס"
                                        dataType="number"
                                        format={"###,###,##0.##"}
                                    >
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    <Summary>
                                        <TotalItem
                                            column={'PercentFromNetwork'}
                                            summaryType={'sum'}
                                            valueFormat={"###,###,##0.##"}
                                            displayFormat={'{0}'} />
                                        <TotalItem
                                            column={'PercentFromBranch'}
                                            summaryType={'sum'}
                                            valueFormat={"###,###,##0.##"}
                                            displayFormat={'{0}'} />
                                        <TotalItem
                                            column={'PercentFromShelf'}
                                            summaryType={'sum'}
                                            valueFormat={"###,###,##0.##"}
                                            displayFormat={'{0}'} />
                                        <TotalItem
                                            column={'PercentFromShelfSold'}
                                            summaryType={'sum'}
                                            valueFormat={"###,###,##0.##"}
                                            displayFormat={'{0}'} />


                                        <GroupItem
                                            // just for coloring this cell
                                            column={'TotalFaces'}
                                            name={'TotalFaces'}
                                            summaryType={'custom'}
                                            displayFormat={'{0}'}
                                            customizeText={''}
                                            alignByColumn={true} />
                                        <GroupItem
                                            column={'ColorName'}
                                            name={'ColorName'}
                                            summaryType="max"
                                            displayFormat={'Color: {0}'}
                                            alignByColumn={true} />
                                        <GroupItem
                                            column={'PercentFromNetwork'}
                                            name={'PercentFromNetwork'}
                                            summaryType={'sum'}
                                            valueFormat={"###,###,##0.##"}
                                            displayFormat={'{0}'}
                                            alignByColumn={true} />
                                        <GroupItem
                                            column={'PercentFromBranch'}
                                            name={'PercentFromBranch'}
                                            summaryType={'sum'}
                                            valueFormat={"###,###,##0.##"}
                                            displayFormat={'{0}'}
                                            alignByColumn={true} />
                                        <GroupItem
                                            column={'PercentFromShelf'}
                                            name={'PercentFromShelf'}
                                            summaryType={'sum'}
                                            valueFormat={"###,###,##0.##"}
                                            displayFormat={'{0}'}
                                            alignByColumn={true} />
                                        <GroupItem
                                            column={'PercentFromShelfSold'}
                                            name={'PercentFromShelfSold'}
                                            summaryType={'sum'}
                                            valueFormat={"###,###,##0.##"}
                                            displayFormat={'{0}'}
                                            alignByColumn={true} />
                                    </Summary>
                                    {/* <SortByGroupSummaryInfo
                                        summaryItem="PercentFromNetwork"
                                    /> */}
                                </DataGrid>
                                {/* <OutsidePopup title={"Upload from a file"} showTitle={true} width={350} height={250} visible={this.state.isPopupVisible} onHiding={() => this.hidePopup()}>
                                    <div className='grid-wrapper'>
                                        <p>
                                            <b>{'just a message'}</b>
                                        </p>
                                    </div>
                                </OutsidePopup> */}
                            </div>
                        </React.Fragment>
                    }
                </div>
            </Rnd>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PlanogramSupplierSummeryThin)