import * as React from "react"
import { connect } from 'react-redux'
import { AppState } from '@src/planogram/shared/store/app.reducer'
import { ThunkDispatch } from 'redux-thunk'
import { AnyAction } from 'redux'
import noImage from "assets/images/no-image.jpg"
import { barcodeImageSrc } from "../generic/BarcodeImage";
import { hideProductDetailer } from "@src/planogram/shared/store/planogram/planogram.actions";
import { saveItemMissing } from '@src/planogram/shared/api/catalog.provider'
import { uiNotify } from '@src/planogram/shared/components/Toast'
import { setStore } from '@src/planogram/shared/store/planogram/store/store.actions';

const mapStateToProps = (state: AppState, ownProps: any) => {
    const barcode = state.planogram.display.productDetailer ? state.planogram.display.productDetailer.barcode : null;
    const product = barcode != null && state.newPlano ? state.newPlano.productsMap[barcode] : null;
    const lowSales = state.planogram.display.productDetailer ? state.planogram.display.productDetailer.lowSales : null;
    const missing = state.planogram.display.productDetailer ? state.planogram.display.productDetailer.missing : 0;
    const netw = state.planogram.display.productDetailer ? state.planogram.display.productDetailer.netw : 'Admin';
    const userPhone = state.planogram.display.productDetailer ? state.planogram.display.productDetailer.userPhone : '';
    const shelfFaces = state.planogram.display.productDetailer ? state.planogram.display.productDetailer.shelfFaces : null;
    return {
        barcode,
        product,
        storePosition: barcode && state.planogram.productDetails[barcode] ? state.planogram.productDetails[barcode].position : null,
        maxAmountShelf: barcode && state.planogram.productDetails[barcode] ? state.planogram.productDetails[barcode].maxAmount : null,
        barcodeFacesCount: barcode && state.planogram.productDetails[barcode] ? state.planogram.productDetails[barcode].facesCount : null,
        weeklySales: barcode && state.catalog.productSales[barcode] ? state.catalog.productSales[barcode].weekly : undefined,
        catalogSupplier: product && product.SapakId ? state.system.data.suppliersMap[product.SapakId] : null,
        catalogClass: product && product.ClassesId ? state.system.data.classesMap[product.ClassesId] : null,
        catalogGroup: product && product.GroupId ? state.system.data.groupsMap[product.GroupId] : null,
        catalogSubGroup: product && product.SubGroupId ? state.system.data.subGroupsMap[product.SubGroupId] : null,
        catalogSerie: product && product.DegemId ? state.system.data.seriesMap[product.DegemId] : null,
        statusCalcInv: state.planogram.store ? state.planogram.store.statusCalcInv : 0,
        calcInvDays: state.planogram.store ? state.planogram.store.calcInvDays : 0,
        markBadProducts: state.displayOptions?.productWithoutSize,
        markArchiveProducts: state.planogram.display.markArchiveProducts,
        lowSales,
        missing,
        store: state.planogram.store ? state.planogram.store : null,
        netw,
        userPhone,
        hourlySales: barcode && state.catalog.productSales[barcode] ? state.catalog.productSales[barcode].hourly : undefined,
        shelfFaces,
    }
}

function mapDispatchToProps(dispatch: ThunkDispatch<AppState, {}, AnyAction>) {
    return {
        hideProductDisplayerBarcode: () => {
            dispatch(hideProductDetailer());
        },
    }
}

class PlanogramProductDetailSpecial extends React.Component<ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>> {
    render() {
        const {
            barcode,
            product,
            storePosition,
            maxAmountShelf,
            barcodeFacesCount,
            catalogSupplier,
            shelfFaces,
            netw,
            userPhone,
            hideProductDisplayerBarcode
        } = this.props;

        let {
            missing,
            store
        } = this.props;


        if (barcode == null || product == null)
            return null;
        return (
            <div className="planogram-product-detailer">
                <div className="detailer-section">
                    <div className="detailer-row">
                        <span style={{ fontWeight: 'normal' }}>שם פריט:&nbsp;</span>
                        <span>{product.Name}</span>
                    </div>
                    <div className="detailer-row">
                        <span style={{ fontWeight: 'normal' }}>ברקוד:</span>
                        <span>{product.BarCode}</span>
                    </div>
                    <div className="detailer-row">
                        <span style={{ fontWeight: 'normal' }}>ספק:</span>
                        <span>{(catalogSupplier ? catalogSupplier.Name : product.SapakId) || "---"}</span>
                    </div>
                    <div className="detailer-row">
                        <span style={{ fontWeight: 'normal' }}>כמות פייסים (גונדולה):</span>
                        <span>{barcodeFacesCount}</span>
                        <span style={{ fontWeight: 'normal' }}>כמות פייסים (מדף):</span><span>{shelfFaces}</span>
                        <span style={{ fontWeight: 'normal' }}>קיבולת מדף:</span>
                        <span>{maxAmountShelf}</span>
                    </div>
                    <div className="spec-item">
                        <span style={{ fontWeight: 'normal' }}>כמות חסרה:&nbsp;&nbsp;</span>
                        <input
                            // autoFocus
                            type="number"
                            name="missingField"
                            min={0}
                            max={maxAmountShelf ? maxAmountShelf : 10000}
                            style={{ width: "6.5em" }}
                            // value={dimension.height}
                            defaultValue={missing > 0 ? JSON.stringify(missing) : ''}
                            // onFocus={handleFocus}
                            onChange={(e) => {
                                if (maxAmountShelf && parseInt(e.target.value) > maxAmountShelf) {
                                    uiNotify("כמות חסרה לא יכולה להיות גדולה מקיבולת מדף    ", "error", 3000);
                                    e.target.value = JSON.stringify(maxAmountShelf);
                                }
                                if (parseInt(e.target.value) < 0) {
                                    uiNotify("כמות חסרה לא יכולה להיות קטנה מאפס    ", "error", 3000);
                                    e.target.value = '';
                                }
                                if (parseInt(e.target.value) > 0) {
                                    missing = parseInt(e.target.value);
                                    console.log('new missing', missing);
                                }
                            }}
                        // onBlur={e => {}} 
                        />
                    </div>
                    <div className="detailer-row">
                        <button style={{ backgroundColor: "white", color: "black", fontWeight: 'normal' }}
                            onClick={async (e) => {
                                // since this item could appear on several shelves we need to update the missing amount to all of them
                                let problem = false;
                                if (storePosition && storePosition.length > 0) {
                                    for (let i = 0; i < storePosition.length; i++) {
                                        // console.log('storePosition[', i, ']', storePosition[i].shelf);
                                        let res = await saveItemMissing(netw, userPhone, product.BarCode, storePosition[i].shelf, missing);
                                        if (res.affectedRows === 0 || res.changedRows === 0) problem = true;
                                        // if there was no problem we need to update the store with the new 'missing' value
                                        else {
                                            let fullshelf = storePosition[0].shelf;
                                            let aisle = fullshelf.substring(0, fullshelf.indexOf('SE'));
                                            let section = fullshelf.substring(0, fullshelf.indexOf('SH'));
                                            let length = 0;
                                            if (store) length = store.aisles.length;
                                            for (let a = 0; a < length; a++) {
                                                if (store && store.aisles[a].id === aisle) {
                                                    let lengthb = 0;
                                                    if (store && store.aisles[a].sections) lengthb = store.aisles[a].sections.length;
                                                    for (let b = 0; b < lengthb; b++) {
                                                        if (store && store.aisles[a].sections[b].id === section) {
                                                            let lengthc = 0;
                                                            if (store && store.aisles[a].sections[b].shelves) lengthc = store.aisles[a].sections[b].shelves.length;
                                                            for (let c = 0; c < lengthc; c++) {
                                                                if (store && store.aisles[a].sections[b].shelves[c].id === fullshelf) {
                                                                    let lengthd = 0;
                                                                    if (store && store.aisles[a].sections[b].shelves[c].items) lengthd = store.aisles[a].sections[b].shelves[c].items.length;
                                                                    for (let d = 0; d < lengthd; d++) {
                                                                        if (store && store.aisles[a].sections[b].shelves[c].items[d].product === parseInt(barcode)) {
                                                                            // place the new missing in the correct loaction for this item, and this shelf
                                                                            store.aisles[a].sections[b].shelves[c].items[d].missing = missing;
                                                                            // save the new location as part of the store
                                                                            setStore(store);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                if (problem) {
                                    uiNotify("היתה בעיה בשמירת הכמות החסרה   ", "error", 3000);
                                } else {
                                    uiNotify("הכמות החסרה נשמרה בהצלחה   ", "success", 3000);
                                }
                                console.log('save data');
                                hideProductDisplayerBarcode();
                            }}
                        >
                            <span>שמור</span>
                        </button>
                        <button style={{ backgroundColor: "white", color: "black", marginLeft: "1em", fontWeight: 'normal' }}
                            onClick={(e) => {
                                console.log('cancel');
                                hideProductDisplayerBarcode();
                                //toggleSettings()
                            }
                            }
                        >
                            <span>בטל</span>
                        </button>
                    </div>
                </div>
                {/* <div className="detailer-section">
                    <h3 className="detailer-section-title">גדלים</h3>
                    <div className="detailer-row">
                        <label>גובה:</label>
                        <span>{product.height != null ? product.height + "mm" : null}</span>
                    </div>
                    <div className="detailer-row">
                        <label>רוחב:</label>
                        <span>{product.width != null ? product.width + "mm" : null}</span>
                    </div>
                    <div className="detailer-row">
                        <label>עומק:</label>
                        <span>{product.length != null ? product.length + "mm" : null}</span>
                    </div>
                    <div className="detailer-row">
                        <label>מדף:</label>
                        <span style={{ fontSize: "0.75em", maxWidth: 150 }}>
                            {storePosition && storePosition.length > 0 ? storePosition.map(p => p.shelf).join(', ') : null}
                        </span>
                    </div>
                </div> */}
                <div className="detailer-section" style={{ minWidth: '0' }}>
                    <img
                        style={{
                            // zIndex: 300 - (__i),
                            // transform: "translate("+(__i * pixelSideStack) + "px,"+(-__i * pixelTopStack) + "px)"
                            // left: (__i * pixelSideStack) + "px",
                            // bottom: (__i * pixelTopStack) + "px",
                            height: '120px'
                        }}
                        alt=""
                        onError={e => {
                            e.currentTarget.src = noImage;
                        }}
                        src={barcodeImageSrc(product.client_image ? product.client_image : product.BarCode)}
                    />
                </div>
                {/* <div className="detailer-section">
                    <h3 className="detailer-section-title">סטטיסטיקה</h3>
                    <div className="detailer-row">
                        <label>כמות פייסים:</label>
                        <span>{barcodeFacesCount}</span>
                    </div>
                    <div className="detailer-row">
                        <label>קיבולת מדף:</label>
                        <span>{maxAmountShelf}</span>
                    </div>
                    <div className="detailer-row">
                        <label>ממוצע מכירות:</label>
                        <span>{weeklySales != null ? Math.round(weeklySales) : "~"}</span>
                    </div>
                    <div className="detailer-row">
                        <label>מכר אונליין:</label>
                        <span>{hourlySales != null ? Math.round(hourlySales) : "~"}</span>
                    </div>
                    {product.IsSubBar ? <div className="detailer-row"><label style={{ fontWeight: "bold" }}>הנתונים כוללים תחליפי</label></div> : null}
                    {markBadProducts && !product.width && !product.height && !product.length ? <div className="detailer-row"><label style={{ fontWeight: "bold" }}>הצבע מייצג פריט ללא מידות</label></div> : null}
                    {markArchiveProducts && product.Archives === 1 ? <div className="detailer-row"><label style={{ fontWeight: "bold" }}>הצבע מייצג פריט ארכיון</label></div> : null}
                    {markArchiveProducts && product.Archives === 0
                        && product.Bdate_sale && product.Bdate_sale != '' ? <div className="detailer-row"><label style={{ fontWeight: "bold" }}>הצבע מייצג פריט חסום מכירה</label></div> : null}
                    {markArchiveProducts && product.Archives === 0
                        && (!product.Bdate_sale || (product.Bdate_sale && product.Bdate_sale === ''))
                        && product.Bdate_buy && product.Bdate_buy != '' ? <div className="detailer-row"><label style={{ fontWeight: "bold" }}>הצבע מייצג פריט חסום רכש</label></div> : null}
                    {markArchiveProducts && product.Archives === 0
                        && (!product.Bdate_sale || (product.Bdate_sale && product.Bdate_sale === ''))
                        && (!product.Bdate_buy || (product.Bdate_buy && product.Bdate_buy === ''))
                        && lowSales ? <div className="detailer-row"><label style={{ fontWeight: "bold" }}>הצבע מייצג פריט דל מכר</label></div> : null}
                </div> */}
            </div>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PlanogramProductDetailSpecial)