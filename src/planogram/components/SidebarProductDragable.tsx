import * as React from 'react';
import { PlanogramDragDropTypes } from '../generic/DragAndDropType';
import { useDrag } from 'react-dnd';
import { useIntl } from 'react-intl';
import {Divider } from '@components';

import { config } from '@src/config';
import classnames from 'classnames';
import CustomCheckBox from '../generic/customCheckBox';

export const SidebarProductDragable: React.FC<{
	picture?: boolean;
	disableDrag?: boolean;
	isOnAisle?: any;
	product: any;
	index?: number;
	clicked?: boolean;
	className?: string;
	isSideBarExpaned: boolean;
	isSelected?: boolean;
	noCheckBox?: boolean;
	columns?: any;
	shouldDisplayImg?: boolean;
	onCheckBoxClicked?: (product: any) => void;
}> = ({
	picture,
	product,
	disableDrag,
	className,
	isSideBarExpaned,
	isSelected,
	clicked,
	isOnAisle,
	noCheckBox,
	columns,
	onCheckBoxClicked,
	shouldDisplayImg
}) => {
	const { formatMessage } = useIntl();
	let border: string = isSelected ? '1px 3.5px 1px 1px' : isOnAisle ? '0px 3.5px 0px 0' : '0';

	const [{ isDragging }, dragRef] = useDrag({
		item: {
			type: PlanogramDragDropTypes.PRODUCT_SIDEBAR,
			payload: product.BarCode,
			faces: product.faces ? product.faces : 0
		},
		// canDrag: monitor => product.dimensions && product.dimensions.height != null && product.dimensions.width != null && product.dimensions.depth != null,
		collect: monitor => ({
			isDragging: monitor.canDrag() && monitor.isDragging()
		})
	});

	const noImg = () => {
		let imgRef: any = document.getElementById(String(product.BarCode));
		if (imgRef) imgRef.src = `../../../${config.imagesPath}noImg/noimgside.jpg`;
	};

	let productBadgeColor: string = '';
	if (product.Status === formatMessage({ id: 'Blocked' })) productBadgeColor = '#31baab';
	else if (product.Status === formatMessage({ id: 'Regular' })) productBadgeColor = '';
	else if (product.Status === formatMessage({ id: 'LowSale' })) productBadgeColor = '#1a56b0';
	else if (product.Status === formatMessage({ id: 'NewBarCode' })) productBadgeColor = '#ff705d';

	function copyToClipboard(text: any) {
		var dummy = document.createElement('textarea');
		document.body.appendChild(dummy);
		dummy.value = text;
		dummy.select();
		document.execCommand('copy');
		document.body.removeChild(dummy);
	}

	if (picture) {
		return (
			<>
				{/* <Tooltip placement="top"  title={product.ProductName ?  product.ProductName : product.Name}> */}
				<div
					onDoubleClick={() => {
						copyToClipboard(product.BarCode);
					}}
					className={`bg-white p-1 cursor-pointer mt-05 d-flex align-items-center justify-content-center
            ${!product.isSelected ? 'opacity-03' : ''}
            ${className ? className : null} ${isDragging ? ' dragged' : ''}`}
					style={{ border: 'solid #31baab', borderWidth: border }}
				>
					<div className="d-flex align-items-center justify-content-center width-100-per">
						<div className="mr-05 min-height-1-2-rem width-1-5-rem">
							{!noCheckBox && (
								<CustomCheckBox
									classNames={'mr-3'}
									option={product}
									checked={product.isSelected}
									onClick={onCheckBoxClicked ? onCheckBoxClicked : () => {}}
								/>
							)}
						</div>

						<div className="d-flex align-items-center position-relative">
							<div
								className={classnames(
									'position-absolute cursor-pointer oval rounded zindex-10002  badge-up text-bold-600 text-white height-1-rem width-1-rem',
									{
										'border-1': productBadgeColor && productBadgeColor.length,
										'border-white': productBadgeColor && productBadgeColor.length,
										'no-border': !productBadgeColor
									}
								)}
								style={{
									backgroundColor: productBadgeColor,
									top: '-0.2rem',
									right: '-0.5rem',
									border: '0.9rem solid white'
								}}
							>
								&nbsp;
							</div>
							<div ref={product.isSelected && !disableDrag ? dragRef : null}>
                            {shouldDisplayImg || shouldDisplayImg == undefined ? (
									<img
										id={String(product.BarCode)}
										onError={noImg}
										style={{ width: '3rem', height: '4rem' }}
										src={config.productsImages + product.BarCode + '.jpg'}
										className={classnames('', { grayscale: !product.isSelected })}
									/>
								) : null}
							</div>
							<div
								className={classnames(
									'position-absolute ml-auto mr-1 text-white text-bold-700 d-flex align-items-center justify-content-center  rounded height-2-rem width-2-rem',
									{
										'bg-plano-dark': !isOnAisle,
										'bg-turquoise': isOnAisle
									}
								)}
								style={{ top: '-0.5rem', left: '-1.5rem' }}
							>
								{product.faces}
							</div>
						</div>
					</div>
				</div>
				{/* </Tooltip>    */}
			</>
		);
	} else {
		return (
			<div
				onDoubleClick={() => {
					copyToClipboard(product.BarCode);
				}}
				className={`cursor-pointer bg-white mt-05 d-flex align-items-center py-1  
      ${!product.isSelected ? 'opacity-03' : ''}
      ${className ? className : null} ${isDragging ? ' dragged' : ''}`}
				style={{ border: 'solid #31baab', borderWidth: border }}
			>
				<div className="d-flex align-items-center justify-content-center width-100-per">
					<CustomCheckBox
						option={product}
						checked={product.isSelected}
						onClick={
							onCheckBoxClicked
								? onCheckBoxClicked
								: () => {
										return;
								  }
						}
					/>
					<div className="position-relative pl-1">
						<div
							className={classnames(
								'position-absolute cursor-pointer oval rounded zindex-10002  badge-up text-bold-600 text-white height-1-rem width-1-rem',
								{
									'border-1': productBadgeColor && productBadgeColor.length,
									'border-white': productBadgeColor && productBadgeColor.length,
									'no-border': !productBadgeColor
								}
							)}
							style={{
								backgroundColor: productBadgeColor,
								top: '-0.2rem',
								right: '0.2rem',
								border: '0.9rem solid white'
							}}
						>
							&nbsp;
						</div>
						<div ref={product.isSelected ? dragRef : null}>
							{shouldDisplayImg || shouldDisplayImg == undefined ? (
								<img
									id={String(product.BarCode)}
									onError={noImg}
									style={{ width: '3rem', height: '4rem' }}
									src={config.productsImages + product.BarCode + '.jpg'}
									className={classnames('', { grayscale: !product.isSelected })}
								/>
							) : null}
						</div>
					</div>
					<div className="d-flex-column justify-content-between pl-2 max-width-9-rem min-width-9-rem">
						<div className="pb-05 font-small-3 text-bold-700">
							{product.ProductName ? product.ProductName : product.Name}
						</div>
						<div className="font-small-2">{product.BarCode}</div>
					</div>
					<div
						className={classnames(
							'ml-auto mr-1 text-white text-bold-700 d-flex align-items-center justify-content-center rounded height-2-rem width-2-rem',
							{
								'bg-plano-dark': !isOnAisle,
								'bg-turquoise': isOnAisle
							}
						)}
					>
						{product.faces}
					</div>
					{isSideBarExpaned ? <span className="divider-small-plano"></span> : null}
				</div>

				{isSideBarExpaned ? (
					<div className="width-100-per d-flex justify-content-around align-items-center text-bold-700">
						{columns.map((column: any) => {
							if (!column.selected) return null;
							else
								return (
									<div>
										{product[column.name]}
										{column?.prefix}
									</div>
								);
						})}
					</div>
				) : null}
			</div>
		);
	}
};
