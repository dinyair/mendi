import * as React from "react"
import { connect } from 'react-redux'
import { DateBox, Button, TagBox, DataGrid } from 'devextreme-react';
import SelectBox from 'devextreme-react/select-box';
import { Column, Texts, Sorting, Scrolling, HeaderFilter, FilterRow, Export, Summary, TotalItem, GroupPanel, GroupItem, Grouping } from 'devextreme-react/data-grid'
import DataSource from 'devextreme/data/data_source'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { AnyAction, bindActionCreators } from 'redux'
import { ThunkDispatch } from 'redux-thunk'
import { Rnd } from 'react-rnd';

import { AppState } from '@src/planogram/shared/store/app.reducer'
import {
    fetchCatalogSales, CatalogSaleRecord, fetchTrumaSnif, fetchTruma,
    fetchTrumaSnifByGroup, fetchTrumaSnifBySubGroup, fetchTrumaSnifByDepartment,
    fetchTrumaByGroup, fetchTrumaBySubGroup, fetchTrumaByDepartment, fetchTotalShelfWidthForStore,
    fetchTotalShelfPerSupplierPerStore,
    fetchTotalShelfPerSupplierPerAisle,
    fetchTotalShelfForAisle,
    fetchTrumaAisleSnif,
    fetchTrumaByAisle,
    fetchTrumaSnifBySelection,
    fetchTrumaBySelection,
    getBranchByStore,
    fetchTotalShelfPerItemPerAisle
} from '@src/planogram/shared/api/sales.provider';
// import { SidebarProductDragable } from './SidebarProductDragable';
import { faFilter, faWindowClose } from '@fortawesome/free-solid-svg-icons';
import { uiNotify } from '@src/planogram/shared/components/Toast';
// import EditableBarcodeStatus from './SaleReport/EditableBarcodeStatus'
import { fetchBarcodeStatuses } from '@src/planogram/shared/api/catalog.provider'
import { setBarcodeStatuses } from '@src/planogram/shared/store/catalog/catalog.action'
import { hideSalesReport } from '@src/planogram/shared/store/planogram/display/display.actions'
import { toggleSettings, toggleRowItems, setColorBy } from '@src/planogram/shared/store/planogram/planogram.actions'
import { CatalogBarcode } from '@src/planogram/shared/interfaces/models/CatalogProduct'
import { deepExtractKey } from '@src/planogram/shared/store/planogram/virtualize/deep-extract'
import { getIndexUniqueColor } from '@src/planogram/shared/store/planogram/virtualize/colors.provider'
import { ColorMap } from '@src/planogram/shared/store/planogram/virtualize/virtualize.reducer'

type ReportSaleRecord = CatalogSaleRecord & {
    InStore: boolean,
/*Barak 13.1.20*/ Name: string,
};

const mapStateToProps = (state: AppState, ownProps: any) => ({
    products: state.catalog.products,
    productsMap: state.newPlano ? state.newPlano.productsMap : null,
    displayAisle: state.planogram.display.aisleIndex != null
        && state.planogram.store
        && state.planogram.store.aisles[state.planogram.display.aisleIndex] ? state.planogram.store.aisles[state.planogram.display.aisleIndex].aisle_id : null,
    displaySalesReport: state.planogram.display.displaySalesReport,
    virtualProductDetails: state.planogram.productDetails,
    virtualStore: state.planogram.virtualStore,
    planogram: state.planogram,
    suppliers: state.system.data.suppliers,
    classes: state.system.data.classes,
    groups: state.system.data.groups,
    subGroups: state.system.data.subGroups,
    segments: state.system.data.segments,
    branches: state.system.data.branches,
    series: state.system.data.series,

    suppliersMap: state.system.data.suppliersMap,
    colorMap: state.planogram.virtualStore.colorMap,
    showColorMap: state.planogram.display.colorBy != null,
})

const mapDispatchToProps = (dispatch: ThunkDispatch<AppState, {}, AnyAction>) => ({
    fetchBarcodeStatuses: () => fetchBarcodeStatuses()
        .then(statuses => dispatch(setBarcodeStatuses(statuses)))
        .catch((err) => {
            uiNotify("Unable to load network barcode statuses")
        }),
    hideSalesReport: () => dispatch(hideSalesReport()),
    /* Barak 7.9.20 *  toggleColorMap: (showColorMap: boolean) => dispatch(showColorMap ? setColorBy(null) : setColorBy("supplier")), */
    /* Barak 7.9.20 */
    toggleColorMap: (showColorMap: boolean) => dispatch(showColorMap ? setColorBy(null) : setColorBy("report")),
    setColorBy: (setBy: string) => dispatch(setColorBy(setBy))
    /****************/
})

/* Barak 23.1.20 */ let Const_Truma_Records: any[] = [];

/* Barak 2.8.20 - https://medium.com/swlh/how-to-round-to-a-certain-number-of-decimal-places-in-javascript-ed74c471c1b8*/
// const roundAccurately = (number:number, decimalPlaces:number) =>
//     Number(Math.round(Number(number + "e" + decimalPlaces)) + "e-" + decimalPlaces);
const roundAccurately = (number: number, decimalPlaces: number) => {
    const factorOfTen = Math.pow(10, decimalPlaces);
    return Math.round(number * factorOfTen) / factorOfTen;
}
/****************/

type PlanogramSupplierSummeryProps = ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>
type PlanogramSupplierSummeryState = {
    loading: boolean,
    pageIndex: number,
    pageSize: number,
    salesRecords: ReportSaleRecord[],
    searchWord: string,
    height: number,
    width: number,
    trumaSnifRecords: any[],
    selectedGroup: number,
    selectedSubGroup: number,
    selectedDepartment: number,
    /* Barak 9.6.20 */ selectedSegment: number,
    supplierObjects: any[],
    currentAisleOnly: boolean,
    /* Barak 2.8.20 */ colorMap: ColorMap,
    /* Barak 2.8.20 */ firstRun: boolean,
    /* Barak 7.9.20 */
    suppColorMap: ColorMap,
    classColorMap: ColorMap,
    groupColorMap: ColorMap,
    subGroupColorMap: ColorMap,
    segmentColorMap: ColorMap,
    isSuppColorVisible: boolean,
    isClassColorVisible: boolean,
    isGroupColorVisible: boolean,
    isSubGroupColorVisible: boolean,
    isSegmentColorVisible: boolean,
    /****************/
};


class TrumaCollection extends Array {
    sum(key: string) {
        return this.reduce((a, b) => a + (b[key] || 0), 0);
    }
}

class PlanogramSupplierSummery extends React.Component<PlanogramSupplierSummeryProps, PlanogramSupplierSummeryState> {
    dataGrid: any | null = null;
    /* Barak 6.9.20 */ selectSegmentRef: any = React.createRef();
    selectSubGroupRef: any = React.createRef();
    selectGroupRef: any = React.createRef();
    selectDepartmentRef: any = React.createRef();
    /* Barak 7.9.20 */
    colorSupp: boolean = false;
    colorClass: boolean = false;
    colorGroup: boolean = false;
    colorSubGroup: boolean = false;
    colorSegment: boolean = false;
    /****************/
    state: PlanogramSupplierSummeryState = {
        loading: false,
        pageIndex: 0,
        pageSize: 50,
        searchWord: "",
        // width: window.innerWidth * 0.5656,
        width: window.innerWidth * 0.6,
        height: window.innerHeight * 0.5656,
        salesRecords: [],
        /*Barak 23.1.20 trumaSnifRecords*/
        trumaSnifRecords: [],
        selectedGroup: 0,
        selectedSubGroup: 0,
        selectedDepartment: 0,
        /* Barak 9.6.20 */ selectedSegment: 0,
        supplierObjects: [],
        currentAisleOnly: false,
        /* Barak 2.8.20 */ colorMap: {},
        /* Barak 2.8.20 */ firstRun: true,
        /* Barak 7.9.20 */
        suppColorMap: {},
        classColorMap: {},
        groupColorMap: {},
        subGroupColorMap: {},
        segmentColorMap: {},
        isSuppColorVisible: false,
        isClassColorVisible: false,
        isGroupColorVisible: false,
        isSubGroupColorVisible: false,
        isSegmentColorVisible: false,
        /****************/
    }
    get selectSubGroup() {
        return this.selectSubGroupRef.current.instance;
    }
    get selectGroup() {
        return this.selectGroupRef.current.instance;
    }
    get selectDepartment() {
        return this.selectDepartmentRef.current.instance;
    }

    componentDidMount() {
        this.props.fetchBarcodeStatuses();
    }
    getRandomColor() {
        let letters = '0123456789ABCDEF';
        let color = '#';
        for (let i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }
    /* Barak 2.8.20 */
    calculateSupplierPercentInit = async (trumaSnifRecords: any[], currentAisleOnly: boolean): Promise<any[]> => {
        console.log('calculateSupplierPercentInit', trumaSnifRecords);
        const { virtualProductDetails, virtualStore, productsMap } = this.props;
        const url = window.location.href;
        const urlElements = url.split('/');
        const currentBranch = parseInt(urlElements[5]);
        const currentStore = parseInt(urlElements[6]);
        const currentAisle = parseInt(urlElements[8]);

        let filteredTrumaSnifRecords: any[];

        /* Barak 2.8.20 */ this.setState({
            colorMap: virtualStore.aisleMap[currentAisle].colorMap,
            /* Barak 7.9.20 */
            suppColorMap: virtualStore.aisleMap[currentAisle].suppColorMap,
            classColorMap: virtualStore.aisleMap[currentAisle].classColorMap,
            groupColorMap: virtualStore.aisleMap[currentAisle].groupColorMap,
            subGroupColorMap: virtualStore.aisleMap[currentAisle].subGroupColorMap,
            segmentColorMap: virtualStore.aisleMap[currentAisle].segmentColorMap,
            /****************/
        });

        // let supplierIds = trumaSnifRecords.map(obj => { return obj.SapakId; });
        // supplierIds = supplierIds.filter((v, i) => { return supplierIds.indexOf(v) == i; });

        /********************************** Barak 7.9.20 ********************************************** 
        // to get the supplierIds I'll look at the aisle colorMap
        let suppliers = Object.keys(virtualStore.aisleMap[currentAisle].colorMap);
        let supplierIds: number[] = [];
        for (let i = 0; i < suppliers.length; i++) {
            supplierIds.push(parseInt(suppliers[i]));
        }
        console.log('calculateSupplierPercentInit currentAisle', currentAisle, 'supplierIds', supplierIds);
    
        let ColorMap: any[] = [];
        let supplierObjects: any[] = [];
    
        // let totalShelf = await fetchTotalShelfWidthForStore(currentStore);
        // let total_shelf = totalShelf[0]["SUM(width)"];
        let totalShelfForCategory = 0;
        // let totalShelfSupplier = await fetchTotalShelfPerSupplierPerStore(currentStore);
        let totalShelfSupplier = await fetchTotalShelfPerSupplierPerAisle(currentAisle);
        let totalShelfForAisle = await fetchTotalShelfForAisle(currentAisle);
    
        let totalSalesInNetwork = new TrumaCollection(...Const_Truma_Records).sum('Amount');
        let totalSalesInBranch = new TrumaCollection(...trumaSnifRecords).sum('Amount');
        console.log('totalSalesInNetwork', totalSalesInNetwork, 'totalSalesInBranch', totalSalesInBranch);
        for (let i = 0; i < supplierIds.length; i++) {
            let supplierRecord = totalShelfSupplier.filter(obj => obj.SapakId === supplierIds[i])[0];
            if (supplierRecord) totalShelfForCategory += supplierRecord.Total;
            let totalSupplierSalesInNetwork = new TrumaCollection(...Const_Truma_Records.filter(obj => obj.SapakId === supplierIds[i])).sum('Amount');
            let totalSupplierSalesInBranch = new TrumaCollection(...trumaSnifRecords.filter(obj => obj.SapakId === supplierIds[i])).sum('Amount');
            let supplierObject = {
                SupplierId: supplierIds[i],
                // SupplierColor: this.getRandomColor(),
                SupplierColor: '',
                PercentFromNetwork: (totalSupplierSalesInNetwork / totalSalesInNetwork) * 100,
                PercentFromBranch: (totalSupplierSalesInBranch / totalSalesInBranch) * 100,
                PercentFromShelf: 0,
                PercentFromShelfSold: 0
            }
            supplierObjects.push(supplierObject);
            let colorObj = {
                SupplierId: supplierIds[i],
                color: supplierObject.SupplierColor
            }
            ColorMap.push(colorObj);
        }
    
        console.log('supplierObjects 0', supplierObjects);
    
        for (let i = 0; i < supplierObjects.length; i++) {
            let supplierRecord = totalShelfSupplier.filter(obj => obj.SapakId === supplierObjects[i].SupplierId)[0];
            let supplierIndex = supplierObjects.findIndex(obj => obj.SupplierId === supplierObjects[i].SupplierId);
            // supplierObjects[supplierIndex].PercentFromShelf = supplierRecord ? (supplierRecord.Total / totalShelfForCategory) * 100 : 0;
            supplierObjects[supplierIndex].PercentFromShelf = supplierRecord ? (supplierRecord.Total / totalShelfForAisle) * 100 : 0;
            supplierObjects[supplierIndex].PercentFromShelfSold = supplierRecord ? (supplierRecord.Total / totalShelfForCategory) * 100 : 0;
        }
    
        console.log('supplierObjects 1', supplierObjects);
    
        // if (currentAisleOnly && currentAisle != null) {
        //     filteredTrumaSnifRecords = trumaSnifRecords.filter(v =>
        //         virtualProductDetails[v.BarCode]
        //         && virtualProductDetails[v.BarCode].position
        //         && virtualProductDetails[v.BarCode].position.findIndex(p => p.aisle_id === currentAisle) !== -1);
    
        //     supplierIds = filteredTrumaSnifRecords.map(obj => { return obj.SapakId; });
        //     supplierIds = supplierIds.filter((v, i) => { return supplierIds.indexOf(v) == i; });
        //     supplierObjects = supplierObjects.filter(function (item) {
        //         return supplierIds.indexOf(item.SupplierId) !== -1;
        //     });
        // }
    
        console.log('supplierObjects 2', supplierObjects);
    
        // sort supplierObjects by a combination of PercentFromBranch, PercentFromNetwork and PercentFromShelf
        supplierObjects.sort(function (a: any, b: any) {
            let aNumber = parseFloat(a.PercentFromBranch) + parseFloat(a.PercentFromNetwork);
            let bNumber = parseFloat(b.PercentFromBranch) + parseFloat(b.PercentFromNetwork);
            return (aNumber < bNumber) ? 1 : (aNumber > bNumber) ? -1 : 0;
        });
        // determine colors after the sort - the top always gets color 0, after color 1 etcw'
        for (let i = 0; i < supplierObjects.length; i++) {
            // supplierObjects[i].SupplierColor = getIndexUniqueColor(i);
            supplierObjects[i].SupplierColor = this.state.colorMap[supplierObjects[i].SupplierId];
        }
        console.log('supplierObjects 3', supplierObjects);
    
        return supplierObjects;
        **********************************************************************************************/
        /************************************* Barak 7.9.20 ******************************************/
        console.log('calculateItemPercentInit currentAisle', currentAisle);
        const { selectedGroup, selectedSubGroup, selectedDepartment, selectedSegment } = this.state;

        let ColorMap: any[] = [];
        let ColorMapSupp: any[] = [];
        let ColorMapClass: any[] = [];
        let ColorMapGroup: any[] = [];
        let ColorMapSubGroup: any[] = [];
        let ColorMapSegment: any[] = [];
        let itemObjects: any[] = [];

        let totalShelfForCategory = 0;
        let totalShelfItem = await fetchTotalShelfPerItemPerAisle(currentAisle, selectedDepartment, selectedGroup, selectedSubGroup, selectedSegment);
        let totalShelfForAisle = await fetchTotalShelfForAisle(currentAisle);

        let totalSalesInNetwork = new TrumaCollection(...Const_Truma_Records).sum('Amount');
        let totalSalesInBranch = new TrumaCollection(...trumaSnifRecords).sum('Amount');
        console.log('totalSalesInNetwork', totalSalesInNetwork, 'totalSalesInBranch', totalSalesInBranch);

        for (let i = 0; i < totalShelfItem.length; i++) {
            let itemRecord = totalShelfItem[i];
            if (itemRecord) totalShelfForCategory += itemRecord.Total;
            let totalItemSalesInNetwork = new TrumaCollection(...Const_Truma_Records.filter(obj => obj.BarCode === totalShelfItem[i].BarCode)).sum('Amount');
            let totalItemSalesInBranch = new TrumaCollection(...trumaSnifRecords.filter(obj => obj.BarCode === totalShelfItem[i].BarCode)).sum('Amount');
            let itemObject = {
                BarCode: itemRecord.BarCode,
                SupplierId: itemRecord.SapakId,
                ClassId: itemRecord.ClassesId,
                GroupId: itemRecord.GroupId,
                SubGroupId: itemRecord.SubGroupId,
                SegmentId: itemRecord.SegmentId,
                // SupplierColor: this.getRandomColor(),
                SupplierColor: '',
                ClassColor: '',
                GroupColor: '',
                SubGroupColor: '',
                SegmentColor: '',
                Color: '',
                TotalFaces: itemRecord.TotalFaces,
                Width: itemRecord.width,
                RecomendedFaces: 0,
                PercentFromNetwork: (totalItemSalesInNetwork / totalSalesInNetwork) * 100,
                PercentFromBranch: (totalItemSalesInBranch / totalSalesInBranch) * 100,
                PercentFromShelf: 0,
                PercentFromShelfSold: 0
            }
            itemObjects.push(itemObject);
            let colorObjSupp = {
                SupplierId: itemRecord.SapakId,
                color: itemObject.SupplierColor
            }
            ColorMapSupp.push(colorObjSupp);
            let colorObjClass = {
                ClassId: itemRecord.ClassesId,
                color: itemObject.ClassColor
            }
            ColorMapClass.push(colorObjClass);
            let colorObjGroup = {
                GroupId: itemRecord.GroupId,
                color: itemObject.GroupColor
            }
            ColorMapGroup.push(colorObjGroup);
            let colorObjSubGroup = {
                SubGroupId: itemRecord.SubGroupId,
                color: itemObject.SubGroupColor
            }
            ColorMapSubGroup.push(colorObjSubGroup);
            let colorObjSegment = {
                SegmentId: itemRecord.SegmentId,
                color: itemObject.SegmentColor
            }
            ColorMapSegment.push(colorObjSegment);
        }

        console.log('itemObjects 0', itemObjects);

        for (let i = 0; i < itemObjects.length; i++) {
            let itemRecord = totalShelfItem.filter(obj => obj.BarCode === itemObjects[i].BarCode)[0];
            let itemIndex = itemObjects.findIndex(obj => obj.BarCode === itemObjects[i].BarCode);
            itemObjects[itemIndex].PercentFromShelf = itemRecord ? (itemRecord.Total / totalShelfForAisle) * 100 : 0;
            itemObjects[itemIndex].PercentFromShelfSold = itemRecord ? (itemRecord.Total / totalShelfForCategory) * 100 : 0;
            itemObjects[itemIndex].RecomendedFaces = Math.floor((totalShelfForAisle * 0.8 * (itemObjects[itemIndex].PercentFromBranch / 100)) / itemObjects[itemIndex].Width);
        }

        console.log('itemObjects 1', itemObjects);

        // sort itemObjects by a combination of PercentFromBranch, PercentFromNetwork and PercentFromShelf
        itemObjects.sort(function (a: any, b: any) {
            let aNumber = parseFloat(a.PercentFromBranch) + parseFloat(a.PercentFromNetwork);
            let bNumber = parseFloat(b.PercentFromBranch) + parseFloat(b.PercentFromNetwork);
            return (aNumber < bNumber) ? 1 : (aNumber > bNumber) ? -1 : 0;
        });

        for (let i = 0; i < itemObjects.length; i++) {
            itemObjects[i].SupplierColor = this.state.suppColorMap[itemObjects[i].SupplierId];
            itemObjects[i].ClassColor = this.state.classColorMap[itemObjects[i].ClassId];
            itemObjects[i].GroupColor = this.state.groupColorMap[itemObjects[i].GroupId];
            itemObjects[i].SubGroupColor = this.state.subGroupColorMap[itemObjects[i].SubGroupId];
            itemObjects[i].SegmentColor = this.state.segmentColorMap[itemObjects[i].SegmentId];
        }

        return itemObjects;
    }
    /*********************************************************************************************************/
    calculateSupplierPercent = async (trumaSnifRecords: any[], currentAisleOnly: boolean): Promise<any[]> => {
        const { virtualProductDetails, virtualStore } = this.props;
        const url = window.location.href;
        const urlElements = url.split('/');
        const currentBranch = parseInt(urlElements[5]);
        const currentStore = parseInt(urlElements[6]);
        const currentAisle = parseInt(urlElements[8]);

        /* Barak 2.8.20 */ this.setState({
            colorMap: virtualStore.aisleMap[currentAisle].colorMap,
            /* Barak 7.9.20 */
            suppColorMap: virtualStore.aisleMap[currentAisle].suppColorMap,
            classColorMap: virtualStore.aisleMap[currentAisle].classColorMap,
            groupColorMap: virtualStore.aisleMap[currentAisle].groupColorMap,
            subGroupColorMap: virtualStore.aisleMap[currentAisle].subGroupColorMap,
            segmentColorMap: virtualStore.aisleMap[currentAisle].segmentColorMap,
            /****************/
        });

        let filteredTrumaSnifRecords: any[];

        /********************************** Barak 7.9.20 **********************************************/
        // /* Barak 2.8.20 *
        // let supplierIds = trumaSnifRecords.map(obj => { return obj.SapakId; });
        // supplierIds = supplierIds.filter((v, i) => { return supplierIds.indexOf(v) == i; });
        // *****************/
        // /* Barak 2.8.20 */
        // let supplierIds = Const_Truma_Records.map(obj => { return obj.SapakId; });
        // supplierIds = supplierIds.filter((v, i) => { return supplierIds.indexOf(v) == i; });
        // /****************/

        // let ColorMap: any[] = [];
        // let supplierObjects: any[] = [];

        // // let totalShelf = await fetchTotalShelfWidthForStore(currentStore);
        // // let total_shelf = totalShelf[0]["SUM(width)"];
        // let totalShelfForCategory = 0;
        // /* Barak 2.8.20 *  let totalShelfSupplier = await fetchTotalShelfPerSupplierPerStore(currentStore); */
        // /* Barak 2.8.20 */ let totalShelfSupplier = await fetchTotalShelfPerSupplierPerAisle(currentAisle);
        // /* Barak 2.8.20 */ let totalShelfForAisle = await fetchTotalShelfForAisle(currentAisle);


        // let totalSalesInNetwork = new TrumaCollection(...Const_Truma_Records).sum('Amount');
        // console.log('totalSalesInNetwork', totalSalesInNetwork);
        // let totalSalesInBranch = new TrumaCollection(...trumaSnifRecords).sum('Amount');
        // for (let i = 0; i < supplierIds.length; i++) {
        //     let supplierRecord = totalShelfSupplier.filter(obj => obj.SapakId === supplierIds[i])[0];
        //     if (supplierRecord) totalShelfForCategory += supplierRecord.Total;
        //     let totalSupplierSalesInNetwork = new TrumaCollection(...Const_Truma_Records.filter(obj => obj.SapakId === supplierIds[i])).sum('Amount');
        //     console.log('totalSupplierSalesInNetwork', totalSupplierSalesInNetwork);
        //     let totalSupplierSalesInBranch = new TrumaCollection(...trumaSnifRecords.filter(obj => obj.SapakId === supplierIds[i])).sum('Amount');
        //     let supplierObject = {
        //         SupplierId: supplierIds[i],
        //         /* Barak 2.8.20 * SupplierColor: this.getRandomColor(), */
        //         /* Barak 2.8.20 */ SupplierColor: this.state.colorMap[supplierIds[i]],
        //         PercentFromNetwork: (totalSupplierSalesInNetwork / totalSalesInNetwork) * 100,
        //         PercentFromBranch: (totalSupplierSalesInBranch / totalSalesInBranch) * 100,
        //         /* Barak 2.8.20 */ PercentFromShelf: 0,
        //         PercentFromShelfSold: 0
        //     }
        //     supplierObjects.push(supplierObject);
        //     let colorObj = {
        //         SupplierId: supplierIds[i],
        //         color: supplierObject.SupplierColor
        //     }
        //     ColorMap.push(colorObj);
        // }

        // for (let i = 0; i < supplierObjects.length; i++) {
        //     let supplierRecord = totalShelfSupplier.filter(obj => obj.SapakId === supplierObjects[i].SupplierId)[0];
        //     let supplierIndex = supplierObjects.findIndex(obj => obj.SupplierId === supplierObjects[i].SupplierId);
        //     supplierObjects[supplierIndex].PercentFromShelfSold = supplierRecord ? (supplierRecord.Total / totalShelfForCategory) * 100 : 0;
        //     /* Barak 2.8.20 */ supplierObjects[supplierIndex].PercentFromShelf = supplierRecord ? (supplierRecord.Total / totalShelfForAisle) * 100 : 0;
        // }

        // if (currentAisleOnly && currentAisle != null) {
        //     filteredTrumaSnifRecords = trumaSnifRecords.filter(v =>
        //         virtualProductDetails[v.BarCode]
        //         && virtualProductDetails[v.BarCode].position
        //         && virtualProductDetails[v.BarCode].position.findIndex(p => p.aisle_id === currentAisle) !== -1);

        //     supplierIds = filteredTrumaSnifRecords.map(obj => { return obj.SapakId; });
        //     supplierIds = supplierIds.filter((v, i) => { return supplierIds.indexOf(v) == i; });
        //     supplierObjects = supplierObjects.filter(function (item) {
        //         return supplierIds.indexOf(item.SupplierId) !== -1;
        //     });
        // }

        // // sort supplierObjects by a combination of PercentFromBranch, PercentFromNetwork and PercentFromShelf
        // supplierObjects.sort(function (a: any, b: any) {
        //     let aNumber = parseFloat(a.PercentFromBranch) + parseFloat(a.PercentFromNetwork);
        //     let bNumber = parseFloat(b.PercentFromBranch) + parseFloat(b.PercentFromNetwork);
        //     return (aNumber < bNumber) ? 1 : (aNumber > bNumber) ? -1 : 0;
        // });

        // return supplierObjects;
        /*********************************************************************************************/
        /************************************* Barak 7.9.20 ******************************************/
        console.log('calculateItemPercentInit currentAisle', currentAisle);
        const { selectedGroup, selectedSubGroup, selectedDepartment, selectedSegment } = this.state;

        let itemObjects: any[] = [];

        let totalShelfForCategory = 0;
        let totalShelfItem = await fetchTotalShelfPerItemPerAisle(currentAisle, selectedDepartment, selectedGroup, selectedSubGroup, selectedSegment);
        let totalShelfForAisle = await fetchTotalShelfForAisle(currentAisle);

        let totalSalesInNetwork = new TrumaCollection(...Const_Truma_Records).sum('Amount');
        let totalSalesInBranch = new TrumaCollection(...trumaSnifRecords).sum('Amount');
        console.log('totalSalesInNetwork', totalSalesInNetwork, 'totalSalesInBranch', totalSalesInBranch);

        for (let i = 0; i < totalShelfItem.length; i++) {
            let itemRecord = totalShelfItem[i];
            if (itemRecord) totalShelfForCategory += itemRecord.Total;
            let totalItemSalesInNetwork = new TrumaCollection(...Const_Truma_Records.filter(obj => obj.BarCode === totalShelfItem[i].BarCode)).sum('Amount');
            let totalItemSalesInBranch = new TrumaCollection(...trumaSnifRecords.filter(obj => obj.BarCode === totalShelfItem[i].BarCode)).sum('Amount');
            let itemObject = {
                BarCode: itemRecord.BarCode,
                SupplierId: itemRecord.SapakId,
                ClassId: itemRecord.ClassesId,
                GroupId: itemRecord.GroupId,
                SubGroupId: itemRecord.SubGroupId,
                SegmentId: itemRecord.SegmentId,
                TotalFaces: itemRecord.TotalFaces,
                RecomendedFaces: 0,
                Width: itemRecord.width,
                SupplierColor: this.state.suppColorMap[itemRecord.SapakId],
                ClassColor: this.state.classColorMap[itemRecord.ClassId],
                GroupColor: this.state.groupColorMap[itemRecord.GroupId],
                SubGroupColor: this.state.subGroupColorMap[itemRecord.SubGroupId],
                SegmentColor: this.state.segmentColorMap[itemRecord.SegmentId],
                Color: '',
                PercentFromNetwork: (totalItemSalesInNetwork / totalSalesInNetwork) * 100,
                PercentFromBranch: (totalItemSalesInBranch / totalSalesInBranch) * 100,
                PercentFromShelf: 0,
                PercentFromShelfSold: 0
            }
            itemObjects.push(itemObject);
        }

        console.log('itemObjects 0', itemObjects);

        for (let i = 0; i < itemObjects.length; i++) {
            let itemRecord = totalShelfItem.filter(obj => obj.BarCode === itemObjects[i].BarCode)[0];
            let itemIndex = itemObjects.findIndex(obj => obj.BarCode === itemObjects[i].BarCode);
            itemObjects[itemIndex].PercentFromShelf = itemRecord ? (itemRecord.Total / totalShelfForAisle) * 100 : 0;
            itemObjects[itemIndex].PercentFromShelfSold = itemRecord ? (itemRecord.Total / totalShelfForCategory) * 100 : 0;
            itemObjects[itemIndex].RecomendedFaces = Math.floor((totalShelfForAisle * 0.8 * (itemObjects[itemIndex].PercentFromBranch / 100)) / itemObjects[itemIndex].Width);
        }

        console.log('itemObjects 1', itemObjects);

        if (currentAisleOnly && currentAisle != null) {
            filteredTrumaSnifRecords = trumaSnifRecords.filter(v =>
                virtualProductDetails[v.BarCode]
                && virtualProductDetails[v.BarCode].position
                && virtualProductDetails[v.BarCode].position.findIndex(p => p.aisle_id === currentAisle) !== -1);

            let barcodes = filteredTrumaSnifRecords.map(obj => { return obj.BarCode; });
            barcodes = barcodes.filter((v, i) => { return barcodes.indexOf(v) == i; });
            itemObjects = itemObjects.filter(function (item) {
                return barcodes.indexOf(item.BarCode) !== -1;
            });
        }

        // sort itemObjects by a combination of PercentFromBranch, PercentFromNetwork and PercentFromShelf
        itemObjects.sort(function (a: any, b: any) {
            let aNumber = parseFloat(a.PercentFromBranch) + parseFloat(a.PercentFromNetwork);
            let bNumber = parseFloat(b.PercentFromBranch) + parseFloat(b.PercentFromNetwork);
            return (aNumber < bNumber) ? 1 : (aNumber > bNumber) ? -1 : 0;
        });

        return itemObjects;
    }

    onSearch(currentAisleOnly: boolean) {
        const { selectedGroup, selectedSubGroup, selectedDepartment,
            /* Barak 9.6.20 */ selectedSegment
        } = this.state;

        const url = window.location.href;
        const urlElements = url.split('/');
        const currentBranch = parseInt(urlElements[5]);
        const currentStore = parseInt(urlElements[6]);

        // const currentBranch = await getBranchByStore(currentStore);

        /* Barak 2.8.20 *
        if (!selectedGroup && !selectedSubGroup && !selectedDepartment)
            return console.log("No category in search");
        *****************/

        this.setState({
            loading: true,
            /* Barak 2.8.20 - clear the slate on each search */ supplierObjects: [],
            /* Barak 2.8.20 */ firstRun: false,
        });

        /* Barak 2.8.20 *
        if (selectedGroup && !selectedSubGroup && !selectedDepartment) {
            console.log('selectedGroup', selectedGroup);
            fetchTrumaSnifByGroup(currentBranch, selectedGroup).then((trumasnifRecords) => {
                fetchTrumaByGroup(selectedGroup).then(async (trumaRecords) => {
                    Const_Truma_Records = trumaRecords;
                    this.setState({ supplierObjects: await this.calculateSupplierPercent(trumasnifRecords, currentAisleOnly), loading: false });
                }).catch((err) => {
                    console.error(err);
                    uiNotify("Unable to load truma table", "error");
                    this.setState({
                        loading: false
                    })
                })
            }).catch((err) => {
                console.error(err);
                uiNotify("Unable to load sales", "error");
                this.setState({
                    loading: false
                })
            })
        }
        if (!selectedGroup && selectedSubGroup && !selectedDepartment) {
            console.log('selectedSubGroup', selectedSubGroup);
            fetchTrumaSnifBySubGroup(currentBranch, selectedSubGroup).then((trumasnifRecords) => {
                fetchTrumaBySubGroup(selectedSubGroup).then(async (trumaRecords) => {
                    Const_Truma_Records = trumaRecords;
                    this.setState({ supplierObjects: await this.calculateSupplierPercent(trumasnifRecords, currentAisleOnly), loading: false });
                }).catch((err) => {
                    console.error(err);
                    uiNotify("Unable to load truma table", "error");
                    this.setState({
                        loading: false
                    })
                })
            }).catch((err) => {
                console.error(err);
                uiNotify("Unable to load sales", "error");
                this.setState({
                    loading: false
                })
            })
        }
        if (!selectedGroup && !selectedSubGroup && selectedDepartment) {
            console.log('selectedDepartment', selectedDepartment);
            fetchTrumaSnifByDepartment(currentBranch, selectedDepartment).then((trumasnifRecords) => {
                fetchTrumaByDepartment(selectedDepartment).then(async (trumaRecords) => {
                    Const_Truma_Records = trumaRecords;
                    this.setState({ supplierObjects: await this.calculateSupplierPercent(trumasnifRecords, currentAisleOnly), loading: false });
                }).catch((err) => {
                    console.error(err);
                    uiNotify("Unable to load truma table", "error");
                    this.setState({
                        loading: false
                    })
                })
            }).catch((err) => {
                console.error(err);
                uiNotify("Unable to load sales", "error");
                this.setState({
                    loading: false
                })
            })
        }
        **********************************************************/
        /* Barak 2.8.20 - get the suppliers relevent for this Aisle only based on items sold in this aisle */
        if (selectedGroup || selectedSubGroup || selectedDepartment || /* Barak 9.6.20 */ selectedSegment) {
            console.log('selectedDepartment', selectedDepartment, 'selectedGroup', selectedGroup, 'selectedSubGroup', selectedSubGroup);
            let department = selectedDepartment ? selectedDepartment : 0;
            let group = selectedGroup ? selectedGroup : 0;
            let subgroup = selectedSubGroup ? selectedSubGroup : 0;
            /* Barak 9.6.20 */ let segment = selectedSegment ? selectedSegment : 0;
            console.log('after selection', selectedGroup, selectedSubGroup, selectedDepartment, selectedSegment);
            fetchTrumaSnifBySelection(currentBranch, department, group, subgroup, segment).then((trumasnifRecords) => {
                fetchTrumaBySelection(department, group, subgroup, segment).then(async (trumaRecords) => {
                    Const_Truma_Records = trumaRecords && trumaRecords.length > 1 ? trumaRecords : [];
                    this.setState({ supplierObjects: await this.calculateSupplierPercent(trumasnifRecords, currentAisleOnly), loading: false });
                }).catch((err) => {
                    console.error(err);
                    uiNotify("Unable to load truma table", "error");
                    this.setState({
                        loading: false
                    })
                })
            }).catch((err) => {
                console.error(err);
                uiNotify("Unable to load sales", "error");
                this.setState({
                    loading: false
                })
            })
        }
        if ((!selectedGroup || selectedGroup === 0) && (!selectedSubGroup || selectedSubGroup === 0) && (!selectedDepartment || selectedDepartment === 0)) {
            console.log('no selection');
            let url = window.location.href;
            const urlElements = url.split('/');
            const currentAisle = parseInt(urlElements[8]);
            fetchTrumaAisleSnif(currentBranch, currentAisle).then((trumasnifRecords) => {
                fetchTrumaByAisle(currentAisle).then(async (trumaRecords) => {
                    Const_Truma_Records = trumaRecords && trumaRecords.length > 1 ? trumaRecords : [];
                    this.setState({ supplierObjects: await this.calculateSupplierPercentInit(trumasnifRecords, true), loading: false });
                }).catch((err) => {
                    console.error(err);
                    uiNotify("Unable to load truma table", "error");
                    this.setState({
                        loading: false
                    })
                })
            }).catch((err) => {
                console.error(err);
                uiNotify("Unable to load sales", "error");
                this.setState({
                    loading: false
                })
            })
        }
        /****************/
        /* Barak 2.8.20 *
        this.setState({
            loading: false
        });
        ****************/
    }

    selectCategorySegment = (value: any) => {
        if (value) {
            this.setState({ selectedSegment: value });
        } else {
            this.setState({ selectedSegment: value });
        }
    }

    selectCategorySubGroup = (value: any) => {
        if (value) {
            this.setState({ selectedSubGroup: value });
            // this.selectGroup.option('disabled', true);
            // this.selectDepartment.option('disabled', true);
        } else {
            this.setState({ selectedSubGroup: value });
            // this.selectGroup.option('disabled', false);
            // this.selectDepartment.option('disabled', false);
        }
    }

    selectCategoryGroup = (value: any) => {
        if (value) {
            this.setState({ selectedGroup: value });
            // this.selectSubGroup.option('disabled', true);
            // this.selectDepartment.option('disabled', true);
        } else {
            this.setState({ selectedGroup: value });
            // this.selectSubGroup.option('disabled', false);
            // this.selectDepartment.option('disabled', false);
        }
    }

    selectCategoryDepartment = (value: any) => {
        if (value) {
            this.setState({ selectedDepartment: value });
            // this.selectSubGroup.option('disabled', true);
            // this.selectGroup.option('disabled', true);
        } else {
            this.setState({ selectedDepartment: value });
            // this.selectSubGroup.option('disabled', false);
            // this.selectGroup.option('disabled', false);
        }
    }


    /* Barak 2.8.20 - When first appearing we need to present the supplier information for the current Branch for the current Aisle suppliers*/
    onCellPrepared = (e: any) => {
        // console.log('onCellPrepared e', e.column);
        if (e.rowType == 'data' && typeof e.column.dataField != 'undefined' && e.column.dataField.indexOf('SupplierColor') >= 0) {
            e.cellElement.style.backgroundColor = e.displayValue; // display background color as the color discribed in the cell value
            e.cellElement.style.color = 'rgba(0, 0, 0, 0)'; // make text invisible
        }
        if (e.rowType == 'data' && typeof e.column.dataField != 'undefined' && e.column.dataField.indexOf('ClassColor') >= 0) {
            e.cellElement.style.backgroundColor = e.displayValue; // display background color as the color discribed in the cell value
            e.cellElement.style.color = 'rgba(0, 0, 0, 0)'; // make text invisible
        }
        if (e.rowType == 'data' && typeof e.column.dataField != 'undefined' && e.column.dataField.indexOf('GroupColor') >= 0) {
            e.cellElement.style.backgroundColor = e.displayValue; // display background color as the color discribed in the cell value
            e.cellElement.style.color = 'rgba(0, 0, 0, 0)'; // make text invisible
        }
        if (e.rowType == 'data' && typeof e.column.dataField != 'undefined' && e.column.dataField.indexOf('SubGroupColor') >= 0) {
            e.cellElement.style.backgroundColor = e.displayValue; // display background color as the color discribed in the cell value
            e.cellElement.style.color = 'rgba(0, 0, 0, 0)'; // make text invisible
        }
        if (e.rowType == 'data' && typeof e.column.dataField != 'undefined' && e.column.dataField.indexOf('SegmentColor') >= 0) {
            e.cellElement.style.backgroundColor = e.displayValue; // display background color as the color discribed in the cell value
            e.cellElement.style.color = 'rgba(0, 0, 0, 0)'; // make text invisible
        }
        if (e.rowType == 'data' && typeof e.column.dataField != 'undefined' && e.column.dataField.indexOf('Color') >= 0) {
            // display background color as the color discribed in the cell value
            // console.log('onCellPrepared Color', e.displayValue);
            e.cellElement.style.backgroundColor = e.displayValue;
            e.cellElement.style.color = e.displayValue;
            // e.cellElement.style.color = 'rgba(0, 0, 0, 0)'; // make text invisible
        }
    }

    componentWillUnmount() {
        localStorage.setItem('origUrl', '');
    }
    /*********************************************************************************************************/

    /* Barak 7.9.20 */
    onOptionChanged = (e: any) => {
        console.log('onOptionChanged e', e.fullName);

        if (e.fullName === 'columns[8].groupIndex') this.colorSupp = !this.colorSupp;
        if (e.fullName === 'columns[9].groupIndex') this.colorClass = !this.colorClass;
        if (e.fullName === 'columns[10].groupIndex') this.colorGroup = !this.colorGroup;
        if (e.fullName === 'columns[11].groupIndex') this.colorSubGroup = !this.colorSubGroup;
        if (e.fullName === 'columns[12].groupIndex') this.colorSegment = !this.colorSegment;

        if (this.colorSegment) {
            this.setState({
                isSuppColorVisible: false,
                isClassColorVisible: false,
                isGroupColorVisible: false,
                isSubGroupColorVisible: false,
                isSegmentColorVisible: true,
            });
            this.props.setColorBy('segment');
            let supplierObjects = JSON.parse(JSON.stringify(this.state.supplierObjects));
            for (let i = 0; i < supplierObjects.length; i++) {
                supplierObjects[i].Color = supplierObjects[i].SegmentColor;
            }
            this.setState({ supplierObjects });
        }
        else if (this.colorSubGroup) {
            this.setState({
                isSuppColorVisible: false,
                isClassColorVisible: false,
                isGroupColorVisible: false,
                isSubGroupColorVisible: true,
                isSegmentColorVisible: false,
            });
            this.props.setColorBy('subgroup');
            let supplierObjects = JSON.parse(JSON.stringify(this.state.supplierObjects));
            for (let i = 0; i < supplierObjects.length; i++) {
                supplierObjects[i].Color = supplierObjects[i].SubGroupColor;
            }
            this.setState({ supplierObjects });
        }
        else if (this.colorGroup) {
            this.setState({
                isSuppColorVisible: false,
                isClassColorVisible: false,
                isGroupColorVisible: true,
                isSubGroupColorVisible: false,
                isSegmentColorVisible: false,
            });
            this.props.setColorBy('group');
            let supplierObjects = JSON.parse(JSON.stringify(this.state.supplierObjects));
            for (let i = 0; i < supplierObjects.length; i++) {
                supplierObjects[i].Color = supplierObjects[i].GroupColor;
            }
            this.setState({ supplierObjects });
        }
        else if (this.colorClass) {
            this.setState({
                isSuppColorVisible: false,
                isClassColorVisible: true,
                isGroupColorVisible: false,
                isSubGroupColorVisible: false,
                isSegmentColorVisible: false,
            });
            this.props.setColorBy('class');
            let supplierObjects = JSON.parse(JSON.stringify(this.state.supplierObjects));
            for (let i = 0; i < supplierObjects.length; i++) {
                supplierObjects[i].Color = supplierObjects[i].ClassColor;
            }
            this.setState({ supplierObjects });
        }
        else if (this.colorSupp) {
            this.setState({
                isSuppColorVisible: true,
                isClassColorVisible: false,
                isGroupColorVisible: false,
                isSubGroupColorVisible: false,
                isSegmentColorVisible: false,
            });
            this.props.setColorBy('supplier');
            let supplierObjects = JSON.parse(JSON.stringify(this.state.supplierObjects));
            for (let i = 0; i < supplierObjects.length; i++) {
                supplierObjects[i].Color = supplierObjects[i].SupplierColor;
            }
            this.setState({ supplierObjects });
        }
        else {
            this.setState({
                isSuppColorVisible: false,
                isClassColorVisible: false,
                isGroupColorVisible: false,
                isSubGroupColorVisible: false,
                isSegmentColorVisible: false,
            });
            this.props.setColorBy('report');
            let supplierObjects = JSON.parse(JSON.stringify(this.state.supplierObjects));
            for (let i = 0; i < supplierObjects.length; i++) {
                supplierObjects[i].Color = '';
            }
            this.setState({ supplierObjects });
        }
        console.log('see colors', this.state.isSuppColorVisible)
    }
    /*********************************************************************************************************/

    render() {
        if (!this.props.showColorMap) {
            /* Barak 2.8.20 */
            if (this.state.supplierObjects.length != 0) {
                this.setState({ supplierObjects: [], selectedGroup: 0, selectedSubGroup: 0, selectedDepartment: 0, selectedSegment: 0, firstRun: true });
            }
            /****************/
            return null;
        }
        const { selectedGroup, selectedSubGroup, selectedDepartment, /* supplierObjects, */ currentAisleOnly, selectedSegment, firstRun } = this.state;

        const { toggleColorMap, displayAisle } = this.props;
        let { classes, groups, subGroups, suppliers, showColorMap, segments, products } = this.props;

        const { height: floatHeight, width: floatWidth } = this.state;


        /* Barak 2.8.20 - When first appearing we need to present the supplier information for the current Branch for the current Aisle suppliers*/
        let { supplierObjects, loading } = this.state;
        let origUrl = localStorage.getItem('origUrl');
        let url;
        if (supplierObjects.length === 0)
            localStorage.setItem('origUrl', window.location.href);
        url = window.location.href;
        console.log('origUrl', origUrl, 'url', url);
        const urlElements = url.split('/');
        // const currentStore = parseInt(urlElements[5]);
        const currentBranch = parseInt(urlElements[5]);
        const currentStore = parseInt(urlElements[6]);
        const currentAisle = parseInt(urlElements[8]);

        // const currentBranch = await getBranchByStore(currentStore);

        // supplierObjects = this.initDataStore();
        if (url != origUrl || (supplierObjects.length === 0 && firstRun)) {
            localStorage.setItem('origUrl', window.location.href);
            // const url = window.location.href;
            const urlElements = url.split('/');
            const currentBranch = parseInt(urlElements[5]);
            // const currentBranch = await getBranchByStore(currentStore);
            // let branches = JSON.parse('[' + currentBranch + ']');
            // this.setState({ loading: true });
            // loading = true;
            fetchTrumaAisleSnif(currentBranch, currentAisle).then((trumasnifRecords) => {
                fetchTrumaByAisle(currentAisle).then(async (trumaRecords) => {
                    Const_Truma_Records = trumaRecords;
                    // this.setState({
                    //     supplierObjects: await this.calculateSupplierPercentInit(trumasnifRecords, true),
                    //     loading: false
                    // });
                    // supplierObjects = await this.calculateSupplierPercentInit(trumasnifRecords, true);
                    this.setState({ supplierObjects: await this.calculateSupplierPercentInit(trumasnifRecords, true), firstRun: false });
                    // loading = false;
                }).catch((err) => {
                    console.error(err);
                    uiNotify("Unable to load truma table", "error");
                    // this.setState({ loading: false });
                    // loading = false;
                })
            }).catch((err) => {
                console.error(err);
                uiNotify("Unable to load sales", "error");
                // this.setState({ loading: false });
                // loading = false;
            })
        }
        /*****************************************************************************************************************************************/

        const classSelectBoxData = new DataSource({
            store: classes,
            paginate: true,
            pageSize: 10
        });
        const groupSelectBoxData = new DataSource({
            store: groups,
            paginate: true,
            pageSize: 10
        });
        const subgroupSelectBoxData = new DataSource({
            store: subGroups,
            paginate: true,
            pageSize: 10
        });
        const segmentSelectBoxData = new DataSource({
            store: segments,
            paginate: true,
            pageSize: 10
        });

        return (
            <Rnd
                dragHandleClassName="planogram-report-handle"
                cancel="planogram-report-container"
                default={{
                    x: floatWidth / 2,
                    y: floatHeight / 2,
                    width: floatWidth /* / 1.2*/ / 0.75,
                    height: floatHeight / 1.2,
                }}
                className="float-window planogram-report"
                resizeHandleClasses={{
                    bottomRight: "planogram-report-resize-handle bottom-right",
                    // bottomLeft: "planogram-report-resize-handle bottom-left",
                }}>
                <div className="float-window-handle planogram-report-handle">
                    {/* <div className="handle-content">{'Supplier Summary Report For Aisle ' + currentAisle}</div> */}
                    <div className="handle-content">{'Summary Color Report For Aisle ' + currentAisle}</div>
                    <div className="float-window-close" onClick={e => {
                        this.colorSupp = false;
                        this.colorClass = false;
                        this.colorGroup = false;
                        this.colorSubGroup = false;
                        this.colorSegment = false;
                        let supplierObjects = JSON.parse(JSON.stringify(this.state.supplierObjects));
                        for (let i = 0; i < supplierObjects.length; i++) {
                            supplierObjects[i].Color = '';
                        }
                        this.setState({ supplierObjects });
                        toggleColorMap(showColorMap);
                    }}>
                        <FontAwesomeIcon icon={faWindowClose} />
                    </div>
                </div>
                <div className="float-window-container planogram-report-container">
                    {this.state.loading || (supplierObjects.length === 0 && firstRun) ?
                        <div className="loader"></div>
                        :
                        <React.Fragment>
                            <div className="report-toolbar">
                                <div className="report-toolbar-container">
                                    <div className="report-toolbar-container">
                                        {/* <Button
                                            rtlEnabled
                                            className="toolbar-item"
                                            disabled={displayAisle == null}
                                            onClick={(e) => this.onSearch(true)}
                                            icon="search"
                                            text="בגונדולה" /> */}
                                        <Button
                                            rtlEnabled
                                            // disabled={selectedBranches == null || selectedBranches.length === 0}
                                            onClick={(e) => this.onSearch(false)}
                                            className="toolbar-item"
                                            icon="search"
                                            text="הצג" />
                                        {/* Barak 6.9.20 - Adding segment */}
                                        <SelectBox
                                            ref={this.selectSegmentRef}
                                            rtlEnabled
                                            showClearButton
                                            className="toolbar-item"
                                            placeholder="סגמנט"
                                            value={selectedSegment}
                                            searchEnabled={true}
                                            valueExpr="Id"
                                            displayExpr="Name"
                                            // items={segments}
                                            dataSource={segmentSelectBoxData}
                                            onValueChanged={(e) => this.selectCategorySegment(e.value)} />
                                        {/*********************************/}
                                        <SelectBox
                                            ref={this.selectSubGroupRef}
                                            rtlEnabled
                                            showClearButton
                                            className="toolbar-item"
                                            placeholder="תת קבוצה"
                                            value={selectedSubGroup}
                                            searchEnabled={true}
                                            valueExpr="Id"
                                            displayExpr="Name"
                                            // items={subGroups}
                                            dataSource={subgroupSelectBoxData}
                                            onValueChanged={(e) => this.selectCategorySubGroup(e.value)} />
                                        <SelectBox
                                            ref={this.selectGroupRef}
                                            rtlEnabled
                                            showClearButton
                                            className="toolbar-item"
                                            placeholder="קבוצה"
                                            value={selectedGroup}
                                            searchEnabled={true}
                                            valueExpr="Id"
                                            displayExpr="Name"
                                            // items={groups}
                                            dataSource={groupSelectBoxData}
                                            onValueChanged={(e) => this.selectCategoryGroup(e.value)} />
                                        <SelectBox
                                            ref={this.selectDepartmentRef}
                                            rtlEnabled
                                            showClearButton
                                            className="toolbar-item"
                                            placeholder="מחלקה"
                                            value={selectedDepartment}
                                            searchEnabled={true}
                                            valueExpr="Id"
                                            displayExpr="Name"
                                            // items={classes}
                                            dataSource={classSelectBoxData}
                                            onValueChanged={(e) => this.selectCategoryDepartment(e.value)} />
                                    </div>
                                </div>
                            </div>
                            <div className="report-content">
                                <DataGrid
                                    rtlEnabled
                                    showRowLines
                                    showColumnHeaders
                                    allowColumnResizing
                                    // allowColumnReordering
                                    className="report-group-table"
                                    noDataText="אין נתונים..."
                                    dataSource={new DataSource({
                                        store: this.state.supplierObjects,
                                    })}
                                    onInitialized={(ref) => { this.dataGrid = ref.component }}
                                    onCellPrepared={(e) => this.onCellPrepared(e)}
                                    onOptionChanged={(e) => this.onOptionChanged(e)}
                                >
                                    <Texts
                                    />
                                    <FilterRow visible />
                                    <HeaderFilter visible />
                                    <Sorting mode="multiple" />
                                    {/* <Sorting /> */}
                                    <Scrolling mode={'virtual'} />
                                    {/* Barak 6.9.20 - add option to group by column */}
                                    {/* GroupPanel - https://js.devexpress.com/Demos/WidgetsGallery/Demo/DataGrid/RecordGrouping/React/Light/ */}
                                    <GroupPanel visible={true} emptyPanelText={'יש לגרור כותרת טור לכאן על מנת לקבץ לפי טור זה'} />
                                    <Grouping autoExpandAll={false} />
                                    {/************************************************/}
                                    <Column
                                        dataField="SupplierColor"
                                        caption="צבע"
                                        // cellRender={({ data }) => {
                                        //     console.log('cellRender data', data);
                                        //     return <div style={{ backgroundColor: data.SupplierColor }}></div>;
                                        // }}
                                        width='40px'
                                        // visible={this.state.isSuppColorVisible}
                                        visible={false}
                                    >
                                    </Column>
                                    {/* Barak 7.9.20 */}
                                    <Column
                                        dataField="Color"
                                        caption="צבע"
                                        width='50px'
                                    ></Column>
                                    <Column
                                        dataField="ClassColor"
                                        caption="צבע"
                                        width='40px'
                                        // visible={this.state.isClassColorVisible}
                                        visible={false}
                                    ></Column>
                                    <Column
                                        dataField="GroupColor"
                                        caption="צבע"
                                        width='40px'
                                        // visible={this.state.isGroupColorVisible}
                                        visible={false}
                                    ></Column>
                                    <Column
                                        dataField="SubGroupColor"
                                        caption="צבע"
                                        width='40px'
                                        // visible={this.state.isSubGroupColorVisible}
                                        visible={false}
                                    ></Column>
                                    <Column
                                        dataField="SegmentColor"
                                        caption="צבע"
                                        width='40px'
                                        // visible={this.state.isSegmentColorVisible}
                                        visible={false}
                                    ></Column>
                                    <Column
                                        dataField="BarCode"
                                        caption="ברקוד"
                                        width={120}
                                    >
                                        {/* <HeaderFilter allowSearch /> */}
                                    </Column>
                                    <Column
                                        dataField="BarCode"
                                        caption="פריט"
                                        width={250}
                                        lookup={{
                                            dataSource: {
                                                store: products,
                                                paginate: true,
                                                pageSize: 30,
                                            },
                                            displayExpr: "Name",
                                            valueExpr: "BarCode",
                                            allowClearing: true
                                        }}
                                    >
                                        {/* <HeaderFilter allowSearch /> */}
                                    </Column>
                                    {/****************/}
                                    <Column
                                        dataField="SupplierId"
                                        caption="ספק"
                                        lookup={{
                                            dataSource: {
                                                store: suppliers,
                                                paginate: true,
                                                pageSize: 30,
                                            },
                                            displayExpr: "Name",
                                            valueExpr: "Id",
                                            allowClearing: true
                                        }}
                                    >
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    {/* Barak 7.9.20 */}
                                    <Column
                                        dataField="ClassId"
                                        caption="מחלקה"
                                        lookup={{
                                            dataSource: {
                                                store: classes,
                                                paginate: true,
                                                pageSize: 30,
                                            },
                                            displayExpr: "Name",
                                            valueExpr: "Id",
                                            allowClearing: true
                                        }}>
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    <Column
                                        dataField="GroupId"
                                        caption="קבוצה"
                                        lookup={{
                                            dataSource: {
                                                store: groups,
                                                paginate: true,
                                                pageSize: 30,
                                            },
                                            displayExpr: "Name",
                                            valueExpr: "Id",
                                            allowClearing: true
                                        }}>
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    <Column
                                        dataField="SubGroupId"
                                        caption="תת-קבוצה"
                                        lookup={{
                                            dataSource: {
                                                store: subGroups,
                                                paginate: true,
                                                pageSize: 30,
                                            },
                                            displayExpr: "Name",
                                            valueExpr: "Id",
                                            allowClearing: true
                                        }}>
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    <Column
                                        dataField="SegmentId"
                                        caption="סגמנט"
                                        lookup={{
                                            dataSource: {
                                                store: segments,
                                                paginate: true,
                                                pageSize: 30,
                                            },
                                            displayExpr: "Name",
                                            valueExpr: "Id",
                                            allowClearing: true
                                        }}>
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    <Column
                                        allowSearch
                                        dataField="TotalFaces"
                                        caption="פייסים"
                                        dataType="number"
                                        format={"##0"}
                                    >
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    <Column
                                        allowSearch
                                        dataField="RecomendedFaces"
                                        caption="פייסים מומלצים"
                                        dataType="number"
                                        format={"##0"}
                                    >
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    {/****************/}
                                    <Column
                                        allowSearch
                                        dataField="PercentFromNetwork"
                                        caption="% תרומה רשתי"
                                        dataType="number"
                                        format={"###,###,##0.##"}
                                    >
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    <Column
                                        allowSearch
                                        dataField="PercentFromBranch"
                                        caption="% תרומה סניפי"
                                        dataType="number"
                                        format={"###,###,##0.##"}
                                    >
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    <Column
                                        allowSearch
                                        dataField="PercentFromShelf"
                                        // caption="% שטח מדף"
                                        caption="% שטח מדף כולל"
                                        dataType="number"
                                        format={"###,###,##0.##"}
                                    >
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    <Column
                                        allowSearch
                                        dataField="PercentFromShelfSold"
                                        // caption="% תרומה מדף"
                                        caption="% שטח מדף תפוס"
                                        dataType="number"
                                        format={"###,###,##0.##"}
                                    >
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    <Summary>
                                        <TotalItem
                                            column={'PercentFromNetwork'}
                                            summaryType={'sum'}
                                            valueFormat={"###,###,##0.##"}
                                            displayFormat={'{0}'} />
                                        <TotalItem
                                            column={'PercentFromBranch'}
                                            summaryType={'sum'}
                                            valueFormat={"###,###,##0.##"}
                                            displayFormat={'{0}'} />
                                        <TotalItem
                                            column={'PercentFromShelf'}
                                            summaryType={'sum'}
                                            valueFormat={"###,###,##0.##"}
                                            displayFormat={'{0}'} />
                                        <TotalItem
                                            column={'PercentFromShelfSold'}
                                            summaryType={'sum'}
                                            valueFormat={"###,###,##0.##"}
                                            displayFormat={'{0}'} />
                                        <GroupItem
                                            column={'PercentFromNetwork'}
                                            summaryType={'sum'}
                                            valueFormat={"###,###,##0.##"}
                                            displayFormat={'{0}'}
                                            alignByColumn={true} />
                                        <GroupItem
                                            column={'PercentFromBranch'}
                                            summaryType={'sum'}
                                            valueFormat={"###,###,##0.##"}
                                            displayFormat={'{0}'}
                                            alignByColumn={true} />
                                        <GroupItem
                                            column={'PercentFromShelf'}
                                            summaryType={'sum'}
                                            valueFormat={"###,###,##0.##"}
                                            displayFormat={'{0}'}
                                            alignByColumn={true} />
                                        <GroupItem
                                            column={'PercentFromShelfSold'}
                                            summaryType={'sum'}
                                            valueFormat={"###,###,##0.##"}
                                            displayFormat={'{0}'}
                                            alignByColumn={true} />
                                    </Summary>
                                </DataGrid>
                            </div>
                        </React.Fragment>
                    }
                </div>
            </Rnd>
        )
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(PlanogramSupplierSummery)