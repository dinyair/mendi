import * as React from "react"
import { connect } from 'react-redux'
import { AppState } from '@src/planogram/shared/store/app.reducer'
import { ThunkDispatch } from 'redux-thunk'
import { AnyAction } from 'redux'
import { barcodeImageSrc } from '../generic/BarcodeImage'
import noImage from "@assets/images/planogram/no-image.jpg"
import { fetchSubstituteBarcodes } from '@src/planogram/shared/api/catalog.provider'
import "@src/planogram/orders.css";

const mapStateToProps = (state: AppState, ownProps: any) => {
    const barcode = state.planogram.display.productDetailer ? state.planogram.display.productDetailer.barcode : null;
    const product = barcode != null && state.newPlano ? state.newPlano.productsMap[barcode] : null;
    const lowSales = state.planogram.display.productDetailer ? state.planogram.display.productDetailer.lowSales : null;
    const shelfFaces = state.planogram.display.productDetailer ? state.planogram.display.productDetailer.shelfFaces : null;
    return {
        barcode,
        product,
        storePosition: barcode && state.planogram.productDetails[barcode] ? state.planogram.productDetails[barcode].position : null,
        maxAmountShelf: barcode && state.planogram.productDetails[barcode] ? state.planogram.productDetails[barcode].maxAmount : null,
        barcodeFacesCount: barcode && state.planogram.productDetails[barcode] ? state.planogram.productDetails[barcode].facesCount : null,
        weeklySales: barcode && state.catalog.productSales[barcode] ? state.catalog.productSales[barcode].weekly : undefined,
        catalogSupplier: product && product.SapakId ? state.system.data.suppliersMap[product.SapakId] : null,
        catalogClass: product && product.ClassesId ? state.system.data.classesMap[product.ClassesId] : null,
        catalogGroup: product && product.GroupId ? state.system.data.groupsMap[product.GroupId] : null,
        catalogSubGroup: product && product.SubGroupId ? state.system.data.subGroupsMap[product.SubGroupId] : null,
        catalogSerie: product && product.DegemId ? state.system.data.seriesMap[product.DegemId] : null,
        catalogSegment: product && product.SegmentId ? state.system.data.segmentsMap[product.SegmentId] : null,
        catalogModfel: product && product.ModelId ? state.system.data.modelsMap[product.ModelId] : null,
        statusCalcInv: state.planogram.store ? state.planogram.store.statusCalcInv : 0,
        calcInvDays: state.planogram.store ? state.planogram.store.calcInvDays : 0,
        markBadProducts: state.displayOptions?.productWithoutSize,
        markArchiveProducts: state.planogram.display.markArchiveProducts,
        lowSales,
        hourlySales: barcode && state.catalog.productSales[barcode] ? state.catalog.productSales[barcode].hourly : undefined,
        productDetails: state.planogram.productDetails,
        productSales: state.catalog.productSales,
        shelfFaces,
    }
}
const mapDispatchToProps = (dispatch: ThunkDispatch<AppState, {}, AnyAction>) => ({
});


class PlanogramProductDetail extends React.Component<ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>> {
    state = {
        maxAmountShelfState: 0,
        barcodeFacesCountState: 0,
        weeklySalesState: 0,
        hourlySalesState: 0,
    }
    componentDidUpdate = async (prevProps: any) => {
        if (this.props.barcode != prevProps.barcode) {
            let sub_barcodes = await fetchSubstituteBarcodes(this.props.barcode);
            let maxAmountShelf = 0;
            let barcodeFacesCount = 0;
            let weeklySales = 0;
            let hourlySales = 0;
            for (let i = 0; i < sub_barcodes.length; i++) {
                maxAmountShelf += this.props.productDetails[sub_barcodes[i]] ? this.props.productDetails[sub_barcodes[i]].maxAmount : 0;
                barcodeFacesCount += this.props.productDetails[sub_barcodes[i]] ? this.props.productDetails[sub_barcodes[i]].facesCount : 0;
                if (this.props.productSales && this.props.productSales[sub_barcodes[i]]) {
                    weeklySales += this.props.productSales[sub_barcodes[i]].weekly ? parseInt(JSON.stringify(this.props.productSales[sub_barcodes[i]].weekly)) : 0;
                    hourlySales += this.props.productSales[sub_barcodes[i]].hourly ? parseInt(JSON.stringify(this.props.productSales[sub_barcodes[i]].hourly)) : 0;
                }
            }
            // we include the substitute item information on the server so there is no need to include it again
            this.setState({ maxAmountShelfState: maxAmountShelf, barcodeFacesCountState: barcodeFacesCount/*,weeklySalesState:weeklySales,hourlySalesState:hourlySales*/ });
        }
    }
    /**************************************************************************/
    render() {
        const {
            barcode,
            product,
            storePosition,
            maxAmountShelf,
            barcodeFacesCount,
            catalogSupplier,
            catalogClass,
            catalogGroup,
            catalogSubGroup,
            catalogSerie,
            catalogSegment,
            catalogModfel,
            weeklySales,
            markBadProducts,
            markArchiveProducts,
            lowSales,
            shelfFaces,
            hourlySales,
        } = this.props;

        let todaytTimeStamp = new Date().getTime();
        let createDateTimeStamp = null;
        if (product && product.Create_Date != undefined) createDateTimeStamp = new Date(product.Create_Date).getTime();

        if (barcode == null || product == null)
            return null;
        return (
            <div className="planogram-product-detailer">
                <div className="detailer-section">
                    <div>
                        {/* <h2 className="detailer-section-title">{product.Name}</h2> */}
                        <h3 className="detailer-section-title">
                            {product.Name}
                            <span className="hover-content">
                                {product.Name}
                            </span>
                        </h3>
                    </div>
                    <div className="detailer-row">
                        <label>ברקוד:</label>
                        <span>{product.BarCode}</span>
                    </div>
                    <div className="detailer-row">
                        <label>ספק:</label>
                        <span className="detailer-span-title-supp">{(catalogSupplier ? catalogSupplier.Name : product.SapakId) || "---"}
                            <span className="hover-content">
                                {(catalogSupplier ? catalogSupplier.Name : product.SapakId) || "---"}
                            </span>
                        </span>
                    </div>
                    <div className="detailer-row">
                        <label>מחלקה:</label>
                        <span>{(catalogClass ? catalogClass.Name : product.ClassesId) || "---"}</span>
                    </div>
                    <div className="detailer-row">
                        <label>קבוצה:</label>
                        <span>{(catalogGroup ? catalogGroup.Name : product.GroupId) || "---"}</span>
                    </div>
                    <div className="detailer-row">
                        <label>תת קבוצה:</label>
                        <span>{(catalogSubGroup ? catalogSubGroup.Name : product.SubGroupId) || "---"}</span>
                    </div>
                    <div className="detailer-row">
                        <label>סדרה:</label>
                        <span className="detailer-span-title-series">{(catalogSerie ? catalogSerie.Name : product.DegemId) || "---"}
                            <span className="hover-content">
                                {(catalogSerie ? catalogSerie.Name : product.DegemId) || "---"}
                            </span>
                        </span>
                    </div>
                    <div className="detailer-row">
                        <label>מותג:</label>
                        <span>{(catalogModfel ? catalogModfel.Name : product.ModelId) || "---"}</span>
                    </div>
                    <div className="detailer-row">
                        <label>סגמנט:</label>
                        <span>{(catalogSegment ? catalogSegment.Name : product.SegmentId) || "---"}</span>
                    </div>
                </div>
                <div className="detailer-section">
                    <h3 className="detailer-section-title">גדלים</h3>
                    <div className="detailer-row">
                        <label>גובה:</label>
                        <span>{product.height != null ? product.height + "mm" : null}</span>
                    </div>
                    <div className="detailer-row">
                        <label>רוחב:</label>
                        <span>{product.width != null ? product.width + "mm" : null}</span>
                    </div>
                    <div className="detailer-row">
                        <label>עומק:</label>
                        <span>{product.length != null ? product.length + "mm" : null}</span>
                    </div>
                    <div className="detailer-row">
                        <label>מדף:</label>
                        <span style={{ fontSize: "0.75em", maxWidth: 150 }}>
                            {storePosition && storePosition.length > 0 ? storePosition.map(p => p.shelf).join(', ') : null}
                        </span>
                    </div>
                </div>
                <div className="detailer-section">
                    <h3 className="detailer-section-title">סטטיסטיקה</h3>
                    <div className="detailer-row">
                        <label>כמות פייסים (גונדולה):</label>
                        <span>{barcodeFacesCount || this.state.barcodeFacesCountState > 0 ? (barcodeFacesCount ? barcodeFacesCount : 0) + this.state.barcodeFacesCountState : barcodeFacesCount}</span>
                    </div>
                    <div className="detailer-row">
                        <label>כמות פייסים (מדף):</label>
                        <span>{shelfFaces}</span>
                    </div>
                    <div className="detailer-row">
                        <label>קיבולת מדף:</label>
                        <span>{maxAmountShelf || this.state.maxAmountShelfState > 0 ? (maxAmountShelf ? maxAmountShelf : 0) + this.state.maxAmountShelfState : maxAmountShelf}</span>
                    </div>
                    <div className="detailer-row">
                        <label>ממוצע מכירות:</label>
                        <span>{weeklySales != null || this.state.weeklySalesState > 0 ? Math.round((weeklySales ? weeklySales : 0) + this.state.weeklySalesState) : "~"}</span>
                    </div>
                    <div className="detailer-row">
                        <label>מכר אונליין:</label>
                        <span>{hourlySales != null || this.state.hourlySalesState > 0 ? Math.round((hourlySales ? hourlySales : 0) + this.state.hourlySalesState) : "~"}</span>
                    </div>
                    {product.IsSubBar ? <div className="detailer-row"><label style={{ fontWeight: "bold" }}>הנתונים כוללים תחליפי</label></div> : null}
                    {markBadProducts && !product.width && !product.height && !product.length ? <div className="detailer-row"><label style={{ fontWeight: "bold" }}>הצבע מייצג פריט ללא מידות</label></div> : null}
                    {markArchiveProducts && product.Archives === 1 ? <div className="detailer-row"><label style={{ fontWeight: "bold" }}>הצבע מייצג פריט ארכיון</label></div> : null}
                    {markArchiveProducts && product.Archives === 0
                        && product.Bdate_sale && product.Bdate_sale != '' ? <div className="detailer-row"><label style={{ fontWeight: "bold" }}>הצבע מייצג פריט חסום מכירה</label></div> : null}
                    {markArchiveProducts && product.Archives === 0
                        && (!product.Bdate_sale || (product.Bdate_sale && product.Bdate_sale === ''))
                        && product.Bdate_buy && product.Bdate_buy != '' ? <div className="detailer-row"><label style={{ fontWeight: "bold" }}>הצבע מייצג פריט חסום רכש</label></div> : null}
                    {markArchiveProducts && product.Archives === 0
                        && (!product.Bdate_sale || (product.Bdate_sale && product.Bdate_sale === ''))
                        && (!product.Bdate_buy || (product.Bdate_buy && product.Bdate_buy === ''))
                        && lowSales ? <div className="detailer-row"><label style={{ fontWeight: "bold" }}>הצבע מייצג פריט דל מכר</label></div> : null}
                </div>

                <div className="detailer-section" style={{ backgroundColor: "#FFF", padding: "10px", display: "flex" }}>
                    <div className="product">
                        <div className="product-badges" style={{ height: "0" }}
                        >
                            {product && product.Create_Date != undefined && createDateTimeStamp && todaytTimeStamp - createDateTimeStamp < (90 * 24 * 60 * 60 * 1000)
                                ?
                                <div className="product-badge sale-badge" style={{ position: "relative",top:"0" }}>
                                    <svg className="badge-icon" aria-hidden="true" focusable="false" data-prefix="fas"
                                        data-icon="certificate" role="img" xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 512 512">
                                        <path fill="currentColor"
                                            d="M458.622 255.92l45.985-45.005c13.708-12.977 7.316-36.039-10.664-40.339l-62.65-15.99 17.661-62.015c4.991-17.838-11.829-34.663-29.661-29.671l-61.994 17.667-15.984-62.671C337.085.197 313.765-6.276 300.99 7.228L256 53.57 211.011 7.229c-12.63-13.351-36.047-7.234-40.325 10.668l-15.984 62.671-61.995-17.667C74.87 57.907 58.056 74.738 63.046 92.572l17.661 62.015-62.65 15.99C.069 174.878-6.31 197.944 7.392 210.915l45.985 45.005-45.985 45.004c-13.708 12.977-7.316 36.039 10.664 40.339l62.65 15.99-17.661 62.015c-4.991 17.838 11.829 34.663 29.661 29.671l61.994-17.667 15.984 62.671c4.439 18.575 27.696 24.018 40.325 10.668L256 458.61l44.989 46.001c12.5 13.488 35.987 7.486 40.325-10.668l15.984-62.671 61.994 17.667c17.836 4.994 34.651-11.837 29.661-29.671l-17.661-62.015 62.65-15.99c17.987-4.302 24.366-27.367 10.664-40.339l-45.984-45.004z"
                                            className=""></path>
                                    </svg>
                                    <span className="badge-label">
                                        <div className="label-bold">{'חדש'}</div>
                                    </span>
                                </div>
                                : null
                            }
                        </div>
                        <img src={barcodeImageSrc(product.client_image ? product.client_image : product.BarCode)} onError={e => {
                            e.currentTarget.src = noImage;
                        }}
                            style={{ maxHeight: "12vh", maxWidth: "12vh", margin: "auto" }} />
                    </div>
                </div>
            </div>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PlanogramProductDetail)