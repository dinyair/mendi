import * as React from "react";
import { connect } from 'react-redux';
import { AppState } from '@src/planogram/shared/store/app.reducer';
import { ThunkDispatch } from 'redux-thunk';
import { AnyAction } from 'redux';
import Draggable from 'react-draggable';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faWindowClose, faEye, faEyeSlash, faDownload, faUpload, faRecycle, faSave } from '@fortawesome/free-solid-svg-icons';
import { toggleSettings, toggleRowItems, setColorBy, toggleAlignLeft } from '@src/planogram/shared/store/planogram/planogram.actions';
import { toggleShelfItems, toggleBadProductsMarker, toggleArchiveProductsMarker, toggleSaleTags } from '@src/planogram/shared/store/planogram/display/display.actions';
import { JsonDownloadButton, fileReadyCurrentTime } from '../generic/MenuButtons';
import { setModal, toggleModal } from '@src/planogram/shared/components/Modal';
import ImportModal from '@src/planogram/ImportModal';
import { uiNotify } from '@src/planogram/shared/components/Toast';
import { PlanogramStore } from '@src/planogram/shared/store/planogram/planogram.types';
import * as planogramProvider from "@src/planogram/shared/api/planogram.provider";
import { shelfItemDimensions } from '@src/planogram/provider/calculation.service';
import { PLANOGRAM_ID, SectionDefaultDimension, ShelfDefaultDimension } from '@src/planogram/shared/store/planogram/planogram.defaults';
import { CatalogProduct } from '@src/planogram/shared/interfaces/models/CatalogProduct';
import { updateCatalogProducts } from '@src/planogram/shared/store/catalog/catalog.action';
import { setStore } from '@src/planogram/shared/store/planogram/store/store.actions';
import { Rnd } from 'react-rnd';
import { refreshBarcodeImage } from '../generic/BarcodeImage';
import * as planogramApi from '@src/planogram/shared/api/planogram.provider';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
// import Tabs from 'devextreme-react/tabs';

const mapStateToProps = (state: AppState, ownProps: any) => ({
    displaySettings: state.planogram.display.showSettings,
    hideShelfItems: state.displayOptions?.hideProducts,
    showRowItems: state.displayOptions?.showProdutDepth,
    markBadProducts: state.displayOptions?.productWithoutSize,
    /* Barak 9.6.2020 - Add statusCalcInv, calcInvDays to props */
    statusCalcInv: state.planogram.store ? state.planogram.store.statusCalcInv : 0,
    calcInvDays: state.planogram.store ? state.planogram.store.calcInvDays : 0,
    /* osher 14.9.20 */itemTagFlag: state.planogram.store && state.planogram.store.itemTagFlag ? state.planogram.store.itemTagFlag : 0,
    /******************/
    /* Barak 17.6.2020 - Add action to mark Archive products */
    markArchiveProducts: state.planogram.display.markArchiveProducts,
    /******************/
    /* Barak 23.6.2020 - Add statusCalMlay options to the averages tab  */
    Status0Percent: state.planogram.store ? state.planogram.store.Status0Percent : 100,
    Status1Percent: state.planogram.store ? state.planogram.store.Status1Percent : 100,
    Status2Percent: state.planogram.store ? state.planogram.store.Status2Percent : 100,
    Status3Percent: state.planogram.store ? state.planogram.store.Status3Percent : 60,
    Status4Percent: state.planogram.store ? state.planogram.store.Status4Percent : 100,
    /******************/
    /* Barak 14.9.2020 - Add default sizes for section and shelf  */
    secHeight: state.planogram.store && state.planogram.store.Section_height ? state.planogram.store.Section_height : SectionDefaultDimension.height,
    secWidth: state.planogram.store && state.planogram.store.Section_width ? state.planogram.store.Section_width : SectionDefaultDimension.width,
    secDepth: state.planogram.store && state.planogram.store.Section_depth ? state.planogram.store.Section_depth : SectionDefaultDimension.depth,
    shelfHeight: state.planogram.store && state.planogram.store.Shelf_height ? state.planogram.store.Shelf_height : ShelfDefaultDimension.height,
    shelfWidth: state.planogram.store && state.planogram.store.Shelf_width ? state.planogram.store.Shelf_width : ShelfDefaultDimension.width,
    shelfDepth: state.planogram.store && state.planogram.store.Shelf_depth ? state.planogram.store.Shelf_depth : ShelfDefaultDimension.depth,
    /******************/
    /* Barak 29.9.20 - Align all items to left or not align */ alignToLeft: state.planogram.display.alignToLeft,

    showColorMap: state.planogram.display.colorBy != null,

    store: state.planogram.store,
    productMap: state.newPlano ? state.newPlano.productsMap : null,

    branchMap: state.system.data.branchesMap,

    /* Barak 24.12.20 - allow hiding of sales tags */ hideSaleTags: state.displayOptions?.hideTags,
})

const mapDispatchToProps = (dispatch: ThunkDispatch<AppState, any, AnyAction>) => ({
    toggleSettings: () => dispatch(toggleSettings()),
    toggleShelfItems: () => dispatch(toggleShelfItems()),
    toggleRowItems: () => dispatch(toggleRowItems()),
    toggleBadProductsMarker: () => dispatch(toggleBadProductsMarker()),
    /* Barak 17.6.2020 - Add action to mark Archive products */
    toggleArchiveProductsMarker: () => dispatch(toggleArchiveProductsMarker()),
    /* Barak 7.9.20  toggleColorMap: (showColorMap: boolean) => dispatch(showColorMap ? setColorBy(null) : setColorBy("supplier")), */
    /* Barak 7.9.20 */ toggleColorMap: (showColorMap: boolean) => dispatch(showColorMap ? setColorBy(null) : setColorBy("report")),
    updatedProducts: (products: CatalogProduct[]) => dispatch(updateCatalogProducts(products)),
    /* Barak 29.9.20 - Align all items to left or not align */ toggleAlignLeft: () => dispatch(toggleAlignLeft()),
    /* Barak 24.12.20 - allow hiding of sales tags */ toggleSaleTags: () => dispatch(toggleSaleTags()),
})

type PlanogramSettingsProps = ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>;

type AisleDataRow = {
    aisle?: string,
    shelf?: number,
    position?: number,
    faces?: number,
    manual_row_only: number,
    row?: number,
    barcode?: number,
    height?: number,
    width?: number,
    depth?: number
}

class PlanogramSettings extends React.Component<PlanogramSettingsProps> {
    state = {
        selectedIndex: 0,
        /* osher 14.9.20 */ itemTagFlag: 0
    }
    /* osher 14.9.20 */
    componentDidMount() {

        this.setState({ itemTagFlag: this.props.itemTagFlag });
    }
    /*****************/
    updateMissingProducts = async (data: AisleDataRow[]) => {
        // const catalog = this.props.productMap;
        const missingProducts = [];

        for (let i = 0; i < data.length; i++) {
            const dataProduct = data[i];
            if (dataProduct.barcode)
                missingProducts.push(dataProduct);
        }
        const updateProducts = missingProducts.map(p => ({
            barcode: p.barcode,
            dimensions: {
                width: p.width,
                height: p.height,
                depth: p.depth,
            }
        }));
        return await planogramProvider.updateMultipleProductsDimensions(updateProducts);
    }

    handleDataProducts = async (data: any[]) => {
        const updatedProducts = await this.updateMissingProducts(data);
        this.props.updatedProducts(updatedProducts);

        // const catalog = {
        //     ...this.props.productMap,
        // }
        // if (updatedProducts)
        //     for (let i = 0; i < updatedProducts.length; i++) {
        //         catalog[updatedProducts[i].BarCode] = updatedProducts[i];
        //     }

        // const fillStore = { ...currentStore };
        // for (const dataAisle of data) {
        //     const aisleIndex = fillStore.aisles.findIndex(a => a.name === dataAisle.name);
        //     if (aisleIndex === -1)
        //         continue;
        //     const storeAisle = fillStore.aisles[aisleIndex];
        //     const productList = [...dataAisle.products];
        //     // shelf_max_index for switch shelf index
        //     const shelf_max_index = productList.length > 0 ? productList.map(p => p.shelf).reduce((p, c) => Math.max(p, c)) : 0;

        //     let currentProduct: AisleDataRow | undefined = productList.shift();
        //     while (productList.length > 0) {
        //         if (currentProduct == null)
        //             break;
        //         const barcode = currentProduct.barcode;
        //         const productFaces = currentProduct.faces;
        //         const productWidth = currentProduct.width * 10 * currentProduct.faces;
        //         const dataShelfIndex = shelf_max_index - currentProduct.shelf;


        //         const shelvesSameIndex = storeAisle.sections.map((se) => se.shelves[dataShelfIndex]);
        //         for (let i = 0; i < shelvesSameIndex.length; i++) {
        //             const sameIndexShelf = shelvesSameIndex[i];

        //             const shelfWidth = sameIndexShelf.dimensions.width;
        //             const shelfProductsWidth = sameIndexShelf.items.length > 0 ? sameIndexShelf.items.map(item => {
        //                 const catalogProduct = catalog[item.product];
        //                 return shelfItemDimensions(item.placement, {
        //                     width: catalogProduct.width || 10,
        //                     height: catalogProduct.height || 10,
        //                     depth: catalogProduct.length || 10,
        //                 })
        //             }).map(dm => dm.width).reduce((p, c) => p + c) : 0;
        //             const availableShelfWidth = shelfWidth - shelfProductsWidth;
        //             if (shelfWidth > productWidth && availableShelfWidth - productWidth < 0)
        //                 continue;


        //             storeAisle.sections[i].shelves[dataShelfIndex].items.push({
        //                 id: sameIndexShelf.id + PLANOGRAM_ID.ITEM + shelvesSameIndex[i].items.length,
        //                 placement: {
        //                     faces: productFaces,
        //                     row: 1,
        //                     stack: 1,
        //                 },
        //                 product: barcode
        //             });
        //             break;
        //         }
        //         currentProduct = productList.shift();
        //     }
        //     fillStore.aisles[aisleIndex] = storeAisle;
        // }
        // if (updatedProducts)
        //     this.props.setStore(fillStore, updatedProducts);
        // else
        //     this.props.setStore(fillStore);
    }
    onTabsSelectionChanged = async (args: any) => {
        this.setState({
            selectedIndex: args.value
        });
    }

    render() {
        if (!this.props.displaySettings) return null;
        const { toggleSettings, toggleRowItems, toggleShelfItems, toggleSaleTags, toggleBadProductsMarker, toggleArchiveProductsMarker, toggleColorMap, toggleAlignLeft } = this.props;
        const { store, branchMap, hideShelfItems, hideSaleTags, showRowItems, markBadProducts, showColorMap, markArchiveProducts, alignToLeft } = this.props;
        if (!store) return null;
        /* Barak 9.6.2020 */
        let { statusCalcInv, calcInvDays } = this.props;
        let selectedIndex = this.state.selectedIndex;
        // let statusCalMlay = [
        //     { Id: 0, Status: 'weekly' },
        //     { Id: 1, Status: 'biweekly' },
        //     { Id: 2, Status: 'monthly' },
        //     { Id: 3, Status: 'Number of days' }
        // ];
        /******************/
        /* Barak 23.6.2020 */
        let { Status0Percent, Status1Percent, Status2Percent, Status3Percent, Status4Percent } = this.props;
        console.log('hideSaleTags', hideSaleTags);
        /* Barak 14.9.20 */  let { secHeight, secWidth, secDepth, shelfHeight, shelfWidth, shelfDepth } = this.props;



        /******************/
        return (
            <Rnd
                dragHandleClassName="settings-header"
                className="planogram-settings"
                minHeight="28em"
                minWidth="50em"
            >
                <Tabs>
                    {/* <div className="planogram-settings"> */}
                    <div className="settings-header" >
                        <div className="header-title">Planogram Settings: </div>
                        {/* <div className="header-close" onClick={(e) => toggleSettings()}> 
                    <FontAwesomeIcon icon={faWindowClose} />
                    </div>*/}
                    </div>
                    {/* Barak 17.6.2020 - Add tabs */}
                    <div style={{ padding: "1em", height: "100%" }}>
                        <TabList>
                            <Tab>Main</Tab>
                            <Tab>Set Average</Tab>
                        </TabList>

                        <TabPanel>
                            <div className="settings-container" style={{ height: "28em", width: "50em" }}>
                                <div className="container-section">
                                    <div>
                                        <h3>{store.name || `Aisle: ${store.store_id}`}</h3>
                                        <h5>{branchMap[store.branch_id] != null ? `${branchMap[store.branch_id].Name}(${store.branch_id})` : `::${store.branch_id}`}</h5><h3 className="section-title">Store Actions</h3>
                                    </div>
                                    {/* Barak 17.6.2020 - remove *
                                    <div className="settings-spec-table">
                                        <div className="spec-row">
                                            <div className="spec-item">Set As Main Store: </div>
                                            <div className="spec-item">
                                                <span className="row-input switch">
                                                    <input
                                                        type="checkbox"
                                                        className="switch-input"
                                                        disabled
                                                        checked={true}
                                                        onChange={e => { }} />
                                                    <div className="switch-display"></div>
                                                </span>
                                            </div>
                                        </div>
                                    </div> */}
                                    <h3 className="section-title">Actions</h3>
                                    <div className="settings-spec-table">
                                        <div className="spec-row">
                                            <div className="spec-item">Download Store To Local Disk: </div>
                                            <div className="spec-item">
                                                <JsonDownloadButton filename={fileReadyCurrentTime() + "_" + store.store_id} getData={() => store} >
                                                    <FontAwesomeIcon icon={faDownload} />
                                                    <span>Download</span>
                                                </JsonDownloadButton>
                                            </div>
                                        </div>
                                        <div className="spec-row">
                                            <div className="spec-item">Import Dimensions CSV: </div>
                                            <div className="spec-item">
                                                <button onClick={() => {
                                                    setModal(() => <ImportModal onSubmit={(data) => {
                                                        if (data == null || data.length === 0)
                                                            return console.log("Nothing loaded.");
                                                        if (this.props.store == null)
                                                            return console.log("No store found.");
                                                        toggleModal();
                                                        this.handleDataProducts(data).then(() => {
                                                            uiNotify("Store was successfully saved.", "success", 5000);
                                                        }).catch(err => {
                                                            uiNotify("", "error", 5000);
                                                            console.error(err);
                                                        });
                                                    }} />)
                                                }}>
                                                    <FontAwesomeIcon icon={faUpload} />
                                                    <span>Load CSV</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="container-section">
                                    <h3 className="section-title">Display</h3>
                                    <div className="settings-spec-table">
                                        <div className="spec-row">
                                            <div className="spec-item">
                                                <label className="row-label">Display Shelf Items: </label>
                                            </div>
                                            <div className="spec-item">
                                                <span className="row-input switch">
                                                    <input
                                                        type="checkbox"
                                                        className="switch-input"
                                                        checked={!hideShelfItems}
                                                        onChange={e => toggleShelfItems()} />
                                                    <div className="switch-display"></div>
                                                </span>
                                            </div>
                                        </div>
                                        {/* Barak 24.12.20 */}
                                        <div className="spec-row">
                                            <div className="spec-item">
                                                <label className="row-label">Display Sales Tags: </label>
                                            </div>
                                            <div className="spec-item">
                                                <span className="row-input switch">
                                                    <input
                                                        type="checkbox"
                                                        className="switch-input"
                                                        checked={!hideSaleTags}
                                                        onChange={e => toggleSaleTags()} />
                                                    <div className="switch-display"></div>
                                                </span>
                                            </div>
                                        </div>
                                        {/******************/}
                                        <div className="spec-row">
                                            <div className="spec-item">
                                                <label className="row-label">Mark Products Without Dimensions: </label>
                                            </div>
                                            <div className="spec-item">
                                                <span className="row-input switch">
                                                    <input
                                                        type="checkbox"
                                                        className="switch-input"
                                                        checked={markBadProducts}
                                                        onChange={e => toggleBadProductsMarker()} />
                                                    <div className="switch-display"></div>
                                                </span>
                                            </div>
                                        </div>
                                        {/* Barak 17.6.2020 - Add action to mark Archive products */}
                                        <div className="spec-row">
                                            <div className="spec-item">
                                                <label className="row-label">Mark Archive Products: </label>
                                            </div>
                                            <div className="spec-item">
                                                <span className="row-input switch">
                                                    <input
                                                        type="checkbox"
                                                        className="switch-input"
                                                        checked={markArchiveProducts}
                                                        onChange={e => toggleArchiveProductsMarker()} />
                                                    <div className="switch-display"></div>
                                                </span>
                                            </div>
                                        </div>
                                        {/********************************************************/}
                                        <div className="spec-row">
                                            <div className="spec-item">
                                                <label className="row-label">Render Item Rows: </label>
                                            </div>
                                            <div className="spec-item">
                                                <span className="row-input switch">
                                                    <input
                                                        type="checkbox"
                                                        className="switch-input"
                                                        checked={showRowItems}
                                                        onChange={e => toggleRowItems()} />
                                                    <div className="switch-display"></div>
                                                </span>
                                            </div>
                                        </div>
                                        {/* Barak 29.9.20 - Align all items to left or not align */}
                                        <div className="spec-row">
                                            <div className="spec-item">
                                                <label className="row-label">Align To Left: </label>
                                            </div>
                                            <div className="spec-item">
                                                <span className="row-input switch">
                                                    <input
                                                        type="checkbox"
                                                        className="switch-input"
                                                        checked={alignToLeft}
                                                        onChange={e => toggleAlignLeft()} />
                                                    <div className="switch-display"></div>
                                                </span>
                                            </div>
                                        </div>
                                        {/********************************************************/}
                                        {/* Barak 22.9.20 - remove this report from settings and move it to context menu *
                                        <div className="spec-row">
                                            <div className="spec-item">
                                                {/ <label className="row-label">Colorize Supplier Products</label> /}
                                                <label className="row-label">Colorize Products</label>
                                            </div>
                                            <div className="spec-item">
                                                <span className="row-input switch">
                                                    <input
                                                        type="checkbox"
                                                        className="switch-input"
                                                        checked={showColorMap}
                                                        onChange={e => toggleColorMap(showColorMap)} />
                                                    <div className="switch-display"></div>
                                                </span>
                                            </div>
                                        </div>
                                        */}
                                        <div className="spec-row">
                                            <div className="spec-item">Update Image Cache</div>
                                            <div className="spec-item">
                                                <button onClick={refreshBarcodeImage}>
                                                    <FontAwesomeIcon icon={faRecycle} />
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {/* Barak 17.6.2020 - Add 
                            <div style={{ paddingBottom: '1em', width: '3em', marginLeft: 'auto' }} onClick={(e) => toggleSettings()}>
                                <FontAwesomeIcon icon={faWindowClose} style={{ fontSize: '1.4em' }} />
                            </div>
                            *************************/}
                        </TabPanel>
                        <TabPanel>
                            {/* Barak 17.6.2020 - Add */}
                            <div className="settings-container" style={{ height: "32em", width: "50em" }}>
                                <div className="settings-spec-table">
                                    <div className="container-section">
                                        {/* osher 14.9.20 - supporting sale average methos to now include minimum stock levels */}
                                        <h3 className="section-title" style={{ marginBottom: "10px" }}>Pick sale average method</h3>
                                        <div className="spec-row" style={{ justifyContent: "flex-start", borderBottom: "none" }}>
                                            <div className="spec-item">
                                                <input type="radio" name="tagFlag" value="0" onChange={(e) => this.setState({ itemTagFlag: parseInt(e.target.value) })} defaultChecked={this.state.itemTagFlag === 0} />
                                                <label className="row-label">&nbsp;	Number of days </label>
                                            </div>
                                            <div className="spec-item">
                                                <input type="radio" name="tagFlag" value="1" onChange={(e) => this.setState({ itemTagFlag: parseInt(e.target.value) })} defaultChecked={this.state.itemTagFlag === 1} />
                                                <label className="row-label">&nbsp;Minimum Stock Level </label>
                                            </div>
                                        </div>
                                        <div className="spec-row" style={{ justifyContent: "flex-start", borderTop: "none", padding: "10px", alignItems: "center" }}>
                                            <label style={{ color: "lightgrey" }}> pick number of days: &nbsp;&nbsp;</label>
                                            <label htmlFor="7"> 7: </label>
                                            <input type="radio" id="7" name="days" value="7" onChange={(e) => calcInvDays = parseInt(e.target.value)} defaultChecked={calcInvDays === 7} disabled={this.state.itemTagFlag === 1} />
                                            <label htmlFor="14">&nbsp;	&nbsp;	 14: </label>
                                            <input type="radio" id="14" name="days" value="14" onChange={(e) => calcInvDays = parseInt(e.target.value)} defaultChecked={calcInvDays === 14} disabled={this.state.itemTagFlag === 1} />
                                            <label htmlFor="21">&nbsp;	&nbsp;	 21: </label>
                                            <input type="radio" id="21" name="days" value="21" onChange={(e) => calcInvDays = parseInt(e.target.value)} defaultChecked={calcInvDays === 21} disabled={this.state.itemTagFlag === 1} />
                                            <label htmlFor="28">&nbsp;	&nbsp;	 28: </label>
                                            <input type="radio" id="28" name="days" value="28" onChange={(e) => calcInvDays = parseInt(e.target.value)} defaultChecked={calcInvDays === 28} disabled={this.state.itemTagFlag === 1} />
                                            <label htmlFor="35">&nbsp;	&nbsp;	 35: </label>
                                            <input type="radio" id="35" name="days" value="35" onChange={(e) => calcInvDays = parseInt(e.target.value)} defaultChecked={calcInvDays === 35} disabled={this.state.itemTagFlag === 1} />
                                            <label htmlFor="42">&nbsp;	&nbsp;	 42: </label>
                                            <input type="radio" id="42" name="days" value="42" onChange={(e) => calcInvDays = parseInt(e.target.value)} defaultChecked={calcInvDays === 42} disabled={this.state.itemTagFlag === 1} />
                                            <label htmlFor="49">&nbsp;	&nbsp;	 49: </label>
                                            <input type="radio" id="49" name="days" value="49" onChange={(e) => calcInvDays = parseInt(e.target.value)} defaultChecked={calcInvDays === 49} disabled={this.state.itemTagFlag === 1} />
                                            <label htmlFor="56">&nbsp;	&nbsp;	 56: </label>
                                            <input type="radio" id="56" name="days" value="56" onChange={(e) => calcInvDays = parseInt(e.target.value)} defaultChecked={calcInvDays === 56} disabled={this.state.itemTagFlag === 1} />
                                        </div>
                                    </div>
                                    {/*************************/}
                                    <div className="container-section">
                                        <h3 className="section-title" style={{ marginBottom: 0 }}>Define sale percentage for Item types</h3>
                                        <div className="spec-row" >
                                            <div className="spec-item">
                                                <label >Regular Item: </label>
                                                <input
                                                    // autoFocus
                                                    type="number"
                                                    name="RegularItemPercent"
                                                    min={1}
                                                    max={100}
                                                    style={{ width: "6.5em" }}
                                                    // value={dimension.height}
                                                    defaultValue={JSON.stringify(Status0Percent)}
                                                    // onFocus={handleFocus}
                                                    onChange={(e) => {
                                                        Status0Percent = parseInt(e.target.value);
                                                        console.log('new Status0Percent', Status0Percent);
                                                    }}
                                                // onBlur={e => {}} 
                                                />
                                                <span style={{ marginLeft: "5px" }}>%</span>
                                            </div>
                                            <div className="spec-item">
                                                <label >Daily Item: </label>
                                                <input
                                                    // autoFocus
                                                    type="number"
                                                    name="DailyItemPercent"
                                                    min={1}
                                                    max={100}
                                                    style={{ width: "6.5em" }}
                                                    // value={dimension.height}
                                                    defaultValue={JSON.stringify(Status1Percent)}
                                                    // onFocus={handleFocus}
                                                    onChange={(e) => {
                                                        Status1Percent = parseInt(e.target.value);
                                                        console.log('new Status1Percent', Status1Percent);
                                                    }}
                                                // onBlur={e => {}} 
                                                />
                                                <span style={{ marginLeft: "5px" }}>%</span>
                                            </div>
                                            <div className="spec-item">
                                                <label >Weekly Item: </label>
                                                <input
                                                    // autoFocus
                                                    type="number"
                                                    name="WeeklyItemPercent"
                                                    min={1}
                                                    max={100}
                                                    style={{ width: "6.5em" }}
                                                    // value={dimension.height}
                                                    defaultValue={JSON.stringify(Status2Percent)}
                                                    // onFocus={handleFocus}
                                                    onChange={(e) => {
                                                        Status2Percent = parseInt(e.target.value);
                                                        console.log('new Status2Percent', Status2Percent);
                                                    }}
                                                // onBlur={e => {}} 
                                                />
                                                <span style={{ marginLeft: "5px" }}>%</span>
                                            </div>
                                            <div className="spec-item">
                                                <label >Short Item: </label>
                                                <input
                                                    // autoFocus
                                                    type="number"
                                                    name="ShortItemPercent"
                                                    min={1}
                                                    max={100}
                                                    style={{ width: "6.5em" }}
                                                    // value={dimension.height}
                                                    defaultValue={JSON.stringify(Status3Percent)}
                                                    // onFocus={handleFocus}
                                                    onChange={(e) => {
                                                        Status3Percent = parseInt(e.target.value);
                                                        console.log('new Status3Percent', Status3Percent);
                                                    }}
                                                // onBlur={e => {}} 
                                                />
                                                <span style={{ marginLeft: "5px" }}>%</span>
                                            </div>
                                            <div className="spec-item">
                                                <label >Internal Item: </label>
                                                <input
                                                    // autoFocus
                                                    type="number"
                                                    name="InternalItemPercent"
                                                    min={1}
                                                    max={100}
                                                    style={{ width: "6.5em" }}
                                                    // value={dimension.height}
                                                    defaultValue={JSON.stringify(Status4Percent)}
                                                    // onFocus={handleFocus}
                                                    onChange={(e) => {
                                                        Status4Percent = parseInt(e.target.value);
                                                        console.log('new Status4Percent', Status4Percent);
                                                    }}
                                                // onBlur={e => {}} 
                                                />
                                                <span style={{ marginLeft: "5px" }}>%</span>
                                            </div>
                                        </div>
                                    </div>
                                    {/*osher 14.9.20 - adding Defaults sizes for section and shelf */}
                                    <div className="container-section">
                                        <h3 className="section-title" style={{ marginBottom: 0 }}>Define Default values for Sections and Shelves</h3>
                                        <div className="spec-row" style={{ display: "flex", justifyContent: "flex-start", marginTop: "20px" }} >
                                            <div className="default-values">
                                                <div className="default-input-value">
                                                    <label>Section Default Height: </label>
                                                    <input
                                                        type="number"
                                                        name="secHeight"
                                                        min={1}
                                                        max={3000}
                                                        style={{ width: "6.5em" }}
                                                        defaultValue={JSON.stringify(secHeight)}
                                                        onChange={(e) => {
                                                            secHeight = parseInt(e.target.value);
                                                        }}
                                                    />
                                                </div>
                                                <div className="default-input-value">
                                                    <label >Section Default Width: </label>
                                                    <input
                                                        type="number"
                                                        name="secWidth"
                                                        min={1}
                                                        max={3000}
                                                        style={{ width: "6.5em" }}
                                                        defaultValue={JSON.stringify(secWidth)}
                                                        onChange={(e) => {
                                                            secWidth = parseInt(e.target.value);
                                                        }}
                                                    />
                                                </div>
                                                <div className="default-input-value">
                                                    <label >Section Default Depth: </label>
                                                    <input
                                                        type="number"
                                                        name="secDepth"
                                                        min={1}
                                                        max={3000}
                                                        style={{ width: "6.5em" }}
                                                        defaultValue={JSON.stringify(secDepth)}
                                                        onChange={(e) => {
                                                            secDepth = parseInt(e.target.value);
                                                        }}
                                                    />
                                                </div>
                                            </div>
                                            <div className="default-values">
                                                <div className="default-input-value">
                                                    <label >Shelf Default Height: </label>
                                                    <input
                                                        type="number"
                                                        name="shelfHeight"
                                                        min={1}
                                                        max={3000}
                                                        style={{ width: "6.5em" }}
                                                        defaultValue={JSON.stringify(shelfHeight)}
                                                        onChange={(e) => {
                                                            shelfHeight = parseInt(e.target.value);
                                                        }}
                                                    />
                                                </div>
                                                <div className="default-input-value">
                                                    <label >Shelf Default Width: </label>
                                                    <input
                                                        type="number"
                                                        name="shelfWidth"
                                                        min={1}
                                                        max={3000}
                                                        style={{ width: "6.5em" }}
                                                        defaultValue={JSON.stringify(shelfWidth)}
                                                        onChange={(e) => {
                                                            shelfWidth = parseInt(e.target.value);
                                                        }}
                                                    />
                                                </div>
                                                <div className="default-input-value">
                                                    <label >Shelf Default Depth: </label>
                                                    <input
                                                        type="number"
                                                        name="shelfDepth"
                                                        min={1}
                                                        max={3000}
                                                        style={{ width: "6.5em" }}
                                                        defaultValue={JSON.stringify(shelfDepth)}
                                                        onChange={(e) => {
                                                            shelfDepth = parseInt(e.target.value);
                                                        }}
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {/***********************************************************************************************/}
                                </div>
                            </div>
                        </TabPanel>
                        <button style={{ backgroundColor: "white", color: "black" }}
                            onClick={async (e) => {
                                if (store) {
                                    // store.statusCalcInv = statusCalcInv;
                                    store.calcInvDays = calcInvDays;
                                    /* osher 14.9.20 */ store.itemTagFlag = this.state.itemTagFlag;
                                    store.Status0Percent = Status0Percent;
                                    store.Status1Percent = Status1Percent;
                                    store.Status2Percent = Status2Percent;
                                    store.Status3Percent = Status3Percent;
                                    store.Status4Percent = Status4Percent;
                                    /* Barak 14.9.2020 - Add default sizes for section and shelf  */
                                    store.Section_height = secHeight;
                                    store.Section_width = secWidth;
                                    store.Section_depth = secDepth;
                                    store.Shelf_height = shelfHeight;
                                    store.Shelf_width = shelfWidth;
                                    store.Shelf_depth = shelfDepth;
                                    /* ********************************************************** */
                                    console.log('new store', store);
                                    let res = await planogramApi.savePlanogramStoreStatusCalc(store);
                                    if (res.status != 'ok') {
                                        uiNotify("Store data failed to save   ", "error", 8000);
                                    } else {
                                        uiNotify("Store data was updated successfully   ", "success", 8000);
                                    }
                                    toggleSettings();
                                }
                            }}
                        >
                            <span>Save</span>
                        </button>
                        <button style={{ backgroundColor: "white", color: "black", marginLeft: "1em" }}
                            onClick={(e) => toggleSettings()}
                        >
                            <span>Cancel</span>
                        </button>
                    </div>
                </Tabs>
            </Rnd >
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PlanogramSettings);