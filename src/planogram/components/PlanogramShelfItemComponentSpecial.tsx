import * as React from "react";
import { PlanogramDragDropTypes } from "../generic/DragAndDropType";

import { DropTarget, DragElementWrapper, DragSource } from 'react-dnd';
import { DragDropResultBase } from "../generic/DragDropWrapper";
import { PlanogramItem, PlacementObject, DimensionObject, PlanogramShelf, PlanogramElementId } from "@src/planogram/shared/store/planogram/planogram.types";
import { connect } from "react-redux";
import { heightDensity, widthDensity } from "@src/planogram/provider/calculation.service";
import { productPredictedSales, getStatusCalM } from "@src/planogram/shared/api/planogram.provider";
import { DimensionModal } from "../modals/DimensionModal";

import * as planogramApi from '@src/planogram/shared/api/planogram.provider';
import { AppState } from "@src/planogram/shared/store/app.reducer";
import { editProductDimensions, setWeeklySale } from "@src/planogram/shared/store/catalog/catalog.action";
import { catalogProductDimensionObject } from "@src/planogram/provider/planogram.service";
import { Dispatch } from "redux";
import { setProductDetailer, hideProductDetailer, setColorBy } from "@src/planogram/shared/store/planogram/planogram.actions";
import { CatalogBarcode } from "@src/planogram/shared/interfaces/models/CatalogProduct";
import { ThunkDispatch } from "redux-thunk";
import config from "@src/planogram/shared/config";

import noImage from "assets/images/no-image.jpg"
import { editShelfItemAction } from "@src/planogram/shared/store/planogram/store/item/item.actions";
import { uiNotify } from "@src/planogram/shared/components/Toast";
import { contextMenu, Menu } from "../generic/ContextMenu";
import { blankPlanogramAisle, ItemMaxPlacement, ProductDefaultDimensions } from "@src/planogram/shared/store/planogram/planogram.defaults";
import { barcodeImageSrc } from "../generic/BarcodeImage";

interface SourceCollectProps {
    connectDragSource: DragElementWrapper<any>,
    isDragging: boolean,
}

interface TargetCollectProps {
    connectDropTarget: DragElementWrapper<any>,
    canDrop: boolean,
}

interface PlanogramItemComponentSpecialProps {
    netw: string,
    userPhone: string,
    item: PlanogramItem,
    shelf: PlanogramShelf,
    aisleId: number,
    multiSelectId: string[],
    updateMultiSelectId: (multi: string[]) => void,
    deleteItem: (shelfId: string, itemId: string) => void,
    dragImg: any,
    shelfHeight: number | null,
    editItem?: () => void,
    onEndDrag?: (item: DragDropResultBase, targetItem: DragDropResultBase) => void,
    onDrop?: (item: DragDropResultBase) => void,
    move?: (item: DragDropResultBase, targetItem: DragDropResultBase) => void,
    remove?: (item: DragDropResultBase) => void
}

const DraggableSource = DragSource<PlanogramItemComponentSpecialProps, SourceCollectProps & SourceCollectProps>(
    PlanogramDragDropTypes.SHELF_ITEM, {
    beginDrag(props, monitor) {
        const { item } = props;
        return ({
            type: PlanogramDragDropTypes.SHELF_ITEM,
            payload: item,
        });
    },
    endDrag(props, monitor) {
        if (!monitor.didDrop())
            return;
        if (props.onEndDrag)
            props.onEndDrag(monitor.getItem(), monitor.getDropResult());
    },
}, (connect, monitor) => ({
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging(),
}));

const dropTypes = [PlanogramDragDropTypes.SHELF_ITEM]

const DroppableTarget = DropTarget<PlanogramItemComponentSpecialProps, TargetCollectProps>(
    dropTypes, {
    drop(props, monitor) {
        const { item } = props;
        if (props.onDrop)
            props.onDrop(monitor.getItem());
        return ({
            type: dropTypes,
            payload: item,
        });
    }
}, (connect, monitor) => ({
    connectDropTarget: connect.dropTarget(),
    canDrop: monitor.canDrop() && monitor.isOver({ shallow: true }),
}));

type ItemProps = PlanogramItemComponentSpecialProps & SourceCollectProps & TargetCollectProps;
// type ItemState = {} & PlanogramItem;


type DispatchProps = ReturnType<typeof mapDispatchToProps>;
type StateProps = ReturnType<typeof mapStateToProps>;

function mapDispatchToProps(dispatch: Dispatch) {
    return {
        editShelfItemPlacement: (item: PlanogramElementId, placement: PlacementObject) => {
            dispatch(editShelfItemAction(item, placement));
        },
        updateProductDimensions: (barcode: number, dimensions: DimensionObject) => {
            dispatch(editProductDimensions(barcode, dimensions));
        },
        setProductDetailerDisplay: (barcode: CatalogBarcode, lowSales: boolean, shelfFaces: number, missing: number, netw: string, userPhone: string) => {
            dispatch(setProductDetailer(barcode, lowSales, shelfFaces, missing, netw, userPhone));
        },
        hideProductDisplayerBarcode: () => {
            dispatch(hideProductDetailer());
        },
    }
}
function mapStateToProps(state: AppState, ownProps: ItemProps) {
    const catalogProduct = state.newPlano.productsMap[ownProps.item.product];
    return {
        ...ownProps,
        catalogProduct,
        productDetail: state.planogram.productDetails[ownProps.item.product],
        hoverProduct: state.planogram.display.productDetailer.barcode,
        hideShelfItems: state.displayOptions?.hideProducts,
        showRowItems: state.displayOptions?.showProdutDepth,
        markBadProducts: state.displayOptions?.productWithoutSize,
        markArchiveProducts: state.planogram.display.markArchiveProducts,

        supplierColor: catalogProduct ?
            state.planogram.virtualStore.colorMap[catalogProduct.SapakId || "none"] || state.planogram.virtualStore.colorMap["none"] : undefined,
        showColorSupplier: state.planogram.display.colorBy === "supplier",
        multiSelectId: state.planogram.display.multiSelectId,
        currentSelectedId: state.planogram.display.currentSelectedId,
        colorBy: state.planogram.display.colorBy,
        showColorClass: state.planogram.display.colorBy === "class",
        classColor: catalogProduct && ownProps.aisleId ?
            state.planogram.virtualStore.aisleMap[ownProps.aisleId].classColorMap[catalogProduct.ClassesId || "none"] || state.planogram.virtualStore.classColorMap["none"] : undefined,
        showColorGroup: state.planogram.display.colorBy === "group",
        groupColor: catalogProduct && ownProps.aisleId ?
            state.planogram.virtualStore.aisleMap[ownProps.aisleId].groupColorMap[catalogProduct.GroupId || "none"] || state.planogram.virtualStore.groupColorMap["none"] : undefined,
        showColorSubGroup: state.planogram.display.colorBy === "subgroup",
        subGroupColor: catalogProduct && ownProps.aisleId ?
            state.planogram.virtualStore.aisleMap[ownProps.aisleId].subGroupColorMap[catalogProduct.SubGroupId || "none"] || state.planogram.virtualStore.subGroupColorMap["none"] : undefined,
        showColorSegment: state.planogram.display.colorBy === "segment",
        segmentColor: catalogProduct && ownProps.aisleId ?
            state.planogram.virtualStore.aisleMap[ownProps.aisleId].segmentColorMap[catalogProduct.SegmentId || "none"] || state.planogram.virtualStore.segmentColorMap["none"] : undefined,
        showColorModel: state.planogram.display.colorBy === "model",
        modelColor: catalogProduct && ownProps.aisleId ?
            state.planogram.virtualStore.aisleMap[ownProps.aisleId].modelColorMap[catalogProduct.SegmentId || "none"] || state.planogram.virtualStore.modelColorMap["none"] : undefined,
        store: state.planogram.store,
        shelvesDetails: state.planogram.virtualStore.shelfDetails,
        aisleShelves: state.planogram.virtualStore.shelfMap,
        alignToLeft: state.planogram.display.alignToLeft,
        virtualStore: state.planogram.virtualStore,
        aisleSaveArray: state.planogram.display.aisleSaveArray,
        aisleSaveIndex: state.planogram.display.aisleSaveIndex,
        hideSaleTags: state.displayOptions?.hideTags,
    }
}

const StoreConnector = connect(mapStateToProps, mapDispatchToProps);
const pixelSideStack = 3;
const pixelTopStack = 3;

function rangeArray(num: number) {
    const list = [];
    for (let i = 1; i <= num; i++)
        list.push(i);
    return list;
}

class PlanogramShelfItemComponentSpecial extends React.Component<DispatchProps & StateProps> {
    keyboardTimeout?: NodeJS.Timeout;
    keyboardCollector = "";

    state = {
        dimensionMenu: false,
        dragImg: new Image(),
        clicked: false,
    }

    handleKeyboard = async (e: KeyboardEvent) => {
        if (this.state.dimensionMenu) return;
        let value = parseInt(e.key);
        if (value >= 0 && value <= 9) {
            if (this.keyboardTimeout)
                clearTimeout(this.keyboardTimeout);
            this.keyboardCollector += value;
            this.keyboardTimeout = setTimeout(() => {
                const { item, catalogProduct } = this.props;
                let newFaces = parseInt(this.keyboardCollector);
                if (newFaces >= 1 && newFaces <= 20) {
                    this.props.editShelfItemPlacement(item.id, {
                        ...item.placement,
                        faces: newFaces
                    });
                    alert(`Successfully updated faces for product: ${(catalogProduct && catalogProduct.Name) || item.product}`)
                }
                this.keyboardCollector = "";
            }, 500)
        }
        /*  Add Delete on Del when this.props.multiSelectId contains id's */
        if (e.keyCode == 46 || e.key === "Delete") {
            // the Delete key was pressed
            if (this.props.multiSelectId.length >= 1) {
                // delete each item saved in multiSelectId from this shelf
                for (let i = 0; i < this.props.multiSelectId.length; i++) {
                    console.log('deleting item', this.props.multiSelectId[i], 'from shelf', this.props.shelf.id);
                    await this.props.deleteItem(this.props.shelf.id, this.props.multiSelectId[i]);
                }
                // empty the multiSelectId array
                this.props.updateMultiSelectId([]);
            }
        }
    }

    bindKeyboard = () => {
        window.addEventListener('keydown', this.handleKeyboard);
    }
    unBindKeyboard = () => {
        window.removeEventListener('keydown', this.handleKeyboard);
    }

    modifyDragImage = (dataTransfer: any) => {
        var elem = document.createElement("div");
        elem.id = "drag-ghost";
        elem.style.backgroundColor = "yellow";
        elem.style.width = "200px";
        elem.style.height = "200px";
        elem.style.zIndex = "9999";
        document.body.appendChild(elem);
        var crt = document.createElement("div");
        crt.style.backgroundColor = "red";
        crt.style.position = "absolute"; crt.style.top = "0px"; crt.style.right = "0px";
        document.body.appendChild(crt);
    }

    render() {
        let { placement, product, id } = this.props.item;
        let { lowSales, missing } = this.props.item;
        const { shelf } = this.props;
        const {
            hideShelfItems,
            catalogProduct,
            hoverProduct,
            updateProductDimensions,
            editShelfItemPlacement,
            setProductDetailerDisplay,
            showRowItems,
            markBadProducts,
            markArchiveProducts,
            showColorSupplier,
            supplierColor,
            netw,
            userPhone,
            colorBy,
            showColorClass,
            classColor,
            showColorGroup,
            groupColor,
            showColorSubGroup,
            subGroupColor,
            showColorSegment,
            segmentColor,
            showColorModel,
            modelColor,
        } = this.props;
        if (hideShelfItems) return null;
        const productDimensions = catalogProductDimensionObject(catalogProduct);
        const { height: productHeight, width: productWidth } = productDimensions;

        const { connectDragSource, connectDropTarget, isDragging, canDrop } = this.props;
        const {
            stack,
            faces,
            row,
            manual_row_only,
            position,
            pWidth,
            pHeight,
            pDepth,
            distFromStart,
            distFromTop
        } = placement;

        let productHasDimensions = catalogProduct ? (catalogProduct.width && catalogProduct.height && catalogProduct.length) : true;
        let productIsArchive = '';
        if (lowSales) productIsArchive = 'rgba(160, 140, 1, 0.5)'; // dirty-brown - דל מכר
        if (catalogProduct) {
            if (catalogProduct.Bdate_buy && catalogProduct.Bdate_buy != '') productIsArchive = 'rgba(255,153,51,0.5)'; // orange - חסום רכש
            if (catalogProduct.Bdate_sale && catalogProduct.Bdate_sale != '') productIsArchive = 'rgba(203, 188, 58, 0.5)'; // dirty-yellow - חסום מכירה
            if (catalogProduct.Archives === 1) productIsArchive = 'rgba(255,255,0,0.5)'; // yellow - ארכיון
        }

        // let isProductMouseOver: NodeJS.Timeout | null;

        if(catalogProduct && catalogProduct.BarCode===7290005423253) console.log('7290005423253',catalogProduct);

        /*  Add currentSelectedId for movment with arrows */
        // if selected item is identocal to this item
        if (this.props.currentSelectedId === id && !this.state.clicked) {
            let x = document.getElementById(id);
            // find the element on screen and simulate a mouse click on it
            if (x) x.click();
        }
        if ((this.props.currentSelectedId != id || this.props.currentSelectedId === '') && this.state.clicked) this.setState({ clicked: false });
        /**************************************************/

        let distFromStart_disp = this.props.item.placement.distFromStart ? this.props.item.placement.distFromStart : 0;
        let distFromTop_disp = this.props.item.placement.distFromTop != null ? this.props.item.placement.distFromTop : null;
        if (distFromTop_disp === null && shelf.freezer === 1) distFromTop_disp = 0; // regular freezer - item will be on top
        if (distFromTop_disp === null && shelf.freezer === 2) // temp freezer - item will be on bottom
            distFromTop_disp = shelf.dimensions.height - ((this.props.item && this.props.item.placement && this.props.item.placement.pHeight ? this.props.item.placement.pHeight : ProductDefaultDimensions.height) * this.props.item.placement.stack);

        let my_div = document.getElementById("sh_id_" + shelf.id);
        let box: any;
        try {
            if (my_div) box = my_div.getBoundingClientRect();
        } catch (e) { }

        if(this.props.item && this.props.item.id.substring(0,this.props.item.id.indexOf('I'))==='A1425SE0SH2') console.log('PlanogramShelfItemComponentSpecial item',this.props.item);

        return connectDropTarget(connectDragSource(
            <div
                className={"planogram-shelf-item" + (canDrop ? " droppable" : "") + (hoverProduct === product ? " active" : "")}
                id={id}
                onDragStart={(e) => {
                    this.modifyDragImage(e.dataTransfer);
                }}
                style={{
                    left: widthDensity(distFromStart_disp),
                    bottom: distFromTop_disp != null ? heightDensity(shelf.dimensions.height - (distFromTop_disp + ((pHeight ? pHeight : 0) * stack))) : '',
                    opacity: isDragging ? 0.5 : 1,
                }}
                onClick={(e) => {
                    if (!e.ctrlKey) {
                        console.log('its here');
                        e.stopPropagation();
                        setProductDetailerDisplay(product, lowSales, this.props.item.placement.faces, missing, netw, userPhone);
                        this.props.updateMultiSelectId([]);
                        this.setState({ clicked: true });
                    }
                }}
                onMouseOver={(e) => {
                    this.bindKeyboard();
                }}
                onMouseOut={(e) => {
                    this.unBindKeyboard();
                }}
                onDoubleClick={e => {
                    e.stopPropagation();
                    contextMenu.show({
                        id: "pde_" + id + "_" + catalogProduct.BarCode,
                        event: e
                    });
                }}>
                {hoverProduct === product ? <div className="shelf-item-cover" style={{ backgroundColor: "#3a404d", opacity: 0.5 }}></div> : null}

                {!productHasDimensions && markBadProducts ? <div className="shelf-item-cover" style={{
                    backgroundColor: "rgba(255,0,0,0.8)" // no dimentions - red color
                }}></div> :
                    markArchiveProducts && productIsArchive != '' ? <div className="shelf-item-cover" style={{ backgroundColor: productIsArchive }}></div>
                        :
                        (showColorSupplier && supplierColor ? <div className="shelf-item-cover" style={{
                            backgroundColor: supplierColor,
                            opacity: 0.8
                        }}></div> : (showColorClass && classColor ? <div className="shelf-item-cover" style={{
                            backgroundColor: classColor,
                            opacity: 0.8
                        }}></div> : (showColorGroup && groupColor ? <div className="shelf-item-cover" style={{
                            backgroundColor: groupColor,
                            opacity: 0.8
                        }}></div> : (showColorSubGroup && subGroupColor ? <div className="shelf-item-cover" style={{
                            backgroundColor: subGroupColor,
                            opacity: 0.8
                        }}></div> : (showColorSegment && segmentColor ? <div className="shelf-item-cover" style={{
                            backgroundColor: segmentColor,
                            opacity: 0.8
                        }}></div> : (showColorModel && modelColor ? <div className="shelf-item-cover" style={{
                            backgroundColor: modelColor,
                            opacity: 0.8
                        }}></div> : null))))))
                }
                <Menu
                    onHidden={() => this.setState({ dimensionMenu: false })}
                    onShown={() => this.setState({ dimensionMenu: true })}
                    id={"pde_" + id + "_" + catalogProduct.BarCode}
                    className="planogram-context-window" >
                    <DimensionModal
                        init={productDimensions}
                        title={"Product Dimensions " + catalogProduct.Name}
                        subtitle={"Barcode " + catalogProduct.BarCode}
                        onSubmit={(dimensions) => {
                            planogramApi.updateProductDimensions(catalogProduct.BarCode, dimensions).then((res) => {
                                updateProductDimensions(
                                    catalogProduct.BarCode,
                                    dimensions);
                                // if not manual_row_only we re-calculate the row    
                                if (!manual_row_only)
                                    editShelfItemPlacement(id, {
                                        faces,
                                        stack,
                                        row: Math.floor(shelf.dimensions.depth / dimensions.depth),
                                        manual_row_only,
                                        position,
                                        pWidth,
                                        pHeight,
                                        pDepth,
                                        distFromStart,
                                        distFromTop

                                    })
                                // if manual_row_only we bring row as is from shelf_item    
                                else
                                    editShelfItemPlacement(id, {
                                        faces,
                                        stack,
                                        row,
                                        manual_row_only,
                                        position,
                                        pWidth,
                                        pHeight,
                                        pDepth,
                                        distFromStart,
                                        distFromTop
                                    })
                                uiNotify("Edited product successfully.", "success");
                                contextMenu.hideAll();
                            }).catch((err) => {
                                console.error(err);
                                uiNotify("Unable to edit product dimensions.", "error");
                                contextMenu.hideAll();
                            });
                        }} />
                </Menu>
                {rangeArray(faces <= ItemMaxPlacement.faces ? faces : ItemMaxPlacement.faces).map((s, i) => (
                    <div
                        className="shelf-item-stack"
                        key={id + "_" + i}>
                        {rangeArray(stack <= ItemMaxPlacement.stack ? stack : ItemMaxPlacement.stack).map((v, _i) => (
                            <div
                                className="shelf-item-product"
                                style={{
                                    width: widthDensity(this.props.item.placement.pWidth && (this.props.item.placement.pWidth > 0) ? this.props.item.placement.pWidth : productWidth),
                                    height: heightDensity(this.props.item.placement.pHeight && (this.props.item.placement.pHeight > 0) ? this.props.item.placement.pHeight : productHeight),
                                }}
                                onClick={(e) => {
                                    console.log('ctrl key is pressed', e.ctrlKey);
                                    if (e.ctrlKey) {
                                        let multi: any[] = this.props.multiSelectId;
                                        // add the id to multiSelectId if it's not there already
                                        if (this.props.multiSelectId.findIndex(lineId => lineId === id) < 0)
                                            multi.push(id);
                                        this.props.updateMultiSelectId(multi);
                                    }
                                    console.log('current multiSelectId', this.props.multiSelectId);
                                }}
                                key={id + "_" + i + "_" + _i}>
                                {this.props.multiSelectId.findIndex(lineId => lineId === id) >= 0
                                    ? <div style={{ backgroundColor: "#3a404d", opacity: 0.5, position: 'absolute', top: '0', height: '100%', width: '100%', zIndex: 500 }}></div>
                                    : null}
                                {showRowItems ? rangeArray(row <= ItemMaxPlacement.row ? row : ItemMaxPlacement.row).map((r, __i) => {
                                    return <img
                                        style={{
                                            zIndex: 300 - (__i),
                                            left: (__i * pixelSideStack) + "px",
                                            bottom: (__i * pixelTopStack) + "px",
                                            width: widthDensity(this.props.item.placement.pWidth && (this.props.item.placement.pWidth > 0) ? this.props.item.placement.pWidth : 0),
                                            height: heightDensity(this.props.item.placement.pHeight && (this.props.item.placement.pHeight > 0) ? this.props.item.placement.pHeight : 0),
                                        }}
                                        alt=""
                                        onError={e => {
                                            e.currentTarget.src = noImage;
                                        }}
                                        src={catalogProduct ? barcodeImageSrc((catalogProduct.client_image ? catalogProduct.client_image : catalogProduct.BarCode), this.props.item.placement.position, netw) : ''} 
                                        key={id + "_" + i + "_" + _i + "_" + __i}
                                    />
                                }) : (<img
                                    style={{
                                        zIndex: 300 - (0),
                                        left: (0 * pixelSideStack) + "px",
                                        bottom: (0 * pixelTopStack) + "px",
                                        width: widthDensity(this.props.item.placement.pWidth && (this.props.item.placement.pWidth > 0) ? this.props.item.placement.pWidth : 0),
                                        height: heightDensity(this.props.item.placement.pHeight && (this.props.item.placement.pHeight > 0) ? this.props.item.placement.pHeight : 0),
                                    }}
                                    alt=""
                                    onError={e => {
                                        e.currentTarget.src = noImage;
                                    }}
                                    src={catalogProduct ? barcodeImageSrc((catalogProduct.client_image ? catalogProduct.client_image : catalogProduct.BarCode), this.props.item.placement.position, netw) : ''} 
                                    key={id + "_" + i + "_" + _i + "_" + 0}
                                />)}
                            </div>
                        ))}
                    </div>
                ))}
            </div>
        ));
    }
}

type ActiveProductTagComponentProps = { barcode: number }
const activeProductTagMapStateToProps = (state: AppState, ownProps: ActiveProductTagComponentProps) => ({
    ...ownProps,
    branch_id: state.planogram.store ? state.planogram.store.branch_id : "",
    weeklySale: state.catalog.productSales[ownProps.barcode] ? state.catalog.productSales[ownProps.barcode].weekly : undefined,
    statusClaMlay: state.catalog.productSales[ownProps.barcode] ? state.catalog.productSales[ownProps.barcode].statusClaMlay : undefined,
    amount: state.planogram.productDetails[ownProps.barcode] ? state.planogram.productDetails[ownProps.barcode].maxAmount : 0,
    statusClacInv: state.planogram.store ? state.planogram.store.statusCalcInv : 0,
    calcInvDays: state.planogram.store ? state.planogram.store.calcInvDays : 0,
    Status0Percent: state.planogram.store ? state.planogram.store.Status0Percent : 100,
    Status1Percent: state.planogram.store ? state.planogram.store.Status1Percent : 100,
    Status2Percent: state.planogram.store ? state.planogram.store.Status2Percent : 100,
    Status3Percent: state.planogram.store ? state.planogram.store.Status3Percent : 60,
    Status4Percent: state.planogram.store ? state.planogram.store.Status4Percent : 100,
})

const activeProductTagMapDispatchToProps = (dispatch: ThunkDispatch<AppState, any, any>) => ({
    setWeeklySale: (barcode: number, weeklySale: number | null, SetStatusCalMlay: number | null, hourly: number | null, createDate:Date) => dispatch(setWeeklySale(barcode, weeklySale, SetStatusCalMlay, hourly, createDate))
})

class ActiveProductTagComponent extends React.Component<ReturnType<typeof activeProductTagMapStateToProps> & ReturnType<typeof activeProductTagMapDispatchToProps>> {
    isActive = false;

    componentDidMount = async () => {

        if (this.props.weeklySale === undefined) {
            this.isActive = true;
            productPredictedSales(this.props.barcode, this.props.branch_id
                // Add StatusClacInv and calcInvDays to function call
                , this.props.statusClacInv, this.props.calcInvDays
            ).then((saleItem) => {
                if (this.isActive && saleItem != null) {
                    this.props.setWeeklySale(this.props.barcode, saleItem.WeeklyAverage, saleItem.statusCalMlay, saleItem.HourlyAverage, saleItem.Create_Date);
                }
            }).catch((err) => {
                console.error(err);
            });
        }
    }
    componentWillUnmount() {
        this.isActive = false;
    }
    render() {
        const { amount, weeklySale, statusClaMlay } = this.props;
        /* Barak 9.1.2020 - Remove old coloring rule *
        let status = false;
        if (weeklySale != null && Math.round(weeklySale) >= amount)
            status = true;
        return (
            <div className={"shelf-item-tag " + (status ? "good" : "bad")}>
                {(weeklySale != null ? Math.round(weeklySale) : "~")}
                /
            {amount}
            </div>
        )
        ****************************************/
        /* Barak 9.1.2020 - Change coloring rule */
        // if (this.props.barcode === 72961223) console.log('coloring rule', this.props.barcode, statusClaMlay);
        let colorTag: string = 'good';
        let sale: number = 0;
        if (weeklySale != null) {
            sale = Math.round(weeklySale);
        }
        /* Barak 13.5.2020 - Add statusCalMlay check to barcode */
        let result = 0;
        /* Barak 23.6.2020 - remove * 
            if (statusClaMlay === 3) result = (sale * 0.6) / amount; 
            else result = sale / amount;
        */
        /* Barak 23.6.2020 - define sale by item type as defined by the items' statusClaMlay */
        let { Status0Percent, Status1Percent, Status2Percent, Status3Percent, Status4Percent } = this.props;
        let percent = 1;
        if (statusClaMlay === 0) percent = Status0Percent / 100;
        if (statusClaMlay === 1) percent = Status1Percent / 100;
        if (statusClaMlay === 2) percent = Status2Percent / 100;
        if (statusClaMlay === 3) percent = Status3Percent / 100;
        if (statusClaMlay === 4) percent = Status4Percent / 100;
        result = (sale * percent) / amount;
        // if (this.props.barcode === 5900020029669)
        //     console.log('item', this.props.barcode, 'statusClaMlay', statusClaMlay, 'sale', sale, 'amount', amount, 'result', result);
        /****************************************/
        if (result < 0.8) colorTag = 'good'; // Green tag
        if (result >= 0.8 && result <= 1.2) colorTag = 'middleColor'; // Yellow tag 
        if (result > 1.2) colorTag = 'bad'; // Red tag

        return (
            <div className={"shelf-item-tag " + (colorTag)}>
                {(weeklySale != null ? Math.round(weeklySale) : "~")}
                /
                {amount}
            </div>
        )
        /****************************************/
    }
}
const ActiveProductTag = connect(activeProductTagMapStateToProps, activeProductTagMapDispatchToProps)(ActiveProductTagComponent);
export default DraggableSource(DroppableTarget(StoreConnector(PlanogramShelfItemComponentSpecial)));
