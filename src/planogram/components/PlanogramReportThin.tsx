import * as React from "react"
import { connect } from 'react-redux'
import { DateBox, SelectBox, Button, TagBox, DataGrid } from 'devextreme-react'
import { Column, Texts, Sorting, Scrolling, HeaderFilter, FilterRow, Export, Summary, TotalItem, GroupPanel, ColumnChooser, GroupItem, Grouping } from 'devextreme-react/data-grid'
import DataSource from 'devextreme/data/data_source'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { AnyAction, bindActionCreators } from 'redux'
import { ThunkDispatch } from 'redux-thunk'
import { Rnd } from 'react-rnd';

import { AppState } from '@src/planogram/shared/store/app.reducer'
import { fetchCatalogSales, CatalogSaleRecord, fetchTruma, fetchTrumaSnif, setUserReportState, getUserReportState } from '@src/planogram/shared/api/sales.provider';
import { SidebarProductDragable } from './SidebarProductDragable';
import { faFilter, faWindowClose } from '@fortawesome/free-solid-svg-icons';
import { uiNotify } from '@src/planogram/shared/components/Toast';
import EditableBarcodeStatus from '../SaleReport/EditableBarcodeStatus'
import { fetchBarcodeStatuses } from '@src/planogram/shared/api/catalog.provider'
import { setBarcodeStatuses } from '@src/planogram/shared/store/catalog/catalog.action'
import { hideSalesReport } from '@src/planogram/shared/store/planogram/display/display.actions'

import * as moment from 'moment';

type ReportSaleRecord = CatalogSaleRecord & {
    InStore: boolean,
    Name: string
    PercentOfSubGroupSaleInBranch: number,
    PercentOfOfSubGroupSaleInNetwork: number,
};

const mapStateToProps = (state: AppState, ownProps: any) => ({
    user: state.auth.user,
    products: state.catalog.products,
    productsMap: state.newPlano ? state.newPlano.productsMap : null,
    displayAisle: state.planogram.display.aisleIndex != null
        && state.planogram.store
        && state.planogram.store.aisles[state.planogram.display.aisleIndex] ? state.planogram.store.aisles[state.planogram.display.aisleIndex].aisle_id : null,
    displaySalesReport: state.planogram.display.displaySalesReport,
    virtualProductDetails: state.planogram.productDetails,
    planogram: state.planogram,
    suppliers: state.system.data.suppliers,
    classes: state.system.data.classes,
    groups: state.system.data.groups,
    subGroups: state.system.data.subGroups,
    branches: state.system.data.branches,
    series: state.system.data.series,
    segments: state.system.data.segments,
    models: state.system.data.models,
    userReportTemplate: state.system.data.userReportTemplates ? state.system.data.userReportTemplates.filter(line => line.report === 'PlanogramReportThin') : []
})

const mapDispatchToProps = (dispatch: ThunkDispatch<AppState, {}, AnyAction>) => ({

    fetchBarcodeStatuses: () => fetchBarcodeStatuses()
        .then(statuses => dispatch(setBarcodeStatuses(statuses)))
        .catch((err) => {
            uiNotify("Unable to load network barcode statuses")
        }),
    hideSalesReport: () => dispatch(hideSalesReport())
})

    let Const_Truma_Records: any[] = [];

type PlanogramReportThinProps = ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>
type PlanogramReportThinState = {
    loading: boolean,
    pageIndex: number,
    pageSize: number,
    salesRecords: ReportSaleRecord[],
    searchWord: string,
    report: {
        beginDate: Date,
        endDate: Date,
        sortBy: string | null,
        currentAisleOnly: boolean,
        sortMap: { [key: string]: boolean },
        selectedBarcodes?: number[],
        selectedBranches?: number[],
        selectedSuppliers?: number[],
        selectedGroups?: number[],
        selectedSubGroups?: number[],
        selectedClasses?: number[],
        selectedSeries?: number[],
        selectedSegments?: number[],
        selectedModels?: number[]
    },
    height: number,
    width: number,
    trumaSnifRecords: any[]
};

class PlanogramReportThin extends React.Component<PlanogramReportThinProps, PlanogramReportThinState> {
    dataGrid: any | null = null;
    state: PlanogramReportThinState = {
        loading: false,
        pageIndex: 0,
        pageSize: 50,
        searchWord: "",
        report: {
            currentAisleOnly: false,
            beginDate: new Date(new Date().getFullYear(), 0, 1),
            endDate: new Date(),
            sortBy: null,
            sortMap: {},
        },
        width: window.innerWidth * 0.8,
        height: window.innerHeight * 0.5656,
        salesRecords: [],
        trumaSnifRecords: []
    }
    componentDidMount() {
        this.props.fetchBarcodeStatuses();
        fetchTruma().then((trumaRecords) => {
            Const_Truma_Records = trumaRecords;
        }).catch((err) => {
            console.error(err);
            uiNotify("Unable to load truma table", "error");
        })
    }

    xlsOutput = (e: any) => {
        const { salesRecords } = this.state;
        const {
            currentAisleOnly,
            selectedGroups,
            selectedSubGroups,
            selectedSeries,
            selectedSuppliers,
            selectedClasses,
            selectedSegments,
           selectedModels,
        } = this.state.report;
        const { productsMap, virtualProductDetails } = this.props;

        let filteredRecords: any[] = salesRecords;

        const url = window.location.href;
        const urlElements = url.split('/');
        let currentAisle = parseInt(urlElements[8]);

        if (currentAisleOnly && currentAisle != null) {
            filteredRecords = filteredRecords.filter(v =>
                virtualProductDetails[v.BarCode]
                && virtualProductDetails[v.BarCode].position
                && virtualProductDetails[v.BarCode].position.findIndex(p => p.aisle_id === currentAisle) !== -1);
        }

        if (selectedSuppliers != null && selectedSuppliers.length > 0)
            filteredRecords = filteredRecords.filter(v => v.SupplierId == null || selectedSuppliers.includes(v.SupplierId));
        if (selectedClasses != null && selectedClasses.length > 0)
            filteredRecords = filteredRecords.filter(v => v.ClassId == null || selectedClasses.includes(v.ClassId));
        if (selectedGroups != null && selectedGroups.length > 0)
            filteredRecords = filteredRecords.filter(v => productsMap[v.BarCode] && productsMap[v.BarCode].GroupId && selectedGroups.includes(productsMap[v.BarCode].GroupId || -1));
        if (selectedSubGroups != null && selectedSubGroups.length > 0)
            filteredRecords = filteredRecords.filter(v => productsMap[v.BarCode] && productsMap[v.BarCode].SubGroupId && selectedSubGroups.includes(productsMap[v.BarCode].SubGroupId || -1));
        if (selectedSeries != null && selectedSeries.length > 0)
            filteredRecords = filteredRecords.filter(v => productsMap[v.BarCode] && productsMap[v.BarCode].DegemId && selectedSeries.includes(productsMap[v.BarCode].DegemId || -1));
        /* Barak 18.8.20 */
        if (selectedSegments != null && selectedSegments.length > 0)
            filteredRecords = filteredRecords.filter(v => productsMap[v.BarCode] && productsMap[v.BarCode].SegmentId && selectedSegments.includes(productsMap[v.BarCode].SegmentId || -1));
        /*****************/
        /* Barak 6.12.20 */
        if (selectedModels != null && selectedModels.length > 0)
            filteredRecords = filteredRecords.filter(v => productsMap[v.BarCode] && productsMap[v.BarCode].ModelId && selectedModels.includes(productsMap[v.BarCode].ModelId || -1));
        /*****************/

        // now that I have all the information I'm building a new JSON for the excel output
        let length = filteredRecords.length;
        let reportJSON = [];
        for (let i = 0; i < length; i++) {
            let branch = this.props.branches ?  this.props.branches.filter(branch => branch.BranchId === filteredRecords[i].BranchId)[0] : null;
            let planogramDetail = this.props.planogram.productDetails[filteredRecords[i].BarCode];
            let object = {
                Branch_Id: filteredRecords[i].BranchId,
                Branch_Name: branch ? branch.Name.replace('"', "''") : null,
                Gondola: planogramDetail && planogramDetail.position ? planogramDetail.position.map(p => p.aisle_id).filter((p, i, list) => list.indexOf(p) === i).join() : null,
                Shelf_Content: planogramDetail ? planogramDetail.maxAmount : null,
                Item_BarCode: filteredRecords[i].BarCode,
                Item_Name: filteredRecords[i].Name.replace('"', "''"),
                Supplier_Id: filteredRecords[i].SapakId,
                Supplier_Name: filteredRecords[i].SupplierName.replace('"', "''"),
                Department_Id: filteredRecords[i].ClassesId,
                Department_Name: filteredRecords[i].ClassName.replace('"', "''"),
                Group_Id: filteredRecords[i].GroupId,
                Group_Name: filteredRecords[i].GroupName.replace('"', "''"),
                SubGroup_Id: filteredRecords[i].SubGroupId,
                SubGroup_Name: filteredRecords[i].SubGroupName.replace('"', "''"),
                Model_Id: filteredRecords[i].ModelId,
                Model_Name: filteredRecords[i].ModelName.replace('"', "''"),
                Segment_Id: filteredRecords[i].SegmentId,
                Segment_Name: filteredRecords[i].SegmentName.replace('"', "''"),
                Degem_Id: filteredRecords[i].DegemId,
                Degem_Name: filteredRecords[i].DegemName.replace('"', "''"),
                Layout_Id: filteredRecords[i].LayoutId,
                Layout_Name: filteredRecords[i].LayoutName.replace('"', "''"),
                TotalAmount: filteredRecords[i].TotalAmount,
                TotalAmountPercent: filteredRecords[i].TotalAmountPercent,
                TotalPrice: filteredRecords[i].TotalPrice,
                TotalPricePercent: filteredRecords[i].TotalPricePercent,
                AvgResultPercent: filteredRecords[i].AvgResultPercent,
                AvgSalesPrice: filteredRecords[i].AvgSalesPrice,
                AvgSalesPercent: filteredRecords[i].AvgSalesPercent,
                SalePrice: filteredRecords[i].SalePrice,
                SalePriceNoVAT: filteredRecords[i].SalePriceNoVAT,
                BuyPrice: filteredRecords[i].BuyPrice,
                Profit: Math.round(filteredRecords[i].Profit * Math.pow(10, 2)) / Math.pow(10, 2),
                ProfitPercent: Math.round(filteredRecords[i].ProfitPercent * Math.pow(10, 2)) / Math.pow(10, 2),
                MinStock: filteredRecords[i].MinStock,
                Create_Date: moment(filteredRecords[i].CrDate).format('DD/MM/YYYY'),
                Exit_Date: moment(filteredRecords[i].ExDate).format('DD/MM/YYYY'),
                InStore: filteredRecords[i].InStore
            }
            reportJSON.push(object);
        }


        // https://www.npmjs.com/package/json2csv
        const { parse } = require('json2csv');
        // let fields = ['Branch_Id', 'Branch_Name', 'Gondola', 'Shelf_Content', 'Item_BarCode', 'Item_Name',
        //     'Supplier_Id', 'Supplier_Name', 'Department_Id', 'Department_Name', 'Group_Id', 'Group_Name', 'SubGroup_Id', 'SubGroup_Name',
        //     'Model_Id', 'Model_Name', 'Segment_Id', 'Segment_Name', 'Degem_Id', 'Degem_Name', 'TotalAmount', 'TotalPrice', 'SalePriceNoVAT', 'BuyPrice', 'Profit',
        //     'ProfitPercent', 'MinStock', 'Create_Date', 'Exit_Date', 'InStore'];
        let fields = ['Branch_Id', 'Branch_Name'];
        let columnNum = this.dataGrid.columnCount();
        console.log('columnCount', columnNum);
        for (let i = 0; i < columnNum; i++) {
            console.log('columnOption', i, this.dataGrid.columnOption(i, 'dataField'), this.dataGrid.columnOption(i, 'visible'));
            let fieldData = this.dataGrid.columnOption(i, 'dataField');
            let visible = this.dataGrid.columnOption(i, 'visible');
            if (fieldData === 'InStore' && visible) {
                fields.push('InStore');
                fields.push('Gondola');
                fields.push('Shelf_Content');
            }
            if (fieldData === 'BarCode' && visible) fields.push('Item_BarCode');
            if (fieldData === 'Name' && visible) fields.push('Item_Name');
            if (fieldData === 'SupplierName' && visible) {
                fields.push('Supplier_Id');
                fields.push('Supplier_Name');
            }
            if (fieldData === 'ClassName' && visible) {
                fields.push('Department_Id');
                fields.push('Department_Name');
            }
            if (fieldData === 'GroupName' && visible) {
                fields.push('Group_Id');
                fields.push('Group_Name');
            }
            if (fieldData === 'SubGroupName' && visible) {
                fields.push('SubGroup_Id');
                fields.push('SubGroup_Name');
            }
            if (fieldData === 'ModelName' && visible) {
                fields.push('Model_Id');
                fields.push('Model_Name');
            }
            if (fieldData === 'SegmentName' && visible) {
                fields.push('Segment_Id');
                fields.push('Segment_Name');
            }
            if (fieldData === 'DegemName' && visible) {
                fields.push('Degem_Id');
                fields.push('Degem_Name');
            }
            if (fieldData === 'LayoutName' && visible) {
                fields.push('Layout_Id');
                fields.push('Layout_Name');
            }
            if (fieldData === 'TotalPrice' && visible) {
                fields.push('TotalPrice');
                fields.push('TotalPricePercent');
                if (fields.findIndex(line => line === 'AvgResultPercent') < 0) fields.push('AvgResultPercent');
            }
            if (fieldData === 'TotalAmount' && visible) {
                fields.push('TotalAmount');
                fields.push('TotalAmountPercent');
                if (fields.findIndex(line => line === 'AvgResultPercent') < 0) fields.push('AvgResultPercent');
            }
            if (fieldData === 'AvgSalesPrice' && visible) fields.push('AvgSalesPrice');
            if (fieldData === 'AvgSalesPercent' && visible) fields.push('AvgSalesPercent');
            if (fieldData === 'Profit' && visible) fields.push('Profit');
            if (fieldData === 'ProfitPercent' && visible) fields.push('ProfitPercent');
            if (fieldData === 'SalePrice' && visible) {
                fields.push('SalePriceNoVAT');
                fields.push('BuyPrice');
            }
            if (fieldData === 'MinStock' && visible) fields.push('MinStock');
            // if(fieldData==='PercentOfSubGroupSaleInBranch' && visible) fields.push('?');
            // if(fieldData==='PercentOfOfSubGroupSaleInNetwork' && visible) fields.push('?');
            if (fieldData === 'CrDate' && visible) fields.push('Create_Date');
            if (fieldData === 'ExDate' && visible) fields.push('Exit_Date');
        }

        const withBOM = true;
        const excelStrings = true;
        const opts = { fields, excelStrings, withBOM };
        try {
            const fileData = parse(reportJSON, opts);
            const blob = new Blob([fileData], { type: "text/plain" });
            const url = URL.createObjectURL(blob);
            const link = document.createElement('a');
            // link.download = 'filename.json';
            link.download = "Sales_Report.csv";
            link.href = url;
            link.click();
        } catch (err) {
            console.error(err);
            uiNotify(err, 'error');
        }
    }

    /* Barak 23.1.20 * get length of filteredRecords */
    countTotalLines = (options: any) => {
        // console.log(options.name,options.summaryProcess);
        const { salesRecords } = this.state;
        const {
            beginDate,      // date effects this.state.salesRecords directly in function fetchCatalogSales
            endDate,        // date effects this.state.salesRecords directly in function fetchCatalogSales
            currentAisleOnly,
            selectedGroups,
            selectedSubGroups,
            selectedSeries,
            selectedSuppliers,
            selectedClasses,
            /*Barak 18.8.20 */ selectedSegments,
            /*Barak 6.12.20 */ selectedModels
        } = this.state.report;
        const { productsMap, virtualProductDetails } = this.props;

        if (options.name === 'SelectedRowsSummary') {
            if (options.summaryProcess === 'start') {
                options.totalValue = 0;
            } else if (options.summaryProcess === 'calculate') {
                let filteredRecords = salesRecords;

                const url = window.location.href;
                const urlElements = url.split('/');
                // const currentAisle = parseInt(urlElements[6]);
                let currentAisle = parseInt(urlElements[8]);

                if (currentAisleOnly && currentAisle != null) {
                    filteredRecords = filteredRecords.filter(v =>
                        virtualProductDetails[v.BarCode]
                        && virtualProductDetails[v.BarCode].position
                        && virtualProductDetails[v.BarCode].position.findIndex(p => p.aisle_id === currentAisle) !== -1);
                }

                if (selectedSuppliers != null && selectedSuppliers.length > 0)
                    filteredRecords = filteredRecords.filter(v => v.SupplierId == null || selectedSuppliers.includes(v.SupplierId));
                if (selectedClasses != null && selectedClasses.length > 0)
                    filteredRecords = filteredRecords.filter(v => v.ClassId == null || selectedClasses.includes(v.ClassId));
                if (selectedGroups != null && selectedGroups.length > 0)
                    filteredRecords = filteredRecords.filter(v => productsMap[v.BarCode] && productsMap[v.BarCode].GroupId && selectedGroups.includes(productsMap[v.BarCode].GroupId || -1));
                if (selectedSubGroups != null && selectedSubGroups.length > 0)
                    filteredRecords = filteredRecords.filter(v => productsMap[v.BarCode] && productsMap[v.BarCode].SubGroupId && selectedSubGroups.includes(productsMap[v.BarCode].SubGroupId || -1));
                if (selectedSeries != null && selectedSeries.length > 0)
                    filteredRecords = filteredRecords.filter(v => productsMap[v.BarCode] && productsMap[v.BarCode].DegemId && selectedSeries.includes(productsMap[v.BarCode].DegemId || -1));
                /* Barak 18.8.20 */
                if (selectedSegments != null && selectedSegments.length > 0)
                    filteredRecords = filteredRecords.filter(v => productsMap[v.BarCode] && productsMap[v.BarCode].SegmentId && selectedSegments.includes(productsMap[v.BarCode].SegmentId || -1));
                /*****************/
                /* Barak 6.12.20 */
                if (selectedModels != null && selectedModels.length > 0)
                    filteredRecords = filteredRecords.filter(v => productsMap[v.BarCode] && productsMap[v.BarCode].ModelId && selectedModels.includes(productsMap[v.BarCode].ModelId || -1));
                /*****************/

                // after all the filtering we can finally return the total length of filteredRecords  
                options.totalValue = filteredRecords.length;
            }
        }
    }

    onSearch = async () => {
        const { beginDate, endDate, selectedBranches, selectedSuppliers, selectedGroups, selectedClasses,
            selectedSubGroups, selectedSegments, selectedModels } = this.state.report;
        const { productsMap, virtualProductDetails, products } = this.props;
        console.log('onSearch', selectedBranches, selectedSuppliers, selectedGroups, selectedClasses,
            selectedSubGroups, selectedSegments, selectedModels);
        if (productsMap == null || products.length === 0)
            return console.log("No products in search");

        this.setState({
            loading: true
        });
        /*Barak 23.1.20 get trumaSnifRecords records*/
        fetchTrumaSnif(selectedBranches).then((trumaRecords) => {
            this.setState({ trumaSnifRecords: trumaRecords });
            // console.log('trumaSnifRecords', trumaRecords);
        }).catch((err) => {
            console.error(err);
            uiNotify("Unable to load sales", "error");
            this.setState({
                loading: false
            })
        })
        let init: any[] = await fetchCatalogSales({
            beginDate,
            endDate,
            branch: selectedBranches,
            supplier: selectedSuppliers,
            group: selectedGroups,
            subGroup: selectedSubGroups,
            class: selectedClasses,
            /* Barak 18.8.20 */ segment: selectedSegments,
            /* Barak 6.12.20 */ model: selectedModels
        });
        for (let i = 0; i < init.length; i++) {
            const snifSale = this.state.trumaSnifRecords ? this.state.trumaSnifRecords.filter(line => line.BarCode === init[i].BarCode)[0] : null;
            const networkSale = Const_Truma_Records.filter(line => line.BarCode === init[i].BarCode)[0];

            init[i].InStore = virtualProductDetails[init[i].BarCode] != null;
            init[i].PercentOfSubGroupSaleInBranch = snifSale ? parseFloat(Number((snifSale.Amount / snifSale.Tot_subgroup) * 100).toFixed(2)) : 0;
            init[i].PercentOfOfSubGroupSaleInNetwork = networkSale ? parseFloat(Number((networkSale.Amount / networkSale.Tot_subgroup) * 100).toFixed(2)) : 0;
        }
        console.log('fetchCatalogSales init', init);
        this.setState({ loading: false, salesRecords: init });
    }

    onCellPrepared = (e: any) => {
        if (e.rowType === "group" && e.summaryItems.length === 1) {
            // e.cellElement.find('div').css('color', 'white'); // doesn't work
            // e.cellElement.css({ "background-color": "red" }); // doesn't work

            // e.cellElement.style.backgroundColor = "lightGray"; // works
            // e.cellElement.style.color = "black"; // works
            // console.log('rowType==group',e);
        }
    }

    render() {
        if (!this.props.displaySalesReport || this.props.products.length === 0)
            return null;
        const { salesRecords } = this.state;
        const { beginDate,
            endDate,
            currentAisleOnly,
            selectedBranches,
            selectedGroups,
            selectedSubGroups,
            selectedSeries,
            /*Barak 13.1.20 */ selectedSuppliers,
            /*Barak 13.1.20 */ selectedClasses,
            /*Barak 18.8.20 */ selectedSegments,
            /*Barak 6.12.20 */ selectedModels,
        } = this.state.report;
        const { productsMap, virtualProductDetails, displayAisle } = this.props;
        let { branches, classes, groups, subGroups, suppliers, series, segments } = this.props;

        // const { pageIndex, pageSize } = this.state;

        let filteredRecords = salesRecords;

        /* Barak 13.1.20 - get asile from url*/
        const url = window.location.href;
        const urlElements = url.split('/');
        // const currentAisle = parseInt(urlElements[6]);
        let currentAisle = parseInt(urlElements[8]);

        /* Barak 13.1.20 - create new currentAisleOnly filter using currentAisle instead of displayAisle*/
        if (currentAisleOnly && currentAisle != null) {
            filteredRecords = filteredRecords.filter(v =>
                virtualProductDetails[v.BarCode]
                && virtualProductDetails[v.BarCode].position
                && virtualProductDetails[v.BarCode].position.findIndex(p => p.aisle_id === currentAisle) !== -1);
        }

        if (selectedSuppliers != null && selectedSuppliers.length > 0)
            filteredRecords = filteredRecords.filter(v => v.SupplierId == null || selectedSuppliers.includes(v.SupplierId));
        if (selectedClasses != null && selectedClasses.length > 0)
            filteredRecords = filteredRecords.filter(v => v.ClassId == null || selectedClasses.includes(v.ClassId));

        if (selectedGroups != null && selectedGroups.length > 0)
            filteredRecords = filteredRecords.filter(v => productsMap[v.BarCode] && productsMap[v.BarCode].GroupId && selectedGroups.includes(productsMap[v.BarCode].GroupId || -1));
        if (selectedSubGroups != null && selectedSubGroups.length > 0)
            filteredRecords = filteredRecords.filter(v => productsMap[v.BarCode] && productsMap[v.BarCode].SubGroupId && selectedSubGroups.includes(productsMap[v.BarCode].SubGroupId || -1));
        if (selectedSeries != null && selectedSeries.length > 0)
            filteredRecords = filteredRecords.filter(v => productsMap[v.BarCode] && productsMap[v.BarCode].DegemId && selectedSeries.includes(productsMap[v.BarCode].DegemId || -1));
        /* Barak 18.8.20 */
        if (selectedSegments != null && selectedSegments.length > 0)
            filteredRecords = filteredRecords.filter(v => productsMap[v.BarCode] && productsMap[v.BarCode].SegmentId && selectedSegments.includes(productsMap[v.BarCode].SegmentId || -1));
        /*****************/
        /* Barak 6.12.20 */
        if (selectedModels != null && selectedModels.length > 0)
            filteredRecords = filteredRecords.filter(v => productsMap[v.BarCode] && productsMap[v.BarCode].ModelId && selectedModels.includes(productsMap[v.BarCode].ModelId || -1));
        /*****************/

        const salesToSupplier: number[] = []
        const salesToClass: number[] = []

        for (let i = 0; i < salesRecords.length; i++) {
            const record = salesRecords[i];
            if (record.SupplierId != null) {
                salesToSupplier.push(record.SupplierId);
            }
            if (record.ClassId != null) {
                salesToClass.push(record.ClassId);
            }
        }
        const filteredSuppliers = suppliers.filter(s => salesToSupplier.includes(s.Id))
        const filteredClasses = classes.filter(c => salesToClass.includes(c.Id))

        const { height: floatHeight, width: floatWidth } = this.state;

        return (
            <Rnd
                dragHandleClassName="planogram-report-handle"
                cancel="planogram-report-container"
                default={{
                    x: floatWidth / 2,
                    y: floatHeight / 2,
                    width: floatWidth,
                    height: floatHeight,
                }}
                className="float-window planogram-report"
                resizeHandleClasses={{
                    bottomRight: "planogram-report-resize-handle bottom-right",
                    // bottomLeft: "planogram-report-resize-handle bottom-left",
                }}>
                <div className="float-window-handle planogram-report-handle">
                    <div className="handle-content">Sale Report</div>
                    <div className="float-window-close" onClick={this.props.hideSalesReport}>
                        <FontAwesomeIcon icon={faWindowClose} />
                    </div>
                </div>
                <div className="float-window-container planogram-report-container">
                    {this.state.loading ?
                        <div className="loader"></div>
                        :
                        <React.Fragment>
                            <div className="report-toolbar">
                                <div className="report-toolbar-container">
                                    {/* Barak 13.1.20 - xlsOutput button */}
                                    <Button
                                        rtlEnabled
                                        onClick={(e) => {
                                            // console.log('click', e);
                                            this.xlsOutput(e);
                                        }}
                                        className="toolbar-item-small"
                                        icon="exportxlsx" />
                                    <DateBox
                                        rtlEnabled
                                        className="toolbar-item"
                                        placeholder="תאריך תחילה"
                                        value={beginDate}
                                        displayFormat="dd/MM/yy"
                                        pickerType={"calendar"}
                                        onValueChanged={(e) => {
                                            // console.log(e.value);
                                            this.setState({
                                                report: {
                                                    ...this.state.report,
                                                    beginDate: e.value
                                                }
                                            })
                                        }} />
                                    <DateBox
                                        rtlEnabled
                                        className="toolbar-item"
                                        placeholder="תאריך סיום"
                                        value={endDate}
                                        displayFormat="dd/MM/yy"
                                        pickerType={"calendar"}
                                        onValueChanged={(e) => {
                                            // console.log(e.value);
                                            this.setState({
                                                report: {
                                                    ...this.state.report,
                                                    endDate: e.value
                                                }
                                            })
                                        }} />
                                    <TagBox // SelectBox
                                        rtlEnabled
                                        showClearButton
                                        className="toolbar-item"
                                        placeholder="סדרה"
                                        value={selectedSeries}
                                        valueExpr="Id"
                                        displayExpr="Name"
                                        showSelectionControls={true}
                                        searchEnabled={true}
                                        searchExpr={["Name", "Id"]}
                                        dataSource={new DataSource({
                                            store: series,
                                            pageSize: 50
                                        })}
                                        onValueChanged={(e) => {
                                            // console.log(e.value);
                                            this.setState({
                                                report: {
                                                    ...this.state.report,
                                                    selectedSeries: e.value
                                                }
                                            })
                                        }} />
                                    <TagBox // SelectBox
                                        rtlEnabled
                                        showClearButton
                                        className="toolbar-item"
                                        placeholder="סניף"
                                        value={selectedBranches}
                                        valueExpr="BranchId"
                                        displayExpr="Name"
                                        showSelectionControls={true}
                                        searchEnabled={true}
                                        items={branches}
                                        onValueChanged={(e) => {
                                            // console.log(e.value);
                                            this.setState({
                                                report: {
                                                    ...this.state.report,
                                                    selectedBranches: e.value
                                                }
                                            })
                                        }} />
                                    <Button
                                        rtlEnabled
                                        // disabled={selectedBranches == null || selectedBranches.length === 0}
                                        onClick={this.onSearch}
                                        className="toolbar-item"
                                        icon="search"
                                        text="חפש" />
                                </div>
                                <div className="report-toolbar-container">
                                    {/* Barak 18.8.20 - Adding segment */}
                                    <TagBox // SelectBox
                                        rtlEnabled
                                        showClearButton
                                        className="toolbar-item"
                                        placeholder="סגמנט"
                                        value={selectedSegments}
                                        valueExpr="Id"
                                        displayExpr="Name"
                                        showSelectionControls={true}
                                        applyValueMode="useButtons"
                                        searchEnabled={true}
                                        searchExpr={["Name", "Id"]}
                                        dataSource={new DataSource({
                                            store: segments,
                                            pageSize: 50
                                        })}
                                        onValueChanged={(e) => {
                                            // console.log(e.value);
                                            this.setState({
                                                report: {
                                                    ...this.state.report,
                                                    selectedSegments: e.value
                                                }
                                            })
                                        }} />
                                    {/*********************************/}
                                    <TagBox // SelectBox
                                        rtlEnabled
                                        showClearButton
                                        className="toolbar-item"
                                        placeholder="תת קבוצה"
                                        value={selectedSubGroups}
                                        valueExpr="Id"
                                        displayExpr="Name"
                                        showSelectionControls={true}
                                        applyValueMode="useButtons"
                                        searchEnabled={true}
                                        searchExpr={["Name", "Id"]}
                                        dataSource={new DataSource({
                                            store: subGroups,
                                            pageSize: 50
                                        })}
                                        onValueChanged={(e) => {
                                            // console.log(e.value);
                                            this.setState({
                                                report: {
                                                    ...this.state.report,
                                                    selectedSubGroups: e.value
                                                }
                                            })
                                        }} />
                                    <TagBox // SelectBox
                                        rtlEnabled
                                        showClearButton
                                        className="toolbar-item"
                                        placeholder="קבוצה"
                                        value={selectedGroups}
                                        valueExpr="Id"
                                        displayExpr="Name"
                                        showSelectionControls={true}
                                        applyValueMode="useButtons"
                                        searchEnabled={true}
                                        searchExpr={["Name", "Id"]}
                                        dataSource={new DataSource({
                                            store: groups,
                                            pageSize: 50
                                        })}
                                        onValueChanged={(e) => {
                                            // console.log(e.value);
                                            this.setState({
                                                report: {
                                                    ...this.state.report,
                                                    selectedGroups: e.value
                                                }
                                            })
                                        }} />
                                    {/* Barak 13.1.20 - un-comment selectedSuppliers, selectedClasses */}
                                    <TagBox // SelectBox
                                        rtlEnabled
                                        searchEnabled
                                        showClearButton
                                        className="toolbar-item"
                                        placeholder="מחלקה"
                                        value={selectedClasses}
                                        valueExpr="Id"
                                        displayExpr="Name"
                                        showSelectionControls={true}
                                        applyValueMode="useButtons"
                                        searchExpr={["Name", "Id"]}
                                        dataSource={new DataSource({
                                            store: classes,
                                            pageSize: 50
                                        })}
                                        onValueChanged={(e) => {
                                            // console.log(e.value);
                                            this.setState({
                                                report: {
                                                    ...this.state.report,
                                                    selectedClasses: e.value
                                                }
                                            })
                                        }} />
                                    <TagBox // SelectBox
                                        rtlEnabled
                                        showClearButton
                                        className="toolbar-item"
                                        placeholder="ספק"
                                        value={selectedSuppliers}
                                        valueExpr="Id"
                                        displayExpr="Name"
                                        showSelectionControls={true}
                                        applyValueMode="useButtons"
                                        searchEnabled={true}
                                        searchExpr={["Name", "Id"]}
                                        dataSource={new DataSource({
                                            store: suppliers,
                                            pageSize: 50
                                        })}
                                        onValueChanged={(e) => {
                                            // console.log(e.value);
                                            this.setState({
                                                report: {
                                                    ...this.state.report,
                                                    selectedSuppliers: e.value
                                                }
                                            })
                                        }} />
                                    <Button
                                        rtlEnabled
                                        className="toolbar-item"
                                        disabled={displayAisle == null}
                                        onClick={(e) => {
                                            this.setState({
                                                report: {
                                                    ...this.state.report,
                                                    currentAisleOnly: currentAisleOnly ? false : true
                                                }
                                            })
                                        }}>
                                        <FontAwesomeIcon icon={faFilter} style={{ display: currentAisleOnly ? "" : "none" }} />
                                        <span>גונדולה נוכחית</span>
                                    </Button>
                                </div>
                            </div>
                            <div className="report-content">
                                <DataGrid
                                    onInitialized={(ref) => { this.dataGrid = ref.component }}
                                    rtlEnabled
                                    showRowLines
                                    showColumnHeaders
                                    allowColumnResizing
                                    // allowColumnReordering
                                    className="report-group-table"
                                    noDataText="אין נתונים..."
                                    dataSource={new DataSource({
                                        store: filteredRecords,
                                    })}
                                    // allow to save the state of the dataGrid including the columnChooser in localStorage / database table planogram_pattern
                                    // based on https://supportcenter.devexpress.com/ticket/details/t550068/datagrid-is-it-possible-to-store-the-selection-of-the-columnchooser
                                    stateStoring={{
                                        enabled: true,
                                        type: "custom",
                                        customLoad: async () => {
                                            // return JSON.parse(getCookie("key") || '{}');

                                            // let localVersion = localStorage.getItem('PlanogramReportThinColumnChooser');
                                            // if (localVersion) {
                                            //     let rec1 = JSON.parse(localVersion);
                                            //     if (this.props.user && rec1.user === this.props.user.id)
                                            //         return rec1.state;
                                            //     else return {};
                                            // }
                                            // else return {};

                                            // if(this.props.userReportTemplate && this.props.userReportTemplate[0]) return JSON.parse(this.props.userReportTemplate[0].state);
                                            // else return {};

                                            let state = await getUserReportState('PlanogramReportThin', this.props.user ? this.props.user.id : 0);
                                            if (state && state != '{}') {
                                                // remove any filterValue from the retrieved columns
                                                let data = JSON.parse(state);
                                                for(let a=0;a<data.columns.length;a++){
                                                    delete data.columns[a].filterValue;
                                                    // console.log('a',a,'filterValue',data.columns[a].filterValue);
                                                }
                                                console.log('getUserReportState PlanogramReportThin', data);
                                                // return JSON.parse(state);
                                                return data;
                                            }
                                            else return {};
                                        },
                                        customSave: (state) => {
                                            // setCookie("key", JSON.stringify(state), 1);

                                            // let rec1 = {
                                            //     state: state,
                                            //     user: this.props.user ? this.props.user.id : 0
                                            // }
                                            // localStorage.setItem('PlanogramReportThinColumnChooser', JSON.stringify(rec1));

                                            setUserReportState('PlanogramReportThin', this.props.user ? this.props.user.id : 0, JSON.stringify(state));
                                        },
                                    }}
                                    onCellPrepared={(e) => this.onCellPrepared(e)}
                                >
                                    <Texts
                                    />
                                    <FilterRow visible />
                                    <HeaderFilter visible />
                                    <Sorting mode="multiple" />
                                    {/* <Sorting /> */}
                                    {/* Barak 1.9.20 - add option to group by column */}
                                    {/* GroupPanel - https://js.devexpress.com/Demos/WidgetsGallery/Demo/DataGrid/RecordGrouping/React/Light/ */}
                                    <GroupPanel visible={true} emptyPanelText={'יש לגרור כותרת טור לכאן על מנת לקבץ לפי טור זה'} />
                                    <Grouping autoExpandAll={false} />
                                    {/************************************************/}
                                    {/* ColumnChooser - https://js.devexpress.com/Demos/WidgetsGallery/Demo/TreeList/ColumnChooser/React/Light/ */}
                                    <ColumnChooser enabled={true} allowSearch={true} mode={'select'} />
                                    <Scrolling mode={'virtual'} />
                                    <Column
                                        dataField="InStore"
                                        dataType="boolean"
                                        caption="מיקום"
                                        defaultSortOrder="desc"
                                        cellRender={({ data }) => {
                                            return <ReportPositionCell record={data} />;
                                        }}
                                    />
                                    <Column
                                        dataField="BarCode"
                                        caption="ברקוד"
                                    >
                                        <HeaderFilter
                                            allowSearch
                                            searchMode="Contains"
                                        />
                                    </Column>
                                    <Column
                                        dataField="Name"
                                        dataType="string"
                                        caption="מוצר"
                                    >
                                        <HeaderFilter
                                            allowSearch
                                            searchMode="Contains"
                                        // searchTimeout={1000}
                                        />
                                    </Column>
                                    <Column
                                        dataField="SupplierName"
                                        caption="ספק"
                                    >
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    <Column
                                        allowSearch
                                        dataField="ClassName"
                                        caption="מחלקה"
                                    >
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    <Column
                                        allowSearch
                                        dataField="GroupName"
                                        caption="קבוצה"
                                    >
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    <Column
                                        allowSearch
                                        dataField="SubGroupName"
                                        caption="תת קבוצה"
                                    >
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    <Column
                                        allowSearch
                                        dataField="ModelName"
                                        caption="מותג"
                                    >
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    <Column
                                        allowSearch
                                        dataField="SegmentName"
                                        caption="סגמנט"
                                    >
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    <Column
                                        allowSearch
                                        dataField="DegemName"
                                        caption="סדרה"
                                    >
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    <Column
                                        allowSearch
                                        dataField="LayoutName"
                                        caption="מערך"
                                    >
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    <Column
                                        allowSearch
                                        dataField="TotalPrice"
                                        // caption="מכירות כספיות"
                                        caption="כספי"
                                        defaultSortOrder="desc"
                                        format="###,###.##">
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    {/* Barak 3.1.21 */}
                                    <Column
                                        allowSearch
                                        dataField="TotalPricePercent"
                                        // caption="מכירות כספיות"
                                        caption="% כספי"
                                        defaultSortOrder="desc"
                                        format="##0.##">
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    {/****************/}
                                    <Column
                                        allowSearch
                                        dataField="TotalAmount"
                                        // caption="מכירות כמותיות"
                                        caption="כמות"
                                        defaultSortOrder="desc"
                                        format="###,###">
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    {/* Barak 3.1.21 */}
                                    <Column
                                        allowSearch
                                        dataField="TotalAmountPercent"
                                        caption="% כמות"
                                        defaultSortOrder="desc"
                                        format="##0.##">
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    <Column
                                        allowSearch
                                        dataField="AvgResultPercent"
                                        caption="% משוקלל"
                                        defaultSortOrder="desc"
                                        format="##0.##">
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    <Column
                                        allowSearch
                                        dataField="AvgSalesPercent"
                                        caption="% מ.שוק"
                                        defaultSortOrder="desc"
                                        format="##0.##" >
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    <Column
                                        allowSearch
                                        dataField="AvgSalesPrice"
                                        caption="מחיר שוק"
                                        defaultSortOrder="desc"
                                        format="###,###.##" >
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    {/****************/}
                                    <Column
                                        allowSearch
                                        dataField="Profit"
                                        caption="רווח"
                                        format="###,###">
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    <Column
                                        allowSearch
                                        dataField="ProfitPercent"
                                        caption="% רווח"
                                        format="##0.##">
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    <Column
                                        allowSearch
                                        dataField="SalePrice"
                                        caption="מחיר מכירה"
                                        format="###,###.##">
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    <Column
                                        allowSearch
                                        dataField="MinStock"
                                        caption="מלאי ברזל"
                                        dataType={"number"}
                                        format="###,###">
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    <Column
                                        allowSearch
                                        dataField="PercentOfSubGroupSaleInBranch"
                                        // caption="% תרומה לסניף"
                                        caption="% ת.סניף"
                                        defaultSortOrder="desc">
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    <Column
                                        allowSearch
                                        dataField="PercentOfOfSubGroupSaleInNetwork"
                                        // caption="% תרומה לרשת"
                                        caption="% ת.רשת"
                                        defaultSortOrder="desc">
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    <Column
                                        allowSearch
                                        dataField="CrDate"
                                        caption="כניסה למגוון"
                                        dataType={"date"}
                                        format={"dd/MM/yyyy"}>
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    <Column
                                        allowSearch
                                        dataField="ExDate"
                                        caption="יציאה ממגוון"
                                        dataType={"date"}
                                        format={"dd/MM/yyyy"}>
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    <Column
                                        caption="תמונה"
                                        cellRender={({ data }) => data.BarCode ?
                                            <SidebarProductDragable
                                                product={data.BarCode}
                                                className="item-image"
                                            /> : null}
                                    />
                                    {/* <Column
                                        caption="הערה"
                                        cellRender={({ data }) => <EditableBarcodeStatus
                                            barcode={data.BarCode}
                                        />}
                                    /> */}
                                    {/* Barak 23.1.20 * summery to columns: TotalPrice, TotalAmount */}
                                    <Summary calculateCustomSummary={this.countTotalLines}>
                                        <TotalItem
                                            // name={'SelectedRowsSummary'}
                                            // summaryType={'custom'}
                                            summaryType={'count'}
                                            valueFormat={"###,###,##0.##"}
                                            displayFormat={'שורות: {0}'}
                                            showInColumn={'SubGroupName'} />
                                        <TotalItem
                                            column={'TotalPrice'}
                                            summaryType={'sum'}
                                            valueFormat={"###,###,##0.##"}
                                            displayFormat={'{0}'} />
                                        {/* Barak 3.1.21 */}
                                        <TotalItem
                                            column={"TotalPricePercent"}
                                            summaryType={'sum'}
                                            valueFormat="##0.##"
                                            displayFormat={'{0}'} />
                                        {/****************/}
                                        <TotalItem
                                            column={'TotalAmount'}
                                            summaryType={'sum'}
                                            valueFormat={"###,###,##0.##"}
                                            displayFormat={'{0}'} />
                                        {/* Barak 3.1.21 */}
                                        <TotalItem
                                            column={"TotalAmountPercent"}
                                            summaryType={'sum'}
                                            valueFormat="##0.##"
                                            displayFormat={'{0}'} />
                                        <TotalItem
                                            column={"AvgResultPercent"}
                                            summaryType={'sum'}
                                            valueFormat="##0.##"
                                            displayFormat={'{0}'} />
                                        {/* <TotalItem
                                            column={"AvgSalesPercent"}
                                            summaryType={'sum'}
                                            valueFormat="##0.##"
                                            displayFormat={'{0}'} />
                                        <TotalItem
                                            column={"AvgSalesPrice"}
                                            summaryType={'sum'}
                                            valueFormat="###,###.##"
                                            displayFormat={'{0}'}/> */}
                                        {/****************/}

                                        {/* <GroupItem
                                            // just for coloring this cell
                                            summaryType={'custom'}
                                            displayFormat={'{0}'}
                                            customizeText={''}
                                            showInColumn={'InStore'}
                                            alignByColumn={true} />
                                        <GroupItem
                                            // just for coloring this cell
                                            summaryType={'custom'}
                                            displayFormat={'{0}'}
                                            customizeText={''}
                                            showInColumn={'BarCode'}
                                            alignByColumn={true} />
                                        <GroupItem
                                            // just for coloring this cell
                                            summaryType={'custom'}
                                            displayFormat={'{0}'}
                                            customizeText={''}
                                            showInColumn={'Name'}
                                            alignByColumn={true} /> */}
                                        <GroupItem
                                            column={'TotalPrice'}
                                            summaryType={'sum'}
                                            valueFormat={"###,###,##0.##"}
                                            displayFormat={'{0}'}
                                            alignByColumn={true} />
                                        {/* Barak 3.1.21 */}
                                        <GroupItem
                                            column={"TotalPricePercent"}
                                            summaryType={'sum'}
                                            valueFormat="##0.##"
                                            displayFormat={'{0}'}
                                            alignByColumn={true} />
                                        {/****************/}
                                        <GroupItem
                                            column={'TotalAmount'}
                                            summaryType={'sum'}
                                            valueFormat={"###,###,##0.##"}
                                            displayFormat={'{0}'}
                                            alignByColumn={true} />
                                        {/* Barak 3.1.21 */}
                                        <GroupItem
                                            column={"TotalAmountPercent"}
                                            summaryType={'sum'}
                                            valueFormat="##0.##"
                                            displayFormat={'{0}'}
                                            alignByColumn={true} />
                                        <GroupItem
                                            column={"AvgResultPercent"}
                                            summaryType={'sum'}
                                            valueFormat="##0.##"
                                            displayFormat={'{0}'}
                                            alignByColumn={true} />
                                        <GroupItem
                                            column={"AvgSalesPercent"}
                                            summaryType={'sum'}
                                            valueFormat="##0.##"
                                            displayFormat={'{0}'} 
                                            alignByColumn={true} />
                                        {/* <GroupItem
                                            column={"AvgSalesPrice"}
                                            summaryType={'sum'}
                                            valueFormat="###,###.##"
                                            displayFormat={'{0}'}
                                            alignByColumn={true} /> */}
                                        {/****************/}
                                    </Summary>
                                </DataGrid>
                            </div>
                        </React.Fragment>
                    }
                </div>
            </Rnd>
        )
    }
}

const reportPositionCellStateToProps = (state: AppState, ownProps: { record: CatalogSaleRecord }) => {
    const product = state.newPlano ? state.newPlano.productsMap[ownProps.record.BarCode] : null;

    return {
                    ...ownProps,
                    supplier: product && product.SapakId ? state.system.data.suppliersMap[product.SapakId] : undefined,
        class: product && product.ClassesId ? state.system.data.classesMap[product.ClassesId] : undefined,
        product: product,
        planogramDetail: state.planogram.productDetails[ownProps.record.BarCode],
        /* Barak 24.6.2020 * isOver: state.planogram.display.productDetailer === ownProps.record.BarCode */
        /* Barak 24.6.2020 */ isOver: state.planogram.display.productDetailer.barcode === ownProps.record.BarCode
    }
};
const reportTitleCellStateToProps = (state: AppState, ownProps: { record: CatalogSaleRecord }) => {
    return {
                    ...ownProps,
                    product: state.newPlano.productsMap[ownProps.record.BarCode],
    }
};

class ReportPositionCellContainer extends React.Component<ReturnType<typeof reportPositionCellStateToProps>> {
                    render() {
                        console.log('ReportPositionCellContainer this.props', this.props);
        const {product, planogramDetail, isOver} = this.props;

        if (product == null || !planogramDetail || planogramDetail.position.length === 0)
            return null;
        /* Barak 23.1.20 */
        // at this point planogramDetail.position may include many duplicate aisle_ids so we need to filter it to unique aisle_id only
        let aisleIds = planogramDetail.position.map(obj => { return obj.aisle_id; });
        aisleIds = aisleIds.filter((v, i) => { return aisleIds.indexOf(v) == i; });
        // at this point aisleIds includce only the unique values of aisle_id so we can use that to check the length

        return (<div className="detail-section" style={{ background: isOver ? "#dbffdc" : "none" }}>
                        <div className="detail-row">
                            {/* <label>{planogramDetail.position.length > 1 ? "גונדולות" : "גונדולה"}</label> */}
                            <label>{aisleIds.length > 1 ? "גונדולות" : "גונדולה"}</label>
                            <span>{planogramDetail.position.map(p => p.aisle_id).filter((p, i, list) => list.indexOf(p) === i).join()}</span>
                        </div>
                        <div className="detail-row">
                            <label>תכולת מדף</label>
                            <span>{planogramDetail.maxAmount}</span>
                        </div>
                    </div>);
    }
}

const ReportPositionCell = connect(reportPositionCellStateToProps)(ReportPositionCellContainer)

export default connect(mapStateToProps, mapDispatchToProps)(PlanogramReportThin)