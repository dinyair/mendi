import * as React from "react";
import { PlanogramDragDropTypes } from "../generic/DragAndDropType";
import PlanogramShelfItemComponentSpecial from "./PlanogramShelfItemComponentSpecial";
import { DropTarget, DragElementWrapper, DragSource } from 'react-dnd';
import { DragDropResultBase } from "../generic/DragDropWrapper";
import { PlanogramShelf, DimensionObject, PlacementObject, PlanogramElementId } from "@src/planogram/shared/store/planogram/planogram.types";
import { Dispatch } from "redux";
import { connect } from "react-redux";
import { switchItemsAction, addProductAction, deleteItemAction, editShelfItemAction } from "@src/planogram/shared/store/planogram/store/item/item.actions";
import { heightDensity, widthDensity, shelfItemDimensions, shelfAvailableSpace } from "@src/planogram/provider/calculation.service";
import { ItemPlacementModal } from "../modals/ItemPlacementModal";
import { editShelfDimensionsAction } from "@src/planogram/shared/store/planogram/store/shelf/shelf.actions";
import { dimensionText, catalogProductDimensionObject } from "@src/planogram/provider/planogram.service";
import { AppState } from "@src/planogram/shared/store/app.reducer";
import { CatalogBarcode } from "@src/planogram/shared/interfaces/models/CatalogProduct";
import { Menu, contextMenu, MenuProvider } from "../generic/ContextMenu";
import { ProductDefaultDimensions } from "@src/planogram/shared/store/planogram/planogram.defaults";

interface DropProps {
    connectDragSource?: DragElementWrapper<any>,
    isDragging?: boolean,
}

interface TargetProps {
    connectDropTarget?: DragElementWrapper<any>,
    canDrop?: boolean,
}

interface ShelfComponentSpecialProps {
    aisleId?: number,
    shelf: PlanogramShelf,
    shelfIndex: number;
    realDropTarget?: string,
    netw: string,
    userPhone: string,
    pid: string,
    onDrop?: (item: DragDropResultBase) => any,
    onDragEnd?: (item: DragDropResultBase, targetItem: DragDropResultBase) => void,
    move?: (item: DragDropResultBase, targetItem: DragDropResultBase) => void,
    // move?: (currentIndex: number, targetIndex: number) => void,
    verifyDrop?: (droppedItem: DragDropResultBase) => boolean,
}


const DraggableSource = DragSource<ShelfComponentSpecialProps, DropProps>(
    PlanogramDragDropTypes.SHELF, {
    beginDrag(props, monitor) {
        const { shelf } = props;
        return ({
            type: PlanogramDragDropTypes.SHELF,
            payload: shelf,
        });
    },
    endDrag(props, monitor) {
        if (!monitor.didDrop()) {
            return;
        }
        if (props.onDragEnd)
            props.onDragEnd(monitor.getItem(), monitor.getDropResult());
    },
}, (connect, monitor, props) => ({
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging(),
})
);

const dropTypes = [
    PlanogramDragDropTypes.PRODUCT_SIDEBAR,
    PlanogramDragDropTypes.SERIES_SIDEBAR,
    PlanogramDragDropTypes.SHELF_ITEM,
    PlanogramDragDropTypes.SHELF
];
const DroppableTarget = DropTarget<ShelfComponentSpecialProps, TargetProps>(dropTypes, {
    drop(props, monitor) {
        const { shelf } = props;
        if (props.onDrop)
            props.onDrop(monitor.getItem());
        return ({
            type: monitor.getItemType(),
            payload: shelf,
        });
    },
    canDrop(props, monitor) {
        if (monitor.didDrop() || !monitor.isOver({ shallow: true }))
            return false;
        return props.verifyDrop ? props.verifyDrop(monitor.getItem()) : true;
    },
}, (connect, monitor, props) => ({
    connectDropTarget: connect.dropTarget(),
    canDrop: monitor.canDrop(),
    position: monitor.getClientOffset(),
    isOver: monitor.isOver(),
    isOverCurrent: monitor.isOver({ shallow: true }),
    itemType: monitor.getItemType(),
}));

type ShelfProps = ShelfComponentSpecialProps & DropProps & TargetProps;

function mapDispatchToProps(dispatch: Dispatch) {
    return {
        switchItems: (base: PlanogramElementId, remote: PlanogramElementId) => {
            dispatch(switchItemsAction(base, remote));
        },
        addProduct: (shelf: PlanogramElementId, product: CatalogBarcode, placement?: PlacementObject) => {
            dispatch(addProductAction(shelf, product, placement));
        },
        deleteItem: (shelf: PlanogramElementId, item: PlanogramElementId) => {
            dispatch(deleteItemAction(shelf, item))
        },
        editShelfDimensions: (shelf: PlanogramElementId, dimensions: DimensionObject) => {
            dispatch(editShelfDimensionsAction(shelf, dimensions))
        },
        editShelfItemPlacement: (item: PlanogramElementId, placement: PlacementObject) => {
            dispatch(editShelfItemAction(item, placement));
        }
    }
}
function mapStateToProps(state: AppState, ownProps: ShelfProps) {
    return {
        ...state.newPlano,
        ...ownProps,
        shelvesDetails: state.planogram.virtualStore.shelfDetails,
        aisleShelves: state.planogram.virtualStore.shelfMap
    }
}
type PlanogramShelfComponentProps = ReturnType<typeof mapDispatchToProps> & ReturnType<typeof mapStateToProps>;

class PlanogramShelfComponentSpecialContainer extends React.Component<PlanogramShelfComponentProps> {

    state = {
        multiSelectId: [],
        dragImg: new Image()
    }
    componentDidMount() {
        const img = new Image();
        img.src = "https://grid.algoretail.co.il/media/images/products/7290008909860.jpg";
        img.onload = () => this.setState({ dragImg: img });
    }
    /**************************************/

    updateMultiSelectId = (multi: string[]) => {
        this.setState({ multiSelectId: multi });
    }
    /*************************************************/

    render() {
        const { canDrop, isDragging, connectDropTarget, connectDragSource, shelf, netw, userPhone,
            deleteItem,
            switchItems,
            addProduct,
            productsMap,
            shelfIndex: mainShelfIndex,
            aisleId
        } = this.props;
        const { dimensions } = shelf;
       let display = true;

        let combinedShelves: PlanogramShelf[] = [shelf];
        const shelfItemsInit = combinedShelves.map(sh => sh.items).reduce((p, c) => p.concat(c));
        let shelfItems = [];
        for (let i = 0; i < shelfItemsInit.length; i++) {
            // make sure there are only real items on the shelf
            if (shelfItemsInit[i]) shelfItems.push(shelfItemsInit[i]);
        }
        const itemDimensions = shelfItems.map((item) => shelfItemDimensions(item.placement, {
            width: item.placement.pWidth ? item.placement.pWidth : ProductDefaultDimensions.width,
            height: item.placement.pHeight ? item.placement.pHeight : ProductDefaultDimensions.height,
            depth: item.placement.pDepth ? item.placement.pDepth : ProductDefaultDimensions.depth
        }));
        const itemsWidth: number = shelfItems.length > 0 ? itemDimensions.map(d => d.width).reduce((p, c) => p + c) : 0;

        // let cursor = 0;
        let collectedShelfWidth = 0;
        let collectedItemsWidth = 0;
        let collectedMargins = 0;

        let nextMargin = 0;
        const element = (
            <div
                onClick={e => e.stopPropagation()}
                style={{
                    height: dimensions.height ? (shelf.freezer != 1  ? heightDensity(dimensions.height + (dimensions.height * 0.25) ) + "px" : heightDensity(dimensions.height) + "px") : "initial",
                    width: widthDensity(dimensions.width) + "px",
                    outline: shelf.freezer === 1 ? 'solid 20px #2b998a' : '',
                    backgroundColor: shelf.freezer === 2 ? 'lightblue' : '',
                    marginTop: shelf.freezer === 1 ? '100px' : ''
                }}
                className={"planogram-shelf" + (canDrop ? " droppable" : "") + (isDragging ? " dragged" : "")}>
                {display ? <div style={{
                    display: display ? "inherit" : "none",
                }}>
                    {combinedShelves.map((shelf) => shelf.items.map((item) => {
                        if (item) {
                            const product = productsMap ? productsMap[item.product] : null;
                            const productDimensions = catalogProductDimensionObject(product);
                            const originalPlacement = JSON.parse(JSON.stringify(item.placement));
                            return <Menu id={"ed_" + item.id} key={"ed_" + item.id} className="planogram-context-window" >
                                <ItemPlacementModal
                                    init={item.placement}
                                    title={"Product: " + (product ? product.Name : '')}
                                    subtitle={"Barcode " + (product ? product.BarCode : '')}
                                    barcode={product ? product.BarCode : 0}
                                    dimensions={productDimensions}
                                    maxDimensions={{
                                        ...shelf.dimensions,
                                    }}
                                    onSubmit={async (placement) => {
                                        let problem: any = false;
                                        // in case we changed stack we need to recalculate distFromTop 
                                        if (placement.distFromTop && placement.distFromTop > 0) {
                                            placement.distFromTop = placement.distFromTop - ((placement.pHeight ? placement.pHeight : ProductDefaultDimensions.height) * (placement.stack - originalPlacement.stack));
                                            if (placement.distFromTop < 0) placement.distFromTop = 0;
                                        }
                                        if (!problem) await this.props.editShelfItemPlacement(item.id, placement);
                                        // get this item's shelfId
                                        let shelfId = item.id.substring(0, item.id.indexOf('I'));
                                        // display most updated shelf
                                        contextMenu.hideAll();
                                    }} />
                            </Menu>
                        }
                    })
                    )}
                    {combinedShelves.map((shelf) => {
                        const _itemDimensions = shelf.items.map((item) => shelfItemDimensions(item ? item.placement : {
                            faces: 1,
                            stack: 1,
                            row: 1,
                            manual_row_only: 0,
                            position: 0,
                            pWidth: ProductDefaultDimensions.width,
                            pHeight: ProductDefaultDimensions.height,
                            pDepth: ProductDefaultDimensions.depth,
                            distFromStart: 0,
                           distFromTop: null
                        }, {
                            width: item && item.placement.pWidth ? item.placement.pWidth : ProductDefaultDimensions.width,
                            height: item && item.placement.pHeight ? item.placement.pHeight : ProductDefaultDimensions.height,
                            depth: item && item.placement.pDepth ? item.placement.pDepth : ProductDefaultDimensions.depth
                        }));
                        const itemsWidth: number = _itemDimensions.length > 0 ? _itemDimensions.map(d => d.width).reduce((p, c) => p + c) : 0;
                        const shelfWidth = shelf.dimensions.width;

                        const currentMargin = nextMargin;

                        collectedShelfWidth += shelfWidth;
                        collectedItemsWidth += itemsWidth;

                        let a72p = document.getElementById(this.props.pid);

                        if (collectedShelfWidth - collectedItemsWidth - collectedMargins > 0) {
                            nextMargin = collectedShelfWidth - collectedItemsWidth - collectedMargins;
                            collectedMargins += nextMargin;
                        }
                        else
                            nextMargin = 0;
                        return <div
                            className="shelf-container"
                            key={"sh_ed_" + shelf.id}
                            style={{
                                marginLeft: (widthDensity(currentMargin)) + "px",
                            }}>
                            {shelf.items.sort(function (a: any, b: any) {
                                let aId = a.id ? parseInt(a.id.substring(a.id.indexOf('I') + 1)) : 0;
                                let bId = b.id ? parseInt(b.id.substring(b.id.indexOf('I') + 1)) : 0;
                                return (aId > bId) ? 1 : (aId < bId) ? -1 : 0;
                            }).map((item, i) => {
                                if (item) {
                                    let availableSpace = shelfAvailableSpace(shelf, productsMap);
                                    let shelfHeight = shelf.dimensions.height;
                                    let shelfDepth = shelf.dimensions.depth;

                                    return <MenuProvider
                                        id={"ed_" + item.id}
                                        style={{
                                            zIndex: 15 + 100 * parseInt(shelf.id.substring(shelf.id.indexOf('SH') + 2))
                                        }}
                                        key={item.id}>
                                        <PlanogramShelfItemComponentSpecial
                                            aisleId={aisleId ? aisleId : 0}
                                            netw={netw}
                                            userPhone={userPhone}
                                            item={item}
                                            shelf={shelf}
                                            multiSelectId={this.state.multiSelectId}
                                            updateMultiSelectId={this.updateMultiSelectId}
                                            deleteItem={deleteItem}
                                         dragImg={this.state.dragImg}
                                            shelfHeight={dimensions.height}
                                            onEndDrag={async (source, target) => {
                                                if (target.type === PlanogramDragDropTypes.TRASH) {
                                                    console.log("TRASHING", item);
                                                    await deleteItem(shelf.id, item.id);
                                                }
                                            }}
                                            // editItem={() => this.createNewItem(item)}
                                            onDrop={async (dropItem) => {
                                                if (dropItem.type === PlanogramDragDropTypes.SHELF_ITEM) {
                                                    if (item.id === dropItem.payload.id)
                                                        return;
                                                    await switchItems(item.id, dropItem.payload.id);
                                                }
                                            }}
                                        />
                                    </MenuProvider>
                                }
                            }
                            )}
                            {a72p ? a72p.focus() : null}
                        </div>
                    })}
                </div> : null}
                {dimensions ? <div className="shelf-dimensions" style={{
                    zIndex: 50 + 100 * /*   mainShelfIndex */ parseInt(shelf.id.substring(shelf.id.indexOf('SH') + 2)),
                }}>
                    {dimensionText(dimensions)}
                </div> : ""}
                <div className="shelf-structure" style={{
                    // display: display ? "inherit" : "none",
                    zIndex: 10 + 100 * /*  mainShelfIndex */ parseInt(shelf.id.substring(shelf.id.indexOf('SH') + 2)),
                    // width: realWidth ? widthDensity(realWidth) + "px" : "initial",
                }}></div>
            </div>
        )

        return connectDragSource && connectDropTarget ? connectDragSource(connectDropTarget(element)) : element;
    }
}
export const PlanogramShelfComponentSpecial = connect(mapStateToProps, mapDispatchToProps)(PlanogramShelfComponentSpecialContainer);
export const PlanogramShelfSpecialDnd = DraggableSource(DroppableTarget(PlanogramShelfComponentSpecial));
