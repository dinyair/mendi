import * as React from "react"
import { connect } from 'react-redux'
import { DateBox, SelectBox, Button, TagBox, DataGrid } from 'devextreme-react'
import { Column, Texts, Sorting, Scrolling, HeaderFilter, FilterRow, Export, Summary, TotalItem, GroupPanel } from 'devextreme-react/data-grid'
import DataSource from 'devextreme/data/data_source'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { AnyAction, bindActionCreators } from 'redux'
import { ThunkDispatch } from 'redux-thunk'
import { Rnd } from 'react-rnd';

import { AppState } from '@src/planogram/shared/store/app.reducer'
import { fetchCatalogSales, CatalogSaleRecord, fetchTruma, fetchTrumaSnif } from '@src/planogram/shared/api/sales.provider';
import { SidebarProductDragable } from './SidebarProductDragable';
import { faFilter, faWindowClose } from '@fortawesome/free-solid-svg-icons';
import { uiNotify } from '@src/planogram/shared/components/Toast';
import EditableBarcodeStatus from '../SaleReport/EditableBarcodeStatus'
import { fetchBarcodeStatuses } from '@src/planogram/shared/api/catalog.provider'
import { setBarcodeStatuses } from '@src/planogram/shared/store/catalog/catalog.action'
import { hideSalesReport } from '@src/planogram/shared/store/planogram/display/display.actions'

import XLSX from 'xlsx';

// const formatQuantity = "###,###";

type ReportSaleRecord = CatalogSaleRecord & {
    InStore: boolean,
    Name: string
    PercentOfSubGroupSaleInBranch: number,
    PercentOfOfSubGroupSaleInNetwork: number,
};

const mapStateToProps = (state: AppState, ownProps: any) => ({
    products: state.catalog.products,
    productsMap: state.newPlano ? state.newPlano.productsMap : null,
    displayAisle: state.planogram.display.aisleIndex != null
        && state.planogram.store
        && state.planogram.store.aisles[state.planogram.display.aisleIndex] ? state.planogram.store.aisles[state.planogram.display.aisleIndex].aisle_id : null,
    displaySalesReport: state.planogram.display.displaySalesReport,
    virtualProductDetails: state.planogram.productDetails,
    planogram: state.planogram,
    suppliers: state.system.data.suppliers,
    classes: state.system.data.classes,
    groups: state.system.data.groups,
    subGroups: state.system.data.subGroups,
    branches: state.system.data.branches,
    series: state.system.data.series,
    segments: state.system.data.segments,
})

const mapDispatchToProps = (dispatch: ThunkDispatch<AppState, {}, AnyAction>) => ({

    fetchBarcodeStatuses: () => fetchBarcodeStatuses()
        .then(statuses => dispatch(setBarcodeStatuses(statuses)))
        .catch((err) => {
            uiNotify("Unable to load network barcode statuses")
        }),
    hideSalesReport: () => dispatch(hideSalesReport())
})

    let Const_Truma_Records: any[] = [];

type PlanogramReportProps = ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>
type PlanogramReportState = {
    loading: boolean,
    pageIndex: number,
    pageSize: number,
    salesRecords: ReportSaleRecord[],
    searchWord: string,
    report: {
        beginDate: Date,
        endDate: Date,
        sortBy: string | null,
        currentAisleOnly: boolean,
        sortMap: { [key: string]: boolean },
        selectedBarcodes?: number[],
        selectedBranches?: number[],
        selectedSuppliers?: number[],
        selectedGroups?: number[],
        selectedSubGroups?: number[],
        selectedClasses?: number[],
        selectedSeries?: number[],
        selectedSegments?: number[]
    },
    height: number,
    width: number,
    trumaSnifRecords: any[]
};


class PlanogramReport extends React.Component<PlanogramReportProps, PlanogramReportState> {
    state: PlanogramReportState = {
        loading: false,
        pageIndex: 0,
        pageSize: 50,
        searchWord: "",
        report: {
            currentAisleOnly: false,
            beginDate: new Date(new Date().getFullYear(), 0, 1),
            endDate: new Date(),
            sortBy: null,
            sortMap: {},
        },
        width: window.innerWidth * 0.8,
        height: window.innerHeight * 0.5656,
        salesRecords: [],
        trumaSnifRecords: []
    }
    componentDidMount() {
        this.props.fetchBarcodeStatuses();
        fetchTruma().then((trumaRecords) => {
            Const_Truma_Records = trumaRecords;
        }).catch((err) => {
            console.error(err);
            uiNotify("Unable to load truma table", "error");
        })
    }
    onSearch = () => {
        const { beginDate, endDate, selectedBranches, selectedSuppliers, selectedGroups, selectedClasses,
            selectedSubGroups, selectedSegments } = this.state.report;
        const { productsMap, virtualProductDetails, products } = this.props;
        if (productsMap == null || products.length === 0)
            return console.log("No products in search");

        this.setState({
            loading: true
        });
        fetchTrumaSnif(selectedBranches).then((trumaRecords) => {
            this.setState({ trumaSnifRecords: trumaRecords });
        }).catch((err) => {
            console.error(err);
            uiNotify("Unable to load sales", "error");
            this.setState({
                loading: false
            })
        })
        fetchCatalogSales({
            beginDate,
            endDate,
            branch: selectedBranches,
            supplier: selectedSuppliers,
            group: selectedGroups,
            subGroup: selectedSubGroups,
            class: selectedClasses,
            segment: selectedSegments
        }).then((catalogSales) => {
            this.setState({
                loading: false,
                salesRecords: catalogSales.map(v => {

                    const snifSale = this.state.trumaSnifRecords.filter(line => line.BarCode === v.BarCode)[0];
                    const networkSale = Const_Truma_Records.filter(line => line.BarCode === v.BarCode)[0];

                    const newProduct: ReportSaleRecord = {
                        ...v,
                        InStore: virtualProductDetails[v.BarCode] != null,
                        Name: productsMap[v.BarCode] ? productsMap[v.BarCode].Name : '',
                        // Amount - total sold of this BarCode, Tot_subgroup - total sold of the Entire sub group 
                        PercentOfSubGroupSaleInBranch: snifSale ? parseFloat(Number((snifSale.Amount / snifSale.Tot_subgroup) * 100).toFixed(2)) : 0,
                        PercentOfOfSubGroupSaleInNetwork: networkSale ? parseFloat(Number((networkSale.Amount / networkSale.Tot_subgroup) * 100).toFixed(2)) : 0,
                    };

                    if (!productsMap[v.BarCode]) return newProduct;
                    const product = productsMap[v.BarCode];
                    return {
                        ...newProduct,
                        SupplierId: product.SapakId,
                        GroupId: product.GroupId,
                        SubGroupId: product.SubGroupId,
                        ClassId: product.ClassesId,
                        DegemId: product.DegemId,
                        SegmentId: product.SegmentId
                    }
                })
            })
        }).catch((err) => {
            console.error(err);
            uiNotify("Unable to load sales", "error");
            this.setState({
                loading: false
            })
        })
    }

    setSortMapItem = (property: string) => () => {
        let newSortMap = { ...this.state.report.sortMap }
        if (newSortMap[property] === false)
            delete newSortMap[property];
        else if (newSortMap[property] === true)
            newSortMap[property] = false;
        else newSortMap = {
            [property]: true,
            ...newSortMap
        }
        this.setState({
            report: {
                ...this.state.report,
                sortMap: newSortMap
            }
        })
    }

    /* callXlsOutput, xlsOutput */
    xlsOutput = (e: any) => {
        // Export XLSX file with errors:
        // https://github.com/SheetJS/sheetjs/issues/817

        const { salesRecords } = this.state;
        const {
            beginDate,      // date effects this.state.salesRecords directly in function fetchCatalogSales
            endDate,        // date effects this.state.salesRecords directly in function fetchCatalogSales
            currentAisleOnly,
            selectedGroups,
            selectedSubGroups,
            selectedSeries,
            selectedSuppliers,
            selectedClasses,
            selectedSegments
        } = this.state.report;
        const { productsMap, virtualProductDetails } = this.props;

        let filteredRecords = salesRecords;

        const url = window.location.href;
        const urlElements = url.split('/');
        // const currentAisle = parseInt(urlElements[6]);
        let currentAisle = parseInt(urlElements[8]);

        if (currentAisleOnly && currentAisle != null) {
            filteredRecords = filteredRecords.filter(v =>
                virtualProductDetails[v.BarCode]
                && virtualProductDetails[v.BarCode].position
                && virtualProductDetails[v.BarCode].position.findIndex(p => p.aisle_id === currentAisle) !== -1);
        }

        if (selectedSuppliers != null && selectedSuppliers.length > 0)
            filteredRecords = filteredRecords.filter(v => v.SupplierId == null || selectedSuppliers.includes(v.SupplierId));
        if (selectedClasses != null && selectedClasses.length > 0)
            filteredRecords = filteredRecords.filter(v => v.ClassId == null || selectedClasses.includes(v.ClassId));
        if (selectedGroups != null && selectedGroups.length > 0)
            filteredRecords = filteredRecords.filter(v => productsMap[v.BarCode] && productsMap[v.BarCode].GroupId && selectedGroups.includes(productsMap[v.BarCode].GroupId || -1));
        if (selectedSubGroups != null && selectedSubGroups.length > 0)
            filteredRecords = filteredRecords.filter(v => productsMap[v.BarCode] && productsMap[v.BarCode].SubGroupId && selectedSubGroups.includes(productsMap[v.BarCode].SubGroupId || -1));
        if (selectedSeries != null && selectedSeries.length > 0)
            filteredRecords = filteredRecords.filter(v => productsMap[v.BarCode] && productsMap[v.BarCode].DegemId && selectedSeries.includes(productsMap[v.BarCode].DegemId || -1));
        if (selectedSegments != null && selectedSegments.length > 0)
            filteredRecords = filteredRecords.filter(v => productsMap[v.BarCode] && productsMap[v.BarCode].SegmentId && selectedSegments.includes(productsMap[v.BarCode].SegmentId || -1));

        // now that I have all the information I'm building a new JSON for the excel output
        let length = filteredRecords.length;
        let reportJSON = [];
        for (let i = 0; i < length; i++) {
            let branch = this.props.branches.filter(branch => branch.BranchId === filteredRecords[i].BranchId)[0];
            let supplier = this.props.suppliers.filter(supplier => supplier.Id === filteredRecords[i].SupplierId)[0];
            let department = this.props.classes.filter(department => department.Id === filteredRecords[i].ClassId)[0];
            let group = this.props.groups.filter(group => group.Id === filteredRecords[i].GroupId)[0];
            let subGroup = this.props.subGroups.filter(subGroup => subGroup.Id === filteredRecords[i].SubGroupId)[0];
            let planogramDetail = this.props.planogram.productDetails[filteredRecords[i].BarCode];
            let segment = this.props.segments.filter(segment => segment.Id === filteredRecords[i].SegmentId)[0];
            let degem = this.props.series.filter(serie => serie.Id === filteredRecords[i].DegemId)[0];
            let object = {
                Branch_Id: filteredRecords[i].BranchId,
                Branch_Name: branch ? branch.Name : null,
                Gondola: planogramDetail && planogramDetail.position ? planogramDetail.position.map(p => p.aisle_id).filter((p, i, list) => list.indexOf(p) === i).join() : null,
                Shelf_Content: planogramDetail ? planogramDetail.maxAmount : null,
                Item_BarCode: filteredRecords[i].BarCode,
                Item_Name: filteredRecords[i].Name,
                Supplier_Id: filteredRecords[i].SupplierId,
                Supplier_Name: supplier ? supplier.Name : null,
                Department_Id: filteredRecords[i].ClassId,
                Department_Name: department ? department.Name : null,
                Group_Id: filteredRecords[i].GroupId,
                Group_Name: group ? group.Name : null,
                SubGroup_Id: filteredRecords[i].SubGroupId,
                SubGroup_Name: subGroup ? subGroup.Name : null,
                TotalAmount: filteredRecords[i].TotalAmount,
                TotalPrice: filteredRecords[i].TotalPrice,
                Segment_Id: filteredRecords[i].SegmentId,
                Segment_Name: segment ? segment.Name : null,
                Degem_Id: filteredRecords[i].DegemId,
                Degem_Name: degem ? degem.Name : null,
                SalePrice: filteredRecords[i].SalePrice,
                SalePriceNoVAT: filteredRecords[i].SalePriceNoVAT,
                BuyPrice: filteredRecords[i].BuyPrice,
                Profit: filteredRecords[i].Profit,
                ProfitPercent: filteredRecords[i].ProfitPercent,
                MinStock: filteredRecords[i].MinStock,
                CrDate: filteredRecords[i].CrDate,
                ExDate: filteredRecords[i].ExDate,
                InStore: filteredRecords[i].InStore
            }
            reportJSON.push(object);
        }

        let ws = XLSX.utils.json_to_sheet(reportJSON);
        let wb = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, "דוח מכירות");
        XLSX.writeFile(wb, "דוח_מכירות.xlsx");
    }

    renderCounter(e: any) {
        console.log(e.row.rowIndex);
        return e.row.rowIndex;
    }
    countTotalLines = (options: any) => {
        const { salesRecords } = this.state;
        const {
            currentAisleOnly,
            selectedGroups,
            selectedSubGroups,
            selectedSeries,
            selectedSuppliers,
            selectedClasses,
            selectedSegments
        } = this.state.report;
        const { productsMap, virtualProductDetails } = this.props;

        if (options.name === 'SelectedRowsSummary') {
            if (options.summaryProcess === 'start') {
                options.totalValue = 0;
            } else if (options.summaryProcess === 'calculate') {
                let filteredRecords = salesRecords;

                const url = window.location.href;
                const urlElements = url.split('/');
                let currentAisle = parseInt(urlElements[8]);

                if (currentAisleOnly && currentAisle != null) {
                    filteredRecords = filteredRecords.filter(v =>
                        virtualProductDetails[v.BarCode]
                        && virtualProductDetails[v.BarCode].position
                        && virtualProductDetails[v.BarCode].position.findIndex(p => p.aisle_id === currentAisle) !== -1);
                }

                if (selectedSuppliers != null && selectedSuppliers.length > 0)
                    filteredRecords = filteredRecords.filter(v => v.SupplierId == null || selectedSuppliers.includes(v.SupplierId));
                if (selectedClasses != null && selectedClasses.length > 0)
                    filteredRecords = filteredRecords.filter(v => v.ClassId == null || selectedClasses.includes(v.ClassId));
                if (selectedGroups != null && selectedGroups.length > 0)
                    filteredRecords = filteredRecords.filter(v => productsMap[v.BarCode] && productsMap[v.BarCode].GroupId && selectedGroups.includes(productsMap[v.BarCode].GroupId || -1));
                if (selectedSubGroups != null && selectedSubGroups.length > 0)
                    filteredRecords = filteredRecords.filter(v => productsMap[v.BarCode] && productsMap[v.BarCode].SubGroupId && selectedSubGroups.includes(productsMap[v.BarCode].SubGroupId || -1));
                if (selectedSeries != null && selectedSeries.length > 0)
                    filteredRecords = filteredRecords.filter(v => productsMap[v.BarCode] && productsMap[v.BarCode].DegemId && selectedSeries.includes(productsMap[v.BarCode].DegemId || -1));
                if (selectedSegments != null && selectedSegments.length > 0)
                    filteredRecords = filteredRecords.filter(v => productsMap[v.BarCode] && productsMap[v.BarCode].SegmentId && selectedSegments.includes(productsMap[v.BarCode].SegmentId || -1));

                // after all the filtering we can finally return the total length of filteredRecords  
                options.totalValue = filteredRecords.length;
            }
        }
    }

    render() {
        if (!this.props.displaySalesReport || this.props.products.length === 0)
            return null;
        const { salesRecords } = this.state;
        const { beginDate,
            endDate,
            currentAisleOnly,
            selectedBranches,
            selectedGroups,
            selectedSubGroups,
            selectedSeries,
            selectedSuppliers,
            selectedClasses,
            selectedSegments,
        } = this.state.report;
        const { productsMap, virtualProductDetails, displayAisle } = this.props;
        let { branches, classes, groups, subGroups, suppliers, series, segments } = this.props;


        let filteredRecords = salesRecords;

  

        const url = window.location.href;
        const urlElements = url.split('/');
        let currentAisle = parseInt(urlElements[8]);

        if (currentAisleOnly && currentAisle != null) {
            filteredRecords = filteredRecords.filter(v =>
                virtualProductDetails[v.BarCode]
                && virtualProductDetails[v.BarCode].position
                && virtualProductDetails[v.BarCode].position.findIndex(p => p.aisle_id === currentAisle) !== -1);
        }

        if (selectedSuppliers != null && selectedSuppliers.length > 0)
            filteredRecords = filteredRecords.filter(v => v.SupplierId == null || selectedSuppliers.includes(v.SupplierId));
        if (selectedClasses != null && selectedClasses.length > 0)
            filteredRecords = filteredRecords.filter(v => v.ClassId == null || selectedClasses.includes(v.ClassId));

        if (selectedGroups != null && selectedGroups.length > 0)
            filteredRecords = filteredRecords.filter(v => productsMap[v.BarCode] && productsMap[v.BarCode].GroupId && selectedGroups.includes(productsMap[v.BarCode].GroupId || -1));
        if (selectedSubGroups != null && selectedSubGroups.length > 0)
            filteredRecords = filteredRecords.filter(v => productsMap[v.BarCode] && productsMap[v.BarCode].SubGroupId && selectedSubGroups.includes(productsMap[v.BarCode].SubGroupId || -1));
        if (selectedSeries != null && selectedSeries.length > 0)
            filteredRecords = filteredRecords.filter(v => productsMap[v.BarCode] && productsMap[v.BarCode].DegemId && selectedSeries.includes(productsMap[v.BarCode].DegemId || -1));
        if (selectedSegments != null && selectedSegments.length > 0)
            filteredRecords = filteredRecords.filter(v => productsMap[v.BarCode] && productsMap[v.BarCode].SegmentId && selectedSegments.includes(productsMap[v.BarCode].SegmentId || -1));

        const salesToSupplier: number[] = []
        const salesToClass: number[] = []

        for (let i = 0; i < salesRecords.length; i++) {
            const record = salesRecords[i];
            if (record.SupplierId != null) {
                salesToSupplier.push(record.SupplierId);
            }
            if (record.ClassId != null) {
                salesToClass.push(record.ClassId);
            }
        }
        const filteredSuppliers = suppliers.filter(s => salesToSupplier.includes(s.Id))
        const filteredClasses = classes.filter(c => salesToClass.includes(c.Id))

        const { height: floatHeight, width: floatWidth } = this.state;

        return (
            <Rnd
                dragHandleClassName="planogram-report-handle"
                cancel="planogram-report-container"
                default={{
                    x: floatWidth / 2,
                    y: floatHeight / 2,
                    width: floatWidth,
                    height: floatHeight,
                }}
                className="float-window planogram-report"
                resizeHandleClasses={{
                    bottomRight: "planogram-report-resize-handle bottom-right",
                }}>
                <div className="float-window-handle planogram-report-handle">
                    <div className="handle-content">Sale Report</div>
                    <div className="float-window-close" onClick={this.props.hideSalesReport}>
                        <FontAwesomeIcon icon={faWindowClose} />
                    </div>
                </div>
                <div className="float-window-container planogram-report-container">
                    {this.state.loading ?
                        <div className="loader"></div>
                        :
                        <React.Fragment>
                            <div className="report-toolbar">
                                <div className="report-toolbar-container">
                                    <Button
                                        rtlEnabled
                                        onClick={(e) => {
                                            this.xlsOutput(e);
                                        }}
                                        className="toolbar-item-small"
                                        icon="exportxlsx" />
                                    <DateBox
                                        rtlEnabled
                                        className="toolbar-item"
                                        placeholder="תאריך תחילה"
                                        value={beginDate}
                                        displayFormat="dd/MM/yy"
                                        pickerType={"calendar"}
                                        onValueChanged={(e) => {
                                            this.setState({
                                                report: {
                                                    ...this.state.report,
                                                    beginDate: e.value
                                                }
                                            })
                                        }} />
                                    <DateBox
                                        rtlEnabled
                                        className="toolbar-item"
                                        placeholder="תאריך סיום"
                                        value={endDate}
                                        displayFormat="dd/MM/yy"
                                        pickerType={"calendar"}
                                        onValueChanged={(e) => {
                                            this.setState({
                                                report: {
                                                    ...this.state.report,
                                                    endDate: e.value
                                                }
                                            })
                                        }} />
                                    <TagBox
                                        rtlEnabled
                                        searchEnabled
                                        showClearButton
                                        className="toolbar-item"
                                        placeholder="סדרה"
                                        value={selectedSeries}
                                        valueExpr="Id"
                                        displayExpr="Name"
                                        searchExpr={["Name", "Id"]}
                                        dataSource={new DataSource({
                                            store: series,
                                            pageSize: 50
                                        })}
                                        onValueChanged={(e) => {
                                            this.setState({
                                                report: {
                                                    ...this.state.report,
                                                    selectedSeries: e.value
                                                }
                                            })
                                        }} />
                                    <SelectBox
                                        rtlEnabled
                                        showClearButton
                                        className="toolbar-item"
                                        placeholder="סניף"
                                        value={selectedBranches}
                                        valueExpr="BranchId"
                                        displayExpr="Name"
                                        items={branches}
                                        onValueChanged={(e) => {
                                            this.setState({
                                                report: {
                                                    ...this.state.report,
                                                    selectedBranches: e.value
                                                }
                                            })
                                        }} />
                                    <Button
                                        rtlEnabled
                                        // disabled={selectedBranches == null || selectedBranches.length === 0}
                                        onClick={this.onSearch}
                                        className="toolbar-item"
                                        icon="search"
                                        text="חפש" />
                                </div>
                                <div className="report-toolbar-container">
                                    {/* <TagBox
                                    rtlEnabled
                                    showClearButton
                                    className="toolbar-item"
                                    valueExpr="BarCode"
                                    displayExpr={(item) => {
                                        if (item)
                                            return item.BarCode + " - " + item.Name
                                    }}
                                    placeholder="ברקוד"
                                    searchEnabled
                                    dataSource={new DataSource({
                                        store: this.props.products,
                                        pageSize: 10,
                                        searchExpr: ["BarCode", "Name"],
                                        searchOperation: "contains",
                                        sort: "BarCode"
                                    })}
                                    onValueChanged={(e) => {
                                        this.setState({
                                            report: {
                                                ...this.state.report,
                                                selectedBarcodes: e.value
                                            }
                                        })
                                    }}
                                /> */}
                                    <TagBox
                                        rtlEnabled
                                        searchEnabled
                                        showClearButton
                                        className="toolbar-item"
                                        placeholder="סגמנט"
                                        value={selectedSegments}
                                        valueExpr="Id"
                                        displayExpr="Name"
                                        searchExpr={["Name", "Id"]}
                                        dataSource={new DataSource({
                                            store: segments,
                                            pageSize: 50
                                        })}
                                        onValueChanged={(e) => {
                                            this.setState({
                                                report: {
                                                    ...this.state.report,
                                                    selectedSegments: e.value
                                                }
                                            })
                                        }} />
                                    <TagBox
                                        rtlEnabled
                                        searchEnabled
                                        showClearButton
                                        className="toolbar-item"
                                        placeholder="תת קבוצה"
                                        value={selectedSubGroups}
                                        valueExpr="Id"
                                        displayExpr="Name"
                                        searchExpr={["Name", "Id"]}
                                        dataSource={new DataSource({
                                            store: subGroups,
                                            pageSize: 50
                                        })}
                                        onValueChanged={(e) => {
                                            this.setState({
                                                report: {
                                                    ...this.state.report,
                                                    selectedSubGroups: e.value
                                                }
                                            })
                                        }} />
                                    <TagBox
                                        rtlEnabled
                                        searchEnabled
                                        showClearButton
                                        className="toolbar-item"
                                        placeholder="קבוצה"
                                        value={selectedGroups}
                                        valueExpr="Id"
                                        displayExpr="Name"
                                        searchExpr={["Name", "Id"]}
                                        dataSource={new DataSource({
                                            store: groups,
                                            pageSize: 50
                                        })}
                                        onValueChanged={(e) => {
                                            this.setState({
                                                report: {
                                                    ...this.state.report,
                                                    selectedGroups: e.value
                                                }
                                            })
                                        }} />
                                    <TagBox
                                        rtlEnabled
                                        searchEnabled
                                        showClearButton
                                        className="toolbar-item"
                                        placeholder="מחלקה"
                                        value={selectedClasses}
                                        valueExpr="Id"
                                        displayExpr="Name"
                                        searchExpr={["Name", "Id"]}
                                        dataSource={new DataSource({
                                            store: classes,
                                            pageSize: 50
                                        })}
                                        onValueChanged={(e) => {
                                            this.setState({
                                                report: {
                                                    ...this.state.report,
                                                    selectedClasses: e.value
                                                }
                                            })
                                        }} />
                                    <TagBox
                                        rtlEnabled
                                        searchEnabled
                                        showClearButton
                                        className="toolbar-item"
                                        placeholder="ספק"
                                        value={selectedSuppliers}
                                        valueExpr="Id"
                                        displayExpr="Name"
                                        searchExpr={["Name", "Id"]}
                                        dataSource={new DataSource({
                                            store: suppliers,
                                            pageSize: 50
                                        })}
                                        onValueChanged={(e) => {
                                            this.setState({
                                                report: {
                                                    ...this.state.report,
                                                    selectedSuppliers: e.value
                                                }
                                            })
                                        }} />
                                    {/* <TagBox
                                        rtlEnabled
                                        searchEnabled
                                        showClearButton
                                        className="toolbar-item"
                                        placeholder="סדרה"
                                        value={selectedSeries}
                                        valueExpr="Id"
                                        displayExpr="Name"
                                        searchExpr={["Name", "Id"]}
                                        dataSource={new DataSource({
                                            store: series,
                                            pageSize: 50
                                        })}
                                        onValueChanged={(e) => {
                                            // console.log(e.value);
                                            this.setState({
                                                report: {
                                                    ...this.state.report,
                                                    selectedSeries: e.value
                                                }
                                            })
                                        }} /> */}
                                    {/* <Button
                                    rtlEnabled
                                    // onClick={this.onSearch}
                                    className="toolbar-item"
                                    icon="sort"
                                    text="סנן" /> */}
                                    <Button
                                        rtlEnabled
                                        className="toolbar-item"
                                        disabled={displayAisle == null}
                                        onClick={(e) => {
                                            this.setState({
                                                report: {
                                                    ...this.state.report,
                                                    currentAisleOnly: currentAisleOnly ? false : true
                                                }
                                            })
                                        }}>
                                        <FontAwesomeIcon icon={faFilter} style={{ display: currentAisleOnly ? "" : "none" }} />
                                        <span>גונדולה נוכחית</span>
                                    </Button>
                                </div>
                            </div>
                            <div className="report-content">
                                <DataGrid
                                    rtlEnabled
                                    showRowLines
                                    showColumnHeaders
                                    allowColumnResizing
                                    allowColumnReordering
                                    className="report-group-table"
                                    noDataText="אין נתונים..."
                                    dataSource={new DataSource({
                                        store: filteredRecords,
                                    })}>
                                    <Texts
                                    />
                                    <FilterRow visible />
                                    <HeaderFilter visible />
                                    <Sorting mode="multiple" />
                                    <GroupPanel visible={true} emptyPanelText={'יש לגרור כותרת טור לכאן על מנת לקבץ לפי טור זה'} />
                                    <Scrolling mode={'virtual'} />
                                    <Column
                                        dataField="InStore"
                                        dataType="boolean"
                                        caption="מיקום"
                                        defaultSortOrder="desc"
                                        cellRender={({ data }) => {
                                            return <ReportPositionCell record={data} />;
                                        }} />
                                    <Column
                                        dataField="BarCode"
                                        dataType="string"
                                        caption="מוצר"
                                        calculateCellValue={(data: CatalogSaleRecord) => productsMap[data.BarCode] ? productsMap[data.BarCode].BarCode + " " + productsMap[data.BarCode].Name : data.BarCode}
                                        cellRender={({ data }) => <ReportTitleCell record={data} />}>
                                        <HeaderFilter
                                            allowSearch
                                            searchMode="Contains"
                                        // searchTimeout={1000}
                                        />
                                    </Column>
                                    <Column
                                        dataField="SupplierId"
                                        caption="ספק"
                                        lookup={{
                                            dataSource: filteredSuppliers,
                                            displayExpr: "Name",
                                            valueExpr: "Id",
                                            allowClearing: true
                                        }}>
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    <Column
                                        allowSearch
                                        dataField="ClassId"
                                        caption="מחלקה"
                                        lookup={{
                                            dataSource: filteredClasses,
                                            displayExpr: "Name",
                                            valueExpr: "Id",
                                            allowClearing: true
                                        }}>
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    <Column
                                        allowSearch
                                        dataField="GroupId"
                                        caption="קבוצה"
                                        lookup={{
                                            dataSource: groups,
                                            displayExpr: "Name",
                                            valueExpr: "Id",
                                            allowClearing: true
                                        }}>
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    <Column
                                        allowSearch
                                        dataField="SubGroupId"
                                        caption="תת קבוצה"
                                        lookup={{
                                            dataSource: subGroups,
                                            displayExpr: "Name",
                                            valueExpr: "Id",
                                            allowClearing: true
                                        }}>
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    <Column
                                        allowSearch
                                        dataField="SegmentId"
                                        caption="סגמנט"
                                        lookup={{
                                            dataSource: segments,
                                            displayExpr: "Name",
                                            valueExpr: "Id",
                                            allowClearing: true
                                        }}>
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    <Column
                                        allowSearch
                                        dataField="DegemId"
                                        caption="סדרה"
                                        lookup={{
                                            dataSource: series,
                                            displayExpr: "Name",
                                            valueExpr: "Id",
                                            allowClearing: true
                                        }}
                                    >
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    <Column
                                        allowSearch
                                        dataField="TotalPrice"
                                        // caption="מכירות כספיות"
                                        caption="כספי"
                                        defaultSortOrder="desc"
                                        format="###,###.##">
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    <Column
                                        allowSearch
                                        dataField="TotalAmount"
                                        // caption="מכירות כמותיות"
                                        caption="כמות"
                                        defaultSortOrder="desc"
                                        format="###,###">
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    <Column
                                        allowSearch
                                        dataField="Profit"
                                        caption="רווח"
                                        format="###,###">
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    <Column
                                        allowSearch
                                        dataField="ProfitPercent"
                                        caption="% רווח"
                                        format="##0.##">
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    <Column
                                        allowSearch
                                        dataField="SalePrice"
                                        caption="מחיר מכירה"
                                        format="###,###.##">
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    <Column
                                        allowSearch
                                        dataField="MinStock"
                                        caption="מלאי ברזל"
                                        dataType={"number"} 
                                        format="###,###">
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    <Column
                                        allowSearch
                                        dataField="PercentOfSubGroupSaleInBranch"
                                        // caption="% תרומה לסניף"
                                        caption="% ת.סניף"
                                        defaultSortOrder="desc">
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    <Column
                                        allowSearch
                                        dataField="PercentOfOfSubGroupSaleInNetwork"
                                        // caption="% תרומה לרשת"
                                        caption="% ת.רשת"
                                        defaultSortOrder="desc">
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    <Column
                                        allowSearch
                                        dataField="CrDate"
                                        caption="כניסה למגוון"
                                        dataType={"date"} 
                                        format={"dd/MM/yyyy"}>
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    <Column
                                        allowSearch
                                        dataField="ExDate"
                                        caption="יציאה ממגוון"
                                        dataType={"date"} 
                                        format={"dd/MM/yyyy"}>
                                        <HeaderFilter allowSearch />
                                    </Column>
                                    <Column
                                        caption="תמונה"
                                        cellRender={({ data }) => <SidebarProductDragable
                                            product={data.BarCode}
                                            className="item-image"
                                        />} />
                                    <Column
                                        caption="הערה"
                                        cellRender={({ data }) => <EditableBarcodeStatus
                                            barcode={data.BarCode}
                                        />} />
                                    <Summary calculateCustomSummary={this.countTotalLines}>
                                        <TotalItem
                                            name={'SelectedRowsSummary'}
                                            summaryType={'custom'}
                                            valueFormat={"###,###,##0.##"}
                                            displayFormat={'סהכ שורות: {0}'}
                                            showInColumn={'SubGroupId'} />
                                        <TotalItem
                                            column={'TotalPrice'}
                                            summaryType={'sum'}
                                            valueFormat={"###,###,##0.##"}
                                            displayFormat={'סהכ {0}'} />
                                        <TotalItem
                                            column={'TotalAmount'}
                                            summaryType={'sum'}
                                            valueFormat={"###,###,##0.##"}
                                            displayFormat={'סהכ {0}'} />
                                    </Summary>
                                </DataGrid>
                            </div>
                        </React.Fragment>
                    }
                </div>
            </Rnd>
        )
    }
}

const reportPositionCellStateToProps = (state: AppState, ownProps: { record: CatalogSaleRecord }) => {
    const product = state.newPlano ? state.newPlano.productsMap[ownProps.record.BarCode] : null;

    return {
        ...ownProps,
        supplier: product && product.SapakId ? state.system.data.suppliersMap[product.SapakId] : undefined,
        class: product && product.ClassesId ? state.system.data.classesMap[product.ClassesId] : undefined,
        product: product,
        planogramDetail: state.planogram.productDetails[ownProps.record.BarCode],
        isOver: state.planogram.display.productDetailer.barcode === ownProps.record.BarCode
    }
};
const reportTitleCellStateToProps = (state: AppState, ownProps: { record: CatalogSaleRecord }) => {
    return {
        ...ownProps,
        product: state.newPlano ? state.newPlano.productsMap[ownProps.record.BarCode] : null,
    }
};

class ReportPositionCellContainer extends React.Component<ReturnType<typeof reportPositionCellStateToProps>> {
    render() {
        console.log('ReportPositionCellContainer this.props', this.props);
        const { product, planogramDetail, isOver } = this.props;

        if (product == null || !planogramDetail || planogramDetail.position.length === 0)
            return null;
        let aisleIds = planogramDetail.position.map(obj => { return obj.aisle_id; });
        aisleIds = aisleIds.filter((v, i) => { return aisleIds.indexOf(v) == i; });
        // at this point aisleIds includce only the unique values of aisle_id so we can use that to check the length

        return (<div className="detail-section" style={{ background: isOver ? "#dbffdc" : "none" }}>
            <div className="detail-row">
                <label>{aisleIds.length > 1 ? "גונדולות" : "גונדולה"}</label>
                <span>{planogramDetail.position.map(p => p.aisle_id).filter((p, i, list) => list.indexOf(p) === i).join()}</span>
            </div>
            <div className="detail-row">
                <label>תכולת מדף</label>
                <span>{planogramDetail.maxAmount}</span>
            </div>
        </div>);
    }
}
class ReportTitleCellContainer extends React.Component<ReturnType<typeof reportTitleCellStateToProps>> {
    render() {
        const { product, record } = this.props;
        return <div>
            <div className="item-title">{product ? product.Name : record.BarCode}</div>
            {product ? <div className="item-subtitle">{record.BarCode}</div> : null}
        </div>
    }
}

const ReportPositionCell = connect(reportPositionCellStateToProps)(ReportPositionCellContainer)
const ReportTitleCell = connect(reportTitleCellStateToProps)(ReportTitleCellContainer)

export default connect(mapStateToProps, mapDispatchToProps)(PlanogramReport)