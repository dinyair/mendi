
import * as React from "react"
import { PlanogramDragDropTypes } from '../generic/DragAndDropType';
import { DragElementWrapper, DropTarget } from 'react-dnd';
import { Trash, Trash2 } from 'react-feather';
import { Icon } from '@src/components';
import { config } from '@src/config';

interface TargetCollectProps {
    connectDropTarget: DragElementWrapper<any>,
    canDrop: boolean,
    isOver: boolean
}

export interface TrashComponentProps {
    // onDrop?: (item: DragDropResultBase, targetItem: DragDropResultBase) => void
}

const dropTypes = [
    PlanogramDragDropTypes.SHELF_ITEM, 
    PlanogramDragDropTypes.SHELF, 
    PlanogramDragDropTypes.SECTION
];

const SectionDroppableTarget = DropTarget<TrashComponentProps, TargetCollectProps>(
    dropTypes, {
        drop(props, monitor) {
            return {
                type: PlanogramDragDropTypes.TRASH,
                payload: {},
            };
        }
    }, (connect, monitor) => ({
        isOver: monitor.isOver(),
        connectDropTarget: connect.dropTarget(),
        canDrop: monitor.canDrop(),
    })
);

class PlanogramTrashComponent extends React.Component<TargetCollectProps, any> {
    render() {
        const { connectDropTarget,  isOver } = this.props;
        return connectDropTarget(
            <div className="planogram-drop-garbage">
            <Icon  src={`../../${config.iconsPath}planogram/editor/${!isOver ? 'trash': 'trash_open'}.svg`} 
							style={{height: '0.5rem', width: '0.5rem'}}/>
                    
            </div>
        )
    }
}
export default SectionDroppableTarget(PlanogramTrashComponent);