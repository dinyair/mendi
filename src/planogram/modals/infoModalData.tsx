import * as React from 'react';

import { GenericModals } from '@src/components/GenericModals/index';
import { ModalInterface,  ModalTypes } from '@src/components/GenericModals/interfaces';

import { useIntl, FormattedMessage } from "react-intl"


interface Props {
    modalType: ModalTypes,
    toggleModal: (type: ModalTypes) => void;
    modalHandlers: any;
    readonly message: any;
    readonly header? :string;
}

export const InfoModalData: React.FC<Props> = props => {
    const {
        toggleModal,
        modalHandlers,
        modalType,
        message,
        header
    } = props;

    const { formatMessage } = useIntl();

    const infoModal: ModalInterface = {
        classNames: 'modal-dialog-centered modal-sm',
        isOpen: modalType === ModalTypes.info, toggle: () => toggleModal(ModalTypes.none), header: header ? header : formatMessage({id:'warning'}), body: message,
        buttons: [{ color: 'primary', onClick: () => toggleModal(ModalTypes.none), label: formatMessage({ id: 'no' }) }, { color: 'primary', outline: true, onClick: modalHandlers, label: formatMessage({ id: 'yes' }) }
        ]
    }


return (
    <GenericModals key={modalType} modal={infoModal} />
);
};


export default InfoModalData;
