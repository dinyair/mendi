import * as React from 'react';

import { GenericModals } from '@src/components/GenericModals/index';
import { ModalInterface,  ModalTypes } from '@src/components/GenericModals/interfaces';

import { useIntl, FormattedMessage } from "react-intl"


interface Props {
    modalType: ModalTypes,
    toggleModal: (type: ModalTypes) => void;
    modalHandlers: any;
    readonly message: any;
    readonly header? :string;
}

export const DeleteModalData: React.FC<Props> = props => {
    const {
        toggleModal,
        modalHandlers,
        modalType,
        message,
        header
    } = props;

    const { formatMessage } = useIntl();

    const deleteModal: ModalInterface = {
        classNames: 'modal-dialog-centered modal-sm',
        isOpen: modalType === ModalTypes.delete, toggle: () => toggleModal(ModalTypes.none), header: header ? header : formatMessage({id:'WarningBeforeDelete'}), body: message,
        buttons: [{ color: 'primary', onClick: () => toggleModal(ModalTypes.none), label: formatMessage({ id: 'No' }) }, { color: 'primary', outline: true, onClick: modalHandlers, label: formatMessage({ id: 'yesDelete' }) }
        ]
    }


return (
    <GenericModals key={modalType} modal={deleteModal} />
);
};


export default DeleteModalData;
