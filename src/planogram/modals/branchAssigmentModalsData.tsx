import * as React from 'react';

import { GenericModals } from '@src/components/GenericModals/index';
import { ModalInterface, ModalTypes } from '@src/components/GenericModals/interfaces';

import { useIntl, FormattedMessage } from "react-intl"


interface Props {
    modalType: ModalTypes,
    toggleModal: (type: ModalTypes) => void;
    modalHandlers: any;
    selectedStore: any;
}

export const BranchAssigmentModalsData: React.FC<Props> = props => {
    const {
        toggleModal,
        modalHandlers,
        modalType,
        selectedStore,
    } = props;

    const { formatMessage } = useIntl();

    const deleteModal: ModalInterface = {
        classNames: 'modal-dialog-centered modal-sm',
        isOpen: modalType === ModalTypes.delete, toggle: () => toggleModal(ModalTypes.none), header: formatMessage({ id: 'delete the planogram?' }), 
        bodyClassNames: 'font-medium-3',
        body: (<>
            {formatMessage({ id: 'you have to delete the association from the branch' })} <span className='text-bold-700'>{selectedStore ? selectedStore.name : ''}</span> {formatMessage({ id: 'this action will delete the planogram from the branch, the changes could not be reversed' })}
        </>),
        buttons: [{ color: 'primary', outline: true, onClick: () => modalHandlers.delete(), label: formatMessage({ id: 'I understood delete' }) }, { color: 'primary', onClick: () => toggleModal(ModalTypes.none), label: formatMessage({ id: 'go back' }) }
        ]
    }

    const addModal: ModalInterface = {
        classNames: 'modal-dialog-centered modal-sm',
        isOpen: modalType === ModalTypes.add, toggle: () => toggleModal(ModalTypes.none), header: formatMessage({ id: 'override the planogram?' }), 
        bodyClassNames: 'font-medium-3',
        body: (<>
            {formatMessage({id:'planogramHasChangeAreYouSure'})}
        </>),
        buttons: [{ color: 'primary', outline: true, onClick: () => modalHandlers.add(), label: formatMessage({ id: 'I understood override' }) }, { color: 'primary', onClick: () => toggleModal(ModalTypes.none), label: formatMessage({ id: 'go back' }) }
        ]
    }


    return (
        <GenericModals key={modalType} modal={modalType === ModalTypes.delete ? deleteModal : modalType === ModalTypes.add ? addModal : undefined} />
    );
};


export default BranchAssigmentModalsData;
