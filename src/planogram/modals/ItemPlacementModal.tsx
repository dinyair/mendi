import * as React from "react";
import { PlacementObject, DimensionObject } from "@src/planogram/shared/store/planogram/planogram.types";
import { ItemDefualtPlacement, ProductDefaultDimensions } from "@src/planogram/shared/store/planogram/planogram.defaults";
import * as planogramProvider from "@src/planogram/shared/api/planogram.provider";
import { useIntl, FormattedMessage } from "react-intl"

type PlacementModalProps = {
    dimensions: DimensionObject,
    maxDimensions: DimensionObject,
    minDimensions?: DimensionObject,
    title?: string,
    subtitle?: string,
    barcode: number,
    init: PlacementObject,
    onSubmit: (placement: PlacementObject) => void
};

const handleFocus = (event: React.FocusEvent<HTMLInputElement>) => event.target.select();

export const ItemPlacementModal: React.FC<PlacementModalProps> = ({ init, onSubmit, maxDimensions, minDimensions, dimensions, title, subtitle, barcode }) => {
    const { formatMessage } = useIntl();

    const [placement, setPlacement] = React.useState<PlacementObject>({
        ...ItemDefualtPlacement,
        ...init
    });

    const [error] = React.useState<string | null>(null);
    const { height: maxHeight, width: maxWidth, depth: maxDepth } = maxDimensions;

    /* barak 10.9.20 * const { height: productHeight, width: productWidth, depth: productDepth } = dimensions; */

    return (
        <form
            onSubmit={(e) => {
                e.stopPropagation();
                onSubmit(placement);
            }}
            onClick={(e) => e.stopPropagation()}>
            <div className="context-title">{title || "Placement"}</div>
            {subtitle != null ? <div className="context-subtitle">{subtitle}</div> : null}
            {error != null ? <div style={{ background: "#e74c3c", padding: "0.2em" }}>
                {error}
            </div> : ""}
            <div className="input-row">
                <label htmlFor="faces"><FormattedMessage id="faces" />: </label>
                <input
                    autoFocus
                    type="number"
                    name="faces"
                    tabIndex={1}
                    min={1}
                    value={placement.faces}
                    onFocus={handleFocus}
                    onChange={(e) => {
                        let value = !e.target.value ? 1 : parseInt(e.target.value);
                        // osher 30.08.20 - sending an alert if ther's enough room on the shelf

                        if (value * /*productWidth*/ (placement.pWidth ? placement.pWidth : ProductDefaultDimensions.width) > maxWidth) {
                            /* osher 13.9.20 * alert("אזהרה! ישנה חריגה מגודל המדף"); */
                            //value = Math.floor(maxWidth / productWidth);
                            // setError("Reached shelf width limit.");
                        }
                        // else setError(null);
                        setPlacement({
                            ...placement,
                            faces: value || 1
                        });
                    }} />
            </div>
            <div className="input-row">
                <label htmlFor="row"><FormattedMessage id="row" />: </label>
                <input
                    type="number"
                    name="row"
                    tabIndex={2}
                    min={1}
                    value={placement.row}
                    onFocus={handleFocus}
                    onChange={(e) => {
                        let value = !e.target.value ? 1 : parseInt(e.target.value);
                        if (value * /*productDepth*/ (placement.pDepth ? placement.pDepth : 0) > maxDepth) {
                            // Osher 30.08.20 - sending an alert if ther's not enough room on the shelf
                            // alert("אזהרה! ישנה חריגה בעומק המדף");
                            alert('- אזהרה! ישנה חריגה מעומק המדף');
                            //value = Math.floor(maxDepth / productDepth);
                            // setError("Reached shelf depth limit.");
                        }
                        // else setError(null);
                        setPlacement({
                            ...placement,
                            row: value || 1
                        });
                    }} />
                {/* <label htmlFor="row">Manual Only: </label>
                <input
                    type="checkbox"
                    name="manual_row_only"
                    tabIndex={3}
                    checked={Boolean(placement.manual_row_only)}
                    onFocus={handleFocus}
                    onChange={(e) => {
                        let value = !e.target.checked ? 0 : 1;
                        setPlacement({
                            ...placement,
                            manual_row_only: value
                        });
                    }} /> */}
            </div>
            <div className="input-row">
                <label htmlFor="stack"><FormattedMessage id="stack" />: </label>
                <input
                    type="number"
                    name="stack"
                    tabIndex={4}
                    min={1}
                    value={placement.stack}
                    onFocus={handleFocus}
                    onChange={async (e) => {
                        console.log('stack onChange value', e.target.value);
                        let value = !e.target.value ? 1 : parseInt(e.target.value);
                        if (value *  /*productHeight*/ (placement.pHeight ? placement.pHeight : 0) > maxHeight) {
                            // Osher 30.08.20 - sending an alert if ther's not enough room on the shelf
                            // alert("אזהרה! ישנה חריגה מגובה המדף");
                            alert('- אזהרה! ישנה חריגה מגובה המדף');
                            // value = Math.floor(maxHeight / productHeight);
                            // setError("Reached shelf height limit.");
                        }
                        // else setError(null);
                        setPlacement({
                            ...placement,
                            stack: value || 1
                        });
                    }} />
            </div>

            <div className="input-row">
                <input type="submit" value={formatMessage({ id: 'submit' })} tabIndex={5} disabled={error != null} />
            </div>
        </form>
    )
}